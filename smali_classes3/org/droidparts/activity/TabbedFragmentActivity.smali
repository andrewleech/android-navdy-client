.class public abstract Lorg/droidparts/activity/TabbedFragmentActivity;
.super Lorg/droidparts/activity/Activity;
.source "TabbedFragmentActivity.java"


# static fields
.field private static final CURR_TAB:Ljava/lang/String; = "__curr_tab__"


# instance fields
.field private enterAnimation:I

.field private exitAnimation:I

.field private final fragmentsOnTab:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<[",
            "Landroid/app/Fragment;",
            ">;"
        }
    .end annotation
.end field

.field private final manuallyHiddenFragments:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Landroid/app/Fragment;",
            ">;"
        }
    .end annotation
.end field

.field private final tabListener:Landroid/app/ActionBar$TabListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Lorg/droidparts/activity/Activity;-><init>()V

    .line 34
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/droidparts/activity/TabbedFragmentActivity;->fragmentsOnTab:Ljava/util/ArrayList;

    .line 36
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lorg/droidparts/activity/TabbedFragmentActivity;->manuallyHiddenFragments:Ljava/util/HashSet;

    .line 38
    new-instance v0, Lorg/droidparts/activity/TabbedFragmentActivity$1;

    invoke-direct {v0, p0}, Lorg/droidparts/activity/TabbedFragmentActivity$1;-><init>(Lorg/droidparts/activity/TabbedFragmentActivity;)V

    iput-object v0, p0, Lorg/droidparts/activity/TabbedFragmentActivity;->tabListener:Landroid/app/ActionBar$TabListener;

    return-void
.end method

.method static synthetic access$000(Lorg/droidparts/activity/TabbedFragmentActivity;Landroid/app/FragmentTransaction;)V
    .locals 0
    .param p0, "x0"    # Lorg/droidparts/activity/TabbedFragmentActivity;
    .param p1, "x1"    # Landroid/app/FragmentTransaction;

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lorg/droidparts/activity/TabbedFragmentActivity;->showFragmentsForCurrentTab(Landroid/app/FragmentTransaction;)V

    return-void
.end method

.method private showFragmentsForCurrentTab(Landroid/app/FragmentTransaction;)V
    .locals 9
    .param p1, "ft"    # Landroid/app/FragmentTransaction;

    .prologue
    .line 130
    invoke-virtual {p0}, Lorg/droidparts/activity/TabbedFragmentActivity;->getCurrentTab()I

    move-result v1

    .line 131
    .local v1, "currTabPos":I
    iget v7, p0, Lorg/droidparts/activity/TabbedFragmentActivity;->enterAnimation:I

    if-eqz v7, :cond_0

    iget v7, p0, Lorg/droidparts/activity/TabbedFragmentActivity;->exitAnimation:I

    if-eqz v7, :cond_0

    .line 132
    iget v7, p0, Lorg/droidparts/activity/TabbedFragmentActivity;->enterAnimation:I

    iget v8, p0, Lorg/droidparts/activity/TabbedFragmentActivity;->exitAnimation:I

    invoke-virtual {p1, v7, v8}, Landroid/app/FragmentTransaction;->setCustomAnimations(II)Landroid/app/FragmentTransaction;

    .line 134
    :cond_0
    const/4 v6, 0x0

    .local v6, "tabPos":I
    :goto_0
    iget-object v7, p0, Lorg/droidparts/activity/TabbedFragmentActivity;->fragmentsOnTab:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-ge v6, v7, :cond_5

    .line 135
    if-ne v6, v1, :cond_2

    const/4 v4, 0x1

    .line 136
    .local v4, "isCurrTab":Z
    :goto_1
    iget-object v7, p0, Lorg/droidparts/activity/TabbedFragmentActivity;->fragmentsOnTab:Ljava/util/ArrayList;

    invoke-virtual {v7, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/app/Fragment;

    .local v0, "arr$":[Landroid/app/Fragment;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_2
    if-ge v3, v5, :cond_4

    aget-object v2, v0, v3

    .line 137
    .local v2, "fragment":Landroid/app/Fragment;
    if-eqz v4, :cond_3

    .line 138
    iget-object v7, p0, Lorg/droidparts/activity/TabbedFragmentActivity;->manuallyHiddenFragments:Ljava/util/HashSet;

    invoke-virtual {v7, v2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 139
    invoke-virtual {p1, v2}, Landroid/app/FragmentTransaction;->show(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 136
    :cond_1
    :goto_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 135
    .end local v0    # "arr$":[Landroid/app/Fragment;
    .end local v2    # "fragment":Landroid/app/Fragment;
    .end local v3    # "i$":I
    .end local v4    # "isCurrTab":Z
    .end local v5    # "len$":I
    :cond_2
    const/4 v4, 0x0

    goto :goto_1

    .line 142
    .restart local v0    # "arr$":[Landroid/app/Fragment;
    .restart local v2    # "fragment":Landroid/app/Fragment;
    .restart local v3    # "i$":I
    .restart local v4    # "isCurrTab":Z
    .restart local v5    # "len$":I
    :cond_3
    invoke-virtual {p1, v2}, Landroid/app/FragmentTransaction;->hide(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    goto :goto_3

    .line 134
    .end local v2    # "fragment":Landroid/app/Fragment;
    :cond_4
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 146
    .end local v0    # "arr$":[Landroid/app/Fragment;
    .end local v3    # "i$":I
    .end local v4    # "isCurrTab":Z
    .end local v5    # "len$":I
    :cond_5
    return-void
.end method


# virtual methods
.method public varargs addTab(ILandroid/app/ActionBar$Tab;[Landroid/app/Fragment;)V
    .locals 2
    .param p1, "position"    # I
    .param p2, "tab"    # Landroid/app/ActionBar$Tab;
    .param p3, "tabFragments"    # [Landroid/app/Fragment;

    .prologue
    .line 83
    iget-object v1, p0, Lorg/droidparts/activity/TabbedFragmentActivity;->tabListener:Landroid/app/ActionBar$TabListener;

    invoke-virtual {p2, v1}, Landroid/app/ActionBar$Tab;->setTabListener(Landroid/app/ActionBar$TabListener;)Landroid/app/ActionBar$Tab;

    .line 84
    invoke-virtual {p0}, Lorg/droidparts/activity/TabbedFragmentActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, p2, p1}, Landroid/app/ActionBar;->addTab(Landroid/app/ActionBar$Tab;I)V

    .line 85
    iget-object v1, p0, Lorg/droidparts/activity/TabbedFragmentActivity;->fragmentsOnTab:Ljava/util/ArrayList;

    invoke-virtual {v1, p1, p3}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 86
    invoke-virtual {p0}, Lorg/droidparts/activity/TabbedFragmentActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 87
    .local v0, "ft":Landroid/app/FragmentTransaction;
    invoke-direct {p0, v0}, Lorg/droidparts/activity/TabbedFragmentActivity;->showFragmentsForCurrentTab(Landroid/app/FragmentTransaction;)V

    .line 88
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    .line 89
    return-void
.end method

.method public varargs addTab(Landroid/app/ActionBar$Tab;[Landroid/app/Fragment;)V
    .locals 1
    .param p1, "tab"    # Landroid/app/ActionBar$Tab;
    .param p2, "tabFragments"    # [Landroid/app/Fragment;

    .prologue
    .line 78
    iget-object v0, p0, Lorg/droidparts/activity/TabbedFragmentActivity;->fragmentsOnTab:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-virtual {p0, v0, p1, p2}, Lorg/droidparts/activity/TabbedFragmentActivity;->addTab(ILandroid/app/ActionBar$Tab;[Landroid/app/Fragment;)V

    .line 79
    return-void
.end method

.method public getCurrentTab()I
    .locals 1

    .prologue
    .line 101
    invoke-virtual {p0}, Lorg/droidparts/activity/TabbedFragmentActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ActionBar;->getSelectedTab()Landroid/app/ActionBar$Tab;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ActionBar$Tab;->getPosition()I

    move-result v0

    return v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 61
    invoke-super {p0, p1}, Lorg/droidparts/activity/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 62
    invoke-virtual {p0}, Lorg/droidparts/activity/TabbedFragmentActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setNavigationMode(I)V

    .line 63
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 73
    invoke-super {p0, p1}, Lorg/droidparts/activity/Activity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 74
    const-string v0, "__curr_tab__"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    invoke-virtual {p0, v0}, Lorg/droidparts/activity/TabbedFragmentActivity;->setCurrentTab(I)V

    .line 75
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 67
    invoke-super {p0, p1}, Lorg/droidparts/activity/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 68
    const-string v0, "__curr_tab__"

    invoke-virtual {p0}, Lorg/droidparts/activity/TabbedFragmentActivity;->getCurrentTab()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 69
    return-void
.end method

.method protected onTabChanged(I)V
    .locals 0
    .param p1, "position"    # I

    .prologue
    .line 106
    return-void
.end method

.method public setCurrentTab(I)V
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 97
    invoke-virtual {p0}, Lorg/droidparts/activity/TabbedFragmentActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/app/ActionBar;->setSelectedNavigationItem(I)V

    .line 98
    return-void
.end method

.method public setCustomAnimations(II)V
    .locals 0
    .param p1, "enter"    # I
    .param p2, "exit"    # I

    .prologue
    .line 92
    iput p1, p0, Lorg/droidparts/activity/TabbedFragmentActivity;->enterAnimation:I

    .line 93
    iput p2, p0, Lorg/droidparts/activity/TabbedFragmentActivity;->exitAnimation:I

    .line 94
    return-void
.end method

.method public varargs setFragmentVisible(Z[Landroid/app/Fragment;)V
    .locals 13
    .param p1, "visible"    # Z
    .param p2, "fragments"    # [Landroid/app/Fragment;

    .prologue
    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 111
    move-object v0, p2

    .local v0, "arr$":[Landroid/app/Fragment;
    array-length v7, v0

    .local v7, "len$":I
    const/4 v5, 0x0

    .local v5, "i$":I
    move v6, v5

    .end local v0    # "arr$":[Landroid/app/Fragment;
    .end local v5    # "i$":I
    .end local v7    # "len$":I
    .local v6, "i$":I
    :goto_0
    if-ge v6, v7, :cond_3

    aget-object v4, v0, v6

    .line 112
    .local v4, "fragment":Landroid/app/Fragment;
    if-eqz p1, :cond_2

    .line 113
    iget-object v9, p0, Lorg/droidparts/activity/TabbedFragmentActivity;->manuallyHiddenFragments:Ljava/util/HashSet;

    invoke-virtual {v9, v4}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 114
    iget-object v9, p0, Lorg/droidparts/activity/TabbedFragmentActivity;->fragmentsOnTab:Ljava/util/ArrayList;

    invoke-virtual {p0}, Lorg/droidparts/activity/TabbedFragmentActivity;->getCurrentTab()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Landroid/app/Fragment;

    .line 116
    .local v3, "currTabFragments":[Landroid/app/Fragment;
    move-object v1, v3

    .local v1, "arr$":[Landroid/app/Fragment;
    array-length v8, v1

    .local v8, "len$":I
    const/4 v5, 0x0

    .end local v6    # "i$":I
    .restart local v5    # "i$":I
    :goto_1
    if-ge v5, v8, :cond_0

    aget-object v2, v1, v5

    .line 117
    .local v2, "currTabFragment":Landroid/app/Fragment;
    if-ne v4, v2, :cond_1

    .line 118
    new-array v9, v12, [Landroid/app/Fragment;

    aput-object v4, v9, v11

    invoke-super {p0, v12, v9}, Lorg/droidparts/activity/Activity;->setFragmentVisible(Z[Landroid/app/Fragment;)V

    .line 111
    .end local v1    # "arr$":[Landroid/app/Fragment;
    .end local v2    # "currTabFragment":Landroid/app/Fragment;
    .end local v3    # "currTabFragments":[Landroid/app/Fragment;
    .end local v5    # "i$":I
    .end local v8    # "len$":I
    :cond_0
    :goto_2
    add-int/lit8 v5, v6, 0x1

    .restart local v5    # "i$":I
    move v6, v5

    .end local v5    # "i$":I
    .restart local v6    # "i$":I
    goto :goto_0

    .line 116
    .end local v6    # "i$":I
    .restart local v1    # "arr$":[Landroid/app/Fragment;
    .restart local v2    # "currTabFragment":Landroid/app/Fragment;
    .restart local v3    # "currTabFragments":[Landroid/app/Fragment;
    .restart local v5    # "i$":I
    .restart local v8    # "len$":I
    :cond_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 123
    .end local v1    # "arr$":[Landroid/app/Fragment;
    .end local v2    # "currTabFragment":Landroid/app/Fragment;
    .end local v3    # "currTabFragments":[Landroid/app/Fragment;
    .end local v5    # "i$":I
    .end local v8    # "len$":I
    .restart local v6    # "i$":I
    :cond_2
    iget-object v9, p0, Lorg/droidparts/activity/TabbedFragmentActivity;->manuallyHiddenFragments:Ljava/util/HashSet;

    invoke-virtual {v9, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 124
    new-array v9, v12, [Landroid/app/Fragment;

    aput-object v4, v9, v11

    invoke-super {p0, v11, v9}, Lorg/droidparts/activity/Activity;->setFragmentVisible(Z[Landroid/app/Fragment;)V

    goto :goto_2

    .line 127
    .end local v4    # "fragment":Landroid/app/Fragment;
    :cond_3
    return-void
.end method
