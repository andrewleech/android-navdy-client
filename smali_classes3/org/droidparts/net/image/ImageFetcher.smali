.class public Lorg/droidparts/net/image/ImageFetcher;
.super Ljava/lang/Object;
.source "ImageFetcher.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/droidparts/net/image/ImageFetcher$SetBitmapRunnable;,
        Lorg/droidparts/net/image/ImageFetcher$FetchAndCacheRunnable;,
        Lorg/droidparts/net/image/ImageFetcher$ReadFromCacheRunnable;,
        Lorg/droidparts/net/image/ImageFetcher$ImageViewSpecRunnable;,
        Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;
    }
.end annotation


# instance fields
.field protected final cacheExecutor:Ljava/util/concurrent/ThreadPoolExecutor;

.field private final diskCache:Lorg/droidparts/net/image/cache/BitmapDiskCache;

.field protected final fetchExecutor:Ljava/util/concurrent/ThreadPoolExecutor;

.field private handler:Landroid/os/Handler;

.field private final memoryCache:Lorg/droidparts/net/image/cache/BitmapMemoryCache;

.field private final mockImageView:Landroid/widget/ImageView;

.field private volatile paused:Z

.field private final pending:Ljava/util/LinkedHashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashSet",
            "<",
            "Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;",
            ">;"
        }
    .end annotation
.end field

.field private final restClient:Lorg/droidparts/net/http/RESTClient;

.field private final wip:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "ctx"    # Landroid/content/Context;

    .prologue
    .line 73
    invoke-static {p1}, Lorg/droidparts/net/image/cache/BitmapMemoryCache;->getDefaultInstance(Landroid/content/Context;)Lorg/droidparts/net/image/cache/BitmapMemoryCache;

    move-result-object v0

    invoke-static {p1}, Lorg/droidparts/net/image/cache/BitmapDiskCache;->getDefaultInstance(Landroid/content/Context;)Lorg/droidparts/net/image/cache/BitmapDiskCache;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1}, Lorg/droidparts/net/image/ImageFetcher;-><init>(Landroid/content/Context;Lorg/droidparts/net/image/cache/BitmapMemoryCache;Lorg/droidparts/net/image/cache/BitmapDiskCache;)V

    .line 75
    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;Ljava/util/concurrent/ThreadPoolExecutor;Lorg/droidparts/net/http/RESTClient;Lorg/droidparts/net/image/cache/BitmapMemoryCache;Lorg/droidparts/net/image/cache/BitmapDiskCache;)V
    .locals 3
    .param p1, "ctx"    # Landroid/content/Context;
    .param p2, "fetchExecutor"    # Ljava/util/concurrent/ThreadPoolExecutor;
    .param p3, "restClient"    # Lorg/droidparts/net/http/RESTClient;
    .param p4, "memoryCache"    # Lorg/droidparts/net/image/cache/BitmapMemoryCache;
    .param p5, "diskCache"    # Lorg/droidparts/net/image/cache/BitmapDiskCache;

    .prologue
    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lorg/droidparts/net/image/ImageFetcher;->pending:Ljava/util/LinkedHashSet;

    .line 67
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lorg/droidparts/net/image/ImageFetcher;->wip:Ljava/util/concurrent/ConcurrentHashMap;

    .line 86
    iput-object p2, p0, Lorg/droidparts/net/image/ImageFetcher;->fetchExecutor:Ljava/util/concurrent/ThreadPoolExecutor;

    .line 87
    iput-object p3, p0, Lorg/droidparts/net/image/ImageFetcher;->restClient:Lorg/droidparts/net/http/RESTClient;

    .line 88
    iput-object p4, p0, Lorg/droidparts/net/image/ImageFetcher;->memoryCache:Lorg/droidparts/net/image/cache/BitmapMemoryCache;

    .line 89
    iput-object p5, p0, Lorg/droidparts/net/image/ImageFetcher;->diskCache:Lorg/droidparts/net/image/cache/BitmapDiskCache;

    .line 90
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lorg/droidparts/net/image/ImageFetcher;->handler:Landroid/os/Handler;

    .line 91
    new-instance v0, Lorg/droidparts/concurrent/thread/BackgroundThreadExecutor;

    const/4 v1, 0x1

    const-string v2, "ImageFetcher-Cache"

    invoke-direct {v0, v1, v2}, Lorg/droidparts/concurrent/thread/BackgroundThreadExecutor;-><init>(ILjava/lang/String;)V

    iput-object v0, p0, Lorg/droidparts/net/image/ImageFetcher;->cacheExecutor:Ljava/util/concurrent/ThreadPoolExecutor;

    .line 92
    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lorg/droidparts/net/image/ImageFetcher;->mockImageView:Landroid/widget/ImageView;

    .line 93
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lorg/droidparts/net/image/cache/BitmapMemoryCache;Lorg/droidparts/net/image/cache/BitmapDiskCache;)V
    .locals 6
    .param p1, "ctx"    # Landroid/content/Context;
    .param p2, "memoryCache"    # Lorg/droidparts/net/image/cache/BitmapMemoryCache;
    .param p3, "diskCache"    # Lorg/droidparts/net/image/cache/BitmapDiskCache;

    .prologue
    .line 79
    new-instance v2, Lorg/droidparts/concurrent/thread/BackgroundThreadExecutor;

    const/4 v0, 0x2

    const-string v1, "ImageFetcher-Fetch"

    invoke-direct {v2, v0, v1}, Lorg/droidparts/concurrent/thread/BackgroundThreadExecutor;-><init>(ILjava/lang/String;)V

    new-instance v3, Lorg/droidparts/net/http/RESTClient;

    invoke-direct {v3, p1}, Lorg/droidparts/net/http/RESTClient;-><init>(Landroid/content/Context;)V

    move-object v0, p0

    move-object v1, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lorg/droidparts/net/image/ImageFetcher;-><init>(Landroid/content/Context;Ljava/util/concurrent/ThreadPoolExecutor;Lorg/droidparts/net/http/RESTClient;Lorg/droidparts/net/image/cache/BitmapMemoryCache;Lorg/droidparts/net/image/cache/BitmapDiskCache;)V

    .line 81
    return-void
.end method

.method static synthetic access$000(Lorg/droidparts/net/image/ImageFetcher;)Lorg/droidparts/net/image/cache/BitmapDiskCache;
    .locals 1
    .param p0, "x0"    # Lorg/droidparts/net/image/ImageFetcher;

    .prologue
    .line 54
    iget-object v0, p0, Lorg/droidparts/net/image/ImageFetcher;->diskCache:Lorg/droidparts/net/image/cache/BitmapDiskCache;

    return-object v0
.end method


# virtual methods
.method attachIfMostRecent(Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;JLandroid/graphics/Bitmap;)V
    .locals 4
    .param p1, "spec"    # Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;
    .param p2, "submitted"    # J
    .param p4, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 295
    iget-object v2, p0, Lorg/droidparts/net/image/ImageFetcher;->wip:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v2, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 296
    .local v0, "mostRecent":Ljava/lang/Long;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    cmp-long v2, p2, v2

    if-nez v2, :cond_1

    .line 297
    iget-object v2, p0, Lorg/droidparts/net/image/ImageFetcher;->wip:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v2, p1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 298
    iget-boolean v2, p0, Lorg/droidparts/net/image/ImageFetcher;->paused:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lorg/droidparts/net/image/ImageFetcher;->pending:Ljava/util/LinkedHashSet;

    invoke-virtual {v2, p1}, Ljava/util/LinkedHashSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 299
    :cond_0
    new-instance v1, Lorg/droidparts/net/image/ImageFetcher$SetBitmapRunnable;

    invoke-direct {v1, p0, p1, p4}, Lorg/droidparts/net/image/ImageFetcher$SetBitmapRunnable;-><init>(Lorg/droidparts/net/image/ImageFetcher;Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;Landroid/graphics/Bitmap;)V

    .line 300
    .local v1, "r":Lorg/droidparts/net/image/ImageFetcher$SetBitmapRunnable;
    invoke-virtual {p0, v1}, Lorg/droidparts/net/image/ImageFetcher;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 303
    .end local v1    # "r":Lorg/droidparts/net/image/ImageFetcher$SetBitmapRunnable;
    :cond_1
    return-void
.end method

.method public attachImage(Ljava/lang/String;Landroid/widget/ImageView;)V
    .locals 1
    .param p1, "imgUrl"    # Ljava/lang/String;
    .param p2, "imageView"    # Landroid/widget/ImageView;

    .prologue
    .line 117
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lorg/droidparts/net/image/ImageFetcher;->attachImage(Ljava/lang/String;Landroid/widget/ImageView;I)V

    .line 118
    return-void
.end method

.method public attachImage(Ljava/lang/String;Landroid/widget/ImageView;I)V
    .locals 1
    .param p1, "imgUrl"    # Ljava/lang/String;
    .param p2, "imageView"    # Landroid/widget/ImageView;
    .param p3, "crossFadeMillis"    # I

    .prologue
    .line 122
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0, p3}, Lorg/droidparts/net/image/ImageFetcher;->attachImage(Ljava/lang/String;Landroid/widget/ImageView;Lorg/droidparts/net/image/ImageReshaper;I)V

    .line 123
    return-void
.end method

.method public attachImage(Ljava/lang/String;Landroid/widget/ImageView;Lorg/droidparts/net/image/ImageReshaper;I)V
    .locals 6
    .param p1, "imgUrl"    # Ljava/lang/String;
    .param p2, "imageView"    # Landroid/widget/ImageView;
    .param p3, "reshaper"    # Lorg/droidparts/net/image/ImageReshaper;
    .param p4, "crossFadeMillis"    # I

    .prologue
    .line 127
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    invoke-virtual/range {v0 .. v5}, Lorg/droidparts/net/image/ImageFetcher;->attachImage(Ljava/lang/String;Landroid/widget/ImageView;Lorg/droidparts/net/image/ImageReshaper;ILorg/droidparts/net/image/ImageFetchListener;)V

    .line 128
    return-void
.end method

.method public attachImage(Ljava/lang/String;Landroid/widget/ImageView;Lorg/droidparts/net/image/ImageReshaper;ILorg/droidparts/net/image/ImageFetchListener;)V
    .locals 7
    .param p1, "imgUrl"    # Ljava/lang/String;
    .param p2, "imageView"    # Landroid/widget/ImageView;
    .param p3, "reshaper"    # Lorg/droidparts/net/image/ImageReshaper;
    .param p4, "crossFadeMillis"    # I
    .param p5, "listener"    # Lorg/droidparts/net/image/ImageFetchListener;

    .prologue
    .line 133
    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v6}, Lorg/droidparts/net/image/ImageFetcher;->attachImage(Ljava/lang/String;Landroid/widget/ImageView;Lorg/droidparts/net/image/ImageReshaper;ILorg/droidparts/net/image/ImageFetchListener;Landroid/graphics/Bitmap;)V

    .line 135
    return-void
.end method

.method public attachImage(Ljava/lang/String;Landroid/widget/ImageView;Lorg/droidparts/net/image/ImageReshaper;ILorg/droidparts/net/image/ImageFetchListener;Landroid/graphics/Bitmap;)V
    .locals 10
    .param p1, "imgUrl"    # Ljava/lang/String;
    .param p2, "imageView"    # Landroid/widget/ImageView;
    .param p3, "reshaper"    # Lorg/droidparts/net/image/ImageReshaper;
    .param p4, "crossFadeMillis"    # I
    .param p5, "listener"    # Lorg/droidparts/net/image/ImageFetchListener;
    .param p6, "inBitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 140
    new-instance v0, Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;

    move-object v1, p2

    move-object v2, p1

    move-object/from16 v3, p6

    move v4, p4

    move-object v5, p3

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;-><init>(Landroid/widget/ImageView;Ljava/lang/String;Landroid/graphics/Bitmap;ILorg/droidparts/net/image/ImageReshaper;Lorg/droidparts/net/image/ImageFetchListener;)V

    .line 142
    .local v0, "spec":Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v8

    .line 143
    .local v8, "submitted":J
    iget-object v1, p0, Lorg/droidparts/net/image/ImageFetcher;->wip:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 144
    iget-boolean v1, p0, Lorg/droidparts/net/image/ImageFetcher;->paused:Z

    if-eqz v1, :cond_1

    .line 145
    iget-object v1, p0, Lorg/droidparts/net/image/ImageFetcher;->pending:Ljava/util/LinkedHashSet;

    invoke-virtual {v1, v0}, Ljava/util/LinkedHashSet;->remove(Ljava/lang/Object;)Z

    .line 146
    iget-object v1, p0, Lorg/droidparts/net/image/ImageFetcher;->pending:Ljava/util/LinkedHashSet;

    invoke-virtual {v1, v0}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    .line 162
    :cond_0
    :goto_0
    return-void

    .line 148
    :cond_1
    if-eqz p5, :cond_2

    .line 149
    invoke-interface {p5, p2, p1}, Lorg/droidparts/net/image/ImageFetchListener;->onFetchAdded(Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 151
    :cond_2
    new-instance v7, Lorg/droidparts/net/image/ImageFetcher$ReadFromCacheRunnable;

    invoke-direct {v7, p0, v0, v8, v9}, Lorg/droidparts/net/image/ImageFetcher$ReadFromCacheRunnable;-><init>(Lorg/droidparts/net/image/ImageFetcher;Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;J)V

    .line 152
    .local v7, "r":Ljava/lang/Runnable;
    iget-object v1, p0, Lorg/droidparts/net/image/ImageFetcher;->cacheExecutor:Ljava/util/concurrent/ThreadPoolExecutor;

    invoke-virtual {v1, v7}, Ljava/util/concurrent/ThreadPoolExecutor;->remove(Ljava/lang/Runnable;)Z

    .line 153
    iget-object v1, p0, Lorg/droidparts/net/image/ImageFetcher;->fetchExecutor:Ljava/util/concurrent/ThreadPoolExecutor;

    invoke-virtual {v1, v7}, Ljava/util/concurrent/ThreadPoolExecutor;->remove(Ljava/lang/Runnable;)Z

    .line 154
    invoke-static {p1}, Lorg/droidparts/util/Strings;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 155
    iget-object v1, p0, Lorg/droidparts/net/image/ImageFetcher;->cacheExecutor:Ljava/util/concurrent/ThreadPoolExecutor;

    invoke-virtual {v1, v7}, Ljava/util/concurrent/ThreadPoolExecutor;->execute(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 157
    :cond_3
    if-eqz p5, :cond_0

    .line 158
    const/4 v1, 0x0

    invoke-interface {p5, p2, p1, v1}, Lorg/droidparts/net/image/ImageFetchListener;->onFetchCompleted(Landroid/widget/ImageView;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method cacheRawImage(Ljava/lang/String;[B)V
    .locals 1
    .param p1, "imgUrl"    # Ljava/lang/String;
    .param p2, "data"    # [B

    .prologue
    .line 268
    iget-object v0, p0, Lorg/droidparts/net/image/ImageFetcher;->diskCache:Lorg/droidparts/net/image/cache/BitmapDiskCache;

    if-eqz v0, :cond_0

    .line 269
    iget-object v0, p0, Lorg/droidparts/net/image/ImageFetcher;->diskCache:Lorg/droidparts/net/image/cache/BitmapDiskCache;

    invoke-virtual {v0, p1, p2}, Lorg/droidparts/net/image/cache/BitmapDiskCache;->put(Ljava/lang/String;[B)Z

    .line 271
    :cond_0
    return-void
.end method

.method public clearCacheOlderThan(I)V
    .locals 6
    .param p1, "hours"    # I

    .prologue
    .line 184
    iget-object v2, p0, Lorg/droidparts/net/image/ImageFetcher;->diskCache:Lorg/droidparts/net/image/cache/BitmapDiskCache;

    if-eqz v2, :cond_0

    .line 185
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    mul-int/lit8 v4, p1, 0x3c

    mul-int/lit8 v4, v4, 0x3c

    mul-int/lit16 v4, v4, 0x3e8

    int-to-long v4, v4

    sub-long v0, v2, v4

    .line 187
    .local v0, "timestamp":J
    iget-object v2, p0, Lorg/droidparts/net/image/ImageFetcher;->cacheExecutor:Ljava/util/concurrent/ThreadPoolExecutor;

    new-instance v3, Lorg/droidparts/net/image/ImageFetcher$1;

    invoke-direct {v3, p0, v0, v1}, Lorg/droidparts/net/image/ImageFetcher$1;-><init>(Lorg/droidparts/net/image/ImageFetcher;J)V

    invoke-virtual {v2, v3}, Ljava/util/concurrent/ThreadPoolExecutor;->execute(Ljava/lang/Runnable;)V

    .line 197
    .end local v0    # "timestamp":J
    :goto_0
    return-void

    .line 195
    :cond_0
    const-string v2, "Disk cache not set."

    invoke-static {v2}, Lorg/droidparts/util/L;->w(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method fetchAndDecode(Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;)Landroid/util/Pair;
    .locals 16
    .param p1, "spec"    # Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;",
            ")",
            "Landroid/util/Pair",
            "<[B",
            "Landroid/util/Pair",
            "<",
            "Landroid/graphics/Bitmap;",
            "Landroid/graphics/BitmapFactory$Options;",
            ">;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 203
    move-object/from16 v0, p1

    iget-object v1, v0, Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;->imgViewRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    .line 204
    .local v4, "imgView":Landroid/widget/ImageView;
    if-nez v4, :cond_0

    .line 205
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "ImageView is null."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 207
    :cond_0
    const/4 v12, 0x0

    .line 208
    .local v12, "bytesReadTotal":I
    const/16 v1, 0x2000

    new-array v10, v1, [B

    .line 209
    .local v10, "buffer":[B
    const/4 v8, 0x0

    .line 210
    .local v8, "bis":Ljava/io/BufferedInputStream;
    new-instance v7, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v7}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 212
    .local v7, "baos":Ljava/io/ByteArrayOutputStream;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lorg/droidparts/net/image/ImageFetcher;->restClient:Lorg/droidparts/net/http/RESTClient;

    move-object/from16 v0, p1

    iget-object v2, v0, Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;->imgUrl:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lorg/droidparts/net/http/RESTClient;->getInputStream(Ljava/lang/String;)Lorg/droidparts/net/http/HTTPResponse;

    move-result-object v14

    .line 213
    .local v14, "resp":Lorg/droidparts/net/http/HTTPResponse;
    const-string v1, "Content-Length"

    invoke-virtual {v14, v1}, Lorg/droidparts/net/http/HTTPResponse;->getHeaderInt(Ljava/lang/String;)I

    move-result v1

    div-int/lit16 v5, v1, 0x400

    .line 214
    .local v5, "kBTotal":I
    iget-object v8, v14, Lorg/droidparts/net/http/HTTPResponse;->inputStream:Lorg/droidparts/net/http/worker/HTTPInputStream;

    .line 216
    :cond_1
    :goto_0
    invoke-virtual {v8, v10}, Ljava/io/BufferedInputStream;->read([B)I

    move-result v11

    .local v11, "bytesRead":I
    const/4 v1, -0x1

    if-eq v11, v1, :cond_2

    .line 217
    const/4 v1, 0x0

    invoke-virtual {v7, v10, v1, v11}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 218
    add-int/2addr v12, v11

    .line 219
    move-object/from16 v0, p1

    iget-object v1, v0, Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;->listener:Lorg/droidparts/net/image/ImageFetchListener;

    if-eqz v1, :cond_1

    .line 220
    div-int/lit16 v6, v12, 0x400

    .line 221
    .local v6, "kBReceived":I
    new-instance v1, Lorg/droidparts/net/image/ImageFetcher$2;

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    invoke-direct/range {v1 .. v6}, Lorg/droidparts/net/image/ImageFetcher$2;-><init>(Lorg/droidparts/net/image/ImageFetcher;Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;Landroid/widget/ImageView;II)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lorg/droidparts/net/image/ImageFetcher;->runOnUiThread(Ljava/lang/Runnable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 237
    .end local v5    # "kBTotal":I
    .end local v6    # "kBReceived":I
    .end local v11    # "bytesRead":I
    .end local v14    # "resp":Lorg/droidparts/net/http/HTTPResponse;
    :catchall_0
    move-exception v1

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/io/Closeable;

    const/4 v3, 0x0

    aput-object v8, v2, v3

    const/4 v3, 0x1

    aput-object v7, v2, v3

    invoke-static {v2}, Lorg/droidparts/util/IOUtils;->silentlyClose([Ljava/io/Closeable;)V

    throw v1

    .line 231
    .restart local v5    # "kBTotal":I
    .restart local v11    # "bytesRead":I
    .restart local v14    # "resp":Lorg/droidparts/net/http/HTTPResponse;
    :cond_2
    :try_start_1
    invoke-virtual {v7}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v13

    .line 232
    .local v13, "data":[B
    move-object/from16 v0, p1

    iget v2, v0, Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;->widthHint:I

    move-object/from16 v0, p1

    iget v3, v0, Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;->heightHint:I

    move-object/from16 v0, p1

    iget-object v15, v0, Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;->configHint:Landroid/graphics/Bitmap$Config;

    move-object/from16 v0, p1

    iget-object v1, v0, Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;->inBitmapRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Bitmap;

    invoke-static {v13, v2, v3, v15, v1}, Lorg/droidparts/inner/BitmapFactoryUtils;->decodeScaled([BIILandroid/graphics/Bitmap$Config;Landroid/graphics/Bitmap;)Landroid/util/Pair;

    move-result-object v9

    .line 235
    .local v9, "bm":Landroid/util/Pair;, "Landroid/util/Pair<Landroid/graphics/Bitmap;Landroid/graphics/BitmapFactory$Options;>;"
    invoke-static {v13, v9}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    .line 237
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/io/Closeable;

    const/4 v3, 0x0

    aput-object v8, v2, v3

    const/4 v3, 0x1

    aput-object v7, v2, v3

    invoke-static {v2}, Lorg/droidparts/util/IOUtils;->silentlyClose([Ljava/io/Closeable;)V

    return-object v1
.end method

.method public getImage(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 2
    .param p1, "imgUrl"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 165
    iget-object v0, p0, Lorg/droidparts/net/image/ImageFetcher;->mockImageView:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, Lorg/droidparts/net/image/ImageFetcher;->getImage(Ljava/lang/String;Landroid/widget/ImageView;Lorg/droidparts/net/image/ImageReshaper;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public getImage(Ljava/lang/String;Landroid/widget/ImageView;Lorg/droidparts/net/image/ImageReshaper;)Landroid/graphics/Bitmap;
    .locals 9
    .param p1, "imgUrl"    # Ljava/lang/String;
    .param p2, "hintImageView"    # Landroid/widget/ImageView;
    .param p3, "reshaper"    # Lorg/droidparts/net/image/ImageReshaper;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 170
    new-instance v0, Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;

    const/4 v4, 0x0

    move-object v1, p2

    move-object v2, p1

    move-object v5, p3

    move-object v6, v3

    invoke-direct/range {v0 .. v6}, Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;-><init>(Landroid/widget/ImageView;Ljava/lang/String;Landroid/graphics/Bitmap;ILorg/droidparts/net/image/ImageReshaper;Lorg/droidparts/net/image/ImageFetchListener;)V

    .line 172
    .local v0, "spec":Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;
    invoke-virtual {p0, v0}, Lorg/droidparts/net/image/ImageFetcher;->readCached(Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 173
    .local v7, "bm":Landroid/graphics/Bitmap;
    if-nez v7, :cond_0

    .line 174
    invoke-virtual {p0, v0}, Lorg/droidparts/net/image/ImageFetcher;->fetchAndDecode(Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;)Landroid/util/Pair;

    move-result-object v8

    .line 175
    .local v8, "bmData":Landroid/util/Pair;, "Landroid/util/Pair<[BLandroid/util/Pair<Landroid/graphics/Bitmap;Landroid/graphics/BitmapFactory$Options;>;>;"
    iget-object v1, v8, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, [B

    invoke-virtual {p0, p1, v1}, Lorg/droidparts/net/image/ImageFetcher;->cacheRawImage(Ljava/lang/String;[B)V

    .line 176
    iget-object v1, v8, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Landroid/util/Pair;

    invoke-virtual {p0, v0, v1}, Lorg/droidparts/net/image/ImageFetcher;->reshapeAndCache(Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;Landroid/util/Pair;)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 178
    .end local v8    # "bmData":Landroid/util/Pair;, "Landroid/util/Pair<[BLandroid/util/Pair<Landroid/graphics/Bitmap;Landroid/graphics/BitmapFactory$Options;>;>;"
    :cond_0
    return-object v7
.end method

.method public pause()V
    .locals 1

    .prologue
    .line 96
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/droidparts/net/image/ImageFetcher;->paused:Z

    .line 97
    return-void
.end method

.method readCached(Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;)Landroid/graphics/Bitmap;
    .locals 8
    .param p1, "spec"    # Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;

    .prologue
    .line 242
    const/4 v6, 0x0

    .line 243
    .local v6, "bm":Landroid/graphics/Bitmap;
    iget-object v0, p0, Lorg/droidparts/net/image/ImageFetcher;->memoryCache:Lorg/droidparts/net/image/cache/BitmapMemoryCache;

    if-eqz v0, :cond_0

    .line 244
    iget-object v0, p0, Lorg/droidparts/net/image/ImageFetcher;->memoryCache:Lorg/droidparts/net/image/cache/BitmapMemoryCache;

    iget-object v1, p1, Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;->cacheKey:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lorg/droidparts/net/image/cache/BitmapMemoryCache;->get(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 246
    :cond_0
    if-nez v6, :cond_1

    iget-object v0, p0, Lorg/droidparts/net/image/ImageFetcher;->diskCache:Lorg/droidparts/net/image/cache/BitmapDiskCache;

    if-eqz v0, :cond_1

    .line 247
    iget-object v0, p0, Lorg/droidparts/net/image/ImageFetcher;->diskCache:Lorg/droidparts/net/image/cache/BitmapDiskCache;

    iget-object v1, p1, Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;->cacheKey:Ljava/lang/String;

    iget v2, p1, Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;->widthHint:I

    iget v3, p1, Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;->heightHint:I

    iget-object v4, p1, Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;->configHint:Landroid/graphics/Bitmap$Config;

    iget-object v5, p1, Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;->inBitmapRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v5}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/graphics/Bitmap;

    invoke-virtual/range {v0 .. v5}, Lorg/droidparts/net/image/cache/BitmapDiskCache;->get(Ljava/lang/String;IILandroid/graphics/Bitmap$Config;Landroid/graphics/Bitmap;)Landroid/util/Pair;

    move-result-object v7

    .line 250
    .local v7, "bmData":Landroid/util/Pair;, "Landroid/util/Pair<Landroid/graphics/Bitmap;Landroid/graphics/BitmapFactory$Options;>;"
    if-eqz v7, :cond_2

    .line 251
    iget-object v6, v7, Landroid/util/Pair;->first:Ljava/lang/Object;

    .end local v6    # "bm":Landroid/graphics/Bitmap;
    check-cast v6, Landroid/graphics/Bitmap;

    .line 252
    .restart local v6    # "bm":Landroid/graphics/Bitmap;
    iget-object v0, p0, Lorg/droidparts/net/image/ImageFetcher;->memoryCache:Lorg/droidparts/net/image/cache/BitmapMemoryCache;

    if-eqz v0, :cond_1

    .line 253
    iget-object v0, p0, Lorg/droidparts/net/image/ImageFetcher;->memoryCache:Lorg/droidparts/net/image/cache/BitmapMemoryCache;

    iget-object v1, p1, Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;->cacheKey:Ljava/lang/String;

    invoke-virtual {v0, v1, v6}, Lorg/droidparts/net/image/cache/BitmapMemoryCache;->put(Ljava/lang/String;Landroid/graphics/Bitmap;)Z

    .line 264
    .end local v7    # "bmData":Landroid/util/Pair;, "Landroid/util/Pair<Landroid/graphics/Bitmap;Landroid/graphics/BitmapFactory$Options;>;"
    :cond_1
    :goto_0
    return-object v6

    .line 256
    .restart local v7    # "bmData":Landroid/util/Pair;, "Landroid/util/Pair<Landroid/graphics/Bitmap;Landroid/graphics/BitmapFactory$Options;>;"
    :cond_2
    iget-object v0, p0, Lorg/droidparts/net/image/ImageFetcher;->diskCache:Lorg/droidparts/net/image/cache/BitmapDiskCache;

    iget-object v1, p1, Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;->imgUrl:Ljava/lang/String;

    iget v2, p1, Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;->widthHint:I

    iget v3, p1, Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;->heightHint:I

    iget-object v4, p1, Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;->configHint:Landroid/graphics/Bitmap$Config;

    iget-object v5, p1, Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;->inBitmapRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v5}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/graphics/Bitmap;

    invoke-virtual/range {v0 .. v5}, Lorg/droidparts/net/image/cache/BitmapDiskCache;->get(Ljava/lang/String;IILandroid/graphics/Bitmap$Config;Landroid/graphics/Bitmap;)Landroid/util/Pair;

    move-result-object v7

    .line 259
    if-eqz v7, :cond_1

    .line 260
    invoke-virtual {p0, p1, v7}, Lorg/droidparts/net/image/ImageFetcher;->reshapeAndCache(Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;Landroid/util/Pair;)Landroid/graphics/Bitmap;

    move-result-object v6

    goto :goto_0
.end method

.method reshapeAndCache(Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;Landroid/util/Pair;)Landroid/graphics/Bitmap;
    .locals 5
    .param p1, "spec"    # Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;",
            "Landroid/util/Pair",
            "<",
            "Landroid/graphics/Bitmap;",
            "Landroid/graphics/BitmapFactory$Options;",
            ">;)",
            "Landroid/graphics/Bitmap;"
        }
    .end annotation

    .prologue
    .line 275
    .local p2, "bmData":Landroid/util/Pair;, "Landroid/util/Pair<Landroid/graphics/Bitmap;Landroid/graphics/BitmapFactory$Options;>;"
    iget-object v0, p2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Landroid/graphics/Bitmap;

    .line 276
    .local v0, "bm":Landroid/graphics/Bitmap;
    iget-object v3, p1, Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;->reshaper:Lorg/droidparts/net/image/ImageReshaper;

    if-eqz v3, :cond_1

    .line 277
    iget-object v3, p1, Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;->reshaper:Lorg/droidparts/net/image/ImageReshaper;

    invoke-interface {v3, v0}, Lorg/droidparts/net/image/ImageReshaper;->reshape(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 278
    .local v2, "reshapedBm":Landroid/graphics/Bitmap;
    if-eq v0, v2, :cond_0

    .line 279
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 281
    :cond_0
    move-object v0, v2

    .line 283
    .end local v2    # "reshapedBm":Landroid/graphics/Bitmap;
    :cond_1
    iget-object v3, p0, Lorg/droidparts/net/image/ImageFetcher;->memoryCache:Lorg/droidparts/net/image/cache/BitmapMemoryCache;

    if-eqz v3, :cond_2

    .line 284
    iget-object v3, p0, Lorg/droidparts/net/image/ImageFetcher;->memoryCache:Lorg/droidparts/net/image/cache/BitmapMemoryCache;

    iget-object v4, p1, Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;->cacheKey:Ljava/lang/String;

    invoke-virtual {v3, v4, v0}, Lorg/droidparts/net/image/cache/BitmapMemoryCache;->put(Ljava/lang/String;Landroid/graphics/Bitmap;)Z

    .line 286
    :cond_2
    iget-object v3, p0, Lorg/droidparts/net/image/ImageFetcher;->diskCache:Lorg/droidparts/net/image/cache/BitmapDiskCache;

    if-eqz v3, :cond_3

    iget-object v3, p1, Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;->reshaper:Lorg/droidparts/net/image/ImageReshaper;

    if-eqz v3, :cond_3

    .line 287
    iget-object v4, p1, Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;->reshaper:Lorg/droidparts/net/image/ImageReshaper;

    iget-object v3, p2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, Landroid/graphics/BitmapFactory$Options;

    iget-object v3, v3, Landroid/graphics/BitmapFactory$Options;->outMimeType:Ljava/lang/String;

    invoke-interface {v4, v3}, Lorg/droidparts/net/image/ImageReshaper;->getCacheFormat(Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v1

    .line 289
    .local v1, "cacheFormat":Landroid/util/Pair;, "Landroid/util/Pair<Landroid/graphics/Bitmap$CompressFormat;Ljava/lang/Integer;>;"
    iget-object v3, p0, Lorg/droidparts/net/image/ImageFetcher;->diskCache:Lorg/droidparts/net/image/cache/BitmapDiskCache;

    iget-object v4, p1, Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;->cacheKey:Ljava/lang/String;

    invoke-virtual {v3, v4, v0, v1}, Lorg/droidparts/net/image/cache/BitmapDiskCache;->put(Ljava/lang/String;Landroid/graphics/Bitmap;Landroid/util/Pair;)Z

    .line 291
    .end local v1    # "cacheFormat":Landroid/util/Pair;, "Landroid/util/Pair<Landroid/graphics/Bitmap$CompressFormat;Ljava/lang/Integer;>;"
    :cond_3
    return-object v0
.end method

.method public resume(Z)V
    .locals 9
    .param p1, "executePendingTasks"    # Z

    .prologue
    .line 100
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/droidparts/net/image/ImageFetcher;->paused:Z

    .line 101
    if-eqz p1, :cond_1

    .line 102
    iget-object v0, p0, Lorg/droidparts/net/image/ImageFetcher;->pending:Ljava/util/LinkedHashSet;

    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;

    .line 103
    .local v8, "spec":Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;
    iget-object v0, v8, Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;->imgViewRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 104
    .local v2, "imgView":Landroid/widget/ImageView;
    if-eqz v2, :cond_0

    .line 105
    iget-object v1, v8, Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;->imgUrl:Ljava/lang/String;

    iget-object v3, v8, Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;->reshaper:Lorg/droidparts/net/image/ImageReshaper;

    iget v4, v8, Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;->crossFadeMillis:I

    iget-object v5, v8, Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;->listener:Lorg/droidparts/net/image/ImageFetchListener;

    iget-object v0, v8, Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;->inBitmapRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/graphics/Bitmap;

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Lorg/droidparts/net/image/ImageFetcher;->attachImage(Ljava/lang/String;Landroid/widget/ImageView;Lorg/droidparts/net/image/ImageReshaper;ILorg/droidparts/net/image/ImageFetchListener;Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 111
    .end local v2    # "imgView":Landroid/widget/ImageView;
    .end local v7    # "i$":Ljava/util/Iterator;
    .end local v8    # "spec":Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;
    :cond_1
    iget-object v0, p0, Lorg/droidparts/net/image/ImageFetcher;->pending:Ljava/util/LinkedHashSet;

    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->clear()V

    .line 112
    return-void
.end method

.method runOnUiThread(Ljava/lang/Runnable;)V
    .locals 3
    .param p1, "r"    # Ljava/lang/Runnable;

    .prologue
    .line 306
    iget-object v1, p0, Lorg/droidparts/net/image/ImageFetcher;->handler:Landroid/os/Handler;

    invoke-virtual {v1, p1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    move-result v0

    .line 308
    .local v0, "success":Z
    :goto_0
    if-nez v0, :cond_0

    .line 309
    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Lorg/droidparts/net/image/ImageFetcher;->handler:Landroid/os/Handler;

    .line 310
    invoke-virtual {p0, p1}, Lorg/droidparts/net/image/ImageFetcher;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 312
    :cond_0
    return-void
.end method
