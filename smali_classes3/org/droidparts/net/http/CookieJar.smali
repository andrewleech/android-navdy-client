.class public Lorg/droidparts/net/http/CookieJar;
.super Ljava/net/CookieHandler;
.source "CookieJar.java"

# interfaces
.implements Lorg/apache/http/client/CookieStore;


# static fields
.field private static final SEP:Ljava/lang/String; = ";"


# instance fields
.field private final cookieSpec:Lorg/apache/http/cookie/CookieSpec;

.field private final cookies:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Lorg/apache/http/cookie/Cookie;",
            ">;"
        }
    .end annotation
.end field

.field private persistCookies:Z

.field private final prefs:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "ctx"    # Landroid/content/Context;

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/net/CookieHandler;-><init>()V

    .line 152
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lorg/droidparts/net/http/CookieJar;->cookies:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 59
    new-instance v0, Lorg/apache/http/impl/cookie/BrowserCompatSpec;

    invoke-direct {v0}, Lorg/apache/http/impl/cookie/BrowserCompatSpec;-><init>()V

    iput-object v0, p0, Lorg/droidparts/net/http/CookieJar;->cookieSpec:Lorg/apache/http/cookie/CookieSpec;

    .line 60
    const-string v0, "droidparts_restclient_cookies"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lorg/droidparts/net/http/CookieJar;->prefs:Landroid/content/SharedPreferences;

    .line 62
    return-void
.end method

.method private static fromString(Ljava/lang/String;)Lorg/apache/http/cookie/Cookie;
    .locals 6
    .param p0, "str"    # Ljava/lang/String;

    .prologue
    .line 230
    const-string v2, ";"

    invoke-virtual {p0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 231
    .local v1, "parts":[Ljava/lang/String;
    new-instance v0, Lorg/apache/http/impl/cookie/BasicClientCookie;

    const/4 v2, 0x0

    aget-object v2, v1, v2

    const/4 v3, 0x1

    aget-object v3, v1, v3

    invoke-direct {v0, v2, v3}, Lorg/apache/http/impl/cookie/BasicClientCookie;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    .local v0, "cookie":Lorg/apache/http/impl/cookie/BasicClientCookie;
    const/4 v2, 0x2

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Lorg/apache/http/impl/cookie/BasicClientCookie;->setDomain(Ljava/lang/String;)V

    .line 233
    const/4 v2, 0x3

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Lorg/apache/http/impl/cookie/BasicClientCookie;->setPath(Ljava/lang/String;)V

    .line 234
    array-length v2, v1

    const/4 v3, 0x5

    if-ne v2, v3, :cond_0

    .line 235
    new-instance v2, Ljava/util/Date;

    const/4 v3, 0x4

    aget-object v3, v1, v3

    invoke-static {v3}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-direct {v2, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v2}, Lorg/apache/http/impl/cookie/BasicClientCookie;->setExpiryDate(Ljava/util/Date;)V

    .line 237
    :cond_0
    return-object v0
.end method

.method private getCookies(Ljava/net/URI;)Ljava/util/Collection;
    .locals 11
    .param p1, "uri"    # Ljava/net/URI;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/net/URI;",
            ")",
            "Ljava/util/Collection",
            "<",
            "Lorg/apache/http/cookie/Cookie;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 172
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 173
    .local v3, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lorg/apache/http/cookie/Cookie;>;"
    invoke-virtual {p0}, Lorg/droidparts/net/http/CookieJar;->getCookies()Ljava/util/List;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/http/cookie/Cookie;

    .line 174
    .local v1, "cookie":Lorg/apache/http/cookie/Cookie;
    invoke-virtual {p1}, Ljava/net/URI;->getHost()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v1}, Lorg/apache/http/cookie/Cookie;->getDomain()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-virtual {p1}, Ljava/net/URI;->getPath()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v1}, Lorg/apache/http/cookie/Cookie;->getPath()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_2

    move v6, v7

    .line 176
    .local v6, "suitable":Z
    :goto_1
    if-eqz v6, :cond_0

    .line 177
    const/4 v5, 0x1

    .line 178
    .local v5, "put":Z
    invoke-interface {v1}, Lorg/apache/http/cookie/Cookie;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v9}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 179
    invoke-interface {v1}, Lorg/apache/http/cookie/Cookie;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/http/cookie/Cookie;

    .line 180
    .local v4, "otherCookie":Lorg/apache/http/cookie/Cookie;
    invoke-interface {v1}, Lorg/apache/http/cookie/Cookie;->getPath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    invoke-interface {v4}, Lorg/apache/http/cookie/Cookie;->getPath()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    if-le v9, v10, :cond_3

    move v0, v7

    .line 182
    .local v0, "betterMatchingPath":Z
    :goto_2
    move v5, v0

    .line 184
    .end local v0    # "betterMatchingPath":Z
    .end local v4    # "otherCookie":Lorg/apache/http/cookie/Cookie;
    :cond_1
    if-eqz v5, :cond_0

    .line 185
    invoke-interface {v1}, Lorg/apache/http/cookie/Cookie;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v9, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .end local v5    # "put":Z
    .end local v6    # "suitable":Z
    :cond_2
    move v6, v8

    .line 174
    goto :goto_1

    .restart local v4    # "otherCookie":Lorg/apache/http/cookie/Cookie;
    .restart local v5    # "put":Z
    .restart local v6    # "suitable":Z
    :cond_3
    move v0, v8

    .line 180
    goto :goto_2

    .line 189
    .end local v1    # "cookie":Lorg/apache/http/cookie/Cookie;
    .end local v4    # "otherCookie":Lorg/apache/http/cookie/Cookie;
    .end local v5    # "put":Z
    .end local v6    # "suitable":Z
    :cond_4
    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v7

    return-object v7
.end method

.method private static isEqual(Lorg/apache/http/cookie/Cookie;Lorg/apache/http/cookie/Cookie;)Z
    .locals 3
    .param p0, "first"    # Lorg/apache/http/cookie/Cookie;
    .param p1, "second"    # Lorg/apache/http/cookie/Cookie;

    .prologue
    .line 243
    invoke-interface {p0}, Lorg/apache/http/cookie/Cookie;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1}, Lorg/apache/http/cookie/Cookie;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Lorg/apache/http/cookie/Cookie;->getDomain()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1}, Lorg/apache/http/cookie/Cookie;->getDomain()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Lorg/apache/http/cookie/Cookie;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1}, Lorg/apache/http/cookie/Cookie;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 246
    .local v0, "equal":Z
    :goto_0
    return v0

    .line 243
    .end local v0    # "equal":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private parseCookies(Ljava/net/URI;Ljava/util/List;)Ljava/util/List;
    .locals 10
    .param p1, "uri"    # Ljava/net/URI;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/net/URI;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/cookie/Cookie;",
            ">;"
        }
    .end annotation

    .prologue
    .line 155
    .local p2, "cookieHeaders":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 156
    .local v1, "cookies":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/cookie/Cookie;>;"
    invoke-virtual {p1}, Ljava/net/URI;->getPort()I

    move-result v8

    if-gez v8, :cond_0

    const/16 v6, 0x50

    .line 157
    .local v6, "port":I
    :goto_0
    const-string v8, "https"

    invoke-virtual {p1}, Ljava/net/URI;->getScheme()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    .line 158
    .local v7, "secure":Z
    new-instance v5, Lorg/apache/http/cookie/CookieOrigin;

    invoke-virtual {p1}, Ljava/net/URI;->getHost()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1}, Ljava/net/URI;->getPath()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v5, v8, v6, v9, v7}, Lorg/apache/http/cookie/CookieOrigin;-><init>(Ljava/lang/String;ILjava/lang/String;Z)V

    .line 160
    .local v5, "origin":Lorg/apache/http/cookie/CookieOrigin;
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 161
    .local v0, "cookieHeader":Ljava/lang/String;
    new-instance v3, Lorg/apache/http/message/BasicHeader;

    const-string v8, "Set-Cookie"

    invoke-direct {v3, v8, v0}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    .local v3, "header":Lorg/apache/http/message/BasicHeader;
    :try_start_0
    iget-object v8, p0, Lorg/droidparts/net/http/CookieJar;->cookieSpec:Lorg/apache/http/cookie/CookieSpec;

    invoke-interface {v8, v3, v5}, Lorg/apache/http/cookie/CookieSpec;->parse(Lorg/apache/http/Header;Lorg/apache/http/cookie/CookieOrigin;)Ljava/util/List;

    move-result-object v8

    invoke-virtual {v1, v8}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z
    :try_end_0
    .catch Lorg/apache/http/cookie/MalformedCookieException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 164
    :catch_0
    move-exception v2

    .line 165
    .local v2, "e":Lorg/apache/http/cookie/MalformedCookieException;
    invoke-static {v2}, Lorg/droidparts/util/L;->d(Ljava/lang/Object;)V

    goto :goto_1

    .line 156
    .end local v0    # "cookieHeader":Ljava/lang/String;
    .end local v2    # "e":Lorg/apache/http/cookie/MalformedCookieException;
    .end local v3    # "header":Lorg/apache/http/message/BasicHeader;
    .end local v4    # "i$":Ljava/util/Iterator;
    .end local v5    # "origin":Lorg/apache/http/cookie/CookieOrigin;
    .end local v6    # "port":I
    .end local v7    # "secure":Z
    :cond_0
    invoke-virtual {p1}, Ljava/net/URI;->getPort()I

    move-result v6

    goto :goto_0

    .line 168
    .restart local v4    # "i$":Ljava/util/Iterator;
    .restart local v5    # "origin":Lorg/apache/http/cookie/CookieOrigin;
    .restart local v6    # "port":I
    .restart local v7    # "secure":Z
    :cond_1
    return-object v1
.end method

.method private persistCookies()V
    .locals 4

    .prologue
    .line 193
    iget-object v2, p0, Lorg/droidparts/net/http/CookieJar;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 194
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    .line 195
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lorg/droidparts/net/http/CookieJar;->cookies:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 196
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    iget-object v2, p0, Lorg/droidparts/net/http/CookieJar;->cookies:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v2, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/http/cookie/Cookie;

    invoke-static {v2}, Lorg/droidparts/net/http/CookieJar;->toString(Lorg/apache/http/cookie/Cookie;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v3, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 195
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 198
    :cond_0
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 199
    return-void
.end method

.method private restoreCookies()V
    .locals 5

    .prologue
    .line 202
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const v2, 0x7fffffff

    if-ge v0, v2, :cond_0

    .line 203
    iget-object v2, p0, Lorg/droidparts/net/http/CookieJar;->prefs:Landroid/content/SharedPreferences;

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 204
    .local v1, "str":Ljava/lang/String;
    if-nez v1, :cond_1

    .line 210
    .end local v1    # "str":Ljava/lang/String;
    :cond_0
    return-void

    .line 207
    .restart local v1    # "str":Ljava/lang/String;
    :cond_1
    iget-object v2, p0, Lorg/droidparts/net/http/CookieJar;->cookies:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-static {v1}, Lorg/droidparts/net/http/CookieJar;->fromString(Ljava/lang/String;)Lorg/apache/http/cookie/Cookie;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 202
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private static toString(Lorg/apache/http/cookie/Cookie;)Ljava/lang/String;
    .locals 4
    .param p0, "cookie"    # Lorg/apache/http/cookie/Cookie;

    .prologue
    .line 213
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 214
    .local v1, "sb":Ljava/lang/StringBuilder;
    invoke-interface {p0}, Lorg/apache/http/cookie/Cookie;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 215
    const-string v2, ";"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 216
    invoke-interface {p0}, Lorg/apache/http/cookie/Cookie;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 217
    const-string v2, ";"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 218
    invoke-interface {p0}, Lorg/apache/http/cookie/Cookie;->getDomain()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 219
    const-string v2, ";"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 220
    invoke-interface {p0}, Lorg/apache/http/cookie/Cookie;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 221
    invoke-interface {p0}, Lorg/apache/http/cookie/Cookie;->getExpiryDate()Ljava/util/Date;

    move-result-object v0

    .line 222
    .local v0, "expiryDate":Ljava/util/Date;
    if-eqz v0, :cond_0

    .line 223
    const-string v2, ";"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 224
    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 226
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method


# virtual methods
.method public addCookie(Lorg/apache/http/cookie/Cookie;)V
    .locals 5
    .param p1, "cookie"    # Lorg/apache/http/cookie/Cookie;

    .prologue
    .line 105
    const-string v2, "Got a cookie: %s."

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-static {v2, v3}, Lorg/droidparts/util/L;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 106
    iget-object v2, p0, Lorg/droidparts/net/http/CookieJar;->cookies:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/http/cookie/Cookie;>;"
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 107
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/http/cookie/Cookie;

    .line 108
    .local v0, "c":Lorg/apache/http/cookie/Cookie;
    invoke-static {p1, v0}, Lorg/droidparts/net/http/CookieJar;->isEqual(Lorg/apache/http/cookie/Cookie;Lorg/apache/http/cookie/Cookie;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 109
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 112
    .end local v0    # "c":Lorg/apache/http/cookie/Cookie;
    :cond_1
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    invoke-interface {p1, v2}, Lorg/apache/http/cookie/Cookie;->isExpired(Ljava/util/Date;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 113
    iget-object v2, p0, Lorg/droidparts/net/http/CookieJar;->cookies:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v2, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 115
    :cond_2
    iget-boolean v2, p0, Lorg/droidparts/net/http/CookieJar;->persistCookies:Z

    if-eqz v2, :cond_3

    .line 116
    invoke-direct {p0}, Lorg/droidparts/net/http/CookieJar;->persistCookies()V

    .line 118
    :cond_3
    return-void
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lorg/droidparts/net/http/CookieJar;->cookies:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->clear()V

    .line 123
    iget-boolean v0, p0, Lorg/droidparts/net/http/CookieJar;->persistCookies:Z

    if-eqz v0, :cond_0

    .line 124
    invoke-direct {p0}, Lorg/droidparts/net/http/CookieJar;->persistCookies()V

    .line 126
    :cond_0
    return-void
.end method

.method public clearExpired(Ljava/util/Date;)Z
    .locals 4
    .param p1, "date"    # Ljava/util/Date;

    .prologue
    .line 130
    const/4 v2, 0x0

    .line 131
    .local v2, "purged":Z
    iget-object v3, p0, Lorg/droidparts/net/http/CookieJar;->cookies:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v3}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/http/cookie/Cookie;>;"
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 132
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/http/cookie/Cookie;

    .line 133
    .local v0, "cookie":Lorg/apache/http/cookie/Cookie;
    invoke-interface {v0, p1}, Lorg/apache/http/cookie/Cookie;->isExpired(Ljava/util/Date;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 134
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    .line 135
    const/4 v2, 0x1

    goto :goto_0

    .line 138
    .end local v0    # "cookie":Lorg/apache/http/cookie/Cookie;
    :cond_1
    iget-boolean v3, p0, Lorg/droidparts/net/http/CookieJar;->persistCookies:Z

    if-eqz v3, :cond_2

    if-eqz v2, :cond_2

    .line 139
    invoke-direct {p0}, Lorg/droidparts/net/http/CookieJar;->persistCookies()V

    .line 141
    :cond_2
    return v2
.end method

.method public get(Ljava/net/URI;Ljava/util/Map;)Ljava/util/Map;
    .locals 5
    .param p1, "uri"    # Ljava/net/URI;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/net/URI;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 77
    .local p2, "requestHeaders":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;"
    new-instance v3, Ljava/util/Date;

    invoke-direct {v3}, Ljava/util/Date;-><init>()V

    invoke-virtual {p0, v3}, Lorg/droidparts/net/http/CookieJar;->clearExpired(Ljava/util/Date;)Z

    .line 78
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 79
    .local v1, "cookies":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-direct {p0, p1}, Lorg/droidparts/net/http/CookieJar;->getCookies(Ljava/net/URI;)Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/http/cookie/Cookie;

    .line 80
    .local v0, "cookie":Lorg/apache/http/cookie/Cookie;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v0}, Lorg/apache/http/cookie/Cookie;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v0}, Lorg/apache/http/cookie/Cookie;->getValue()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 82
    .end local v0    # "cookie":Lorg/apache/http/cookie/Cookie;
    :cond_0
    const-string v3, "Cookie"

    const-string v4, ";"

    invoke-static {v1, v4}, Lorg/droidparts/util/Strings;->join(Ljava/util/Collection;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    invoke-static {v3, v4}, Ljava/util/Collections;->singletonMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v3

    return-object v3
.end method

.method public getCookies()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/cookie/Cookie;",
            ">;"
        }
    .end annotation

    .prologue
    .line 146
    const-string v0, "Cookie count: %d."

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lorg/droidparts/net/http/CookieJar;->cookies:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v3}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lorg/droidparts/util/L;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 147
    iget-object v0, p0, Lorg/droidparts/net/http/CookieJar;->cookies:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public put(Ljava/net/URI;Ljava/util/Map;)V
    .locals 5
    .param p1, "uri"    # Ljava/net/URI;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/net/URI;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 88
    .local p2, "responseHeaders":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;"
    invoke-interface {p2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 89
    .local v3, "key":Ljava/lang/String;
    const-string v4, "Set-Cookie"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    const-string v4, "Set-Cookie2"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 91
    :cond_1
    invoke-interface {p2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    invoke-direct {p0, p1, v4}, Lorg/droidparts/net/http/CookieJar;->parseCookies(Ljava/net/URI;Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    .line 93
    .local v1, "cookies":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/cookie/Cookie;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/http/cookie/Cookie;

    .line 94
    .local v0, "c":Lorg/apache/http/cookie/Cookie;
    invoke-virtual {p0, v0}, Lorg/droidparts/net/http/CookieJar;->addCookie(Lorg/apache/http/cookie/Cookie;)V

    goto :goto_0

    .line 99
    .end local v0    # "c":Lorg/apache/http/cookie/Cookie;
    .end local v1    # "cookies":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/cookie/Cookie;>;"
    .end local v3    # "key":Ljava/lang/String;
    :cond_2
    return-void
.end method

.method public setPersistent(Z)V
    .locals 1
    .param p1, "persistent"    # Z

    .prologue
    .line 65
    iput-boolean p1, p0, Lorg/droidparts/net/http/CookieJar;->persistCookies:Z

    .line 66
    iget-boolean v0, p0, Lorg/droidparts/net/http/CookieJar;->persistCookies:Z

    if-eqz v0, :cond_0

    .line 67
    iget-object v0, p0, Lorg/droidparts/net/http/CookieJar;->cookies:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->clear()V

    .line 68
    invoke-direct {p0}, Lorg/droidparts/net/http/CookieJar;->restoreCookies()V

    .line 70
    :cond_0
    return-void
.end method
