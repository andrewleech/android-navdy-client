.class public Lorg/droidparts/inner/AnnotationElementsReader;
.super Ljava/lang/Object;
.source "AnnotationElementsReader.java"


# static fields
.field private static elementsField:Ljava/lang/reflect/Field;

.field private static nameField:Ljava/lang/reflect/Field;

.field private static validateValueMethod:Ljava/lang/reflect/Method;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getElements(Ljava/lang/annotation/Annotation;)Ljava/util/HashMap;
    .locals 14
    .param p0, "annotation"    # Ljava/lang/annotation/Annotation;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/annotation/Annotation;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v13, 0x0

    const/4 v12, 0x1

    .line 33
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    .line 34
    .local v7, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-static {p0}, Ljava/lang/reflect/Proxy;->getInvocationHandler(Ljava/lang/Object;)Ljava/lang/reflect/InvocationHandler;

    move-result-object v4

    .line 35
    .local v4, "handler":Ljava/lang/reflect/InvocationHandler;
    sget-object v10, Lorg/droidparts/inner/AnnotationElementsReader;->elementsField:Ljava/lang/reflect/Field;

    if-nez v10, :cond_0

    .line 36
    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v10

    const-string v11, "elements"

    invoke-virtual {v10, v11}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v10

    sput-object v10, Lorg/droidparts/inner/AnnotationElementsReader;->elementsField:Ljava/lang/reflect/Field;

    .line 37
    sget-object v10, Lorg/droidparts/inner/AnnotationElementsReader;->elementsField:Ljava/lang/reflect/Field;

    invoke-virtual {v10, v12}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 39
    :cond_0
    sget-object v10, Lorg/droidparts/inner/AnnotationElementsReader;->elementsField:Ljava/lang/reflect/Field;

    invoke-virtual {v10, v4}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, [Ljava/lang/Object;

    move-object v1, v10

    check-cast v1, [Ljava/lang/Object;

    .line 40
    .local v1, "annotationMembers":[Ljava/lang/Object;
    move-object v2, v1

    .local v2, "arr$":[Ljava/lang/Object;
    array-length v6, v2

    .local v6, "len$":I
    const/4 v5, 0x0

    .local v5, "i$":I
    :goto_0
    if-ge v5, v6, :cond_2

    aget-object v0, v2, v5

    .line 41
    .local v0, "annotationMember":Ljava/lang/Object;
    sget-object v10, Lorg/droidparts/inner/AnnotationElementsReader;->nameField:Ljava/lang/reflect/Field;

    if-nez v10, :cond_1

    .line 42
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    .line 43
    .local v3, "cl":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-string v10, "name"

    invoke-virtual {v3, v10}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v10

    sput-object v10, Lorg/droidparts/inner/AnnotationElementsReader;->nameField:Ljava/lang/reflect/Field;

    .line 44
    sget-object v10, Lorg/droidparts/inner/AnnotationElementsReader;->nameField:Ljava/lang/reflect/Field;

    invoke-virtual {v10, v12}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 45
    const-string v10, "validateValue"

    new-array v11, v13, [Ljava/lang/Class;

    invoke-virtual {v3, v10, v11}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v10

    sput-object v10, Lorg/droidparts/inner/AnnotationElementsReader;->validateValueMethod:Ljava/lang/reflect/Method;

    .line 46
    sget-object v10, Lorg/droidparts/inner/AnnotationElementsReader;->validateValueMethod:Ljava/lang/reflect/Method;

    invoke-virtual {v10, v12}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 48
    .end local v3    # "cl":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :cond_1
    sget-object v10, Lorg/droidparts/inner/AnnotationElementsReader;->nameField:Ljava/lang/reflect/Field;

    invoke-virtual {v10, v0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 49
    .local v8, "name":Ljava/lang/String;
    sget-object v10, Lorg/droidparts/inner/AnnotationElementsReader;->validateValueMethod:Ljava/lang/reflect/Method;

    new-array v11, v13, [Ljava/lang/Object;

    invoke-virtual {v10, v0, v11}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    .line 50
    .local v9, "val":Ljava/lang/Object;
    invoke-virtual {v7, v8, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 40
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 52
    .end local v0    # "annotationMember":Ljava/lang/Object;
    .end local v8    # "name":Ljava/lang/String;
    .end local v9    # "val":Ljava/lang/Object;
    :cond_2
    return-object v7
.end method
