.class public final Lorg/droidparts/inner/PersistUtils;
.super Ljava/lang/Object;
.source "PersistUtils.java"

# interfaces
.implements Lorg/droidparts/contract/SQL$DDL;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static appendForeignKeyDef(Lorg/droidparts/inner/ann/FieldSpec;Ljava/lang/StringBuilder;)V
    .locals 4
    .param p1, "sb"    # Ljava/lang/StringBuilder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/droidparts/inner/ann/FieldSpec",
            "<",
            "Lorg/droidparts/inner/ann/sql/ColumnAnn;",
            ">;",
            "Ljava/lang/StringBuilder;",
            ")V"
        }
    .end annotation

    .prologue
    .line 309
    .local p0, "spec":Lorg/droidparts/inner/ann/FieldSpec;, "Lorg/droidparts/inner/ann/FieldSpec<Lorg/droidparts/inner/ann/sql/ColumnAnn;>;"
    iget-object v2, p0, Lorg/droidparts/inner/ann/FieldSpec;->field:Ljava/lang/reflect/Field;

    invoke-virtual {v2}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v2

    const-class v3, Lorg/droidparts/model/Entity;

    invoke-virtual {v2, v3}, Ljava/lang/Class;->asSubclass(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v0

    .line 311
    .local v0, "entityType":Ljava/lang/Class;, "Ljava/lang/Class<+Lorg/droidparts/model/Entity;>;"
    invoke-static {v0}, Lorg/droidparts/inner/ClassSpecRegistry;->getTableName(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v1

    .line 312
    .local v1, "foreignTableName":Ljava/lang/String;
    const-string v2, "FOREIGN KEY("

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 313
    iget-object v2, p0, Lorg/droidparts/inner/ann/FieldSpec;->ann:Lorg/droidparts/inner/ann/Ann;

    check-cast v2, Lorg/droidparts/inner/ann/sql/ColumnAnn;

    iget-object v2, v2, Lorg/droidparts/inner/ann/sql/ColumnAnn;->name:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 314
    const-string v2, ") REFERENCES "

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 315
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 316
    const-string v2, "("

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_id"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") ON DELETE CASCADE"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 317
    return-void
.end method

.method public static buildPlaceholders(I)Ljava/lang/String;
    .locals 3
    .param p0, "count"    # I

    .prologue
    .line 127
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 128
    .local v1, "sb":Ljava/lang/StringBuilder;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, p0, :cond_1

    .line 129
    if-eqz v0, :cond_0

    .line 130
    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 132
    :cond_0
    const-string v2, "?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 128
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 134
    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public static executeInTransaction(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/concurrent/Callable;)Ljava/lang/Object;
    .locals 3
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<Result:",
            "Ljava/lang/Object;",
            ">(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/util/concurrent/Callable",
            "<TResult;>;)TResult;"
        }
    .end annotation

    .prologue
    .line 153
    .local p1, "task":Ljava/util/concurrent/Callable;, "Ljava/util/concurrent/Callable<TResult;>;"
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 155
    :try_start_0
    invoke-interface {p1}, Ljava/util/concurrent/Callable;->call()Ljava/lang/Object;

    move-result-object v1

    .line 156
    .local v1, "result":Ljava/lang/Object;, "TResult;"
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 163
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .end local v1    # "result":Ljava/lang/Object;, "TResult;"
    :goto_0
    return-object v1

    .line 158
    :catch_0
    move-exception v0

    .line 159
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lorg/droidparts/util/L;->w(Ljava/lang/Object;)V

    .line 160
    invoke-static {v0}, Lorg/droidparts/util/L;->d(Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 161
    const/4 v1, 0x0

    .line 163
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto :goto_0

    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v2
.end method

.method public static executeStatements(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/ArrayList;)Z
    .locals 3
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 171
    .local p1, "statements":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v1, Lorg/droidparts/inner/PersistUtils$1;

    invoke-direct {v1, p1, p0}, Lorg/droidparts/inner/PersistUtils$1;-><init>(Ljava/util/ArrayList;Landroid/database/sqlite/SQLiteDatabase;)V

    .line 182
    .local v1, "task":Ljava/util/concurrent/Callable;, "Ljava/util/concurrent/Callable<Ljava/lang/Boolean;>;"
    invoke-static {p0, v1}, Lorg/droidparts/inner/PersistUtils;->executeInTransaction(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/concurrent/Callable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 183
    .local v0, "result":Ljava/lang/Boolean;
    if-eqz v0, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static getAddColumn(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/Object;)Ljava/lang/String;
    .locals 2
    .param p0, "table"    # Ljava/lang/String;
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "type"    # Ljava/lang/String;
    .param p3, "nullable"    # Z
    .param p4, "defaultVal"    # Ljava/lang/Object;

    .prologue
    .line 239
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 240
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string v1, "ALTER TABLE "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 241
    const-string v1, " ADD COLUMN "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 242
    if-nez p3, :cond_0

    .line 243
    const-string v1, " NOT NULL"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 244
    const-string v1, " DEFAULT "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 246
    :cond_0
    const-string v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 247
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static getAddMissingColumns(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/Class;)Ljava/util/ArrayList;
    .locals 19
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/lang/Class",
            "<+",
            "Lorg/droidparts/model/Entity;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 204
    .local p1, "cls":Ljava/lang/Class;, "Ljava/lang/Class<+Lorg/droidparts/model/Entity;>;"
    invoke-static/range {p1 .. p1}, Lorg/droidparts/inner/ClassSpecRegistry;->getTableName(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v18

    .line 205
    .local v18, "tableName":Ljava/lang/String;
    invoke-static/range {p1 .. p1}, Lorg/droidparts/inner/ClassSpecRegistry;->getTableColumnSpecs(Ljava/lang/Class;)[Lorg/droidparts/inner/ann/FieldSpec;

    move-result-object v9

    .line 207
    .local v9, "columnSpecs":[Lorg/droidparts/inner/ann/FieldSpec;, "[Lorg/droidparts/inner/ann/FieldSpec<Lorg/droidparts/inner/ann/sql/ColumnAnn;>;"
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-static {v0, v1}, Lorg/droidparts/inner/PersistUtils;->getColumnNames(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v14

    .line 209
    .local v14, "presentColumns":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 210
    .local v10, "columns":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/droidparts/inner/ann/FieldSpec<Lorg/droidparts/inner/ann/sql/ColumnAnn;>;>;"
    move-object v8, v9

    .local v8, "arr$":[Lorg/droidparts/inner/ann/FieldSpec;
    array-length v13, v8

    .local v13, "len$":I
    const/4 v12, 0x0

    .local v12, "i$":I
    :goto_0
    if-ge v12, v13, :cond_1

    aget-object v15, v8, v12

    .line 211
    .local v15, "spec":Lorg/droidparts/inner/ann/FieldSpec;, "Lorg/droidparts/inner/ann/FieldSpec<Lorg/droidparts/inner/ann/sql/ColumnAnn;>;"
    iget-object v3, v15, Lorg/droidparts/inner/ann/FieldSpec;->ann:Lorg/droidparts/inner/ann/Ann;

    check-cast v3, Lorg/droidparts/inner/ann/sql/ColumnAnn;

    iget-object v3, v3, Lorg/droidparts/inner/ann/sql/ColumnAnn;->name:Ljava/lang/String;

    invoke-virtual {v14, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 212
    invoke-virtual {v10, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 210
    :cond_0
    add-int/lit8 v12, v12, 0x1

    goto :goto_0

    .line 216
    .end local v15    # "spec":Lorg/droidparts/inner/ann/FieldSpec;, "Lorg/droidparts/inner/ann/FieldSpec<Lorg/droidparts/inner/ann/sql/ColumnAnn;>;"
    :cond_1
    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    .line 217
    .local v17, "statements":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v12

    .local v12, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lorg/droidparts/inner/ann/FieldSpec;

    .line 218
    .restart local v15    # "spec":Lorg/droidparts/inner/ann/FieldSpec;, "Lorg/droidparts/inner/ann/FieldSpec<Lorg/droidparts/inner/ann/sql/ColumnAnn;>;"
    invoke-static/range {p1 .. p1}, Lorg/droidparts/inner/ReflectionUtils;->newInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lorg/droidparts/model/Entity;

    .line 219
    .local v11, "entity":Lorg/droidparts/model/Entity;
    iget-object v3, v15, Lorg/droidparts/inner/ann/FieldSpec;->field:Ljava/lang/reflect/Field;

    invoke-virtual {v3}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v3

    invoke-static {v3}, Lorg/droidparts/inner/ConverterRegistry;->getConverter(Ljava/lang/Class;)Lorg/droidparts/inner/converter/Converter;

    move-result-object v2

    .line 221
    .local v2, "converter":Lorg/droidparts/inner/converter/Converter;, "Lorg/droidparts/inner/converter/Converter<TT;>;"
    iget-object v3, v15, Lorg/droidparts/inner/ann/FieldSpec;->field:Ljava/lang/reflect/Field;

    invoke-static {v11, v3}, Lorg/droidparts/inner/ReflectionUtils;->getFieldVal(Ljava/lang/Object;Ljava/lang/reflect/Field;)Ljava/lang/Object;

    move-result-object v7

    .line 223
    .local v7, "defaultVal":Ljava/lang/Object;
    if-eqz v7, :cond_2

    .line 224
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 225
    .local v5, "cv":Landroid/content/ContentValues;
    iget-object v3, v15, Lorg/droidparts/inner/ann/FieldSpec;->field:Ljava/lang/reflect/Field;

    invoke-virtual {v3}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v3

    iget-object v4, v15, Lorg/droidparts/inner/ann/FieldSpec;->componentType:Ljava/lang/Class;

    const-string v6, " DEFAULT "

    invoke-virtual/range {v2 .. v7}, Lorg/droidparts/inner/converter/Converter;->putToContentValues(Ljava/lang/Class;Ljava/lang/Class;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/Object;)V

    .line 227
    const-string v3, " DEFAULT "

    invoke-virtual {v5, v3}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    .line 230
    .end local v5    # "cv":Landroid/content/ContentValues;
    :cond_2
    iget-object v3, v15, Lorg/droidparts/inner/ann/FieldSpec;->ann:Lorg/droidparts/inner/ann/Ann;

    check-cast v3, Lorg/droidparts/inner/ann/sql/ColumnAnn;

    iget-object v4, v3, Lorg/droidparts/inner/ann/sql/ColumnAnn;->name:Ljava/lang/String;

    invoke-virtual {v2}, Lorg/droidparts/inner/converter/Converter;->getDBColumnType()Ljava/lang/String;

    move-result-object v6

    iget-object v3, v15, Lorg/droidparts/inner/ann/FieldSpec;->ann:Lorg/droidparts/inner/ann/Ann;

    check-cast v3, Lorg/droidparts/inner/ann/sql/ColumnAnn;

    iget-boolean v3, v3, Lorg/droidparts/inner/ann/sql/ColumnAnn;->nullable:Z

    move-object/from16 v0, v18

    invoke-static {v0, v4, v6, v3, v7}, Lorg/droidparts/inner/PersistUtils;->getAddColumn(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    .line 232
    .local v16, "statement":Ljava/lang/String;
    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 234
    .end local v2    # "converter":Lorg/droidparts/inner/converter/Converter;, "Lorg/droidparts/inner/converter/Converter<TT;>;"
    .end local v7    # "defaultVal":Ljava/lang/Object;
    .end local v11    # "entity":Lorg/droidparts/model/Entity;
    .end local v15    # "spec":Lorg/droidparts/inner/ann/FieldSpec;, "Lorg/droidparts/inner/ann/FieldSpec<Lorg/droidparts/inner/ann/sql/ColumnAnn;>;"
    .end local v16    # "statement":Ljava/lang/String;
    :cond_3
    return-object v17
.end method

.method public static getColumnNames(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 2
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "table"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 272
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PRAGMA table_info("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Lorg/droidparts/inner/PersistUtils;->readStrings(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;I)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public static varargs getCreateIndex(Ljava/lang/String;ZLjava/lang/String;[Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "table"    # Ljava/lang/String;
    .param p1, "unique"    # Z
    .param p2, "firstColumn"    # Ljava/lang/String;
    .param p3, "otherColumns"    # [Ljava/lang/String;

    .prologue
    .line 252
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 253
    .local v0, "columns":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 254
    invoke-static {p3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 255
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 256
    .local v1, "sb":Ljava/lang/StringBuilder;
    if-eqz p1, :cond_0

    const-string v2, "CREATE UNIQUE INDEX "

    :goto_0
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 257
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "idx_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_"

    invoke-static {v0, v3}, Lorg/droidparts/util/Strings;->join(Ljava/util/Collection;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 258
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " ON "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 259
    const-string v2, " ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 260
    const-string v2, ", "

    invoke-static {v0, v2}, Lorg/droidparts/util/Strings;->join(Ljava/util/Collection;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 261
    const-string v2, ");"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 262
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 256
    :cond_0
    const-string v2, "CREATE INDEX "

    goto :goto_0
.end method

.method public static varargs getDropTables(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 6
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "optionalTableNames"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "[",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 188
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 189
    .local v3, "tableNames":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    array-length v4, p1

    if-nez v4, :cond_0

    .line 190
    invoke-static {p0}, Lorg/droidparts/inner/PersistUtils;->getTableNames(Landroid/database/sqlite/SQLiteDatabase;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    .line 194
    :goto_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 195
    .local v1, "statements":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v3}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 196
    .local v2, "tableName":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "DROP TABLE IF EXISTS "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ";"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 192
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "statements":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v2    # "tableName":Ljava/lang/String;
    :cond_0
    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 198
    .restart local v0    # "i$":Ljava/util/Iterator;
    .restart local v1    # "statements":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_1
    return-object v1
.end method

.method public static getNodeText(Lorg/w3c/dom/Node;)Ljava/lang/String;
    .locals 1
    .param p0, "n"    # Lorg/w3c/dom/Node;

    .prologue
    .line 59
    invoke-interface {p0}, Lorg/w3c/dom/Node;->getTextContent()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getRowCount(Landroid/database/sqlite/SQLiteDatabase;ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .locals 13
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "distinct"    # Z
    .param p2, "table"    # Ljava/lang/String;
    .param p3, "columns"    # [Ljava/lang/String;
    .param p4, "selection"    # Ljava/lang/String;
    .param p5, "selectionArgs"    # [Ljava/lang/String;
    .param p6, "groupBy"    # Ljava/lang/String;
    .param p7, "having"    # Ljava/lang/String;
    .param p8, "orderBy"    # Ljava/lang/String;
    .param p9, "limit"    # Ljava/lang/String;

    .prologue
    .line 141
    if-eqz p3, :cond_0

    move-object/from16 v0, p3

    array-length v2, v0

    if-lez v2, :cond_0

    .line 142
    const/4 v2, 0x1

    new-array v10, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const/4 v3, 0x0

    aget-object v3, p3, v3

    aput-object v3, v10, v2

    .end local p3    # "columns":[Ljava/lang/String;
    .local v10, "columns":[Ljava/lang/String;
    move-object/from16 p3, v10

    .end local v10    # "columns":[Ljava/lang/String;
    .restart local p3    # "columns":[Ljava/lang/String;
    :cond_0
    move v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    .line 144
    invoke-static/range {v2 .. v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildQueryString(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 146
    .local v12, "sql":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SELECT count(*) FROM ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 147
    .local v11, "countSelection":Ljava/lang/String;
    move-object/from16 v0, p5

    invoke-static {p0, v11, v0}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v2

    long-to-int v2, v2

    return v2
.end method

.method public static getSQLCreate(Ljava/lang/String;[Lorg/droidparts/inner/ann/FieldSpec;)Ljava/lang/String;
    .locals 9
    .param p0, "tableName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "[",
            "Lorg/droidparts/inner/ann/FieldSpec",
            "<",
            "Lorg/droidparts/inner/ann/sql/ColumnAnn;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 277
    .local p1, "specs":[Lorg/droidparts/inner/ann/FieldSpec;, "[Lorg/droidparts/inner/ann/FieldSpec<Lorg/droidparts/inner/ann/sql/ColumnAnn;>;"
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 278
    .local v5, "sb":Ljava/lang/StringBuilder;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "CREATE TABLE "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " ("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 279
    const-string v7, "_id INTEGER PRIMARY KEY"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 280
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 281
    .local v2, "fkSb":Ljava/lang/StringBuilder;
    move-object v0, p1

    .local v0, "arr$":[Lorg/droidparts/inner/ann/FieldSpec;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_4

    aget-object v6, v0, v3

    .line 282
    .local v6, "spec":Lorg/droidparts/inner/ann/FieldSpec;, "Lorg/droidparts/inner/ann/FieldSpec<Lorg/droidparts/inner/ann/sql/ColumnAnn;>;"
    const-string v8, "_id"

    iget-object v7, v6, Lorg/droidparts/inner/ann/FieldSpec;->ann:Lorg/droidparts/inner/ann/Ann;

    check-cast v7, Lorg/droidparts/inner/ann/sql/ColumnAnn;

    iget-object v7, v7, Lorg/droidparts/inner/ann/sql/ColumnAnn;->name:Ljava/lang/String;

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 281
    :cond_0
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 286
    :cond_1
    const-string v7, ", "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 287
    iget-object v7, v6, Lorg/droidparts/inner/ann/FieldSpec;->ann:Lorg/droidparts/inner/ann/Ann;

    check-cast v7, Lorg/droidparts/inner/ann/sql/ColumnAnn;

    iget-object v7, v7, Lorg/droidparts/inner/ann/sql/ColumnAnn;->name:Ljava/lang/String;

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 288
    iget-object v7, v6, Lorg/droidparts/inner/ann/FieldSpec;->field:Ljava/lang/reflect/Field;

    invoke-virtual {v7}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v7

    invoke-static {v7}, Lorg/droidparts/inner/ConverterRegistry;->getConverter(Ljava/lang/Class;)Lorg/droidparts/inner/converter/Converter;

    move-result-object v1

    .line 290
    .local v1, "converter":Lorg/droidparts/inner/converter/Converter;, "Lorg/droidparts/inner/converter/Converter<*>;"
    invoke-virtual {v1}, Lorg/droidparts/inner/converter/Converter;->getDBColumnType()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 291
    iget-object v7, v6, Lorg/droidparts/inner/ann/FieldSpec;->ann:Lorg/droidparts/inner/ann/Ann;

    check-cast v7, Lorg/droidparts/inner/ann/sql/ColumnAnn;

    iget-boolean v7, v7, Lorg/droidparts/inner/ann/sql/ColumnAnn;->nullable:Z

    if-nez v7, :cond_2

    .line 292
    const-string v7, " NOT NULL"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 294
    :cond_2
    iget-object v7, v6, Lorg/droidparts/inner/ann/FieldSpec;->ann:Lorg/droidparts/inner/ann/Ann;

    check-cast v7, Lorg/droidparts/inner/ann/sql/ColumnAnn;

    iget-boolean v7, v7, Lorg/droidparts/inner/ann/sql/ColumnAnn;->unique:Z

    if-eqz v7, :cond_3

    .line 295
    const-string v7, " UNIQUE"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 297
    :cond_3
    iget-object v7, v6, Lorg/droidparts/inner/ann/FieldSpec;->field:Ljava/lang/reflect/Field;

    invoke-virtual {v7}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v7

    invoke-static {v7}, Lorg/droidparts/inner/TypeHelper;->isEntity(Ljava/lang/Class;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 298
    const-string v7, ", "

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 299
    invoke-static {v6, v2}, Lorg/droidparts/inner/PersistUtils;->appendForeignKeyDef(Lorg/droidparts/inner/ann/FieldSpec;Ljava/lang/StringBuilder;)V

    goto :goto_1

    .line 302
    .end local v1    # "converter":Lorg/droidparts/inner/converter/Converter;, "Lorg/droidparts/inner/converter/Converter<*>;"
    .end local v6    # "spec":Lorg/droidparts/inner/ann/FieldSpec;, "Lorg/droidparts/inner/ann/FieldSpec<Lorg/droidparts/inner/ann/sql/ColumnAnn;>;"
    :cond_4
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 303
    const-string v7, ");"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 304
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    return-object v7
.end method

.method public static getTableNames(Landroid/database/sqlite/SQLiteDatabase;)Ljava/util/ArrayList;
    .locals 2
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 266
    const-string v0, "SELECT name FROM sqlite_master WHERE type=\'table\'"

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lorg/droidparts/inner/PersistUtils;->readStrings(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;I)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public static hasNonNull(Lorg/json/JSONObject;Ljava/lang/String;)Z
    .locals 2
    .param p0, "obj"    # Lorg/json/JSONObject;
    .param p1, "key"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 53
    invoke-virtual {p0, p1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lorg/json/JSONObject;->NULL:Ljava/lang/Object;

    invoke-virtual {p0, p1}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static readAll(Lorg/droidparts/persist/sql/AbstractEntityManager;Landroid/database/Cursor;)Ljava/util/ArrayList;
    .locals 2
    .param p1, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<EntityType:",
            "Lorg/droidparts/model/Entity;",
            ">(",
            "Lorg/droidparts/persist/sql/AbstractEntityManager",
            "<TEntityType;>;",
            "Landroid/database/Cursor;",
            ")",
            "Ljava/util/ArrayList",
            "<TEntityType;>;"
        }
    .end annotation

    .prologue
    .line 92
    .local p0, "entityManager":Lorg/droidparts/persist/sql/AbstractEntityManager;, "Lorg/droidparts/persist/sql/AbstractEntityManager<TEntityType;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 94
    .local v0, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<TEntityType;>;"
    :goto_0
    :try_start_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 95
    invoke-virtual {p0, p1}, Lorg/droidparts/persist/sql/AbstractEntityManager;->readRow(Landroid/database/Cursor;)Lorg/droidparts/model/Entity;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 98
    :catchall_0
    move-exception v1

    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    throw v1

    :cond_0
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    .line 100
    return-object v0
.end method

.method public static readFirst(Lorg/droidparts/persist/sql/AbstractEntityManager;Landroid/database/Cursor;)Lorg/droidparts/model/Entity;
    .locals 2
    .param p1, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<EntityType:",
            "Lorg/droidparts/model/Entity;",
            ">(",
            "Lorg/droidparts/persist/sql/AbstractEntityManager",
            "<TEntityType;>;",
            "Landroid/database/Cursor;",
            ")TEntityType;"
        }
    .end annotation

    .prologue
    .line 79
    .local p0, "entityManager":Lorg/droidparts/persist/sql/AbstractEntityManager;, "Lorg/droidparts/persist/sql/AbstractEntityManager<TEntityType;>;"
    const/4 v0, 0x0

    .line 81
    .local v0, "item":Lorg/droidparts/model/Entity;, "TEntityType;"
    :try_start_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 82
    invoke-virtual {p0, p1}, Lorg/droidparts/persist/sql/AbstractEntityManager;->readRow(Landroid/database/Cursor;)Lorg/droidparts/model/Entity;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 85
    :cond_0
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    .line 87
    return-object v0

    .line 85
    :catchall_0
    move-exception v1

    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    throw v1
.end method

.method public static readIds(Landroid/database/Cursor;)[J
    .locals 6
    .param p0, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 65
    invoke-interface {p0}, Landroid/database/Cursor;->getCount()I

    move-result v3

    new-array v0, v3, [J

    .line 66
    .local v0, "arr":[J
    const/4 v1, 0x0

    .local v1, "count":I
    move v2, v1

    .line 68
    .end local v1    # "count":I
    .local v2, "count":I
    :goto_0
    :try_start_0
    invoke-interface {p0}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-eqz v3, :cond_0

    .line 69
    add-int/lit8 v1, v2, 0x1

    .end local v2    # "count":I
    .restart local v1    # "count":I
    const/4 v3, 0x0

    :try_start_1
    invoke-interface {p0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    aput-wide v4, v0, v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move v2, v1

    .end local v1    # "count":I
    .restart local v2    # "count":I
    goto :goto_0

    .line 72
    :cond_0
    invoke-interface {p0}, Landroid/database/Cursor;->close()V

    .line 74
    return-object v0

    .line 72
    :catchall_0
    move-exception v3

    move v1, v2

    .end local v2    # "count":I
    .restart local v1    # "count":I
    :goto_1
    invoke-interface {p0}, Landroid/database/Cursor;->close()V

    throw v3

    :catchall_1
    move-exception v3

    goto :goto_1
.end method

.method private static readStrings(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;I)Ljava/util/ArrayList;
    .locals 3
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "query"    # Ljava/lang/String;
    .param p2, "colIdx"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 321
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 322
    .local v1, "rows":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v2, 0x0

    invoke-virtual {p0, p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 323
    .local v0, "c":Landroid/database/Cursor;
    :goto_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 324
    invoke-interface {v0, p2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 326
    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 327
    return-object v1
.end method

.method public static varargs toWhereArgs([Ljava/lang/Object;)[Ljava/lang/String;
    .locals 6
    .param p0, "args"    # [Ljava/lang/Object;

    .prologue
    .line 106
    array-length v4, p0

    new-array v2, v4, [Ljava/lang/String;

    .line 107
    .local v2, "arr":[Ljava/lang/String;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    array-length v4, p0

    if-ge v3, v4, :cond_5

    .line 108
    aget-object v0, p0, v3

    .line 110
    .local v0, "arg":Ljava/lang/Object;
    if-nez v0, :cond_0

    .line 111
    const-string v1, "NULL"

    .line 121
    .end local v0    # "arg":Ljava/lang/Object;
    .local v1, "argStr":Ljava/lang/String;
    :goto_1
    aput-object v1, v2, v3

    .line 107
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 112
    .end local v1    # "argStr":Ljava/lang/String;
    .restart local v0    # "arg":Ljava/lang/Object;
    :cond_0
    instance-of v4, v0, Ljava/lang/Boolean;

    if-eqz v4, :cond_2

    .line 113
    check-cast v0, Ljava/lang/Boolean;

    .end local v0    # "arg":Ljava/lang/Object;
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_1

    const-string v1, "1"

    .restart local v1    # "argStr":Ljava/lang/String;
    :goto_2
    goto :goto_1

    .end local v1    # "argStr":Ljava/lang/String;
    :cond_1
    const-string v1, "0"

    goto :goto_2

    .line 114
    .restart local v0    # "arg":Ljava/lang/Object;
    :cond_2
    instance-of v4, v0, Ljava/util/Date;

    if-eqz v4, :cond_3

    .line 115
    check-cast v0, Ljava/util/Date;

    .end local v0    # "arg":Ljava/lang/Object;
    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    .restart local v1    # "argStr":Ljava/lang/String;
    goto :goto_1

    .line 116
    .end local v1    # "argStr":Ljava/lang/String;
    .restart local v0    # "arg":Ljava/lang/Object;
    :cond_3
    instance-of v4, v0, Lorg/droidparts/model/Entity;

    if-eqz v4, :cond_4

    .line 117
    check-cast v0, Lorg/droidparts/model/Entity;

    .end local v0    # "arg":Ljava/lang/Object;
    iget-wide v4, v0, Lorg/droidparts/model/Entity;->id:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    .restart local v1    # "argStr":Ljava/lang/String;
    goto :goto_1

    .line 119
    .end local v1    # "argStr":Ljava/lang/String;
    .restart local v0    # "arg":Ljava/lang/Object;
    :cond_4
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .restart local v1    # "argStr":Ljava/lang/String;
    goto :goto_1

    .line 123
    .end local v0    # "arg":Ljava/lang/Object;
    .end local v1    # "argStr":Ljava/lang/String;
    :cond_5
    return-object v2
.end method
