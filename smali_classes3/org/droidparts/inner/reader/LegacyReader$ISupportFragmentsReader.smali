.class interface abstract Lorg/droidparts/inner/reader/LegacyReader$ISupportFragmentsReader;
.super Ljava/lang/Object;
.source "LegacyReader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/droidparts/inner/reader/LegacyReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x608
    name = "ISupportFragmentsReader"
.end annotation


# virtual methods
.method public abstract getFragment(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;
.end method

.method public abstract getFragmentArguments(Ljava/lang/Object;)Landroid/os/Bundle;
.end method

.method public abstract getParentActivity(Ljava/lang/Object;)Landroid/app/Activity;
.end method

.method public abstract isSupportObject(Ljava/lang/Object;)Z
.end method
