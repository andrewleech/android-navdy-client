.class public Lnet/minidev/json/JSONValue;
.super Ljava/lang/Object;
.source "JSONValue.java"


# static fields
.field public static COMPRESSION:Lnet/minidev/json/JSONStyle;

.field private static final FACTORY_FAKE_COINTAINER:Lnet/minidev/json/parser/FakeContainerFactory;

.field public static defaultWriter:Lnet/minidev/json/reader/JsonWriter;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 51
    sget-object v0, Lnet/minidev/json/JSONStyle;->NO_COMPRESS:Lnet/minidev/json/JSONStyle;

    sput-object v0, Lnet/minidev/json/JSONValue;->COMPRESSION:Lnet/minidev/json/JSONStyle;

    .line 56
    new-instance v0, Lnet/minidev/json/parser/FakeContainerFactory;

    invoke-direct {v0}, Lnet/minidev/json/parser/FakeContainerFactory;-><init>()V

    sput-object v0, Lnet/minidev/json/JSONValue;->FACTORY_FAKE_COINTAINER:Lnet/minidev/json/parser/FakeContainerFactory;

    .line 494
    new-instance v0, Lnet/minidev/json/reader/JsonWriter;

    invoke-direct {v0}, Lnet/minidev/json/reader/JsonWriter;-><init>()V

    sput-object v0, Lnet/minidev/json/JSONValue;->defaultWriter:Lnet/minidev/json/reader/JsonWriter;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static SAXParse(Ljava/io/InputStream;Lnet/minidev/json/parser/ContentHandler;)V
    .locals 2
    .param p0, "input"    # Ljava/io/InputStream;
    .param p1, "handler"    # Lnet/minidev/json/parser/ContentHandler;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lnet/minidev/json/parser/ParseException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 235
    new-instance v0, Lnet/minidev/json/parser/JSONParser;

    sget v1, Lnet/minidev/json/parser/JSONParser;->DEFAULT_PERMISSIVE_MODE:I

    invoke-direct {v0, v1}, Lnet/minidev/json/parser/JSONParser;-><init>(I)V

    .line 236
    .local v0, "p":Lnet/minidev/json/parser/JSONParser;
    sget-object v1, Lnet/minidev/json/JSONValue;->FACTORY_FAKE_COINTAINER:Lnet/minidev/json/parser/FakeContainerFactory;

    invoke-virtual {v0, p0, v1, p1}, Lnet/minidev/json/parser/JSONParser;->parse(Ljava/io/InputStream;Lnet/minidev/json/parser/ContainerFactory;Lnet/minidev/json/parser/ContentHandler;)Ljava/lang/Object;

    .line 237
    return-void
.end method

.method public static SAXParse(Ljava/io/Reader;Lnet/minidev/json/parser/ContentHandler;)V
    .locals 2
    .param p0, "input"    # Ljava/io/Reader;
    .param p1, "handler"    # Lnet/minidev/json/parser/ContentHandler;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lnet/minidev/json/parser/ParseException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 245
    new-instance v0, Lnet/minidev/json/parser/JSONParser;

    sget v1, Lnet/minidev/json/parser/JSONParser;->DEFAULT_PERMISSIVE_MODE:I

    invoke-direct {v0, v1}, Lnet/minidev/json/parser/JSONParser;-><init>(I)V

    .line 246
    .local v0, "p":Lnet/minidev/json/parser/JSONParser;
    sget-object v1, Lnet/minidev/json/JSONValue;->FACTORY_FAKE_COINTAINER:Lnet/minidev/json/parser/FakeContainerFactory;

    invoke-virtual {v0, p0, v1, p1}, Lnet/minidev/json/parser/JSONParser;->parse(Ljava/io/Reader;Lnet/minidev/json/parser/ContainerFactory;Lnet/minidev/json/parser/ContentHandler;)Ljava/lang/Object;

    .line 247
    return-void
.end method

.method public static SAXParse(Ljava/lang/String;Lnet/minidev/json/parser/ContentHandler;)V
    .locals 2
    .param p0, "input"    # Ljava/lang/String;
    .param p1, "handler"    # Lnet/minidev/json/parser/ContentHandler;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lnet/minidev/json/parser/ParseException;
        }
    .end annotation

    .prologue
    .line 255
    new-instance v0, Lnet/minidev/json/parser/JSONParser;

    sget v1, Lnet/minidev/json/parser/JSONParser;->DEFAULT_PERMISSIVE_MODE:I

    invoke-direct {v0, v1}, Lnet/minidev/json/parser/JSONParser;-><init>(I)V

    .line 256
    .local v0, "p":Lnet/minidev/json/parser/JSONParser;
    sget-object v1, Lnet/minidev/json/JSONValue;->FACTORY_FAKE_COINTAINER:Lnet/minidev/json/parser/FakeContainerFactory;

    invoke-virtual {v0, p0, v1, p1}, Lnet/minidev/json/parser/JSONParser;->parse(Ljava/lang/String;Lnet/minidev/json/parser/ContainerFactory;Lnet/minidev/json/parser/ContentHandler;)Ljava/lang/Object;

    .line 257
    return-void
.end method

.method public static compress(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    .line 282
    sget-object v0, Lnet/minidev/json/JSONStyle;->MAX_COMPRESS:Lnet/minidev/json/JSONStyle;

    invoke-static {p0, v0}, Lnet/minidev/json/JSONValue;->compress(Ljava/lang/String;Lnet/minidev/json/JSONStyle;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static compress(Ljava/lang/String;Lnet/minidev/json/JSONStyle;)Ljava/lang/String;
    .locals 5
    .param p0, "input"    # Ljava/lang/String;
    .param p1, "style"    # Lnet/minidev/json/JSONStyle;

    .prologue
    .line 266
    :try_start_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 267
    .local v3, "sb":Ljava/lang/StringBuilder;
    new-instance v0, Lnet/minidev/json/parser/ContentHandlerCompressor;

    invoke-direct {v0, v3, p1}, Lnet/minidev/json/parser/ContentHandlerCompressor;-><init>(Ljava/lang/Appendable;Lnet/minidev/json/JSONStyle;)V

    .line 268
    .local v0, "comp":Lnet/minidev/json/parser/ContentHandlerCompressor;
    new-instance v2, Lnet/minidev/json/parser/JSONParser;

    sget v4, Lnet/minidev/json/parser/JSONParser;->DEFAULT_PERMISSIVE_MODE:I

    invoke-direct {v2, v4}, Lnet/minidev/json/parser/JSONParser;-><init>(I)V

    .line 269
    .local v2, "p":Lnet/minidev/json/parser/JSONParser;
    sget-object v4, Lnet/minidev/json/JSONValue;->FACTORY_FAKE_COINTAINER:Lnet/minidev/json/parser/FakeContainerFactory;

    invoke-virtual {v2, p0, v4, v0}, Lnet/minidev/json/parser/JSONParser;->parse(Ljava/lang/String;Lnet/minidev/json/parser/ContainerFactory;Lnet/minidev/json/parser/ContentHandler;)Ljava/lang/Object;

    .line 270
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p0

    .line 272
    .end local v0    # "comp":Lnet/minidev/json/parser/ContentHandlerCompressor;
    .end local v2    # "p":Lnet/minidev/json/parser/JSONParser;
    .end local v3    # "sb":Ljava/lang/StringBuilder;
    .end local p0    # "input":Ljava/lang/String;
    :goto_0
    return-object p0

    .line 271
    .restart local p0    # "input":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 272
    .local v1, "e":Ljava/lang/Exception;
    goto :goto_0
.end method

.method public static escape(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    .line 571
    sget-object v0, Lnet/minidev/json/JSONValue;->COMPRESSION:Lnet/minidev/json/JSONStyle;

    invoke-static {p0, v0}, Lnet/minidev/json/JSONValue;->escape(Ljava/lang/String;Lnet/minidev/json/JSONStyle;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static escape(Ljava/lang/String;Lnet/minidev/json/JSONStyle;)Ljava/lang/String;
    .locals 2
    .param p0, "s"    # Ljava/lang/String;
    .param p1, "compression"    # Lnet/minidev/json/JSONStyle;

    .prologue
    .line 579
    if-nez p0, :cond_0

    .line 580
    const/4 v1, 0x0

    .line 583
    :goto_0
    return-object v1

    .line 581
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 582
    .local v0, "sb":Ljava/lang/StringBuilder;
    invoke-virtual {p1, p0, v0}, Lnet/minidev/json/JSONStyle;->escape(Ljava/lang/String;Ljava/lang/Appendable;)V

    .line 583
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public static escape(Ljava/lang/String;Ljava/lang/Appendable;)V
    .locals 1
    .param p0, "s"    # Ljava/lang/String;
    .param p1, "ap"    # Ljava/lang/Appendable;

    .prologue
    .line 587
    sget-object v0, Lnet/minidev/json/JSONValue;->COMPRESSION:Lnet/minidev/json/JSONStyle;

    invoke-static {p0, p1, v0}, Lnet/minidev/json/JSONValue;->escape(Ljava/lang/String;Ljava/lang/Appendable;Lnet/minidev/json/JSONStyle;)V

    .line 588
    return-void
.end method

.method public static escape(Ljava/lang/String;Ljava/lang/Appendable;Lnet/minidev/json/JSONStyle;)V
    .locals 0
    .param p0, "s"    # Ljava/lang/String;
    .param p1, "ap"    # Ljava/lang/Appendable;
    .param p2, "compression"    # Lnet/minidev/json/JSONStyle;

    .prologue
    .line 591
    if-nez p0, :cond_0

    .line 594
    :goto_0
    return-void

    .line 593
    :cond_0
    invoke-virtual {p2, p0, p1}, Lnet/minidev/json/JSONStyle;->escape(Ljava/lang/String;Ljava/lang/Appendable;)V

    goto :goto_0
.end method

.method public static isValidJson(Ljava/io/Reader;)Z
    .locals 3
    .param p0, "in"    # Ljava/io/Reader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 459
    :try_start_0
    new-instance v1, Lnet/minidev/json/parser/JSONParser;

    sget v2, Lnet/minidev/json/parser/JSONParser;->DEFAULT_PERMISSIVE_MODE:I

    invoke-direct {v1, v2}, Lnet/minidev/json/parser/JSONParser;-><init>(I)V

    sget-object v2, Lnet/minidev/json/JSONValue;->FACTORY_FAKE_COINTAINER:Lnet/minidev/json/parser/FakeContainerFactory;

    invoke-virtual {v1, p0, v2}, Lnet/minidev/json/parser/JSONParser;->parse(Ljava/io/Reader;Lnet/minidev/json/parser/ContainerFactory;)Ljava/lang/Object;
    :try_end_0
    .catch Lnet/minidev/json/parser/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    .line 460
    const/4 v1, 0x1

    .line 462
    :goto_0
    return v1

    .line 461
    :catch_0
    move-exception v0

    .line 462
    .local v0, "e":Lnet/minidev/json/parser/ParseException;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isValidJson(Ljava/lang/String;)Z
    .locals 3
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    .line 473
    :try_start_0
    new-instance v1, Lnet/minidev/json/parser/JSONParser;

    sget v2, Lnet/minidev/json/parser/JSONParser;->DEFAULT_PERMISSIVE_MODE:I

    invoke-direct {v1, v2}, Lnet/minidev/json/parser/JSONParser;-><init>(I)V

    sget-object v2, Lnet/minidev/json/JSONValue;->FACTORY_FAKE_COINTAINER:Lnet/minidev/json/parser/FakeContainerFactory;

    invoke-virtual {v1, p0, v2}, Lnet/minidev/json/parser/JSONParser;->parse(Ljava/lang/String;Lnet/minidev/json/parser/ContainerFactory;)Ljava/lang/Object;
    :try_end_0
    .catch Lnet/minidev/json/parser/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    .line 474
    const/4 v1, 0x1

    .line 476
    :goto_0
    return v1

    .line 475
    :catch_0
    move-exception v0

    .line 476
    .local v0, "e":Lnet/minidev/json/parser/ParseException;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isValidJsonStrict(Ljava/io/Reader;)Z
    .locals 3
    .param p0, "in"    # Ljava/io/Reader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 431
    :try_start_0
    new-instance v1, Lnet/minidev/json/parser/JSONParser;

    const/16 v2, 0x190

    invoke-direct {v1, v2}, Lnet/minidev/json/parser/JSONParser;-><init>(I)V

    sget-object v2, Lnet/minidev/json/JSONValue;->FACTORY_FAKE_COINTAINER:Lnet/minidev/json/parser/FakeContainerFactory;

    invoke-virtual {v1, p0, v2}, Lnet/minidev/json/parser/JSONParser;->parse(Ljava/io/Reader;Lnet/minidev/json/parser/ContainerFactory;)Ljava/lang/Object;
    :try_end_0
    .catch Lnet/minidev/json/parser/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    .line 432
    const/4 v1, 0x1

    .line 434
    :goto_0
    return v1

    .line 433
    :catch_0
    move-exception v0

    .line 434
    .local v0, "e":Lnet/minidev/json/parser/ParseException;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isValidJsonStrict(Ljava/lang/String;)Z
    .locals 3
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    .line 445
    :try_start_0
    new-instance v1, Lnet/minidev/json/parser/JSONParser;

    const/16 v2, 0x190

    invoke-direct {v1, v2}, Lnet/minidev/json/parser/JSONParser;-><init>(I)V

    sget-object v2, Lnet/minidev/json/JSONValue;->FACTORY_FAKE_COINTAINER:Lnet/minidev/json/parser/FakeContainerFactory;

    invoke-virtual {v1, p0, v2}, Lnet/minidev/json/parser/JSONParser;->parse(Ljava/lang/String;Lnet/minidev/json/parser/ContainerFactory;)Ljava/lang/Object;
    :try_end_0
    .catch Lnet/minidev/json/parser/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    .line 446
    const/4 v1, 0x1

    .line 448
    :goto_0
    return v1

    .line 447
    :catch_0
    move-exception v0

    .line 448
    .local v0, "e":Lnet/minidev/json/parser/ParseException;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static parse(Ljava/io/InputStream;)Ljava/lang/Object;
    .locals 3
    .param p0, "in"    # Ljava/io/InputStream;

    .prologue
    .line 118
    :try_start_0
    new-instance v1, Lnet/minidev/json/parser/JSONParser;

    sget v2, Lnet/minidev/json/parser/JSONParser;->DEFAULT_PERMISSIVE_MODE:I

    invoke-direct {v1, v2}, Lnet/minidev/json/parser/JSONParser;-><init>(I)V

    invoke-virtual {v1, p0}, Lnet/minidev/json/parser/JSONParser;->parse(Ljava/io/InputStream;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 120
    :goto_0
    return-object v1

    .line 119
    :catch_0
    move-exception v0

    .line 120
    .local v0, "e":Ljava/lang/Exception;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static parse(Ljava/io/Reader;)Ljava/lang/Object;
    .locals 3
    .param p0, "in"    # Ljava/io/Reader;

    .prologue
    .line 138
    :try_start_0
    new-instance v1, Lnet/minidev/json/parser/JSONParser;

    sget v2, Lnet/minidev/json/parser/JSONParser;->DEFAULT_PERMISSIVE_MODE:I

    invoke-direct {v1, v2}, Lnet/minidev/json/parser/JSONParser;-><init>(I)V

    invoke-virtual {v1, p0}, Lnet/minidev/json/parser/JSONParser;->parse(Ljava/io/Reader;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 140
    :goto_0
    return-object v1

    .line 139
    :catch_0
    move-exception v0

    .line 140
    .local v0, "e":Ljava/lang/Exception;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static parse(Ljava/lang/String;)Ljava/lang/Object;
    .locals 3
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    .line 158
    :try_start_0
    new-instance v1, Lnet/minidev/json/parser/JSONParser;

    sget v2, Lnet/minidev/json/parser/JSONParser;->DEFAULT_PERMISSIVE_MODE:I

    invoke-direct {v1, v2}, Lnet/minidev/json/parser/JSONParser;-><init>(I)V

    invoke-virtual {v1, p0}, Lnet/minidev/json/parser/JSONParser;->parse(Ljava/lang/String;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 160
    :goto_0
    return-object v1

    .line 159
    :catch_0
    move-exception v0

    .line 160
    .local v0, "e":Ljava/lang/Exception;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static parse([B)Ljava/lang/Object;
    .locals 3
    .param p0, "in"    # [B

    .prologue
    .line 74
    :try_start_0
    new-instance v1, Lnet/minidev/json/parser/JSONParser;

    sget v2, Lnet/minidev/json/parser/JSONParser;->DEFAULT_PERMISSIVE_MODE:I

    invoke-direct {v1, v2}, Lnet/minidev/json/parser/JSONParser;-><init>(I)V

    invoke-virtual {v1, p0}, Lnet/minidev/json/parser/JSONParser;->parse([B)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 76
    :goto_0
    return-object v1

    .line 75
    :catch_0
    move-exception v0

    .line 76
    .local v0, "e":Ljava/lang/Exception;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static parse([BII)Ljava/lang/Object;
    .locals 3
    .param p0, "in"    # [B
    .param p1, "offset"    # I
    .param p2, "length"    # I

    .prologue
    .line 96
    :try_start_0
    new-instance v1, Lnet/minidev/json/parser/JSONParser;

    sget v2, Lnet/minidev/json/parser/JSONParser;->DEFAULT_PERMISSIVE_MODE:I

    invoke-direct {v1, v2}, Lnet/minidev/json/parser/JSONParser;-><init>(I)V

    invoke-virtual {v1, p0, p1, p2}, Lnet/minidev/json/parser/JSONParser;->parse([BII)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 98
    :goto_0
    return-object v1

    .line 97
    :catch_0
    move-exception v0

    .line 98
    .local v0, "e":Ljava/lang/Exception;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static parseKeepingOrder(Ljava/io/InputStream;)Ljava/lang/Object;
    .locals 3
    .param p0, "in"    # Ljava/io/InputStream;

    .prologue
    .line 197
    :try_start_0
    new-instance v1, Lnet/minidev/json/parser/JSONParser;

    sget v2, Lnet/minidev/json/parser/JSONParser;->DEFAULT_PERMISSIVE_MODE:I

    invoke-direct {v1, v2}, Lnet/minidev/json/parser/JSONParser;-><init>(I)V

    sget-object v2, Lnet/minidev/json/parser/ContainerFactory;->FACTORY_ORDERED:Lnet/minidev/json/parser/ContainerFactory;

    invoke-virtual {v1, p0, v2}, Lnet/minidev/json/parser/JSONParser;->parse(Ljava/io/InputStream;Lnet/minidev/json/parser/ContainerFactory;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 199
    :goto_0
    return-object v1

    .line 198
    :catch_0
    move-exception v0

    .line 199
    .local v0, "e":Ljava/lang/Exception;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static parseKeepingOrder(Ljava/io/Reader;)Ljava/lang/Object;
    .locals 3
    .param p0, "in"    # Ljava/io/Reader;

    .prologue
    .line 210
    :try_start_0
    new-instance v1, Lnet/minidev/json/parser/JSONParser;

    sget v2, Lnet/minidev/json/parser/JSONParser;->DEFAULT_PERMISSIVE_MODE:I

    invoke-direct {v1, v2}, Lnet/minidev/json/parser/JSONParser;-><init>(I)V

    sget-object v2, Lnet/minidev/json/parser/ContainerFactory;->FACTORY_ORDERED:Lnet/minidev/json/parser/ContainerFactory;

    invoke-virtual {v1, p0, v2}, Lnet/minidev/json/parser/JSONParser;->parse(Ljava/io/Reader;Lnet/minidev/json/parser/ContainerFactory;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 212
    :goto_0
    return-object v1

    .line 211
    :catch_0
    move-exception v0

    .line 212
    .local v0, "e":Ljava/lang/Exception;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static parseKeepingOrder(Ljava/lang/String;)Ljava/lang/Object;
    .locals 3
    .param p0, "in"    # Ljava/lang/String;

    .prologue
    .line 223
    :try_start_0
    new-instance v1, Lnet/minidev/json/parser/JSONParser;

    sget v2, Lnet/minidev/json/parser/JSONParser;->DEFAULT_PERMISSIVE_MODE:I

    invoke-direct {v1, v2}, Lnet/minidev/json/parser/JSONParser;-><init>(I)V

    sget-object v2, Lnet/minidev/json/parser/ContainerFactory;->FACTORY_ORDERED:Lnet/minidev/json/parser/ContainerFactory;

    invoke-virtual {v1, p0, v2}, Lnet/minidev/json/parser/JSONParser;->parse(Ljava/lang/String;Lnet/minidev/json/parser/ContainerFactory;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 225
    :goto_0
    return-object v1

    .line 224
    :catch_0
    move-exception v0

    .line 225
    .local v0, "e":Ljava/lang/Exception;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static parseKeepingOrder([B)Ljava/lang/Object;
    .locals 3
    .param p0, "in"    # [B

    .prologue
    .line 171
    :try_start_0
    new-instance v1, Lnet/minidev/json/parser/JSONParser;

    sget v2, Lnet/minidev/json/parser/JSONParser;->DEFAULT_PERMISSIVE_MODE:I

    invoke-direct {v1, v2}, Lnet/minidev/json/parser/JSONParser;-><init>(I)V

    sget-object v2, Lnet/minidev/json/parser/ContainerFactory;->FACTORY_ORDERED:Lnet/minidev/json/parser/ContainerFactory;

    invoke-virtual {v1, p0, v2}, Lnet/minidev/json/parser/JSONParser;->parse([BLnet/minidev/json/parser/ContainerFactory;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 173
    :goto_0
    return-object v1

    .line 172
    :catch_0
    move-exception v0

    .line 173
    .local v0, "e":Ljava/lang/Exception;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static parseKeepingOrder([BII)Ljava/lang/Object;
    .locals 3
    .param p0, "in"    # [B
    .param p1, "offset"    # I
    .param p2, "length"    # I

    .prologue
    .line 184
    :try_start_0
    new-instance v1, Lnet/minidev/json/parser/JSONParser;

    sget v2, Lnet/minidev/json/parser/JSONParser;->DEFAULT_PERMISSIVE_MODE:I

    invoke-direct {v1, v2}, Lnet/minidev/json/parser/JSONParser;-><init>(I)V

    sget-object v2, Lnet/minidev/json/parser/ContainerFactory;->FACTORY_ORDERED:Lnet/minidev/json/parser/ContainerFactory;

    invoke-virtual {v1, p0, p1, p2, v2}, Lnet/minidev/json/parser/JSONParser;->parse([BIILnet/minidev/json/parser/ContainerFactory;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 186
    :goto_0
    return-object v1

    .line 185
    :catch_0
    move-exception v0

    .line 186
    .local v0, "e":Ljava/lang/Exception;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static parseStrict(Ljava/io/InputStream;)Ljava/lang/Object;
    .locals 2
    .param p0, "in"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lnet/minidev/json/parser/ParseException;
        }
    .end annotation

    .prologue
    .line 371
    new-instance v0, Lnet/minidev/json/parser/JSONParser;

    const/16 v1, 0x190

    invoke-direct {v0, v1}, Lnet/minidev/json/parser/JSONParser;-><init>(I)V

    sget-object v1, Lnet/minidev/json/parser/ContainerFactory;->FACTORY_SIMPLE:Lnet/minidev/json/parser/ContainerFactory;

    invoke-virtual {v0, p0, v1}, Lnet/minidev/json/parser/JSONParser;->parse(Ljava/io/InputStream;Lnet/minidev/json/parser/ContainerFactory;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static parseStrict(Ljava/io/Reader;)Ljava/lang/Object;
    .locals 2
    .param p0, "in"    # Ljava/io/Reader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lnet/minidev/json/parser/ParseException;
        }
    .end annotation

    .prologue
    .line 383
    new-instance v0, Lnet/minidev/json/parser/JSONParser;

    const/16 v1, 0x190

    invoke-direct {v0, v1}, Lnet/minidev/json/parser/JSONParser;-><init>(I)V

    sget-object v1, Lnet/minidev/json/parser/ContainerFactory;->FACTORY_SIMPLE:Lnet/minidev/json/parser/ContainerFactory;

    invoke-virtual {v0, p0, v1}, Lnet/minidev/json/parser/JSONParser;->parse(Ljava/io/Reader;Lnet/minidev/json/parser/ContainerFactory;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static parseStrict(Ljava/lang/String;)Ljava/lang/Object;
    .locals 2
    .param p0, "s"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lnet/minidev/json/parser/ParseException;
        }
    .end annotation

    .prologue
    .line 395
    new-instance v0, Lnet/minidev/json/parser/JSONParser;

    const/16 v1, 0x190

    invoke-direct {v0, v1}, Lnet/minidev/json/parser/JSONParser;-><init>(I)V

    sget-object v1, Lnet/minidev/json/parser/ContainerFactory;->FACTORY_SIMPLE:Lnet/minidev/json/parser/ContainerFactory;

    invoke-virtual {v0, p0, v1}, Lnet/minidev/json/parser/JSONParser;->parse(Ljava/lang/String;Lnet/minidev/json/parser/ContainerFactory;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static parseStrict([B)Ljava/lang/Object;
    .locals 2
    .param p0, "s"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lnet/minidev/json/parser/ParseException;
        }
    .end annotation

    .prologue
    .line 407
    new-instance v0, Lnet/minidev/json/parser/JSONParser;

    const/16 v1, 0x190

    invoke-direct {v0, v1}, Lnet/minidev/json/parser/JSONParser;-><init>(I)V

    sget-object v1, Lnet/minidev/json/parser/ContainerFactory;->FACTORY_SIMPLE:Lnet/minidev/json/parser/ContainerFactory;

    invoke-virtual {v0, p0, v1}, Lnet/minidev/json/parser/JSONParser;->parse([BLnet/minidev/json/parser/ContainerFactory;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static parseStrict([BII)Ljava/lang/Object;
    .locals 2
    .param p0, "s"    # [B
    .param p1, "offset"    # I
    .param p2, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lnet/minidev/json/parser/ParseException;
        }
    .end annotation

    .prologue
    .line 421
    new-instance v0, Lnet/minidev/json/parser/JSONParser;

    const/16 v1, 0x190

    invoke-direct {v0, v1}, Lnet/minidev/json/parser/JSONParser;-><init>(I)V

    sget-object v1, Lnet/minidev/json/parser/ContainerFactory;->FACTORY_SIMPLE:Lnet/minidev/json/parser/ContainerFactory;

    invoke-virtual {v0, p0, p1, p2, v1}, Lnet/minidev/json/parser/JSONParser;->parse([BIILnet/minidev/json/parser/ContainerFactory;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static parseWithException(Ljava/io/InputStream;)Ljava/lang/Object;
    .locals 2
    .param p0, "in"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lnet/minidev/json/parser/ParseException;
        }
    .end annotation

    .prologue
    .line 333
    new-instance v0, Lnet/minidev/json/parser/JSONParser;

    sget v1, Lnet/minidev/json/parser/JSONParser;->DEFAULT_PERMISSIVE_MODE:I

    invoke-direct {v0, v1}, Lnet/minidev/json/parser/JSONParser;-><init>(I)V

    sget-object v1, Lnet/minidev/json/parser/ContainerFactory;->FACTORY_SIMPLE:Lnet/minidev/json/parser/ContainerFactory;

    invoke-virtual {v0, p0, v1}, Lnet/minidev/json/parser/JSONParser;->parse(Ljava/io/InputStream;Lnet/minidev/json/parser/ContainerFactory;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static parseWithException(Ljava/io/Reader;)Ljava/lang/Object;
    .locals 2
    .param p0, "in"    # Ljava/io/Reader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lnet/minidev/json/parser/ParseException;
        }
    .end annotation

    .prologue
    .line 345
    new-instance v0, Lnet/minidev/json/parser/JSONParser;

    sget v1, Lnet/minidev/json/parser/JSONParser;->DEFAULT_PERMISSIVE_MODE:I

    invoke-direct {v0, v1}, Lnet/minidev/json/parser/JSONParser;-><init>(I)V

    sget-object v1, Lnet/minidev/json/parser/ContainerFactory;->FACTORY_SIMPLE:Lnet/minidev/json/parser/ContainerFactory;

    invoke-virtual {v0, p0, v1}, Lnet/minidev/json/parser/JSONParser;->parse(Ljava/io/Reader;Lnet/minidev/json/parser/ContainerFactory;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static parseWithException(Ljava/lang/String;)Ljava/lang/Object;
    .locals 2
    .param p0, "s"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lnet/minidev/json/parser/ParseException;
        }
    .end annotation

    .prologue
    .line 357
    new-instance v0, Lnet/minidev/json/parser/JSONParser;

    sget v1, Lnet/minidev/json/parser/JSONParser;->DEFAULT_PERMISSIVE_MODE:I

    invoke-direct {v0, v1}, Lnet/minidev/json/parser/JSONParser;-><init>(I)V

    sget-object v1, Lnet/minidev/json/parser/ContainerFactory;->FACTORY_SIMPLE:Lnet/minidev/json/parser/ContainerFactory;

    invoke-virtual {v0, p0, v1}, Lnet/minidev/json/parser/JSONParser;->parse(Ljava/lang/String;Lnet/minidev/json/parser/ContainerFactory;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static parseWithException([B)Ljava/lang/Object;
    .locals 2
    .param p0, "in"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lnet/minidev/json/parser/ParseException;
        }
    .end annotation

    .prologue
    .line 305
    new-instance v0, Lnet/minidev/json/parser/JSONParser;

    sget v1, Lnet/minidev/json/parser/JSONParser;->DEFAULT_PERMISSIVE_MODE:I

    invoke-direct {v0, v1}, Lnet/minidev/json/parser/JSONParser;-><init>(I)V

    sget-object v1, Lnet/minidev/json/parser/ContainerFactory;->FACTORY_SIMPLE:Lnet/minidev/json/parser/ContainerFactory;

    invoke-virtual {v0, p0, v1}, Lnet/minidev/json/parser/JSONParser;->parse([BLnet/minidev/json/parser/ContainerFactory;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static parseWithException([BII)Ljava/lang/Object;
    .locals 2
    .param p0, "in"    # [B
    .param p1, "offset"    # I
    .param p2, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lnet/minidev/json/parser/ParseException;
        }
    .end annotation

    .prologue
    .line 319
    new-instance v0, Lnet/minidev/json/parser/JSONParser;

    sget v1, Lnet/minidev/json/parser/JSONParser;->DEFAULT_PERMISSIVE_MODE:I

    invoke-direct {v0, v1}, Lnet/minidev/json/parser/JSONParser;-><init>(I)V

    sget-object v1, Lnet/minidev/json/parser/ContainerFactory;->FACTORY_SIMPLE:Lnet/minidev/json/parser/ContainerFactory;

    invoke-virtual {v0, p0, p1, p2, v1}, Lnet/minidev/json/parser/JSONParser;->parse([BIILnet/minidev/json/parser/ContainerFactory;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static toJSONString(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1
    .param p0, "value"    # Ljava/lang/Object;

    .prologue
    .line 541
    sget-object v0, Lnet/minidev/json/JSONValue;->COMPRESSION:Lnet/minidev/json/JSONStyle;

    invoke-static {p0, v0}, Lnet/minidev/json/JSONValue;->toJSONString(Ljava/lang/Object;Lnet/minidev/json/JSONStyle;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static toJSONString(Ljava/lang/Object;Lnet/minidev/json/JSONStyle;)Ljava/lang/String;
    .locals 2
    .param p0, "value"    # Ljava/lang/Object;
    .param p1, "compression"    # Lnet/minidev/json/JSONStyle;

    .prologue
    .line 561
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 563
    .local v0, "sb":Ljava/lang/StringBuilder;
    :try_start_0
    invoke-static {p0, v0, p1}, Lnet/minidev/json/JSONValue;->writeJSONString(Ljava/lang/Object;Ljava/lang/Appendable;Lnet/minidev/json/JSONStyle;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 567
    :goto_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 564
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public static uncompress(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    .line 291
    sget-object v0, Lnet/minidev/json/JSONStyle;->NO_COMPRESS:Lnet/minidev/json/JSONStyle;

    invoke-static {p0, v0}, Lnet/minidev/json/JSONValue;->compress(Ljava/lang/String;Lnet/minidev/json/JSONStyle;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static writeJSONString(Ljava/lang/Object;Ljava/lang/Appendable;)V
    .locals 1
    .param p0, "value"    # Ljava/lang/Object;
    .param p1, "out"    # Ljava/lang/Appendable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 491
    sget-object v0, Lnet/minidev/json/JSONValue;->COMPRESSION:Lnet/minidev/json/JSONStyle;

    invoke-static {p0, p1, v0}, Lnet/minidev/json/JSONValue;->writeJSONString(Ljava/lang/Object;Ljava/lang/Appendable;Lnet/minidev/json/JSONStyle;)V

    .line 492
    return-void
.end method

.method public static writeJSONString(Ljava/lang/Object;Ljava/lang/Appendable;Lnet/minidev/json/JSONStyle;)V
    .locals 5
    .param p0, "value"    # Ljava/lang/Object;
    .param p1, "out"    # Ljava/lang/Appendable;
    .param p2, "compression"    # Lnet/minidev/json/JSONStyle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 508
    if-nez p0, :cond_0

    .line 509
    const-string v2, "null"

    invoke-interface {p1, v2}, Ljava/lang/Appendable;->append(Ljava/lang/CharSequence;)Ljava/lang/Appendable;

    .line 528
    :goto_0
    return-void

    .line 512
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    .line 514
    .local v0, "clz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    sget-object v2, Lnet/minidev/json/JSONValue;->defaultWriter:Lnet/minidev/json/reader/JsonWriter;

    invoke-virtual {v2, v0}, Lnet/minidev/json/reader/JsonWriter;->getWrite(Ljava/lang/Class;)Lnet/minidev/json/reader/JsonWriterI;

    move-result-object v1

    .line 515
    .local v1, "w":Lnet/minidev/json/reader/JsonWriterI;
    if-nez v1, :cond_2

    .line 516
    invoke-virtual {v0}, Ljava/lang/Class;->isArray()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 517
    sget-object v1, Lnet/minidev/json/reader/JsonWriter;->arrayWriter:Lnet/minidev/json/reader/JsonWriterI;

    .line 525
    :cond_1
    :goto_1
    sget-object v2, Lnet/minidev/json/JSONValue;->defaultWriter:Lnet/minidev/json/reader/JsonWriter;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Class;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v2, v1, v3}, Lnet/minidev/json/reader/JsonWriter;->registerWriter(Lnet/minidev/json/reader/JsonWriterI;[Ljava/lang/Class;)V

    .line 527
    :cond_2
    invoke-interface {v1, p0, p1, p2}, Lnet/minidev/json/reader/JsonWriterI;->writeJSONString(Ljava/lang/Object;Ljava/lang/Appendable;Lnet/minidev/json/JSONStyle;)V

    goto :goto_0

    .line 519
    :cond_3
    sget-object v2, Lnet/minidev/json/JSONValue;->defaultWriter:Lnet/minidev/json/reader/JsonWriter;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v2, v3}, Lnet/minidev/json/reader/JsonWriter;->getWriterByInterface(Ljava/lang/Class;)Lnet/minidev/json/reader/JsonWriterI;

    move-result-object v1

    .line 520
    if-nez v1, :cond_1

    .line 521
    sget-object v1, Lnet/minidev/json/reader/JsonWriter;->beansWriter:Lnet/minidev/json/reader/JsonWriterI;

    goto :goto_1
.end method
