.class public Lcom/navdy/service/library/network/http/services/JiraClient;
.super Ljava/lang/Object;
.source "JiraClient.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;,
        Lcom/navdy/service/library/network/http/services/JiraClient$Attachment;
    }
.end annotation


# static fields
.field public static final DESCRIPTION:Ljava/lang/String; = "description"

.field public static final FIELDS:Ljava/lang/String; = "fields"

.field public static final ISSUE_TYPE:Ljava/lang/String; = "issuetype"

.field public static final ISSUE_TYPE_BUG:Ljava/lang/String; = "Bug"

.field private static final JIRA_ATTACHMENTS:Ljava/lang/String; = "/attachments/"

.field private static final JIRA_ISSUE_API_URL:Ljava/lang/String; = "https://navdyhud.atlassian.net/rest/api/2/issue/"

.field public static final KEY:Ljava/lang/String; = "key"

.field public static final NAME:Ljava/lang/String; = "name"

.field public static final PROJECT:Ljava/lang/String; = "project"

.field public static final SUMMARY:Ljava/lang/String; = "summary"

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private encodedCredentials:Ljava/lang/String;

.field private mClient:Lokhttp3/OkHttpClient;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 33
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/service/library/network/http/services/JiraClient;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/service/library/network/http/services/JiraClient;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>(Lokhttp3/OkHttpClient;)V
    .locals 0
    .param p1, "httpClient"    # Lokhttp3/OkHttpClient;

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    iput-object p1, p0, Lcom/navdy/service/library/network/http/services/JiraClient;->mClient:Lokhttp3/OkHttpClient;

    .line 60
    return-void
.end method


# virtual methods
.method public attachFilesToTicket(Ljava/lang/String;Ljava/util/List;Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;)V
    .locals 11
    .param p1, "ticketId"    # Ljava/lang/String;
    .param p3, "callback"    # Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/network/http/services/JiraClient$Attachment;",
            ">;",
            "Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;",
            ")V"
        }
    .end annotation

    .prologue
    .line 126
    .local p2, "attachments":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/network/http/services/JiraClient$Attachment;>;"
    new-instance v7, Lokhttp3/MultipartBody$Builder;

    invoke-direct {v7}, Lokhttp3/MultipartBody$Builder;-><init>()V

    sget-object v8, Lokhttp3/MultipartBody;->FORM:Lokhttp3/MediaType;

    .line 127
    invoke-virtual {v7, v8}, Lokhttp3/MultipartBody$Builder;->setType(Lokhttp3/MediaType;)Lokhttp3/MultipartBody$Builder;

    move-result-object v1

    .line 128
    .local v1, "builder":Lokhttp3/MultipartBody$Builder;
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/network/http/services/JiraClient$Attachment;

    .line 129
    .local v0, "attachment":Lcom/navdy/service/library/network/http/services/JiraClient$Attachment;
    new-instance v2, Ljava/io/File;

    iget-object v8, v0, Lcom/navdy/service/library/network/http/services/JiraClient$Attachment;->filePath:Ljava/lang/String;

    invoke-direct {v2, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 130
    .local v2, "file":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v8

    if-nez v8, :cond_0

    .line 131
    new-instance v7, Ljava/lang/IllegalArgumentException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Attachment "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "File does not exists"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 133
    :cond_0
    invoke-virtual {v2}, Ljava/io/File;->canRead()Z

    move-result v8

    if-nez v8, :cond_1

    .line 134
    new-instance v7, Ljava/lang/IllegalArgumentException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Attachment "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "File cannot be read"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 136
    :cond_1
    iget-object v8, v0, Lcom/navdy/service/library/network/http/services/JiraClient$Attachment;->mimeType:Ljava/lang/String;

    invoke-static {v8}, Lokhttp3/MediaType;->parse(Ljava/lang/String;)Lokhttp3/MediaType;

    move-result-object v3

    .line 137
    .local v3, "mediaType":Lokhttp3/MediaType;
    const-string v8, "file"

    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v9

    .line 138
    invoke-static {v3, v2}, Lokhttp3/RequestBody;->create(Lokhttp3/MediaType;Ljava/io/File;)Lokhttp3/RequestBody;

    move-result-object v10

    .line 137
    invoke-virtual {v1, v8, v9, v10}, Lokhttp3/MultipartBody$Builder;->addFormDataPart(Ljava/lang/String;Ljava/lang/String;Lokhttp3/RequestBody;)Lokhttp3/MultipartBody$Builder;

    goto :goto_0

    .line 140
    .end local v0    # "attachment":Lcom/navdy/service/library/network/http/services/JiraClient$Attachment;
    .end local v2    # "file":Ljava/io/File;
    .end local v3    # "mediaType":Lokhttp3/MediaType;
    :cond_2
    invoke-virtual {v1}, Lokhttp3/MultipartBody$Builder;->build()Lokhttp3/MultipartBody;

    move-result-object v5

    .line 141
    .local v5, "requestBody":Lokhttp3/RequestBody;
    new-instance v7, Lokhttp3/Request$Builder;

    invoke-direct {v7}, Lokhttp3/Request$Builder;-><init>()V

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "https://navdyhud.atlassian.net/rest/api/2/issue/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/attachments/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lokhttp3/Request$Builder;->url(Ljava/lang/String;)Lokhttp3/Request$Builder;

    move-result-object v7

    const-string v8, "X-Atlassian-Token"

    const-string v9, "nocheck"

    invoke-virtual {v7, v8, v9}, Lokhttp3/Request$Builder;->addHeader(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/Request$Builder;

    move-result-object v7

    invoke-virtual {v7, v5}, Lokhttp3/Request$Builder;->post(Lokhttp3/RequestBody;)Lokhttp3/Request$Builder;

    move-result-object v6

    .line 143
    .local v6, "requestBuilder":Lokhttp3/Request$Builder;
    iget-object v7, p0, Lcom/navdy/service/library/network/http/services/JiraClient;->encodedCredentials:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_3

    .line 144
    const-string v7, "Authorization"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Basic "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/navdy/service/library/network/http/services/JiraClient;->encodedCredentials:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lokhttp3/Request$Builder;->addHeader(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/Request$Builder;

    .line 146
    :cond_3
    invoke-virtual {v6}, Lokhttp3/Request$Builder;->build()Lokhttp3/Request;

    move-result-object v4

    .line 147
    .local v4, "request":Lokhttp3/Request;
    iget-object v7, p0, Lcom/navdy/service/library/network/http/services/JiraClient;->mClient:Lokhttp3/OkHttpClient;

    invoke-virtual {v7, v4}, Lokhttp3/OkHttpClient;->newCall(Lokhttp3/Request;)Lokhttp3/Call;

    move-result-object v7

    new-instance v8, Lcom/navdy/service/library/network/http/services/JiraClient$2;

    invoke-direct {v8, p0, p3}, Lcom/navdy/service/library/network/http/services/JiraClient$2;-><init>(Lcom/navdy/service/library/network/http/services/JiraClient;Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;)V

    invoke-interface {v7, v8}, Lokhttp3/Call;->enqueue(Lokhttp3/Callback;)V

    .line 171
    return-void
.end method

.method public setEncodedCredentials(Ljava/lang/String;)V
    .locals 0
    .param p1, "encodedCredentials"    # Ljava/lang/String;

    .prologue
    .line 122
    iput-object p1, p0, Lcom/navdy/service/library/network/http/services/JiraClient;->encodedCredentials:Ljava/lang/String;

    .line 123
    return-void
.end method

.method public submitTicket(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;)V
    .locals 14
    .param p1, "projectName"    # Ljava/lang/String;
    .param p2, "issueTypeName"    # Ljava/lang/String;
    .param p3, "summary"    # Ljava/lang/String;
    .param p4, "description"    # Ljava/lang/String;
    .param p5, "callback"    # Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 63
    const/4 v10, 0x0

    .line 65
    .local v10, "writer":Ljava/io/OutputStreamWriter;
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 66
    .local v1, "data":Lorg/json/JSONObject;
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 67
    .local v2, "fields":Lorg/json/JSONObject;
    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5}, Lorg/json/JSONObject;-><init>()V

    .line 68
    .local v5, "project":Lorg/json/JSONObject;
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    .line 69
    .local v3, "issueType":Lorg/json/JSONObject;
    const-string v11, "key"

    invoke-virtual {v5, v11, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 70
    const-string v11, "project"

    invoke-virtual {v2, v11, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 71
    const-string v11, "name"

    move-object/from16 v0, p2

    invoke-virtual {v3, v11, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 72
    const-string v11, "issuetype"

    invoke-virtual {v2, v11, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 73
    const-string v11, "summary"

    move-object/from16 v0, p3

    invoke-virtual {v2, v11, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 74
    const-string v11, "description"

    move-object/from16 v0, p4

    invoke-virtual {v2, v11, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 75
    const-string v11, "fields"

    invoke-virtual {v1, v11, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 76
    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v4

    .line 77
    .local v4, "jsonData":Ljava/lang/String;
    invoke-static {v4}, Lcom/navdy/service/library/network/http/HttpUtils;->getJsonRequestBody(Ljava/lang/String;)Lokhttp3/RequestBody;

    move-result-object v7

    .line 78
    .local v7, "requestBody":Lokhttp3/RequestBody;
    new-instance v11, Lokhttp3/Request$Builder;

    invoke-direct {v11}, Lokhttp3/Request$Builder;-><init>()V

    const-string v12, "https://navdyhud.atlassian.net/rest/api/2/issue/"

    invoke-virtual {v11, v12}, Lokhttp3/Request$Builder;->url(Ljava/lang/String;)Lokhttp3/Request$Builder;

    move-result-object v11

    invoke-virtual {v11, v7}, Lokhttp3/Request$Builder;->post(Lokhttp3/RequestBody;)Lokhttp3/Request$Builder;

    move-result-object v8

    .line 79
    .local v8, "requestBuilder":Lokhttp3/Request$Builder;
    iget-object v11, p0, Lcom/navdy/service/library/network/http/services/JiraClient;->encodedCredentials:Ljava/lang/String;

    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_0

    .line 80
    const-string v11, "Authorization"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Basic "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-object v13, p0, Lcom/navdy/service/library/network/http/services/JiraClient;->encodedCredentials:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v8, v11, v12}, Lokhttp3/Request$Builder;->addHeader(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/Request$Builder;

    .line 82
    :cond_0
    invoke-virtual {v8}, Lokhttp3/Request$Builder;->build()Lokhttp3/Request;

    move-result-object v6

    .line 83
    .local v6, "request":Lokhttp3/Request;
    iget-object v11, p0, Lcom/navdy/service/library/network/http/services/JiraClient;->mClient:Lokhttp3/OkHttpClient;

    invoke-virtual {v11, v6}, Lokhttp3/OkHttpClient;->newCall(Lokhttp3/Request;)Lokhttp3/Call;

    move-result-object v11

    new-instance v12, Lcom/navdy/service/library/network/http/services/JiraClient$1;

    move-object/from16 v0, p5

    invoke-direct {v12, p0, v0}, Lcom/navdy/service/library/network/http/services/JiraClient$1;-><init>(Lcom/navdy/service/library/network/http/services/JiraClient;Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;)V

    invoke-interface {v11, v12}, Lokhttp3/Call;->enqueue(Lokhttp3/Callback;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 117
    invoke-static {v10}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 119
    .end local v1    # "data":Lorg/json/JSONObject;
    .end local v2    # "fields":Lorg/json/JSONObject;
    .end local v3    # "issueType":Lorg/json/JSONObject;
    .end local v4    # "jsonData":Ljava/lang/String;
    .end local v5    # "project":Lorg/json/JSONObject;
    .end local v6    # "request":Lokhttp3/Request;
    .end local v7    # "requestBody":Lokhttp3/RequestBody;
    .end local v8    # "requestBuilder":Lokhttp3/Request$Builder;
    :goto_0
    return-void

    .line 114
    :catch_0
    move-exception v9

    .line 115
    .local v9, "t":Ljava/lang/Throwable;
    :try_start_1
    move-object/from16 v0, p5

    invoke-interface {v0, v9}, Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;->onError(Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 117
    invoke-static {v10}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    goto :goto_0

    .end local v9    # "t":Ljava/lang/Throwable;
    :catchall_0
    move-exception v11

    invoke-static {v10}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v11
.end method
