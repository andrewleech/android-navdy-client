.class public interface abstract Lcom/navdy/service/library/device/RemoteDevice$Listener;
.super Ljava/lang/Object;
.source "RemoteDevice.java"

# interfaces
.implements Lcom/navdy/service/library/util/Listenable$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/device/RemoteDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Listener"
.end annotation


# virtual methods
.method public abstract onDeviceConnectFailure(Lcom/navdy/service/library/device/RemoteDevice;Lcom/navdy/service/library/device/connection/Connection$ConnectionFailureCause;)V
.end method

.method public abstract onDeviceConnected(Lcom/navdy/service/library/device/RemoteDevice;)V
.end method

.method public abstract onDeviceConnecting(Lcom/navdy/service/library/device/RemoteDevice;)V
.end method

.method public abstract onDeviceDisconnected(Lcom/navdy/service/library/device/RemoteDevice;Lcom/navdy/service/library/device/connection/Connection$DisconnectCause;)V
.end method

.method public abstract onNavdyEventReceived(Lcom/navdy/service/library/device/RemoteDevice;Lcom/navdy/service/library/events/NavdyEvent;)V
.end method

.method public abstract onNavdyEventReceived(Lcom/navdy/service/library/device/RemoteDevice;[B)V
.end method
