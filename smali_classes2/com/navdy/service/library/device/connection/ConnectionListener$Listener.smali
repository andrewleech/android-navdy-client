.class public interface abstract Lcom/navdy/service/library/device/connection/ConnectionListener$Listener;
.super Ljava/lang/Object;
.source "ConnectionListener.java"

# interfaces
.implements Lcom/navdy/service/library/util/Listenable$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/device/connection/ConnectionListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Listener"
.end annotation


# virtual methods
.method public abstract onConnected(Lcom/navdy/service/library/device/connection/ConnectionListener;Lcom/navdy/service/library/device/connection/Connection;)V
.end method

.method public abstract onConnectionFailed(Lcom/navdy/service/library/device/connection/ConnectionListener;)V
.end method

.method public abstract onStartFailure(Lcom/navdy/service/library/device/connection/ConnectionListener;)V
.end method

.method public abstract onStarted(Lcom/navdy/service/library/device/connection/ConnectionListener;)V
.end method

.method public abstract onStopped(Lcom/navdy/service/library/device/connection/ConnectionListener;)V
.end method
