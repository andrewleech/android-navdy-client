.class public final enum Lcom/navdy/service/library/task/TaskManager$TaskPriority;
.super Ljava/lang/Enum;
.source "TaskManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/task/TaskManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "TaskPriority"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/service/library/task/TaskManager$TaskPriority;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/service/library/task/TaskManager$TaskPriority;

.field public static final enum HIGH:Lcom/navdy/service/library/task/TaskManager$TaskPriority;

.field public static final enum LOW:Lcom/navdy/service/library/task/TaskManager$TaskPriority;

.field public static final enum NORMAL:Lcom/navdy/service/library/task/TaskManager$TaskPriority;


# instance fields
.field private final val:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x0

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 32
    new-instance v0, Lcom/navdy/service/library/task/TaskManager$TaskPriority;

    const-string v1, "LOW"

    invoke-direct {v0, v1, v4, v2}, Lcom/navdy/service/library/task/TaskManager$TaskPriority;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/task/TaskManager$TaskPriority;->LOW:Lcom/navdy/service/library/task/TaskManager$TaskPriority;

    .line 33
    new-instance v0, Lcom/navdy/service/library/task/TaskManager$TaskPriority;

    const-string v1, "NORMAL"

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/task/TaskManager$TaskPriority;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/task/TaskManager$TaskPriority;->NORMAL:Lcom/navdy/service/library/task/TaskManager$TaskPriority;

    .line 34
    new-instance v0, Lcom/navdy/service/library/task/TaskManager$TaskPriority;

    const-string v1, "HIGH"

    invoke-direct {v0, v1, v3, v5}, Lcom/navdy/service/library/task/TaskManager$TaskPriority;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/task/TaskManager$TaskPriority;->HIGH:Lcom/navdy/service/library/task/TaskManager$TaskPriority;

    .line 31
    new-array v0, v5, [Lcom/navdy/service/library/task/TaskManager$TaskPriority;

    sget-object v1, Lcom/navdy/service/library/task/TaskManager$TaskPriority;->LOW:Lcom/navdy/service/library/task/TaskManager$TaskPriority;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/service/library/task/TaskManager$TaskPriority;->NORMAL:Lcom/navdy/service/library/task/TaskManager$TaskPriority;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/service/library/task/TaskManager$TaskPriority;->HIGH:Lcom/navdy/service/library/task/TaskManager$TaskPriority;

    aput-object v1, v0, v3

    sput-object v0, Lcom/navdy/service/library/task/TaskManager$TaskPriority;->$VALUES:[Lcom/navdy/service/library/task/TaskManager$TaskPriority;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "val"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 38
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 39
    iput p3, p0, Lcom/navdy/service/library/task/TaskManager$TaskPriority;->val:I

    .line 40
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/service/library/task/TaskManager$TaskPriority;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 31
    const-class v0, Lcom/navdy/service/library/task/TaskManager$TaskPriority;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/task/TaskManager$TaskPriority;

    return-object v0
.end method

.method public static values()[Lcom/navdy/service/library/task/TaskManager$TaskPriority;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/navdy/service/library/task/TaskManager$TaskPriority;->$VALUES:[Lcom/navdy/service/library/task/TaskManager$TaskPriority;

    invoke-virtual {v0}, [Lcom/navdy/service/library/task/TaskManager$TaskPriority;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/service/library/task/TaskManager$TaskPriority;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 43
    iget v0, p0, Lcom/navdy/service/library/task/TaskManager$TaskPriority;->val:I

    return v0
.end method
