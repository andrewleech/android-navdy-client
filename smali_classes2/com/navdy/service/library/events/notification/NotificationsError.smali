.class public final enum Lcom/navdy/service/library/events/notification/NotificationsError;
.super Ljava/lang/Enum;
.source "NotificationsError.java"

# interfaces
.implements Lcom/squareup/wire/ProtoEnum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/service/library/events/notification/NotificationsError;",
        ">;",
        "Lcom/squareup/wire/ProtoEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/service/library/events/notification/NotificationsError;

.field public static final enum NOTIFICATIONS_ERROR_AUTH_FAILED:Lcom/navdy/service/library/events/notification/NotificationsError;

.field public static final enum NOTIFICATIONS_ERROR_BOND_REMOVED:Lcom/navdy/service/library/events/notification/NotificationsError;

.field public static final enum NOTIFICATIONS_ERROR_UNKNOWN:Lcom/navdy/service/library/events/notification/NotificationsError;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x0

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 12
    new-instance v0, Lcom/navdy/service/library/events/notification/NotificationsError;

    const-string v1, "NOTIFICATIONS_ERROR_UNKNOWN"

    invoke-direct {v0, v1, v4, v2}, Lcom/navdy/service/library/events/notification/NotificationsError;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/notification/NotificationsError;->NOTIFICATIONS_ERROR_UNKNOWN:Lcom/navdy/service/library/events/notification/NotificationsError;

    .line 16
    new-instance v0, Lcom/navdy/service/library/events/notification/NotificationsError;

    const-string v1, "NOTIFICATIONS_ERROR_AUTH_FAILED"

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/notification/NotificationsError;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/notification/NotificationsError;->NOTIFICATIONS_ERROR_AUTH_FAILED:Lcom/navdy/service/library/events/notification/NotificationsError;

    .line 21
    new-instance v0, Lcom/navdy/service/library/events/notification/NotificationsError;

    const-string v1, "NOTIFICATIONS_ERROR_BOND_REMOVED"

    invoke-direct {v0, v1, v3, v5}, Lcom/navdy/service/library/events/notification/NotificationsError;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/notification/NotificationsError;->NOTIFICATIONS_ERROR_BOND_REMOVED:Lcom/navdy/service/library/events/notification/NotificationsError;

    .line 7
    new-array v0, v5, [Lcom/navdy/service/library/events/notification/NotificationsError;

    sget-object v1, Lcom/navdy/service/library/events/notification/NotificationsError;->NOTIFICATIONS_ERROR_UNKNOWN:Lcom/navdy/service/library/events/notification/NotificationsError;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/service/library/events/notification/NotificationsError;->NOTIFICATIONS_ERROR_AUTH_FAILED:Lcom/navdy/service/library/events/notification/NotificationsError;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/service/library/events/notification/NotificationsError;->NOTIFICATIONS_ERROR_BOND_REMOVED:Lcom/navdy/service/library/events/notification/NotificationsError;

    aput-object v1, v0, v3

    sput-object v0, Lcom/navdy/service/library/events/notification/NotificationsError;->$VALUES:[Lcom/navdy/service/library/events/notification/NotificationsError;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 26
    iput p3, p0, Lcom/navdy/service/library/events/notification/NotificationsError;->value:I

    .line 27
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/service/library/events/notification/NotificationsError;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 7
    const-class v0, Lcom/navdy/service/library/events/notification/NotificationsError;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/notification/NotificationsError;

    return-object v0
.end method

.method public static values()[Lcom/navdy/service/library/events/notification/NotificationsError;
    .locals 1

    .prologue
    .line 7
    sget-object v0, Lcom/navdy/service/library/events/notification/NotificationsError;->$VALUES:[Lcom/navdy/service/library/events/notification/NotificationsError;

    invoke-virtual {v0}, [Lcom/navdy/service/library/events/notification/NotificationsError;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/service/library/events/notification/NotificationsError;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 31
    iget v0, p0, Lcom/navdy/service/library/events/notification/NotificationsError;->value:I

    return v0
.end method
