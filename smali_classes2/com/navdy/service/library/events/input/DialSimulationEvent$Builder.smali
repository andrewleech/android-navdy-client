.class public final Lcom/navdy/service/library/events/input/DialSimulationEvent$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "DialSimulationEvent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/input/DialSimulationEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/input/DialSimulationEvent;",
        ">;"
    }
.end annotation


# instance fields
.field public dialAction:Lcom/navdy/service/library/events/input/DialSimulationEvent$DialInputAction;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 47
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/input/DialSimulationEvent;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/input/DialSimulationEvent;

    .prologue
    .line 50
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 51
    if-nez p1, :cond_0

    .line 53
    :goto_0
    return-void

    .line 52
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/input/DialSimulationEvent;->dialAction:Lcom/navdy/service/library/events/input/DialSimulationEvent$DialInputAction;

    iput-object v0, p0, Lcom/navdy/service/library/events/input/DialSimulationEvent$Builder;->dialAction:Lcom/navdy/service/library/events/input/DialSimulationEvent$DialInputAction;

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/navdy/service/library/events/input/DialSimulationEvent;
    .locals 2

    .prologue
    .line 62
    invoke-virtual {p0}, Lcom/navdy/service/library/events/input/DialSimulationEvent$Builder;->checkRequiredFields()V

    .line 63
    new-instance v0, Lcom/navdy/service/library/events/input/DialSimulationEvent;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/input/DialSimulationEvent;-><init>(Lcom/navdy/service/library/events/input/DialSimulationEvent$Builder;Lcom/navdy/service/library/events/input/DialSimulationEvent$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 42
    invoke-virtual {p0}, Lcom/navdy/service/library/events/input/DialSimulationEvent$Builder;->build()Lcom/navdy/service/library/events/input/DialSimulationEvent;

    move-result-object v0

    return-object v0
.end method

.method public dialAction(Lcom/navdy/service/library/events/input/DialSimulationEvent$DialInputAction;)Lcom/navdy/service/library/events/input/DialSimulationEvent$Builder;
    .locals 0
    .param p1, "dialAction"    # Lcom/navdy/service/library/events/input/DialSimulationEvent$DialInputAction;

    .prologue
    .line 56
    iput-object p1, p0, Lcom/navdy/service/library/events/input/DialSimulationEvent$Builder;->dialAction:Lcom/navdy/service/library/events/input/DialSimulationEvent$DialInputAction;

    .line 57
    return-object p0
.end method
