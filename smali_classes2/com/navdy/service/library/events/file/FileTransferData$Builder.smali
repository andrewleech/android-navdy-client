.class public final Lcom/navdy/service/library/events/file/FileTransferData$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "FileTransferData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/file/FileTransferData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/file/FileTransferData;",
        ">;"
    }
.end annotation


# instance fields
.field public chunkIndex:Ljava/lang/Integer;

.field public dataBytes:Lokio/ByteString;

.field public fileCheckSum:Ljava/lang/String;

.field public lastChunk:Ljava/lang/Boolean;

.field public transferId:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 96
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 97
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/file/FileTransferData;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/file/FileTransferData;

    .prologue
    .line 100
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 101
    if-nez p1, :cond_0

    .line 107
    :goto_0
    return-void

    .line 102
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/file/FileTransferData;->transferId:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/navdy/service/library/events/file/FileTransferData$Builder;->transferId:Ljava/lang/Integer;

    .line 103
    iget-object v0, p1, Lcom/navdy/service/library/events/file/FileTransferData;->chunkIndex:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/navdy/service/library/events/file/FileTransferData$Builder;->chunkIndex:Ljava/lang/Integer;

    .line 104
    iget-object v0, p1, Lcom/navdy/service/library/events/file/FileTransferData;->dataBytes:Lokio/ByteString;

    iput-object v0, p0, Lcom/navdy/service/library/events/file/FileTransferData$Builder;->dataBytes:Lokio/ByteString;

    .line 105
    iget-object v0, p1, Lcom/navdy/service/library/events/file/FileTransferData;->lastChunk:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/navdy/service/library/events/file/FileTransferData$Builder;->lastChunk:Ljava/lang/Boolean;

    .line 106
    iget-object v0, p1, Lcom/navdy/service/library/events/file/FileTransferData;->fileCheckSum:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/file/FileTransferData$Builder;->fileCheckSum:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/navdy/service/library/events/file/FileTransferData;
    .locals 2

    .prologue
    .line 146
    invoke-virtual {p0}, Lcom/navdy/service/library/events/file/FileTransferData$Builder;->checkRequiredFields()V

    .line 147
    new-instance v0, Lcom/navdy/service/library/events/file/FileTransferData;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/file/FileTransferData;-><init>(Lcom/navdy/service/library/events/file/FileTransferData$Builder;Lcom/navdy/service/library/events/file/FileTransferData$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 88
    invoke-virtual {p0}, Lcom/navdy/service/library/events/file/FileTransferData$Builder;->build()Lcom/navdy/service/library/events/file/FileTransferData;

    move-result-object v0

    return-object v0
.end method

.method public chunkIndex(Ljava/lang/Integer;)Lcom/navdy/service/library/events/file/FileTransferData$Builder;
    .locals 0
    .param p1, "chunkIndex"    # Ljava/lang/Integer;

    .prologue
    .line 121
    iput-object p1, p0, Lcom/navdy/service/library/events/file/FileTransferData$Builder;->chunkIndex:Ljava/lang/Integer;

    .line 122
    return-object p0
.end method

.method public dataBytes(Lokio/ByteString;)Lcom/navdy/service/library/events/file/FileTransferData$Builder;
    .locals 0
    .param p1, "dataBytes"    # Lokio/ByteString;

    .prologue
    .line 126
    iput-object p1, p0, Lcom/navdy/service/library/events/file/FileTransferData$Builder;->dataBytes:Lokio/ByteString;

    .line 127
    return-object p0
.end method

.method public fileCheckSum(Ljava/lang/String;)Lcom/navdy/service/library/events/file/FileTransferData$Builder;
    .locals 0
    .param p1, "fileCheckSum"    # Ljava/lang/String;

    .prologue
    .line 140
    iput-object p1, p0, Lcom/navdy/service/library/events/file/FileTransferData$Builder;->fileCheckSum:Ljava/lang/String;

    .line 141
    return-object p0
.end method

.method public lastChunk(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/file/FileTransferData$Builder;
    .locals 0
    .param p1, "lastChunk"    # Ljava/lang/Boolean;

    .prologue
    .line 131
    iput-object p1, p0, Lcom/navdy/service/library/events/file/FileTransferData$Builder;->lastChunk:Ljava/lang/Boolean;

    .line 132
    return-object p0
.end method

.method public transferId(Ljava/lang/Integer;)Lcom/navdy/service/library/events/file/FileTransferData$Builder;
    .locals 0
    .param p1, "transferId"    # Ljava/lang/Integer;

    .prologue
    .line 113
    iput-object p1, p0, Lcom/navdy/service/library/events/file/FileTransferData$Builder;->transferId:Ljava/lang/Integer;

    .line 114
    return-object p0
.end method
