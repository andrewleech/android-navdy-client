.class public final enum Lcom/navdy/service/library/events/photo/PhotoType;
.super Ljava/lang/Enum;
.source "PhotoType.java"

# interfaces
.implements Lcom/squareup/wire/ProtoEnum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/service/library/events/photo/PhotoType;",
        ">;",
        "Lcom/squareup/wire/ProtoEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/service/library/events/photo/PhotoType;

.field public static final enum PHOTO_ALBUM_ART:Lcom/navdy/service/library/events/photo/PhotoType;

.field public static final enum PHOTO_CONTACT:Lcom/navdy/service/library/events/photo/PhotoType;

.field public static final enum PHOTO_DRIVER_PROFILE:Lcom/navdy/service/library/events/photo/PhotoType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 9
    new-instance v0, Lcom/navdy/service/library/events/photo/PhotoType;

    const-string v1, "PHOTO_CONTACT"

    invoke-direct {v0, v1, v2, v2}, Lcom/navdy/service/library/events/photo/PhotoType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/photo/PhotoType;->PHOTO_CONTACT:Lcom/navdy/service/library/events/photo/PhotoType;

    .line 10
    new-instance v0, Lcom/navdy/service/library/events/photo/PhotoType;

    const-string v1, "PHOTO_ALBUM_ART"

    invoke-direct {v0, v1, v3, v3}, Lcom/navdy/service/library/events/photo/PhotoType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/photo/PhotoType;->PHOTO_ALBUM_ART:Lcom/navdy/service/library/events/photo/PhotoType;

    .line 11
    new-instance v0, Lcom/navdy/service/library/events/photo/PhotoType;

    const-string v1, "PHOTO_DRIVER_PROFILE"

    invoke-direct {v0, v1, v4, v4}, Lcom/navdy/service/library/events/photo/PhotoType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/photo/PhotoType;->PHOTO_DRIVER_PROFILE:Lcom/navdy/service/library/events/photo/PhotoType;

    .line 7
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/navdy/service/library/events/photo/PhotoType;

    sget-object v1, Lcom/navdy/service/library/events/photo/PhotoType;->PHOTO_CONTACT:Lcom/navdy/service/library/events/photo/PhotoType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/service/library/events/photo/PhotoType;->PHOTO_ALBUM_ART:Lcom/navdy/service/library/events/photo/PhotoType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/service/library/events/photo/PhotoType;->PHOTO_DRIVER_PROFILE:Lcom/navdy/service/library/events/photo/PhotoType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/navdy/service/library/events/photo/PhotoType;->$VALUES:[Lcom/navdy/service/library/events/photo/PhotoType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 15
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 16
    iput p3, p0, Lcom/navdy/service/library/events/photo/PhotoType;->value:I

    .line 17
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/service/library/events/photo/PhotoType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 7
    const-class v0, Lcom/navdy/service/library/events/photo/PhotoType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/photo/PhotoType;

    return-object v0
.end method

.method public static values()[Lcom/navdy/service/library/events/photo/PhotoType;
    .locals 1

    .prologue
    .line 7
    sget-object v0, Lcom/navdy/service/library/events/photo/PhotoType;->$VALUES:[Lcom/navdy/service/library/events/photo/PhotoType;

    invoke-virtual {v0}, [Lcom/navdy/service/library/events/photo/PhotoType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/service/library/events/photo/PhotoType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 21
    iget v0, p0, Lcom/navdy/service/library/events/photo/PhotoType;->value:I

    return v0
.end method
