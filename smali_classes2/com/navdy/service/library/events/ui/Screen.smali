.class public final enum Lcom/navdy/service/library/events/ui/Screen;
.super Ljava/lang/Enum;
.source "Screen.java"

# interfaces
.implements Lcom/squareup/wire/ProtoEnum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/service/library/events/ui/Screen;",
        ">;",
        "Lcom/squareup/wire/ProtoEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/service/library/events/ui/Screen;

.field public static final enum SCREEN_AUTO_BRIGHTNESS:Lcom/navdy/service/library/events/ui/Screen;

.field public static final enum SCREEN_BACK:Lcom/navdy/service/library/events/ui/Screen;

.field public static final enum SCREEN_BLUETOOTH_PAIRING:Lcom/navdy/service/library/events/ui/Screen;

.field public static final enum SCREEN_BRIGHTNESS:Lcom/navdy/service/library/events/ui/Screen;

.field public static final enum SCREEN_CALL_CONTROL:Lcom/navdy/service/library/events/ui/Screen;

.field public static final enum SCREEN_CAROUSEL_MENU:Lcom/navdy/service/library/events/ui/Screen;

.field public static final enum SCREEN_CONTEXT_MENU:Lcom/navdy/service/library/events/ui/Screen;

.field public static final enum SCREEN_DASHBOARD:Lcom/navdy/service/library/events/ui/Screen;

.field public static final enum SCREEN_DESTINATION_PICKER:Lcom/navdy/service/library/events/ui/Screen;

.field public static final enum SCREEN_DIAL_PAIRING:Lcom/navdy/service/library/events/ui/Screen;

.field public static final enum SCREEN_DIAL_UPDATE_CONFIRMATION:Lcom/navdy/service/library/events/ui/Screen;

.field public static final enum SCREEN_DIAL_UPDATE_PROGRESS:Lcom/navdy/service/library/events/ui/Screen;

.field public static final enum SCREEN_FACTORY_RESET:Lcom/navdy/service/library/events/ui/Screen;

.field public static final enum SCREEN_FAVORITE_CONTACTS:Lcom/navdy/service/library/events/ui/Screen;

.field public static final enum SCREEN_FAVORITE_PLACES:Lcom/navdy/service/library/events/ui/Screen;

.field public static final enum SCREEN_FIRST_LAUNCH:Lcom/navdy/service/library/events/ui/Screen;

.field public static final enum SCREEN_FORCE_UPDATE:Lcom/navdy/service/library/events/ui/Screen;

.field public static final enum SCREEN_GESTURE_LEARNING:Lcom/navdy/service/library/events/ui/Screen;

.field public static final enum SCREEN_HOME:Lcom/navdy/service/library/events/ui/Screen;

.field public static final enum SCREEN_HYBRID_MAP:Lcom/navdy/service/library/events/ui/Screen;

.field public static final enum SCREEN_MAIN_MENU:Lcom/navdy/service/library/events/ui/Screen;

.field public static final enum SCREEN_MAP_ANIMATION_MODE:Lcom/navdy/service/library/events/ui/Screen;

.field public static final enum SCREEN_MEDIA_BROWSER:Lcom/navdy/service/library/events/ui/Screen;

.field public static final enum SCREEN_MENU:Lcom/navdy/service/library/events/ui/Screen;

.field public static final enum SCREEN_MUSIC:Lcom/navdy/service/library/events/ui/Screen;

.field public static final enum SCREEN_MUSIC_DETAILS:Lcom/navdy/service/library/events/ui/Screen;

.field public static final enum SCREEN_NOTIFICATION:Lcom/navdy/service/library/events/ui/Screen;

.field public static final enum SCREEN_OPTIONS:Lcom/navdy/service/library/events/ui/Screen;

.field public static final enum SCREEN_OTA_CONFIRMATION:Lcom/navdy/service/library/events/ui/Screen;

.field public static final enum SCREEN_PLACES:Lcom/navdy/service/library/events/ui/Screen;

.field public static final enum SCREEN_READ_MESSAGE:Lcom/navdy/service/library/events/ui/Screen;

.field public static final enum SCREEN_RECENT_CALLS:Lcom/navdy/service/library/events/ui/Screen;

.field public static final enum SCREEN_RECOMMENDED_PLACES:Lcom/navdy/service/library/events/ui/Screen;

.field public static final enum SCREEN_REPLY_MESSAGE:Lcom/navdy/service/library/events/ui/Screen;

.field public static final enum SCREEN_REPORT_ISSUE:Lcom/navdy/service/library/events/ui/Screen;

.field public static final enum SCREEN_SETTINGS:Lcom/navdy/service/library/events/ui/Screen;

.field public static final enum SCREEN_SHUTDOWN_CONFIRMATION:Lcom/navdy/service/library/events/ui/Screen;

.field public static final enum SCREEN_SYSTEM_INFO:Lcom/navdy/service/library/events/ui/Screen;

.field public static final enum SCREEN_TEMPERATURE_WARNING:Lcom/navdy/service/library/events/ui/Screen;

.field public static final enum SCREEN_TOAST:Lcom/navdy/service/library/events/ui/Screen;

.field public static final enum SCREEN_TURN_BY_TURN:Lcom/navdy/service/library/events/ui/Screen;

.field public static final enum SCREEN_VOICE_CONTROL:Lcom/navdy/service/library/events/ui/Screen;

.field public static final enum SCREEN_WELCOME:Lcom/navdy/service/library/events/ui/Screen;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x5

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 9
    new-instance v0, Lcom/navdy/service/library/events/ui/Screen;

    const-string v1, "SCREEN_DASHBOARD"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v4}, Lcom/navdy/service/library/events/ui/Screen;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_DASHBOARD:Lcom/navdy/service/library/events/ui/Screen;

    .line 10
    new-instance v0, Lcom/navdy/service/library/events/ui/Screen;

    const-string v1, "SCREEN_TURN_BY_TURN"

    invoke-direct {v0, v1, v4, v5}, Lcom/navdy/service/library/events/ui/Screen;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_TURN_BY_TURN:Lcom/navdy/service/library/events/ui/Screen;

    .line 14
    new-instance v0, Lcom/navdy/service/library/events/ui/Screen;

    const-string v1, "SCREEN_HYBRID_MAP"

    invoke-direct {v0, v1, v5, v6}, Lcom/navdy/service/library/events/ui/Screen;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_HYBRID_MAP:Lcom/navdy/service/library/events/ui/Screen;

    .line 18
    new-instance v0, Lcom/navdy/service/library/events/ui/Screen;

    const-string v1, "SCREEN_MEDIA_BROWSER"

    invoke-direct {v0, v1, v6, v7}, Lcom/navdy/service/library/events/ui/Screen;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_MEDIA_BROWSER:Lcom/navdy/service/library/events/ui/Screen;

    .line 25
    new-instance v0, Lcom/navdy/service/library/events/ui/Screen;

    const-string v1, "SCREEN_MENU"

    invoke-direct {v0, v1, v7, v8}, Lcom/navdy/service/library/events/ui/Screen;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_MENU:Lcom/navdy/service/library/events/ui/Screen;

    .line 26
    new-instance v0, Lcom/navdy/service/library/events/ui/Screen;

    const-string v1, "SCREEN_NOTIFICATION"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v8, v2}, Lcom/navdy/service/library/events/ui/Screen;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_NOTIFICATION:Lcom/navdy/service/library/events/ui/Screen;

    .line 30
    new-instance v0, Lcom/navdy/service/library/events/ui/Screen;

    const-string v1, "SCREEN_CALL_CONTROL"

    const/4 v2, 0x6

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/ui/Screen;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_CALL_CONTROL:Lcom/navdy/service/library/events/ui/Screen;

    .line 31
    new-instance v0, Lcom/navdy/service/library/events/ui/Screen;

    const-string v1, "SCREEN_MUSIC"

    const/4 v2, 0x7

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/ui/Screen;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_MUSIC:Lcom/navdy/service/library/events/ui/Screen;

    .line 35
    new-instance v0, Lcom/navdy/service/library/events/ui/Screen;

    const-string v1, "SCREEN_VOICE_CONTROL"

    const/16 v2, 0x8

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/ui/Screen;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_VOICE_CONTROL:Lcom/navdy/service/library/events/ui/Screen;

    .line 36
    new-instance v0, Lcom/navdy/service/library/events/ui/Screen;

    const-string v1, "SCREEN_RECENT_CALLS"

    const/16 v2, 0x9

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/ui/Screen;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_RECENT_CALLS:Lcom/navdy/service/library/events/ui/Screen;

    .line 37
    new-instance v0, Lcom/navdy/service/library/events/ui/Screen;

    const-string v1, "SCREEN_BACK"

    const/16 v2, 0xa

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/ui/Screen;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_BACK:Lcom/navdy/service/library/events/ui/Screen;

    .line 38
    new-instance v0, Lcom/navdy/service/library/events/ui/Screen;

    const-string v1, "SCREEN_CONTEXT_MENU"

    const/16 v2, 0xb

    const/16 v3, 0xc

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/ui/Screen;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_CONTEXT_MENU:Lcom/navdy/service/library/events/ui/Screen;

    .line 39
    new-instance v0, Lcom/navdy/service/library/events/ui/Screen;

    const-string v1, "SCREEN_CAROUSEL_MENU"

    const/16 v2, 0xc

    const/16 v3, 0xd

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/ui/Screen;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_CAROUSEL_MENU:Lcom/navdy/service/library/events/ui/Screen;

    .line 40
    new-instance v0, Lcom/navdy/service/library/events/ui/Screen;

    const-string v1, "SCREEN_OPTIONS"

    const/16 v2, 0xd

    const/16 v3, 0xe

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/ui/Screen;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_OPTIONS:Lcom/navdy/service/library/events/ui/Screen;

    .line 41
    new-instance v0, Lcom/navdy/service/library/events/ui/Screen;

    const-string v1, "SCREEN_PLACES"

    const/16 v2, 0xe

    const/16 v3, 0xf

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/ui/Screen;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_PLACES:Lcom/navdy/service/library/events/ui/Screen;

    .line 42
    new-instance v0, Lcom/navdy/service/library/events/ui/Screen;

    const-string v1, "SCREEN_OTA_CONFIRMATION"

    const/16 v2, 0xf

    const/16 v3, 0x10

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/ui/Screen;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_OTA_CONFIRMATION:Lcom/navdy/service/library/events/ui/Screen;

    .line 43
    new-instance v0, Lcom/navdy/service/library/events/ui/Screen;

    const-string v1, "SCREEN_WELCOME"

    const/16 v2, 0x10

    const/16 v3, 0x11

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/ui/Screen;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_WELCOME:Lcom/navdy/service/library/events/ui/Screen;

    .line 44
    new-instance v0, Lcom/navdy/service/library/events/ui/Screen;

    const-string v1, "SCREEN_FAVORITE_CONTACTS"

    const/16 v2, 0x11

    const/16 v3, 0x12

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/ui/Screen;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_FAVORITE_CONTACTS:Lcom/navdy/service/library/events/ui/Screen;

    .line 45
    new-instance v0, Lcom/navdy/service/library/events/ui/Screen;

    const-string v1, "SCREEN_FAVORITE_PLACES"

    const/16 v2, 0x12

    const/16 v3, 0x13

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/ui/Screen;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_FAVORITE_PLACES:Lcom/navdy/service/library/events/ui/Screen;

    .line 46
    new-instance v0, Lcom/navdy/service/library/events/ui/Screen;

    const-string v1, "SCREEN_BRIGHTNESS"

    const/16 v2, 0x13

    const/16 v3, 0x14

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/ui/Screen;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_BRIGHTNESS:Lcom/navdy/service/library/events/ui/Screen;

    .line 47
    new-instance v0, Lcom/navdy/service/library/events/ui/Screen;

    const-string v1, "SCREEN_RECOMMENDED_PLACES"

    const/16 v2, 0x14

    const/16 v3, 0x15

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/ui/Screen;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_RECOMMENDED_PLACES:Lcom/navdy/service/library/events/ui/Screen;

    .line 48
    new-instance v0, Lcom/navdy/service/library/events/ui/Screen;

    const-string v1, "SCREEN_REPLY_MESSAGE"

    const/16 v2, 0x15

    const/16 v3, 0x16

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/ui/Screen;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_REPLY_MESSAGE:Lcom/navdy/service/library/events/ui/Screen;

    .line 49
    new-instance v0, Lcom/navdy/service/library/events/ui/Screen;

    const-string v1, "SCREEN_READ_MESSAGE"

    const/16 v2, 0x16

    const/16 v3, 0x17

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/ui/Screen;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_READ_MESSAGE:Lcom/navdy/service/library/events/ui/Screen;

    .line 50
    new-instance v0, Lcom/navdy/service/library/events/ui/Screen;

    const-string v1, "SCREEN_MAP_ANIMATION_MODE"

    const/16 v2, 0x17

    const/16 v3, 0x18

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/ui/Screen;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_MAP_ANIMATION_MODE:Lcom/navdy/service/library/events/ui/Screen;

    .line 51
    new-instance v0, Lcom/navdy/service/library/events/ui/Screen;

    const-string v1, "SCREEN_SHUTDOWN_CONFIRMATION"

    const/16 v2, 0x18

    const/16 v3, 0x19

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/ui/Screen;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_SHUTDOWN_CONFIRMATION:Lcom/navdy/service/library/events/ui/Screen;

    .line 52
    new-instance v0, Lcom/navdy/service/library/events/ui/Screen;

    const-string v1, "SCREEN_DIAL_PAIRING"

    const/16 v2, 0x19

    const/16 v3, 0x1a

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/ui/Screen;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_DIAL_PAIRING:Lcom/navdy/service/library/events/ui/Screen;

    .line 53
    new-instance v0, Lcom/navdy/service/library/events/ui/Screen;

    const-string v1, "SCREEN_SETTINGS"

    const/16 v2, 0x1a

    const/16 v3, 0x1b

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/ui/Screen;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_SETTINGS:Lcom/navdy/service/library/events/ui/Screen;

    .line 54
    new-instance v0, Lcom/navdy/service/library/events/ui/Screen;

    const-string v1, "SCREEN_AUTO_BRIGHTNESS"

    const/16 v2, 0x1b

    const/16 v3, 0x1c

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/ui/Screen;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_AUTO_BRIGHTNESS:Lcom/navdy/service/library/events/ui/Screen;

    .line 55
    new-instance v0, Lcom/navdy/service/library/events/ui/Screen;

    const-string v1, "SCREEN_FACTORY_RESET"

    const/16 v2, 0x1c

    const/16 v3, 0x1d

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/ui/Screen;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_FACTORY_RESET:Lcom/navdy/service/library/events/ui/Screen;

    .line 59
    new-instance v0, Lcom/navdy/service/library/events/ui/Screen;

    const-string v1, "SCREEN_BLUETOOTH_PAIRING"

    const/16 v2, 0x1d

    const/16 v3, 0x1e

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/ui/Screen;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_BLUETOOTH_PAIRING:Lcom/navdy/service/library/events/ui/Screen;

    .line 60
    new-instance v0, Lcom/navdy/service/library/events/ui/Screen;

    const-string v1, "SCREEN_REPORT_ISSUE"

    const/16 v2, 0x1e

    const/16 v3, 0x1f

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/ui/Screen;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_REPORT_ISSUE:Lcom/navdy/service/library/events/ui/Screen;

    .line 64
    new-instance v0, Lcom/navdy/service/library/events/ui/Screen;

    const-string v1, "SCREEN_HOME"

    const/16 v2, 0x1f

    const/16 v3, 0x20

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/ui/Screen;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_HOME:Lcom/navdy/service/library/events/ui/Screen;

    .line 68
    new-instance v0, Lcom/navdy/service/library/events/ui/Screen;

    const-string v1, "SCREEN_TOAST"

    const/16 v2, 0x20

    const/16 v3, 0x21

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/ui/Screen;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_TOAST:Lcom/navdy/service/library/events/ui/Screen;

    .line 72
    new-instance v0, Lcom/navdy/service/library/events/ui/Screen;

    const-string v1, "SCREEN_DIAL_UPDATE_PROGRESS"

    const/16 v2, 0x21

    const/16 v3, 0x22

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/ui/Screen;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_DIAL_UPDATE_PROGRESS:Lcom/navdy/service/library/events/ui/Screen;

    .line 76
    new-instance v0, Lcom/navdy/service/library/events/ui/Screen;

    const-string v1, "SCREEN_FIRST_LAUNCH"

    const/16 v2, 0x22

    const/16 v3, 0x23

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/ui/Screen;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_FIRST_LAUNCH:Lcom/navdy/service/library/events/ui/Screen;

    .line 80
    new-instance v0, Lcom/navdy/service/library/events/ui/Screen;

    const-string v1, "SCREEN_FORCE_UPDATE"

    const/16 v2, 0x23

    const/16 v3, 0x24

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/ui/Screen;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_FORCE_UPDATE:Lcom/navdy/service/library/events/ui/Screen;

    .line 84
    new-instance v0, Lcom/navdy/service/library/events/ui/Screen;

    const-string v1, "SCREEN_TEMPERATURE_WARNING"

    const/16 v2, 0x24

    const/16 v3, 0x25

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/ui/Screen;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_TEMPERATURE_WARNING:Lcom/navdy/service/library/events/ui/Screen;

    .line 88
    new-instance v0, Lcom/navdy/service/library/events/ui/Screen;

    const-string v1, "SCREEN_DIAL_UPDATE_CONFIRMATION"

    const/16 v2, 0x25

    const/16 v3, 0x26

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/ui/Screen;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_DIAL_UPDATE_CONFIRMATION:Lcom/navdy/service/library/events/ui/Screen;

    .line 92
    new-instance v0, Lcom/navdy/service/library/events/ui/Screen;

    const-string v1, "SCREEN_GESTURE_LEARNING"

    const/16 v2, 0x26

    const/16 v3, 0x27

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/ui/Screen;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_GESTURE_LEARNING:Lcom/navdy/service/library/events/ui/Screen;

    .line 96
    new-instance v0, Lcom/navdy/service/library/events/ui/Screen;

    const-string v1, "SCREEN_SYSTEM_INFO"

    const/16 v2, 0x27

    const/16 v3, 0x28

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/ui/Screen;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_SYSTEM_INFO:Lcom/navdy/service/library/events/ui/Screen;

    .line 100
    new-instance v0, Lcom/navdy/service/library/events/ui/Screen;

    const-string v1, "SCREEN_MAIN_MENU"

    const/16 v2, 0x28

    const/16 v3, 0x29

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/ui/Screen;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_MAIN_MENU:Lcom/navdy/service/library/events/ui/Screen;

    .line 104
    new-instance v0, Lcom/navdy/service/library/events/ui/Screen;

    const-string v1, "SCREEN_DESTINATION_PICKER"

    const/16 v2, 0x29

    const/16 v3, 0x2a

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/ui/Screen;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_DESTINATION_PICKER:Lcom/navdy/service/library/events/ui/Screen;

    .line 108
    new-instance v0, Lcom/navdy/service/library/events/ui/Screen;

    const-string v1, "SCREEN_MUSIC_DETAILS"

    const/16 v2, 0x2a

    const/16 v3, 0x2b

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/ui/Screen;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_MUSIC_DETAILS:Lcom/navdy/service/library/events/ui/Screen;

    .line 7
    const/16 v0, 0x2b

    new-array v0, v0, [Lcom/navdy/service/library/events/ui/Screen;

    const/4 v1, 0x0

    sget-object v2, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_DASHBOARD:Lcom/navdy/service/library/events/ui/Screen;

    aput-object v2, v0, v1

    sget-object v1, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_TURN_BY_TURN:Lcom/navdy/service/library/events/ui/Screen;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_HYBRID_MAP:Lcom/navdy/service/library/events/ui/Screen;

    aput-object v1, v0, v5

    sget-object v1, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_MEDIA_BROWSER:Lcom/navdy/service/library/events/ui/Screen;

    aput-object v1, v0, v6

    sget-object v1, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_MENU:Lcom/navdy/service/library/events/ui/Screen;

    aput-object v1, v0, v7

    sget-object v1, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_NOTIFICATION:Lcom/navdy/service/library/events/ui/Screen;

    aput-object v1, v0, v8

    const/4 v1, 0x6

    sget-object v2, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_CALL_CONTROL:Lcom/navdy/service/library/events/ui/Screen;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_MUSIC:Lcom/navdy/service/library/events/ui/Screen;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_VOICE_CONTROL:Lcom/navdy/service/library/events/ui/Screen;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_RECENT_CALLS:Lcom/navdy/service/library/events/ui/Screen;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_BACK:Lcom/navdy/service/library/events/ui/Screen;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_CONTEXT_MENU:Lcom/navdy/service/library/events/ui/Screen;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_CAROUSEL_MENU:Lcom/navdy/service/library/events/ui/Screen;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_OPTIONS:Lcom/navdy/service/library/events/ui/Screen;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_PLACES:Lcom/navdy/service/library/events/ui/Screen;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_OTA_CONFIRMATION:Lcom/navdy/service/library/events/ui/Screen;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_WELCOME:Lcom/navdy/service/library/events/ui/Screen;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_FAVORITE_CONTACTS:Lcom/navdy/service/library/events/ui/Screen;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_FAVORITE_PLACES:Lcom/navdy/service/library/events/ui/Screen;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_BRIGHTNESS:Lcom/navdy/service/library/events/ui/Screen;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_RECOMMENDED_PLACES:Lcom/navdy/service/library/events/ui/Screen;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_REPLY_MESSAGE:Lcom/navdy/service/library/events/ui/Screen;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_READ_MESSAGE:Lcom/navdy/service/library/events/ui/Screen;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_MAP_ANIMATION_MODE:Lcom/navdy/service/library/events/ui/Screen;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_SHUTDOWN_CONFIRMATION:Lcom/navdy/service/library/events/ui/Screen;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_DIAL_PAIRING:Lcom/navdy/service/library/events/ui/Screen;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_SETTINGS:Lcom/navdy/service/library/events/ui/Screen;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_AUTO_BRIGHTNESS:Lcom/navdy/service/library/events/ui/Screen;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_FACTORY_RESET:Lcom/navdy/service/library/events/ui/Screen;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_BLUETOOTH_PAIRING:Lcom/navdy/service/library/events/ui/Screen;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_REPORT_ISSUE:Lcom/navdy/service/library/events/ui/Screen;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_HOME:Lcom/navdy/service/library/events/ui/Screen;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_TOAST:Lcom/navdy/service/library/events/ui/Screen;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_DIAL_UPDATE_PROGRESS:Lcom/navdy/service/library/events/ui/Screen;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_FIRST_LAUNCH:Lcom/navdy/service/library/events/ui/Screen;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_FORCE_UPDATE:Lcom/navdy/service/library/events/ui/Screen;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_TEMPERATURE_WARNING:Lcom/navdy/service/library/events/ui/Screen;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_DIAL_UPDATE_CONFIRMATION:Lcom/navdy/service/library/events/ui/Screen;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_GESTURE_LEARNING:Lcom/navdy/service/library/events/ui/Screen;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_SYSTEM_INFO:Lcom/navdy/service/library/events/ui/Screen;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_MAIN_MENU:Lcom/navdy/service/library/events/ui/Screen;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_DESTINATION_PICKER:Lcom/navdy/service/library/events/ui/Screen;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_MUSIC_DETAILS:Lcom/navdy/service/library/events/ui/Screen;

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/service/library/events/ui/Screen;->$VALUES:[Lcom/navdy/service/library/events/ui/Screen;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 112
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 113
    iput p3, p0, Lcom/navdy/service/library/events/ui/Screen;->value:I

    .line 114
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/service/library/events/ui/Screen;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 7
    const-class v0, Lcom/navdy/service/library/events/ui/Screen;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/ui/Screen;

    return-object v0
.end method

.method public static values()[Lcom/navdy/service/library/events/ui/Screen;
    .locals 1

    .prologue
    .line 7
    sget-object v0, Lcom/navdy/service/library/events/ui/Screen;->$VALUES:[Lcom/navdy/service/library/events/ui/Screen;

    invoke-virtual {v0}, [Lcom/navdy/service/library/events/ui/Screen;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/service/library/events/ui/Screen;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 118
    iget v0, p0, Lcom/navdy/service/library/events/ui/Screen;->value:I

    return v0
.end method
