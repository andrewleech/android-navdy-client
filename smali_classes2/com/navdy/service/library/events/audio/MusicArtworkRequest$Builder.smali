.class public final Lcom/navdy/service/library/events/audio/MusicArtworkRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "MusicArtworkRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/audio/MusicArtworkRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/audio/MusicArtworkRequest;",
        ">;"
    }
.end annotation


# instance fields
.field public album:Ljava/lang/String;

.field public author:Ljava/lang/String;

.field public collectionId:Ljava/lang/String;

.field public collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

.field public collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

.field public name:Ljava/lang/String;

.field public size:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 108
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 109
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/audio/MusicArtworkRequest;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/audio/MusicArtworkRequest;

    .prologue
    .line 112
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 113
    if-nez p1, :cond_0

    .line 121
    :goto_0
    return-void

    .line 114
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/MusicArtworkRequest;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/MusicArtworkRequest$Builder;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    .line 115
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/MusicArtworkRequest;->name:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/MusicArtworkRequest$Builder;->name:Ljava/lang/String;

    .line 116
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/MusicArtworkRequest;->album:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/MusicArtworkRequest$Builder;->album:Ljava/lang/String;

    .line 117
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/MusicArtworkRequest;->author:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/MusicArtworkRequest$Builder;->author:Ljava/lang/String;

    .line 118
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/MusicArtworkRequest;->size:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/MusicArtworkRequest$Builder;->size:Ljava/lang/Integer;

    .line 119
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/MusicArtworkRequest;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/MusicArtworkRequest$Builder;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    .line 120
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/MusicArtworkRequest;->collectionId:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/MusicArtworkRequest$Builder;->collectionId:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public album(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/MusicArtworkRequest$Builder;
    .locals 0
    .param p1, "album"    # Ljava/lang/String;

    .prologue
    .line 140
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/MusicArtworkRequest$Builder;->album:Ljava/lang/String;

    .line 141
    return-object p0
.end method

.method public author(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/MusicArtworkRequest$Builder;
    .locals 0
    .param p1, "author"    # Ljava/lang/String;

    .prologue
    .line 145
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/MusicArtworkRequest$Builder;->author:Ljava/lang/String;

    .line 146
    return-object p0
.end method

.method public build()Lcom/navdy/service/library/events/audio/MusicArtworkRequest;
    .locals 2

    .prologue
    .line 169
    new-instance v0, Lcom/navdy/service/library/events/audio/MusicArtworkRequest;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/audio/MusicArtworkRequest;-><init>(Lcom/navdy/service/library/events/audio/MusicArtworkRequest$Builder;Lcom/navdy/service/library/events/audio/MusicArtworkRequest$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 98
    invoke-virtual {p0}, Lcom/navdy/service/library/events/audio/MusicArtworkRequest$Builder;->build()Lcom/navdy/service/library/events/audio/MusicArtworkRequest;

    move-result-object v0

    return-object v0
.end method

.method public collectionId(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/MusicArtworkRequest$Builder;
    .locals 0
    .param p1, "collectionId"    # Ljava/lang/String;

    .prologue
    .line 163
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/MusicArtworkRequest$Builder;->collectionId:Ljava/lang/String;

    .line 164
    return-object p0
.end method

.method public collectionSource(Lcom/navdy/service/library/events/audio/MusicCollectionSource;)Lcom/navdy/service/library/events/audio/MusicArtworkRequest$Builder;
    .locals 0
    .param p1, "collectionSource"    # Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    .prologue
    .line 127
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/MusicArtworkRequest$Builder;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    .line 128
    return-object p0
.end method

.method public collectionType(Lcom/navdy/service/library/events/audio/MusicCollectionType;)Lcom/navdy/service/library/events/audio/MusicArtworkRequest$Builder;
    .locals 0
    .param p1, "collectionType"    # Lcom/navdy/service/library/events/audio/MusicCollectionType;

    .prologue
    .line 158
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/MusicArtworkRequest$Builder;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    .line 159
    return-object p0
.end method

.method public name(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/MusicArtworkRequest$Builder;
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 135
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/MusicArtworkRequest$Builder;->name:Ljava/lang/String;

    .line 136
    return-object p0
.end method

.method public size(Ljava/lang/Integer;)Lcom/navdy/service/library/events/audio/MusicArtworkRequest$Builder;
    .locals 0
    .param p1, "size"    # Ljava/lang/Integer;

    .prologue
    .line 153
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/MusicArtworkRequest$Builder;->size:Ljava/lang/Integer;

    .line 154
    return-object p0
.end method
