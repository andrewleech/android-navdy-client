.class public final Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "MusicCollectionInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/audio/MusicCollectionInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/audio/MusicCollectionInfo;",
        ">;"
    }
.end annotation


# instance fields
.field public canShuffle:Ljava/lang/Boolean;

.field public collectionId:Ljava/lang/String;

.field public collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

.field public collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

.field public index:Ljava/lang/Integer;

.field public name:Ljava/lang/String;

.field public size:Ljava/lang/Integer;

.field public subtitle:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 131
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 132
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/audio/MusicCollectionInfo;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    .prologue
    .line 135
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 136
    if-nez p1, :cond_0

    .line 145
    :goto_0
    return-void

    .line 137
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    .line 138
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    .line 139
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;->collectionId:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;->collectionId:Ljava/lang/String;

    .line 140
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;->name:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;->name:Ljava/lang/String;

    .line 141
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;->size:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;->size:Ljava/lang/Integer;

    .line 142
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;->index:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;->index:Ljava/lang/Integer;

    .line 143
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;->subtitle:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;->subtitle:Ljava/lang/String;

    .line 144
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;->canShuffle:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;->canShuffle:Ljava/lang/Boolean;

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/navdy/service/library/events/audio/MusicCollectionInfo;
    .locals 2

    .prologue
    .line 212
    new-instance v0, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;-><init>(Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;Lcom/navdy/service/library/events/audio/MusicCollectionInfo$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 120
    invoke-virtual {p0}, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;->build()Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    move-result-object v0

    return-object v0
.end method

.method public canShuffle(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;
    .locals 0
    .param p1, "canShuffle"    # Ljava/lang/Boolean;

    .prologue
    .line 206
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;->canShuffle:Ljava/lang/Boolean;

    .line 207
    return-object p0
.end method

.method public collectionId(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;
    .locals 0
    .param p1, "collectionId"    # Ljava/lang/String;

    .prologue
    .line 170
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;->collectionId:Ljava/lang/String;

    .line 171
    return-object p0
.end method

.method public collectionSource(Lcom/navdy/service/library/events/audio/MusicCollectionSource;)Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;
    .locals 0
    .param p1, "collectionSource"    # Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    .prologue
    .line 152
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    .line 153
    return-object p0
.end method

.method public collectionType(Lcom/navdy/service/library/events/audio/MusicCollectionType;)Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;
    .locals 0
    .param p1, "collectionType"    # Lcom/navdy/service/library/events/audio/MusicCollectionType;

    .prologue
    .line 161
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    .line 162
    return-object p0
.end method

.method public index(Ljava/lang/Integer;)Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;
    .locals 0
    .param p1, "index"    # Ljava/lang/Integer;

    .prologue
    .line 188
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;->index:Ljava/lang/Integer;

    .line 189
    return-object p0
.end method

.method public name(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 175
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;->name:Ljava/lang/String;

    .line 176
    return-object p0
.end method

.method public size(Ljava/lang/Integer;)Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;
    .locals 0
    .param p1, "size"    # Ljava/lang/Integer;

    .prologue
    .line 180
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;->size:Ljava/lang/Integer;

    .line 181
    return-object p0
.end method

.method public subtitle(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;
    .locals 0
    .param p1, "subtitle"    # Ljava/lang/String;

    .prologue
    .line 196
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;->subtitle:Ljava/lang/String;

    .line 197
    return-object p0
.end method
