.class public final enum Lcom/navdy/service/library/events/audio/AudioStatus$ProfileType;
.super Ljava/lang/Enum;
.source "AudioStatus.java"

# interfaces
.implements Lcom/squareup/wire/ProtoEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/audio/AudioStatus;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ProfileType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/service/library/events/audio/AudioStatus$ProfileType;",
        ">;",
        "Lcom/squareup/wire/ProtoEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/service/library/events/audio/AudioStatus$ProfileType;

.field public static final enum AUDIO_PROFILE_A2DP:Lcom/navdy/service/library/events/audio/AudioStatus$ProfileType;

.field public static final enum AUDIO_PROFILE_HFP:Lcom/navdy/service/library/events/audio/AudioStatus$ProfileType;

.field public static final enum AUDIO_PROFILE_NONE:Lcom/navdy/service/library/events/audio/AudioStatus$ProfileType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x0

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 178
    new-instance v0, Lcom/navdy/service/library/events/audio/AudioStatus$ProfileType;

    const-string v1, "AUDIO_PROFILE_NONE"

    invoke-direct {v0, v1, v4, v2}, Lcom/navdy/service/library/events/audio/AudioStatus$ProfileType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/audio/AudioStatus$ProfileType;->AUDIO_PROFILE_NONE:Lcom/navdy/service/library/events/audio/AudioStatus$ProfileType;

    .line 179
    new-instance v0, Lcom/navdy/service/library/events/audio/AudioStatus$ProfileType;

    const-string v1, "AUDIO_PROFILE_A2DP"

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/audio/AudioStatus$ProfileType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/audio/AudioStatus$ProfileType;->AUDIO_PROFILE_A2DP:Lcom/navdy/service/library/events/audio/AudioStatus$ProfileType;

    .line 180
    new-instance v0, Lcom/navdy/service/library/events/audio/AudioStatus$ProfileType;

    const-string v1, "AUDIO_PROFILE_HFP"

    invoke-direct {v0, v1, v3, v5}, Lcom/navdy/service/library/events/audio/AudioStatus$ProfileType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/audio/AudioStatus$ProfileType;->AUDIO_PROFILE_HFP:Lcom/navdy/service/library/events/audio/AudioStatus$ProfileType;

    .line 176
    new-array v0, v5, [Lcom/navdy/service/library/events/audio/AudioStatus$ProfileType;

    sget-object v1, Lcom/navdy/service/library/events/audio/AudioStatus$ProfileType;->AUDIO_PROFILE_NONE:Lcom/navdy/service/library/events/audio/AudioStatus$ProfileType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/service/library/events/audio/AudioStatus$ProfileType;->AUDIO_PROFILE_A2DP:Lcom/navdy/service/library/events/audio/AudioStatus$ProfileType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/service/library/events/audio/AudioStatus$ProfileType;->AUDIO_PROFILE_HFP:Lcom/navdy/service/library/events/audio/AudioStatus$ProfileType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/navdy/service/library/events/audio/AudioStatus$ProfileType;->$VALUES:[Lcom/navdy/service/library/events/audio/AudioStatus$ProfileType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 184
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 185
    iput p3, p0, Lcom/navdy/service/library/events/audio/AudioStatus$ProfileType;->value:I

    .line 186
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/AudioStatus$ProfileType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 176
    const-class v0, Lcom/navdy/service/library/events/audio/AudioStatus$ProfileType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/audio/AudioStatus$ProfileType;

    return-object v0
.end method

.method public static values()[Lcom/navdy/service/library/events/audio/AudioStatus$ProfileType;
    .locals 1

    .prologue
    .line 176
    sget-object v0, Lcom/navdy/service/library/events/audio/AudioStatus$ProfileType;->$VALUES:[Lcom/navdy/service/library/events/audio/AudioStatus$ProfileType;

    invoke-virtual {v0}, [Lcom/navdy/service/library/events/audio/AudioStatus$ProfileType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/service/library/events/audio/AudioStatus$ProfileType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 190
    iget v0, p0, Lcom/navdy/service/library/events/audio/AudioStatus$ProfileType;->value:I

    return v0
.end method
