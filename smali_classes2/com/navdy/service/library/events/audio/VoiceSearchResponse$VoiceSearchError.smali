.class public final enum Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;
.super Ljava/lang/Enum;
.source "VoiceSearchResponse.java"

# interfaces
.implements Lcom/squareup/wire/ProtoEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/audio/VoiceSearchResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "VoiceSearchError"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;",
        ">;",
        "Lcom/squareup/wire/ProtoEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

.field public static final enum AMBIENT_NOISE:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

.field public static final enum FAILED_TO_START:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

.field public static final enum NEED_PERMISSION:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

.field public static final enum NOT_AVAILABLE:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

.field public static final enum NO_RESULTS_FOUND:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

.field public static final enum NO_WORDS_RECOGNIZED:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

.field public static final enum OFFLINE:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

.field public static final enum VOICE_SEARCH_ERROR_CARPLAY_BLACKLIST:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 276
    new-instance v0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

    const-string v1, "NOT_AVAILABLE"

    invoke-direct {v0, v1, v4, v4}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;->NOT_AVAILABLE:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

    .line 280
    new-instance v0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

    const-string v1, "NEED_PERMISSION"

    invoke-direct {v0, v1, v5, v5}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;->NEED_PERMISSION:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

    .line 284
    new-instance v0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

    const-string v1, "OFFLINE"

    invoke-direct {v0, v1, v6, v6}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;->OFFLINE:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

    .line 288
    new-instance v0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

    const-string v1, "FAILED_TO_START"

    invoke-direct {v0, v1, v7, v7}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;->FAILED_TO_START:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

    .line 292
    new-instance v0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

    const-string v1, "NO_WORDS_RECOGNIZED"

    invoke-direct {v0, v1, v8, v8}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;->NO_WORDS_RECOGNIZED:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

    .line 296
    new-instance v0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

    const-string v1, "NO_RESULTS_FOUND"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;->NO_RESULTS_FOUND:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

    .line 300
    new-instance v0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

    const-string v1, "AMBIENT_NOISE"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;->AMBIENT_NOISE:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

    .line 304
    new-instance v0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

    const-string v1, "VOICE_SEARCH_ERROR_CARPLAY_BLACKLIST"

    const/4 v2, 0x7

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;->VOICE_SEARCH_ERROR_CARPLAY_BLACKLIST:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

    .line 274
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

    sget-object v1, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;->NOT_AVAILABLE:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;->NEED_PERMISSION:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

    aput-object v1, v0, v5

    sget-object v1, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;->OFFLINE:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

    aput-object v1, v0, v6

    sget-object v1, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;->FAILED_TO_START:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

    aput-object v1, v0, v7

    sget-object v1, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;->NO_WORDS_RECOGNIZED:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;->NO_RESULTS_FOUND:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;->AMBIENT_NOISE:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;->VOICE_SEARCH_ERROR_CARPLAY_BLACKLIST:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;->$VALUES:[Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 308
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 309
    iput p3, p0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;->value:I

    .line 310
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 274
    const-class v0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

    return-object v0
.end method

.method public static values()[Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;
    .locals 1

    .prologue
    .line 274
    sget-object v0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;->$VALUES:[Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

    invoke-virtual {v0}, [Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 314
    iget v0, p0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;->value:I

    return v0
.end method
