.class public final Lcom/navdy/service/library/events/destination/RecommendedDestinationsUpdate$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "RecommendedDestinationsUpdate.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/destination/RecommendedDestinationsUpdate;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/destination/RecommendedDestinationsUpdate;",
        ">;"
    }
.end annotation


# instance fields
.field public destinations:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/destination/Destination;",
            ">;"
        }
    .end annotation
.end field

.field public serial_number:Ljava/lang/Long;

.field public status:Lcom/navdy/service/library/events/RequestStatus;

.field public statusDetail:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 97
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 98
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/destination/RecommendedDestinationsUpdate;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/destination/RecommendedDestinationsUpdate;

    .prologue
    .line 101
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 102
    if-nez p1, :cond_0

    .line 107
    :goto_0
    return-void

    .line 103
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/destination/RecommendedDestinationsUpdate;->status:Lcom/navdy/service/library/events/RequestStatus;

    iput-object v0, p0, Lcom/navdy/service/library/events/destination/RecommendedDestinationsUpdate$Builder;->status:Lcom/navdy/service/library/events/RequestStatus;

    .line 104
    iget-object v0, p1, Lcom/navdy/service/library/events/destination/RecommendedDestinationsUpdate;->statusDetail:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/destination/RecommendedDestinationsUpdate$Builder;->statusDetail:Ljava/lang/String;

    .line 105
    iget-object v0, p1, Lcom/navdy/service/library/events/destination/RecommendedDestinationsUpdate;->serial_number:Ljava/lang/Long;

    iput-object v0, p0, Lcom/navdy/service/library/events/destination/RecommendedDestinationsUpdate$Builder;->serial_number:Ljava/lang/Long;

    .line 106
    iget-object v0, p1, Lcom/navdy/service/library/events/destination/RecommendedDestinationsUpdate;->destinations:Ljava/util/List;

    invoke-static {v0}, Lcom/navdy/service/library/events/destination/RecommendedDestinationsUpdate;->access$000(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/destination/RecommendedDestinationsUpdate$Builder;->destinations:Ljava/util/List;

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/navdy/service/library/events/destination/RecommendedDestinationsUpdate;
    .locals 2

    .prologue
    .line 145
    invoke-virtual {p0}, Lcom/navdy/service/library/events/destination/RecommendedDestinationsUpdate$Builder;->checkRequiredFields()V

    .line 146
    new-instance v0, Lcom/navdy/service/library/events/destination/RecommendedDestinationsUpdate;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/destination/RecommendedDestinationsUpdate;-><init>(Lcom/navdy/service/library/events/destination/RecommendedDestinationsUpdate$Builder;Lcom/navdy/service/library/events/destination/RecommendedDestinationsUpdate$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 90
    invoke-virtual {p0}, Lcom/navdy/service/library/events/destination/RecommendedDestinationsUpdate$Builder;->build()Lcom/navdy/service/library/events/destination/RecommendedDestinationsUpdate;

    move-result-object v0

    return-object v0
.end method

.method public destinations(Ljava/util/List;)Lcom/navdy/service/library/events/destination/RecommendedDestinationsUpdate$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/destination/Destination;",
            ">;)",
            "Lcom/navdy/service/library/events/destination/RecommendedDestinationsUpdate$Builder;"
        }
    .end annotation

    .prologue
    .line 139
    .local p1, "destinations":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/destination/Destination;>;"
    invoke-static {p1}, Lcom/navdy/service/library/events/destination/RecommendedDestinationsUpdate$Builder;->checkForNulls(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/destination/RecommendedDestinationsUpdate$Builder;->destinations:Ljava/util/List;

    .line 140
    return-object p0
.end method

.method public serial_number(Ljava/lang/Long;)Lcom/navdy/service/library/events/destination/RecommendedDestinationsUpdate$Builder;
    .locals 0
    .param p1, "serial_number"    # Ljava/lang/Long;

    .prologue
    .line 129
    iput-object p1, p0, Lcom/navdy/service/library/events/destination/RecommendedDestinationsUpdate$Builder;->serial_number:Ljava/lang/Long;

    .line 130
    return-object p0
.end method

.method public status(Lcom/navdy/service/library/events/RequestStatus;)Lcom/navdy/service/library/events/destination/RecommendedDestinationsUpdate$Builder;
    .locals 0
    .param p1, "status"    # Lcom/navdy/service/library/events/RequestStatus;

    .prologue
    .line 113
    iput-object p1, p0, Lcom/navdy/service/library/events/destination/RecommendedDestinationsUpdate$Builder;->status:Lcom/navdy/service/library/events/RequestStatus;

    .line 114
    return-object p0
.end method

.method public statusDetail(Ljava/lang/String;)Lcom/navdy/service/library/events/destination/RecommendedDestinationsUpdate$Builder;
    .locals 0
    .param p1, "statusDetail"    # Ljava/lang/String;

    .prologue
    .line 121
    iput-object p1, p0, Lcom/navdy/service/library/events/destination/RecommendedDestinationsUpdate$Builder;->statusDetail:Ljava/lang/String;

    .line 122
    return-object p0
.end method
