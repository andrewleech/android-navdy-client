.class public final Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "PhoneBatteryStatus.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus;",
        ">;"
    }
.end annotation


# instance fields
.field public charging:Ljava/lang/Boolean;

.field public level:Ljava/lang/Integer;

.field public status:Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 78
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 79
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus;

    .prologue
    .line 82
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 83
    if-nez p1, :cond_0

    .line 87
    :goto_0
    return-void

    .line 84
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus;->status:Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;

    iput-object v0, p0, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$Builder;->status:Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;

    .line 85
    iget-object v0, p1, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus;->level:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$Builder;->level:Ljava/lang/Integer;

    .line 86
    iget-object v0, p1, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus;->charging:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$Builder;->charging:Ljava/lang/Boolean;

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus;
    .locals 2

    .prologue
    .line 115
    invoke-virtual {p0}, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$Builder;->checkRequiredFields()V

    .line 116
    new-instance v0, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus;-><init>(Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$Builder;Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 72
    invoke-virtual {p0}, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$Builder;->build()Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus;

    move-result-object v0

    return-object v0
.end method

.method public charging(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$Builder;
    .locals 0
    .param p1, "charging"    # Ljava/lang/Boolean;

    .prologue
    .line 109
    iput-object p1, p0, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$Builder;->charging:Ljava/lang/Boolean;

    .line 110
    return-object p0
.end method

.method public level(Ljava/lang/Integer;)Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$Builder;
    .locals 0
    .param p1, "level"    # Ljava/lang/Integer;

    .prologue
    .line 101
    iput-object p1, p0, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$Builder;->level:Ljava/lang/Integer;

    .line 102
    return-object p0
.end method

.method public status(Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;)Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$Builder;
    .locals 0
    .param p1, "status"    # Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;

    .prologue
    .line 93
    iput-object p1, p0, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$Builder;->status:Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;

    .line 94
    return-object p0
.end method
