.class public final Lcom/navdy/service/library/events/callcontrol/PhoneStatusResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "PhoneStatusResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/callcontrol/PhoneStatusResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/callcontrol/PhoneStatusResponse;",
        ">;"
    }
.end annotation


# instance fields
.field public callStatus:Lcom/navdy/service/library/events/callcontrol/PhoneEvent;

.field public status:Lcom/navdy/service/library/events/RequestStatus;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 65
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/callcontrol/PhoneStatusResponse;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/callcontrol/PhoneStatusResponse;

    .prologue
    .line 68
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 69
    if-nez p1, :cond_0

    .line 72
    :goto_0
    return-void

    .line 70
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/callcontrol/PhoneStatusResponse;->status:Lcom/navdy/service/library/events/RequestStatus;

    iput-object v0, p0, Lcom/navdy/service/library/events/callcontrol/PhoneStatusResponse$Builder;->status:Lcom/navdy/service/library/events/RequestStatus;

    .line 71
    iget-object v0, p1, Lcom/navdy/service/library/events/callcontrol/PhoneStatusResponse;->callStatus:Lcom/navdy/service/library/events/callcontrol/PhoneEvent;

    iput-object v0, p0, Lcom/navdy/service/library/events/callcontrol/PhoneStatusResponse$Builder;->callStatus:Lcom/navdy/service/library/events/callcontrol/PhoneEvent;

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/navdy/service/library/events/callcontrol/PhoneStatusResponse;
    .locals 2

    .prologue
    .line 92
    invoke-virtual {p0}, Lcom/navdy/service/library/events/callcontrol/PhoneStatusResponse$Builder;->checkRequiredFields()V

    .line 93
    new-instance v0, Lcom/navdy/service/library/events/callcontrol/PhoneStatusResponse;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/callcontrol/PhoneStatusResponse;-><init>(Lcom/navdy/service/library/events/callcontrol/PhoneStatusResponse$Builder;Lcom/navdy/service/library/events/callcontrol/PhoneStatusResponse$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 59
    invoke-virtual {p0}, Lcom/navdy/service/library/events/callcontrol/PhoneStatusResponse$Builder;->build()Lcom/navdy/service/library/events/callcontrol/PhoneStatusResponse;

    move-result-object v0

    return-object v0
.end method

.method public callStatus(Lcom/navdy/service/library/events/callcontrol/PhoneEvent;)Lcom/navdy/service/library/events/callcontrol/PhoneStatusResponse$Builder;
    .locals 0
    .param p1, "callStatus"    # Lcom/navdy/service/library/events/callcontrol/PhoneEvent;

    .prologue
    .line 86
    iput-object p1, p0, Lcom/navdy/service/library/events/callcontrol/PhoneStatusResponse$Builder;->callStatus:Lcom/navdy/service/library/events/callcontrol/PhoneEvent;

    .line 87
    return-object p0
.end method

.method public status(Lcom/navdy/service/library/events/RequestStatus;)Lcom/navdy/service/library/events/callcontrol/PhoneStatusResponse$Builder;
    .locals 0
    .param p1, "status"    # Lcom/navdy/service/library/events/RequestStatus;

    .prologue
    .line 78
    iput-object p1, p0, Lcom/navdy/service/library/events/callcontrol/PhoneStatusResponse$Builder;->status:Lcom/navdy/service/library/events/RequestStatus;

    .line 79
    return-object p0
.end method
