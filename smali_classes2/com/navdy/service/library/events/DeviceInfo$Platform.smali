.class public final enum Lcom/navdy/service/library/events/DeviceInfo$Platform;
.super Ljava/lang/Enum;
.source "DeviceInfo.java"

# interfaces
.implements Lcom/squareup/wire/ProtoEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/DeviceInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Platform"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/service/library/events/DeviceInfo$Platform;",
        ">;",
        "Lcom/squareup/wire/ProtoEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/service/library/events/DeviceInfo$Platform;

.field public static final enum PLATFORM_Android:Lcom/navdy/service/library/events/DeviceInfo$Platform;

.field public static final enum PLATFORM_iOS:Lcom/navdy/service/library/events/DeviceInfo$Platform;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 423
    new-instance v0, Lcom/navdy/service/library/events/DeviceInfo$Platform;

    const-string v1, "PLATFORM_iOS"

    invoke-direct {v0, v1, v3, v2}, Lcom/navdy/service/library/events/DeviceInfo$Platform;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/DeviceInfo$Platform;->PLATFORM_iOS:Lcom/navdy/service/library/events/DeviceInfo$Platform;

    .line 424
    new-instance v0, Lcom/navdy/service/library/events/DeviceInfo$Platform;

    const-string v1, "PLATFORM_Android"

    invoke-direct {v0, v1, v2, v4}, Lcom/navdy/service/library/events/DeviceInfo$Platform;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/DeviceInfo$Platform;->PLATFORM_Android:Lcom/navdy/service/library/events/DeviceInfo$Platform;

    .line 421
    new-array v0, v4, [Lcom/navdy/service/library/events/DeviceInfo$Platform;

    sget-object v1, Lcom/navdy/service/library/events/DeviceInfo$Platform;->PLATFORM_iOS:Lcom/navdy/service/library/events/DeviceInfo$Platform;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/service/library/events/DeviceInfo$Platform;->PLATFORM_Android:Lcom/navdy/service/library/events/DeviceInfo$Platform;

    aput-object v1, v0, v2

    sput-object v0, Lcom/navdy/service/library/events/DeviceInfo$Platform;->$VALUES:[Lcom/navdy/service/library/events/DeviceInfo$Platform;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 428
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 429
    iput p3, p0, Lcom/navdy/service/library/events/DeviceInfo$Platform;->value:I

    .line 430
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/service/library/events/DeviceInfo$Platform;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 421
    const-class v0, Lcom/navdy/service/library/events/DeviceInfo$Platform;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/DeviceInfo$Platform;

    return-object v0
.end method

.method public static values()[Lcom/navdy/service/library/events/DeviceInfo$Platform;
    .locals 1

    .prologue
    .line 421
    sget-object v0, Lcom/navdy/service/library/events/DeviceInfo$Platform;->$VALUES:[Lcom/navdy/service/library/events/DeviceInfo$Platform;

    invoke-virtual {v0}, [Lcom/navdy/service/library/events/DeviceInfo$Platform;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/service/library/events/DeviceInfo$Platform;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 434
    iget v0, p0, Lcom/navdy/service/library/events/DeviceInfo$Platform;->value:I

    return v0
.end method
