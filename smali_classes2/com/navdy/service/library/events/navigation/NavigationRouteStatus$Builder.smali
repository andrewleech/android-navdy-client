.class public final Lcom/navdy/service/library/events/navigation/NavigationRouteStatus$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "NavigationRouteStatus.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/navigation/NavigationRouteStatus;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/navigation/NavigationRouteStatus;",
        ">;"
    }
.end annotation


# instance fields
.field public handle:Ljava/lang/String;

.field public progress:Ljava/lang/Integer;

.field public requestId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 75
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 76
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/navigation/NavigationRouteStatus;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/navigation/NavigationRouteStatus;

    .prologue
    .line 79
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 80
    if-nez p1, :cond_0

    .line 84
    :goto_0
    return-void

    .line 81
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteStatus;->handle:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteStatus$Builder;->handle:Ljava/lang/String;

    .line 82
    iget-object v0, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteStatus;->progress:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteStatus$Builder;->progress:Ljava/lang/Integer;

    .line 83
    iget-object v0, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteStatus;->requestId:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteStatus$Builder;->requestId:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/navdy/service/library/events/navigation/NavigationRouteStatus;
    .locals 2

    .prologue
    .line 112
    new-instance v0, Lcom/navdy/service/library/events/navigation/NavigationRouteStatus;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/navigation/NavigationRouteStatus;-><init>(Lcom/navdy/service/library/events/navigation/NavigationRouteStatus$Builder;Lcom/navdy/service/library/events/navigation/NavigationRouteStatus$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 69
    invoke-virtual {p0}, Lcom/navdy/service/library/events/navigation/NavigationRouteStatus$Builder;->build()Lcom/navdy/service/library/events/navigation/NavigationRouteStatus;

    move-result-object v0

    return-object v0
.end method

.method public handle(Ljava/lang/String;)Lcom/navdy/service/library/events/navigation/NavigationRouteStatus$Builder;
    .locals 0
    .param p1, "handle"    # Ljava/lang/String;

    .prologue
    .line 90
    iput-object p1, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteStatus$Builder;->handle:Ljava/lang/String;

    .line 91
    return-object p0
.end method

.method public progress(Ljava/lang/Integer;)Lcom/navdy/service/library/events/navigation/NavigationRouteStatus$Builder;
    .locals 0
    .param p1, "progress"    # Ljava/lang/Integer;

    .prologue
    .line 98
    iput-object p1, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteStatus$Builder;->progress:Ljava/lang/Integer;

    .line 99
    return-object p0
.end method

.method public requestId(Ljava/lang/String;)Lcom/navdy/service/library/events/navigation/NavigationRouteStatus$Builder;
    .locals 0
    .param p1, "requestId"    # Ljava/lang/String;

    .prologue
    .line 106
    iput-object p1, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteStatus$Builder;->requestId:Ljava/lang/String;

    .line 107
    return-object p0
.end method
