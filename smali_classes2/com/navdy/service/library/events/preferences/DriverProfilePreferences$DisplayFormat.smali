.class public final enum Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DisplayFormat;
.super Ljava/lang/Enum;
.source "DriverProfilePreferences.java"

# interfaces
.implements Lcom/squareup/wire/ProtoEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "DisplayFormat"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DisplayFormat;",
        ">;",
        "Lcom/squareup/wire/ProtoEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DisplayFormat;

.field public static final enum DISPLAY_FORMAT_COMPACT:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DisplayFormat;

.field public static final enum DISPLAY_FORMAT_NORMAL:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DisplayFormat;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 485
    new-instance v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DisplayFormat;

    const-string v1, "DISPLAY_FORMAT_NORMAL"

    invoke-direct {v0, v1, v2, v2}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DisplayFormat;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DisplayFormat;->DISPLAY_FORMAT_NORMAL:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DisplayFormat;

    .line 486
    new-instance v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DisplayFormat;

    const-string v1, "DISPLAY_FORMAT_COMPACT"

    invoke-direct {v0, v1, v3, v3}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DisplayFormat;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DisplayFormat;->DISPLAY_FORMAT_COMPACT:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DisplayFormat;

    .line 483
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DisplayFormat;

    sget-object v1, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DisplayFormat;->DISPLAY_FORMAT_NORMAL:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DisplayFormat;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DisplayFormat;->DISPLAY_FORMAT_COMPACT:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DisplayFormat;

    aput-object v1, v0, v3

    sput-object v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DisplayFormat;->$VALUES:[Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DisplayFormat;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 490
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 491
    iput p3, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DisplayFormat;->value:I

    .line 492
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DisplayFormat;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 483
    const-class v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DisplayFormat;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DisplayFormat;

    return-object v0
.end method

.method public static values()[Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DisplayFormat;
    .locals 1

    .prologue
    .line 483
    sget-object v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DisplayFormat;->$VALUES:[Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DisplayFormat;

    invoke-virtual {v0}, [Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DisplayFormat;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DisplayFormat;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 496
    iget v0, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DisplayFormat;->value:I

    return v0
.end method
