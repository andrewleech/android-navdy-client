.class public final Lcom/navdy/client/R$array;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "array"
.end annotation


# static fields
.field public static final default_replies_list:I = 0x7f090002

.field public static final end_trip:I = 0x7f09000d

.field public static final exit:I = 0x7f09000e

.field public static final glance_messages:I = 0x7f090003

.field public static final go_back:I = 0x7f09000f

.field public static final i_love_you:I = 0x7f090010

.field public static final jira_projects:I = 0x7f090023

.field public static final maps_compass_directions:I = 0x7f090000

.field public static final maps_full_compass_directions:I = 0x7f090001

.field public static final nav_debug_actions:I = 0x7f090024

.field public static final pref_map_search_provider_entries:I = 0x7f090025

.field public static final pref_map_search_provider_values:I = 0x7f090026

.field public static final settings_audio_sequence_delay_values:I = 0x7f090004

.field public static final settings_audio_speech_levels:I = 0x7f090005

.field public static final settings_contact_us_photo_options:I = 0x7f090006

.field public static final settings_profile_edit_picture:I = 0x7f090007

.field public static final settings_report_a_problem_problem_types:I = 0x7f090008

.field public static final settings_report_a_problem_problem_types_tags:I = 0x7f090027

.field public static final settings_report_a_problem_select_a_topic:I = 0x7f090009

.field public static final show_brightness:I = 0x7f090011

.field public static final show_dash:I = 0x7f090012

.field public static final show_dial_pairing:I = 0x7f090013

.field public static final show_favorite_contacts:I = 0x7f090014

.field public static final show_favorite_places:I = 0x7f090015

.field public static final show_gesture_learning:I = 0x7f090016

.field public static final show_map:I = 0x7f090017

.field public static final show_media_browser:I = 0x7f090018

.field public static final show_menu:I = 0x7f090019

.field public static final show_music:I = 0x7f09001a

.field public static final show_options:I = 0x7f09001b

.field public static final show_places:I = 0x7f09001c

.field public static final show_recent_calls:I = 0x7f09001d

.field public static final show_settings:I = 0x7f09001e

.field public static final show_system_info:I = 0x7f09001f

.field public static final test_audio:I = 0x7f09000a

.field public static final this_is_broken:I = 0x7f090020

.field public static final video_urls:I = 0x7f09000b

.field public static final video_urls_2017:I = 0x7f09000c

.field public static final who_are_you:I = 0x7f090021

.field public static final zip_it:I = 0x7f090022


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
