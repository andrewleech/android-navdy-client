.class Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment$4$1$1;
.super Ljava/lang/Object;
.source "NavAddressPickerFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment$4$1;->onCompleted(Ljava/util/List;Lcom/here/android/mpa/search/ErrorCode;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$2:Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment$4$1;

.field final synthetic val$errorCode:Lcom/here/android/mpa/search/ErrorCode;

.field final synthetic val$locations:Ljava/util/List;


# direct methods
.method constructor <init>(Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment$4$1;Lcom/here/android/mpa/search/ErrorCode;Ljava/util/List;)V
    .locals 0
    .param p1, "this$2"    # Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment$4$1;

    .prologue
    .line 426
    iput-object p1, p0, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment$4$1$1;->this$2:Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment$4$1;

    iput-object p2, p0, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment$4$1$1;->val$errorCode:Lcom/here/android/mpa/search/ErrorCode;

    iput-object p3, p0, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment$4$1$1;->val$locations:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 429
    iget-object v1, p0, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment$4$1$1;->val$errorCode:Lcom/here/android/mpa/search/ErrorCode;

    sget-object v2, Lcom/here/android/mpa/search/ErrorCode;->NONE:Lcom/here/android/mpa/search/ErrorCode;

    if-eq v1, v2, :cond_0

    .line 430
    iget-object v1, p0, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment$4$1$1;->this$2:Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment$4$1;

    iget-object v1, v1, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment$4$1;->this$1:Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment$4;

    iget-object v1, v1, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment$4;->this$0:Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Search error: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment$4$1$1;->val$errorCode:Lcom/here/android/mpa/search/ErrorCode;

    invoke-virtual {v3}, Lcom/here/android/mpa/search/ErrorCode;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->showError(Ljava/lang/String;)V

    .line 442
    :goto_0
    return-void

    .line 434
    :cond_0
    iget-object v1, p0, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment$4$1$1;->val$locations:Ljava/util/List;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment$4$1$1;->val$locations:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_1

    .line 435
    sget-object v1, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "returned results: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment$4$1$1;->val$locations:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 436
    iget-object v1, p0, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment$4$1$1;->this$2:Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment$4$1;

    iget-object v1, v1, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment$4$1;->this$1:Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment$4;

    iget-object v1, v1, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment$4;->this$0:Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;

    iget-object v2, p0, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment$4$1$1;->val$locations:Ljava/util/List;

    invoke-virtual {v1, v2}, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->addDestinations(Ljava/util/List;)V

    goto :goto_0

    .line 438
    :cond_1
    const-string v0, "No results"

    .line 439
    .local v0, "str":Ljava/lang/String;
    iget-object v1, p0, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment$4$1$1;->this$2:Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment$4$1;

    iget-object v1, v1, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment$4$1;->this$1:Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment$4;

    iget-object v1, v1, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment$4;->this$0:Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;

    invoke-virtual {v1, v0}, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->showError(Ljava/lang/String;)V

    .line 440
    sget-object v1, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v1, v0}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    goto :goto_0
.end method
