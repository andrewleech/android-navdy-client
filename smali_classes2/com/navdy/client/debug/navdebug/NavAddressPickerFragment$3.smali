.class Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment$3;
.super Ljava/lang/Object;
.source "NavAddressPickerFragment.java"

# interfaces
.implements Lcom/here/android/mpa/common/OnEngineInitListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->doPlaceSearch(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;

.field final synthetic val$queryText:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;

    .prologue
    .line 363
    iput-object p1, p0, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment$3;->this$0:Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;

    iput-object p2, p0, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment$3;->val$queryText:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEngineInitializationCompleted(Lcom/here/android/mpa/common/OnEngineInitListener$Error;)V
    .locals 4
    .param p1, "error"    # Lcom/here/android/mpa/common/OnEngineInitListener$Error;

    .prologue
    .line 366
    iget-object v1, p0, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment$3;->this$0:Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;

    iget-object v1, v1, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->mLastSearchRequest:Lcom/here/android/mpa/search/SearchRequest;

    if-eqz v1, :cond_0

    .line 367
    iget-object v1, p0, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment$3;->this$0:Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;

    iget-object v1, v1, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->mLastSearchRequest:Lcom/here/android/mpa/search/SearchRequest;

    invoke-virtual {v1}, Lcom/here/android/mpa/search/SearchRequest;->cancel()Z

    .line 370
    :cond_0
    iget-object v1, p0, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment$3;->this$0:Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;

    new-instance v2, Lcom/here/android/mpa/search/SearchRequest;

    iget-object v3, p0, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment$3;->val$queryText:Ljava/lang/String;

    invoke-direct {v2, v3}, Lcom/here/android/mpa/search/SearchRequest;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment$3;->this$0:Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;

    iget-object v3, v3, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->mCurrentGeoPosition:Lcom/here/android/mpa/common/GeoPosition;

    invoke-virtual {v3}, Lcom/here/android/mpa/common/GeoPosition;->getCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/here/android/mpa/search/SearchRequest;->setSearchCenter(Lcom/here/android/mpa/common/GeoCoordinate;)Lcom/here/android/mpa/search/SearchRequest;

    move-result-object v2

    iput-object v2, v1, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->mLastSearchRequest:Lcom/here/android/mpa/search/SearchRequest;

    .line 372
    iget-object v1, p0, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment$3;->this$0:Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;

    iget-object v1, v1, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->mLastSearchRequest:Lcom/here/android/mpa/search/SearchRequest;

    if-nez v1, :cond_2

    .line 373
    iget-object v1, p0, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment$3;->this$0:Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;

    const-string v2, "Unable to search."

    invoke-virtual {v1, v2}, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->showError(Ljava/lang/String;)V

    .line 405
    :cond_1
    :goto_0
    return-void

    .line 377
    :cond_2
    iget-object v1, p0, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment$3;->this$0:Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;

    iget-object v1, v1, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->mLastSearchRequest:Lcom/here/android/mpa/search/SearchRequest;

    new-instance v2, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment$3$1;

    invoke-direct {v2, p0}, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment$3$1;-><init>(Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment$3;)V

    invoke-virtual {v1, v2}, Lcom/here/android/mpa/search/SearchRequest;->execute(Lcom/here/android/mpa/search/ResultListener;)Lcom/here/android/mpa/search/ErrorCode;

    move-result-object v0

    .line 402
    .local v0, "errorCode":Lcom/here/android/mpa/search/ErrorCode;
    sget-object v1, Lcom/here/android/mpa/search/ErrorCode;->NONE:Lcom/here/android/mpa/search/ErrorCode;

    if-eq v0, v1, :cond_1

    .line 403
    iget-object v1, p0, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment$3;->this$0:Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Search error: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/here/android/mpa/search/ErrorCode;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->showError(Ljava/lang/String;)V

    goto :goto_0
.end method
