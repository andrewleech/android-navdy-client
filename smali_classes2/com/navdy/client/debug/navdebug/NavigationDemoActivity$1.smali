.class Lcom/navdy/client/debug/navdebug/NavigationDemoActivity$1;
.super Ljava/lang/Object;
.source "NavigationDemoActivity.java"

# interfaces
.implements Lcom/here/android/mpa/common/OnEngineInitListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;


# direct methods
.method constructor <init>(Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;

    .prologue
    .line 153
    iput-object p1, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity$1;->this$0:Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEngineInitializationCompleted(Lcom/here/android/mpa/common/OnEngineInitListener$Error;)V
    .locals 3
    .param p1, "error"    # Lcom/here/android/mpa/common/OnEngineInitListener$Error;

    .prologue
    .line 157
    sget-object v0, Lcom/here/android/mpa/common/OnEngineInitListener$Error;->NONE:Lcom/here/android/mpa/common/OnEngineInitListener$Error;

    if-ne p1, v0, :cond_0

    .line 158
    iget-object v0, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity$1;->this$0:Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;

    invoke-static {v0}, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->access$000(Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;)V

    .line 159
    iget-object v0, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity$1;->this$0:Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;

    iget-object v1, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity$1;->this$0:Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;

    invoke-static {v1}, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->access$100(Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;)Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity$1;->this$0:Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;

    invoke-static {v2}, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->access$200(Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;)Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->access$300(Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/common/GeoCoordinate;)V

    .line 163
    :goto_0
    return-void

    .line 161
    :cond_0
    sget-object v0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cannot initialize map fragment:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/here/android/mpa/common/OnEngineInitListener$Error;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0
.end method
