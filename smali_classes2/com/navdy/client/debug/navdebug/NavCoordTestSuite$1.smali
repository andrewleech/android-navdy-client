.class Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$1;
.super Ljava/lang/Object;
.source "NavCoordTestSuite.java"

# interfaces
.implements Lcom/navdy/client/app/framework/search/GooglePlacesSearch$GoogleSearchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->runTest(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;


# direct methods
.method constructor <init>(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    .prologue
    .line 194
    iput-object p1, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$1;->this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGoogleSearchResult(Ljava/util/List;Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Error;)V
    .locals 19
    .param p2, "queryType"    # Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;
    .param p3, "error"    # Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Error;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;",
            ">;",
            "Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;",
            "Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Error;",
            ")V"
        }
    .end annotation

    .prologue
    .line 200
    .local p1, "googleResults":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;>;"
    sget-object v2, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;->TEXTSEARCH:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;

    move-object/from16 v0, p2

    if-ne v0, v2, :cond_3

    .line 201
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$1;->this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "  \"google_text_search_results\":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$1;->this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    move-object/from16 v0, p1

    invoke-static {v4, v0}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->access$000(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;Ljava/util/List;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ",\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->access$100(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;Ljava/lang/String;)V

    .line 202
    if-eqz p1, :cond_2

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 203
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$1;->this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "  number of results: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->access$200(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;Ljava/lang/String;)V

    .line 204
    invoke-static/range {p1 .. p1}, Lcom/navdy/client/app/framework/search/SearchResults;->getDestinationListFromGoogleTextSearchResults(Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v16

    .line 205
    .local v16, "googleSearchDestinations":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/Destination;>;"
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$1;->this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "  \"google_search_result_destinations\":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$1;->this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    move-object/from16 v0, v16

    invoke-static {v4, v0}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->access$300(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;Ljava/util/List;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ",\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->access$100(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;Ljava/lang/String;)V

    .line 207
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$1;->this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->access$402(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;Lcom/navdy/client/app/framework/models/Destination;)Lcom/navdy/client/app/framework/models/Destination;

    .line 208
    if-eqz v16, :cond_0

    invoke-interface/range {v16 .. v16}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 209
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$1;->this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    const/4 v2, 0x0

    move-object/from16 v0, v16

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/navdy/client/app/framework/models/Destination;

    invoke-static {v3, v2}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->access$402(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;Lcom/navdy/client/app/framework/models/Destination;)Lcom/navdy/client/app/framework/models/Destination;

    .line 211
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$1;->this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "  \"selected_destination\":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$1;->this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    invoke-static {v4}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->access$400(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;)Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v4

    invoke-virtual {v4}, Lcom/navdy/client/app/framework/models/Destination;->toJsonString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ",\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->access$100(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;Ljava/lang/String;)V

    .line 213
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$1;->this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "  Selected currentDestination: name["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$1;->this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    .line 214
    invoke-static {v4}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->access$400(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;)Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v4

    iget-object v4, v4, Lcom/navdy/client/app/framework/models/Destination;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " address["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$1;->this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    .line 215
    invoke-static {v4}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->access$400(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;)Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v4

    iget-object v4, v4, Lcom/navdy/client/app/framework/models/Destination;->rawAddressNotForDisplay:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 213
    invoke-static {v2, v3}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->access$200(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;Ljava/lang/String;)V

    .line 216
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$1;->this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "  Display coords:    "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$1;->this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    invoke-static {v4}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->access$400(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;)Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v4

    iget-wide v4, v4, Lcom/navdy/client/app/framework/models/Destination;->displayLat:D

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$1;->this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    invoke-static {v4}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->access$400(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;)Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v4

    iget-wide v4, v4, Lcom/navdy/client/app/framework/models/Destination;->displayLong:D

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\tmap: https://www.google.com/maps?sourceid=chrome&q="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$1;->this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    .line 217
    invoke-static {v4}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->access$400(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;)Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v4

    iget-wide v4, v4, Lcom/navdy/client/app/framework/models/Destination;->displayLat:D

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$1;->this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    invoke-static {v4}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->access$400(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;)Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v4

    iget-wide v4, v4, Lcom/navdy/client/app/framework/models/Destination;->displayLong:D

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 216
    invoke-static {v2, v3}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->access$200(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;Ljava/lang/String;)V

    .line 220
    new-instance v9, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;

    move-object/from16 v0, p0

    invoke-direct {v9, v0}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;-><init>(Lcom/navdy/client/app/framework/search/GooglePlacesSearch$GoogleSearchListener;)V

    .line 221
    .local v9, "googlePlacesSearch":Lcom/navdy/client/app/framework/search/GooglePlacesSearch;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$1;->this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    invoke-static {v2}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->access$400(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;)Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v2

    iget-object v2, v2, Lcom/navdy/client/app/framework/models/Destination;->placeId:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 222
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$1;->this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    const-string v3, "\n==========\nGoogle Place Details (red)\n=========="

    invoke-static {v2, v3}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->access$200(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;Ljava/lang/String;)V

    .line 223
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$1;->this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "  placeId:         "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$1;->this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    invoke-static {v4}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->access$400(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;)Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v4

    iget-object v4, v4, Lcom/navdy/client/app/framework/models/Destination;->placeId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->access$200(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;Ljava/lang/String;)V

    .line 224
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$1;->this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    invoke-static {v2}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->access$400(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;)Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v2

    iget-object v2, v2, Lcom/navdy/client/app/framework/models/Destination;->placeId:Ljava/lang/String;

    invoke-virtual {v9, v2}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->runDetailsSearchWebApi(Ljava/lang/String;)V

    .line 276
    .end local v9    # "googlePlacesSearch":Lcom/navdy/client/app/framework/search/GooglePlacesSearch;
    .end local v16    # "googleSearchDestinations":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/Destination;>;"
    :cond_0
    :goto_0
    return-void

    .line 227
    .restart local v9    # "googlePlacesSearch":Lcom/navdy/client/app/framework/search/GooglePlacesSearch;
    .restart local v16    # "googleSearchDestinations":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/Destination;>;"
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$1;->this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$1;->this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    invoke-static {v3}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->access$400(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;)Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v3

    invoke-static {}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;->firstStep()Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->access$500(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;)V

    goto :goto_0

    .line 231
    .end local v9    # "googlePlacesSearch":Lcom/navdy/client/app/framework/search/GooglePlacesSearch;
    .end local v16    # "googleSearchDestinations":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/Destination;>;"
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$1;->this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    invoke-static {v2}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->access$600(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;)V

    goto :goto_0

    .line 233
    :cond_3
    sget-object v2, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;->DETAILS:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;

    move-object/from16 v0, p2

    if-ne v0, v2, :cond_0

    .line 234
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$1;->this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    invoke-static {v2}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->access$400(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;)Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v2

    if-nez v2, :cond_4

    .line 235
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$1;->this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    invoke-static {v2}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->access$700(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;)Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "Received a google place details result for an unknown currentDestination. Ignoring results."

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 239
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$1;->this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "  \"google_place_details_results\":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$1;->this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    move-object/from16 v0, p1

    invoke-static {v4, v0}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->access$000(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;Ljava/util/List;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ",\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->access$100(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;Ljava/lang/String;)V

    .line 241
    if-eqz p1, :cond_5

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_5

    .line 242
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v17

    .line 243
    .local v17, "iterator":Ljava/util/ListIterator;, "Ljava/util/ListIterator<Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;>;"
    invoke-interface/range {v17 .. v17}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;

    .line 245
    .local v8, "destinationResult":Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;
    new-instance v18, Lcom/navdy/client/app/framework/models/Destination;

    invoke-direct/range {v18 .. v18}, Lcom/navdy/client/app/framework/models/Destination;-><init>()V

    .line 246
    .local v18, "newDestination":Lcom/navdy/client/app/framework/models/Destination;
    sget-object v2, Lcom/navdy/client/app/framework/models/Destination$SearchType;->DETAILS:Lcom/navdy/client/app/framework/models/Destination$SearchType;

    move-object/from16 v0, v18

    invoke-virtual {v8, v2, v0}, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->toModelDestinationObject(Lcom/navdy/client/app/framework/models/Destination$SearchType;Lcom/navdy/client/app/framework/models/Destination;)V

    .line 248
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$1;->this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    .line 249
    invoke-static {v2}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->access$400(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;)Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/client/app/framework/models/Destination;->getDisplayCoordinate()Lcom/navdy/service/library/events/location/Coordinate;

    move-result-object v2

    .line 250
    invoke-virtual/range {v18 .. v18}, Lcom/navdy/client/app/framework/models/Destination;->getNavigationCoordinate()Lcom/navdy/service/library/events/location/Coordinate;

    move-result-object v3

    .line 248
    invoke-static {v2, v3}, Lcom/navdy/client/app/framework/map/MapUtils;->distanceBetween(Lcom/navdy/service/library/events/location/Coordinate;Lcom/navdy/service/library/events/location/Coordinate;)D

    move-result-wide v10

    .line 251
    .local v10, "distanceBetweenInOutCoords":D
    invoke-virtual/range {v18 .. v18}, Lcom/navdy/client/app/framework/models/Destination;->getDisplayCoordinate()Lcom/navdy/service/library/events/location/Coordinate;

    move-result-object v2

    .line 252
    invoke-virtual/range {v18 .. v18}, Lcom/navdy/client/app/framework/models/Destination;->getNavigationCoordinate()Lcom/navdy/service/library/events/location/Coordinate;

    move-result-object v3

    .line 251
    invoke-static {v2, v3}, Lcom/navdy/client/app/framework/map/MapUtils;->distanceBetween(Lcom/navdy/service/library/events/location/Coordinate;Lcom/navdy/service/library/events/location/Coordinate;)D

    move-result-wide v12

    .line 253
    .local v12, "distanceBetweenOutCoords":D
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$1;->this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    .line 254
    invoke-static {v2}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->access$400(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;)Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v2

    .line 253
    invoke-static {v2}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor;->getDistanceThresholdFor(Lcom/navdy/client/app/framework/models/Destination;)D

    move-result-wide v14

    .line 256
    .local v14, "distanceThreshold":D
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$1;->this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "  \"place_details_destination\":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {v18 .. v18}, Lcom/navdy/client/app/framework/models/Destination;->toJsonString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ",\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->access$100(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;Ljava/lang/String;)V

    .line 257
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$1;->this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    const-string v3, "  \"is_successful\":true,\n"

    invoke-static {v2, v3}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->access$100(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;Ljava/lang/String;)V

    .line 258
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$1;->this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "  \"display_coords\":\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v18

    iget-wide v4, v0, Lcom/navdy/client/app/framework/models/Destination;->displayLat:D

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v18

    iget-wide v4, v0, Lcom/navdy/client/app/framework/models/Destination;->displayLong:D

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\",\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->access$100(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;Ljava/lang/String;)V

    .line 259
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$1;->this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "  \"nav_coords\":\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v18

    iget-wide v4, v0, Lcom/navdy/client/app/framework/models/Destination;->navigationLat:D

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v18

    iget-wide v4, v0, Lcom/navdy/client/app/framework/models/Destination;->navigationLong:D

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\",\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->access$100(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;Ljava/lang/String;)V

    .line 260
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$1;->this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "  \"distance_between_out_coords\":\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v12, v13}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\",\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->access$100(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;Ljava/lang/String;)V

    .line 261
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$1;->this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "  \"distance_between_in_n_out_coords\":\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v10, v11}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\",\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->access$100(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;Ljava/lang/String;)V

    .line 262
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$1;->this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "  \"distance_in_n_out_threshold\":\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v14, v15}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\",\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->access$100(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;Ljava/lang/String;)V

    .line 264
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$1;->this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "  display_coords:  "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v18

    iget-wide v4, v0, Lcom/navdy/client/app/framework/models/Destination;->displayLat:D

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v18

    iget-wide v4, v0, Lcom/navdy/client/app/framework/models/Destination;->displayLong:D

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\tmap: https://www.google.com/maps?sourceid=chrome&q="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v18

    iget-wide v4, v0, Lcom/navdy/client/app/framework/models/Destination;->displayLat:D

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v18

    iget-wide v4, v0, Lcom/navdy/client/app/framework/models/Destination;->displayLong:D

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->access$200(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;Ljava/lang/String;)V

    .line 265
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$1;->this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "  Address:                          "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v18

    iget-object v4, v0, Lcom/navdy/client/app/framework/models/Destination;->rawAddressNotForDisplay:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->access$200(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;Ljava/lang/String;)V

    .line 266
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$1;->this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    const-string v3, "  Display coords:                   "

    move-object/from16 v0, v18

    iget-wide v4, v0, Lcom/navdy/client/app/framework/models/Destination;->displayLat:D

    move-object/from16 v0, v18

    iget-wide v6, v0, Lcom/navdy/client/app/framework/models/Destination;->displayLong:D

    invoke-static/range {v2 .. v7}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->access$800(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;Ljava/lang/String;DD)V

    .line 267
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$1;->this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    const-string v3, "  Nav coords:                       "

    move-object/from16 v0, v18

    iget-wide v4, v0, Lcom/navdy/client/app/framework/models/Destination;->navigationLat:D

    move-object/from16 v0, v18

    iget-wide v6, v0, Lcom/navdy/client/app/framework/models/Destination;->navigationLong:D

    invoke-static/range {v2 .. v7}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->access$800(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;Ljava/lang/String;DD)V

    .line 268
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$1;->this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "  Distance between out coords:      "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v12, v13}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->access$200(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;Ljava/lang/String;)V

    .line 269
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$1;->this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "  Distance between in & out coords: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v10, v11}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->access$200(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;Ljava/lang/String;)V

    .line 270
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$1;->this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "  Distance threshold for in & out:  "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v14, v15}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->access$200(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;Ljava/lang/String;)V

    .line 274
    .end local v8    # "destinationResult":Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;
    .end local v10    # "distanceBetweenInOutCoords":D
    .end local v12    # "distanceBetweenOutCoords":D
    .end local v14    # "distanceThreshold":D
    .end local v17    # "iterator":Ljava/util/ListIterator;, "Ljava/util/ListIterator<Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;>;"
    .end local v18    # "newDestination":Lcom/navdy/client/app/framework/models/Destination;
    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$1;->this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$1;->this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    invoke-static {v3}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->access$400(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;)Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v3

    invoke-static {}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;->firstStep()Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->access$500(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;)V

    goto/16 :goto_0
.end method
