.class public Lcom/navdy/client/debug/view/S3BrowserFragment$$ViewInjector;
.super Ljava/lang/Object;
.source "S3BrowserFragment$$ViewInjector.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static inject(Lbutterknife/ButterKnife$Finder;Lcom/navdy/client/debug/view/S3BrowserFragment;Ljava/lang/Object;)V
    .locals 3
    .param p0, "finder"    # Lbutterknife/ButterKnife$Finder;
    .param p1, "target"    # Lcom/navdy/client/debug/view/S3BrowserFragment;
    .param p2, "source"    # Ljava/lang/Object;

    .prologue
    .line 10
    const v1, 0x7f10027d

    const-string v2, "field \'mList\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 11
    .local v0, "view":Landroid/view/View;
    check-cast v0, Landroid/widget/ListView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/client/debug/view/S3BrowserFragment;->mList:Landroid/widget/ListView;

    .line 12
    const v1, 0x7f10027e

    const-string v2, "field \'mRefreshButton\' and method \'onRefresh\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .restart local v0    # "view":Landroid/view/View;
    move-object v1, v0

    .line 13
    check-cast v1, Landroid/widget/Button;

    iput-object v1, p1, Lcom/navdy/client/debug/view/S3BrowserFragment;->mRefreshButton:Landroid/widget/Button;

    .line 14
    new-instance v1, Lcom/navdy/client/debug/view/S3BrowserFragment$$ViewInjector$1;

    invoke-direct {v1, p1}, Lcom/navdy/client/debug/view/S3BrowserFragment$$ViewInjector$1;-><init>(Lcom/navdy/client/debug/view/S3BrowserFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 22
    const v1, 0x7f100184

    const-string v2, "field \'mBackButton\' and method \'onBack\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    move-object v1, v0

    .line 23
    check-cast v1, Landroid/widget/Button;

    iput-object v1, p1, Lcom/navdy/client/debug/view/S3BrowserFragment;->mBackButton:Landroid/widget/Button;

    .line 24
    new-instance v1, Lcom/navdy/client/debug/view/S3BrowserFragment$$ViewInjector$2;

    invoke-direct {v1, p1}, Lcom/navdy/client/debug/view/S3BrowserFragment$$ViewInjector$2;-><init>(Lcom/navdy/client/debug/view/S3BrowserFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 32
    return-void
.end method

.method public static reset(Lcom/navdy/client/debug/view/S3BrowserFragment;)V
    .locals 1
    .param p0, "target"    # Lcom/navdy/client/debug/view/S3BrowserFragment;

    .prologue
    const/4 v0, 0x0

    .line 35
    iput-object v0, p0, Lcom/navdy/client/debug/view/S3BrowserFragment;->mList:Landroid/widget/ListView;

    .line 36
    iput-object v0, p0, Lcom/navdy/client/debug/view/S3BrowserFragment;->mRefreshButton:Landroid/widget/Button;

    .line 37
    iput-object v0, p0, Lcom/navdy/client/debug/view/S3BrowserFragment;->mBackButton:Landroid/widget/Button;

    .line 38
    return-void
.end method
