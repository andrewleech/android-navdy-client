.class Lcom/navdy/client/debug/view/OTAS3BrowserFragment$1;
.super Ljava/lang/Object;
.source "OTAS3BrowserFragment.java"

# interfaces
.implements Lcom/amazonaws/event/ProgressListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/debug/view/OTAS3BrowserFragment;->onFileSelected(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/debug/view/OTAS3BrowserFragment;

.field final synthetic val$fileName:Ljava/lang/String;

.field final synthetic val$tempFile:Ljava/io/File;


# direct methods
.method constructor <init>(Lcom/navdy/client/debug/view/OTAS3BrowserFragment;Ljava/io/File;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/debug/view/OTAS3BrowserFragment;

    .prologue
    .line 147
    iput-object p1, p0, Lcom/navdy/client/debug/view/OTAS3BrowserFragment$1;->this$0:Lcom/navdy/client/debug/view/OTAS3BrowserFragment;

    iput-object p2, p0, Lcom/navdy/client/debug/view/OTAS3BrowserFragment$1;->val$tempFile:Ljava/io/File;

    iput-object p3, p0, Lcom/navdy/client/debug/view/OTAS3BrowserFragment$1;->val$fileName:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public progressChanged(Lcom/amazonaws/event/ProgressEvent;)V
    .locals 8
    .param p1, "progressEvent"    # Lcom/amazonaws/event/ProgressEvent;

    .prologue
    .line 150
    invoke-virtual {p1}, Lcom/amazonaws/event/ProgressEvent;->getEventCode()I

    move-result v1

    const/4 v2, 0x4

    if-ne v1, v2, :cond_0

    .line 151
    new-instance v0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;

    iget-object v1, p0, Lcom/navdy/client/debug/view/OTAS3BrowserFragment$1;->this$0:Lcom/navdy/client/debug/view/OTAS3BrowserFragment;

    iget-object v1, v1, Lcom/navdy/client/debug/view/OTAS3BrowserFragment;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/navdy/client/debug/view/OTAS3BrowserFragment$1;->val$tempFile:Ljava/io/File;

    sget-object v3, Lcom/navdy/service/library/events/file/FileType;->FILE_TYPE_OTA:Lcom/navdy/service/library/events/file/FileType;

    iget-object v4, p0, Lcom/navdy/client/debug/view/OTAS3BrowserFragment$1;->val$fileName:Ljava/lang/String;

    new-instance v5, Lcom/navdy/client/debug/view/OTAS3BrowserFragment$TempFileTransferListener;

    iget-object v6, p0, Lcom/navdy/client/debug/view/OTAS3BrowserFragment$1;->this$0:Lcom/navdy/client/debug/view/OTAS3BrowserFragment;

    iget-object v7, p0, Lcom/navdy/client/debug/view/OTAS3BrowserFragment$1;->val$tempFile:Ljava/io/File;

    invoke-direct {v5, v6, v7}, Lcom/navdy/client/debug/view/OTAS3BrowserFragment$TempFileTransferListener;-><init>(Lcom/navdy/client/debug/view/OTAS3BrowserFragment;Ljava/io/File;)V

    invoke-direct/range {v0 .. v5}, Lcom/navdy/client/debug/file/RemoteFileTransferManager;-><init>(Landroid/content/Context;Ljava/io/File;Lcom/navdy/service/library/events/file/FileType;Ljava/lang/String;Lcom/navdy/client/debug/file/FileTransferManager$FileTransferListener;)V

    .line 154
    .local v0, "fileTransferManager":Lcom/navdy/client/debug/file/RemoteFileTransferManager;
    invoke-virtual {v0}, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->sendFile()Z

    .line 156
    .end local v0    # "fileTransferManager":Lcom/navdy/client/debug/file/RemoteFileTransferManager;
    :cond_0
    return-void
.end method
