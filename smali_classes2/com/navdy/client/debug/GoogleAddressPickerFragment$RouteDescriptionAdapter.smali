.class Lcom/navdy/client/debug/GoogleAddressPickerFragment$RouteDescriptionAdapter;
.super Landroid/widget/BaseAdapter;
.source "GoogleAddressPickerFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/debug/GoogleAddressPickerFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RouteDescriptionAdapter"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/debug/GoogleAddressPickerFragment$RouteDescriptionAdapter$ViewHolder;
    }
.end annotation


# instance fields
.field private context:Landroid/content/Context;

.field private data:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/client/debug/adapter/RouteDescriptionData;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/navdy/client/debug/GoogleAddressPickerFragment;


# direct methods
.method constructor <init>(Lcom/navdy/client/debug/GoogleAddressPickerFragment;Ljava/util/List;Landroid/content/Context;)V
    .locals 0
    .param p3, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/client/debug/adapter/RouteDescriptionData;",
            ">;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .prologue
    .line 910
    .local p2, "data":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/debug/adapter/RouteDescriptionData;>;"
    iput-object p1, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment$RouteDescriptionAdapter;->this$0:Lcom/navdy/client/debug/GoogleAddressPickerFragment;

    .line 911
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 912
    iput-object p2, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment$RouteDescriptionAdapter;->data:Ljava/util/List;

    .line 913
    iput-object p3, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment$RouteDescriptionAdapter;->context:Landroid/content/Context;

    .line 914
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 927
    iget-object v0, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment$RouteDescriptionAdapter;->data:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getData()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/client/debug/adapter/RouteDescriptionData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 922
    iget-object v0, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment$RouteDescriptionAdapter;->data:Ljava/util/List;

    return-object v0
.end method

.method public getItem(I)Lcom/navdy/client/debug/adapter/RouteDescriptionData;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 932
    iget-object v0, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment$RouteDescriptionAdapter;->data:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/debug/adapter/RouteDescriptionData;

    return-object v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 905
    invoke-virtual {p0, p1}, Lcom/navdy/client/debug/GoogleAddressPickerFragment$RouteDescriptionAdapter;->getItem(I)Lcom/navdy/client/debug/adapter/RouteDescriptionData;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 937
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/16 v8, 0x8

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 942
    invoke-virtual {p0, p1}, Lcom/navdy/client/debug/GoogleAddressPickerFragment$RouteDescriptionAdapter;->getItem(I)Lcom/navdy/client/debug/adapter/RouteDescriptionData;

    move-result-object v1

    .line 944
    .local v1, "item":Lcom/navdy/client/debug/adapter/RouteDescriptionData;
    if-nez p2, :cond_1

    .line 945
    iget-object v3, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment$RouteDescriptionAdapter;->context:Landroid/content/Context;

    const-string v4, "layout_inflater"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    .line 946
    .local v2, "li":Landroid/view/LayoutInflater;
    const v3, 0x7f0300d5

    invoke-virtual {v2, v3, p3, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 947
    new-instance v0, Lcom/navdy/client/debug/GoogleAddressPickerFragment$RouteDescriptionAdapter$ViewHolder;

    invoke-direct {v0, p0}, Lcom/navdy/client/debug/GoogleAddressPickerFragment$RouteDescriptionAdapter$ViewHolder;-><init>(Lcom/navdy/client/debug/GoogleAddressPickerFragment$RouteDescriptionAdapter;)V

    .line 948
    .local v0, "holder":Lcom/navdy/client/debug/GoogleAddressPickerFragment$RouteDescriptionAdapter$ViewHolder;
    const v3, 0x7f100256

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v0, Lcom/navdy/client/debug/GoogleAddressPickerFragment$RouteDescriptionAdapter$ViewHolder;->routeDiff:Landroid/widget/TextView;

    .line 949
    const v3, 0x7f100159

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v0, Lcom/navdy/client/debug/GoogleAddressPickerFragment$RouteDescriptionAdapter$ViewHolder;->duration:Landroid/widget/TextView;

    .line 950
    const v3, 0x7f100337

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v0, Lcom/navdy/client/debug/GoogleAddressPickerFragment$RouteDescriptionAdapter$ViewHolder;->length:Landroid/widget/TextView;

    .line 951
    const v3, 0x7f100132

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, v0, Lcom/navdy/client/debug/GoogleAddressPickerFragment$RouteDescriptionAdapter$ViewHolder;->startNavigation:Landroid/widget/Button;

    .line 952
    const v3, 0x7f100339

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, v0, Lcom/navdy/client/debug/GoogleAddressPickerFragment$RouteDescriptionAdapter$ViewHolder;->queueNavigation:Landroid/widget/Button;

    .line 953
    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 955
    iget-object v3, v0, Lcom/navdy/client/debug/GoogleAddressPickerFragment$RouteDescriptionAdapter$ViewHolder;->startNavigation:Landroid/widget/Button;

    new-instance v4, Lcom/navdy/client/debug/GoogleAddressPickerFragment$RouteDescriptionAdapter$1;

    invoke-direct {v4, p0}, Lcom/navdy/client/debug/GoogleAddressPickerFragment$RouteDescriptionAdapter$1;-><init>(Lcom/navdy/client/debug/GoogleAddressPickerFragment$RouteDescriptionAdapter;)V

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 979
    .end local v2    # "li":Landroid/view/LayoutInflater;
    :goto_0
    iget-object v3, v0, Lcom/navdy/client/debug/GoogleAddressPickerFragment$RouteDescriptionAdapter$ViewHolder;->routeDiff:Landroid/widget/TextView;

    iget-object v4, v1, Lcom/navdy/client/debug/adapter/RouteDescriptionData;->diff:Ljava/lang/String;

    invoke-static {v4}, Lcom/navdy/client/debug/util/FormatUtils;->addPrefixForRouteDifference(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 980
    if-ne p1, v7, :cond_2

    .line 981
    iget-object v3, v0, Lcom/navdy/client/debug/GoogleAddressPickerFragment$RouteDescriptionAdapter$ViewHolder;->duration:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment$RouteDescriptionAdapter;->this$0:Lcom/navdy/client/debug/GoogleAddressPickerFragment;

    invoke-virtual {v4}, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0f00cd

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 987
    :cond_0
    :goto_1
    iget-object v3, v0, Lcom/navdy/client/debug/GoogleAddressPickerFragment$RouteDescriptionAdapter$ViewHolder;->duration:Landroid/widget/TextView;

    iget v4, v1, Lcom/navdy/client/debug/adapter/RouteDescriptionData;->duration:I

    invoke-static {v4}, Lcom/navdy/client/debug/util/FormatUtils;->formatDurationFromSecondsToSecondsMinutesHours(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 988
    iget-object v3, v0, Lcom/navdy/client/debug/GoogleAddressPickerFragment$RouteDescriptionAdapter$ViewHolder;->length:Landroid/widget/TextView;

    iget v4, v1, Lcom/navdy/client/debug/adapter/RouteDescriptionData;->length:I

    invoke-static {v4}, Lcom/navdy/client/debug/util/FormatUtils;->formatLengthFromMetersToMiles(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 989
    invoke-virtual {p0}, Lcom/navdy/client/debug/GoogleAddressPickerFragment$RouteDescriptionAdapter;->getCount()I

    move-result v3

    if-ne v3, v7, :cond_4

    .line 990
    iget-object v3, v0, Lcom/navdy/client/debug/GoogleAddressPickerFragment$RouteDescriptionAdapter$ViewHolder;->startNavigation:Landroid/widget/Button;

    invoke-virtual {v3, v6}, Landroid/widget/Button;->setVisibility(I)V

    .line 991
    iget-object v3, v0, Lcom/navdy/client/debug/GoogleAddressPickerFragment$RouteDescriptionAdapter$ViewHolder;->queueNavigation:Landroid/widget/Button;

    invoke-virtual {v3, v6}, Landroid/widget/Button;->setVisibility(I)V

    .line 996
    :goto_2
    return-object p2

    .line 977
    .end local v0    # "holder":Lcom/navdy/client/debug/GoogleAddressPickerFragment$RouteDescriptionAdapter$ViewHolder;
    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/debug/GoogleAddressPickerFragment$RouteDescriptionAdapter$ViewHolder;

    .restart local v0    # "holder":Lcom/navdy/client/debug/GoogleAddressPickerFragment$RouteDescriptionAdapter$ViewHolder;
    goto :goto_0

    .line 982
    :cond_2
    const/4 v3, 0x2

    if-ne p1, v3, :cond_3

    .line 983
    iget-object v3, v0, Lcom/navdy/client/debug/GoogleAddressPickerFragment$RouteDescriptionAdapter$ViewHolder;->duration:Landroid/widget/TextView;

    const/high16 v4, -0x10000

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_1

    .line 984
    :cond_3
    if-nez p1, :cond_0

    .line 985
    iget-object v3, v0, Lcom/navdy/client/debug/GoogleAddressPickerFragment$RouteDescriptionAdapter$ViewHolder;->duration:Landroid/widget/TextView;

    const/high16 v4, -0x1000000

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_1

    .line 993
    :cond_4
    iget-object v3, v0, Lcom/navdy/client/debug/GoogleAddressPickerFragment$RouteDescriptionAdapter$ViewHolder;->startNavigation:Landroid/widget/Button;

    invoke-virtual {v3, v8}, Landroid/widget/Button;->setVisibility(I)V

    .line 994
    iget-object v3, v0, Lcom/navdy/client/debug/GoogleAddressPickerFragment$RouteDescriptionAdapter$ViewHolder;->queueNavigation:Landroid/widget/Button;

    invoke-virtual {v3, v8}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_2
.end method

.method public setData(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/client/debug/adapter/RouteDescriptionData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 917
    .local p1, "data":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/debug/adapter/RouteDescriptionData;>;"
    iput-object p1, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment$RouteDescriptionAdapter;->data:Ljava/util/List;

    .line 918
    invoke-virtual {p0}, Lcom/navdy/client/debug/GoogleAddressPickerFragment$RouteDescriptionAdapter;->notifyDataSetChanged()V

    .line 919
    return-void
.end method
