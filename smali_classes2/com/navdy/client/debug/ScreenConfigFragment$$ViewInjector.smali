.class public Lcom/navdy/client/debug/ScreenConfigFragment$$ViewInjector;
.super Ljava/lang/Object;
.source "ScreenConfigFragment$$ViewInjector.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static inject(Lbutterknife/ButterKnife$Finder;Lcom/navdy/client/debug/ScreenConfigFragment;Ljava/lang/Object;)V
    .locals 3
    .param p0, "finder"    # Lbutterknife/ButterKnife$Finder;
    .param p1, "target"    # Lcom/navdy/client/debug/ScreenConfigFragment;
    .param p2, "source"    # Ljava/lang/Object;

    .prologue
    .line 10
    const v1, 0x7f100281

    const-string v2, "method \'onResetScale\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 11
    .local v0, "view":Landroid/view/View;
    new-instance v1, Lcom/navdy/client/debug/ScreenConfigFragment$$ViewInjector$1;

    invoke-direct {v1, p1}, Lcom/navdy/client/debug/ScreenConfigFragment$$ViewInjector$1;-><init>(Lcom/navdy/client/debug/ScreenConfigFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 19
    const v1, 0x7f10027f

    const-string v2, "method \'onTouchButton\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 20
    new-instance v1, Lcom/navdy/client/debug/ScreenConfigFragment$$ViewInjector$2;

    invoke-direct {v1, p1}, Lcom/navdy/client/debug/ScreenConfigFragment$$ViewInjector$2;-><init>(Lcom/navdy/client/debug/ScreenConfigFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 29
    const v1, 0x7f100280

    const-string v2, "method \'onTouchButton\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 30
    new-instance v1, Lcom/navdy/client/debug/ScreenConfigFragment$$ViewInjector$3;

    invoke-direct {v1, p1}, Lcom/navdy/client/debug/ScreenConfigFragment$$ViewInjector$3;-><init>(Lcom/navdy/client/debug/ScreenConfigFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 39
    return-void
.end method

.method public static reset(Lcom/navdy/client/debug/ScreenConfigFragment;)V
    .locals 0
    .param p0, "target"    # Lcom/navdy/client/debug/ScreenConfigFragment;

    .prologue
    .line 42
    return-void
.end method
