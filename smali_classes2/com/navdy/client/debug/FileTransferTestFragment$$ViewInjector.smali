.class public Lcom/navdy/client/debug/FileTransferTestFragment$$ViewInjector;
.super Ljava/lang/Object;
.source "FileTransferTestFragment$$ViewInjector.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static inject(Lbutterknife/ButterKnife$Finder;Lcom/navdy/client/debug/FileTransferTestFragment;Ljava/lang/Object;)V
    .locals 3
    .param p0, "finder"    # Lbutterknife/ButterKnife$Finder;
    .param p1, "target"    # Lcom/navdy/client/debug/FileTransferTestFragment;
    .param p2, "source"    # Ljava/lang/Object;

    .prologue
    .line 10
    const v1, 0x7f100237

    const-string v2, "field \'sizeSelection\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 11
    .local v0, "view":Landroid/view/View;
    check-cast v0, Landroid/widget/RadioGroup;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/client/debug/FileTransferTestFragment;->sizeSelection:Landroid/widget/RadioGroup;

    .line 12
    const v1, 0x7f10023b

    const-string v2, "field \'transferButton\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 13
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/Button;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/client/debug/FileTransferTestFragment;->transferButton:Landroid/widget/Button;

    .line 14
    const v1, 0x7f10023d

    const-string v2, "field \'transferProgress\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 15
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/ProgressBar;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/client/debug/FileTransferTestFragment;->transferProgress:Landroid/widget/ProgressBar;

    .line 16
    const v1, 0x7f10023c

    const-string v2, "field \'progressContainer\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 17
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/LinearLayout;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/client/debug/FileTransferTestFragment;->progressContainer:Landroid/widget/LinearLayout;

    .line 18
    const v1, 0x7f10023e

    const-string v2, "field \'transferMessage\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 19
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/client/debug/FileTransferTestFragment;->transferMessage:Landroid/widget/TextView;

    .line 20
    return-void
.end method

.method public static reset(Lcom/navdy/client/debug/FileTransferTestFragment;)V
    .locals 1
    .param p0, "target"    # Lcom/navdy/client/debug/FileTransferTestFragment;

    .prologue
    const/4 v0, 0x0

    .line 23
    iput-object v0, p0, Lcom/navdy/client/debug/FileTransferTestFragment;->sizeSelection:Landroid/widget/RadioGroup;

    .line 24
    iput-object v0, p0, Lcom/navdy/client/debug/FileTransferTestFragment;->transferButton:Landroid/widget/Button;

    .line 25
    iput-object v0, p0, Lcom/navdy/client/debug/FileTransferTestFragment;->transferProgress:Landroid/widget/ProgressBar;

    .line 26
    iput-object v0, p0, Lcom/navdy/client/debug/FileTransferTestFragment;->progressContainer:Landroid/widget/LinearLayout;

    .line 27
    iput-object v0, p0, Lcom/navdy/client/debug/FileTransferTestFragment;->transferMessage:Landroid/widget/TextView;

    .line 28
    return-void
.end method
