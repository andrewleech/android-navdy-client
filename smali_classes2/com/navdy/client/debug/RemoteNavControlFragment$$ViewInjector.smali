.class public Lcom/navdy/client/debug/RemoteNavControlFragment$$ViewInjector;
.super Ljava/lang/Object;
.source "RemoteNavControlFragment$$ViewInjector.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static inject(Lbutterknife/ButterKnife$Finder;Lcom/navdy/client/debug/RemoteNavControlFragment;Ljava/lang/Object;)V
    .locals 3
    .param p0, "finder"    # Lbutterknife/ButterKnife$Finder;
    .param p1, "target"    # Lcom/navdy/client/debug/RemoteNavControlFragment;
    .param p2, "source"    # Ljava/lang/Object;

    .prologue
    .line 10
    const v1, 0x7f10026d

    const-string v2, "field \'mDestinationLabelTextView\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 11
    .local v0, "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/client/debug/RemoteNavControlFragment;->mDestinationLabelTextView:Landroid/widget/TextView;

    .line 12
    const v1, 0x7f10026e

    const-string v2, "field \'mNavigationButtonStart\' and method \'onStartClicked\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .restart local v0    # "view":Landroid/view/View;
    move-object v1, v0

    .line 13
    check-cast v1, Landroid/widget/Button;

    iput-object v1, p1, Lcom/navdy/client/debug/RemoteNavControlFragment;->mNavigationButtonStart:Landroid/widget/Button;

    .line 14
    new-instance v1, Lcom/navdy/client/debug/RemoteNavControlFragment$$ViewInjector$1;

    invoke-direct {v1, p1}, Lcom/navdy/client/debug/RemoteNavControlFragment$$ViewInjector$1;-><init>(Lcom/navdy/client/debug/RemoteNavControlFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 22
    const v1, 0x7f10026f

    const-string v2, "field \'mNavigationButtonSimulate\' and method \'onSimulateClicked\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    move-object v1, v0

    .line 23
    check-cast v1, Landroid/widget/Button;

    iput-object v1, p1, Lcom/navdy/client/debug/RemoteNavControlFragment;->mNavigationButtonSimulate:Landroid/widget/Button;

    .line 24
    new-instance v1, Lcom/navdy/client/debug/RemoteNavControlFragment$$ViewInjector$2;

    invoke-direct {v1, p1}, Lcom/navdy/client/debug/RemoteNavControlFragment$$ViewInjector$2;-><init>(Lcom/navdy/client/debug/RemoteNavControlFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 32
    const v1, 0x7f100270

    const-string v2, "field \'mNavigationButtonPause\' and method \'onPauseClicked\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    move-object v1, v0

    .line 33
    check-cast v1, Landroid/widget/Button;

    iput-object v1, p1, Lcom/navdy/client/debug/RemoteNavControlFragment;->mNavigationButtonPause:Landroid/widget/Button;

    .line 34
    new-instance v1, Lcom/navdy/client/debug/RemoteNavControlFragment$$ViewInjector$3;

    invoke-direct {v1, p1}, Lcom/navdy/client/debug/RemoteNavControlFragment$$ViewInjector$3;-><init>(Lcom/navdy/client/debug/RemoteNavControlFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 42
    const v1, 0x7f100271

    const-string v2, "field \'mNavigationButtonResume\' and method \'onResumeClicked\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    move-object v1, v0

    .line 43
    check-cast v1, Landroid/widget/Button;

    iput-object v1, p1, Lcom/navdy/client/debug/RemoteNavControlFragment;->mNavigationButtonResume:Landroid/widget/Button;

    .line 44
    new-instance v1, Lcom/navdy/client/debug/RemoteNavControlFragment$$ViewInjector$4;

    invoke-direct {v1, p1}, Lcom/navdy/client/debug/RemoteNavControlFragment$$ViewInjector$4;-><init>(Lcom/navdy/client/debug/RemoteNavControlFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 52
    const v1, 0x7f100272

    const-string v2, "field \'mNavigationButtonStop\' and method \'onStopClicked\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    move-object v1, v0

    .line 53
    check-cast v1, Landroid/widget/Button;

    iput-object v1, p1, Lcom/navdy/client/debug/RemoteNavControlFragment;->mNavigationButtonStop:Landroid/widget/Button;

    .line 54
    new-instance v1, Lcom/navdy/client/debug/RemoteNavControlFragment$$ViewInjector$5;

    invoke-direct {v1, p1}, Lcom/navdy/client/debug/RemoteNavControlFragment$$ViewInjector$5;-><init>(Lcom/navdy/client/debug/RemoteNavControlFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 62
    const v1, 0x7f100232

    const-string v2, "field \'mTextViewCurrentSimSpeed\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 63
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/client/debug/RemoteNavControlFragment;->mTextViewCurrentSimSpeed:Landroid/widget/TextView;

    .line 64
    return-void
.end method

.method public static reset(Lcom/navdy/client/debug/RemoteNavControlFragment;)V
    .locals 1
    .param p0, "target"    # Lcom/navdy/client/debug/RemoteNavControlFragment;

    .prologue
    const/4 v0, 0x0

    .line 67
    iput-object v0, p0, Lcom/navdy/client/debug/RemoteNavControlFragment;->mDestinationLabelTextView:Landroid/widget/TextView;

    .line 68
    iput-object v0, p0, Lcom/navdy/client/debug/RemoteNavControlFragment;->mNavigationButtonStart:Landroid/widget/Button;

    .line 69
    iput-object v0, p0, Lcom/navdy/client/debug/RemoteNavControlFragment;->mNavigationButtonSimulate:Landroid/widget/Button;

    .line 70
    iput-object v0, p0, Lcom/navdy/client/debug/RemoteNavControlFragment;->mNavigationButtonPause:Landroid/widget/Button;

    .line 71
    iput-object v0, p0, Lcom/navdy/client/debug/RemoteNavControlFragment;->mNavigationButtonResume:Landroid/widget/Button;

    .line 72
    iput-object v0, p0, Lcom/navdy/client/debug/RemoteNavControlFragment;->mNavigationButtonStop:Landroid/widget/Button;

    .line 73
    iput-object v0, p0, Lcom/navdy/client/debug/RemoteNavControlFragment;->mTextViewCurrentSimSpeed:Landroid/widget/TextView;

    .line 74
    return-void
.end method
