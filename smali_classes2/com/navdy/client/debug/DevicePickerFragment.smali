.class public Lcom/navdy/client/debug/DevicePickerFragment;
.super Landroid/app/ListFragment;
.source "DevicePickerFragment.java"


# instance fields
.field protected mAppInstance:Lcom/navdy/client/app/framework/AppInstance;

.field mButtonAddDevice:Landroid/widget/Button;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f100235
    .end annotation
.end field

.field protected mCurrentDevice:Lcom/navdy/service/library/device/connection/ConnectionInfo;

.field protected mDeviceListUpdateListener:Lcom/navdy/service/library/device/RemoteDeviceRegistry$DeviceListUpdatedListener;

.field protected mDevices:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/client/debug/devicepicker/Device;",
            ">;"
        }
    .end annotation
.end field

.field mEditTextNewDevice:Landroid/widget/EditText;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f100234
    .end annotation
.end field

.field protected mRemoteDeviceRegistry:Lcom/navdy/service/library/device/RemoteDeviceRegistry;

.field mTextViewCurrentDevice:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f100233
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Landroid/app/ListFragment;-><init>()V

    .line 39
    return-void
.end method

.method private getActionBar()Landroid/app/ActionBar;
    .locals 1

    .prologue
    .line 188
    invoke-virtual {p0}, Lcom/navdy/client/debug/DevicePickerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected addConnectionInfo(Lcom/navdy/service/library/device/connection/ConnectionInfo;Z)V
    .locals 4
    .param p1, "connectionInfo"    # Lcom/navdy/service/library/device/connection/ConnectionInfo;
    .param p2, "updateIfFound"    # Z

    .prologue
    .line 87
    new-instance v1, Lcom/navdy/client/debug/devicepicker/Device;

    invoke-direct {v1, p1}, Lcom/navdy/client/debug/devicepicker/Device;-><init>(Lcom/navdy/service/library/device/connection/ConnectionInfo;)V

    .line 88
    .local v1, "newDevice":Lcom/navdy/client/debug/devicepicker/Device;
    iget-object v3, p0, Lcom/navdy/client/debug/DevicePickerFragment;->mDevices:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 89
    .local v0, "index":I
    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    .line 90
    iget-object v3, p0, Lcom/navdy/client/debug/DevicePickerFragment;->mDevices:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/navdy/client/debug/devicepicker/Device;

    .line 91
    .local v2, "oldDevice":Lcom/navdy/client/debug/devicepicker/Device;
    invoke-virtual {v2, p1, p2}, Lcom/navdy/client/debug/devicepicker/Device;->add(Lcom/navdy/service/library/device/connection/ConnectionInfo;Z)V

    .line 95
    .end local v2    # "oldDevice":Lcom/navdy/client/debug/devicepicker/Device;
    :goto_0
    return-void

    .line 93
    :cond_0
    iget-object v3, p0, Lcom/navdy/client/debug/DevicePickerFragment;->mDevices:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method protected addConnectionInfo(Ljava/util/Set;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/navdy/service/library/device/connection/ConnectionInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 98
    .local p1, "connectionInfoSet":Ljava/util/Set;, "Ljava/util/Set<Lcom/navdy/service/library/device/connection/ConnectionInfo;>;"
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/device/connection/ConnectionInfo;

    .line 99
    .local v0, "connectionInfo":Lcom/navdy/service/library/device/connection/ConnectionInfo;
    const/4 v2, 0x1

    invoke-virtual {p0, v0, v2}, Lcom/navdy/client/debug/DevicePickerFragment;->addConnectionInfo(Lcom/navdy/service/library/device/connection/ConnectionInfo;Z)V

    goto :goto_0

    .line 101
    .end local v0    # "connectionInfo":Lcom/navdy/service/library/device/connection/ConnectionInfo;
    :cond_0
    return-void
.end method

.method protected hideKeyboard()V
    .locals 3

    .prologue
    .line 182
    invoke-virtual {p0}, Lcom/navdy/client/debug/DevicePickerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-string v2, "input_method"

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 184
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    iget-object v1, p0, Lcom/navdy/client/debug/DevicePickerFragment;->mEditTextNewDevice:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 185
    return-void
.end method

.method public onAddClicked()V
    .locals 4
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f100235
        }
    .end annotation

    .prologue
    .line 168
    iget-object v2, p0, Lcom/navdy/client/debug/DevicePickerFragment;->mEditTextNewDevice:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 170
    .local v1, "newDeviceAddress":Ljava/lang/String;
    invoke-static {v1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 179
    :goto_0
    return-void

    .line 174
    :cond_0
    new-instance v0, Lcom/navdy/service/library/device/connection/TCPConnectionInfo;

    sget-object v2, Lcom/navdy/service/library/device/NavdyDeviceId;->UNKNOWN_ID:Lcom/navdy/service/library/device/NavdyDeviceId;

    invoke-direct {v0, v2, v1}, Lcom/navdy/service/library/device/connection/TCPConnectionInfo;-><init>(Lcom/navdy/service/library/device/NavdyDeviceId;Ljava/lang/String;)V

    .line 175
    .local v0, "connectionInfo":Lcom/navdy/service/library/device/connection/TCPConnectionInfo;
    iget-object v2, p0, Lcom/navdy/client/debug/DevicePickerFragment;->mRemoteDeviceRegistry:Lcom/navdy/service/library/device/RemoteDeviceRegistry;

    invoke-virtual {v2, v0}, Lcom/navdy/service/library/device/RemoteDeviceRegistry;->addDiscoveredConnectionInfo(Lcom/navdy/service/library/device/connection/ConnectionInfo;)V

    .line 177
    invoke-virtual {p0}, Lcom/navdy/client/debug/DevicePickerFragment;->hideKeyboard()V

    .line 178
    iget-object v2, p0, Lcom/navdy/client/debug/DevicePickerFragment;->mEditTextNewDevice:Landroid/widget/EditText;

    const-string v3, ""

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 53
    invoke-super {p0, p1}, Landroid/app/ListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 55
    invoke-virtual {p0}, Lcom/navdy/client/debug/DevicePickerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 57
    .local v0, "context":Landroid/content/Context;
    invoke-static {v0}, Lcom/navdy/service/library/device/RemoteDeviceRegistry;->getInstance(Landroid/content/Context;)Lcom/navdy/service/library/device/RemoteDeviceRegistry;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/client/debug/DevicePickerFragment;->mRemoteDeviceRegistry:Lcom/navdy/service/library/device/RemoteDeviceRegistry;

    .line 59
    invoke-static {}, Lcom/navdy/client/app/framework/AppInstance;->getInstance()Lcom/navdy/client/app/framework/AppInstance;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/client/debug/DevicePickerFragment;->mAppInstance:Lcom/navdy/client/app/framework/AppInstance;

    .line 61
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/navdy/client/debug/DevicePickerFragment;->mDevices:Ljava/util/ArrayList;

    .line 63
    iget-object v1, p0, Lcom/navdy/client/debug/DevicePickerFragment;->mRemoteDeviceRegistry:Lcom/navdy/service/library/device/RemoteDeviceRegistry;

    invoke-virtual {v1}, Lcom/navdy/service/library/device/RemoteDeviceRegistry;->getKnownConnectionInfo()Ljava/util/Set;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/navdy/client/debug/DevicePickerFragment;->addConnectionInfo(Ljava/util/Set;)V

    .line 66
    new-instance v1, Lcom/navdy/client/debug/devicepicker/DeviceAdapter;

    invoke-virtual {p0}, Lcom/navdy/client/debug/DevicePickerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/client/debug/DevicePickerFragment;->mDevices:Ljava/util/ArrayList;

    invoke-direct {v1, v2, v3}, Lcom/navdy/client/debug/devicepicker/DeviceAdapter;-><init>(Landroid/content/Context;Ljava/util/List;)V

    invoke-virtual {p0, v1}, Lcom/navdy/client/debug/DevicePickerFragment;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 68
    new-instance v1, Lcom/navdy/client/debug/DevicePickerFragment$1;

    invoke-direct {v1, p0}, Lcom/navdy/client/debug/DevicePickerFragment$1;-><init>(Lcom/navdy/client/debug/DevicePickerFragment;)V

    iput-object v1, p0, Lcom/navdy/client/debug/DevicePickerFragment;->mDeviceListUpdateListener:Lcom/navdy/service/library/device/RemoteDeviceRegistry$DeviceListUpdatedListener;

    .line 83
    iget-object v1, p0, Lcom/navdy/client/debug/DevicePickerFragment;->mRemoteDeviceRegistry:Lcom/navdy/service/library/device/RemoteDeviceRegistry;

    iget-object v2, p0, Lcom/navdy/client/debug/DevicePickerFragment;->mDeviceListUpdateListener:Lcom/navdy/service/library/device/RemoteDeviceRegistry$DeviceListUpdatedListener;

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/device/RemoteDeviceRegistry;->addListener(Lcom/navdy/service/library/util/Listenable$Listener;)Z

    .line 84
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 106
    const v1, 0x7f030089

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 107
    .local v0, "rootView":Landroid/view/View;
    invoke-static {p0, v0}, Lbutterknife/ButterKnife;->inject(Ljava/lang/Object;Landroid/view/View;)V

    .line 109
    return-object v0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 128
    invoke-super {p0}, Landroid/app/ListFragment;->onDestroy()V

    .line 129
    iget-object v0, p0, Lcom/navdy/client/debug/DevicePickerFragment;->mRemoteDeviceRegistry:Lcom/navdy/service/library/device/RemoteDeviceRegistry;

    iget-object v1, p0, Lcom/navdy/client/debug/DevicePickerFragment;->mDeviceListUpdateListener:Lcom/navdy/service/library/device/RemoteDeviceRegistry$DeviceListUpdatedListener;

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/device/RemoteDeviceRegistry;->removeListener(Lcom/navdy/service/library/util/Listenable$Listener;)Z

    .line 130
    return-void
.end method

.method public onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 8
    .param p1, "l"    # Landroid/widget/ListView;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J

    .prologue
    .line 144
    invoke-super/range {p0 .. p5}, Landroid/app/ListFragment;->onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V

    .line 146
    iget-object v5, p0, Lcom/navdy/client/debug/DevicePickerFragment;->mDevices:Ljava/util/ArrayList;

    invoke-virtual {v5, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/navdy/client/debug/devicepicker/Device;

    .line 147
    .local v3, "pickedDevice":Lcom/navdy/client/debug/devicepicker/Device;
    invoke-virtual {v3}, Lcom/navdy/client/debug/devicepicker/Device;->getPreferredConnectionInfo()Lcom/navdy/service/library/device/connection/ConnectionInfo;

    move-result-object v0

    .line 148
    .local v0, "connectionInfo":Lcom/navdy/service/library/device/connection/ConnectionInfo;
    iget-object v5, p0, Lcom/navdy/client/debug/DevicePickerFragment;->mRemoteDeviceRegistry:Lcom/navdy/service/library/device/RemoteDeviceRegistry;

    invoke-virtual {v5, v0}, Lcom/navdy/service/library/device/RemoteDeviceRegistry;->setDefaultConnectionInfo(Lcom/navdy/service/library/device/connection/ConnectionInfo;)V

    .line 149
    iget-object v5, p0, Lcom/navdy/client/debug/DevicePickerFragment;->mAppInstance:Lcom/navdy/client/app/framework/AppInstance;

    invoke-virtual {v5}, Lcom/navdy/client/app/framework/AppInstance;->initializeDevice()V

    .line 151
    invoke-virtual {p0}, Lcom/navdy/client/debug/DevicePickerFragment;->hideKeyboard()V

    .line 153
    invoke-virtual {p0}, Lcom/navdy/client/debug/DevicePickerFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    .line 154
    .local v1, "fragmentManager":Landroid/app/FragmentManager;
    const-string v5, "splash"

    invoke-virtual {v1, v5}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v4

    .line 155
    .local v4, "splash":Landroid/app/Fragment;
    invoke-virtual {v1}, Landroid/app/FragmentManager;->popBackStackImmediate()Z

    .line 156
    if-eqz v4, :cond_0

    .line 159
    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v2

    .line 160
    .local v2, "ft":Landroid/app/FragmentTransaction;
    const v5, 0x7f1000c6

    new-instance v6, Lcom/navdy/client/debug/MainDebugActivity$MainFragment;

    invoke-direct {v6}, Lcom/navdy/client/debug/MainDebugActivity$MainFragment;-><init>()V

    const-string v7, "main"

    invoke-virtual {v2, v5, v6, v7}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 161
    const/16 v5, 0x1003

    invoke-virtual {v2, v5}, Landroid/app/FragmentTransaction;->setTransition(I)Landroid/app/FragmentTransaction;

    .line 162
    invoke-virtual {v2}, Landroid/app/FragmentTransaction;->commit()I

    .line 164
    .end local v2    # "ft":Landroid/app/FragmentTransaction;
    :cond_0
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 123
    invoke-super {p0}, Landroid/app/ListFragment;->onPause()V

    .line 124
    iget-object v0, p0, Lcom/navdy/client/debug/DevicePickerFragment;->mRemoteDeviceRegistry:Lcom/navdy/service/library/device/RemoteDeviceRegistry;

    invoke-virtual {v0}, Lcom/navdy/service/library/device/RemoteDeviceRegistry;->stopScanning()V

    .line 125
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 114
    invoke-super {p0}, Landroid/app/ListFragment;->onResume()V

    .line 115
    invoke-virtual {p0}, Lcom/navdy/client/debug/DevicePickerFragment;->refreshCurrentDevice()V

    .line 118
    iget-object v0, p0, Lcom/navdy/client/debug/DevicePickerFragment;->mRemoteDeviceRegistry:Lcom/navdy/service/library/device/RemoteDeviceRegistry;

    invoke-virtual {v0}, Lcom/navdy/service/library/device/RemoteDeviceRegistry;->startScanning()V

    .line 119
    return-void
.end method

.method public refreshCurrentDevice()V
    .locals 3

    .prologue
    .line 133
    invoke-virtual {p0}, Lcom/navdy/client/debug/DevicePickerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/navdy/service/library/device/RemoteDeviceRegistry;->getInstance(Landroid/content/Context;)Lcom/navdy/service/library/device/RemoteDeviceRegistry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/service/library/device/RemoteDeviceRegistry;->getDefaultConnectionInfo()Lcom/navdy/service/library/device/connection/ConnectionInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/debug/DevicePickerFragment;->mCurrentDevice:Lcom/navdy/service/library/device/connection/ConnectionInfo;

    .line 134
    iget-object v0, p0, Lcom/navdy/client/debug/DevicePickerFragment;->mCurrentDevice:Lcom/navdy/service/library/device/connection/ConnectionInfo;

    if-eqz v0, :cond_0

    .line 135
    iget-object v0, p0, Lcom/navdy/client/debug/DevicePickerFragment;->mTextViewCurrentDevice:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Current device: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/client/debug/DevicePickerFragment;->mCurrentDevice:Lcom/navdy/service/library/device/connection/ConnectionInfo;

    invoke-static {v2}, Lcom/navdy/client/debug/devicepicker/Device;->prettyName(Lcom/navdy/service/library/device/connection/ConnectionInfo;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 138
    iget-object v0, p0, Lcom/navdy/client/debug/DevicePickerFragment;->mCurrentDevice:Lcom/navdy/service/library/device/connection/ConnectionInfo;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/navdy/client/debug/DevicePickerFragment;->addConnectionInfo(Lcom/navdy/service/library/device/connection/ConnectionInfo;Z)V

    .line 140
    :cond_0
    return-void
.end method
