.class public final Lcom/navdy/client/R$color;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "color"
.end annotation


# static fields
.field public static final abc_background_cache_hint_selector_material_dark:I = 0x7f0f00ce

.field public static final abc_background_cache_hint_selector_material_light:I = 0x7f0f00cf

.field public static final abc_btn_colored_borderless_text_material:I = 0x7f0f00d0

.field public static final abc_btn_colored_text_material:I = 0x7f0f00d1

.field public static final abc_color_highlight_material:I = 0x7f0f00d2

.field public static final abc_hint_foreground_material_dark:I = 0x7f0f00d3

.field public static final abc_hint_foreground_material_light:I = 0x7f0f00d4

.field public static final abc_input_method_navigation_guard:I = 0x7f0f0003

.field public static final abc_primary_text_disable_only_material_dark:I = 0x7f0f00d5

.field public static final abc_primary_text_disable_only_material_light:I = 0x7f0f00d6

.field public static final abc_primary_text_material_dark:I = 0x7f0f00d7

.field public static final abc_primary_text_material_light:I = 0x7f0f00d8

.field public static final abc_search_url_text:I = 0x7f0f00d9

.field public static final abc_search_url_text_normal:I = 0x7f0f0004

.field public static final abc_search_url_text_pressed:I = 0x7f0f0005

.field public static final abc_search_url_text_selected:I = 0x7f0f0006

.field public static final abc_secondary_text_material_dark:I = 0x7f0f00da

.field public static final abc_secondary_text_material_light:I = 0x7f0f00db

.field public static final abc_tint_btn_checkable:I = 0x7f0f00dc

.field public static final abc_tint_default:I = 0x7f0f00dd

.field public static final abc_tint_edittext:I = 0x7f0f00de

.field public static final abc_tint_seek_thumb:I = 0x7f0f00df

.field public static final abc_tint_spinner:I = 0x7f0f00e0

.field public static final abc_tint_switch_thumb:I = 0x7f0f00e1

.field public static final abc_tint_switch_track:I = 0x7f0f00e2

.field public static final accent_material_dark:I = 0x7f0f0007

.field public static final accent_material_light:I = 0x7f0f0008

.field public static final action_mode_background_color:I = 0x7f0f0009

.field public static final article_attachment_list_divider_color:I = 0x7f0f000a

.field public static final article_attachment_row_background_color:I = 0x7f0f000b

.field public static final background_floating_material_dark:I = 0x7f0f000c

.field public static final background_floating_material_light:I = 0x7f0f000d

.field public static final background_material_dark:I = 0x7f0f000e

.field public static final background_material_light:I = 0x7f0f000f

.field public static final black:I = 0x7f0f0010

.field public static final black_high:I = 0x7f0f0011

.field public static final blue:I = 0x7f0f0012

.field public static final blue_full:I = 0x7f0f0013

.field public static final blue_here_maps:I = 0x7f0f0014

.field public static final blue_high:I = 0x7f0f0015

.field public static final blue_text_color:I = 0x7f0f00e3

.field public static final blue_warm:I = 0x7f0f0016

.field public static final bright_foreground_disabled_material_dark:I = 0x7f0f0017

.field public static final bright_foreground_disabled_material_light:I = 0x7f0f0018

.field public static final bright_foreground_inverse_material_dark:I = 0x7f0f0019

.field public static final bright_foreground_inverse_material_light:I = 0x7f0f001a

.field public static final bright_foreground_material_dark:I = 0x7f0f001b

.field public static final bright_foreground_material_light:I = 0x7f0f001c

.field public static final button_material_dark:I = 0x7f0f001d

.field public static final button_material_light:I = 0x7f0f001e

.field public static final cardview_dark_background:I = 0x7f0f001f

.field public static final cardview_light_background:I = 0x7f0f0020

.field public static final cardview_shadow_end_color:I = 0x7f0f0021

.field public static final cardview_shadow_start_color:I = 0x7f0f0022

.field public static final common_action_bar_splitter:I = 0x7f0f0023

.field public static final common_google_signin_btn_text_dark:I = 0x7f0f00e4

.field public static final common_google_signin_btn_text_dark_default:I = 0x7f0f0024

.field public static final common_google_signin_btn_text_dark_disabled:I = 0x7f0f0025

.field public static final common_google_signin_btn_text_dark_focused:I = 0x7f0f0026

.field public static final common_google_signin_btn_text_dark_pressed:I = 0x7f0f0027

.field public static final common_google_signin_btn_text_light:I = 0x7f0f00e5

.field public static final common_google_signin_btn_text_light_default:I = 0x7f0f0028

.field public static final common_google_signin_btn_text_light_disabled:I = 0x7f0f0029

.field public static final common_google_signin_btn_text_light_focused:I = 0x7f0f002a

.field public static final common_google_signin_btn_text_light_pressed:I = 0x7f0f002b

.field public static final common_google_signin_btn_tint:I = 0x7f0f00e6

.field public static final common_plus_signin_btn_text_dark:I = 0x7f0f00e7

.field public static final common_plus_signin_btn_text_dark_default:I = 0x7f0f002c

.field public static final common_plus_signin_btn_text_dark_disabled:I = 0x7f0f002d

.field public static final common_plus_signin_btn_text_dark_focused:I = 0x7f0f002e

.field public static final common_plus_signin_btn_text_dark_pressed:I = 0x7f0f002f

.field public static final common_plus_signin_btn_text_light:I = 0x7f0f00e8

.field public static final common_plus_signin_btn_text_light_default:I = 0x7f0f0030

.field public static final common_plus_signin_btn_text_light_disabled:I = 0x7f0f0031

.field public static final common_plus_signin_btn_text_light_focused:I = 0x7f0f0032

.field public static final common_plus_signin_btn_text_light_pressed:I = 0x7f0f0033

.field public static final contact_fragment_seperator_body:I = 0x7f0f0034

.field public static final contact_fragment_seperator_border_color:I = 0x7f0f0035

.field public static final crop__button_bar:I = 0x7f0f0036

.field public static final crop__button_text:I = 0x7f0f0037

.field public static final crop__selector_focused:I = 0x7f0f0038

.field public static final crop__selector_pressed:I = 0x7f0f0000

.field public static final design_bottom_navigation_shadow_color:I = 0x7f0f0039

.field public static final design_error:I = 0x7f0f00e9

.field public static final design_fab_shadow_end_color:I = 0x7f0f003a

.field public static final design_fab_shadow_mid_color:I = 0x7f0f003b

.field public static final design_fab_shadow_start_color:I = 0x7f0f003c

.field public static final design_fab_stroke_end_inner_color:I = 0x7f0f003d

.field public static final design_fab_stroke_end_outer_color:I = 0x7f0f003e

.field public static final design_fab_stroke_top_inner_color:I = 0x7f0f003f

.field public static final design_fab_stroke_top_outer_color:I = 0x7f0f0040

.field public static final design_snackbar_background_color:I = 0x7f0f0041

.field public static final design_textinput_error_color_dark:I = 0x7f0f0042

.field public static final design_textinput_error_color_light:I = 0x7f0f0043

.field public static final design_tint_password_toggle:I = 0x7f0f00ea

.field public static final dim_foreground_disabled_material_dark:I = 0x7f0f0044

.field public static final dim_foreground_disabled_material_light:I = 0x7f0f0045

.field public static final dim_foreground_material_dark:I = 0x7f0f0046

.field public static final dim_foreground_material_light:I = 0x7f0f0047

.field public static final disabled_color:I = 0x7f0f0048

.field public static final disabled_text_color:I = 0x7f0f0049

.field public static final enabled_text_color:I = 0x7f0f004a

.field public static final eta_calendar:I = 0x7f0f004b

.field public static final eta_default:I = 0x7f0f004c

.field public static final eta_home:I = 0x7f0f004d

.field public static final eta_work:I = 0x7f0f004e

.field public static final fallback_text_color:I = 0x7f0f004f

.field public static final feedback_container:I = 0x7f0f0050

.field public static final feedback_divider:I = 0x7f0f0051

.field public static final foreground_material_dark:I = 0x7f0f0052

.field public static final foreground_material_light:I = 0x7f0f0053

.field public static final full_transparent:I = 0x7f0f0054

.field public static final green:I = 0x7f0f0055

.field public static final green_high:I = 0x7f0f0056

.field public static final green_lime:I = 0x7f0f0057

.field public static final green_success:I = 0x7f0f0058

.field public static final grey:I = 0x7f0f0059

.field public static final grey_1:I = 0x7f0f005a

.field public static final grey_2:I = 0x7f0f005b

.field public static final grey_3:I = 0x7f0f005c

.field public static final grey_4:I = 0x7f0f005d

.field public static final grey_cool:I = 0x7f0f005e

.field public static final grey_dark:I = 0x7f0f005f

.field public static final grey_dark_card:I = 0x7f0f0060

.field public static final grey_help:I = 0x7f0f0061

.field public static final grey_here_maps:I = 0x7f0f0062

.field public static final grey_hint:I = 0x7f0f0063

.field public static final grey_light:I = 0x7f0f0064

.field public static final grey_light_card:I = 0x7f0f0065

.field public static final grey_spacer:I = 0x7f0f0066

.field public static final grey_super_dark:I = 0x7f0f0067

.field public static final grey_text:I = 0x7f0f0068

.field public static final help_divider_color:I = 0x7f0f0069

.field public static final highlighted_text_material_dark:I = 0x7f0f006a

.field public static final highlighted_text_material_light:I = 0x7f0f006b

.field public static final hockeyapp_background_header:I = 0x7f0f006c

.field public static final hockeyapp_background_light:I = 0x7f0f006d

.field public static final hockeyapp_background_white:I = 0x7f0f006e

.field public static final hockeyapp_button_background:I = 0x7f0f006f

.field public static final hockeyapp_button_background_pressed:I = 0x7f0f0070

.field public static final hockeyapp_button_background_selected:I = 0x7f0f0071

.field public static final hockeyapp_text_black:I = 0x7f0f0072

.field public static final hockeyapp_text_light:I = 0x7f0f0073

.field public static final hockeyapp_text_normal:I = 0x7f0f0074

.field public static final hockeyapp_text_white:I = 0x7f0f0075

.field public static final icon_user_bg_0:I = 0x7f0f0076

.field public static final icon_user_bg_1:I = 0x7f0f0077

.field public static final icon_user_bg_2:I = 0x7f0f0078

.field public static final icon_user_bg_3:I = 0x7f0f0079

.field public static final icon_user_bg_4:I = 0x7f0f007a

.field public static final icon_user_bg_5:I = 0x7f0f007b

.field public static final icon_user_bg_6:I = 0x7f0f007c

.field public static final icon_user_bg_7:I = 0x7f0f007d

.field public static final icon_user_bg_8:I = 0x7f0f007e

.field public static final maps_accuracy_circle_fill_color:I = 0x7f0f007f

.field public static final maps_accuracy_circle_line_color:I = 0x7f0f0080

.field public static final maps_bubble_highlight:I = 0x7f0f0081

.field public static final maps_floorpicker_black:I = 0x7f0f0082

.field public static final maps_floorpicker_text:I = 0x7f0f0083

.field public static final maps_qu_google_blue_500:I = 0x7f0f0084

.field public static final maps_qu_google_yellow_500:I = 0x7f0f0085

.field public static final material_blue_grey_800:I = 0x7f0f0086

.field public static final material_blue_grey_900:I = 0x7f0f0087

.field public static final material_blue_grey_950:I = 0x7f0f0088

.field public static final material_deep_teal_200:I = 0x7f0f0089

.field public static final material_deep_teal_500:I = 0x7f0f008a

.field public static final material_grey_100:I = 0x7f0f008b

.field public static final material_grey_300:I = 0x7f0f008c

.field public static final material_grey_50:I = 0x7f0f008d

.field public static final material_grey_600:I = 0x7f0f008e

.field public static final material_grey_800:I = 0x7f0f008f

.field public static final material_grey_850:I = 0x7f0f0090

.field public static final material_grey_900:I = 0x7f0f0091

.field public static final navdy_blue:I = 0x7f0f0092

.field public static final no_network_view_container_background_color:I = 0x7f0f0093

.field public static final no_network_view_separator_color:I = 0x7f0f0094

.field public static final no_network_view_text_color:I = 0x7f0f0095

.field public static final notification_action_color_filter:I = 0x7f0f0001

.field public static final notification_icon_bg_color:I = 0x7f0f0096

.field public static final notification_material_background_media_default_color:I = 0x7f0f0097

.field public static final orange:I = 0x7f0f0098

.field public static final orange_here_maps:I = 0x7f0f0099

.field public static final peach:I = 0x7f0f009a

.field public static final pink:I = 0x7f0f009b

.field public static final place_autocomplete_prediction_primary_text:I = 0x7f0f009c

.field public static final place_autocomplete_prediction_primary_text_highlight:I = 0x7f0f009d

.field public static final place_autocomplete_prediction_secondary_text:I = 0x7f0f009e

.field public static final place_autocomplete_search_hint:I = 0x7f0f009f

.field public static final place_autocomplete_search_text:I = 0x7f0f00a0

.field public static final place_autocomplete_separator:I = 0x7f0f00a1

.field public static final primary_dark_material_dark:I = 0x7f0f00a2

.field public static final primary_dark_material_light:I = 0x7f0f00a3

.field public static final primary_material_dark:I = 0x7f0f00a4

.field public static final primary_material_light:I = 0x7f0f00a5

.field public static final primary_text_default_material_dark:I = 0x7f0f00a6

.field public static final primary_text_default_material_light:I = 0x7f0f00a7

.field public static final primary_text_disabled_material_dark:I = 0x7f0f00a8

.field public static final primary_text_disabled_material_light:I = 0x7f0f00a9

.field public static final progress_second_half:I = 0x7f0f00aa

.field public static final progress_second_half_bg:I = 0x7f0f00ab

.field public static final purple:I = 0x7f0f00ac

.field public static final purple_high:I = 0x7f0f00ad

.field public static final purple_warm:I = 0x7f0f00ae

.field public static final red:I = 0x7f0f00af

.field public static final red_failure:I = 0x7f0f00b0

.field public static final red_here_maps:I = 0x7f0f00b1

.field public static final red_high:I = 0x7f0f00b2

.field public static final red_warm:I = 0x7f0f00b3

.field public static final request_error_message:I = 0x7f0f00b4

.field public static final request_list_divider_color:I = 0x7f0f00b5

.field public static final request_list_retry_button_text_color:I = 0x7f0f00b6

.field public static final request_list_unread_indicator_color:I = 0x7f0f00b7

.field public static final request_view_comment_list_divider:I = 0x7f0f00b8

.field public static final retry_view_container_background_color:I = 0x7f0f00b9

.field public static final retry_view_separator_color:I = 0x7f0f00ba

.field public static final retry_view_text_color:I = 0x7f0f00bb

.field public static final ripple_material_dark:I = 0x7f0f00bc

.field public static final ripple_material_light:I = 0x7f0f00bd

.field public static final search_header_offline_background:I = 0x7f0f00be

.field public static final search_header_offline_text:I = 0x7f0f00bf

.field public static final search_header_online_background:I = 0x7f0f00c0

.field public static final search_header_online_text:I = 0x7f0f00c1

.field public static final search_result_text:I = 0x7f0f00c2

.field public static final secondary_text_default_material_dark:I = 0x7f0f00c3

.field public static final secondary_text_default_material_light:I = 0x7f0f00c4

.field public static final secondary_text_disabled_material_dark:I = 0x7f0f00c5

.field public static final secondary_text_disabled_material_light:I = 0x7f0f00c6

.field public static final switch_thumb_disabled_material_dark:I = 0x7f0f00c7

.field public static final switch_thumb_disabled_material_light:I = 0x7f0f00c8

.field public static final switch_thumb_material_dark:I = 0x7f0f00eb

.field public static final switch_thumb_material_light:I = 0x7f0f00ec

.field public static final switch_thumb_normal_material_dark:I = 0x7f0f00c9

.field public static final switch_thumb_normal_material_light:I = 0x7f0f00ca

.field public static final tabs_cover_color:I = 0x7f0f0002

.field public static final text_color:I = 0x7f0f00ed

.field public static final transparent:I = 0x7f0f00cb

.field public static final white:I = 0x7f0f00cc

.field public static final yellow:I = 0x7f0f00cd


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4388
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
