.class public final enum Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;
.super Ljava/lang/Enum;
.source "GooglePlacesSearch.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/framework/search/GooglePlacesSearch;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Query"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;

.field public static final enum DETAILS:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;

.field public static final enum DIRECTIONS:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;

.field public static final enum NEARBY:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;

.field public static final enum QUERY_AUTOCOMPLETE:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;

.field public static final enum TEXTSEARCH:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 84
    new-instance v0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;

    const-string v1, "QUERY_AUTOCOMPLETE"

    invoke-direct {v0, v1, v2}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;->QUERY_AUTOCOMPLETE:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;

    .line 85
    new-instance v0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;

    const-string v1, "TEXTSEARCH"

    invoke-direct {v0, v1, v3}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;->TEXTSEARCH:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;

    .line 86
    new-instance v0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;

    const-string v1, "DETAILS"

    invoke-direct {v0, v1, v4}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;->DETAILS:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;

    .line 87
    new-instance v0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;

    const-string v1, "NEARBY"

    invoke-direct {v0, v1, v5}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;->NEARBY:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;

    .line 88
    new-instance v0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;

    const-string v1, "DIRECTIONS"

    invoke-direct {v0, v1, v6}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;->DIRECTIONS:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;

    .line 83
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;

    sget-object v1, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;->QUERY_AUTOCOMPLETE:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;->TEXTSEARCH:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;->DETAILS:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;->NEARBY:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;

    aput-object v1, v0, v5

    sget-object v1, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;->DIRECTIONS:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;

    aput-object v1, v0, v6

    sput-object v0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;->$VALUES:[Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 83
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static getBaseUrl(Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;)Ljava/lang/String;
    .locals 2
    .param p0, "query"    # Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;

    .prologue
    .line 91
    sget-object v0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$3;->$SwitchMap$com$navdy$client$app$framework$search$GooglePlacesSearch$Query:[I

    invoke-virtual {p0}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 102
    const-string v0, "https://maps.googleapis.com/maps/api/place/nearbysearch/json?"

    :goto_0
    return-object v0

    .line 93
    :pswitch_0
    const-string v0, "https://maps.googleapis.com/maps/api/place/queryautocomplete/json?"

    goto :goto_0

    .line 95
    :pswitch_1
    const-string v0, "https://maps.googleapis.com/maps/api/place/textsearch/json?"

    goto :goto_0

    .line 97
    :pswitch_2
    const-string v0, "https://maps.googleapis.com/maps/api/place/details/json?"

    goto :goto_0

    .line 99
    :pswitch_3
    const-string v0, "https://maps.googleapis.com/maps/api/directions/json?"

    goto :goto_0

    .line 91
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 83
    const-class v0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;

    return-object v0
.end method

.method public static values()[Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;
    .locals 1

    .prologue
    .line 83
    sget-object v0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;->$VALUES:[Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;

    invoke-virtual {v0}, [Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;

    return-object v0
.end method
