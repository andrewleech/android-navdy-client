.class Lcom/navdy/client/app/framework/search/NavdySearch$1;
.super Ljava/lang/Object;
.source "NavdySearch.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/framework/search/NavdySearch;->runSearch(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/framework/search/NavdySearch;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/search/NavdySearch;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/framework/search/NavdySearch;

    .prologue
    .line 127
    iput-object p1, p0, Lcom/navdy/client/app/framework/search/NavdySearch$1;->this$0:Lcom/navdy/client/app/framework/search/NavdySearch;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 130
    new-instance v1, Lcom/navdy/service/library/events/places/PlacesSearchResponse$Builder;

    invoke-direct {v1}, Lcom/navdy/service/library/events/places/PlacesSearchResponse$Builder;-><init>()V

    iget-object v2, p0, Lcom/navdy/client/app/framework/search/NavdySearch$1;->this$0:Lcom/navdy/client/app/framework/search/NavdySearch;

    .line 131
    invoke-static {v2}, Lcom/navdy/client/app/framework/search/NavdySearch;->access$000(Lcom/navdy/client/app/framework/search/NavdySearch;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/events/places/PlacesSearchResponse$Builder;->searchQuery(Ljava/lang/String;)Lcom/navdy/service/library/events/places/PlacesSearchResponse$Builder;

    move-result-object v1

    sget-object v2, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_UNKNOWN_ERROR:Lcom/navdy/service/library/events/RequestStatus;

    .line 132
    invoke-virtual {v1, v2}, Lcom/navdy/service/library/events/places/PlacesSearchResponse$Builder;->status(Lcom/navdy/service/library/events/RequestStatus;)Lcom/navdy/service/library/events/places/PlacesSearchResponse$Builder;

    move-result-object v1

    const-string v2, "Hud Search timed out"

    .line 133
    invoke-virtual {v1, v2}, Lcom/navdy/service/library/events/places/PlacesSearchResponse$Builder;->statusDetail(Ljava/lang/String;)Lcom/navdy/service/library/events/places/PlacesSearchResponse$Builder;

    move-result-object v1

    new-instance v2, Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 134
    invoke-virtual {v1, v2}, Lcom/navdy/service/library/events/places/PlacesSearchResponse$Builder;->results(Ljava/util/List;)Lcom/navdy/service/library/events/places/PlacesSearchResponse$Builder;

    move-result-object v1

    .line 135
    invoke-virtual {v1}, Lcom/navdy/service/library/events/places/PlacesSearchResponse$Builder;->build()Lcom/navdy/service/library/events/places/PlacesSearchResponse;

    move-result-object v0

    .line 136
    .local v0, "placesSearchResponse":Lcom/navdy/service/library/events/places/PlacesSearchResponse;
    iget-object v1, p0, Lcom/navdy/client/app/framework/search/NavdySearch$1;->this$0:Lcom/navdy/client/app/framework/search/NavdySearch;

    invoke-virtual {v1, v0}, Lcom/navdy/client/app/framework/search/NavdySearch;->onPlacesSearchResponse(Lcom/navdy/service/library/events/places/PlacesSearchResponse;)V

    .line 137
    return-void
.end method
