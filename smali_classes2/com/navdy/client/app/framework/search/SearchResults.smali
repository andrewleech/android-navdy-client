.class public Lcom/navdy/client/app/framework/search/SearchResults;
.super Ljava/lang/Object;
.source "SearchResults.java"


# instance fields
.field private contactDestinations:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/client/app/framework/models/Destination;",
            ">;"
        }
    .end annotation
.end field

.field private contacts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/client/app/framework/models/ContactModel;",
            ">;"
        }
    .end annotation
.end field

.field private destinationsFromDatabase:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/client/app/framework/models/Destination;",
            ">;"
        }
    .end annotation
.end field

.field private googleResults:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;",
            ">;"
        }
    .end annotation
.end field

.field private hudSearchResults:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/places/PlacesSearchResult;",
            ">;"
        }
    .end annotation
.end field

.field private query:Ljava/lang/String;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private getContactDestinations()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/client/app/framework/models/Destination;",
            ">;"
        }
    .end annotation

    .prologue
    .line 66
    iget-object v0, p0, Lcom/navdy/client/app/framework/search/SearchResults;->contactDestinations:Ljava/util/List;

    if-nez v0, :cond_0

    .line 67
    iget-object v0, p0, Lcom/navdy/client/app/framework/search/SearchResults;->contacts:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/navdy/client/app/framework/search/SearchResults;->getDestinationListFromContactResults(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/framework/search/SearchResults;->contactDestinations:Ljava/util/List;

    .line 69
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/framework/search/SearchResults;->contactDestinations:Ljava/util/List;

    return-object v0
.end method

.method private getDestinationListFromContactResults(Ljava/util/List;)Ljava/util/List;
    .locals 10
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/client/app/framework/models/ContactModel;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/client/app/framework/models/Destination;",
            ">;"
        }
    .end annotation

    .prologue
    .line 95
    .local p1, "contacts":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/ContactModel;>;"
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 96
    :cond_0
    const/4 v3, 0x0

    .line 120
    :cond_1
    return-object v3

    .line 99
    :cond_2
    new-instance v3, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v5

    invoke-direct {v3, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 100
    .local v3, "destinations":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/Destination;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/client/app/framework/models/ContactModel;

    .line 101
    .local v1, "contact":Lcom/navdy/client/app/framework/models/ContactModel;
    if-eqz v1, :cond_3

    .line 105
    iget-object v6, v1, Lcom/navdy/client/app/framework/models/ContactModel;->addresses:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_4
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/framework/models/Address;

    .line 106
    .local v0, "address":Lcom/navdy/client/app/framework/models/Address;
    invoke-static {v0}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->getCleanAddressString(Lcom/navdy/client/app/framework/models/Address;)Ljava/lang/String;

    move-result-object v4

    .line 107
    .local v4, "fullAddress":Ljava/lang/String;
    invoke-static {v4}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_4

    .line 110
    new-instance v2, Lcom/navdy/client/app/framework/models/Destination;

    invoke-direct {v2}, Lcom/navdy/client/app/framework/models/Destination;-><init>()V

    .line 111
    .local v2, "destination":Lcom/navdy/client/app/framework/models/Destination;
    iget-object v7, v1, Lcom/navdy/client/app/framework/models/ContactModel;->name:Ljava/lang/String;

    iput-object v7, v2, Lcom/navdy/client/app/framework/models/Destination;->name:Ljava/lang/String;

    .line 112
    iput-object v4, v2, Lcom/navdy/client/app/framework/models/Destination;->rawAddressNotForDisplay:Ljava/lang/String;

    .line 113
    sget-object v7, Lcom/navdy/client/app/framework/models/Destination$SearchType;->CONTACT:Lcom/navdy/client/app/framework/models/Destination$SearchType;

    invoke-virtual {v7}, Lcom/navdy/client/app/framework/models/Destination$SearchType;->getValue()I

    move-result v7

    iput v7, v2, Lcom/navdy/client/app/framework/models/Destination;->searchResultType:I

    .line 114
    iget-object v7, v1, Lcom/navdy/client/app/framework/models/ContactModel;->lookupKey:Ljava/lang/String;

    iput-object v7, v2, Lcom/navdy/client/app/framework/models/Destination;->contactLookupKey:Ljava/lang/String;

    .line 115
    iget-wide v8, v1, Lcom/navdy/client/app/framework/models/ContactModel;->id:J

    iput-wide v8, v2, Lcom/navdy/client/app/framework/models/Destination;->lastKnownContactId:J

    .line 116
    iget-wide v8, v1, Lcom/navdy/client/app/framework/models/ContactModel;->lookupTimestamp:J

    iput-wide v8, v2, Lcom/navdy/client/app/framework/models/Destination;->lastContactLookup:J

    .line 117
    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static getDestinationListFromGoogleTextSearchResults(Ljava/util/List;)Ljava/util/ArrayList;
    .locals 5
    .param p0    # Ljava/util/List;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/client/app/framework/models/Destination;",
            ">;"
        }
    .end annotation

    .prologue
    .line 138
    .local p0, "googleTextSearchResults":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 139
    .local v1, "destinations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/client/app/framework/models/Destination;>;"
    if-eqz p0, :cond_0

    .line 140
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;

    .line 141
    .local v2, "googleTextSearchDestinationResult":Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;
    new-instance v0, Lcom/navdy/client/app/framework/models/Destination;

    invoke-direct {v0}, Lcom/navdy/client/app/framework/models/Destination;-><init>()V

    .line 142
    .local v0, "destination":Lcom/navdy/client/app/framework/models/Destination;
    sget-object v4, Lcom/navdy/client/app/framework/models/Destination$SearchType;->TEXT_SEARCH:Lcom/navdy/client/app/framework/models/Destination$SearchType;

    invoke-virtual {v2, v4, v0}, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->toModelDestinationObject(Lcom/navdy/client/app/framework/models/Destination$SearchType;Lcom/navdy/client/app/framework/models/Destination;)V

    .line 143
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 146
    .end local v0    # "destination":Lcom/navdy/client/app/framework/models/Destination;
    .end local v2    # "googleTextSearchDestinationResult":Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;
    :cond_0
    return-object v1
.end method

.method static getDestinationListFromHudSearchResults(Ljava/util/List;)Ljava/util/ArrayList;
    .locals 5
    .param p0    # Ljava/util/List;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/places/PlacesSearchResult;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/client/app/framework/models/Destination;",
            ">;"
        }
    .end annotation

    .prologue
    .line 125
    .local p0, "placesSearchResults":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/places/PlacesSearchResult;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 126
    .local v1, "destinations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/client/app/framework/models/Destination;>;"
    if-eqz p0, :cond_0

    .line 127
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/navdy/service/library/events/places/PlacesSearchResult;

    .line 128
    .local v2, "placesSearchResult":Lcom/navdy/service/library/events/places/PlacesSearchResult;
    new-instance v0, Lcom/navdy/client/app/framework/models/Destination;

    invoke-direct {v0}, Lcom/navdy/client/app/framework/models/Destination;-><init>()V

    .line 129
    .local v0, "destination":Lcom/navdy/client/app/framework/models/Destination;
    invoke-static {v2, v0}, Lcom/navdy/client/app/framework/models/Destination;->placesSearchResultToDestinationObject(Lcom/navdy/service/library/events/places/PlacesSearchResult;Lcom/navdy/client/app/framework/models/Destination;)V

    .line 130
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 133
    .end local v0    # "destination":Lcom/navdy/client/app/framework/models/Destination;
    .end local v2    # "placesSearchResult":Lcom/navdy/service/library/events/places/PlacesSearchResult;
    :cond_0
    return-object v1
.end method

.method public static isEmpty(Lcom/navdy/client/app/framework/search/SearchResults;)Z
    .locals 2
    .param p0, "sr"    # Lcom/navdy/client/app/framework/search/SearchResults;

    .prologue
    .line 153
    invoke-direct {p0}, Lcom/navdy/client/app/framework/search/SearchResults;->getContactDestinations()Ljava/util/List;

    move-result-object v0

    .line 154
    .local v0, "contactDestinations":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/Destination;>;"
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_4

    :cond_0
    iget-object v1, p0, Lcom/navdy/client/app/framework/search/SearchResults;->googleResults:Ljava/util/List;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/navdy/client/app/framework/search/SearchResults;->googleResults:Ljava/util/List;

    .line 155
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_4

    :cond_1
    iget-object v1, p0, Lcom/navdy/client/app/framework/search/SearchResults;->hudSearchResults:Ljava/util/List;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/navdy/client/app/framework/search/SearchResults;->hudSearchResults:Ljava/util/List;

    .line 156
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_4

    :cond_2
    iget-object v1, p0, Lcom/navdy/client/app/framework/search/SearchResults;->destinationsFromDatabase:Ljava/util/ArrayList;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/navdy/client/app/framework/search/SearchResults;->destinationsFromDatabase:Ljava/util/ArrayList;

    .line 157
    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_4

    :cond_3
    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_4
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getContacts()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/client/app/framework/models/ContactModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 62
    iget-object v0, p0, Lcom/navdy/client/app/framework/search/SearchResults;->contacts:Ljava/util/List;

    return-object v0
.end method

.method public getDestinationsFromDatabase()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/client/app/framework/models/Destination;",
            ">;"
        }
    .end annotation

    .prologue
    .line 77
    iget-object v0, p0, Lcom/navdy/client/app/framework/search/SearchResults;->destinationsFromDatabase:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getDestinationsList()Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/client/app/framework/models/Destination;",
            ">;"
        }
    .end annotation

    .prologue
    .line 162
    new-instance v1, Ljava/util/ArrayList;

    const/4 v4, 0x0

    invoke-direct {v1, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 164
    .local v1, "destinationsList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/Destination;>;"
    invoke-direct {p0}, Lcom/navdy/client/app/framework/search/SearchResults;->getContactDestinations()Ljava/util/List;

    move-result-object v0

    .line 165
    .local v0, "contactDestinations":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/Destination;>;"
    invoke-static {v1, v0}, Lcom/navdy/client/app/framework/models/Destination;->mergeAndDeduplicateLists(Ljava/util/List;Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    .line 167
    iget-object v4, p0, Lcom/navdy/client/app/framework/search/SearchResults;->googleResults:Ljava/util/List;

    invoke-static {v4}, Lcom/navdy/client/app/framework/search/SearchResults;->getDestinationListFromGoogleTextSearchResults(Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v2

    .line 168
    .local v2, "googleSearchDestinations":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/Destination;>;"
    invoke-static {v1, v2}, Lcom/navdy/client/app/framework/models/Destination;->mergeAndDeduplicateLists(Ljava/util/List;Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    .line 170
    iget-object v4, p0, Lcom/navdy/client/app/framework/search/SearchResults;->hudSearchResults:Ljava/util/List;

    invoke-static {v4}, Lcom/navdy/client/app/framework/search/SearchResults;->getDestinationListFromHudSearchResults(Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v3

    .line 171
    .local v3, "hudDestinations":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/Destination;>;"
    invoke-static {v1, v3}, Lcom/navdy/client/app/framework/models/Destination;->mergeAndDeduplicateLists(Ljava/util/List;Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    .line 173
    iget-object v4, p0, Lcom/navdy/client/app/framework/search/SearchResults;->destinationsFromDatabase:Ljava/util/ArrayList;

    invoke-static {v1, v4}, Lcom/navdy/client/app/framework/models/Destination;->mergeAndDeduplicateLists(Ljava/util/List;Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    .line 175
    check-cast v1, Ljava/util/ArrayList;

    .end local v1    # "destinationsList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/Destination;>;"
    return-object v1
.end method

.method public getFirstDestination()Lcom/navdy/client/app/framework/models/Destination;
    .locals 4

    .prologue
    .line 179
    invoke-direct {p0}, Lcom/navdy/client/app/framework/search/SearchResults;->getContactDestinations()Ljava/util/List;

    move-result-object v1

    .line 180
    .local v1, "destinations":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/Destination;>;"
    if-eqz v1, :cond_1

    .line 181
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/framework/models/Destination;

    .line 182
    .local v0, "contactDestination":Lcom/navdy/client/app/framework/models/Destination;
    if-eqz v0, :cond_0

    .line 214
    .end local v0    # "contactDestination":Lcom/navdy/client/app/framework/models/Destination;
    :goto_0
    return-object v0

    .line 188
    :cond_1
    iget-object v2, p0, Lcom/navdy/client/app/framework/search/SearchResults;->googleResults:Ljava/util/List;

    invoke-static {v2}, Lcom/navdy/client/app/framework/search/SearchResults;->getDestinationListFromGoogleTextSearchResults(Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v1

    .line 189
    if-eqz v1, :cond_3

    .line 190
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/framework/models/Destination;

    .line 191
    .restart local v0    # "contactDestination":Lcom/navdy/client/app/framework/models/Destination;
    if-eqz v0, :cond_2

    goto :goto_0

    .line 197
    .end local v0    # "contactDestination":Lcom/navdy/client/app/framework/models/Destination;
    :cond_3
    iget-object v2, p0, Lcom/navdy/client/app/framework/search/SearchResults;->hudSearchResults:Ljava/util/List;

    invoke-static {v2}, Lcom/navdy/client/app/framework/search/SearchResults;->getDestinationListFromHudSearchResults(Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v1

    .line 198
    if-eqz v1, :cond_5

    .line 199
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/framework/models/Destination;

    .line 200
    .restart local v0    # "contactDestination":Lcom/navdy/client/app/framework/models/Destination;
    if-eqz v0, :cond_4

    goto :goto_0

    .line 206
    .end local v0    # "contactDestination":Lcom/navdy/client/app/framework/models/Destination;
    :cond_5
    iget-object v1, p0, Lcom/navdy/client/app/framework/search/SearchResults;->destinationsFromDatabase:Ljava/util/ArrayList;

    .line 207
    if-eqz v1, :cond_7

    .line 208
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_6
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/framework/models/Destination;

    .line 209
    .restart local v0    # "contactDestination":Lcom/navdy/client/app/framework/models/Destination;
    if-eqz v0, :cond_6

    goto :goto_0

    .line 214
    .end local v0    # "contactDestination":Lcom/navdy/client/app/framework/models/Destination;
    :cond_7
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getGoogleResults()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 73
    iget-object v0, p0, Lcom/navdy/client/app/framework/search/SearchResults;->googleResults:Ljava/util/List;

    return-object v0
.end method

.method public getHudSearchResults()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/places/PlacesSearchResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 81
    iget-object v0, p0, Lcom/navdy/client/app/framework/search/SearchResults;->hudSearchResults:Ljava/util/List;

    return-object v0
.end method

.method public getQuery()Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/navdy/client/app/framework/search/SearchResults;->query:Ljava/lang/String;

    return-object v0
.end method

.method public setContacts(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/client/app/framework/models/ContactModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 40
    .local p1, "contacts":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/ContactModel;>;"
    iput-object p1, p0, Lcom/navdy/client/app/framework/search/SearchResults;->contacts:Ljava/util/List;

    .line 41
    invoke-direct {p0, p1}, Lcom/navdy/client/app/framework/search/SearchResults;->getDestinationListFromContactResults(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/framework/search/SearchResults;->contactDestinations:Ljava/util/List;

    .line 42
    return-void
.end method

.method setDestinationsFromDatabase(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/client/app/framework/models/Destination;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 53
    .local p1, "destinationsFromDatabase":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/client/app/framework/models/Destination;>;"
    iput-object p1, p0, Lcom/navdy/client/app/framework/search/SearchResults;->destinationsFromDatabase:Ljava/util/ArrayList;

    .line 54
    return-void
.end method

.method setGoogleResults(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 45
    .local p1, "googleResults":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;>;"
    iput-object p1, p0, Lcom/navdy/client/app/framework/search/SearchResults;->googleResults:Ljava/util/List;

    .line 46
    return-void
.end method

.method setHudSearchResults(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/places/PlacesSearchResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 49
    .local p1, "hudSearchResults":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/places/PlacesSearchResult;>;"
    iput-object p1, p0, Lcom/navdy/client/app/framework/search/SearchResults;->hudSearchResults:Ljava/util/List;

    .line 50
    return-void
.end method

.method public setQuery(Ljava/lang/String;)V
    .locals 0
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    .line 57
    iput-object p1, p0, Lcom/navdy/client/app/framework/search/SearchResults;->query:Ljava/lang/String;

    .line 58
    return-void
.end method
