.class Lcom/navdy/client/app/framework/models/Destination$11;
.super Landroid/os/AsyncTask;
.source "Destination.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/framework/models/Destination;->saveDestinationAsFavoritesAsync(ILcom/navdy/client/app/providers/NavdyContentProvider$QueryResultCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Object;",
        "Ljava/lang/Object;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/framework/models/Destination;

.field final synthetic val$callback:Lcom/navdy/client/app/providers/NavdyContentProvider$QueryResultCallback;

.field final synthetic val$specialType:I


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/models/Destination;ILcom/navdy/client/app/providers/NavdyContentProvider$QueryResultCallback;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/framework/models/Destination;

    .prologue
    .line 2721
    iput-object p1, p0, Lcom/navdy/client/app/framework/models/Destination$11;->this$0:Lcom/navdy/client/app/framework/models/Destination;

    iput p2, p0, Lcom/navdy/client/app/framework/models/Destination$11;->val$specialType:I

    iput-object p3, p0, Lcom/navdy/client/app/framework/models/Destination$11;->val$callback:Lcom/navdy/client/app/providers/NavdyContentProvider$QueryResultCallback;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Object;)Ljava/lang/Integer;
    .locals 2
    .param p1, "params"    # [Ljava/lang/Object;

    .prologue
    .line 2725
    iget-object v0, p0, Lcom/navdy/client/app/framework/models/Destination$11;->this$0:Lcom/navdy/client/app/framework/models/Destination;

    iget v1, p0, Lcom/navdy/client/app/framework/models/Destination$11;->val$specialType:I

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/models/Destination;->access$500(Lcom/navdy/client/app/framework/models/Destination;I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2721
    invoke-virtual {p0, p1}, Lcom/navdy/client/app/framework/models/Destination$11;->doInBackground([Ljava/lang/Object;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Integer;)V
    .locals 3
    .param p1, "nbRows"    # Ljava/lang/Integer;

    .prologue
    .line 2730
    iget-object v0, p0, Lcom/navdy/client/app/framework/models/Destination$11;->val$callback:Lcom/navdy/client/app/providers/NavdyContentProvider$QueryResultCallback;

    if-eqz v0, :cond_0

    .line 2731
    iget-object v0, p0, Lcom/navdy/client/app/framework/models/Destination$11;->val$callback:Lcom/navdy/client/app/providers/NavdyContentProvider$QueryResultCallback;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/navdy/client/app/providers/NavdyContentProvider$QueryResultCallback;->onQueryCompleted(ILandroid/net/Uri;)V

    .line 2733
    :cond_0
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2721
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/navdy/client/app/framework/models/Destination$11;->onPostExecute(Ljava/lang/Integer;)V

    return-void
.end method
