.class public Lcom/navdy/client/app/framework/models/DestinationCacheEntry;
.super Ljava/lang/Object;
.source "DestinationCacheEntry.java"


# instance fields
.field public destination:Lcom/navdy/client/app/framework/models/Destination;

.field public destinationId:I

.field public id:I

.field public lastResponseDate:J

.field public locationString:Ljava/lang/String;


# direct methods
.method public constructor <init>(IJLjava/lang/String;I)V
    .locals 2
    .param p1, "id"    # I
    .param p2, "lastResponseDate"    # J
    .param p4, "locationString"    # Ljava/lang/String;
    .param p5, "destinationId"    # I

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    const/4 v0, -0x1

    iput v0, p0, Lcom/navdy/client/app/framework/models/DestinationCacheEntry;->id:I

    .line 19
    iput p1, p0, Lcom/navdy/client/app/framework/models/DestinationCacheEntry;->id:I

    .line 20
    iput-wide p2, p0, Lcom/navdy/client/app/framework/models/DestinationCacheEntry;->lastResponseDate:J

    .line 21
    iput-object p4, p0, Lcom/navdy/client/app/framework/models/DestinationCacheEntry;->locationString:Ljava/lang/String;

    .line 22
    iput p5, p0, Lcom/navdy/client/app/framework/models/DestinationCacheEntry;->destinationId:I

    .line 23
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 27
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DestinationCacheEntry{id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/navdy/client/app/framework/models/DestinationCacheEntry;->id:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", lastResponseDate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/navdy/client/app/framework/models/DestinationCacheEntry;->lastResponseDate:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", locationString=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/client/app/framework/models/DestinationCacheEntry;->locationString:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", destinationId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/navdy/client/app/framework/models/DestinationCacheEntry;->destinationId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", destination="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/client/app/framework/models/DestinationCacheEntry;->destination:Lcom/navdy/client/app/framework/models/Destination;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
