.class public Lcom/navdy/client/app/framework/models/MountInfo;
.super Ljava/lang/Object;
.source "MountInfo.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/app/framework/models/MountInfo$MountType;
    }
.end annotation


# instance fields
.field public mediumSupported:Z

.field public recommendedMount:Lcom/navdy/client/app/framework/models/MountInfo$MountType;

.field public shortSupported:Z

.field public tallSupported:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    iput-boolean v0, p0, Lcom/navdy/client/app/framework/models/MountInfo;->shortSupported:Z

    .line 11
    iput-boolean v0, p0, Lcom/navdy/client/app/framework/models/MountInfo;->mediumSupported:Z

    .line 12
    iput-boolean v0, p0, Lcom/navdy/client/app/framework/models/MountInfo;->tallSupported:Z

    .line 13
    sget-object v0, Lcom/navdy/client/app/framework/models/MountInfo$MountType;->SHORT:Lcom/navdy/client/app/framework/models/MountInfo$MountType;

    iput-object v0, p0, Lcom/navdy/client/app/framework/models/MountInfo;->recommendedMount:Lcom/navdy/client/app/framework/models/MountInfo$MountType;

    return-void
.end method


# virtual methods
.method public noneOfTheMountsWillWork()Z
    .locals 1

    .prologue
    .line 16
    iget-boolean v0, p0, Lcom/navdy/client/app/framework/models/MountInfo;->shortSupported:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/navdy/client/app/framework/models/MountInfo;->mediumSupported:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/navdy/client/app/framework/models/MountInfo;->tallSupported:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
