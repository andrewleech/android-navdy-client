.class Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$4$2;
.super Ljava/lang/Object;
.source "NavCoordsAddressProcessor.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$4;->onFailure(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$4;

.field final synthetic val$error:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$4;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$4;

    .prologue
    .line 313
    iput-object p1, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$4$2;->this$0:Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$4;

    iput-object p2, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$4$2;->val$error:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 316
    iget-object v0, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$4$2;->val$error:Ljava/lang/String;

    sget-object v2, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;->SERVICE_ERROR:Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;

    invoke-virtual {v2}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;->name()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 317
    invoke-static {}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "callGoogleDirectionsWebApi returned Google service error; calling back failure"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 318
    iget-object v0, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$4$2;->this$0:Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$4;

    iget-object v0, v0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$4;->val$callback:Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$OnCompleteCallback;

    iget-object v1, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$4$2;->this$0:Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$4;

    iget-object v1, v1, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$4;->val$processedDestination:Lcom/navdy/client/app/framework/models/Destination;

    sget-object v2, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;->BAD_COORDS:Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;

    invoke-static {v0, v1, v2}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor;->access$400(Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$OnCompleteCallback;Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;)V

    .line 325
    :goto_0
    return-void

    .line 320
    :cond_0
    invoke-static {}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v2, "callGoogleDirectionsWebApi returned Google error; can\'t do anything else, saving and calling back success"

    invoke-virtual {v0, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 322
    iget-object v0, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$4$2;->this$0:Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$4;

    iget-object v0, v0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$4;->val$processedDestination:Lcom/navdy/client/app/framework/models/Destination;

    sget-object v4, Lcom/navdy/client/app/framework/models/Destination$Precision;->UNKNOWN:Lcom/navdy/client/app/framework/models/Destination$Precision;

    iget-object v2, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$4$2;->this$0:Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$4;

    iget-object v5, v2, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$4;->val$callback:Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$OnCompleteCallback;

    move-object v2, v1

    move-object v3, v1

    invoke-static/range {v0 .. v5}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor;->access$300(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/service/library/events/location/Coordinate;Lcom/navdy/service/library/events/location/Coordinate;Lcom/here/android/mpa/search/Address;Lcom/navdy/client/app/framework/models/Destination$Precision;Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$OnCompleteCallback;)V

    goto :goto_0
.end method
