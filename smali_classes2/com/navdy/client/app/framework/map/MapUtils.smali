.class public Lcom/navdy/client/app/framework/map/MapUtils;
.super Ljava/lang/Object;
.source "MapUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/app/framework/map/MapUtils$UnitSystem;
    }
.end annotation


# static fields
.field private static final EARTH_RADIUS:I = 0x6136b8

.field private static final MAX_LAT:D

.field private static final MAX_LON:D

.field private static final METERS_PER_FOOT:D = 0.3048

.field private static final METERS_PER_KM:I = 0x3e8

.field public static final METERS_PER_MILE:D = 1609.34

.field private static final MIN_LAT:D

.field private static final MIN_LON:D

.field private static final VERBOSE:Z

.field public static final hereMapSidePadding:I

.field public static final hereMapTopDownPadding:I

.field private static final logger:Lcom/navdy/service/library/log/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    .line 50
    new-instance v7, Lcom/navdy/service/library/log/Logger;

    const-class v8, Lcom/navdy/client/app/framework/map/MapUtils;

    invoke-direct {v7, v8}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v7, Lcom/navdy/client/app/framework/map/MapUtils;->logger:Lcom/navdy/service/library/log/Logger;

    .line 59
    const-wide v8, -0x3fa9800000000000L    # -90.0

    invoke-static {v8, v9}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v8

    sput-wide v8, Lcom/navdy/client/app/framework/map/MapUtils;->MIN_LAT:D

    .line 60
    const-wide v8, 0x4056800000000000L    # 90.0

    invoke-static {v8, v9}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v8

    sput-wide v8, Lcom/navdy/client/app/framework/map/MapUtils;->MAX_LAT:D

    .line 61
    const-wide v8, -0x3f99800000000000L    # -180.0

    invoke-static {v8, v9}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v8

    sput-wide v8, Lcom/navdy/client/app/framework/map/MapUtils;->MIN_LON:D

    .line 62
    const-wide v8, 0x4066800000000000L    # 180.0

    invoke-static {v8, v9}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v8

    sput-wide v8, Lcom/navdy/client/app/framework/map/MapUtils;->MAX_LON:D

    .line 68
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 69
    .local v0, "context":Landroid/content/Context;
    const v7, 0x7f02019c

    invoke-static {v0, v7}, Landroid/support/v4/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    check-cast v3, Landroid/graphics/drawable/BitmapDrawable;

    .line 70
    .local v3, "markerDrawable":Landroid/graphics/drawable/BitmapDrawable;
    const v7, 0x7f0201d3

    invoke-static {v0, v7}, Landroid/support/v4/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    check-cast v6, Landroid/graphics/drawable/BitmapDrawable;

    .line 72
    .local v6, "positionMarkerDrawable":Landroid/graphics/drawable/BitmapDrawable;
    invoke-virtual {v3}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v7

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    .line 73
    .local v2, "iconWidth":I
    invoke-virtual {v6}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v7

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    .line 74
    .local v5, "positionIconWidth":I
    invoke-static {v2, v5}, Ljava/lang/Math;->max(II)I

    move-result v7

    sput v7, Lcom/navdy/client/app/framework/map/MapUtils;->hereMapSidePadding:I

    .line 76
    invoke-virtual {v3}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v7

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    .line 77
    .local v1, "iconHeight":I
    invoke-virtual {v6}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v7

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    .line 78
    .local v4, "positionIconHeight":I
    invoke-static {v1, v4}, Ljava/lang/Math;->max(II)I

    move-result v7

    sput v7, Lcom/navdy/client/app/framework/map/MapUtils;->hereMapTopDownPadding:I

    .line 79
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 48
    sget-object v0, Lcom/navdy/client/app/framework/map/MapUtils;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method private static areCloseToEachOther(DDDDDF)Z
    .locals 4
    .param p0, "latitude1"    # D
    .param p2, "longitude1"    # D
    .param p4, "latitude2"    # D
    .param p6, "longitude2"    # D
    .param p8, "coordLimit"    # D
    .param p10, "distanceLimit"    # F

    .prologue
    const-wide/16 v2, 0x0

    .line 261
    cmpl-double v0, p0, v2

    if-nez v0, :cond_0

    cmpl-double v0, p2, v2

    if-eqz v0, :cond_2

    :cond_0
    cmpl-double v0, p4, v2

    if-nez v0, :cond_1

    cmpl-double v0, p6, v2

    if-eqz v0, :cond_2

    :cond_1
    sub-double v0, p0, p4

    .line 262
    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    cmpg-double v0, v0, p8

    if-gez v0, :cond_2

    sub-double v0, p2, p6

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    cmpg-double v0, v0, p8

    if-gez v0, :cond_2

    .line 263
    invoke-static/range {p0 .. p7}, Lcom/navdy/client/app/framework/map/MapUtils;->distanceBetween(DDDD)F

    move-result v0

    cmpg-float v0, v0, p10

    if-gez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static areCloseToEachOther(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/app/framework/models/Destination;DF)Z
    .locals 12
    .param p0, "d1"    # Lcom/navdy/client/app/framework/models/Destination;
    .param p1, "d2"    # Lcom/navdy/client/app/framework/models/Destination;
    .param p2, "coordLimit"    # D
    .param p4, "distanceLimit"    # F

    .prologue
    .line 243
    iget-wide v0, p0, Lcom/navdy/client/app/framework/models/Destination;->displayLat:D

    iget-wide v2, p0, Lcom/navdy/client/app/framework/models/Destination;->displayLong:D

    iget-wide v4, p1, Lcom/navdy/client/app/framework/models/Destination;->displayLat:D

    iget-wide v6, p1, Lcom/navdy/client/app/framework/models/Destination;->displayLong:D

    move-wide v8, p2

    move/from16 v10, p4

    invoke-static/range {v0 .. v10}, Lcom/navdy/client/app/framework/map/MapUtils;->areCloseToEachOther(DDDDDF)Z

    move-result v0

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/navdy/client/app/framework/models/Destination;->navigationLat:D

    iget-wide v2, p0, Lcom/navdy/client/app/framework/models/Destination;->navigationLong:D

    iget-wide v4, p1, Lcom/navdy/client/app/framework/models/Destination;->navigationLat:D

    iget-wide v6, p1, Lcom/navdy/client/app/framework/models/Destination;->navigationLong:D

    move-wide v8, p2

    move/from16 v10, p4

    .line 244
    invoke-static/range {v0 .. v10}, Lcom/navdy/client/app/framework/map/MapUtils;->areCloseToEachOther(DDDDDF)Z

    move-result v0

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/navdy/client/app/framework/models/Destination;->displayLat:D

    iget-wide v2, p0, Lcom/navdy/client/app/framework/models/Destination;->displayLong:D

    iget-wide v4, p1, Lcom/navdy/client/app/framework/models/Destination;->navigationLat:D

    iget-wide v6, p1, Lcom/navdy/client/app/framework/models/Destination;->navigationLong:D

    move-wide v8, p2

    move/from16 v10, p4

    .line 245
    invoke-static/range {v0 .. v10}, Lcom/navdy/client/app/framework/map/MapUtils;->areCloseToEachOther(DDDDDF)Z

    move-result v0

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/navdy/client/app/framework/models/Destination;->navigationLat:D

    iget-wide v2, p0, Lcom/navdy/client/app/framework/models/Destination;->navigationLong:D

    iget-wide v4, p1, Lcom/navdy/client/app/framework/models/Destination;->displayLat:D

    iget-wide v6, p1, Lcom/navdy/client/app/framework/models/Destination;->displayLong:D

    move-wide v8, p2

    move/from16 v10, p4

    .line 246
    invoke-static/range {v0 .. v10}, Lcom/navdy/client/app/framework/map/MapUtils;->areCloseToEachOther(DDDDDF)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static buildNewCoordinate(DD)Lcom/navdy/service/library/events/location/Coordinate;
    .locals 2
    .param p0, "latitude"    # D
    .param p2, "longitude"    # D

    .prologue
    .line 323
    new-instance v0, Lcom/navdy/service/library/events/location/Coordinate$Builder;

    invoke-direct {v0}, Lcom/navdy/service/library/events/location/Coordinate$Builder;-><init>()V

    .line 324
    invoke-static {p0, p1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/location/Coordinate$Builder;->latitude(Ljava/lang/Double;)Lcom/navdy/service/library/events/location/Coordinate$Builder;

    move-result-object v0

    .line 325
    invoke-static {p2, p3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/location/Coordinate$Builder;->longitude(Ljava/lang/Double;)Lcom/navdy/service/library/events/location/Coordinate$Builder;

    move-result-object v0

    .line 326
    invoke-virtual {v0}, Lcom/navdy/service/library/events/location/Coordinate$Builder;->build()Lcom/navdy/service/library/events/location/Coordinate;

    move-result-object v0

    return-object v0
.end method

.method public static distanceBetween(Lcom/navdy/service/library/events/location/Coordinate;Lcom/navdy/service/library/events/location/Coordinate;)D
    .locals 8
    .param p0, "first"    # Lcom/navdy/service/library/events/location/Coordinate;
    .param p1, "second"    # Lcom/navdy/service/library/events/location/Coordinate;
    .annotation build Lorg/jetbrains/annotations/Contract;
        value = "null, _ -> fail; !null, null -> fail"
    .end annotation

    .prologue
    .line 188
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 189
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "one or more arguments are null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 192
    :cond_1
    iget-object v0, p0, Lcom/navdy/service/library/events/location/Coordinate;->latitude:Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    iget-object v2, p0, Lcom/navdy/service/library/events/location/Coordinate;->longitude:Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    iget-object v4, p1, Lcom/navdy/service/library/events/location/Coordinate;->latitude:Ljava/lang/Double;

    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    iget-object v6, p1, Lcom/navdy/service/library/events/location/Coordinate;->longitude:Ljava/lang/Double;

    invoke-virtual {v6}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    invoke-static/range {v0 .. v7}, Lcom/navdy/client/app/framework/map/MapUtils;->distanceBetween(DDDD)F

    move-result v0

    float-to-double v0, v0

    return-wide v0
.end method

.method private static distanceBetween(DDDD)F
    .locals 10
    .param p0, "firstLat"    # D
    .param p2, "firstLng"    # D
    .param p4, "secondLat"    # D
    .param p6, "secondLng"    # D

    .prologue
    .line 223
    const/4 v0, 0x1

    new-array v8, v0, [F

    .local v8, "results":[F
    move-wide v0, p0

    move-wide v2, p2

    move-wide v4, p4

    move-wide/from16 v6, p6

    .line 224
    invoke-static/range {v0 .. v8}, Landroid/location/Location;->distanceBetween(DDDD[F)V

    .line 226
    const/4 v0, 0x0

    aget v0, v8, v0

    return v0
.end method

.method public static distanceBetween(Landroid/location/Location;Landroid/location/Location;)F
    .locals 8
    .param p0, "location1"    # Landroid/location/Location;
    .param p1, "location2"    # Landroid/location/Location;
    .annotation build Lorg/jetbrains/annotations/Contract;
        value = "null, _ -> fail; !null, null -> fail"
    .end annotation

    .prologue
    .line 171
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 172
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "one or more arguments are null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 174
    :cond_1
    invoke-virtual {p0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v0

    invoke-virtual {p0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v4

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v6

    invoke-static/range {v0 .. v7}, Lcom/navdy/client/app/framework/map/MapUtils;->distanceBetween(DDDD)F

    move-result v0

    return v0
.end method

.method public static distanceBetween(Lcom/google/android/gms/maps/model/LatLng;Lcom/google/android/gms/maps/model/LatLng;)F
    .locals 8
    .param p0, "first"    # Lcom/google/android/gms/maps/model/LatLng;
    .param p1, "second"    # Lcom/google/android/gms/maps/model/LatLng;
    .annotation build Lorg/jetbrains/annotations/Contract;
        value = "null, _ -> fail; !null, null -> fail"
    .end annotation

    .prologue
    .line 197
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 198
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "one or more arguments are null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 201
    :cond_1
    iget-wide v0, p0, Lcom/google/android/gms/maps/model/LatLng;->latitude:D

    iget-wide v2, p0, Lcom/google/android/gms/maps/model/LatLng;->longitude:D

    iget-wide v4, p1, Lcom/google/android/gms/maps/model/LatLng;->latitude:D

    iget-wide v6, p1, Lcom/google/android/gms/maps/model/LatLng;->longitude:D

    invoke-static/range {v0 .. v7}, Lcom/navdy/client/app/framework/map/MapUtils;->distanceBetween(DDDD)F

    move-result v0

    return v0
.end method

.method public static distanceBetween(Lcom/navdy/service/library/events/location/Coordinate;Landroid/location/Location;)F
    .locals 8
    .param p0, "coordinate"    # Lcom/navdy/service/library/events/location/Coordinate;
    .param p1, "location"    # Landroid/location/Location;
    .annotation build Lorg/jetbrains/annotations/Contract;
        value = "null, _ -> fail; !null, null -> fail"
    .end annotation

    .prologue
    .line 180
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 181
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "one or more arguments are null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 183
    :cond_1
    iget-object v0, p0, Lcom/navdy/service/library/events/location/Coordinate;->latitude:Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    iget-object v2, p0, Lcom/navdy/service/library/events/location/Coordinate;->longitude:Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v4

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v6

    invoke-static/range {v0 .. v7}, Lcom/navdy/client/app/framework/map/MapUtils;->distanceBetween(DDDD)F

    move-result v0

    return v0
.end method

.method public static distanceBetween(Lcom/navdy/service/library/events/location/Coordinate;Lcom/google/android/gms/maps/model/LatLng;)F
    .locals 8
    .param p0, "coordinate"    # Lcom/navdy/service/library/events/location/Coordinate;
    .param p1, "latLng"    # Lcom/google/android/gms/maps/model/LatLng;
    .annotation build Lorg/jetbrains/annotations/Contract;
        value = "null, _ -> fail; !null, null -> fail"
    .end annotation

    .prologue
    .line 207
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 208
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "one or more arguments are null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 211
    :cond_1
    iget-object v0, p0, Lcom/navdy/service/library/events/location/Coordinate;->latitude:Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    iget-object v2, p0, Lcom/navdy/service/library/events/location/Coordinate;->longitude:Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    iget-wide v4, p1, Lcom/google/android/gms/maps/model/LatLng;->latitude:D

    iget-wide v6, p1, Lcom/google/android/gms/maps/model/LatLng;->longitude:D

    invoke-static/range {v0 .. v7}, Lcom/navdy/client/app/framework/map/MapUtils;->distanceBetween(DDDD)F

    move-result v0

    return v0
.end method

.method public static doReverseGeocodingFor(Lcom/google/android/gms/maps/model/LatLng;Lcom/navdy/client/app/framework/models/Destination;Ljava/lang/Runnable;)V
    .locals 2
    .param p0, "center"    # Lcom/google/android/gms/maps/model/LatLng;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "destination"    # Lcom/navdy/client/app/framework/models/Destination;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "callback"    # Ljava/lang/Runnable;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 347
    invoke-static {p0}, Lcom/navdy/client/app/framework/map/MapUtils;->isValidSetOfCoordinates(Lcom/google/android/gms/maps/model/LatLng;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 348
    sget-object v0, Lcom/navdy/client/app/framework/map/MapUtils;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Trying to reverse geocode a location with invalid coordinates"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 349
    invoke-interface {p2}, Ljava/lang/Runnable;->run()V

    .line 353
    :cond_0
    new-instance v0, Lcom/navdy/client/app/framework/map/MapUtils$1;

    invoke-direct {v0, p0, p1, p2}, Lcom/navdy/client/app/framework/map/MapUtils$1;-><init>(Lcom/google/android/gms/maps/model/LatLng;Lcom/navdy/client/app/framework/models/Destination;Ljava/lang/Runnable;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    .line 408
    invoke-virtual {v0, v1}, Lcom/navdy/client/app/framework/map/MapUtils$1;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 409
    return-void
.end method

.method public static formatDistance(ILcom/navdy/client/app/framework/map/MapUtils$UnitSystem;)Ljava/lang/String;
    .locals 17
    .param p0, "meters"    # I
    .param p1, "unitSystem"    # Lcom/navdy/client/app/framework/map/MapUtils$UnitSystem;

    .prologue
    .line 104
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    .line 106
    .local v8, "resources":Landroid/content/res/Resources;
    sget-object v9, Lcom/navdy/client/app/framework/map/MapUtils$UnitSystem;->METRIC:Lcom/navdy/client/app/framework/map/MapUtils$UnitSystem;

    move-object/from16 v0, p1

    if-ne v0, v9, :cond_2

    .line 107
    move/from16 v0, p0

    int-to-float v9, v0

    const v10, 0x461c4000    # 10000.0f

    cmpl-float v9, v9, v10

    if-ltz v9, :cond_0

    .line 109
    move/from16 v0, p0

    int-to-double v10, v0

    const-wide v12, 0x408f400000000000L    # 1000.0

    div-double/2addr v10, v12

    invoke-static {v10, v11}, Ljava/lang/Math;->round(D)J

    move-result-wide v10

    long-to-double v4, v10

    .line 110
    .local v4, "kms":D
    const v9, 0x7f080272

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    double-to-int v12, v4

    invoke-static {v12}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {v8, v9, v10}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    .line 131
    .end local v4    # "kms":D
    :goto_0
    return-object v9

    .line 111
    :cond_0
    const/16 v9, 0x3e8

    move/from16 v0, p0

    if-le v0, v9, :cond_1

    .line 113
    move/from16 v0, p0

    int-to-double v10, v0

    const-wide v12, 0x408f400000000000L    # 1000.0

    div-double/2addr v10, v12

    const-wide/high16 v12, 0x4024000000000000L    # 10.0

    mul-double/2addr v10, v12

    invoke-static {v10, v11}, Ljava/lang/Math;->round(D)J

    move-result-wide v10

    long-to-double v10, v10

    const-wide/high16 v12, 0x4024000000000000L    # 10.0

    div-double v4, v10, v12

    .line 114
    .restart local v4    # "kms":D
    const v9, 0x7f080272

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v12

    const-string v13, "%.1f"

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v16

    aput-object v16, v14, v15

    invoke-static {v12, v13, v14}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {v8, v9, v10}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    goto :goto_0

    .line 117
    .end local v4    # "kms":D
    :cond_1
    const v9, 0x7f0802c5

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-static/range {p0 .. p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {v8, v9, v10}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    goto :goto_0

    .line 120
    :cond_2
    move/from16 v0, p0

    int-to-double v10, v0

    const-wide v12, 0x40cf6eb333333333L    # 16093.4

    cmpl-double v9, v10, v12

    if-ltz v9, :cond_3

    .line 122
    move/from16 v0, p0

    int-to-double v10, v0

    const-wide v12, 0x4099255c28f5c28fL    # 1609.34

    div-double/2addr v10, v12

    invoke-static {v10, v11}, Ljava/lang/Math;->round(D)J

    move-result-wide v10

    long-to-double v6, v10

    .line 123
    .local v6, "miles":D
    const v9, 0x7f0802c8

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    double-to-int v12, v6

    invoke-static {v12}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {v8, v9, v10}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    goto :goto_0

    .line 124
    .end local v6    # "miles":D
    :cond_3
    move/from16 v0, p0

    int-to-double v10, v0

    const-wide v12, 0x40641de353f7ced9L    # 160.934

    cmpl-double v9, v10, v12

    if-ltz v9, :cond_4

    .line 126
    move/from16 v0, p0

    int-to-double v10, v0

    const-wide v12, 0x4099255c28f5c28fL    # 1609.34

    div-double/2addr v10, v12

    const-wide/high16 v12, 0x4024000000000000L    # 10.0

    mul-double/2addr v10, v12

    invoke-static {v10, v11}, Ljava/lang/Math;->round(D)J

    move-result-wide v10

    long-to-double v10, v10

    const-wide/high16 v12, 0x4024000000000000L    # 10.0

    div-double v6, v10, v12

    .line 127
    .restart local v6    # "miles":D
    const v9, 0x7f0802c8

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v12

    const-string v13, "%.1f"

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v16

    aput-object v16, v14, v15

    invoke-static {v12, v13, v14}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {v8, v9, v10}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    goto/16 :goto_0

    .line 130
    .end local v6    # "miles":D
    :cond_4
    move/from16 v0, p0

    int-to-double v10, v0

    const-wide v12, 0x3fd381d7dbf487fdL    # 0.3048

    div-double/2addr v10, v12

    const-wide/high16 v12, 0x4024000000000000L    # 10.0

    mul-double/2addr v10, v12

    invoke-static {v10, v11}, Ljava/lang/Math;->round(D)J

    move-result-wide v10

    long-to-double v10, v10

    const-wide/high16 v12, 0x4024000000000000L    # 10.0

    div-double v2, v10, v12

    .line 131
    .local v2, "feet":D
    const v9, 0x7f08018d

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {v8, v9, v10}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    goto/16 :goto_0
.end method

.method public static generateAlternateRoutePolyline(Lcom/here/android/mpa/common/GeoPolyline;)Lcom/here/android/mpa/mapping/MapPolyline;
    .locals 2
    .param p0, "geoPolyline"    # Lcom/here/android/mpa/common/GeoPolyline;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 166
    const v0, 0x7f0f0062

    const v1, 0x7f0e0003

    invoke-static {p0, v0, v1}, Lcom/navdy/client/app/framework/map/MapUtils;->generatePolyline(Lcom/here/android/mpa/common/GeoPolyline;II)Lcom/here/android/mpa/mapping/MapPolyline;

    move-result-object v0

    return-object v0
.end method

.method private static generatePolyline(Lcom/here/android/mpa/common/GeoPolyline;II)Lcom/here/android/mpa/mapping/MapPolyline;
    .locals 3
    .param p0, "geoPolyline"    # Lcom/here/android/mpa/common/GeoPolyline;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p1, "colorId"    # I
    .param p2, "zIndexId"    # I

    .prologue
    .line 330
    if-nez p0, :cond_0

    .line 331
    const/4 v0, 0x0

    .line 341
    :goto_0
    return-object v0

    .line 334
    :cond_0
    new-instance v0, Lcom/here/android/mpa/mapping/MapPolyline;

    invoke-direct {v0, p0}, Lcom/here/android/mpa/mapping/MapPolyline;-><init>(Lcom/here/android/mpa/common/GeoPolyline;)V

    .line 335
    .local v0, "mapPolyline":Lcom/here/android/mpa/mapping/MapPolyline;
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 337
    .local v1, "resources":Landroid/content/res/Resources;
    const v2, 0x7f0e000e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/here/android/mpa/mapping/MapPolyline;->setLineWidth(I)Lcom/here/android/mpa/mapping/MapPolyline;

    .line 338
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, p1}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/here/android/mpa/mapping/MapPolyline;->setLineColor(I)Lcom/here/android/mpa/mapping/MapPolyline;

    .line 339
    invoke-virtual {v1, p2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/here/android/mpa/mapping/MapPolyline;->setZIndex(I)Lcom/here/android/mpa/mapping/MapObject;

    goto :goto_0
.end method

.method public static generateProgressPolyline(Lcom/here/android/mpa/common/GeoPolyline;)Lcom/here/android/mpa/mapping/MapPolyline;
    .locals 2
    .param p0, "progress"    # Lcom/here/android/mpa/common/GeoPolyline;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 161
    const v0, 0x7f0f0010

    const v1, 0x7f0e0009

    invoke-static {p0, v0, v1}, Lcom/navdy/client/app/framework/map/MapUtils;->generatePolyline(Lcom/here/android/mpa/common/GeoPolyline;II)Lcom/here/android/mpa/mapping/MapPolyline;

    move-result-object v0

    return-object v0
.end method

.method public static generateRoutePolyline(Lcom/here/android/mpa/common/GeoPolyline;)Lcom/here/android/mpa/mapping/MapPolyline;
    .locals 2
    .param p0, "route"    # Lcom/here/android/mpa/common/GeoPolyline;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 156
    const v0, 0x7f0f0014

    const v1, 0x7f0e000a

    invoke-static {p0, v0, v1}, Lcom/navdy/client/app/framework/map/MapUtils;->generatePolyline(Lcom/here/android/mpa/common/GeoPolyline;II)Lcom/here/android/mpa/mapping/MapPolyline;

    move-result-object v0

    return-object v0
.end method

.method public static getBoundingBox(DDD)[Landroid/location/Location;
    .locals 26
    .param p0, "latitude"    # D
    .param p2, "longitude"    # D
    .param p4, "distance"    # D

    .prologue
    .line 273
    invoke-static/range {p0 .. p1}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v8

    .line 274
    .local v8, "latRad":D
    invoke-static/range {p2 .. p3}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v10

    .line 275
    .local v10, "longRad":D
    const-wide v22, 0x41584dae00000000L    # 6371000.0

    div-double v4, p4, v22

    .line 276
    .local v4, "angularDistance":D
    sub-double v18, v8, v4

    .line 277
    .local v18, "minLat":D
    add-double v14, v8, v4

    .line 279
    .local v14, "maxLat":D
    sget-wide v22, Lcom/navdy/client/app/framework/map/MapUtils;->MIN_LAT:D

    cmpl-double v22, v18, v22

    if-lez v22, :cond_2

    sget-wide v22, Lcom/navdy/client/app/framework/map/MapUtils;->MAX_LAT:D

    cmpg-double v22, v14, v22

    if-gez v22, :cond_2

    .line 280
    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v22

    .line 281
    invoke-static {v8, v9}, Ljava/lang/Math;->cos(D)D

    move-result-wide v24

    div-double v22, v22, v24

    .line 280
    invoke-static/range {v22 .. v23}, Ljava/lang/Math;->asin(D)D

    move-result-wide v6

    .line 282
    .local v6, "deltaLon":D
    sub-double v20, v10, v6

    .line 283
    .local v20, "minLon":D
    sget-wide v22, Lcom/navdy/client/app/framework/map/MapUtils;->MIN_LON:D

    cmpg-double v22, v20, v22

    if-gez v22, :cond_0

    const-wide v22, 0x401921fb54442d18L    # 6.283185307179586

    add-double v20, v20, v22

    .line 284
    :cond_0
    add-double v16, v10, v6

    .line 285
    .local v16, "maxLon":D
    sget-wide v22, Lcom/navdy/client/app/framework/map/MapUtils;->MAX_LON:D

    cmpl-double v22, v16, v22

    if-lez v22, :cond_1

    const-wide v22, 0x401921fb54442d18L    # 6.283185307179586

    sub-double v16, v16, v22

    .line 293
    .end local v6    # "deltaLon":D
    :cond_1
    :goto_0
    new-instance v13, Landroid/location/Location;

    const-string v22, ""

    move-object/from16 v0, v22

    invoke-direct {v13, v0}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    .line 294
    .local v13, "min":Landroid/location/Location;
    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v22

    move-wide/from16 v0, v22

    invoke-virtual {v13, v0, v1}, Landroid/location/Location;->setLatitude(D)V

    .line 295
    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v22

    move-wide/from16 v0, v22

    invoke-virtual {v13, v0, v1}, Landroid/location/Location;->setLongitude(D)V

    .line 296
    new-instance v12, Landroid/location/Location;

    const-string v22, ""

    move-object/from16 v0, v22

    invoke-direct {v12, v0}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    .line 297
    .local v12, "max":Landroid/location/Location;
    invoke-static {v14, v15}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v22

    move-wide/from16 v0, v22

    invoke-virtual {v12, v0, v1}, Landroid/location/Location;->setLatitude(D)V

    .line 298
    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v22

    move-wide/from16 v0, v22

    invoke-virtual {v12, v0, v1}, Landroid/location/Location;->setLongitude(D)V

    .line 299
    const/16 v22, 0x2

    move/from16 v0, v22

    new-array v0, v0, [Landroid/location/Location;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    aput-object v13, v22, v23

    const/16 v23, 0x1

    aput-object v12, v22, v23

    return-object v22

    .line 288
    .end local v12    # "max":Landroid/location/Location;
    .end local v13    # "min":Landroid/location/Location;
    .end local v16    # "maxLon":D
    .end local v20    # "minLon":D
    :cond_2
    sget-wide v22, Lcom/navdy/client/app/framework/map/MapUtils;->MIN_LAT:D

    move-wide/from16 v0, v18

    move-wide/from16 v2, v22

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(DD)D

    move-result-wide v18

    .line 289
    sget-wide v22, Lcom/navdy/client/app/framework/map/MapUtils;->MAX_LAT:D

    move-wide/from16 v0, v22

    invoke-static {v14, v15, v0, v1}, Ljava/lang/Math;->min(DD)D

    move-result-wide v14

    .line 290
    sget-wide v20, Lcom/navdy/client/app/framework/map/MapUtils;->MIN_LON:D

    .line 291
    .restart local v20    # "minLon":D
    sget-wide v16, Lcom/navdy/client/app/framework/map/MapUtils;->MAX_LON:D

    .restart local v16    # "maxLon":D
    goto :goto_0
.end method

.method public static getGmsLatLongsFromRouteResult(Lcom/navdy/service/library/events/navigation/NavigationRouteResult;)Ljava/util/List;
    .locals 10
    .param p0, "routeResult"    # Lcom/navdy/service/library/events/navigation/NavigationRouteResult;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/service/library/events/navigation/NavigationRouteResult;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/maps/model/LatLng;",
            ">;"
        }
    .end annotation

    .prologue
    .line 82
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 84
    .local v3, "result":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/gms/maps/model/LatLng;>;"
    if-eqz p0, :cond_0

    iget-object v4, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->routeLatLongs:Ljava/util/List;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->routeLatLongs:Ljava/util/List;

    .line 86
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    rem-int/lit8 v4, v4, 0x2

    const/4 v5, 0x1

    if-ne v4, v5, :cond_1

    .line 99
    :cond_0
    return-object v3

    .line 90
    :cond_1
    iget-object v4, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->routeLatLongs:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v0

    .line 92
    .local v0, "iterator":Ljava/util/ListIterator;, "Ljava/util/ListIterator<Ljava/lang/Float;>;"
    :goto_0
    invoke-interface {v0}, Ljava/util/ListIterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 93
    invoke-interface {v0}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    .line 94
    .local v1, "latitude":Ljava/lang/Float;
    invoke-interface {v0}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    .line 96
    .local v2, "longitude":Ljava/lang/Float;
    new-instance v4, Lcom/google/android/gms/maps/model/LatLng;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v5

    float-to-double v6, v5

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v5

    float-to-double v8, v5

    invoke-direct {v4, v6, v7, v8, v9}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static isValidSetOfCoordinates(DD)Z
    .locals 4
    .param p0, "latitude"    # D
    .param p2, "longitude"    # D

    .prologue
    const-wide/16 v2, 0x0

    .line 303
    cmpl-double v0, p0, v2

    if-nez v0, :cond_0

    cmpl-double v0, p2, v2

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isValidSetOfCoordinates(Landroid/location/Location;)Z
    .locals 4
    .param p0, "location"    # Landroid/location/Location;

    .prologue
    .line 315
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v0

    invoke-virtual {p0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lcom/navdy/client/app/framework/map/MapUtils;->isValidSetOfCoordinates(DD)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isValidSetOfCoordinates(Lcom/google/android/gms/maps/model/LatLng;)Z
    .locals 4
    .param p0, "latLng"    # Lcom/google/android/gms/maps/model/LatLng;

    .prologue
    .line 307
    if-eqz p0, :cond_0

    iget-wide v0, p0, Lcom/google/android/gms/maps/model/LatLng;->latitude:D

    iget-wide v2, p0, Lcom/google/android/gms/maps/model/LatLng;->longitude:D

    invoke-static {v0, v1, v2, v3}, Lcom/navdy/client/app/framework/map/MapUtils;->isValidSetOfCoordinates(DD)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isValidSetOfCoordinates(Lcom/here/android/mpa/common/GeoCoordinate;)Z
    .locals 4
    .param p0, "coordinate"    # Lcom/here/android/mpa/common/GeoCoordinate;

    .prologue
    .line 319
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/here/android/mpa/common/GeoCoordinate;->getLatitude()D

    move-result-wide v0

    invoke-virtual {p0}, Lcom/here/android/mpa/common/GeoCoordinate;->getLongitude()D

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lcom/navdy/client/app/framework/map/MapUtils;->isValidSetOfCoordinates(DD)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isValidSetOfCoordinates(Lcom/navdy/service/library/events/location/Coordinate;)Z
    .locals 4
    .param p0, "coordinates"    # Lcom/navdy/service/library/events/location/Coordinate;

    .prologue
    .line 311
    if-eqz p0, :cond_0

    iget-object v0, p0, Lcom/navdy/service/library/events/location/Coordinate;->latitude:Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    iget-object v2, p0, Lcom/navdy/service/library/events/location/Coordinate;->longitude:Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lcom/navdy/client/app/framework/map/MapUtils;->isValidSetOfCoordinates(DD)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseLatLng(Ljava/lang/String;)Lcom/google/android/gms/maps/model/LatLng;
    .locals 12
    .param p0, "string"    # Ljava/lang/String;

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x0

    .line 412
    invoke-static {p0}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 413
    const/4 v2, 0x0

    .line 439
    :cond_0
    :goto_0
    return-object v2

    .line 416
    :cond_1
    move-object v7, p0

    .line 418
    .local v7, "str":Ljava/lang/String;
    :try_start_0
    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v8

    invoke-virtual {v8}, Ljava/nio/charset/Charset;->name()Ljava/lang/String;

    move-result-object v8

    invoke-static {p0, v8}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v7

    .line 423
    :goto_1
    const/4 v2, 0x0

    .line 424
    .local v2, "latLng":Lcom/google/android/gms/maps/model/LatLng;
    const-string v8, "(-?[0-9]+\\.?[0-9]*), *(-?[0-9]+\\.?[0-9]*)"

    invoke-static {v8}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v6

    .line 425
    .local v6, "pattern":Ljava/util/regex/Pattern;
    invoke-virtual {v6, v7}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v5

    .line 426
    .local v5, "matcher":Ljava/util/regex/Matcher;
    invoke-virtual {v5}, Ljava/util/regex/Matcher;->matches()Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-virtual {v5}, Ljava/util/regex/Matcher;->groupCount()I

    move-result v8

    if-ne v8, v10, :cond_3

    .line 428
    const/4 v8, 0x1

    :try_start_1
    invoke-virtual {v5, v8}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    .line 429
    .local v1, "lat":F
    const/4 v8, 0x2

    invoke-virtual {v5, v8}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v4

    .line 430
    .local v4, "lng":F
    cmpl-float v8, v1, v9

    if-nez v8, :cond_2

    cmpl-float v8, v4, v9

    if-eqz v8, :cond_0

    .line 431
    :cond_2
    new-instance v3, Lcom/google/android/gms/maps/model/LatLng;

    float-to-double v8, v1

    float-to-double v10, v4

    invoke-direct {v3, v8, v9, v10, v11}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    .end local v2    # "latLng":Lcom/google/android/gms/maps/model/LatLng;
    .local v3, "latLng":Lcom/google/android/gms/maps/model/LatLng;
    move-object v2, v3

    .end local v3    # "latLng":Lcom/google/android/gms/maps/model/LatLng;
    .restart local v2    # "latLng":Lcom/google/android/gms/maps/model/LatLng;
    goto :goto_0

    .line 419
    .end local v1    # "lat":F
    .end local v2    # "latLng":Lcom/google/android/gms/maps/model/LatLng;
    .end local v4    # "lng":F
    .end local v5    # "matcher":Ljava/util/regex/Matcher;
    .end local v6    # "pattern":Ljava/util/regex/Pattern;
    :catch_0
    move-exception v0

    .line 420
    .local v0, "e":Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_1

    .line 433
    .end local v0    # "e":Ljava/io/UnsupportedEncodingException;
    .restart local v2    # "latLng":Lcom/google/android/gms/maps/model/LatLng;
    .restart local v5    # "matcher":Ljava/util/regex/Matcher;
    .restart local v6    # "pattern":Ljava/util/regex/Pattern;
    :catch_1
    move-exception v0

    .line 434
    .local v0, "e":Ljava/lang/NumberFormatException;
    sget-object v8, Lcom/navdy/client/app/framework/map/MapUtils;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Non-float lat/lng: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 437
    .end local v0    # "e":Ljava/lang/NumberFormatException;
    :cond_3
    sget-object v8, Lcom/navdy/client/app/framework/map/MapUtils;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Can\'t parse lat/lng: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static setMarkerImageResource(Lcom/google/android/gms/maps/model/Marker;I)V
    .locals 5
    .param p0, "marker"    # Lcom/google/android/gms/maps/model/Marker;
    .param p1, "resId"    # I

    .prologue
    .line 142
    if-nez p0, :cond_0

    .line 151
    :goto_0
    return-void

    .line 146
    :cond_0
    :try_start_0
    invoke-static {p1}, Lcom/google/android/gms/maps/model/BitmapDescriptorFactory;->fromResource(I)Lcom/google/android/gms/maps/model/BitmapDescriptor;

    move-result-object v1

    .line 147
    .local v1, "icon":Lcom/google/android/gms/maps/model/BitmapDescriptor;
    invoke-virtual {p0, v1}, Lcom/google/android/gms/maps/model/Marker;->setIcon(Lcom/google/android/gms/maps/model/BitmapDescriptor;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 148
    .end local v1    # "icon":Lcom/google/android/gms/maps/model/BitmapDescriptor;
    :catch_0
    move-exception v0

    .line 149
    .local v0, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/navdy/client/app/framework/map/MapUtils;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unable to set the marker image. marker = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0
.end method
