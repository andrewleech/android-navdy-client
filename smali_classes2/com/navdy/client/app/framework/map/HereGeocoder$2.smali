.class final Lcom/navdy/client/app/framework/map/HereGeocoder$2;
.super Ljava/lang/Object;
.source "HereGeocoder.java"

# interfaces
.implements Lcom/here/android/mpa/search/ResultListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/framework/map/HereGeocoder;->getSearchResultsListener(Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;)Lcom/here/android/mpa/search/ResultListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/here/android/mpa/search/ResultListener",
        "<",
        "Lcom/here/android/mpa/search/DiscoveryResultPage;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic val$requestCallback:Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;)V
    .locals 0

    .prologue
    .line 111
    iput-object p1, p0, Lcom/navdy/client/app/framework/map/HereGeocoder$2;->val$requestCallback:Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCompleted(Lcom/here/android/mpa/search/DiscoveryResultPage;Lcom/here/android/mpa/search/ErrorCode;)V
    .locals 6
    .param p1, "discoveryResultPage"    # Lcom/here/android/mpa/search/DiscoveryResultPage;
    .param p2, "searchResponseError"    # Lcom/here/android/mpa/search/ErrorCode;

    .prologue
    .line 114
    sget-object v5, Lcom/here/android/mpa/search/ErrorCode;->NONE:Lcom/here/android/mpa/search/ErrorCode;

    if-eq p2, v5, :cond_1

    .line 115
    iget-object v5, p0, Lcom/navdy/client/app/framework/map/HereGeocoder$2;->val$requestCallback:Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;

    invoke-interface {v5, p2}, Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;->onError(Lcom/here/android/mpa/search/ErrorCode;)V

    .line 141
    :cond_0
    :goto_0
    return-void

    .line 119
    :cond_1
    if-eqz p1, :cond_2

    .line 120
    invoke-virtual {p1}, Lcom/here/android/mpa/search/DiscoveryResultPage;->getPlaceLinks()Ljava/util/List;

    move-result-object v2

    .line 122
    .local v2, "placeLinks":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/search/PlaceLink;>;"
    if-eqz v2, :cond_2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v5

    if-lez v5, :cond_2

    .line 123
    const/4 v5, 0x0

    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/here/android/mpa/search/PlaceLink;

    .line 125
    .local v1, "placeLink":Lcom/here/android/mpa/search/PlaceLink;
    if-eqz v1, :cond_2

    .line 126
    invoke-virtual {v1}, Lcom/here/android/mpa/search/PlaceLink;->getDetailsRequest()Lcom/here/android/mpa/search/PlaceRequest;

    move-result-object v0

    .line 128
    .local v0, "detailsRequest":Lcom/here/android/mpa/search/PlaceRequest;
    if-eqz v0, :cond_2

    .line 129
    iget-object v5, p0, Lcom/navdy/client/app/framework/map/HereGeocoder$2;->val$requestCallback:Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;

    invoke-static {v5}, Lcom/navdy/client/app/framework/map/HereGeocoder;->access$200(Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;)Lcom/here/android/mpa/search/ResultListener;

    move-result-object v4

    .line 130
    .local v4, "placeResultListener":Lcom/here/android/mpa/search/ResultListener;, "Lcom/here/android/mpa/search/ResultListener<Lcom/here/android/mpa/search/Place;>;"
    invoke-virtual {v0, v4}, Lcom/here/android/mpa/search/PlaceRequest;->execute(Lcom/here/android/mpa/search/ResultListener;)Lcom/here/android/mpa/search/ErrorCode;

    move-result-object v3

    .line 132
    .local v3, "placeRequestError":Lcom/here/android/mpa/search/ErrorCode;
    sget-object v5, Lcom/here/android/mpa/search/ErrorCode;->NONE:Lcom/here/android/mpa/search/ErrorCode;

    if-eq v3, v5, :cond_0

    .line 133
    iget-object v5, p0, Lcom/navdy/client/app/framework/map/HereGeocoder$2;->val$requestCallback:Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;

    invoke-interface {v5, p2}, Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;->onError(Lcom/here/android/mpa/search/ErrorCode;)V

    goto :goto_0

    .line 140
    .end local v0    # "detailsRequest":Lcom/here/android/mpa/search/PlaceRequest;
    .end local v1    # "placeLink":Lcom/here/android/mpa/search/PlaceLink;
    .end local v2    # "placeLinks":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/search/PlaceLink;>;"
    .end local v3    # "placeRequestError":Lcom/here/android/mpa/search/ErrorCode;
    .end local v4    # "placeResultListener":Lcom/here/android/mpa/search/ResultListener;, "Lcom/here/android/mpa/search/ResultListener<Lcom/here/android/mpa/search/Place;>;"
    :cond_2
    iget-object v5, p0, Lcom/navdy/client/app/framework/map/HereGeocoder$2;->val$requestCallback:Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;

    invoke-interface {v5, p2}, Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;->onError(Lcom/here/android/mpa/search/ErrorCode;)V

    goto :goto_0
.end method

.method public bridge synthetic onCompleted(Ljava/lang/Object;Lcom/here/android/mpa/search/ErrorCode;)V
    .locals 0

    .prologue
    .line 111
    check-cast p1, Lcom/here/android/mpa/search/DiscoveryResultPage;

    invoke-virtual {p0, p1, p2}, Lcom/navdy/client/app/framework/map/HereGeocoder$2;->onCompleted(Lcom/here/android/mpa/search/DiscoveryResultPage;Lcom/here/android/mpa/search/ErrorCode;)V

    return-void
.end method
