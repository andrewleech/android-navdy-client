.class public interface abstract Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;
.super Ljava/lang/Object;
.source "HereGeocoder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/framework/map/HereGeocoder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OnGeocodeCompleteCallback"
.end annotation


# virtual methods
.method public abstract onComplete(Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/search/Address;Lcom/navdy/client/app/framework/models/Destination$Precision;)V
    .param p1    # Lcom/here/android/mpa/common/GeoCoordinate;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/here/android/mpa/common/GeoCoordinate;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Lcom/here/android/mpa/search/Address;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
.end method

.method public abstract onError(Lcom/here/android/mpa/search/ErrorCode;)V
    .param p1    # Lcom/here/android/mpa/search/ErrorCode;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method
