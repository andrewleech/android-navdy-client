.class final Lcom/navdy/client/app/framework/map/HereGeocoder$3;
.super Ljava/lang/Object;
.source "HereGeocoder.java"

# interfaces
.implements Lcom/here/android/mpa/search/ResultListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/framework/map/HereGeocoder;->getPlaceResultListener(Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;)Lcom/here/android/mpa/search/ResultListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/here/android/mpa/search/ResultListener",
        "<",
        "Lcom/here/android/mpa/search/Place;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic val$requestCallback:Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;)V
    .locals 0

    .prologue
    .line 147
    iput-object p1, p0, Lcom/navdy/client/app/framework/map/HereGeocoder$3;->val$requestCallback:Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCompleted(Lcom/here/android/mpa/search/Place;Lcom/here/android/mpa/search/ErrorCode;)V
    .locals 12
    .param p1, "place"    # Lcom/here/android/mpa/search/Place;
    .param p2, "placeResponseError"    # Lcom/here/android/mpa/search/ErrorCode;

    .prologue
    .line 150
    sget-object v9, Lcom/here/android/mpa/search/ErrorCode;->NONE:Lcom/here/android/mpa/search/ErrorCode;

    if-eq p2, v9, :cond_0

    .line 151
    iget-object v9, p0, Lcom/navdy/client/app/framework/map/HereGeocoder$3;->val$requestCallback:Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;

    invoke-interface {v9, p2}, Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;->onError(Lcom/here/android/mpa/search/ErrorCode;)V

    .line 197
    :goto_0
    return-void

    .line 155
    :cond_0
    if-eqz p1, :cond_7

    .line 156
    invoke-virtual {p1}, Lcom/here/android/mpa/search/Place;->getLocation()Lcom/here/android/mpa/search/Location;

    move-result-object v6

    .line 158
    .local v6, "location":Lcom/here/android/mpa/search/Location;
    if-eqz v6, :cond_7

    .line 159
    invoke-virtual {v6}, Lcom/here/android/mpa/search/Location;->getCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v4

    .line 160
    .local v4, "displayCoord":Lcom/here/android/mpa/common/GeoCoordinate;
    const/4 v7, 0x0

    .line 162
    .local v7, "navCoord":Lcom/here/android/mpa/common/GeoCoordinate;
    invoke-virtual {v6}, Lcom/here/android/mpa/search/Location;->getAccessPoints()Ljava/util/List;

    move-result-object v1

    .line 164
    .local v1, "accessPoints":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/search/NavigationPosition;>;"
    if-eqz v1, :cond_2

    .line 165
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_1
    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_2

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/here/android/mpa/search/NavigationPosition;

    .line 166
    .local v0, "accessPoint":Lcom/here/android/mpa/search/NavigationPosition;
    invoke-virtual {v0}, Lcom/here/android/mpa/search/NavigationPosition;->getAccessType()Ljava/lang/String;

    move-result-object v10

    if-eqz v10, :cond_1

    invoke-virtual {v0}, Lcom/here/android/mpa/search/NavigationPosition;->getAccessType()Ljava/lang/String;

    move-result-object v10

    const-string v11, "road"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 167
    invoke-virtual {v0}, Lcom/here/android/mpa/search/NavigationPosition;->getCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v7

    goto :goto_1

    .line 172
    .end local v0    # "accessPoint":Lcom/here/android/mpa/search/NavigationPosition;
    :cond_2
    sget-object v8, Lcom/navdy/client/app/framework/models/Destination$Precision;->PRECISE:Lcom/navdy/client/app/framework/models/Destination$Precision;

    .line 173
    .local v8, "precision":Lcom/navdy/client/app/framework/models/Destination$Precision;
    invoke-virtual {p1}, Lcom/here/android/mpa/search/Place;->getCategories()Ljava/util/List;

    move-result-object v2

    .line 175
    .local v2, "categories":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/search/Category;>;"
    if-eqz v2, :cond_5

    .line 176
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_3
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_5

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/here/android/mpa/search/Category;

    .line 177
    .local v3, "category":Lcom/here/android/mpa/search/Category;
    invoke-virtual {v3}, Lcom/here/android/mpa/search/Category;->getId()Ljava/lang/String;

    move-result-object v5

    .line 178
    .local v5, "id":Ljava/lang/String;
    const-string v10, "administrative-region"

    invoke-static {v5, v10}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_4

    const-string v10, "city-town-village"

    .line 179
    invoke-static {v5, v10}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_4

    const-string v10, "postal-area"

    .line 180
    invoke-static {v5, v10}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 181
    :cond_4
    sget-object v8, Lcom/navdy/client/app/framework/models/Destination$Precision;->IMPRECISE:Lcom/navdy/client/app/framework/models/Destination$Precision;

    .line 187
    .end local v3    # "category":Lcom/here/android/mpa/search/Category;
    .end local v5    # "id":Ljava/lang/String;
    :cond_5
    if-eqz v4, :cond_6

    .line 188
    iget-object v9, p0, Lcom/navdy/client/app/framework/map/HereGeocoder$3;->val$requestCallback:Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;

    invoke-virtual {v6}, Lcom/here/android/mpa/search/Location;->getAddress()Lcom/here/android/mpa/search/Address;

    move-result-object v10

    invoke-interface {v9, v4, v7, v10, v8}, Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;->onComplete(Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/search/Address;Lcom/navdy/client/app/framework/models/Destination$Precision;)V

    goto :goto_0

    .line 191
    :cond_6
    iget-object v9, p0, Lcom/navdy/client/app/framework/map/HereGeocoder$3;->val$requestCallback:Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;

    invoke-interface {v9, p2}, Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;->onError(Lcom/here/android/mpa/search/ErrorCode;)V

    goto/16 :goto_0

    .line 196
    .end local v1    # "accessPoints":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/search/NavigationPosition;>;"
    .end local v2    # "categories":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/search/Category;>;"
    .end local v4    # "displayCoord":Lcom/here/android/mpa/common/GeoCoordinate;
    .end local v6    # "location":Lcom/here/android/mpa/search/Location;
    .end local v7    # "navCoord":Lcom/here/android/mpa/common/GeoCoordinate;
    .end local v8    # "precision":Lcom/navdy/client/app/framework/models/Destination$Precision;
    :cond_7
    iget-object v9, p0, Lcom/navdy/client/app/framework/map/HereGeocoder$3;->val$requestCallback:Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;

    invoke-interface {v9, p2}, Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;->onError(Lcom/here/android/mpa/search/ErrorCode;)V

    goto/16 :goto_0
.end method

.method public bridge synthetic onCompleted(Ljava/lang/Object;Lcom/here/android/mpa/search/ErrorCode;)V
    .locals 0

    .prologue
    .line 147
    check-cast p1, Lcom/here/android/mpa/search/Place;

    invoke-virtual {p0, p1, p2}, Lcom/navdy/client/app/framework/map/HereGeocoder$3;->onCompleted(Lcom/here/android/mpa/search/Place;Lcom/here/android/mpa/search/ErrorCode;)V

    return-void
.end method
