.class Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$6$1;
.super Ljava/lang/Object;
.source "NavCoordsAddressProcessor.java"

# interfaces
.implements Lcom/android/volley/Response$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$6;->fetchMetaData(Ljava/lang/String;Ljava/lang/Double;Ljava/lang/Double;Lcom/here/android/mpa/search/Address;Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/android/volley/Response$Listener",
        "<",
        "Lorg/json/JSONObject;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$6;

.field final synthetic val$callback:Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;

.field final synthetic val$hereAddress:Lcom/here/android/mpa/search/Address;

.field final synthetic val$navLat:Ljava/lang/Double;

.field final synthetic val$navLong:Ljava/lang/Double;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$6;Ljava/lang/Double;Ljava/lang/Double;Lcom/here/android/mpa/search/Address;Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$6;

    .prologue
    .line 614
    iput-object p1, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$6$1;->this$0:Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$6;

    iput-object p2, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$6$1;->val$navLat:Ljava/lang/Double;

    iput-object p3, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$6$1;->val$navLong:Ljava/lang/Double;

    iput-object p4, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$6$1;->val$hereAddress:Lcom/here/android/mpa/search/Address;

    iput-object p5, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$6$1;->val$callback:Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic onResponse(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 614
    check-cast p1, Lorg/json/JSONObject;

    invoke-virtual {p0, p1}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$6$1;->onResponse(Lorg/json/JSONObject;)V

    return-void
.end method

.method public onResponse(Lorg/json/JSONObject;)V
    .locals 6
    .param p1, "response"    # Lorg/json/JSONObject;

    .prologue
    .line 618
    iget-object v0, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$6$1;->this$0:Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$6;

    iget-object v2, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$6$1;->val$navLat:Ljava/lang/Double;

    iget-object v3, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$6$1;->val$navLong:Ljava/lang/Double;

    iget-object v4, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$6$1;->val$hereAddress:Lcom/here/android/mpa/search/Address;

    iget-object v5, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$6$1;->val$callback:Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;

    move-object v1, p1

    invoke-static/range {v0 .. v5}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$6;->access$900(Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$6;Lorg/json/JSONObject;Ljava/lang/Double;Ljava/lang/Double;Lcom/here/android/mpa/search/Address;Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;)V

    .line 619
    return-void
.end method
