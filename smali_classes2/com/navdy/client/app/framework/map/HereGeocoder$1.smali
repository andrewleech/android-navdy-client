.class final Lcom/navdy/client/app/framework/map/HereGeocoder$1;
.super Ljava/lang/Object;
.source "HereGeocoder.java"

# interfaces
.implements Lcom/here/android/mpa/common/OnEngineInitListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/framework/map/HereGeocoder;->makeRequest(Ljava/lang/String;Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$requestCallback:Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;

.field final synthetic val$searchQuery:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 55
    iput-object p1, p0, Lcom/navdy/client/app/framework/map/HereGeocoder$1;->val$requestCallback:Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;

    iput-object p2, p0, Lcom/navdy/client/app/framework/map/HereGeocoder$1;->val$searchQuery:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEngineInitializationCompleted(Lcom/here/android/mpa/common/OnEngineInitListener$Error;)V
    .locals 4
    .param p1, "error"    # Lcom/here/android/mpa/common/OnEngineInitListener$Error;

    .prologue
    .line 58
    sget-object v1, Lcom/here/android/mpa/common/OnEngineInitListener$Error;->NONE:Lcom/here/android/mpa/common/OnEngineInitListener$Error;

    if-eq p1, v1, :cond_0

    .line 59
    invoke-static {}, Lcom/navdy/client/app/framework/map/HereGeocoder;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "makeHereRequestForDestination, OnEngineInitListener error: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 60
    iget-object v1, p0, Lcom/navdy/client/app/framework/map/HereGeocoder$1;->val$requestCallback:Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;

    sget-object v2, Lcom/here/android/mpa/search/ErrorCode;->NOT_INITIALIZED:Lcom/here/android/mpa/search/ErrorCode;

    invoke-interface {v1, v2}, Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;->onError(Lcom/here/android/mpa/search/ErrorCode;)V

    .line 71
    :goto_0
    return-void

    .line 64
    :cond_0
    iget-object v1, p0, Lcom/navdy/client/app/framework/map/HereGeocoder$1;->val$searchQuery:Ljava/lang/String;

    if-nez v1, :cond_1

    .line 65
    invoke-static {}, Lcom/navdy/client/app/framework/map/HereGeocoder;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "makeHereRequestForDestination, no searchQuery"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 66
    iget-object v1, p0, Lcom/navdy/client/app/framework/map/HereGeocoder$1;->val$requestCallback:Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;

    sget-object v2, Lcom/here/android/mpa/search/ErrorCode;->BAD_REQUEST:Lcom/here/android/mpa/search/ErrorCode;

    invoke-interface {v1, v2}, Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;->onError(Lcom/here/android/mpa/search/ErrorCode;)V

    goto :goto_0

    .line 69
    :cond_1
    invoke-static {}, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->getInstance()Lcom/navdy/client/app/framework/location/NavdyLocationManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->getSmartStartCoordinates()Lcom/navdy/service/library/events/location/Coordinate;

    move-result-object v0

    .line 70
    .local v0, "coordinate":Lcom/navdy/service/library/events/location/Coordinate;
    iget-object v1, p0, Lcom/navdy/client/app/framework/map/HereGeocoder$1;->val$searchQuery:Ljava/lang/String;

    iget-object v2, p0, Lcom/navdy/client/app/framework/map/HereGeocoder$1;->val$requestCallback:Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;

    invoke-static {v0, v1, v2}, Lcom/navdy/client/app/framework/map/HereGeocoder;->access$100(Lcom/navdy/service/library/events/location/Coordinate;Ljava/lang/String;Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;)V

    goto :goto_0
.end method
