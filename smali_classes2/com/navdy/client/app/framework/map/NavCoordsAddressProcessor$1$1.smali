.class Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$1$1;
.super Ljava/lang/Object;
.source "NavCoordsAddressProcessor.java"

# interfaces
.implements Lcom/navdy/client/app/framework/search/GooglePlacesSearch$GoogleSearchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$1;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$1;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$1;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$1;

    .prologue
    .line 103
    iput-object p1, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$1$1;->this$0:Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGoogleSearchResult(Ljava/util/List;Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Error;)V
    .locals 4
    .param p2, "queryType"    # Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;
    .param p3, "error"    # Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Error;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;",
            ">;",
            "Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;",
            "Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Error;",
            ")V"
        }
    .end annotation

    .prologue
    .line 108
    .local p1, "destinations":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;>;"
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 109
    invoke-interface {p1}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v1

    .line 111
    .local v1, "iterator":Ljava/util/ListIterator;, "Ljava/util/ListIterator<Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;>;"
    invoke-interface {v1}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;

    .line 112
    .local v0, "destinationResult":Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;
    sget-object v2, Lcom/navdy/client/app/framework/models/Destination$SearchType;->DETAILS:Lcom/navdy/client/app/framework/models/Destination$SearchType;

    iget-object v3, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$1$1;->this$0:Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$1;

    iget-object v3, v3, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$1;->val$destination:Lcom/navdy/client/app/framework/models/Destination;

    invoke-virtual {v0, v2, v3}, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->toModelDestinationObject(Lcom/navdy/client/app/framework/models/Destination$SearchType;Lcom/navdy/client/app/framework/models/Destination;)V

    .line 114
    .end local v0    # "destinationResult":Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;
    .end local v1    # "iterator":Ljava/util/ListIterator;, "Ljava/util/ListIterator<Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;>;"
    :cond_0
    iget-object v2, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$1$1;->this$0:Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$1;

    invoke-static {v2}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$1;->access$000(Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$1;)V

    .line 115
    return-void
.end method
