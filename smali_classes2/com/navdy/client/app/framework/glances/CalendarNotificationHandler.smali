.class public Lcom/navdy/client/app/framework/glances/CalendarNotificationHandler;
.super Ljava/lang/Object;
.source "CalendarNotificationHandler.java"


# static fields
.field private static sLogger:Lcom/navdy/service/library/log/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->sLogger:Lcom/navdy/service/library/log/Logger;

    sput-object v0, Lcom/navdy/client/app/framework/glances/CalendarNotificationHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static handleCalendarNotification(Landroid/service/notification/StatusBarNotification;)V
    .locals 4
    .param p0, "sbn"    # Landroid/service/notification/StatusBarNotification;

    .prologue
    .line 28
    invoke-virtual {p0}, Landroid/service/notification/StatusBarNotification;->getPackageName()Ljava/lang/String;

    move-result-object v0

    .line 30
    .local v0, "packageName":Ljava/lang/String;
    invoke-static {v0}, Lcom/navdy/client/app/ui/glances/GlanceUtils;->isThisGlanceEnabledAsWellAsGlobal(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 43
    :goto_0
    return-void

    .line 34
    :cond_0
    const/4 v1, -0x1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :cond_1
    :goto_1
    packed-switch v1, :pswitch_data_0

    .line 41
    sget-object v1, Lcom/navdy/client/app/framework/glances/CalendarNotificationHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "calendar notification not handled ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    goto :goto_0

    .line 34
    :sswitch_0
    const-string v2, "com.android.calendar"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v1, 0x0

    goto :goto_1

    :sswitch_1
    const-string v2, "com.google.android.calendar"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v1, 0x1

    goto :goto_1

    .line 37
    :pswitch_0
    invoke-static {p0}, Lcom/navdy/client/app/framework/glances/CalendarNotificationHandler;->handleGoogleCalendarNotification(Landroid/service/notification/StatusBarNotification;)V

    goto :goto_0

    .line 34
    nop

    :sswitch_data_0
    .sparse-switch
        -0x1b2f0756 -> :sswitch_0
        0x227a1d85 -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private static handleGoogleCalendarNotification(Landroid/service/notification/StatusBarNotification;)V
    .locals 22
    .param p0, "sbn"    # Landroid/service/notification/StatusBarNotification;

    .prologue
    .line 274
    invoke-virtual/range {p0 .. p0}, Landroid/service/notification/StatusBarNotification;->getPackageName()Ljava/lang/String;

    move-result-object v15

    .line 275
    .local v15, "packageName":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Landroid/service/notification/StatusBarNotification;->getNotification()Landroid/app/Notification;

    move-result-object v14

    .line 276
    .local v14, "notification":Landroid/app/Notification;
    invoke-static {v14}, Landroid/support/v7/app/NotificationCompat;->getExtras(Landroid/app/Notification;)Landroid/os/Bundle;

    move-result-object v8

    .line 277
    .local v8, "extras":Landroid/os/Bundle;
    const-string v19, "android.title"

    move-object/from16 v0, v19

    invoke-virtual {v8, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 278
    .local v17, "title":Ljava/lang/String;
    const-string v19, "android.bigText"

    move-object/from16 v0, v19

    invoke-virtual {v8, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 279
    .local v5, "bigtext":Ljava/lang/String;
    const-string v19, "android.text"

    move-object/from16 v0, v19

    invoke-virtual {v8, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 280
    .local v16, "text":Ljava/lang/String;
    const/16 v18, 0x0

    .line 281
    .local v18, "when":Ljava/lang/String;
    const/4 v13, 0x0

    .line 283
    .local v13, "location":Ljava/lang/String;
    invoke-static {v5}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v19

    if-nez v19, :cond_0

    .line 284
    move-object/from16 v16, v5

    .line 287
    :cond_0
    if-eqz v16, :cond_1

    .line 289
    const-string v19, "\n"

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v10

    .line 290
    .local v10, "i":I
    const/16 v19, -0x1

    move/from16 v0, v19

    if-eq v10, v0, :cond_6

    .line 291
    const/16 v19, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v19

    invoke-virtual {v0, v1, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v18

    .line 292
    add-int/lit8 v19, v10, 0x1

    move-object/from16 v0, v16

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v13

    .line 312
    .end local v10    # "i":I
    :cond_1
    :goto_0
    invoke-static {v14}, Lcom/navdy/client/app/framework/glances/GlancesHelper;->getActions(Landroid/app/Notification;)Ljava/util/HashSet;

    move-result-object v4

    .line 314
    .local v4, "actions":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    const/4 v9, 0x0

    .line 315
    .local v9, "hasLocationAction":Z
    const-string v19, "Map"

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_9

    .line 316
    const/4 v9, 0x1

    .line 321
    :goto_1
    invoke-static {}, Lcom/navdy/client/app/framework/glances/GlancesHelper;->getId()Ljava/lang/String;

    move-result-object v12

    .line 323
    .local v12, "id":Ljava/lang/String;
    if-eqz v17, :cond_2

    .line 324
    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v17

    .line 326
    :cond_2
    if-eqz v18, :cond_3

    .line 327
    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v18

    .line 329
    :cond_3
    if-eqz v13, :cond_4

    .line 330
    invoke-virtual {v13}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v13

    .line 333
    :cond_4
    sget-object v19, Lcom/navdy/client/app/framework/glances/CalendarNotificationHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "[navdyinfo-gcalendar] title["

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "] when["

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "] location["

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "] hasLocationAction["

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "] uuid["

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "]"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 335
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 336
    .local v7, "data":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/glances/KeyValue;>;"
    new-instance v19, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v20, Lcom/navdy/service/library/events/glances/CalendarConstants;->CALENDAR_TITLE:Lcom/navdy/service/library/events/glances/CalendarConstants;

    invoke-virtual/range {v20 .. v20}, Lcom/navdy/service/library/events/glances/CalendarConstants;->name()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    move-object/from16 v2, v17

    invoke-direct {v0, v1, v2}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v19

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 337
    new-instance v19, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v20, Lcom/navdy/service/library/events/glances/CalendarConstants;->CALENDAR_TIME_STR:Lcom/navdy/service/library/events/glances/CalendarConstants;

    invoke-virtual/range {v20 .. v20}, Lcom/navdy/service/library/events/glances/CalendarConstants;->name()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v2}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v19

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 338
    if-eqz v9, :cond_5

    .line 339
    new-instance v19, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v20, Lcom/navdy/service/library/events/glances/CalendarConstants;->CALENDAR_LOCATION:Lcom/navdy/service/library/events/glances/CalendarConstants;

    invoke-virtual/range {v20 .. v20}, Lcom/navdy/service/library/events/glances/CalendarConstants;->name()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-direct {v0, v1, v13}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v19

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 342
    :cond_5
    new-instance v19, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    invoke-direct/range {v19 .. v19}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;-><init>()V

    sget-object v20, Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;->GLANCE_TYPE_CALENDAR:Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;

    .line 343
    invoke-virtual/range {v19 .. v20}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->glanceType(Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v19

    .line 344
    move-object/from16 v0, v19

    invoke-virtual {v0, v15}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->provider(Ljava/lang/String;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v19

    .line 345
    move-object/from16 v0, v19

    invoke-virtual {v0, v12}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->id(Ljava/lang/String;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v19

    .line 346
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v20

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->postTime(Ljava/lang/Long;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v19

    .line 347
    move-object/from16 v0, v19

    invoke-virtual {v0, v7}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->glanceData(Ljava/util/List;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->build()Lcom/navdy/service/library/events/glances/GlanceEvent;

    move-result-object v6

    .line 349
    .local v6, "calendarEvent":Lcom/navdy/service/library/events/glances/GlanceEvent;
    invoke-static {v6}, Lcom/navdy/client/app/framework/glances/GlancesHelper;->sendEvent(Lcom/navdy/service/library/events/glances/GlanceEvent;)V

    .line 350
    return-void

    .line 295
    .end local v4    # "actions":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    .end local v6    # "calendarEvent":Lcom/navdy/service/library/events/glances/GlanceEvent;
    .end local v7    # "data":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/glances/KeyValue;>;"
    .end local v9    # "hasLocationAction":Z
    .end local v12    # "id":Ljava/lang/String;
    .restart local v10    # "i":I
    :cond_6
    invoke-static {v5}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v19

    if-eqz v19, :cond_7

    .line 297
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v19

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, ": "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    goto/16 :goto_0

    .line 299
    :cond_7
    const-string v19, ","

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v11

    .line 300
    .local v11, "i2":I
    const/16 v19, -0x1

    move/from16 v0, v19

    if-eq v11, v0, :cond_8

    .line 302
    const/16 v19, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v19

    invoke-virtual {v0, v1, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v18

    .line 303
    add-int/lit8 v19, v11, 0x1

    move-object/from16 v0, v16

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v13

    goto/16 :goto_0

    .line 306
    :cond_8
    move-object/from16 v18, v16

    goto/16 :goto_0

    .line 318
    .end local v10    # "i":I
    .end local v11    # "i2":I
    .restart local v4    # "actions":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    .restart local v9    # "hasLocationAction":Z
    :cond_9
    const/4 v13, 0x0

    goto/16 :goto_1
.end method
