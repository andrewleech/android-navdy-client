.class public Lcom/navdy/client/app/framework/receiver/WifiReceiver;
.super Landroid/content/BroadcastReceiver;
.source "WifiReceiver.java"


# static fields
.field private static final logger:Lcom/navdy/service/library/log/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 26
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/framework/receiver/WifiReceiver;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/app/framework/receiver/WifiReceiver;->logger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v4, 0x1

    .line 30
    sget-object v2, Lcom/navdy/client/app/framework/receiver/WifiReceiver;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "onReceive connection"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 31
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 33
    .local v0, "appContext":Landroid/content/Context;
    invoke-static {v0}, Lcom/navdy/service/library/util/SystemUtils;->isConnectedToWifi(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->isLimitingCellularData()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 34
    sget-object v2, Lcom/navdy/client/app/framework/receiver/WifiReceiver;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "app is limiting cellular data, do not submit support tickets when wifi is off"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 59
    :cond_0
    :goto_0
    return-void

    .line 38
    :cond_1
    invoke-static {v0}, Lcom/navdy/service/library/util/SystemUtils;->isConnectedToNetwork(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 39
    invoke-static {}, Lcom/navdy/client/app/framework/util/SupportTicketService;->submitTicketsIfConditionsAreMet()V

    .line 42
    :cond_2
    invoke-static {v0}, Lcom/navdy/service/library/util/SystemUtils;->isConnectedToWifi(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 43
    sget-object v2, Lcom/navdy/client/app/framework/receiver/WifiReceiver;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Wi-Fi connected. Starting OTA Service"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 44
    invoke-static {v0}, Lcom/navdy/client/ota/OTAUpdateService;->getServiceIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    .line 45
    .local v1, "i":Landroid/content/Intent;
    const-string v2, "WIFI_TRIGGER"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 46
    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 47
    const/4 v2, 0x0

    invoke-static {p1, v2}, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->uploadTripDatabase(Landroid/content/Context;Z)V

    .line 49
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v2

    new-instance v3, Lcom/navdy/client/app/framework/receiver/WifiReceiver$1;

    invoke-direct {v3, p0}, Lcom/navdy/client/app/framework/receiver/WifiReceiver$1;-><init>(Lcom/navdy/client/app/framework/receiver/WifiReceiver;)V

    .line 50
    invoke-virtual {v2, v3, v4}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    goto :goto_0
.end method
