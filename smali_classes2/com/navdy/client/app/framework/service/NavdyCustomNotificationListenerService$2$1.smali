.class Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService$2$1;
.super Ljava/lang/Object;
.source "NavdyCustomNotificationListenerService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService$2;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService$2;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService$2;)V
    .locals 0
    .param p1, "this$1"    # Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService$2;

    .prologue
    .line 419
    iput-object p1, p0, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService$2$1;->this$1:Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService$2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const/4 v5, 0x1

    .line 422
    iget-object v4, p0, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService$2$1;->this$1:Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService$2;

    iget-object v4, v4, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService$2;->val$notif:Landroid/app/Notification;

    invoke-static {v4}, Landroid/support/v4/app/NotificationCompat;->getGroup(Landroid/app/Notification;)Ljava/lang/String;

    move-result-object v0

    .line 423
    .local v0, "group":Ljava/lang/String;
    invoke-static {v0}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    sget-object v4, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v6, p0, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService$2$1;->this$1:Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService$2;

    iget-object v6, v6, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService$2;->this$0:Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;

    .line 424
    invoke-static {v6}, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->access$100(Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;)Ljava/util/HashMap;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    move v4, v5

    .line 423
    :goto_0
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 425
    .local v2, "isKnown":Ljava/lang/Boolean;
    iget-object v4, p0, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService$2$1;->this$1:Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService$2;

    iget-object v4, v4, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService$2;->val$notif:Landroid/app/Notification;

    invoke-static {v4}, Landroid/support/v4/app/NotificationCompat;->isGroupSummary(Landroid/app/Notification;)Z

    move-result v1

    .line 427
    .local v1, "isGroupSummary":Z
    sget-object v4, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "NotifListenerService: handleStatusBarNotification: pkg ["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService$2$1;->this$1:Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService$2;

    iget-object v7, v7, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService$2;->val$packageName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " id ["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService$2$1;->this$1:Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService$2;

    iget-object v7, v7, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService$2;->val$sbn:Landroid/service/notification/StatusBarNotification;

    .line 430
    invoke-virtual {v7}, Landroid/service/notification/StatusBarNotification;->getId()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " tag ["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService$2$1;->this$1:Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService$2;

    iget-object v7, v7, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService$2;->val$sbn:Landroid/service/notification/StatusBarNotification;

    .line 431
    invoke-virtual {v7}, Landroid/service/notification/StatusBarNotification;->getTag()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " ticker["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService$2$1;->this$1:Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService$2;

    iget-object v7, v7, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService$2;->val$finalTicker:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " group["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " isKnownGroup["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " isGroupSummary["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 427
    invoke-virtual {v4, v6}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 437
    invoke-static {v0}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 438
    iget-object v4, p0, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService$2$1;->this$1:Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService$2;

    iget-object v4, v4, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService$2;->this$0:Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;

    invoke-static {v4}, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->access$100(Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;)Ljava/util/HashMap;

    move-result-object v4

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 440
    :cond_0
    if-nez v1, :cond_1

    iget-object v4, p0, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService$2$1;->this$1:Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService$2;

    iget-object v4, v4, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService$2;->val$packageName:Ljava/lang/String;

    const-string v5, "com.whatsapp"

    .line 442
    invoke-static {v4, v5}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 444
    :cond_1
    sget-object v4, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v4, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService$2$1;->this$1:Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService$2;

    iget-object v4, v4, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService$2;->val$packageName:Ljava/lang/String;

    const-string v5, "com.whatsapp"

    .line 445
    invoke-static {v4, v5}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 446
    sget-object v4, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "Ignoring summary notifications to prevent duplicates."

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 475
    :goto_1
    return-void

    .line 424
    .end local v1    # "isGroupSummary":Z
    .end local v2    # "isKnown":Ljava/lang/Boolean;
    :cond_2
    const/4 v4, 0x0

    goto/16 :goto_0

    .line 448
    .restart local v1    # "isGroupSummary":Z
    .restart local v2    # "isKnown":Ljava/lang/Boolean;
    :cond_3
    sget-object v4, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Remove Pending Handling For Group: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 449
    iget-object v4, p0, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService$2$1;->this$1:Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService$2;

    iget-object v4, v4, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService$2;->this$0:Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;

    invoke-virtual {v4, v0}, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->removePendingHandlingForGroup(Ljava/lang/String;)V

    .line 450
    new-instance v3, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService$2$1$1;

    invoke-direct {v3, p0}, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService$2$1$1;-><init>(Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService$2$1;)V

    .line 461
    .local v3, "r":Ljava/lang/Runnable;
    sget-object v4, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Adding Pending Handling For Group: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 462
    iget-object v4, p0, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService$2$1;->this$1:Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService$2;

    iget-object v4, v4, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService$2;->this$0:Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;

    invoke-static {v4}, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->access$300(Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;)Ljava/util/HashMap;

    move-result-object v4

    invoke-virtual {v4, v0, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 466
    sget-object v4, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->notifListenerServiceHandler:Landroid/os/Handler;

    const-wide/16 v6, 0x3e8

    invoke-virtual {v4, v3, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_1

    .line 470
    .end local v3    # "r":Ljava/lang/Runnable;
    :cond_4
    sget-object v4, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Remove Pending Handling For Group: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 471
    iget-object v4, p0, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService$2$1;->this$1:Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService$2;

    iget-object v4, v4, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService$2;->this$0:Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;

    invoke-virtual {v4, v0}, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->removePendingHandlingForGroup(Ljava/lang/String;)V

    .line 474
    iget-object v4, p0, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService$2$1;->this$1:Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService$2;

    iget-object v4, v4, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService$2;->this$0:Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;

    iget-object v5, p0, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService$2$1;->this$1:Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService$2;

    iget-object v5, v5, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService$2;->val$sbn:Landroid/service/notification/StatusBarNotification;

    iget-object v6, p0, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService$2$1;->this$1:Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService$2;

    iget-object v6, v6, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService$2;->val$packageName:Ljava/lang/String;

    iget-object v7, p0, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService$2$1;->this$1:Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService$2;

    iget-object v7, v7, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService$2;->val$finalTicker:Ljava/lang/String;

    invoke-static {v4, v5, v6, v7}, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->access$200(Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;Landroid/service/notification/StatusBarNotification;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1
.end method
