.class public Lcom/navdy/client/app/framework/util/MusicUtils;
.super Ljava/lang/Object;
.source "MusicUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/app/framework/util/MusicUtils$MediaKeyEventRunnable;
    }
.end annotation


# static fields
.field private static final ALBUM_ART_URI:Landroid/net/Uri;

.field public static final ARTWORK_SIZE:I = 0xc8

.field private static final BITMAP_OPTIONS:Landroid/graphics/BitmapFactory$Options;

.field private static final DEFAULT_ARTWORK_CHECKSUMS:[Ljava/lang/String;

.field private static final KEY_CODES_MAPPING:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/navdy/service/library/events/audio/MusicEvent$Action;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final MAX_PLAY_RETRIES:I = 0xa

.field private static final PAUSE_BETWEEN_RETRIES:J = 0x1f4L

.field private static final SEEK_KEY_EVENTS_MAPPING:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/navdy/service/library/events/audio/MusicEvent$Action;",
            "Landroid/view/KeyEvent;",
            ">;"
        }
    .end annotation
.end field

.field private static final SONG_ART_URI_POSTFIX:Ljava/lang/String; = "/albumart"

.field private static final SONG_URI:Ljava/lang/String; = "content://media/external/audio/media/"

.field private static volatile audioManager:Landroid/media/AudioManager;

.field private static localMusicPlayer:Lcom/navdy/client/app/framework/music/LocalMusicPlayer;

.field public static final sLogger:Lcom/navdy/service/library/log/Logger;

.field private static startedContinuousKeyEvent:Landroid/view/KeyEvent;

.field private static startedEventDataSource:Lcom/navdy/service/library/events/audio/MusicDataSource;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 59
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/framework/util/MusicUtils;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/app/framework/util/MusicUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 61
    const-string v0, "content://media/external/audio/albumart"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/navdy/client/app/framework/util/MusicUtils;->ALBUM_ART_URI:Landroid/net/Uri;

    .line 64
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    sput-object v0, Lcom/navdy/client/app/framework/util/MusicUtils;->BITMAP_OPTIONS:Landroid/graphics/BitmapFactory$Options;

    .line 72
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "a980062052ec31e96ae73029ec7e6772"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "6bbaf7d1b3318d9414d51d8a71705a04"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "3f89e08544651f0345b0ee39cb3ef3a4"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "59d9cdaa3c6f52efd34ce5e8d282cf5e"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "1cb6a67160592a08bf314e9742e0fed6"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "4dba0b6fe14bf656c92aed8d50b56b8d"

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/client/app/framework/util/MusicUtils;->DEFAULT_ARTWORK_CHECKSUMS:[Ljava/lang/String;

    .line 86
    new-instance v0, Lcom/navdy/client/app/framework/util/MusicUtils$1;

    invoke-direct {v0}, Lcom/navdy/client/app/framework/util/MusicUtils$1;-><init>()V

    sput-object v0, Lcom/navdy/client/app/framework/util/MusicUtils;->KEY_CODES_MAPPING:Ljava/util/Map;

    .line 94
    new-instance v0, Lcom/navdy/client/app/framework/util/MusicUtils$2;

    invoke-direct {v0}, Lcom/navdy/client/app/framework/util/MusicUtils$2;-><init>()V

    sput-object v0, Lcom/navdy/client/app/framework/util/MusicUtils;->SEEK_KEY_EVENTS_MAPPING:Ljava/util/Map;

    .line 109
    sput-object v3, Lcom/navdy/client/app/framework/util/MusicUtils;->startedContinuousKeyEvent:Landroid/view/KeyEvent;

    .line 113
    sput-object v3, Lcom/navdy/client/app/framework/util/MusicUtils;->startedEventDataSource:Lcom/navdy/service/library/events/audio/MusicDataSource;

    .line 115
    new-instance v0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;

    invoke-direct {v0}, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;-><init>()V

    sput-object v0, Lcom/navdy/client/app/framework/util/MusicUtils;->localMusicPlayer:Lcom/navdy/client/app/framework/music/LocalMusicPlayer;

    .line 118
    sput-object v3, Lcom/navdy/client/app/framework/util/MusicUtils;->audioManager:Landroid/media/AudioManager;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/navdy/service/library/events/audio/MusicEvent;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/service/library/events/audio/MusicEvent;

    .prologue
    .line 58
    invoke-static {p0}, Lcom/navdy/client/app/framework/util/MusicUtils;->handleLocalPlayEvent(Lcom/navdy/service/library/events/audio/MusicEvent;)V

    return-void
.end method

.method static synthetic access$400(Lcom/navdy/service/library/events/audio/MusicEvent;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/service/library/events/audio/MusicEvent;

    .prologue
    .line 58
    invoke-static {p0}, Lcom/navdy/client/app/framework/util/MusicUtils;->broadcastMusicEvent(Lcom/navdy/service/library/events/audio/MusicEvent;)V

    return-void
.end method

.method static synthetic access$500()Landroid/media/AudioManager;
    .locals 1

    .prologue
    .line 58
    sget-object v0, Lcom/navdy/client/app/framework/util/MusicUtils;->audioManager:Landroid/media/AudioManager;

    return-object v0
.end method

.method static synthetic access$600()Landroid/view/KeyEvent;
    .locals 1

    .prologue
    .line 58
    sget-object v0, Lcom/navdy/client/app/framework/util/MusicUtils;->startedContinuousKeyEvent:Landroid/view/KeyEvent;

    return-object v0
.end method

.method static synthetic access$602(Landroid/view/KeyEvent;)Landroid/view/KeyEvent;
    .locals 0
    .param p0, "x0"    # Landroid/view/KeyEvent;

    .prologue
    .line 58
    sput-object p0, Lcom/navdy/client/app/framework/util/MusicUtils;->startedContinuousKeyEvent:Landroid/view/KeyEvent;

    return-object p0
.end method

.method static synthetic access$700(Landroid/view/KeyEvent;)V
    .locals 0
    .param p0, "x0"    # Landroid/view/KeyEvent;

    .prologue
    .line 58
    invoke-static {p0}, Lcom/navdy/client/app/framework/util/MusicUtils;->dispatchMediaKeyEvent(Landroid/view/KeyEvent;)V

    return-void
.end method

.method static synthetic access$802(Lcom/navdy/service/library/events/audio/MusicDataSource;)Lcom/navdy/service/library/events/audio/MusicDataSource;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/service/library/events/audio/MusicDataSource;

    .prologue
    .line 58
    sput-object p0, Lcom/navdy/client/app/framework/util/MusicUtils;->startedEventDataSource:Lcom/navdy/service/library/events/audio/MusicDataSource;

    return-object p0
.end method

.method private static broadcastMusicEvent(Lcom/navdy/service/library/events/audio/MusicEvent;)V
    .locals 9
    .param p0, "event"    # Lcom/navdy/service/library/events/audio/MusicEvent;

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 345
    sget-object v4, Lcom/navdy/client/app/framework/util/MusicUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "broadcastMusicEvent"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 346
    invoke-static {}, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->getInstance()Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;

    move-result-object v3

    .line 347
    .local v3, "musicServiceHandler":Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;
    iget-object v0, p0, Lcom/navdy/service/library/events/audio/MusicEvent;->action:Lcom/navdy/service/library/events/audio/MusicEvent$Action;

    .line 349
    .local v0, "action":Lcom/navdy/service/library/events/audio/MusicEvent$Action;
    const/4 v1, 0x0

    .line 350
    .local v1, "eventRunnable":Lcom/navdy/client/app/framework/util/MusicUtils$MediaKeyEventRunnable;
    sget-object v4, Lcom/navdy/client/app/framework/util/MusicUtils;->KEY_CODES_MAPPING:Ljava/util/Map;

    invoke-interface {v4, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 351
    new-instance v1, Lcom/navdy/client/app/framework/util/MusicUtils$MediaKeyEventRunnable;

    .end local v1    # "eventRunnable":Lcom/navdy/client/app/framework/util/MusicUtils$MediaKeyEventRunnable;
    iget-object v5, p0, Lcom/navdy/service/library/events/audio/MusicEvent;->dataSource:Lcom/navdy/service/library/events/audio/MusicDataSource;

    new-instance v6, Landroid/view/KeyEvent;

    sget-object v4, Lcom/navdy/client/app/framework/util/MusicUtils;->KEY_CODES_MAPPING:Ljava/util/Map;

    .line 353
    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-direct {v6, v7, v4}, Landroid/view/KeyEvent;-><init>(II)V

    invoke-direct {v1, v5, v6, v8}, Lcom/navdy/client/app/framework/util/MusicUtils$MediaKeyEventRunnable;-><init>(Lcom/navdy/service/library/events/audio/MusicDataSource;Landroid/view/KeyEvent;Z)V

    .line 368
    .restart local v1    # "eventRunnable":Lcom/navdy/client/app/framework/util/MusicUtils$MediaKeyEventRunnable;
    :cond_0
    :goto_0
    if-eqz v1, :cond_1

    .line 369
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v4

    invoke-virtual {v4, v1, v8}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 372
    :cond_1
    :goto_1
    return-void

    .line 354
    :cond_2
    sget-object v4, Lcom/navdy/client/app/framework/util/MusicUtils;->SEEK_KEY_EVENTS_MAPPING:Ljava/util/Map;

    invoke-interface {v4, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 358
    invoke-virtual {v3}, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->getMusicSeekHelper()Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$MusicSeekHelper;

    move-result-object v2

    .line 359
    .local v2, "ffAndRewindRunnable":Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$MusicSeekHelper;
    if-eqz v2, :cond_3

    .line 360
    invoke-interface {v2, v0}, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$MusicSeekHelper;->executeMusicSeekAction(Lcom/navdy/service/library/events/audio/MusicEvent$Action;)V

    goto :goto_1

    .line 364
    :cond_3
    new-instance v1, Lcom/navdy/client/app/framework/util/MusicUtils$MediaKeyEventRunnable;

    .end local v1    # "eventRunnable":Lcom/navdy/client/app/framework/util/MusicUtils$MediaKeyEventRunnable;
    iget-object v5, p0, Lcom/navdy/service/library/events/audio/MusicEvent;->dataSource:Lcom/navdy/service/library/events/audio/MusicDataSource;

    sget-object v4, Lcom/navdy/client/app/framework/util/MusicUtils;->SEEK_KEY_EVENTS_MAPPING:Ljava/util/Map;

    .line 365
    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/KeyEvent;

    invoke-direct {v1, v5, v4, v7}, Lcom/navdy/client/app/framework/util/MusicUtils$MediaKeyEventRunnable;-><init>(Lcom/navdy/service/library/events/audio/MusicDataSource;Landroid/view/KeyEvent;Z)V

    .restart local v1    # "eventRunnable":Lcom/navdy/client/app/framework/util/MusicUtils$MediaKeyEventRunnable;
    goto :goto_0
.end method

.method private static dispatchMediaKeyEvent(Landroid/view/KeyEvent;)V
    .locals 3
    .param p0, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 528
    sget-object v0, Lcom/navdy/client/app/framework/util/MusicUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Dispatching key event: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 529
    sget-object v0, Lcom/navdy/client/app/framework/util/MusicUtils;->audioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, p0}, Landroid/media/AudioManager;->dispatchMediaKeyEvent(Landroid/view/KeyEvent;)V

    .line 530
    return-void
.end method

.method public static declared-synchronized executeLongPressedKeyUp()V
    .locals 5

    .prologue
    .line 516
    const-class v2, Lcom/navdy/client/app/framework/util/MusicUtils;

    monitor-enter v2

    :try_start_0
    sget-object v1, Lcom/navdy/client/app/framework/util/MusicUtils;->startedContinuousKeyEvent:Landroid/view/KeyEvent;

    if-eqz v1, :cond_0

    .line 517
    invoke-static {}, Lcom/navdy/client/app/framework/util/MusicUtils;->populateAudioManagerFromContext()V

    .line 518
    sget-object v1, Lcom/navdy/client/app/framework/util/MusicUtils;->startedContinuousKeyEvent:Landroid/view/KeyEvent;

    const/4 v3, 0x1

    invoke-static {v1, v3}, Landroid/view/KeyEvent;->changeAction(Landroid/view/KeyEvent;I)Landroid/view/KeyEvent;

    move-result-object v0

    .line 519
    .local v0, "event":Landroid/view/KeyEvent;
    sget-object v1, Lcom/navdy/client/app/framework/util/MusicUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Executing specific key event: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 520
    invoke-static {v0}, Lcom/navdy/client/app/framework/util/MusicUtils;->dispatchMediaKeyEvent(Landroid/view/KeyEvent;)V

    .line 521
    const/4 v1, 0x0

    sput-object v1, Lcom/navdy/client/app/framework/util/MusicUtils;->startedContinuousKeyEvent:Landroid/view/KeyEvent;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 525
    :cond_0
    monitor-exit v2

    return-void

    .line 516
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1
.end method

.method public static declared-synchronized executeMusicAction(Lcom/navdy/service/library/events/audio/MusicEvent;)V
    .locals 7
    .param p0, "event"    # Lcom/navdy/service/library/events/audio/MusicEvent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/UnsupportedOperationException;,
            Landroid/content/pm/PackageManager$NameNotFoundException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 130
    const-class v4, Lcom/navdy/client/app/framework/util/MusicUtils;

    monitor-enter v4

    :try_start_0
    sget-object v3, Lcom/navdy/client/app/framework/util/MusicUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "executeMusicAction "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 131
    invoke-static {}, Lcom/navdy/client/app/framework/util/MusicUtils;->populateAudioManagerFromContext()V

    .line 133
    invoke-static {}, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->getInstance()Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;

    move-result-object v2

    .line 135
    .local v2, "musicServiceHandler":Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;
    iget-object v0, p0, Lcom/navdy/service/library/events/audio/MusicEvent;->action:Lcom/navdy/service/library/events/audio/MusicEvent$Action;

    .line 136
    .local v0, "action":Lcom/navdy/service/library/events/audio/MusicEvent$Action;
    invoke-virtual {v2}, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->getCurrentMediaTrackInfo()Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    move-result-object v1

    .line 137
    .local v1, "currentTrackInfo":Lcom/navdy/service/library/events/audio/MusicTrackInfo;
    sget-object v3, Lcom/navdy/service/library/events/audio/MusicCollectionSource;->COLLECTION_SOURCE_ANDROID_LOCAL:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    iget-object v5, p0, Lcom/navdy/service/library/events/audio/MusicEvent;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    invoke-virtual {v3, v5}, Lcom/navdy/service/library/events/audio/MusicCollectionSource;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    sget-object v3, Lcom/navdy/service/library/events/audio/MusicEvent$Action;->MUSIC_ACTION_PLAY:Lcom/navdy/service/library/events/audio/MusicEvent$Action;

    .line 138
    invoke-virtual {v3, v0}, Lcom/navdy/service/library/events/audio/MusicEvent$Action;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/navdy/service/library/events/audio/MusicEvent;->index:Ljava/lang/Integer;

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/navdy/service/library/events/audio/MusicEvent;->shuffleMode:Lcom/navdy/service/library/events/audio/MusicShuffleMode;

    sget-object v5, Lcom/navdy/service/library/events/audio/MusicShuffleMode;->MUSIC_SHUFFLE_MODE_SONGS:Lcom/navdy/service/library/events/audio/MusicShuffleMode;

    if-eq v3, v5, :cond_0

    sget-object v3, Lcom/navdy/client/app/framework/util/MusicUtils;->localMusicPlayer:Lcom/navdy/client/app/framework/music/LocalMusicPlayer;

    if-eqz v3, :cond_2

    sget-object v3, Lcom/navdy/client/app/framework/util/MusicUtils;->localMusicPlayer:Lcom/navdy/client/app/framework/music/LocalMusicPlayer;

    invoke-virtual {v3}, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->isActive()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 139
    :cond_0
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v3

    new-instance v5, Lcom/navdy/client/app/framework/util/MusicUtils$3;

    invoke-direct {v5, p0}, Lcom/navdy/client/app/framework/util/MusicUtils$3;-><init>(Lcom/navdy/service/library/events/audio/MusicEvent;)V

    const/4 v6, 0x1

    invoke-virtual {v3, v5, v6}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 173
    :cond_1
    :goto_0
    monitor-exit v4

    return-void

    .line 145
    :cond_2
    :try_start_1
    sget-object v3, Lcom/navdy/service/library/events/audio/MusicCollectionSource;->COLLECTION_SOURCE_ANDROID_LOCAL:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    iget-object v5, v1, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    invoke-virtual {v3, v5}, Lcom/navdy/service/library/events/audio/MusicCollectionSource;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 146
    sget-object v3, Lcom/navdy/service/library/events/audio/MusicEvent$Action;->MUSIC_ACTION_PLAY:Lcom/navdy/service/library/events/audio/MusicEvent$Action;

    invoke-virtual {v3, v0}, Lcom/navdy/service/library/events/audio/MusicEvent$Action;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 147
    sget-object v3, Lcom/navdy/client/app/framework/util/MusicUtils;->localMusicPlayer:Lcom/navdy/client/app/framework/music/LocalMusicPlayer;

    invoke-virtual {v3}, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->play()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 130
    .end local v0    # "action":Lcom/navdy/service/library/events/audio/MusicEvent$Action;
    .end local v1    # "currentTrackInfo":Lcom/navdy/service/library/events/audio/MusicTrackInfo;
    .end local v2    # "musicServiceHandler":Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;
    :catchall_0
    move-exception v3

    monitor-exit v4

    throw v3

    .line 148
    .restart local v0    # "action":Lcom/navdy/service/library/events/audio/MusicEvent$Action;
    .restart local v1    # "currentTrackInfo":Lcom/navdy/service/library/events/audio/MusicTrackInfo;
    .restart local v2    # "musicServiceHandler":Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;
    :cond_3
    :try_start_2
    sget-object v3, Lcom/navdy/service/library/events/audio/MusicEvent$Action;->MUSIC_ACTION_PAUSE:Lcom/navdy/service/library/events/audio/MusicEvent$Action;

    invoke-virtual {v3, v0}, Lcom/navdy/service/library/events/audio/MusicEvent$Action;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 149
    sget-object v3, Lcom/navdy/client/app/framework/util/MusicUtils;->localMusicPlayer:Lcom/navdy/client/app/framework/music/LocalMusicPlayer;

    const/4 v5, 0x1

    invoke-virtual {v3, v5}, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->pause(Z)V

    goto :goto_0

    .line 150
    :cond_4
    sget-object v3, Lcom/navdy/service/library/events/audio/MusicEvent$Action;->MUSIC_ACTION_NEXT:Lcom/navdy/service/library/events/audio/MusicEvent$Action;

    invoke-virtual {v3, v0}, Lcom/navdy/service/library/events/audio/MusicEvent$Action;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 151
    sget-object v3, Lcom/navdy/client/app/framework/util/MusicUtils;->localMusicPlayer:Lcom/navdy/client/app/framework/music/LocalMusicPlayer;

    invoke-virtual {v3}, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->next()V

    goto :goto_0

    .line 152
    :cond_5
    sget-object v3, Lcom/navdy/service/library/events/audio/MusicEvent$Action;->MUSIC_ACTION_PREVIOUS:Lcom/navdy/service/library/events/audio/MusicEvent$Action;

    invoke-virtual {v3, v0}, Lcom/navdy/service/library/events/audio/MusicEvent$Action;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 153
    sget-object v3, Lcom/navdy/client/app/framework/util/MusicUtils;->localMusicPlayer:Lcom/navdy/client/app/framework/music/LocalMusicPlayer;

    invoke-virtual {v3}, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->previous()V

    goto :goto_0

    .line 154
    :cond_6
    sget-object v3, Lcom/navdy/client/app/framework/util/MusicUtils;->SEEK_KEY_EVENTS_MAPPING:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 155
    sget-object v3, Lcom/navdy/service/library/events/audio/MusicEvent$Action;->MUSIC_ACTION_FAST_FORWARD_START:Lcom/navdy/service/library/events/audio/MusicEvent$Action;

    invoke-virtual {v3, v0}, Lcom/navdy/service/library/events/audio/MusicEvent$Action;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 156
    sget-object v3, Lcom/navdy/client/app/framework/util/MusicUtils;->localMusicPlayer:Lcom/navdy/client/app/framework/music/LocalMusicPlayer;

    invoke-virtual {v3}, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->startFastForward()V

    goto :goto_0

    .line 157
    :cond_7
    sget-object v3, Lcom/navdy/service/library/events/audio/MusicEvent$Action;->MUSIC_ACTION_FAST_FORWARD_STOP:Lcom/navdy/service/library/events/audio/MusicEvent$Action;

    invoke-virtual {v3, v0}, Lcom/navdy/service/library/events/audio/MusicEvent$Action;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 158
    sget-object v3, Lcom/navdy/client/app/framework/util/MusicUtils;->localMusicPlayer:Lcom/navdy/client/app/framework/music/LocalMusicPlayer;

    invoke-virtual {v3}, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->stopFastForward()V

    goto :goto_0

    .line 159
    :cond_8
    sget-object v3, Lcom/navdy/service/library/events/audio/MusicEvent$Action;->MUSIC_ACTION_REWIND_START:Lcom/navdy/service/library/events/audio/MusicEvent$Action;

    invoke-virtual {v3, v0}, Lcom/navdy/service/library/events/audio/MusicEvent$Action;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 160
    sget-object v3, Lcom/navdy/client/app/framework/util/MusicUtils;->localMusicPlayer:Lcom/navdy/client/app/framework/music/LocalMusicPlayer;

    invoke-virtual {v3}, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->startRewind()V

    goto :goto_0

    .line 161
    :cond_9
    sget-object v3, Lcom/navdy/service/library/events/audio/MusicEvent$Action;->MUSIC_ACTION_REWIND_STOP:Lcom/navdy/service/library/events/audio/MusicEvent$Action;

    invoke-virtual {v3, v0}, Lcom/navdy/service/library/events/audio/MusicEvent$Action;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 162
    sget-object v3, Lcom/navdy/client/app/framework/util/MusicUtils;->localMusicPlayer:Lcom/navdy/client/app/framework/music/LocalMusicPlayer;

    invoke-virtual {v3}, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->stopRewind()V

    goto/16 :goto_0

    .line 164
    :cond_a
    sget-object v3, Lcom/navdy/service/library/events/audio/MusicEvent$Action;->MUSIC_ACTION_MODE_CHANGE:Lcom/navdy/service/library/events/audio/MusicEvent$Action;

    invoke-virtual {v3, v0}, Lcom/navdy/service/library/events/audio/MusicEvent$Action;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 165
    sget-object v3, Lcom/navdy/client/app/framework/util/MusicUtils;->localMusicPlayer:Lcom/navdy/client/app/framework/music/LocalMusicPlayer;

    iget-object v5, p0, Lcom/navdy/service/library/events/audio/MusicEvent;->shuffleMode:Lcom/navdy/service/library/events/audio/MusicShuffleMode;

    invoke-virtual {v3, v5}, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->shuffle(Lcom/navdy/service/library/events/audio/MusicShuffleMode;)V

    goto/16 :goto_0

    .line 167
    :cond_b
    invoke-virtual {v2}, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->isMusicPlayerActive()Z

    move-result v3

    if-nez v3, :cond_c

    sget-object v3, Lcom/navdy/service/library/events/audio/MusicEvent$Action;->MUSIC_ACTION_PLAY:Lcom/navdy/service/library/events/audio/MusicEvent$Action;

    .line 168
    invoke-virtual {v3, v0}, Lcom/navdy/service/library/events/audio/MusicEvent$Action;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 169
    invoke-static {p0}, Lcom/navdy/client/app/framework/util/MusicUtils;->startMediaPlayer(Lcom/navdy/service/library/events/audio/MusicEvent;)V

    goto/16 :goto_0

    .line 171
    :cond_c
    invoke-static {p0}, Lcom/navdy/client/app/framework/util/MusicUtils;->broadcastMusicEvent(Lcom/navdy/service/library/events/audio/MusicEvent;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0
.end method

.method protected static getAlbumIdForSong(Landroid/content/ContentResolver;J)J
    .locals 7
    .param p0, "cr"    # Landroid/content/ContentResolver;
    .param p1, "songId"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 610
    const/4 v6, 0x0

    .line 612
    .local v6, "song":Landroid/database/Cursor;
    :try_start_0
    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v3, "_id"

    aput-object v3, v2, v0

    const/4 v0, 0x1

    const-string v3, "album_id"

    aput-object v3, v2, v0

    const-string v3, "_id=?"

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    .line 615
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v0

    const/4 v5, 0x0

    move-object v0, p0

    .line 612
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 616
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_1

    .line 617
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Song with ID "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not found in media store"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 623
    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v0

    .line 620
    :cond_1
    :try_start_1
    const-string v0, "album_id"

    .line 621
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    .line 620
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v0

    .line 623
    invoke-static {v6}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    return-wide v0
.end method

.method public static getArtworkForAlbum(Landroid/content/ContentResolver;J)Landroid/graphics/Bitmap;
    .locals 7
    .param p0, "cr"    # Landroid/content/ContentResolver;
    .param p1, "albumId"    # J

    .prologue
    const/4 v3, 0x0

    .line 650
    sget-object v4, Lcom/navdy/client/app/framework/util/MusicUtils;->ALBUM_ART_URI:Landroid/net/Uri;

    invoke-static {v4, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    .line 651
    .local v2, "uri":Landroid/net/Uri;
    if-nez v2, :cond_0

    .line 652
    sget-object v4, Lcom/navdy/client/app/framework/util/MusicUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Couldn\'t get URI for album: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 665
    :goto_0
    return-object v3

    .line 656
    :cond_0
    const/4 v1, 0x0

    .line 658
    .local v1, "in":Ljava/io/InputStream;
    :try_start_0
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v1

    .line 659
    const/4 v4, 0x0

    sget-object v5, Lcom/navdy/client/app/framework/util/MusicUtils;->BITMAP_OPTIONS:Landroid/graphics/BitmapFactory$Options;

    invoke-static {v1, v4, v5}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    .line 663
    invoke-static {v1}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    goto :goto_0

    .line 660
    :catch_0
    move-exception v0

    .line 661
    .local v0, "e":Ljava/lang/Throwable;
    :try_start_1
    sget-object v4, Lcom/navdy/client/app/framework/util/MusicUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "Couldn\'t get album art"

    invoke-virtual {v4, v5, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 663
    invoke-static {v1}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    goto :goto_0

    .end local v0    # "e":Ljava/lang/Throwable;
    :catchall_0
    move-exception v3

    invoke-static {v1}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v3
.end method

.method private static getArtworkForAlbumWithSong(Landroid/content/ContentResolver;J)Landroid/graphics/Bitmap;
    .locals 7
    .param p0, "cr"    # Landroid/content/ContentResolver;
    .param p1, "songId"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;,
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 630
    invoke-static {p0, p1, p2}, Lcom/navdy/client/app/framework/util/MusicUtils;->getAlbumIdForSong(Landroid/content/ContentResolver;J)J

    move-result-wide v0

    .line 632
    .local v0, "albumId":J
    const-wide/16 v4, 0x0

    cmp-long v4, v0, v4

    if-gez v4, :cond_0

    .line 633
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "albumId must be positive or 0"

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 636
    :cond_0
    sget-object v4, Lcom/navdy/client/app/framework/util/MusicUtils;->ALBUM_ART_URI:Landroid/net/Uri;

    invoke-static {v4, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    .line 637
    .local v3, "uri":Landroid/net/Uri;
    if-nez v3, :cond_1

    .line 638
    new-instance v4, Ljava/io/FileNotFoundException;

    const-string v5, "Cannot build URI for album art"

    invoke-direct {v4, v5}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 641
    :cond_1
    invoke-virtual {p0, v3}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v2

    .line 643
    .local v2, "in":Ljava/io/InputStream;
    const/4 v4, 0x0

    :try_start_0
    sget-object v5, Lcom/navdy/client/app/framework/util/MusicUtils;->BITMAP_OPTIONS:Landroid/graphics/BitmapFactory$Options;

    invoke-static {v2, v4, v5}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v4

    .line 645
    invoke-static {v2}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    return-object v4

    :catchall_0
    move-exception v4

    invoke-static {v2}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v4
.end method

.method public static getArtworkForArtist(Landroid/content/ContentResolver;J)Landroid/graphics/Bitmap;
    .locals 13
    .param p0, "cr"    # Landroid/content/ContentResolver;
    .param p1, "artistId"    # J

    .prologue
    const/4 v8, 0x0

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 669
    sget-object v0, Lcom/navdy/client/app/framework/util/MusicUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "getArtworkForArtist "

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 670
    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 671
    .local v1, "songsUri":Landroid/net/Uri;
    new-array v2, v11, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v10

    .line 672
    .local v2, "songsColumns":[Ljava/lang/String;
    const-string v3, "artist_id = ?"

    .line 673
    .local v3, "query":Ljava/lang/String;
    new-array v4, v11, [Ljava/lang/String;

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v10

    .line 674
    .local v4, "args":[Ljava/lang/String;
    const/4 v7, 0x0

    .line 676
    .local v7, "songsCursor":Landroid/database/Cursor;
    const/4 v5, 0x0

    move-object v0, p0

    :try_start_0
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 677
    if-eqz v7, :cond_2

    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 679
    :cond_0
    const/4 v0, 0x0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/navdy/client/app/framework/util/MusicUtils;->getLocalMusicPhoto(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 680
    .local v6, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v6, :cond_1

    .line 681
    invoke-interface {v7}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 688
    invoke-static {v7}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 690
    .end local v6    # "bitmap":Landroid/graphics/Bitmap;
    :goto_0
    return-object v6

    .line 684
    .restart local v6    # "bitmap":Landroid/graphics/Bitmap;
    :cond_1
    :try_start_1
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 685
    invoke-interface {v7}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 688
    .end local v6    # "bitmap":Landroid/graphics/Bitmap;
    :cond_2
    invoke-static {v7}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    move-object v6, v8

    .line 690
    goto :goto_0

    .line 688
    :catchall_0
    move-exception v0

    invoke-static {v7}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v0
.end method

.method public static getArtworkForPlaylist(Landroid/content/ContentResolver;J)Landroid/graphics/Bitmap;
    .locals 3
    .param p0, "cr"    # Landroid/content/ContentResolver;
    .param p1, "playlistId"    # J

    .prologue
    .line 694
    long-to-int v2, p1

    invoke-static {v2}, Lcom/navdy/client/app/framework/util/MusicDbUtils;->getPlaylistMembersCursor(I)Landroid/database/Cursor;

    move-result-object v1

    .line 696
    .local v1, "membersCursor":Landroid/database/Cursor;
    if-eqz v1, :cond_1

    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 697
    const-string v2, "SourceId"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/navdy/client/app/framework/util/MusicUtils;->getLocalMusicPhoto(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 698
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_0

    .line 699
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 705
    invoke-static {v1}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 707
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    :goto_0
    return-object v0

    .line 702
    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_0
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 705
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_1
    invoke-static {v1}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 707
    const/4 v0, 0x0

    goto :goto_0

    .line 705
    :catchall_0
    move-exception v2

    invoke-static {v1}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v2
.end method

.method private static getArtworkForSong(Landroid/content/ContentResolver;Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 4
    .param p0, "cr"    # Landroid/content/ContentResolver;
    .param p1, "songIdStr"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/io/FileNotFoundException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 713
    sget-object v0, Lcom/navdy/client/app/framework/util/MusicUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getArtworkForSong "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 714
    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    .line 715
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "songId must be positive or 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 718
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "content://media/external/audio/media/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/albumart"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/navdy/client/app/framework/util/MusicUtils;->getBitmapFromUri(Landroid/content/ContentResolver;Landroid/net/Uri;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method private static getBitmapFromUri(Landroid/content/ContentResolver;Landroid/net/Uri;)Landroid/graphics/Bitmap;
    .locals 5
    .param p0, "cr"    # Landroid/content/ContentResolver;
    .param p1, "uri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 724
    sget-object v2, Lcom/navdy/client/app/framework/util/MusicUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getBitmapFromUri "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 725
    const-string v2, "r"

    invoke-virtual {p0, p1, v2}, Landroid/content/ContentResolver;->openFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;

    move-result-object v1

    .line 726
    .local v1, "pfd":Landroid/os/ParcelFileDescriptor;
    if-nez v1, :cond_0

    .line 727
    new-instance v2, Ljava/io/FileNotFoundException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Cannot open ParcelFileDescriptor for URI: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 731
    :cond_0
    :try_start_0
    invoke-virtual {v1}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v0

    .line 732
    .local v0, "fd":Ljava/io/FileDescriptor;
    const/4 v2, 0x0

    sget-object v3, Lcom/navdy/client/app/framework/util/MusicUtils;->BITMAP_OPTIONS:Landroid/graphics/BitmapFactory$Options;

    invoke-static {v0, v2, v3}, Landroid/graphics/BitmapFactory;->decodeFileDescriptor(Ljava/io/FileDescriptor;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 734
    invoke-static {v1}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    return-object v2

    .end local v0    # "fd":Ljava/io/FileDescriptor;
    :catchall_0
    move-exception v2

    invoke-static {v1}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v2
.end method

.method public static getLocalMusicPhoto(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 8
    .param p0, "songIdStr"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 567
    sget-object v5, Lcom/navdy/client/app/framework/util/MusicUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Trying to fetch local music artwork for song with ID "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 570
    if-nez p0, :cond_0

    .line 571
    sget-object v5, Lcom/navdy/client/app/framework/util/MusicUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "Null sent as song ID"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 601
    :goto_0
    return-object v4

    .line 576
    :cond_0
    :try_start_0
    invoke-static {p0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 582
    .local v2, "songId":J
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 585
    .local v0, "cr":Landroid/content/ContentResolver;
    :try_start_1
    invoke-static {v0, v2, v3}, Lcom/navdy/client/app/framework/util/MusicUtils;->getArtworkForAlbumWithSong(Landroid/content/ContentResolver;J)Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_4

    move-result-object v4

    goto :goto_0

    .line 577
    .end local v0    # "cr":Landroid/content/ContentResolver;
    .end local v2    # "songId":J
    :catch_0
    move-exception v1

    .line 578
    .local v1, "e":Ljava/lang/NumberFormatException;
    sget-object v5, Lcom/navdy/client/app/framework/util/MusicUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Cannot parse long from song\'s ID: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " - not local song"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 586
    .end local v1    # "e":Ljava/lang/NumberFormatException;
    .restart local v0    # "cr":Landroid/content/ContentResolver;
    .restart local v2    # "songId":J
    :catch_1
    move-exception v1

    .line 587
    .local v1, "e":Ljava/lang/Exception;
    :goto_1
    sget-object v5, Lcom/navdy/client/app/framework/util/MusicUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Cannot get artwork for album for the song with ID "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 591
    sget-object v5, Lcom/navdy/client/app/framework/util/MusicUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "Couldn\'t get artwork for album, trying song"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 593
    :try_start_2
    invoke-static {v0, p0}, Lcom/navdy/client/app/framework/util/MusicUtils;->getArtworkForSong(Landroid/content/ContentResolver;Ljava/lang/String;)Landroid/graphics/Bitmap;
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_5
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_6

    move-result-object v4

    goto :goto_0

    .line 594
    :catch_2
    move-exception v1

    .line 595
    .local v1, "e":Ljava/lang/NumberFormatException;
    sget-object v5, Lcom/navdy/client/app/framework/util/MusicUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Cannot parse received song ID "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " to long "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 596
    .local v1, "e":Ljava/lang/Exception;
    :catch_3
    move-exception v1

    .line 597
    :goto_2
    sget-object v5, Lcom/navdy/client/app/framework/util/MusicUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Cannot get artwork for song with ID "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 586
    .end local v1    # "e":Ljava/lang/Exception;
    :catch_4
    move-exception v1

    goto :goto_1

    .line 596
    .restart local v1    # "e":Ljava/lang/Exception;
    :catch_5
    move-exception v1

    goto :goto_2

    :catch_6
    move-exception v1

    goto :goto_2
.end method

.method public static getMusicPhoto(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 4
    .param p0, "songIdStr"    # Ljava/lang/String;

    .prologue
    .line 544
    sget-object v1, Lcom/navdy/client/app/framework/util/MusicUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Trying to fetch music artwork for song with ID "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 546
    invoke-static {}, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->getInstance()Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->getCurrentMediaArtwork()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 547
    .local v0, "currentArtwork":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_0

    .line 551
    .end local v0    # "currentArtwork":Landroid/graphics/Bitmap;
    :goto_0
    return-object v0

    .restart local v0    # "currentArtwork":Landroid/graphics/Bitmap;
    :cond_0
    invoke-static {p0}, Lcom/navdy/client/app/framework/util/MusicUtils;->getLocalMusicPhoto(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method private static handleLocalPlayEvent(Lcom/navdy/service/library/events/audio/MusicEvent;)V
    .locals 6
    .param p0, "event"    # Lcom/navdy/service/library/events/audio/MusicEvent;

    .prologue
    .line 176
    invoke-static {p0}, Lcom/navdy/client/app/framework/util/MusicUtils;->isNewCollection(Lcom/navdy/service/library/events/audio/MusicEvent;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 177
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 178
    .local v2, "trackList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicTrackInfo;>;"
    sget-object v3, Lcom/navdy/service/library/events/audio/MusicCollectionType;->COLLECTION_TYPE_PLAYLISTS:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    iget-object v4, p0, Lcom/navdy/service/library/events/audio/MusicEvent;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/events/audio/MusicCollectionType;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 179
    iget-object v3, p0, Lcom/navdy/service/library/events/audio/MusicEvent;->collectionId:Ljava/lang/String;

    invoke-static {v3}, Lcom/navdy/client/app/framework/util/MusicDbUtils;->getPlaylistMembersCursor(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 181
    .local v0, "membersCursor":Landroid/database/Cursor;
    if-eqz v0, :cond_1

    :try_start_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 183
    :cond_0
    new-instance v3, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    invoke-direct {v3}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;-><init>()V

    iget-object v4, p0, Lcom/navdy/service/library/events/audio/MusicEvent;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    .line 184
    invoke-virtual {v3, v4}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->collectionSource(Lcom/navdy/service/library/events/audio/MusicCollectionSource;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/service/library/events/audio/MusicEvent;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    .line 185
    invoke-virtual {v3, v4}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->collectionType(Lcom/navdy/service/library/events/audio/MusicCollectionType;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/service/library/events/audio/MusicEvent;->collectionId:Ljava/lang/String;

    .line 186
    invoke-virtual {v3, v4}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->collectionId(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v3

    const-string v4, "title"

    .line 187
    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->name(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v3

    const-string v4, "artist"

    .line 188
    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->author(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v3

    const-string v4, "album"

    .line 189
    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->album(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v3

    const-string v4, "SourceId"

    .line 190
    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->trackId(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v3

    sget-object v4, Lcom/navdy/service/library/events/audio/MusicPlaybackState;->PLAYBACK_PLAYING:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    .line 191
    invoke-virtual {v3, v4}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->playbackState(Lcom/navdy/service/library/events/audio/MusicPlaybackState;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v3

    const/4 v4, 0x1

    .line 192
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->isNextAllowed(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v3

    const/4 v4, 0x1

    .line 193
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->isPreviousAllowed(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v3

    .line 194
    invoke-virtual {v3}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->build()Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    move-result-object v3

    .line 183
    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 195
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 196
    invoke-interface {v0}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 201
    :cond_1
    invoke-static {v0}, Lcom/navdy/service/library/util/IOUtils;->closeObject(Ljava/io/Closeable;)V

    .line 237
    :goto_0
    sget-object v3, Lcom/navdy/client/app/framework/util/MusicUtils;->localMusicPlayer:Lcom/navdy/client/app/framework/music/LocalMusicPlayer;

    invoke-virtual {v3, v2}, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->initWithQueue(Ljava/util/List;)V

    .line 238
    iget-object v3, p0, Lcom/navdy/service/library/events/audio/MusicEvent;->shuffleMode:Lcom/navdy/service/library/events/audio/MusicShuffleMode;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/navdy/service/library/events/audio/MusicEvent;->shuffleMode:Lcom/navdy/service/library/events/audio/MusicShuffleMode;

    sget-object v4, Lcom/navdy/service/library/events/audio/MusicShuffleMode;->MUSIC_SHUFFLE_MODE_UNKNOWN:Lcom/navdy/service/library/events/audio/MusicShuffleMode;

    if-eq v3, v4, :cond_2

    .line 239
    sget-object v3, Lcom/navdy/client/app/framework/util/MusicUtils;->localMusicPlayer:Lcom/navdy/client/app/framework/music/LocalMusicPlayer;

    iget-object v4, p0, Lcom/navdy/service/library/events/audio/MusicEvent;->shuffleMode:Lcom/navdy/service/library/events/audio/MusicShuffleMode;

    invoke-virtual {v3, v4}, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->shuffle(Lcom/navdy/service/library/events/audio/MusicShuffleMode;)V

    .line 241
    :cond_2
    sget-object v3, Lcom/navdy/client/app/framework/util/MusicUtils;->localMusicPlayer:Lcom/navdy/client/app/framework/music/LocalMusicPlayer;

    invoke-virtual {v3}, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->getListener()Lcom/navdy/client/app/framework/music/LocalMusicPlayer$Listener;

    move-result-object v3

    if-nez v3, :cond_3

    .line 242
    sget-object v3, Lcom/navdy/client/app/framework/util/MusicUtils;->localMusicPlayer:Lcom/navdy/client/app/framework/music/LocalMusicPlayer;

    new-instance v4, Lcom/navdy/client/app/framework/util/MusicUtils$4;

    invoke-direct {v4}, Lcom/navdy/client/app/framework/util/MusicUtils$4;-><init>()V

    invoke-virtual {v3, v4}, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->setListener(Lcom/navdy/client/app/framework/music/LocalMusicPlayer$Listener;)V

    .line 319
    .end local v0    # "membersCursor":Landroid/database/Cursor;
    .end local v2    # "trackList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicTrackInfo;>;"
    :cond_3
    iget-object v3, p0, Lcom/navdy/service/library/events/audio/MusicEvent;->index:Ljava/lang/Integer;

    if-eqz v3, :cond_9

    .line 320
    sget-object v3, Lcom/navdy/client/app/framework/util/MusicUtils;->localMusicPlayer:Lcom/navdy/client/app/framework/music/LocalMusicPlayer;

    iget-object v4, p0, Lcom/navdy/service/library/events/audio/MusicEvent;->index:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->play(I)V

    .line 327
    :goto_1
    return-void

    .line 198
    .restart local v0    # "membersCursor":Landroid/database/Cursor;
    .restart local v2    # "trackList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicTrackInfo;>;"
    :catch_0
    move-exception v1

    .line 199
    .local v1, "t":Ljava/lang/Throwable;
    :try_start_1
    sget-object v3, Lcom/navdy/client/app/framework/util/MusicUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Error querying GPM database: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 201
    invoke-static {v0}, Lcom/navdy/service/library/util/IOUtils;->closeObject(Ljava/io/Closeable;)V

    goto :goto_0

    .end local v1    # "t":Ljava/lang/Throwable;
    :catchall_0
    move-exception v3

    invoke-static {v0}, Lcom/navdy/service/library/util/IOUtils;->closeObject(Ljava/io/Closeable;)V

    throw v3

    .line 205
    .end local v0    # "membersCursor":Landroid/database/Cursor;
    :cond_4
    sget-object v3, Lcom/navdy/service/library/events/audio/MusicCollectionType;->COLLECTION_TYPE_ALBUMS:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    iget-object v4, p0, Lcom/navdy/service/library/events/audio/MusicEvent;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/events/audio/MusicCollectionType;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 206
    iget-object v3, p0, Lcom/navdy/service/library/events/audio/MusicEvent;->collectionId:Ljava/lang/String;

    invoke-static {v3}, Lcom/navdy/client/app/framework/util/MusicDbUtils;->getAlbumMembersCursor(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 214
    .restart local v0    # "membersCursor":Landroid/database/Cursor;
    :goto_2
    if-eqz v0, :cond_6

    :try_start_2
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 216
    :cond_5
    new-instance v3, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    invoke-direct {v3}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;-><init>()V

    iget-object v4, p0, Lcom/navdy/service/library/events/audio/MusicEvent;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    .line 217
    invoke-virtual {v3, v4}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->collectionSource(Lcom/navdy/service/library/events/audio/MusicCollectionSource;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/service/library/events/audio/MusicEvent;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    .line 218
    invoke-virtual {v3, v4}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->collectionType(Lcom/navdy/service/library/events/audio/MusicCollectionType;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/service/library/events/audio/MusicEvent;->collectionId:Ljava/lang/String;

    .line 219
    invoke-virtual {v3, v4}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->collectionId(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v3

    const-string v4, "title"

    .line 220
    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->name(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v3

    const-string v4, "artist"

    .line 221
    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->author(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v3

    const-string v4, "album"

    .line 222
    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->album(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v3

    const-string v4, "_id"

    .line 223
    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->trackId(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v3

    sget-object v4, Lcom/navdy/service/library/events/audio/MusicPlaybackState;->PLAYBACK_PLAYING:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    .line 224
    invoke-virtual {v3, v4}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->playbackState(Lcom/navdy/service/library/events/audio/MusicPlaybackState;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v3

    const/4 v4, 0x1

    .line 225
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->isNextAllowed(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v3

    const/4 v4, 0x1

    .line 226
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->isPreviousAllowed(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v3

    .line 227
    invoke-virtual {v3}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->build()Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    move-result-object v3

    .line 216
    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 228
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-nez v3, :cond_5

    .line 229
    invoke-interface {v0}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 234
    :cond_6
    invoke-static {v0}, Lcom/navdy/service/library/util/IOUtils;->closeObject(Ljava/io/Closeable;)V

    goto/16 :goto_0

    .line 207
    .end local v0    # "membersCursor":Landroid/database/Cursor;
    :cond_7
    sget-object v3, Lcom/navdy/service/library/events/audio/MusicCollectionType;->COLLECTION_TYPE_ARTISTS:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    iget-object v4, p0, Lcom/navdy/service/library/events/audio/MusicEvent;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/events/audio/MusicCollectionType;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 208
    iget-object v3, p0, Lcom/navdy/service/library/events/audio/MusicEvent;->collectionId:Ljava/lang/String;

    invoke-static {v3}, Lcom/navdy/client/app/framework/util/MusicDbUtils;->getArtistMembersCursor(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .restart local v0    # "membersCursor":Landroid/database/Cursor;
    goto/16 :goto_2

    .line 210
    .end local v0    # "membersCursor":Landroid/database/Cursor;
    :cond_8
    sget-object v3, Lcom/navdy/client/app/framework/util/MusicUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "Unknown type"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 231
    .restart local v0    # "membersCursor":Landroid/database/Cursor;
    :catch_1
    move-exception v1

    .line 232
    .restart local v1    # "t":Ljava/lang/Throwable;
    :try_start_3
    sget-object v3, Lcom/navdy/client/app/framework/util/MusicUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Error querying MediaStore database: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 234
    invoke-static {v0}, Lcom/navdy/service/library/util/IOUtils;->closeObject(Ljava/io/Closeable;)V

    goto/16 :goto_0

    .end local v1    # "t":Ljava/lang/Throwable;
    :catchall_1
    move-exception v3

    invoke-static {v0}, Lcom/navdy/service/library/util/IOUtils;->closeObject(Ljava/io/Closeable;)V

    throw v3

    .line 321
    .end local v0    # "membersCursor":Landroid/database/Cursor;
    .end local v2    # "trackList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicTrackInfo;>;"
    :cond_9
    iget-object v3, p0, Lcom/navdy/service/library/events/audio/MusicEvent;->shuffleMode:Lcom/navdy/service/library/events/audio/MusicShuffleMode;

    sget-object v4, Lcom/navdy/service/library/events/audio/MusicShuffleMode;->MUSIC_SHUFFLE_MODE_SONGS:Lcom/navdy/service/library/events/audio/MusicShuffleMode;

    if-ne v3, v4, :cond_a

    .line 322
    sget-object v3, Lcom/navdy/client/app/framework/util/MusicUtils;->localMusicPlayer:Lcom/navdy/client/app/framework/music/LocalMusicPlayer;

    iget-object v4, p0, Lcom/navdy/service/library/events/audio/MusicEvent;->shuffleMode:Lcom/navdy/service/library/events/audio/MusicShuffleMode;

    invoke-virtual {v3, v4}, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->shuffle(Lcom/navdy/service/library/events/audio/MusicShuffleMode;)V

    .line 323
    sget-object v3, Lcom/navdy/client/app/framework/util/MusicUtils;->localMusicPlayer:Lcom/navdy/client/app/framework/music/LocalMusicPlayer;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->play(I)V

    goto/16 :goto_1

    .line 325
    :cond_a
    sget-object v3, Lcom/navdy/client/app/framework/util/MusicUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "No event index and not shuffling!!!"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method public static isDefaultArtwork(Ljava/lang/String;)Z
    .locals 1
    .param p0, "checksum"    # Ljava/lang/String;

    .prologue
    .line 605
    sget-object v0, Lcom/navdy/client/app/framework/util/MusicUtils;->DEFAULT_ARTWORK_CHECKSUMS:[Ljava/lang/String;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private static isNewCollection(Lcom/navdy/service/library/events/audio/MusicEvent;)Z
    .locals 4
    .param p0, "event"    # Lcom/navdy/service/library/events/audio/MusicEvent;

    .prologue
    .line 336
    invoke-static {}, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->getInstance()Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;

    move-result-object v1

    .line 337
    .local v1, "musicServiceHandler":Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;
    invoke-virtual {v1}, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->getCurrentMediaTrackInfo()Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    move-result-object v0

    .line 338
    .local v0, "currentTrackInfo":Lcom/navdy/service/library/events/audio/MusicTrackInfo;
    iget-object v2, v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    iget-object v3, p0, Lcom/navdy/service/library/events/audio/MusicEvent;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    if-ne v2, v3, :cond_0

    iget-object v2, v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    iget-object v3, p0, Lcom/navdy/service/library/events/audio/MusicEvent;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    if-ne v2, v3, :cond_0

    iget-object v2, v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->collectionId:Ljava/lang/String;

    iget-object v3, p0, Lcom/navdy/service/library/events/audio/MusicEvent;->collectionId:Ljava/lang/String;

    .line 340
    invoke-static {v2, v3}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->shuffleMode:Lcom/navdy/service/library/events/audio/MusicShuffleMode;

    iget-object v3, p0, Lcom/navdy/service/library/events/audio/MusicEvent;->shuffleMode:Lcom/navdy/service/library/events/audio/MusicShuffleMode;

    if-eq v2, v3, :cond_1

    :cond_0
    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private static populateAudioManagerFromContext()V
    .locals 2

    .prologue
    .line 121
    sget-object v0, Lcom/navdy/client/app/framework/util/MusicUtils;->audioManager:Landroid/media/AudioManager;

    if-nez v0, :cond_0

    .line 122
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    sput-object v0, Lcom/navdy/client/app/framework/util/MusicUtils;->audioManager:Landroid/media/AudioManager;

    .line 124
    :cond_0
    return-void
.end method

.method private static resolveMediaPlayerIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 427
    invoke-static {}, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->getInstance()Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->getLastMusicApp()Ljava/lang/String;

    move-result-object v0

    .line 430
    .local v0, "packageName":Ljava/lang/String;
    invoke-static {v0}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, Lcom/navdy/client/app/framework/util/SystemUtils;->isPackageInstalled(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 432
    :cond_0
    const-string v1, "com.pandora.android"

    invoke-static {v1}, Lcom/navdy/client/app/framework/util/SystemUtils;->isPackageInstalled(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 433
    const-string v0, "com.pandora.android"

    .line 441
    :cond_1
    :goto_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    return-object v1

    .line 434
    :cond_2
    const-string v1, "com.spotify.music"

    invoke-static {v1}, Lcom/navdy/client/app/framework/util/SystemUtils;->isPackageInstalled(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 435
    const-string v0, "com.spotify.music"

    goto :goto_0

    .line 437
    :cond_3
    const-string v0, "com.google.android.music"

    goto :goto_0
.end method

.method private static sendPlaybackIntents(Ljava/lang/String;Lcom/navdy/service/library/events/audio/MusicEvent;)V
    .locals 3
    .param p0, "packageName"    # Ljava/lang/String;
    .param p1, "event"    # Lcom/navdy/service/library/events/audio/MusicEvent;

    .prologue
    .line 390
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/client/app/framework/util/MusicUtils$5;

    invoke-direct {v1, p0, p1}, Lcom/navdy/client/app/framework/util/MusicUtils$5;-><init>(Ljava/lang/String;Lcom/navdy/service/library/events/audio/MusicEvent;)V

    const/16 v2, 0xa

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 423
    return-void
.end method

.method private static startMediaPlayer(Lcom/navdy/service/library/events/audio/MusicEvent;)V
    .locals 6
    .param p0, "event"    # Lcom/navdy/service/library/events/audio/MusicEvent;

    .prologue
    .line 375
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 377
    .local v0, "context":Landroid/content/Context;
    invoke-static {v0}, Lcom/navdy/client/app/framework/util/MusicUtils;->resolveMediaPlayerIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    .line 378
    .local v1, "intent":Landroid/content/Intent;
    if-eqz v1, :cond_0

    .line 379
    invoke-virtual {v1}, Landroid/content/Intent;->getPackage()Ljava/lang/String;

    move-result-object v2

    .line 380
    .local v2, "packageName":Ljava/lang/String;
    sget-object v3, Lcom/navdy/client/app/framework/util/MusicUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "startMediaPlayer "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 381
    const/high16 v3, 0x10000000

    invoke-virtual {v1, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 382
    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 383
    invoke-static {v2, p0}, Lcom/navdy/client/app/framework/util/MusicUtils;->sendPlaybackIntents(Ljava/lang/String;Lcom/navdy/service/library/events/audio/MusicEvent;)V

    .line 387
    .end local v2    # "packageName":Ljava/lang/String;
    :goto_0
    return-void

    .line 385
    :cond_0
    sget-object v3, Lcom/navdy/client/app/framework/util/MusicUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "startMediaPlayer couldn\'t create an intent to start a media player"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static stopInternalMusicPlayer()V
    .locals 2

    .prologue
    .line 330
    sget-object v0, Lcom/navdy/client/app/framework/util/MusicUtils;->localMusicPlayer:Lcom/navdy/client/app/framework/music/LocalMusicPlayer;

    if-eqz v0, :cond_0

    .line 331
    sget-object v0, Lcom/navdy/client/app/framework/util/MusicUtils;->localMusicPlayer:Lcom/navdy/client/app/framework/music/LocalMusicPlayer;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->pause(Z)V

    .line 333
    :cond_0
    return-void
.end method
