.class Lcom/navdy/client/app/framework/util/MusicUtils$MediaKeyEventRunnable;
.super Ljava/lang/Object;
.source "MusicUtils.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/framework/util/MusicUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MediaKeyEventRunnable"
.end annotation


# instance fields
.field private dataSource:Lcom/navdy/service/library/events/audio/MusicDataSource;

.field private event:Landroid/view/KeyEvent;

.field private isDownUpEvent:Z


# direct methods
.method public constructor <init>(Lcom/navdy/service/library/events/audio/MusicDataSource;Landroid/view/KeyEvent;Z)V
    .locals 0
    .param p1, "dataSource"    # Lcom/navdy/service/library/events/audio/MusicDataSource;
    .param p2, "event"    # Landroid/view/KeyEvent;
    .param p3, "isDownUpEvent"    # Z

    .prologue
    .line 451
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 452
    iput-object p1, p0, Lcom/navdy/client/app/framework/util/MusicUtils$MediaKeyEventRunnable;->dataSource:Lcom/navdy/service/library/events/audio/MusicDataSource;

    .line 453
    iput-object p2, p0, Lcom/navdy/client/app/framework/util/MusicUtils$MediaKeyEventRunnable;->event:Landroid/view/KeyEvent;

    .line 454
    iput-boolean p3, p0, Lcom/navdy/client/app/framework/util/MusicUtils$MediaKeyEventRunnable;->isDownUpEvent:Z

    .line 455
    return-void
.end method

.method private executeKeyDownUp(Landroid/view/KeyEvent;)V
    .locals 2
    .param p1, "eventToRun"    # Landroid/view/KeyEvent;

    .prologue
    .line 458
    invoke-static {}, Lcom/navdy/client/app/framework/util/MusicUtils;->access$500()Landroid/media/AudioManager;

    move-result-object v0

    const/4 v1, 0x0

    .line 459
    invoke-static {p1, v1}, Landroid/view/KeyEvent;->changeAction(Landroid/view/KeyEvent;I)Landroid/view/KeyEvent;

    move-result-object v1

    .line 458
    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->dispatchMediaKeyEvent(Landroid/view/KeyEvent;)V

    .line 460
    invoke-static {}, Lcom/navdy/client/app/framework/util/MusicUtils;->access$500()Landroid/media/AudioManager;

    move-result-object v0

    const/4 v1, 0x1

    .line 461
    invoke-static {p1, v1}, Landroid/view/KeyEvent;->changeAction(Landroid/view/KeyEvent;I)Landroid/view/KeyEvent;

    move-result-object v1

    .line 460
    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->dispatchMediaKeyEvent(Landroid/view/KeyEvent;)V

    .line 462
    return-void
.end method

.method private static declared-synchronized executeSplitKeyEvent(Lcom/navdy/service/library/events/audio/MusicDataSource;Landroid/view/KeyEvent;)V
    .locals 8
    .param p0, "dataSource"    # Lcom/navdy/service/library/events/audio/MusicDataSource;
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v4, 0x1

    .line 475
    const-class v5, Lcom/navdy/client/app/framework/util/MusicUtils$MediaKeyEventRunnable;

    monitor-enter v5

    :try_start_0
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    .line 477
    .local v0, "eventAction":I
    if-nez v0, :cond_1

    .line 480
    invoke-static {}, Lcom/navdy/client/app/framework/util/MusicUtils;->access$600()Landroid/view/KeyEvent;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 481
    sget-object v4, Lcom/navdy/client/app/framework/util/MusicUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Other key down event already started pressed event - ending it: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 482
    invoke-static {}, Lcom/navdy/client/app/framework/util/MusicUtils;->access$600()Landroid/view/KeyEvent;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 481
    invoke-virtual {v4, v6}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 483
    invoke-static {}, Lcom/navdy/client/app/framework/util/MusicUtils;->executeLongPressedKeyUp()V

    .line 485
    :cond_0
    invoke-static {p1}, Lcom/navdy/client/app/framework/util/MusicUtils;->access$700(Landroid/view/KeyEvent;)V

    .line 487
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    .line 488
    .local v2, "uptime":J
    const/4 v4, 0x0

    invoke-static {p1, v2, v3, v4}, Landroid/view/KeyEvent;->changeTimeRepeat(Landroid/view/KeyEvent;JI)Landroid/view/KeyEvent;

    move-result-object v4

    invoke-static {v4}, Lcom/navdy/client/app/framework/util/MusicUtils;->access$602(Landroid/view/KeyEvent;)Landroid/view/KeyEvent;

    .line 489
    invoke-static {p0}, Lcom/navdy/client/app/framework/util/MusicUtils;->access$802(Lcom/navdy/service/library/events/audio/MusicDataSource;)Lcom/navdy/service/library/events/audio/MusicDataSource;

    .line 490
    sget-object v4, Lcom/navdy/client/app/framework/util/MusicUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Executing specific key event: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {}, Lcom/navdy/client/app/framework/util/MusicUtils;->access$600()Landroid/view/KeyEvent;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " with after-event for long press."

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 492
    invoke-static {}, Lcom/navdy/client/app/framework/util/MusicUtils;->access$600()Landroid/view/KeyEvent;

    move-result-object v4

    invoke-static {v4}, Lcom/navdy/client/app/framework/util/MusicUtils;->access$700(Landroid/view/KeyEvent;)V

    .line 493
    const/4 v4, 0x1

    .line 494
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getFlags()I

    move-result v6

    or-int/lit16 v6, v6, 0x80

    .line 493
    invoke-static {p1, v2, v3, v4, v6}, Landroid/view/KeyEvent;->changeTimeRepeat(Landroid/view/KeyEvent;JII)Landroid/view/KeyEvent;

    move-result-object v1

    .line 495
    .local v1, "repeatedEvent":Landroid/view/KeyEvent;
    invoke-static {v1}, Lcom/navdy/client/app/framework/util/MusicUtils;->access$700(Landroid/view/KeyEvent;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 512
    .end local v1    # "repeatedEvent":Landroid/view/KeyEvent;
    .end local v2    # "uptime":J
    :goto_0
    monitor-exit v5

    return-void

    .line 497
    :cond_1
    if-ne v0, v4, :cond_4

    .line 499
    :try_start_1
    invoke-static {}, Lcom/navdy/client/app/framework/util/MusicUtils;->access$600()Landroid/view/KeyEvent;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 500
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v4

    invoke-static {}, Lcom/navdy/client/app/framework/util/MusicUtils;->access$600()Landroid/view/KeyEvent;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v6

    if-eq v4, v6, :cond_2

    .line 501
    sget-object v4, Lcom/navdy/client/app/framework/util/MusicUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Unmatched key up event - ending the current just in case: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 502
    invoke-static {}, Lcom/navdy/client/app/framework/util/MusicUtils;->access$600()Landroid/view/KeyEvent;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 501
    invoke-virtual {v4, v6}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 504
    :cond_2
    invoke-static {}, Lcom/navdy/client/app/framework/util/MusicUtils;->executeLongPressedKeyUp()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 475
    .end local v0    # "eventAction":I
    :catchall_0
    move-exception v4

    monitor-exit v5

    throw v4

    .line 506
    .restart local v0    # "eventAction":I
    :cond_3
    :try_start_2
    sget-object v4, Lcom/navdy/client/app/framework/util/MusicUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Unexpected key up event - doing nothing: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    goto :goto_0

    .line 510
    :cond_4
    invoke-static {p1}, Lcom/navdy/client/app/framework/util/MusicUtils;->access$700(Landroid/view/KeyEvent;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 467
    iget-boolean v0, p0, Lcom/navdy/client/app/framework/util/MusicUtils$MediaKeyEventRunnable;->isDownUpEvent:Z

    if-eqz v0, :cond_0

    .line 468
    iget-object v0, p0, Lcom/navdy/client/app/framework/util/MusicUtils$MediaKeyEventRunnable;->event:Landroid/view/KeyEvent;

    invoke-direct {p0, v0}, Lcom/navdy/client/app/framework/util/MusicUtils$MediaKeyEventRunnable;->executeKeyDownUp(Landroid/view/KeyEvent;)V

    .line 472
    :goto_0
    return-void

    .line 470
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/framework/util/MusicUtils$MediaKeyEventRunnable;->dataSource:Lcom/navdy/service/library/events/audio/MusicDataSource;

    iget-object v1, p0, Lcom/navdy/client/app/framework/util/MusicUtils$MediaKeyEventRunnable;->event:Landroid/view/KeyEvent;

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/MusicUtils$MediaKeyEventRunnable;->executeSplitKeyEvent(Lcom/navdy/service/library/events/audio/MusicDataSource;Landroid/view/KeyEvent;)V

    goto :goto_0
.end method
