.class Lcom/navdy/client/app/framework/util/ContactObserver$2;
.super Ljava/lang/Object;
.source "ContactObserver.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/framework/util/ContactObserver;-><init>(Lcom/navdy/client/app/framework/servicehandler/ContactServiceHandler;Landroid/os/Handler;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/framework/util/ContactObserver;

.field final synthetic val$handler:Landroid/os/Handler;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/util/ContactObserver;Landroid/os/Handler;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/framework/util/ContactObserver;

    .prologue
    .line 53
    iput-object p1, p0, Lcom/navdy/client/app/framework/util/ContactObserver$2;->this$0:Lcom/navdy/client/app/framework/util/ContactObserver;

    iput-object p2, p0, Lcom/navdy/client/app/framework/util/ContactObserver$2;->val$handler:Landroid/os/Handler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 56
    invoke-static {}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->weHaveContactsPermission()Z

    move-result v0

    .line 58
    .local v0, "hasGainedContactsPermission":Z
    if-eqz v0, :cond_0

    .line 59
    invoke-static {}, Lcom/navdy/client/app/framework/util/ContactObserver;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "has gained contacts permission"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 60
    iget-object v1, p0, Lcom/navdy/client/app/framework/util/ContactObserver$2;->val$handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/navdy/client/app/framework/util/ContactObserver$2;->this$0:Lcom/navdy/client/app/framework/util/ContactObserver;

    invoke-static {v2}, Lcom/navdy/client/app/framework/util/ContactObserver;->access$200(Lcom/navdy/client/app/framework/util/ContactObserver;)Ljava/lang/Runnable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 64
    :goto_0
    return-void

    .line 62
    :cond_0
    iget-object v1, p0, Lcom/navdy/client/app/framework/util/ContactObserver$2;->val$handler:Landroid/os/Handler;

    invoke-static {}, Lcom/navdy/client/app/framework/util/ContactObserver;->access$300()J

    move-result-wide v2

    invoke-virtual {v1, p0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method
