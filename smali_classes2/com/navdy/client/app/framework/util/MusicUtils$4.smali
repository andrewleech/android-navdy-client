.class final Lcom/navdy/client/app/framework/util/MusicUtils$4;
.super Ljava/lang/Object;
.source "MusicUtils.java"

# interfaces
.implements Lcom/navdy/client/app/framework/music/LocalMusicPlayer$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/framework/util/MusicUtils;->handleLocalPlayEvent(Lcom/navdy/service/library/events/audio/MusicEvent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field private albumArt:Landroid/graphics/Bitmap;

.field private musicTrackInfo:Lcom/navdy/service/library/events/audio/MusicTrackInfo;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 242
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 243
    sget-object v0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->EMPTY_TRACK_INFO:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    iput-object v0, p0, Lcom/navdy/client/app/framework/util/MusicUtils$4;->musicTrackInfo:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    .line 245
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/client/app/framework/util/MusicUtils$4;->albumArt:Landroid/graphics/Bitmap;

    return-void
.end method

.method static synthetic access$100(Lcom/navdy/client/app/framework/util/MusicUtils$4;Lcom/navdy/service/library/events/audio/MusicTrackInfo;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/util/MusicUtils$4;
    .param p1, "x1"    # Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    .prologue
    .line 242
    invoke-direct {p0, p1}, Lcom/navdy/client/app/framework/util/MusicUtils$4;->setMusicTrackInfo(Lcom/navdy/service/library/events/audio/MusicTrackInfo;)V

    return-void
.end method

.method static synthetic access$200(Lcom/navdy/client/app/framework/util/MusicUtils$4;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/util/MusicUtils$4;

    .prologue
    .line 242
    invoke-direct {p0}, Lcom/navdy/client/app/framework/util/MusicUtils$4;->sendArtwork()V

    return-void
.end method

.method static synthetic access$302(Lcom/navdy/client/app/framework/util/MusicUtils$4;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/util/MusicUtils$4;
    .param p1, "x1"    # Landroid/graphics/Bitmap;

    .prologue
    .line 242
    iput-object p1, p0, Lcom/navdy/client/app/framework/util/MusicUtils$4;->albumArt:Landroid/graphics/Bitmap;

    return-object p1
.end method

.method private sendArtwork()V
    .locals 4

    .prologue
    .line 312
    sget-object v1, Lcom/navdy/client/app/framework/util/MusicUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sendArtwork "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/client/app/framework/util/MusicUtils$4;->musicTrackInfo:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 313
    invoke-static {}, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->getInstance()Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;

    move-result-object v0

    .line 314
    .local v0, "musicServiceHandler":Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;
    iget-object v1, p0, Lcom/navdy/client/app/framework/util/MusicUtils$4;->musicTrackInfo:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    iget-object v2, p0, Lcom/navdy/client/app/framework/util/MusicUtils$4;->albumArt:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1, v2}, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->checkAndSetCurrentMediaArtwork(Lcom/navdy/service/library/events/audio/MusicTrackInfo;Landroid/graphics/Bitmap;)V

    .line 315
    return-void
.end method

.method private setMusicTrackInfo(Lcom/navdy/service/library/events/audio/MusicTrackInfo;)V
    .locals 4
    .param p1, "trackInfo"    # Lcom/navdy/service/library/events/audio/MusicTrackInfo;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 305
    sget-object v1, Lcom/navdy/client/app/framework/util/MusicUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setMusicTrackInfo "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 306
    iput-object p1, p0, Lcom/navdy/client/app/framework/util/MusicUtils$4;->musicTrackInfo:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    .line 307
    invoke-static {}, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->getInstance()Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;

    move-result-object v0

    .line 308
    .local v0, "musicServiceHandler":Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;
    iget-object v1, p0, Lcom/navdy/client/app/framework/util/MusicUtils$4;->musicTrackInfo:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->setCurrentMediaTrackInfo(Lcom/navdy/service/library/events/audio/MusicTrackInfo;)V

    .line 309
    return-void
.end method


# virtual methods
.method public onMetadataUpdate(Lcom/navdy/service/library/events/audio/MusicTrackInfo;)V
    .locals 3
    .param p1, "trackInfo"    # Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    .prologue
    .line 249
    sget-object v0, Lcom/navdy/client/app/framework/util/MusicUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onMetadataUpdate "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 250
    invoke-direct {p0, p1}, Lcom/navdy/client/app/framework/util/MusicUtils$4;->setMusicTrackInfo(Lcom/navdy/service/library/events/audio/MusicTrackInfo;)V

    .line 251
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/client/app/framework/util/MusicUtils$4;->albumArt:Landroid/graphics/Bitmap;

    .line 253
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/client/app/framework/util/MusicUtils$4$1;

    invoke-direct {v1, p0, p1}, Lcom/navdy/client/app/framework/util/MusicUtils$4$1;-><init>(Lcom/navdy/client/app/framework/util/MusicUtils$4;Lcom/navdy/service/library/events/audio/MusicTrackInfo;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 284
    return-void
.end method

.method public onPlaybackStateUpdate(Lcom/navdy/service/library/events/audio/MusicPlaybackState;)V
    .locals 3
    .param p1, "musicPlaybackState"    # Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    .prologue
    .line 288
    sget-object v0, Lcom/navdy/client/app/framework/util/MusicUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onPlaybackStateUpdate "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 289
    new-instance v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    iget-object v1, p0, Lcom/navdy/client/app/framework/util/MusicUtils$4;->musicTrackInfo:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;-><init>(Lcom/navdy/service/library/events/audio/MusicTrackInfo;)V

    .line 290
    invoke-virtual {v0, p1}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->playbackState(Lcom/navdy/service/library/events/audio/MusicPlaybackState;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v0

    .line 291
    invoke-virtual {v0}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->build()Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    move-result-object v0

    .line 289
    invoke-direct {p0, v0}, Lcom/navdy/client/app/framework/util/MusicUtils$4;->setMusicTrackInfo(Lcom/navdy/service/library/events/audio/MusicTrackInfo;)V

    .line 292
    invoke-direct {p0}, Lcom/navdy/client/app/framework/util/MusicUtils$4;->sendArtwork()V

    .line 293
    return-void
.end method

.method public onPositionUpdate(II)V
    .locals 2
    .param p1, "position"    # I
    .param p2, "duration"    # I

    .prologue
    .line 297
    sget-object v0, Lcom/navdy/client/app/framework/util/MusicUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "onPositionUpdate"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 298
    new-instance v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    iget-object v1, p0, Lcom/navdy/client/app/framework/util/MusicUtils$4;->musicTrackInfo:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;-><init>(Lcom/navdy/service/library/events/audio/MusicTrackInfo;)V

    .line 299
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->currentPosition(Ljava/lang/Integer;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v0

    .line 300
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->duration(Ljava/lang/Integer;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v0

    .line 301
    invoke-virtual {v0}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->build()Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    move-result-object v0

    .line 298
    invoke-direct {p0, v0}, Lcom/navdy/client/app/framework/util/MusicUtils$4;->setMusicTrackInfo(Lcom/navdy/service/library/events/audio/MusicTrackInfo;)V

    .line 302
    return-void
.end method
