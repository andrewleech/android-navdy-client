.class public Lcom/navdy/client/app/framework/util/ThrottlingLogger;
.super Ljava/lang/Object;
.source "ThrottlingLogger.java"


# instance fields
.field private lastLogTimes:Landroid/util/SparseLongArray;

.field private final logger:Lcom/navdy/service/library/log/Logger;

.field private final wait:J


# direct methods
.method public constructor <init>(Lcom/navdy/service/library/log/Logger;J)V
    .locals 2
    .param p1, "logger"    # Lcom/navdy/service/library/log/Logger;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "wait"    # J

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    new-instance v0, Landroid/util/SparseLongArray;

    invoke-direct {v0}, Landroid/util/SparseLongArray;-><init>()V

    iput-object v0, p0, Lcom/navdy/client/app/framework/util/ThrottlingLogger;->lastLogTimes:Landroid/util/SparseLongArray;

    .line 21
    iput-object p1, p0, Lcom/navdy/client/app/framework/util/ThrottlingLogger;->logger:Lcom/navdy/service/library/log/Logger;

    .line 22
    iput-wide p2, p0, Lcom/navdy/client/app/framework/util/ThrottlingLogger;->wait:J

    .line 23
    return-void
.end method

.method private declared-synchronized checkTime(I)Z
    .locals 8
    .param p1, "tag"    # I

    .prologue
    .line 86
    monitor-enter p0

    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 87
    .local v0, "currentTime":J
    iget-object v3, p0, Lcom/navdy/client/app/framework/util/ThrottlingLogger;->lastLogTimes:Landroid/util/SparseLongArray;

    const-wide/16 v4, 0x0

    invoke-virtual {v3, p1, v4, v5}, Landroid/util/SparseLongArray;->get(IJ)J

    move-result-wide v4

    sub-long v4, v0, v4

    iget-wide v6, p0, Lcom/navdy/client/app/framework/util/ThrottlingLogger;->wait:J

    cmp-long v3, v4, v6

    if-lez v3, :cond_1

    const/4 v2, 0x1

    .line 88
    .local v2, "isReady":Z
    :goto_0
    if-eqz v2, :cond_0

    .line 89
    iget-object v3, p0, Lcom/navdy/client/app/framework/util/ThrottlingLogger;->lastLogTimes:Landroid/util/SparseLongArray;

    invoke-virtual {v3, p1, v0, v1}, Landroid/util/SparseLongArray;->put(IJ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 91
    :cond_0
    monitor-exit p0

    return v2

    .line 87
    .end local v2    # "isReady":Z
    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    .line 86
    .end local v0    # "currentTime":J
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method


# virtual methods
.method public d(ILjava/lang/String;)V
    .locals 1
    .param p1, "tag"    # I
    .param p2, "msg"    # Ljava/lang/String;

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/navdy/client/app/framework/util/ThrottlingLogger;->checkTime(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 39
    iget-object v0, p0, Lcom/navdy/client/app/framework/util/ThrottlingLogger;->logger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v0, p2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 41
    :cond_0
    return-void
.end method

.method public d(ILjava/lang/String;Ljava/lang/Throwable;)V
    .locals 1
    .param p1, "tag"    # I
    .param p2, "msg"    # Ljava/lang/String;
    .param p3, "tr"    # Ljava/lang/Throwable;

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/navdy/client/app/framework/util/ThrottlingLogger;->checkTime(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 45
    iget-object v0, p0, Lcom/navdy/client/app/framework/util/ThrottlingLogger;->logger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v0, p2, p3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 47
    :cond_0
    return-void
.end method

.method public e(ILjava/lang/String;)V
    .locals 1
    .param p1, "tag"    # I
    .param p2, "msg"    # Ljava/lang/String;

    .prologue
    .line 74
    invoke-direct {p0, p1}, Lcom/navdy/client/app/framework/util/ThrottlingLogger;->checkTime(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 75
    iget-object v0, p0, Lcom/navdy/client/app/framework/util/ThrottlingLogger;->logger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v0, p2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 77
    :cond_0
    return-void
.end method

.method public e(ILjava/lang/String;Ljava/lang/Throwable;)V
    .locals 1
    .param p1, "tag"    # I
    .param p2, "msg"    # Ljava/lang/String;
    .param p3, "tr"    # Ljava/lang/Throwable;

    .prologue
    .line 80
    invoke-direct {p0, p1}, Lcom/navdy/client/app/framework/util/ThrottlingLogger;->checkTime(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 81
    iget-object v0, p0, Lcom/navdy/client/app/framework/util/ThrottlingLogger;->logger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v0, p2, p3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 83
    :cond_0
    return-void
.end method

.method public i(ILjava/lang/String;)V
    .locals 1
    .param p1, "tag"    # I
    .param p2, "msg"    # Ljava/lang/String;

    .prologue
    .line 50
    invoke-direct {p0, p1}, Lcom/navdy/client/app/framework/util/ThrottlingLogger;->checkTime(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 51
    iget-object v0, p0, Lcom/navdy/client/app/framework/util/ThrottlingLogger;->logger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v0, p2}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 53
    :cond_0
    return-void
.end method

.method public i(ILjava/lang/String;Ljava/lang/Throwable;)V
    .locals 1
    .param p1, "tag"    # I
    .param p2, "msg"    # Ljava/lang/String;
    .param p3, "tr"    # Ljava/lang/Throwable;

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lcom/navdy/client/app/framework/util/ThrottlingLogger;->checkTime(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 57
    iget-object v0, p0, Lcom/navdy/client/app/framework/util/ThrottlingLogger;->logger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v0, p2, p3}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 59
    :cond_0
    return-void
.end method

.method public v(ILjava/lang/String;)V
    .locals 1
    .param p1, "tag"    # I
    .param p2, "msg"    # Ljava/lang/String;

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/navdy/client/app/framework/util/ThrottlingLogger;->checkTime(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 27
    iget-object v0, p0, Lcom/navdy/client/app/framework/util/ThrottlingLogger;->logger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v0, p2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 29
    :cond_0
    return-void
.end method

.method public v(ILjava/lang/String;Ljava/lang/Throwable;)V
    .locals 1
    .param p1, "tag"    # I
    .param p2, "msg"    # Ljava/lang/String;
    .param p3, "tr"    # Ljava/lang/Throwable;

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/navdy/client/app/framework/util/ThrottlingLogger;->checkTime(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 33
    iget-object v0, p0, Lcom/navdy/client/app/framework/util/ThrottlingLogger;->logger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v0, p2, p3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 35
    :cond_0
    return-void
.end method

.method public w(ILjava/lang/String;)V
    .locals 1
    .param p1, "tag"    # I
    .param p2, "msg"    # Ljava/lang/String;

    .prologue
    .line 62
    invoke-direct {p0, p1}, Lcom/navdy/client/app/framework/util/ThrottlingLogger;->checkTime(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 63
    iget-object v0, p0, Lcom/navdy/client/app/framework/util/ThrottlingLogger;->logger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v0, p2}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 65
    :cond_0
    return-void
.end method

.method public w(ILjava/lang/String;Ljava/lang/Throwable;)V
    .locals 1
    .param p1, "tag"    # I
    .param p2, "msg"    # Ljava/lang/String;
    .param p3, "tr"    # Ljava/lang/Throwable;

    .prologue
    .line 68
    invoke-direct {p0, p1}, Lcom/navdy/client/app/framework/util/ThrottlingLogger;->checkTime(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 69
    iget-object v0, p0, Lcom/navdy/client/app/framework/util/ThrottlingLogger;->logger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v0, p2, p3}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 71
    :cond_0
    return-void
.end method
