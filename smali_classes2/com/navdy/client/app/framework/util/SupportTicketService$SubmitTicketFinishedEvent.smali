.class public Lcom/navdy/client/app/framework/util/SupportTicketService$SubmitTicketFinishedEvent;
.super Ljava/lang/Object;
.source "SupportTicketService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/framework/util/SupportTicketService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SubmitTicketFinishedEvent"
.end annotation


# instance fields
.field private conditionsMet:Z

.field private ticketId:Ljava/lang/String;


# direct methods
.method constructor <init>(ZLjava/lang/String;)V
    .locals 1
    .param p1, "conditionsMet"    # Z
    .param p2, "ticketId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 124
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 122
    const-string v0, ""

    iput-object v0, p0, Lcom/navdy/client/app/framework/util/SupportTicketService$SubmitTicketFinishedEvent;->ticketId:Ljava/lang/String;

    .line 125
    iput-boolean p1, p0, Lcom/navdy/client/app/framework/util/SupportTicketService$SubmitTicketFinishedEvent;->conditionsMet:Z

    .line 126
    iput-object p2, p0, Lcom/navdy/client/app/framework/util/SupportTicketService$SubmitTicketFinishedEvent;->ticketId:Ljava/lang/String;

    .line 127
    return-void
.end method


# virtual methods
.method public getConditionsMet()Z
    .locals 1

    .prologue
    .line 130
    iget-boolean v0, p0, Lcom/navdy/client/app/framework/util/SupportTicketService$SubmitTicketFinishedEvent;->conditionsMet:Z

    return v0
.end method

.method public getTicketId()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 133
    iget-object v0, p0, Lcom/navdy/client/app/framework/util/SupportTicketService$SubmitTicketFinishedEvent;->ticketId:Ljava/lang/String;

    return-object v0
.end method
