.class Lcom/navdy/client/app/framework/util/CalendarObserver$1;
.super Ljava/lang/Object;
.source "CalendarObserver.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/framework/util/CalendarObserver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/framework/util/CalendarObserver;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/util/CalendarObserver;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/framework/util/CalendarObserver;

    .prologue
    .line 24
    iput-object p1, p0, Lcom/navdy/client/app/framework/util/CalendarObserver$1;->this$0:Lcom/navdy/client/app/framework/util/CalendarObserver;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 28
    invoke-static {}, Lcom/navdy/client/app/framework/util/CalendarObserver;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "should send calendar events in runnable: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/client/app/framework/util/CalendarObserver$1;->this$0:Lcom/navdy/client/app/framework/util/CalendarObserver;

    invoke-static {v2}, Lcom/navdy/client/app/framework/util/CalendarObserver;->access$000(Lcom/navdy/client/app/framework/util/CalendarObserver;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 29
    iget-object v0, p0, Lcom/navdy/client/app/framework/util/CalendarObserver$1;->this$0:Lcom/navdy/client/app/framework/util/CalendarObserver;

    invoke-static {v0}, Lcom/navdy/client/app/framework/util/CalendarObserver;->access$000(Lcom/navdy/client/app/framework/util/CalendarObserver;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 30
    iget-object v0, p0, Lcom/navdy/client/app/framework/util/CalendarObserver$1;->this$0:Lcom/navdy/client/app/framework/util/CalendarObserver;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lcom/navdy/client/app/framework/util/CalendarObserver;->access$202(Lcom/navdy/client/app/framework/util/CalendarObserver;J)J

    .line 31
    invoke-static {}, Lcom/navdy/client/app/ui/homescreen/CalendarUtils;->sendCalendarsToHud()V

    .line 32
    iget-object v0, p0, Lcom/navdy/client/app/framework/util/CalendarObserver$1;->this$0:Lcom/navdy/client/app/framework/util/CalendarObserver;

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/util/CalendarObserver;->postSendCalendarRunnable()V

    .line 34
    :cond_0
    return-void
.end method
