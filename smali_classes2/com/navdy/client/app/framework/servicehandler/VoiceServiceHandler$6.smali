.class Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$6;
.super Ljava/lang/Object;
.source "VoiceServiceHandler.java"

# interfaces
.implements Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$OnCompleteCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->sendDestinationsToHud(Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;

.field final synthetic val$destinations:Ljava/util/ArrayList;

.field final synthetic val$prefix:Ljava/lang/String;

.field final synthetic val$query:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;

    .prologue
    .line 947
    iput-object p1, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$6;->this$0:Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;

    iput-object p2, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$6;->val$destinations:Ljava/util/ArrayList;

    iput-object p3, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$6;->val$query:Ljava/lang/String;

    iput-object p4, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$6;->val$prefix:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private replaceAndSend(Lcom/navdy/client/app/framework/models/Destination;)V
    .locals 4
    .param p1, "destination"    # Lcom/navdy/client/app/framework/models/Destination;

    .prologue
    .line 959
    iget-object v0, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$6;->val$destinations:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 960
    iget-object v0, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$6;->this$0:Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;

    iget-object v1, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$6;->val$destinations:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$6;->val$query:Ljava/lang/String;

    iget-object v3, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$6;->val$prefix:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3}, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->access$1700(Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)V

    .line 961
    return-void
.end method


# virtual methods
.method public onFailure(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;)V
    .locals 0
    .param p1, "destination"    # Lcom/navdy/client/app/framework/models/Destination;
    .param p2, "error"    # Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;

    .prologue
    .line 955
    invoke-direct {p0, p1}, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$6;->replaceAndSend(Lcom/navdy/client/app/framework/models/Destination;)V

    .line 956
    return-void
.end method

.method public onSuccess(Lcom/navdy/client/app/framework/models/Destination;)V
    .locals 0
    .param p1, "destination"    # Lcom/navdy/client/app/framework/models/Destination;

    .prologue
    .line 950
    invoke-direct {p0, p1}, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$6;->replaceAndSend(Lcom/navdy/client/app/framework/models/Destination;)V

    .line 951
    return-void
.end method
