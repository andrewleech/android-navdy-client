.class Lcom/navdy/client/app/framework/servicehandler/PlaceTypeSearchServiceHandler$PlaceTypeSearchListener;
.super Ljava/lang/Object;
.source "PlaceTypeSearchServiceHandler.java"

# interfaces
.implements Lcom/navdy/client/app/framework/search/GooglePlacesSearch$GoogleSearchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/framework/servicehandler/PlaceTypeSearchServiceHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "PlaceTypeSearchListener"
.end annotation


# static fields
.field private static final MAX_RESULTS:I = 0x7

.field private static final MIN_ACCEPTABLE_RESULTS:I = 0x5

.field private static final RETRY_RADIUS:I = 0x3edd


# instance fields
.field private final handler:Landroid/os/Handler;

.field private hasRetried:Z

.field private final requestId:Ljava/lang/String;

.field private final serviceType:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;


# direct methods
.method private constructor <init>(Ljava/lang/String;Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;)V
    .locals 2
    .param p1, "requestId"    # Ljava/lang/String;
    .param p2, "serviceType"    # Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;

    .prologue
    .line 178
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 171
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/navdy/client/app/framework/servicehandler/PlaceTypeSearchServiceHandler$PlaceTypeSearchListener;->handler:Landroid/os/Handler;

    .line 179
    iput-object p1, p0, Lcom/navdy/client/app/framework/servicehandler/PlaceTypeSearchServiceHandler$PlaceTypeSearchListener;->requestId:Ljava/lang/String;

    .line 180
    iput-object p2, p0, Lcom/navdy/client/app/framework/servicehandler/PlaceTypeSearchServiceHandler$PlaceTypeSearchListener;->serviceType:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;

    .line 182
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/client/app/framework/servicehandler/PlaceTypeSearchServiceHandler$PlaceTypeSearchListener;->hasRetried:Z

    .line 183
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;Lcom/navdy/client/app/framework/servicehandler/PlaceTypeSearchServiceHandler$1;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/String;
    .param p2, "x1"    # Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;
    .param p3, "x2"    # Lcom/navdy/client/app/framework/servicehandler/PlaceTypeSearchServiceHandler$1;

    .prologue
    .line 164
    invoke-direct {p0, p1, p2}, Lcom/navdy/client/app/framework/servicehandler/PlaceTypeSearchServiceHandler$PlaceTypeSearchListener;-><init>(Ljava/lang/String;Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;)V

    return-void
.end method

.method private getPlaceTypeForGoogleServiceType(Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;)Lcom/navdy/service/library/events/places/PlaceType;
    .locals 2
    .param p1, "serviceType"    # Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;

    .prologue
    .line 260
    sget-object v0, Lcom/navdy/client/app/framework/servicehandler/PlaceTypeSearchServiceHandler$2;->$SwitchMap$com$navdy$client$app$framework$search$GooglePlacesSearch$ServiceTypes:[I

    invoke-virtual {p1}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 276
    sget-object v0, Lcom/navdy/service/library/events/places/PlaceType;->PLACE_TYPE_UNKNOWN:Lcom/navdy/service/library/events/places/PlaceType;

    :goto_0
    return-object v0

    .line 262
    :pswitch_0
    sget-object v0, Lcom/navdy/service/library/events/places/PlaceType;->PLACE_TYPE_GAS:Lcom/navdy/service/library/events/places/PlaceType;

    goto :goto_0

    .line 264
    :pswitch_1
    sget-object v0, Lcom/navdy/service/library/events/places/PlaceType;->PLACE_TYPE_PARKING:Lcom/navdy/service/library/events/places/PlaceType;

    goto :goto_0

    .line 266
    :pswitch_2
    sget-object v0, Lcom/navdy/service/library/events/places/PlaceType;->PLACE_TYPE_RESTAURANT:Lcom/navdy/service/library/events/places/PlaceType;

    goto :goto_0

    .line 268
    :pswitch_3
    sget-object v0, Lcom/navdy/service/library/events/places/PlaceType;->PLACE_TYPE_COFFEE:Lcom/navdy/service/library/events/places/PlaceType;

    goto :goto_0

    .line 270
    :pswitch_4
    sget-object v0, Lcom/navdy/service/library/events/places/PlaceType;->PLACE_TYPE_ATM:Lcom/navdy/service/library/events/places/PlaceType;

    goto :goto_0

    .line 272
    :pswitch_5
    sget-object v0, Lcom/navdy/service/library/events/places/PlaceType;->PLACE_TYPE_HOSPITAL:Lcom/navdy/service/library/events/places/PlaceType;

    goto :goto_0

    .line 274
    :pswitch_6
    sget-object v0, Lcom/navdy/service/library/events/places/PlaceType;->PLACE_TYPE_STORE:Lcom/navdy/service/library/events/places/PlaceType;

    goto :goto_0

    .line 260
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method private postFailure()V
    .locals 2

    .prologue
    .line 243
    new-instance v0, Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse$Builder;

    invoke-direct {v0}, Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse$Builder;-><init>()V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 244
    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse$Builder;->destinations(Ljava/util/List;)Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/client/app/framework/servicehandler/PlaceTypeSearchServiceHandler$PlaceTypeSearchListener;->requestId:Ljava/lang/String;

    .line 245
    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse$Builder;->request_id(Ljava/lang/String;)Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse$Builder;

    move-result-object v0

    sget-object v1, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_NOT_AVAILABLE:Lcom/navdy/service/library/events/RequestStatus;

    .line 246
    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse$Builder;->request_status(Lcom/navdy/service/library/events/RequestStatus;)Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse$Builder;

    move-result-object v0

    .line 247
    invoke-virtual {v0}, Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse$Builder;->build()Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse;

    move-result-object v0

    .line 243
    invoke-static {v0}, Lcom/navdy/client/app/framework/DeviceConnection;->postEvent(Lcom/squareup/wire/Message;)Z

    .line 249
    return-void
.end method

.method private retryWithBiggerRadius()V
    .locals 3

    .prologue
    .line 252
    invoke-static {}, Lcom/navdy/client/app/framework/servicehandler/PlaceTypeSearchServiceHandler;->access$400()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "retrying with a 10 mile radius since results count is less than 5"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 253
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/navdy/client/app/framework/servicehandler/PlaceTypeSearchServiceHandler$PlaceTypeSearchListener;->hasRetried:Z

    .line 255
    new-instance v0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;

    iget-object v1, p0, Lcom/navdy/client/app/framework/servicehandler/PlaceTypeSearchServiceHandler$PlaceTypeSearchListener;->handler:Landroid/os/Handler;

    invoke-direct {v0, p0, v1}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;-><init>(Lcom/navdy/client/app/framework/search/GooglePlacesSearch$GoogleSearchListener;Landroid/os/Handler;)V

    .line 256
    .local v0, "googlePlacesSearch":Lcom/navdy/client/app/framework/search/GooglePlacesSearch;
    iget-object v1, p0, Lcom/navdy/client/app/framework/servicehandler/PlaceTypeSearchServiceHandler$PlaceTypeSearchListener;->serviceType:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;

    const/16 v2, 0x3edd

    invoke-virtual {v0, v1, v2}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->runServiceSearchWebApi(Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;I)V

    .line 257
    return-void
.end method


# virtual methods
.method public onGoogleSearchResult(Ljava/util/List;Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Error;)V
    .locals 12
    .param p2, "queryType"    # Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;
    .param p3, "error"    # Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Error;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;",
            ">;",
            "Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;",
            "Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Error;",
            ")V"
        }
    .end annotation

    .prologue
    .line 187
    .local p1, "destinations":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;>;"
    sget-object v6, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Error;->SERVICE_ERROR:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Error;

    if-ne p3, v6, :cond_0

    .line 188
    invoke-direct {p0}, Lcom/navdy/client/app/framework/servicehandler/PlaceTypeSearchServiceHandler$PlaceTypeSearchListener;->postFailure()V

    .line 235
    :goto_0
    return-void

    .line 192
    :cond_0
    if-eqz p1, :cond_1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v6

    const/4 v7, 0x5

    if-ge v6, v7, :cond_2

    :cond_1
    iget-boolean v6, p0, Lcom/navdy/client/app/framework/servicehandler/PlaceTypeSearchServiceHandler$PlaceTypeSearchListener;->hasRetried:Z

    if-nez v6, :cond_2

    .line 193
    invoke-direct {p0}, Lcom/navdy/client/app/framework/servicehandler/PlaceTypeSearchServiceHandler$PlaceTypeSearchListener;->retryWithBiggerRadius()V

    goto :goto_0

    .line 197
    :cond_2
    if-eqz p1, :cond_3

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v6

    if-nez v6, :cond_4

    .line 198
    :cond_3
    invoke-direct {p0}, Lcom/navdy/client/app/framework/servicehandler/PlaceTypeSearchServiceHandler$PlaceTypeSearchListener;->postFailure()V

    goto :goto_0

    .line 202
    :cond_4
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 203
    .local v3, "outboundList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/destination/Destination;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 205
    .local v1, "destinationsIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;>;"
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_6

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v6

    const/4 v7, 0x7

    if-ge v6, v7, :cond_6

    .line 206
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;

    .line 208
    .local v0, "destination":Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;
    iget-object v6, v0, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->streetNumber:Ljava/lang/String;

    if-eqz v6, :cond_5

    iget-object v6, v0, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->streetName:Ljava/lang/String;

    if-eqz v6, :cond_5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, v0, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->streetNumber:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v0, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->streetName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 212
    .local v5, "subtitle":Ljava/lang/String;
    :goto_2
    new-instance v6, Lcom/navdy/service/library/events/destination/Destination$Builder;

    invoke-direct {v6}, Lcom/navdy/service/library/events/destination/Destination$Builder;-><init>()V

    new-instance v7, Lcom/navdy/service/library/events/location/LatLong;

    const-wide/16 v8, 0x0

    .line 213
    invoke-static {v8, v9}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v8

    const-wide/16 v10, 0x0

    invoke-static {v10, v11}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v9

    invoke-direct {v7, v8, v9}, Lcom/navdy/service/library/events/location/LatLong;-><init>(Ljava/lang/Double;Ljava/lang/Double;)V

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/events/destination/Destination$Builder;->navigation_position(Lcom/navdy/service/library/events/location/LatLong;)Lcom/navdy/service/library/events/destination/Destination$Builder;

    move-result-object v6

    new-instance v7, Lcom/navdy/service/library/events/location/LatLong;

    iget-object v8, v0, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->lat:Ljava/lang/String;

    .line 214
    invoke-static {v8}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v8

    iget-object v9, v0, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->lng:Ljava/lang/String;

    invoke-static {v9}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v9

    invoke-direct {v7, v8, v9}, Lcom/navdy/service/library/events/location/LatLong;-><init>(Ljava/lang/Double;Ljava/lang/Double;)V

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/events/destination/Destination$Builder;->display_position(Lcom/navdy/service/library/events/location/LatLong;)Lcom/navdy/service/library/events/destination/Destination$Builder;

    move-result-object v6

    iget-object v7, v0, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->name:Ljava/lang/String;

    .line 215
    invoke-virtual {v6, v7}, Lcom/navdy/service/library/events/destination/Destination$Builder;->destination_title(Ljava/lang/String;)Lcom/navdy/service/library/events/destination/Destination$Builder;

    move-result-object v6

    .line 216
    invoke-virtual {v6, v5}, Lcom/navdy/service/library/events/destination/Destination$Builder;->destination_subtitle(Ljava/lang/String;)Lcom/navdy/service/library/events/destination/Destination$Builder;

    move-result-object v6

    sget-object v7, Lcom/navdy/service/library/events/destination/Destination$FavoriteType;->FAVORITE_NONE:Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

    .line 217
    invoke-virtual {v6, v7}, Lcom/navdy/service/library/events/destination/Destination$Builder;->favorite_type(Lcom/navdy/service/library/events/destination/Destination$FavoriteType;)Lcom/navdy/service/library/events/destination/Destination$Builder;

    move-result-object v6

    iget-object v7, v0, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->address:Ljava/lang/String;

    .line 218
    invoke-virtual {v6, v7}, Lcom/navdy/service/library/events/destination/Destination$Builder;->full_address(Ljava/lang/String;)Lcom/navdy/service/library/events/destination/Destination$Builder;

    move-result-object v6

    .line 219
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/events/destination/Destination$Builder;->identifier(Ljava/lang/String;)Lcom/navdy/service/library/events/destination/Destination$Builder;

    move-result-object v6

    iget-object v7, p0, Lcom/navdy/client/app/framework/servicehandler/PlaceTypeSearchServiceHandler$PlaceTypeSearchListener;->serviceType:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;

    .line 220
    invoke-direct {p0, v7}, Lcom/navdy/client/app/framework/servicehandler/PlaceTypeSearchServiceHandler$PlaceTypeSearchListener;->getPlaceTypeForGoogleServiceType(Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;)Lcom/navdy/service/library/events/places/PlaceType;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/events/destination/Destination$Builder;->place_type(Lcom/navdy/service/library/events/places/PlaceType;)Lcom/navdy/service/library/events/destination/Destination$Builder;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "google-places-id:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, v0, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->place_id:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 221
    invoke-virtual {v6, v7}, Lcom/navdy/service/library/events/destination/Destination$Builder;->place_id(Ljava/lang/String;)Lcom/navdy/service/library/events/destination/Destination$Builder;

    move-result-object v6

    .line 222
    invoke-virtual {v6}, Lcom/navdy/service/library/events/destination/Destination$Builder;->build()Lcom/navdy/service/library/events/destination/Destination;

    move-result-object v2

    .line 224
    .local v2, "outboundDestination":Lcom/navdy/service/library/events/destination/Destination;
    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 208
    .end local v2    # "outboundDestination":Lcom/navdy/service/library/events/destination/Destination;
    .end local v5    # "subtitle":Ljava/lang/String;
    :cond_5
    iget-object v5, v0, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->address:Ljava/lang/String;

    goto/16 :goto_2

    .line 227
    .end local v0    # "destination":Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;
    :cond_6
    new-instance v6, Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse$Builder;

    invoke-direct {v6}, Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse$Builder;-><init>()V

    iget-object v7, p0, Lcom/navdy/client/app/framework/servicehandler/PlaceTypeSearchServiceHandler$PlaceTypeSearchListener;->requestId:Ljava/lang/String;

    .line 228
    invoke-virtual {v6, v7}, Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse$Builder;->request_id(Ljava/lang/String;)Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse$Builder;

    move-result-object v6

    sget-object v7, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_SUCCESS:Lcom/navdy/service/library/events/RequestStatus;

    .line 229
    invoke-virtual {v6, v7}, Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse$Builder;->request_status(Lcom/navdy/service/library/events/RequestStatus;)Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse$Builder;

    move-result-object v6

    .line 230
    invoke-virtual {v6, v3}, Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse$Builder;->destinations(Ljava/util/List;)Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse$Builder;

    move-result-object v6

    .line 231
    invoke-virtual {v6}, Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse$Builder;->build()Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse;

    move-result-object v4

    .line 233
    .local v4, "placeTypeSearchResponse":Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse;
    invoke-static {v4}, Lcom/navdy/client/app/framework/servicehandler/PlaceTypeSearchServiceHandler;->access$300(Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse;)V

    .line 234
    invoke-static {v4}, Lcom/navdy/client/app/framework/DeviceConnection;->postEvent(Lcom/squareup/wire/Message;)Z

    goto/16 :goto_0
.end method

.method runSearch()V
    .locals 2

    .prologue
    .line 238
    new-instance v0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;

    iget-object v1, p0, Lcom/navdy/client/app/framework/servicehandler/PlaceTypeSearchServiceHandler$PlaceTypeSearchListener;->handler:Landroid/os/Handler;

    invoke-direct {v0, p0, v1}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;-><init>(Lcom/navdy/client/app/framework/search/GooglePlacesSearch$GoogleSearchListener;Landroid/os/Handler;)V

    .line 239
    .local v0, "googlePlacesSearch":Lcom/navdy/client/app/framework/search/GooglePlacesSearch;
    iget-object v1, p0, Lcom/navdy/client/app/framework/servicehandler/PlaceTypeSearchServiceHandler$PlaceTypeSearchListener;->serviceType:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/search/NavdySearch;->runServiceSearch(Lcom/navdy/client/app/framework/search/GooglePlacesSearch;Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;)V

    .line 240
    return-void
.end method
