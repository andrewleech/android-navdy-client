.class Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$1$1;
.super Ljava/lang/Object;
.source "VoiceServiceHandler.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$1;->onAudioInputReady(ZZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$1;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$1;)V
    .locals 0
    .param p1, "this$1"    # Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$1;

    .prologue
    .line 302
    iput-object p1, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$1$1;->this$1:Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 306
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 307
    .local v0, "context":Landroid/content/Context;
    iget-object v2, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$1$1;->this$1:Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$1;

    iget-object v2, v2, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$1;->this$0:Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;

    invoke-static {v0}, Landroid/speech/SpeechRecognizer;->createSpeechRecognizer(Landroid/content/Context;)Landroid/speech/SpeechRecognizer;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->access$302(Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;Landroid/speech/SpeechRecognizer;)Landroid/speech/SpeechRecognizer;

    .line 308
    iget-object v2, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$1$1;->this$1:Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$1;

    iget-object v2, v2, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$1;->this$0:Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;

    invoke-static {v2}, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->access$300(Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;)Landroid/speech/SpeechRecognizer;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$1$1;->this$1:Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$1;

    iget-object v3, v3, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$1;->this$0:Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;

    invoke-static {v3}, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->access$400(Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;)Landroid/speech/RecognitionListener;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/speech/SpeechRecognizer;->setRecognitionListener(Landroid/speech/RecognitionListener;)V

    .line 310
    :try_start_0
    iget-object v2, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$1$1;->this$1:Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$1;

    iget-object v2, v2, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$1;->this$0:Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;

    invoke-static {v2}, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->access$300(Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;)Landroid/speech/SpeechRecognizer;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$1$1;->this$1:Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$1;

    iget-object v3, v3, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$1;->this$0:Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;

    invoke-static {v3}, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->access$500(Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/speech/SpeechRecognizer;->startListening(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 315
    :goto_0
    return-void

    .line 311
    :catch_0
    move-exception v1

    .line 312
    .local v1, "e":Ljava/lang/SecurityException;
    sget-object v2, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Unable to start the listening intent due to SecurityException."

    invoke-virtual {v2, v3, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 313
    iget-object v2, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$1$1;->this$1:Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$1;

    iget-object v2, v2, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$1;->this$0:Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;

    invoke-static {v2}, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->access$400(Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;)Landroid/speech/RecognitionListener;

    move-result-object v2

    const/16 v3, 0x9

    invoke-interface {v2, v3}, Landroid/speech/RecognitionListener;->onError(I)V

    goto :goto_0
.end method
