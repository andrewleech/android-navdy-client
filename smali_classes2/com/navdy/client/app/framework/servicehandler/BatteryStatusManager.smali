.class public Lcom/navdy/client/app/framework/servicehandler/BatteryStatusManager;
.super Ljava/lang/Object;
.source "BatteryStatusManager.java"


# static fields
.field private static final LEVEL_EXTREMELY_LOW:I = 0xa

.field private static final LEVEL_LOW:I = 0x14

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private lastCharging:Z

.field private lastLevel:I

.field private lastStatus:Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;

.field private receiver:Landroid/content/BroadcastReceiver;

.field private registered:Z

.field private remoteDevice:Lcom/navdy/service/library/device/RemoteDevice;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 22
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/framework/servicehandler/BatteryStatusManager;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/app/framework/servicehandler/BatteryStatusManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/device/RemoteDevice;)V
    .locals 1
    .param p1, "remoteDevice"    # Lcom/navdy/service/library/device/RemoteDevice;

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    const/4 v0, -0x1

    iput v0, p0, Lcom/navdy/client/app/framework/servicehandler/BatteryStatusManager;->lastLevel:I

    .line 39
    new-instance v0, Lcom/navdy/client/app/framework/servicehandler/BatteryStatusManager$1;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/framework/servicehandler/BatteryStatusManager$1;-><init>(Lcom/navdy/client/app/framework/servicehandler/BatteryStatusManager;)V

    iput-object v0, p0, Lcom/navdy/client/app/framework/servicehandler/BatteryStatusManager;->receiver:Landroid/content/BroadcastReceiver;

    .line 36
    iput-object p1, p0, Lcom/navdy/client/app/framework/servicehandler/BatteryStatusManager;->remoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    .line 37
    return-void
.end method

.method static synthetic access$000()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/navdy/client/app/framework/servicehandler/BatteryStatusManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$100(Lcom/navdy/client/app/framework/servicehandler/BatteryStatusManager;IZ)Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/framework/servicehandler/BatteryStatusManager;
    .param p1, "x1"    # I
    .param p2, "x2"    # Z

    .prologue
    .line 19
    invoke-direct {p0, p1, p2}, Lcom/navdy/client/app/framework/servicehandler/BatteryStatusManager;->getPhoneBatteryStatus(IZ)Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/client/app/framework/servicehandler/BatteryStatusManager;Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/servicehandler/BatteryStatusManager;
    .param p1, "x1"    # Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus;

    .prologue
    .line 19
    invoke-direct {p0, p1}, Lcom/navdy/client/app/framework/servicehandler/BatteryStatusManager;->sendEvent(Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus;)V

    return-void
.end method

.method private getPhoneBatteryStatus(IZ)Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus;
    .locals 5
    .param p1, "newLevel"    # I
    .param p2, "charging"    # Z

    .prologue
    const/4 v1, 0x0

    const/16 v3, 0x14

    .line 93
    if-le p1, v3, :cond_1

    .line 94
    sget-object v0, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;->BATTERY_OK:Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;

    .line 104
    .local v0, "status":Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;
    :goto_0
    iget v2, p0, Lcom/navdy/client/app/framework/servicehandler/BatteryStatusManager;->lastLevel:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_3

    iget-object v2, p0, Lcom/navdy/client/app/framework/servicehandler/BatteryStatusManager;->lastStatus:Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;

    if-ne v2, v0, :cond_3

    iget-boolean v2, p0, Lcom/navdy/client/app/framework/servicehandler/BatteryStatusManager;->lastCharging:Z

    if-ne v2, p2, :cond_3

    .line 113
    .end local v0    # "status":Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;
    :cond_0
    :goto_1
    return-object v1

    .line 95
    :cond_1
    const/16 v2, 0xa

    if-le p1, v2, :cond_2

    if-gt p1, v3, :cond_2

    .line 96
    sget-object v0, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;->BATTERY_LOW:Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;

    .line 97
    .restart local v0    # "status":Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;
    const-string v2, "Battery_Low"

    invoke-static {v2}, Lcom/navdy/client/app/tracking/Tracker;->tagEvent(Ljava/lang/String;)V

    goto :goto_0

    .line 98
    .end local v0    # "status":Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;
    :cond_2
    if-lez p1, :cond_0

    if-gt p1, v3, :cond_0

    .line 99
    sget-object v0, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;->BATTERY_EXTREMELY_LOW:Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;

    .restart local v0    # "status":Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;
    goto :goto_0

    .line 108
    :cond_3
    iput-boolean p2, p0, Lcom/navdy/client/app/framework/servicehandler/BatteryStatusManager;->lastCharging:Z

    .line 109
    iput p1, p0, Lcom/navdy/client/app/framework/servicehandler/BatteryStatusManager;->lastLevel:I

    .line 110
    iput-object v0, p0, Lcom/navdy/client/app/framework/servicehandler/BatteryStatusManager;->lastStatus:Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;

    .line 112
    sget-object v1, Lcom/navdy/client/app/framework/servicehandler/BatteryStatusManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[battery] status["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/client/app/framework/servicehandler/BatteryStatusManager;->lastStatus:Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] level ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/navdy/client/app/framework/servicehandler/BatteryStatusManager;->lastLevel:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] charging["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 113
    new-instance v1, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus;

    iget-object v2, p0, Lcom/navdy/client/app/framework/servicehandler/BatteryStatusManager;->lastStatus:Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;

    iget v3, p0, Lcom/navdy/client/app/framework/servicehandler/BatteryStatusManager;->lastLevel:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iget-boolean v4, p0, Lcom/navdy/client/app/framework/servicehandler/BatteryStatusManager;->lastCharging:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus;-><init>(Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;Ljava/lang/Integer;Ljava/lang/Boolean;)V

    goto :goto_1
.end method

.method private sendEvent(Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus;)V
    .locals 3
    .param p1, "status"    # Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus;

    .prologue
    .line 84
    :try_start_0
    iget-object v1, p0, Lcom/navdy/client/app/framework/servicehandler/BatteryStatusManager;->remoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    invoke-virtual {v1, p1}, Lcom/navdy/service/library/device/RemoteDevice;->postEvent(Lcom/squareup/wire/Message;)Z

    .line 85
    sget-object v1, Lcom/navdy/client/app/framework/servicehandler/BatteryStatusManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "[battery] status sent"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 89
    :goto_0
    return-void

    .line 86
    :catch_0
    move-exception v0

    .line 87
    .local v0, "t":Ljava/lang/Throwable;
    sget-object v1, Lcom/navdy/client/app/framework/servicehandler/BatteryStatusManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "[battery]"

    invoke-virtual {v1, v2, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized register()V
    .locals 3

    .prologue
    .line 62
    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Lcom/navdy/client/app/framework/servicehandler/BatteryStatusManager;->registered:Z

    if-nez v1, :cond_0

    .line 63
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.BATTERY_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 64
    .local v0, "filter":Landroid/content/IntentFilter;
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/client/app/framework/servicehandler/BatteryStatusManager;->receiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 65
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/navdy/client/app/framework/servicehandler/BatteryStatusManager;->registered:Z

    .line 67
    .end local v0    # "filter":Landroid/content/IntentFilter;
    :cond_0
    const/4 v1, -0x1

    iput v1, p0, Lcom/navdy/client/app/framework/servicehandler/BatteryStatusManager;->lastLevel:I

    .line 68
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/navdy/client/app/framework/servicehandler/BatteryStatusManager;->lastCharging:Z

    .line 69
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/navdy/client/app/framework/servicehandler/BatteryStatusManager;->lastStatus:Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 70
    monitor-exit p0

    return-void

    .line 62
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized unregister()V
    .locals 2

    .prologue
    .line 73
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/navdy/client/app/framework/servicehandler/BatteryStatusManager;->registered:Z

    if-eqz v0, :cond_0

    .line 74
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/client/app/framework/servicehandler/BatteryStatusManager;->receiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 75
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/client/app/framework/servicehandler/BatteryStatusManager;->registered:Z

    .line 77
    :cond_0
    const/4 v0, -0x1

    iput v0, p0, Lcom/navdy/client/app/framework/servicehandler/BatteryStatusManager;->lastLevel:I

    .line 78
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/client/app/framework/servicehandler/BatteryStatusManager;->lastCharging:Z

    .line 79
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/client/app/framework/servicehandler/BatteryStatusManager;->lastStatus:Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 80
    monitor-exit p0

    return-void

    .line 73
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
