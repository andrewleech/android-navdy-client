.class Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$DeferStartActiveTripEvent;
.super Ljava/lang/Object;
.source "DisplayNavigationServiceHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DeferStartActiveTripEvent"
.end annotation


# instance fields
.field public final delay:I

.field final expireTimestamp:J

.field final requestId:Ljava/lang/String;

.field final routeId:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;IJ)V
    .locals 0
    .param p1, "requestId"    # Ljava/lang/String;
    .param p2, "routeId"    # Ljava/lang/String;
    .param p3, "delay"    # I
    .param p4, "expireTimestamp"    # J

    .prologue
    .line 102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 103
    iput-object p1, p0, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$DeferStartActiveTripEvent;->requestId:Ljava/lang/String;

    .line 104
    iput-object p2, p0, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$DeferStartActiveTripEvent;->routeId:Ljava/lang/String;

    .line 105
    iput p3, p0, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$DeferStartActiveTripEvent;->delay:I

    .line 106
    iput-wide p4, p0, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$DeferStartActiveTripEvent;->expireTimestamp:J

    .line 107
    return-void
.end method
