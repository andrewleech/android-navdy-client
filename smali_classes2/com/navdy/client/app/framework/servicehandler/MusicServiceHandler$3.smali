.class Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$3;
.super Ljava/lang/Object;
.source "MusicServiceHandler.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->onMusicCollectionRequest(Lcom/navdy/service/library/events/audio/MusicCollectionRequest;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;

.field final synthetic val$request:Lcom/navdy/service/library/events/audio/MusicCollectionRequest;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;Lcom/navdy/service/library/events/audio/MusicCollectionRequest;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;

    .prologue
    .line 194
    iput-object p1, p0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$3;->this$0:Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;

    iput-object p2, p0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$3;->val$request:Lcom/navdy/service/library/events/audio/MusicCollectionRequest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 32

    .prologue
    .line 197
    invoke-static {}, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v27

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string v29, "onMusicCollectionRequest "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$3;->val$request:Lcom/navdy/service/library/events/audio/MusicCollectionRequest;

    move-object/from16 v29, v0

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 198
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$3;->val$request:Lcom/navdy/service/library/events/audio/MusicCollectionRequest;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    iget-object v0, v0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->offset:Ljava/lang/Integer;

    move-object/from16 v27, v0

    if-eqz v27, :cond_5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$3;->val$request:Lcom/navdy/service/library/events/audio/MusicCollectionRequest;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    iget-object v0, v0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->offset:Ljava/lang/Integer;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Ljava/lang/Integer;->intValue()I

    move-result v19

    .line 199
    .local v19, "offset":I
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$3;->val$request:Lcom/navdy/service/library/events/audio/MusicCollectionRequest;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    iget-object v0, v0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->limit:Ljava/lang/Integer;

    move-object/from16 v27, v0

    if-eqz v27, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$3;->val$request:Lcom/navdy/service/library/events/audio/MusicCollectionRequest;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    iget-object v0, v0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->limit:Ljava/lang/Integer;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Ljava/lang/Integer;->intValue()I

    move-result v17

    .line 200
    .local v17, "limit":I
    :goto_1
    const/4 v13, 0x0

    .line 201
    .local v13, "count":I
    const/4 v9, 0x1

    .line 202
    .local v9, "canShuffle":Z
    const/4 v11, 0x0

    .line 203
    .local v11, "collectionList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCollectionInfo;>;"
    const/16 v25, 0x0

    .line 204
    .local v25, "trackList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicTrackInfo;>;"
    const/4 v10, 0x0

    .line 206
    .local v10, "characterMap":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCharacterMap;>;"
    sget-object v27, Lcom/navdy/service/library/events/audio/MusicCollectionSource;->COLLECTION_SOURCE_ANDROID_LOCAL:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$3;->val$request:Lcom/navdy/service/library/events/audio/MusicCollectionRequest;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget-object v0, v0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    move-object/from16 v28, v0

    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/events/audio/MusicCollectionSource;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_4

    .line 207
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$3;->val$request:Lcom/navdy/service/library/events/audio/MusicCollectionRequest;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    iget-object v0, v0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->collectionId:Ljava/lang/String;

    move-object/from16 v27, v0

    if-eqz v27, :cond_19

    .line 208
    sget-object v27, Lcom/navdy/service/library/events/audio/MusicCollectionType;->COLLECTION_TYPE_PLAYLISTS:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$3;->val$request:Lcom/navdy/service/library/events/audio/MusicCollectionRequest;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget-object v0, v0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    move-object/from16 v28, v0

    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/events/audio/MusicCollectionType;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_7

    .line 209
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$3;->val$request:Lcom/navdy/service/library/events/audio/MusicCollectionRequest;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    iget-object v0, v0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->collectionId:Ljava/lang/String;

    move-object/from16 v27, v0

    invoke-static/range {v27 .. v27}, Lcom/navdy/client/app/framework/util/MusicDbUtils;->getPlaylistMembersCursor(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v18

    .line 212
    .local v18, "membersCursor":Landroid/database/Cursor;
    if-eqz v18, :cond_3

    :try_start_0
    invoke-interface/range {v18 .. v19}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v27

    if-eqz v27, :cond_3

    .line 213
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->getCount()I

    move-result v13

    .line 214
    if-nez v17, :cond_0

    .line 215
    move/from16 v17, v13

    .line 217
    :cond_0
    new-instance v26, Ljava/util/ArrayList;

    move-object/from16 v0, v26

    move/from16 v1, v17

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 219
    .end local v25    # "trackList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicTrackInfo;>;"
    .local v26, "trackList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicTrackInfo;>;"
    :cond_1
    :try_start_1
    new-instance v27, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    invoke-direct/range {v27 .. v27}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;-><init>()V

    sget-object v28, Lcom/navdy/service/library/events/audio/MusicCollectionSource;->COLLECTION_SOURCE_ANDROID_LOCAL:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    .line 220
    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->collectionSource(Lcom/navdy/service/library/events/audio/MusicCollectionSource;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v27

    sget-object v28, Lcom/navdy/service/library/events/audio/MusicCollectionType;->COLLECTION_TYPE_PLAYLISTS:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    .line 221
    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->collectionType(Lcom/navdy/service/library/events/audio/MusicCollectionType;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$3;->val$request:Lcom/navdy/service/library/events/audio/MusicCollectionRequest;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget-object v0, v0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->collectionId:Ljava/lang/String;

    move-object/from16 v28, v0

    .line 222
    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->collectionId(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v27

    sget-object v28, Lcom/navdy/service/library/events/audio/MusicPlaybackState;->PLAYBACK_NONE:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    .line 223
    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->playbackState(Lcom/navdy/service/library/events/audio/MusicPlaybackState;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v27

    const-string v28, "title"

    .line 224
    move-object/from16 v0, v18

    move-object/from16 v1, v28

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v28

    move-object/from16 v0, v18

    move/from16 v1, v28

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->name(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v27

    const-string v28, "artist"

    .line 225
    move-object/from16 v0, v18

    move-object/from16 v1, v28

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v28

    move-object/from16 v0, v18

    move/from16 v1, v28

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->author(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v27

    const-string v28, "album"

    .line 226
    move-object/from16 v0, v18

    move-object/from16 v1, v28

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v28

    move-object/from16 v0, v18

    move/from16 v1, v28

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->album(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v27

    const-string v28, "SourceId"

    .line 227
    move-object/from16 v0, v18

    move-object/from16 v1, v28

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v28

    move-object/from16 v0, v18

    move/from16 v1, v28

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v28

    invoke-static/range {v28 .. v28}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->trackId(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v27

    .line 228
    invoke-virtual/range {v27 .. v27}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->build()Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    move-result-object v27

    .line 219
    invoke-interface/range {v26 .. v27}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 230
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->moveToNext()Z

    move-result v27

    if-eqz v27, :cond_2

    invoke-interface/range {v26 .. v26}, Ljava/util/List;->size()I
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_c
    .catchall {:try_start_1 .. :try_end_1} :catchall_e

    move-result v27

    move/from16 v0, v27

    move/from16 v1, v17

    if-lt v0, v1, :cond_1

    :cond_2
    move-object/from16 v25, v26

    .line 235
    .end local v26    # "trackList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicTrackInfo;>;"
    .restart local v25    # "trackList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicTrackInfo;>;"
    :cond_3
    invoke-static/range {v18 .. v18}, Lcom/navdy/service/library/util/IOUtils;->closeObject(Ljava/io/Closeable;)V

    .line 443
    .end local v18    # "membersCursor":Landroid/database/Cursor;
    :cond_4
    :goto_2
    new-instance v27, Lcom/navdy/service/library/events/audio/MusicCollectionResponse$Builder;

    invoke-direct/range {v27 .. v27}, Lcom/navdy/service/library/events/audio/MusicCollectionResponse$Builder;-><init>()V

    new-instance v28, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;

    invoke-direct/range {v28 .. v28}, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$3;->val$request:Lcom/navdy/service/library/events/audio/MusicCollectionRequest;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    iget-object v0, v0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    move-object/from16 v29, v0

    .line 445
    invoke-virtual/range {v28 .. v29}, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;->collectionSource(Lcom/navdy/service/library/events/audio/MusicCollectionSource;)Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;

    move-result-object v28

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$3;->val$request:Lcom/navdy/service/library/events/audio/MusicCollectionRequest;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    iget-object v0, v0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    move-object/from16 v29, v0

    .line 446
    invoke-virtual/range {v28 .. v29}, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;->collectionType(Lcom/navdy/service/library/events/audio/MusicCollectionType;)Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;

    move-result-object v28

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$3;->val$request:Lcom/navdy/service/library/events/audio/MusicCollectionRequest;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    iget-object v0, v0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->collectionId:Ljava/lang/String;

    move-object/from16 v29, v0

    .line 447
    invoke-virtual/range {v28 .. v29}, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;->collectionId(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;

    move-result-object v28

    .line 448
    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v29

    invoke-virtual/range {v28 .. v29}, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;->size(Ljava/lang/Integer;)Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;

    move-result-object v28

    .line 449
    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v29

    invoke-virtual/range {v28 .. v29}, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;->canShuffle(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;

    move-result-object v28

    .line 450
    invoke-virtual/range {v28 .. v28}, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;->build()Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    move-result-object v28

    .line 444
    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/events/audio/MusicCollectionResponse$Builder;->collectionInfo(Lcom/navdy/service/library/events/audio/MusicCollectionInfo;)Lcom/navdy/service/library/events/audio/MusicCollectionResponse$Builder;

    move-result-object v27

    .line 451
    move-object/from16 v0, v27

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/audio/MusicCollectionResponse$Builder;->musicTracks(Ljava/util/List;)Lcom/navdy/service/library/events/audio/MusicCollectionResponse$Builder;

    move-result-object v27

    .line 452
    move-object/from16 v0, v27

    invoke-virtual {v0, v11}, Lcom/navdy/service/library/events/audio/MusicCollectionResponse$Builder;->musicCollections(Ljava/util/List;)Lcom/navdy/service/library/events/audio/MusicCollectionResponse$Builder;

    move-result-object v27

    .line 453
    move-object/from16 v0, v27

    invoke-virtual {v0, v10}, Lcom/navdy/service/library/events/audio/MusicCollectionResponse$Builder;->characterMap(Ljava/util/List;)Lcom/navdy/service/library/events/audio/MusicCollectionResponse$Builder;

    move-result-object v27

    .line 454
    invoke-virtual/range {v27 .. v27}, Lcom/navdy/service/library/events/audio/MusicCollectionResponse$Builder;->build()Lcom/navdy/service/library/events/audio/MusicCollectionResponse;

    move-result-object v27

    .line 443
    invoke-static/range {v27 .. v27}, Lcom/navdy/client/app/framework/DeviceConnection;->postEvent(Lcom/squareup/wire/Message;)Z

    .line 455
    return-void

    .line 198
    .end local v9    # "canShuffle":Z
    .end local v10    # "characterMap":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCharacterMap;>;"
    .end local v11    # "collectionList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCollectionInfo;>;"
    .end local v13    # "count":I
    .end local v17    # "limit":I
    .end local v19    # "offset":I
    .end local v25    # "trackList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicTrackInfo;>;"
    :cond_5
    const/16 v19, 0x0

    goto/16 :goto_0

    .line 199
    .restart local v19    # "offset":I
    :cond_6
    const/16 v17, 0x0

    goto/16 :goto_1

    .line 232
    .restart local v9    # "canShuffle":Z
    .restart local v10    # "characterMap":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCharacterMap;>;"
    .restart local v11    # "collectionList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCollectionInfo;>;"
    .restart local v13    # "count":I
    .restart local v17    # "limit":I
    .restart local v18    # "membersCursor":Landroid/database/Cursor;
    .restart local v25    # "trackList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicTrackInfo;>;"
    :catch_0
    move-exception v24

    .line 233
    .local v24, "t":Ljava/lang/Throwable;
    :goto_3
    :try_start_2
    invoke-static {}, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v27

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string v29, "Error querying GPM database: "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 235
    invoke-static/range {v18 .. v18}, Lcom/navdy/service/library/util/IOUtils;->closeObject(Ljava/io/Closeable;)V

    goto/16 :goto_2

    .end local v24    # "t":Ljava/lang/Throwable;
    :catchall_0
    move-exception v27

    :goto_4
    invoke-static/range {v18 .. v18}, Lcom/navdy/service/library/util/IOUtils;->closeObject(Ljava/io/Closeable;)V

    throw v27

    .line 237
    .end local v18    # "membersCursor":Landroid/database/Cursor;
    :cond_7
    sget-object v27, Lcom/navdy/service/library/events/audio/MusicCollectionType;->COLLECTION_TYPE_ARTISTS:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$3;->val$request:Lcom/navdy/service/library/events/audio/MusicCollectionRequest;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget-object v0, v0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    move-object/from16 v28, v0

    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/events/audio/MusicCollectionType;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_14

    .line 238
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$3;->val$request:Lcom/navdy/service/library/events/audio/MusicCollectionRequest;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    iget-object v0, v0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->groupBy:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    move-object/from16 v27, v0

    sget-object v28, Lcom/navdy/service/library/events/audio/MusicCollectionType;->COLLECTION_TYPE_UNKNOWN:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    invoke-static/range {v27 .. v28}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v27

    sget-object v28, Lcom/navdy/service/library/events/audio/MusicCollectionType;->COLLECTION_TYPE_ALBUMS:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    if-ne v0, v1, :cond_c

    const/4 v14, 0x1

    .line 239
    .local v14, "groupByAlbums":Z
    :goto_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$3;->val$request:Lcom/navdy/service/library/events/audio/MusicCollectionRequest;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    iget-object v0, v0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->collectionId:Ljava/lang/String;

    move-object/from16 v27, v0

    invoke-static/range {v27 .. v27}, Lcom/navdy/client/app/framework/util/MusicDbUtils;->getArtistsAlbums(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4

    .line 240
    .local v4, "albumsCursor":Landroid/database/Cursor;
    const/4 v7, 0x0

    .line 242
    .local v7, "artistTracksCursor":Landroid/database/Cursor;
    if-eqz v14, :cond_e

    .line 243
    if-eqz v4, :cond_b

    :try_start_3
    move/from16 v0, v19

    invoke-interface {v4, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v27

    if-eqz v27, :cond_b

    .line 244
    invoke-interface {v4}, Landroid/database/Cursor;->getCount()I

    move-result v13

    .line 245
    if-nez v17, :cond_8

    .line 246
    move/from16 v17, v13

    .line 248
    :cond_8
    new-instance v12, Ljava/util/ArrayList;

    move/from16 v0, v17

    invoke-direct {v12, v0}, Ljava/util/ArrayList;-><init>(I)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 250
    .end local v11    # "collectionList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCollectionInfo;>;"
    .local v12, "collectionList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCollectionInfo;>;"
    :cond_9
    :try_start_4
    new-instance v27, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;

    invoke-direct/range {v27 .. v27}, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;-><init>()V

    sget-object v28, Lcom/navdy/service/library/events/audio/MusicCollectionSource;->COLLECTION_SOURCE_ANDROID_LOCAL:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    .line 251
    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;->collectionSource(Lcom/navdy/service/library/events/audio/MusicCollectionSource;)Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;

    move-result-object v27

    sget-object v28, Lcom/navdy/service/library/events/audio/MusicCollectionType;->COLLECTION_TYPE_ALBUMS:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    .line 252
    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;->collectionType(Lcom/navdy/service/library/events/audio/MusicCollectionType;)Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;

    move-result-object v27

    const-string v28, "_id"

    .line 253
    move-object/from16 v0, v28

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v28

    move/from16 v0, v28

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v28

    invoke-static/range {v28 .. v28}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;->collectionId(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;

    move-result-object v27

    const-string v28, "album"

    .line 254
    move-object/from16 v0, v28

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v28

    move/from16 v0, v28

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;->name(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;

    move-result-object v27

    const-string v28, "numsongs"

    .line 255
    move-object/from16 v0, v28

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v28

    move/from16 v0, v28

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v28

    invoke-static/range {v28 .. v28}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;->size(Ljava/lang/Integer;)Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;

    move-result-object v27

    .line 256
    invoke-virtual/range {v27 .. v27}, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;->build()Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    move-result-object v15

    .line 257
    .local v15, "info":Lcom/navdy/service/library/events/audio/MusicCollectionInfo;
    invoke-interface {v12, v15}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 259
    invoke-interface {v4}, Landroid/database/Cursor;->moveToNext()Z

    move-result v27

    if-eqz v27, :cond_a

    invoke-interface {v12}, Ljava/util/List;->size()I
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_a
    .catchall {:try_start_4 .. :try_end_4} :catchall_c

    move-result v27

    move/from16 v0, v27

    move/from16 v1, v17

    if-lt v0, v1, :cond_9

    :cond_a
    move-object v11, v12

    .line 261
    .end local v12    # "collectionList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCollectionInfo;>;"
    .end local v15    # "info":Lcom/navdy/service/library/events/audio/MusicCollectionInfo;
    .restart local v11    # "collectionList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCollectionInfo;>;"
    :cond_b
    :try_start_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$3;->val$request:Lcom/navdy/service/library/events/audio/MusicCollectionRequest;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    iget-object v0, v0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->collectionId:Ljava/lang/String;

    move-object/from16 v27, v0

    invoke-static/range {v27 .. v27}, Lcom/navdy/client/app/framework/util/MusicDbUtils;->getArtistSongsCount(Ljava/lang/String;)I
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    move-result v27

    const/16 v28, 0x1

    move/from16 v0, v27

    move/from16 v1, v28

    if-le v0, v1, :cond_d

    const/4 v9, 0x1

    .line 290
    :goto_6
    invoke-static {v4}, Lcom/navdy/service/library/util/IOUtils;->closeObject(Ljava/io/Closeable;)V

    .line 291
    invoke-static {v7}, Lcom/navdy/service/library/util/IOUtils;->closeObject(Ljava/io/Closeable;)V

    goto/16 :goto_2

    .line 238
    .end local v4    # "albumsCursor":Landroid/database/Cursor;
    .end local v7    # "artistTracksCursor":Landroid/database/Cursor;
    .end local v14    # "groupByAlbums":Z
    :cond_c
    const/4 v14, 0x0

    goto/16 :goto_5

    .line 261
    .restart local v4    # "albumsCursor":Landroid/database/Cursor;
    .restart local v7    # "artistTracksCursor":Landroid/database/Cursor;
    .restart local v14    # "groupByAlbums":Z
    :cond_d
    const/4 v9, 0x0

    goto :goto_6

    .line 263
    :cond_e
    :try_start_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$3;->val$request:Lcom/navdy/service/library/events/audio/MusicCollectionRequest;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    iget-object v0, v0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->collectionId:Ljava/lang/String;

    move-object/from16 v27, v0

    invoke-static/range {v27 .. v27}, Lcom/navdy/client/app/framework/util/MusicDbUtils;->getArtistMembersCursor(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 264
    if-eqz v7, :cond_12

    move/from16 v0, v19

    invoke-interface {v7, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v27

    if-eqz v27, :cond_12

    .line 265
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v13

    .line 266
    if-nez v17, :cond_f

    .line 267
    move/from16 v17, v13

    .line 269
    :cond_f
    new-instance v26, Ljava/util/ArrayList;

    move-object/from16 v0, v26

    move/from16 v1, v17

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 271
    .end local v25    # "trackList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicTrackInfo;>;"
    .restart local v26    # "trackList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicTrackInfo;>;"
    :cond_10
    :try_start_7
    new-instance v27, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    invoke-direct/range {v27 .. v27}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;-><init>()V

    sget-object v28, Lcom/navdy/service/library/events/audio/MusicCollectionSource;->COLLECTION_SOURCE_ANDROID_LOCAL:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    .line 272
    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->collectionSource(Lcom/navdy/service/library/events/audio/MusicCollectionSource;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v27

    sget-object v28, Lcom/navdy/service/library/events/audio/MusicCollectionType;->COLLECTION_TYPE_ARTISTS:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    .line 273
    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->collectionType(Lcom/navdy/service/library/events/audio/MusicCollectionType;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$3;->val$request:Lcom/navdy/service/library/events/audio/MusicCollectionRequest;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget-object v0, v0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->collectionId:Ljava/lang/String;

    move-object/from16 v28, v0

    .line 274
    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->collectionId(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v27

    sget-object v28, Lcom/navdy/service/library/events/audio/MusicPlaybackState;->PLAYBACK_NONE:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    .line 275
    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->playbackState(Lcom/navdy/service/library/events/audio/MusicPlaybackState;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v27

    const-string v28, "title"

    .line 276
    move-object/from16 v0, v28

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v28

    move/from16 v0, v28

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->name(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v27

    const-string v28, "artist"

    .line 277
    move-object/from16 v0, v28

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v28

    move/from16 v0, v28

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->author(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v27

    const-string v28, "album"

    .line 278
    move-object/from16 v0, v28

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v28

    move/from16 v0, v28

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->album(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v27

    const-string v28, "_id"

    .line 279
    move-object/from16 v0, v28

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v28

    move/from16 v0, v28

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v28

    invoke-static/range {v28 .. v28}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->trackId(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v27

    .line 280
    invoke-virtual/range {v27 .. v27}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->build()Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    move-result-object v27

    .line 271
    invoke-interface/range {v26 .. v27}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 282
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v27

    if-eqz v27, :cond_11

    invoke-interface/range {v26 .. v26}, Ljava/util/List;->size()I
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_b
    .catchall {:try_start_7 .. :try_end_7} :catchall_d

    move-result v27

    move/from16 v0, v27

    move/from16 v1, v17

    if-lt v0, v1, :cond_10

    :cond_11
    move-object/from16 v25, v26

    .line 284
    .end local v26    # "trackList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicTrackInfo;>;"
    .restart local v25    # "trackList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicTrackInfo;>;"
    :cond_12
    if-eqz v7, :cond_13

    :try_start_8
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v27

    if-eqz v27, :cond_13

    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    move-result v27

    const/16 v28, 0x1

    move/from16 v0, v27

    move/from16 v1, v28

    if-le v0, v1, :cond_13

    const/4 v9, 0x1

    :goto_7
    goto/16 :goto_6

    :cond_13
    const/4 v9, 0x0

    goto :goto_7

    .line 287
    :catch_1
    move-exception v24

    .line 288
    .restart local v24    # "t":Ljava/lang/Throwable;
    :goto_8
    :try_start_9
    invoke-static {}, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v27

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string v29, "Error querying MediaStore database: "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 290
    invoke-static {v4}, Lcom/navdy/service/library/util/IOUtils;->closeObject(Ljava/io/Closeable;)V

    .line 291
    invoke-static {v7}, Lcom/navdy/service/library/util/IOUtils;->closeObject(Ljava/io/Closeable;)V

    goto/16 :goto_2

    .line 290
    .end local v24    # "t":Ljava/lang/Throwable;
    :catchall_1
    move-exception v27

    :goto_9
    invoke-static {v4}, Lcom/navdy/service/library/util/IOUtils;->closeObject(Ljava/io/Closeable;)V

    .line 291
    invoke-static {v7}, Lcom/navdy/service/library/util/IOUtils;->closeObject(Ljava/io/Closeable;)V

    throw v27

    .line 293
    .end local v4    # "albumsCursor":Landroid/database/Cursor;
    .end local v7    # "artistTracksCursor":Landroid/database/Cursor;
    .end local v14    # "groupByAlbums":Z
    :cond_14
    sget-object v27, Lcom/navdy/service/library/events/audio/MusicCollectionType;->COLLECTION_TYPE_ALBUMS:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$3;->val$request:Lcom/navdy/service/library/events/audio/MusicCollectionRequest;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget-object v0, v0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    move-object/from16 v28, v0

    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/events/audio/MusicCollectionType;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_4

    .line 294
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$3;->val$request:Lcom/navdy/service/library/events/audio/MusicCollectionRequest;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    iget-object v0, v0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->collectionId:Ljava/lang/String;

    move-object/from16 v27, v0

    invoke-static/range {v27 .. v27}, Lcom/navdy/client/app/framework/util/MusicDbUtils;->getAlbumMembersCursor(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v18

    .line 297
    .restart local v18    # "membersCursor":Landroid/database/Cursor;
    if-eqz v18, :cond_18

    :try_start_a
    invoke-interface/range {v18 .. v19}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v27

    if-eqz v27, :cond_18

    .line 298
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->getCount()I

    move-result v13

    .line 299
    if-nez v17, :cond_15

    .line 300
    move/from16 v17, v13

    .line 302
    :cond_15
    new-instance v26, Ljava/util/ArrayList;

    move-object/from16 v0, v26

    move/from16 v1, v17

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V
    :try_end_a
    .catch Ljava/lang/Throwable; {:try_start_a .. :try_end_a} :catch_2
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    .line 304
    .end local v25    # "trackList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicTrackInfo;>;"
    .restart local v26    # "trackList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicTrackInfo;>;"
    :cond_16
    :try_start_b
    new-instance v27, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    invoke-direct/range {v27 .. v27}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;-><init>()V

    sget-object v28, Lcom/navdy/service/library/events/audio/MusicCollectionSource;->COLLECTION_SOURCE_ANDROID_LOCAL:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    .line 305
    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->collectionSource(Lcom/navdy/service/library/events/audio/MusicCollectionSource;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v27

    sget-object v28, Lcom/navdy/service/library/events/audio/MusicCollectionType;->COLLECTION_TYPE_ALBUMS:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    .line 306
    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->collectionType(Lcom/navdy/service/library/events/audio/MusicCollectionType;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$3;->val$request:Lcom/navdy/service/library/events/audio/MusicCollectionRequest;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget-object v0, v0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->collectionId:Ljava/lang/String;

    move-object/from16 v28, v0

    .line 307
    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->collectionId(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v27

    sget-object v28, Lcom/navdy/service/library/events/audio/MusicPlaybackState;->PLAYBACK_NONE:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    .line 308
    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->playbackState(Lcom/navdy/service/library/events/audio/MusicPlaybackState;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v27

    const-string v28, "title"

    .line 309
    move-object/from16 v0, v18

    move-object/from16 v1, v28

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v28

    move-object/from16 v0, v18

    move/from16 v1, v28

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->name(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v27

    const-string v28, "artist"

    .line 310
    move-object/from16 v0, v18

    move-object/from16 v1, v28

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v28

    move-object/from16 v0, v18

    move/from16 v1, v28

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->author(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v27

    const-string v28, "album"

    .line 311
    move-object/from16 v0, v18

    move-object/from16 v1, v28

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v28

    move-object/from16 v0, v18

    move/from16 v1, v28

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->album(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v27

    const-string v28, "_id"

    .line 312
    move-object/from16 v0, v18

    move-object/from16 v1, v28

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v28

    move-object/from16 v0, v18

    move/from16 v1, v28

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v28

    invoke-static/range {v28 .. v28}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->trackId(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v27

    .line 313
    invoke-virtual/range {v27 .. v27}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->build()Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    move-result-object v27

    .line 304
    invoke-interface/range {v26 .. v27}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 315
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->moveToNext()Z

    move-result v27

    if-eqz v27, :cond_17

    invoke-interface/range {v26 .. v26}, Ljava/util/List;->size()I
    :try_end_b
    .catch Ljava/lang/Throwable; {:try_start_b .. :try_end_b} :catch_9
    .catchall {:try_start_b .. :try_end_b} :catchall_b

    move-result v27

    move/from16 v0, v27

    move/from16 v1, v17

    if-lt v0, v1, :cond_16

    :cond_17
    move-object/from16 v25, v26

    .line 320
    .end local v26    # "trackList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicTrackInfo;>;"
    .restart local v25    # "trackList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicTrackInfo;>;"
    :cond_18
    invoke-static/range {v18 .. v18}, Lcom/navdy/service/library/util/IOUtils;->closeObject(Ljava/io/Closeable;)V

    goto/16 :goto_2

    .line 317
    :catch_2
    move-exception v24

    .line 318
    .restart local v24    # "t":Ljava/lang/Throwable;
    :goto_a
    :try_start_c
    invoke-static {}, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v27

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string v29, "Error querying MediaStore database: "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_2

    .line 320
    invoke-static/range {v18 .. v18}, Lcom/navdy/service/library/util/IOUtils;->closeObject(Ljava/io/Closeable;)V

    goto/16 :goto_2

    .end local v24    # "t":Ljava/lang/Throwable;
    :catchall_2
    move-exception v27

    :goto_b
    invoke-static/range {v18 .. v18}, Lcom/navdy/service/library/util/IOUtils;->closeObject(Ljava/io/Closeable;)V

    throw v27

    .line 324
    .end local v18    # "membersCursor":Landroid/database/Cursor;
    :cond_19
    sget-object v27, Lcom/navdy/service/library/events/audio/MusicCollectionType;->COLLECTION_TYPE_PLAYLISTS:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$3;->val$request:Lcom/navdy/service/library/events/audio/MusicCollectionRequest;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget-object v0, v0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    move-object/from16 v28, v0

    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/events/audio/MusicCollectionType;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_1f

    .line 325
    invoke-static {}, Lcom/navdy/client/app/framework/util/MusicDbUtils;->getPlaylistsCursor()Landroid/database/Cursor;

    move-result-object v22

    .line 328
    .local v22, "playlistsCursor":Landroid/database/Cursor;
    if-eqz v22, :cond_1e

    :try_start_d
    move-object/from16 v0, v22

    move/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v27

    if-eqz v27, :cond_1e

    .line 329
    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->getCount()I

    move-result v13

    .line 330
    if-nez v17, :cond_1a

    .line 331
    move/from16 v17, v13

    .line 333
    :cond_1a
    new-instance v12, Ljava/util/ArrayList;

    move/from16 v0, v17

    invoke-direct {v12, v0}, Ljava/util/ArrayList;-><init>(I)V
    :try_end_d
    .catch Ljava/lang/Throwable; {:try_start_d .. :try_end_d} :catch_8
    .catchall {:try_start_d .. :try_end_d} :catchall_4

    .line 335
    .end local v11    # "collectionList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCollectionInfo;>;"
    .restart local v12    # "collectionList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCollectionInfo;>;"
    :cond_1b
    :try_start_e
    const-string v27, "playlist_id"

    move-object/from16 v0, v22

    move-object/from16 v1, v27

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v27

    move-object/from16 v0, v22

    move/from16 v1, v27

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v20

    .line 336
    .local v20, "playlistId":I
    const-string v27, "playlist_name"

    move-object/from16 v0, v22

    move-object/from16 v1, v27

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v27

    move-object/from16 v0, v22

    move/from16 v1, v27

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v21

    .line 337
    .local v21, "playlistName":Ljava/lang/String;
    invoke-static/range {v20 .. v20}, Lcom/navdy/client/app/framework/util/MusicDbUtils;->getPlaylistMembersCursor(I)Landroid/database/Cursor;
    :try_end_e
    .catch Ljava/lang/Throwable; {:try_start_e .. :try_end_e} :catch_3
    .catchall {:try_start_e .. :try_end_e} :catchall_a

    move-result-object v18

    .line 340
    .restart local v18    # "membersCursor":Landroid/database/Cursor;
    if-eqz v18, :cond_1c

    :try_start_f
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v27

    if-eqz v27, :cond_1c

    .line 341
    new-instance v27, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;

    invoke-direct/range {v27 .. v27}, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;-><init>()V

    sget-object v28, Lcom/navdy/service/library/events/audio/MusicCollectionSource;->COLLECTION_SOURCE_ANDROID_LOCAL:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    .line 342
    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;->collectionSource(Lcom/navdy/service/library/events/audio/MusicCollectionSource;)Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;

    move-result-object v27

    sget-object v28, Lcom/navdy/service/library/events/audio/MusicCollectionType;->COLLECTION_TYPE_PLAYLISTS:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    .line 343
    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;->collectionType(Lcom/navdy/service/library/events/audio/MusicCollectionType;)Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;

    move-result-object v27

    .line 344
    invoke-static/range {v20 .. v20}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;->collectionId(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;

    move-result-object v27

    .line 345
    move-object/from16 v0, v27

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;->name(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;

    move-result-object v27

    .line 346
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->getCount()I

    move-result v28

    invoke-static/range {v28 .. v28}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;->size(Ljava/lang/Integer;)Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;

    move-result-object v27

    .line 347
    invoke-virtual/range {v27 .. v27}, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;->build()Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    move-result-object v15

    .line 348
    .restart local v15    # "info":Lcom/navdy/service/library/events/audio/MusicCollectionInfo;
    invoke-interface {v12, v15}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_3

    .line 351
    .end local v15    # "info":Lcom/navdy/service/library/events/audio/MusicCollectionInfo;
    :cond_1c
    :try_start_10
    invoke-static/range {v18 .. v18}, Lcom/navdy/service/library/util/IOUtils;->closeObject(Ljava/io/Closeable;)V

    .line 354
    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->moveToNext()Z

    move-result v27

    if-eqz v27, :cond_1d

    invoke-interface {v12}, Ljava/util/List;->size()I
    :try_end_10
    .catch Ljava/lang/Throwable; {:try_start_10 .. :try_end_10} :catch_3
    .catchall {:try_start_10 .. :try_end_10} :catchall_a

    move-result v27

    move/from16 v0, v27

    move/from16 v1, v17

    if-lt v0, v1, :cond_1b

    :cond_1d
    move-object v11, v12

    .line 359
    .end local v12    # "collectionList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCollectionInfo;>;"
    .end local v18    # "membersCursor":Landroid/database/Cursor;
    .end local v20    # "playlistId":I
    .end local v21    # "playlistName":Ljava/lang/String;
    .restart local v11    # "collectionList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCollectionInfo;>;"
    :cond_1e
    invoke-static/range {v22 .. v22}, Lcom/navdy/service/library/util/IOUtils;->closeObject(Ljava/io/Closeable;)V

    .line 361
    :goto_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$3;->val$request:Lcom/navdy/service/library/events/audio/MusicCollectionRequest;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    iget-object v0, v0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->includeCharacterMap:Ljava/lang/Boolean;

    move-object/from16 v27, v0

    const/16 v28, 0x0

    invoke-static/range {v28 .. v28}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v28

    invoke-static/range {v27 .. v28}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Ljava/lang/Boolean;

    invoke-virtual/range {v27 .. v27}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v27

    if-eqz v27, :cond_4

    .line 362
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$3;->this$0:Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;

    move-object/from16 v27, v0

    invoke-static {}, Lcom/navdy/client/app/framework/util/MusicDbUtils;->getPlaylistsCharacterMapCursor()Landroid/database/Cursor;

    move-result-object v28

    invoke-static/range {v27 .. v28}, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->access$300(Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;Landroid/database/Cursor;)Ljava/util/List;

    move-result-object v10

    goto/16 :goto_2

    .line 351
    .end local v11    # "collectionList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCollectionInfo;>;"
    .restart local v12    # "collectionList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCollectionInfo;>;"
    .restart local v18    # "membersCursor":Landroid/database/Cursor;
    .restart local v20    # "playlistId":I
    .restart local v21    # "playlistName":Ljava/lang/String;
    :catchall_3
    move-exception v27

    :try_start_11
    invoke-static/range {v18 .. v18}, Lcom/navdy/service/library/util/IOUtils;->closeObject(Ljava/io/Closeable;)V

    throw v27
    :try_end_11
    .catch Ljava/lang/Throwable; {:try_start_11 .. :try_end_11} :catch_3
    .catchall {:try_start_11 .. :try_end_11} :catchall_a

    .line 356
    .end local v18    # "membersCursor":Landroid/database/Cursor;
    .end local v20    # "playlistId":I
    .end local v21    # "playlistName":Ljava/lang/String;
    :catch_3
    move-exception v24

    move-object v11, v12

    .line 357
    .end local v12    # "collectionList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCollectionInfo;>;"
    .restart local v11    # "collectionList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCollectionInfo;>;"
    .restart local v24    # "t":Ljava/lang/Throwable;
    :goto_d
    :try_start_12
    invoke-static {}, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v27

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string v29, "Error querying GPM database: "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_4

    .line 359
    invoke-static/range {v22 .. v22}, Lcom/navdy/service/library/util/IOUtils;->closeObject(Ljava/io/Closeable;)V

    goto :goto_c

    .end local v24    # "t":Ljava/lang/Throwable;
    :catchall_4
    move-exception v27

    :goto_e
    invoke-static/range {v22 .. v22}, Lcom/navdy/service/library/util/IOUtils;->closeObject(Ljava/io/Closeable;)V

    throw v27

    .line 364
    .end local v22    # "playlistsCursor":Landroid/database/Cursor;
    :cond_1f
    sget-object v27, Lcom/navdy/service/library/events/audio/MusicCollectionType;->COLLECTION_TYPE_ARTISTS:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$3;->val$request:Lcom/navdy/service/library/events/audio/MusicCollectionRequest;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget-object v0, v0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    move-object/from16 v28, v0

    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/events/audio/MusicCollectionType;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_28

    .line 365
    invoke-static {}, Lcom/navdy/client/app/framework/util/MusicDbUtils;->getArtistsCursor()Landroid/database/Cursor;

    move-result-object v8

    .line 366
    .local v8, "artistsCursor":Landroid/database/Cursor;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$3;->val$request:Lcom/navdy/service/library/events/audio/MusicCollectionRequest;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    iget-object v0, v0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->groupBy:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    move-object/from16 v27, v0

    sget-object v28, Lcom/navdy/service/library/events/audio/MusicCollectionType;->COLLECTION_TYPE_UNKNOWN:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    invoke-static/range {v27 .. v28}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v27

    sget-object v28, Lcom/navdy/service/library/events/audio/MusicCollectionType;->COLLECTION_TYPE_ALBUMS:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    if-ne v0, v1, :cond_26

    const/4 v14, 0x1

    .line 368
    .restart local v14    # "groupByAlbums":Z
    :goto_f
    if-eqz v8, :cond_25

    :try_start_13
    move/from16 v0, v19

    invoke-interface {v8, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v27

    if-eqz v27, :cond_25

    .line 369
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v13

    .line 370
    if-nez v17, :cond_20

    .line 371
    move/from16 v17, v13

    .line 373
    :cond_20
    new-instance v12, Ljava/util/ArrayList;

    move/from16 v0, v17

    invoke-direct {v12, v0}, Ljava/util/ArrayList;-><init>(I)V
    :try_end_13
    .catch Ljava/lang/Throwable; {:try_start_13 .. :try_end_13} :catch_7
    .catchall {:try_start_13 .. :try_end_13} :catchall_6

    .line 375
    .end local v11    # "collectionList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCollectionInfo;>;"
    .restart local v12    # "collectionList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCollectionInfo;>;"
    :cond_21
    :try_start_14
    const-string v27, "_id"

    move-object/from16 v0, v27

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v27

    move/from16 v0, v27

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 376
    .local v5, "artistId":Ljava/lang/String;
    const-string v27, "artist"

    move-object/from16 v0, v27

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v27

    move/from16 v0, v27

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 378
    .local v6, "artistName":Ljava/lang/String;
    if-eqz v14, :cond_27

    invoke-static {v5}, Lcom/navdy/client/app/framework/util/MusicDbUtils;->getArtistsAlbums(Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_14
    .catch Ljava/lang/Throwable; {:try_start_14 .. :try_end_14} :catch_4
    .catchall {:try_start_14 .. :try_end_14} :catchall_9

    move-result-object v18

    .line 380
    .restart local v18    # "membersCursor":Landroid/database/Cursor;
    :goto_10
    if-eqz v18, :cond_23

    :try_start_15
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v27

    if-eqz v27, :cond_23

    .line 381
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->getCount()I

    move-result v3

    .line 382
    .local v3, "albumsCount":I
    new-instance v27, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;

    invoke-direct/range {v27 .. v27}, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;-><init>()V

    sget-object v28, Lcom/navdy/service/library/events/audio/MusicCollectionSource;->COLLECTION_SOURCE_ANDROID_LOCAL:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    .line 383
    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;->collectionSource(Lcom/navdy/service/library/events/audio/MusicCollectionSource;)Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;

    move-result-object v27

    sget-object v28, Lcom/navdy/service/library/events/audio/MusicCollectionType;->COLLECTION_TYPE_ARTISTS:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    .line 384
    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;->collectionType(Lcom/navdy/service/library/events/audio/MusicCollectionType;)Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;

    move-result-object v27

    .line 385
    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;->collectionId(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;

    move-result-object v27

    .line 386
    move-object/from16 v0, v27

    invoke-virtual {v0, v6}, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;->name(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;

    move-result-object v27

    .line 387
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->getCount()I

    move-result v28

    invoke-static/range {v28 .. v28}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;->size(Ljava/lang/Integer;)Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;

    move-result-object v16

    .line 388
    .local v16, "infoBuilder":Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;
    if-eqz v14, :cond_22

    .line 389
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v27

    const/high16 v28, 0x7f0a0000

    const/16 v29, 0x1

    move/from16 v0, v29

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v29, v0

    const/16 v30, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v31

    aput-object v31, v29, v30

    move-object/from16 v0, v27

    move/from16 v1, v28

    move-object/from16 v2, v29

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v23

    .line 390
    .local v23, "subtitle":Ljava/lang/String;
    move-object/from16 v0, v16

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;->subtitle(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;

    .line 392
    .end local v23    # "subtitle":Ljava/lang/String;
    :cond_22
    invoke-virtual/range {v16 .. v16}, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;->build()Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    move-result-object v27

    move-object/from16 v0, v27

    invoke-interface {v12, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_5

    .line 395
    .end local v3    # "albumsCount":I
    .end local v16    # "infoBuilder":Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;
    :cond_23
    :try_start_16
    invoke-static/range {v18 .. v18}, Lcom/navdy/service/library/util/IOUtils;->closeObject(Ljava/io/Closeable;)V

    .line 398
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v27

    if-eqz v27, :cond_24

    invoke-interface {v12}, Ljava/util/List;->size()I
    :try_end_16
    .catch Ljava/lang/Throwable; {:try_start_16 .. :try_end_16} :catch_4
    .catchall {:try_start_16 .. :try_end_16} :catchall_9

    move-result v27

    move/from16 v0, v27

    move/from16 v1, v17

    if-lt v0, v1, :cond_21

    :cond_24
    move-object v11, v12

    .line 403
    .end local v5    # "artistId":Ljava/lang/String;
    .end local v6    # "artistName":Ljava/lang/String;
    .end local v12    # "collectionList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCollectionInfo;>;"
    .end local v18    # "membersCursor":Landroid/database/Cursor;
    .restart local v11    # "collectionList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCollectionInfo;>;"
    :cond_25
    invoke-static {v8}, Lcom/navdy/service/library/util/IOUtils;->closeObject(Ljava/io/Closeable;)V

    .line 405
    :goto_11
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$3;->val$request:Lcom/navdy/service/library/events/audio/MusicCollectionRequest;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    iget-object v0, v0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->includeCharacterMap:Ljava/lang/Boolean;

    move-object/from16 v27, v0

    const/16 v28, 0x0

    invoke-static/range {v28 .. v28}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v28

    invoke-static/range {v27 .. v28}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Ljava/lang/Boolean;

    invoke-virtual/range {v27 .. v27}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v27

    if-eqz v27, :cond_4

    .line 406
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$3;->this$0:Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;

    move-object/from16 v27, v0

    invoke-static {}, Lcom/navdy/client/app/framework/util/MusicDbUtils;->getArtistsCharacterMapCursor()Landroid/database/Cursor;

    move-result-object v28

    invoke-static/range {v27 .. v28}, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->access$300(Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;Landroid/database/Cursor;)Ljava/util/List;

    move-result-object v10

    goto/16 :goto_2

    .line 366
    .end local v14    # "groupByAlbums":Z
    :cond_26
    const/4 v14, 0x0

    goto/16 :goto_f

    .line 378
    .end local v11    # "collectionList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCollectionInfo;>;"
    .restart local v5    # "artistId":Ljava/lang/String;
    .restart local v6    # "artistName":Ljava/lang/String;
    .restart local v12    # "collectionList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCollectionInfo;>;"
    .restart local v14    # "groupByAlbums":Z
    :cond_27
    :try_start_17
    invoke-static {v5}, Lcom/navdy/client/app/framework/util/MusicDbUtils;->getArtistMembersCursor(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v18

    goto/16 :goto_10

    .line 395
    .restart local v18    # "membersCursor":Landroid/database/Cursor;
    :catchall_5
    move-exception v27

    invoke-static/range {v18 .. v18}, Lcom/navdy/service/library/util/IOUtils;->closeObject(Ljava/io/Closeable;)V

    throw v27
    :try_end_17
    .catch Ljava/lang/Throwable; {:try_start_17 .. :try_end_17} :catch_4
    .catchall {:try_start_17 .. :try_end_17} :catchall_9

    .line 400
    .end local v5    # "artistId":Ljava/lang/String;
    .end local v6    # "artistName":Ljava/lang/String;
    .end local v18    # "membersCursor":Landroid/database/Cursor;
    :catch_4
    move-exception v24

    move-object v11, v12

    .line 401
    .end local v12    # "collectionList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCollectionInfo;>;"
    .restart local v11    # "collectionList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCollectionInfo;>;"
    .restart local v24    # "t":Ljava/lang/Throwable;
    :goto_12
    :try_start_18
    invoke-static {}, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v27

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string v29, "Error querying MediaStore database: "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V
    :try_end_18
    .catchall {:try_start_18 .. :try_end_18} :catchall_6

    .line 403
    invoke-static {v8}, Lcom/navdy/service/library/util/IOUtils;->closeObject(Ljava/io/Closeable;)V

    goto :goto_11

    .end local v24    # "t":Ljava/lang/Throwable;
    :catchall_6
    move-exception v27

    :goto_13
    invoke-static {v8}, Lcom/navdy/service/library/util/IOUtils;->closeObject(Ljava/io/Closeable;)V

    throw v27

    .line 408
    .end local v8    # "artistsCursor":Landroid/database/Cursor;
    .end local v14    # "groupByAlbums":Z
    :cond_28
    sget-object v27, Lcom/navdy/service/library/events/audio/MusicCollectionType;->COLLECTION_TYPE_ALBUMS:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$3;->val$request:Lcom/navdy/service/library/events/audio/MusicCollectionRequest;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget-object v0, v0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    move-object/from16 v28, v0

    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/events/audio/MusicCollectionType;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_4

    .line 409
    invoke-static {}, Lcom/navdy/client/app/framework/util/MusicDbUtils;->getAlbumsCursor()Landroid/database/Cursor;

    move-result-object v4

    .line 412
    .restart local v4    # "albumsCursor":Landroid/database/Cursor;
    if-eqz v4, :cond_2c

    :try_start_19
    move/from16 v0, v19

    invoke-interface {v4, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v27

    if-eqz v27, :cond_2c

    .line 413
    invoke-interface {v4}, Landroid/database/Cursor;->getCount()I

    move-result v13

    .line 414
    if-nez v17, :cond_29

    .line 415
    move/from16 v17, v13

    .line 417
    :cond_29
    new-instance v12, Ljava/util/ArrayList;

    move/from16 v0, v17

    invoke-direct {v12, v0}, Ljava/util/ArrayList;-><init>(I)V
    :try_end_19
    .catch Ljava/lang/Throwable; {:try_start_19 .. :try_end_19} :catch_5
    .catchall {:try_start_19 .. :try_end_19} :catchall_7

    .line 419
    .end local v11    # "collectionList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCollectionInfo;>;"
    .restart local v12    # "collectionList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCollectionInfo;>;"
    :cond_2a
    :try_start_1a
    new-instance v27, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;

    invoke-direct/range {v27 .. v27}, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;-><init>()V

    sget-object v28, Lcom/navdy/service/library/events/audio/MusicCollectionSource;->COLLECTION_SOURCE_ANDROID_LOCAL:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    .line 420
    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;->collectionSource(Lcom/navdy/service/library/events/audio/MusicCollectionSource;)Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;

    move-result-object v27

    sget-object v28, Lcom/navdy/service/library/events/audio/MusicCollectionType;->COLLECTION_TYPE_ALBUMS:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    .line 421
    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;->collectionType(Lcom/navdy/service/library/events/audio/MusicCollectionType;)Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;

    move-result-object v27

    const-string v28, "_id"

    .line 422
    move-object/from16 v0, v28

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v28

    move/from16 v0, v28

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v28

    invoke-static/range {v28 .. v28}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;->collectionId(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;

    move-result-object v27

    const-string v28, "album"

    .line 423
    move-object/from16 v0, v28

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v28

    move/from16 v0, v28

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;->name(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;

    move-result-object v27

    const-string v28, "numsongs"

    .line 424
    move-object/from16 v0, v28

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v28

    move/from16 v0, v28

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v28

    invoke-static/range {v28 .. v28}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;->size(Ljava/lang/Integer;)Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;

    move-result-object v27

    const-string v28, "artist"

    .line 425
    move-object/from16 v0, v28

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v28

    move/from16 v0, v28

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;->subtitle(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;

    move-result-object v27

    .line 426
    invoke-virtual/range {v27 .. v27}, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;->build()Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    move-result-object v15

    .line 427
    .restart local v15    # "info":Lcom/navdy/service/library/events/audio/MusicCollectionInfo;
    invoke-interface {v12, v15}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 429
    invoke-interface {v4}, Landroid/database/Cursor;->moveToNext()Z

    move-result v27

    if-eqz v27, :cond_2b

    invoke-interface {v12}, Ljava/util/List;->size()I
    :try_end_1a
    .catch Ljava/lang/Throwable; {:try_start_1a .. :try_end_1a} :catch_6
    .catchall {:try_start_1a .. :try_end_1a} :catchall_8

    move-result v27

    move/from16 v0, v27

    move/from16 v1, v17

    if-lt v0, v1, :cond_2a

    :cond_2b
    move-object v11, v12

    .line 434
    .end local v12    # "collectionList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCollectionInfo;>;"
    .end local v15    # "info":Lcom/navdy/service/library/events/audio/MusicCollectionInfo;
    .restart local v11    # "collectionList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCollectionInfo;>;"
    :cond_2c
    invoke-static {v4}, Lcom/navdy/service/library/util/IOUtils;->closeObject(Ljava/io/Closeable;)V

    .line 436
    :goto_14
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$3;->val$request:Lcom/navdy/service/library/events/audio/MusicCollectionRequest;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    iget-object v0, v0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->includeCharacterMap:Ljava/lang/Boolean;

    move-object/from16 v27, v0

    const/16 v28, 0x0

    invoke-static/range {v28 .. v28}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v28

    invoke-static/range {v27 .. v28}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Ljava/lang/Boolean;

    invoke-virtual/range {v27 .. v27}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v27

    if-eqz v27, :cond_4

    .line 437
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$3;->this$0:Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;

    move-object/from16 v27, v0

    invoke-static {}, Lcom/navdy/client/app/framework/util/MusicDbUtils;->getAlbumsCharacterMapCursor()Landroid/database/Cursor;

    move-result-object v28

    invoke-static/range {v27 .. v28}, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->access$300(Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;Landroid/database/Cursor;)Ljava/util/List;

    move-result-object v10

    goto/16 :goto_2

    .line 431
    :catch_5
    move-exception v24

    .line 432
    .restart local v24    # "t":Ljava/lang/Throwable;
    :goto_15
    :try_start_1b
    invoke-static {}, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v27

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string v29, "Error querying MediaStore database: "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V
    :try_end_1b
    .catchall {:try_start_1b .. :try_end_1b} :catchall_7

    .line 434
    invoke-static {v4}, Lcom/navdy/service/library/util/IOUtils;->closeObject(Ljava/io/Closeable;)V

    goto :goto_14

    .end local v24    # "t":Ljava/lang/Throwable;
    :catchall_7
    move-exception v27

    :goto_16
    invoke-static {v4}, Lcom/navdy/service/library/util/IOUtils;->closeObject(Ljava/io/Closeable;)V

    throw v27

    .end local v11    # "collectionList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCollectionInfo;>;"
    .restart local v12    # "collectionList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCollectionInfo;>;"
    :catchall_8
    move-exception v27

    move-object v11, v12

    .end local v12    # "collectionList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCollectionInfo;>;"
    .restart local v11    # "collectionList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCollectionInfo;>;"
    goto :goto_16

    .line 431
    .end local v11    # "collectionList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCollectionInfo;>;"
    .restart local v12    # "collectionList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCollectionInfo;>;"
    :catch_6
    move-exception v24

    move-object v11, v12

    .end local v12    # "collectionList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCollectionInfo;>;"
    .restart local v11    # "collectionList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCollectionInfo;>;"
    goto :goto_15

    .line 403
    .end local v4    # "albumsCursor":Landroid/database/Cursor;
    .end local v11    # "collectionList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCollectionInfo;>;"
    .restart local v8    # "artistsCursor":Landroid/database/Cursor;
    .restart local v12    # "collectionList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCollectionInfo;>;"
    .restart local v14    # "groupByAlbums":Z
    :catchall_9
    move-exception v27

    move-object v11, v12

    .end local v12    # "collectionList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCollectionInfo;>;"
    .restart local v11    # "collectionList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCollectionInfo;>;"
    goto/16 :goto_13

    .line 400
    :catch_7
    move-exception v24

    goto/16 :goto_12

    .line 359
    .end local v8    # "artistsCursor":Landroid/database/Cursor;
    .end local v11    # "collectionList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCollectionInfo;>;"
    .end local v14    # "groupByAlbums":Z
    .restart local v12    # "collectionList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCollectionInfo;>;"
    .restart local v22    # "playlistsCursor":Landroid/database/Cursor;
    :catchall_a
    move-exception v27

    move-object v11, v12

    .end local v12    # "collectionList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCollectionInfo;>;"
    .restart local v11    # "collectionList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCollectionInfo;>;"
    goto/16 :goto_e

    .line 356
    :catch_8
    move-exception v24

    goto/16 :goto_d

    .line 320
    .end local v22    # "playlistsCursor":Landroid/database/Cursor;
    .end local v25    # "trackList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicTrackInfo;>;"
    .restart local v18    # "membersCursor":Landroid/database/Cursor;
    .restart local v26    # "trackList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicTrackInfo;>;"
    :catchall_b
    move-exception v27

    move-object/from16 v25, v26

    .end local v26    # "trackList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicTrackInfo;>;"
    .restart local v25    # "trackList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicTrackInfo;>;"
    goto/16 :goto_b

    .line 317
    .end local v25    # "trackList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicTrackInfo;>;"
    .restart local v26    # "trackList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicTrackInfo;>;"
    :catch_9
    move-exception v24

    move-object/from16 v25, v26

    .end local v26    # "trackList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicTrackInfo;>;"
    .restart local v25    # "trackList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicTrackInfo;>;"
    goto/16 :goto_a

    .line 290
    .end local v11    # "collectionList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCollectionInfo;>;"
    .end local v18    # "membersCursor":Landroid/database/Cursor;
    .restart local v4    # "albumsCursor":Landroid/database/Cursor;
    .restart local v7    # "artistTracksCursor":Landroid/database/Cursor;
    .restart local v12    # "collectionList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCollectionInfo;>;"
    .restart local v14    # "groupByAlbums":Z
    :catchall_c
    move-exception v27

    move-object v11, v12

    .end local v12    # "collectionList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCollectionInfo;>;"
    .restart local v11    # "collectionList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCollectionInfo;>;"
    goto/16 :goto_9

    .end local v25    # "trackList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicTrackInfo;>;"
    .restart local v26    # "trackList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicTrackInfo;>;"
    :catchall_d
    move-exception v27

    move-object/from16 v25, v26

    .end local v26    # "trackList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicTrackInfo;>;"
    .restart local v25    # "trackList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicTrackInfo;>;"
    goto/16 :goto_9

    .line 287
    .end local v11    # "collectionList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCollectionInfo;>;"
    .restart local v12    # "collectionList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCollectionInfo;>;"
    :catch_a
    move-exception v24

    move-object v11, v12

    .end local v12    # "collectionList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCollectionInfo;>;"
    .restart local v11    # "collectionList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCollectionInfo;>;"
    goto/16 :goto_8

    .end local v25    # "trackList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicTrackInfo;>;"
    .restart local v26    # "trackList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicTrackInfo;>;"
    :catch_b
    move-exception v24

    move-object/from16 v25, v26

    .end local v26    # "trackList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicTrackInfo;>;"
    .restart local v25    # "trackList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicTrackInfo;>;"
    goto/16 :goto_8

    .line 235
    .end local v4    # "albumsCursor":Landroid/database/Cursor;
    .end local v7    # "artistTracksCursor":Landroid/database/Cursor;
    .end local v14    # "groupByAlbums":Z
    .end local v25    # "trackList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicTrackInfo;>;"
    .restart local v18    # "membersCursor":Landroid/database/Cursor;
    .restart local v26    # "trackList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicTrackInfo;>;"
    :catchall_e
    move-exception v27

    move-object/from16 v25, v26

    .end local v26    # "trackList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicTrackInfo;>;"
    .restart local v25    # "trackList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicTrackInfo;>;"
    goto/16 :goto_4

    .line 232
    .end local v25    # "trackList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicTrackInfo;>;"
    .restart local v26    # "trackList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicTrackInfo;>;"
    :catch_c
    move-exception v24

    move-object/from16 v25, v26

    .end local v26    # "trackList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicTrackInfo;>;"
    .restart local v25    # "trackList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicTrackInfo;>;"
    goto/16 :goto_3
.end method
