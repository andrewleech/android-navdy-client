.class Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$9;
.super Ljava/lang/Object;
.source "DisplayNavigationServiceHandler.java"

# interfaces
.implements Lcom/here/android/mpa/common/OnEngineInitListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler;->postWhenReady(Ljava/lang/Object;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler;

.field final synthetic val$event:Ljava/lang/Object;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler;Ljava/lang/Object;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler;

    .prologue
    .line 366
    iput-object p1, p0, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$9;->this$0:Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler;

    iput-object p2, p0, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$9;->val$event:Ljava/lang/Object;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEngineInitializationCompleted(Lcom/here/android/mpa/common/OnEngineInitListener$Error;)V
    .locals 3
    .param p1, "error"    # Lcom/here/android/mpa/common/OnEngineInitListener$Error;

    .prologue
    .line 369
    sget-object v0, Lcom/here/android/mpa/common/OnEngineInitListener$Error;->NONE:Lcom/here/android/mpa/common/OnEngineInitListener$Error;

    if-eq p1, v0, :cond_0

    .line 370
    invoke-static {}, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler;->access$800()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "can\'t post navigation events, error "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/here/android/mpa/common/OnEngineInitListener$Error;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 374
    :goto_0
    return-void

    .line 373
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$9;->this$0:Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler;

    invoke-static {v0}, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler;->access$900(Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler;)Lcom/squareup/otto/Bus;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$9;->val$event:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    goto :goto_0
.end method
