.class Lcom/navdy/client/app/framework/servicehandler/LocationTransmitter$2;
.super Ljava/lang/Object;
.source "LocationTransmitter.java"

# interfaces
.implements Lcom/navdy/service/library/device/RemoteDevice$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/framework/servicehandler/LocationTransmitter;-><init>(Landroid/content/Context;Lcom/navdy/service/library/device/RemoteDevice;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/framework/servicehandler/LocationTransmitter;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/servicehandler/LocationTransmitter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/framework/servicehandler/LocationTransmitter;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/navdy/client/app/framework/servicehandler/LocationTransmitter$2;->this$0:Lcom/navdy/client/app/framework/servicehandler/LocationTransmitter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDeviceConnectFailure(Lcom/navdy/service/library/device/RemoteDevice;Lcom/navdy/service/library/device/connection/Connection$ConnectionFailureCause;)V
    .locals 0
    .param p1, "device"    # Lcom/navdy/service/library/device/RemoteDevice;
    .param p2, "cause"    # Lcom/navdy/service/library/device/connection/Connection$ConnectionFailureCause;

    .prologue
    .line 78
    return-void
.end method

.method public onDeviceConnected(Lcom/navdy/service/library/device/RemoteDevice;)V
    .locals 2
    .param p1, "device"    # Lcom/navdy/service/library/device/RemoteDevice;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/navdy/client/app/framework/servicehandler/LocationTransmitter$2;->this$0:Lcom/navdy/client/app/framework/servicehandler/LocationTransmitter;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/navdy/client/app/framework/servicehandler/LocationTransmitter;->mConnected:Z

    .line 70
    return-void
.end method

.method public onDeviceConnecting(Lcom/navdy/service/library/device/RemoteDevice;)V
    .locals 0
    .param p1, "device"    # Lcom/navdy/service/library/device/RemoteDevice;

    .prologue
    .line 65
    return-void
.end method

.method public onDeviceDisconnected(Lcom/navdy/service/library/device/RemoteDevice;Lcom/navdy/service/library/device/connection/Connection$DisconnectCause;)V
    .locals 2
    .param p1, "device"    # Lcom/navdy/service/library/device/RemoteDevice;
    .param p2, "cause"    # Lcom/navdy/service/library/device/connection/Connection$DisconnectCause;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/navdy/client/app/framework/servicehandler/LocationTransmitter$2;->this$0:Lcom/navdy/client/app/framework/servicehandler/LocationTransmitter;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/navdy/client/app/framework/servicehandler/LocationTransmitter;->mConnected:Z

    .line 75
    return-void
.end method

.method public onNavdyEventReceived(Lcom/navdy/service/library/device/RemoteDevice;Lcom/navdy/service/library/events/NavdyEvent;)V
    .locals 0
    .param p1, "device"    # Lcom/navdy/service/library/device/RemoteDevice;
    .param p2, "event"    # Lcom/navdy/service/library/events/NavdyEvent;

    .prologue
    .line 81
    return-void
.end method

.method public onNavdyEventReceived(Lcom/navdy/service/library/device/RemoteDevice;[B)V
    .locals 0
    .param p1, "device"    # Lcom/navdy/service/library/device/RemoteDevice;
    .param p2, "event"    # [B

    .prologue
    .line 84
    return-void
.end method
