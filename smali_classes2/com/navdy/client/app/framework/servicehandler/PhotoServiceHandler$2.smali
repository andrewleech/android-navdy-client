.class Lcom/navdy/client/app/framework/servicehandler/PhotoServiceHandler$2;
.super Ljava/lang/Object;
.source "PhotoServiceHandler.java"

# interfaces
.implements Lcom/navdy/client/app/framework/util/ImageUtils$StreamFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/framework/servicehandler/PhotoServiceHandler;->getContactPhoto(Landroid/content/ContentResolver;Landroid/database/Cursor;I)Landroid/graphics/Bitmap;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/framework/servicehandler/PhotoServiceHandler;

.field final synthetic val$cr:Landroid/content/ContentResolver;

.field final synthetic val$photoUri:Landroid/net/Uri;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/servicehandler/PhotoServiceHandler;Landroid/content/ContentResolver;Landroid/net/Uri;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/framework/servicehandler/PhotoServiceHandler;

    .prologue
    .line 203
    iput-object p1, p0, Lcom/navdy/client/app/framework/servicehandler/PhotoServiceHandler$2;->this$0:Lcom/navdy/client/app/framework/servicehandler/PhotoServiceHandler;

    iput-object p2, p0, Lcom/navdy/client/app/framework/servicehandler/PhotoServiceHandler$2;->val$cr:Landroid/content/ContentResolver;

    iput-object p3, p0, Lcom/navdy/client/app/framework/servicehandler/PhotoServiceHandler$2;->val$photoUri:Landroid/net/Uri;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getInputStream()Ljava/io/InputStream;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 206
    iget-object v0, p0, Lcom/navdy/client/app/framework/servicehandler/PhotoServiceHandler$2;->val$cr:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/navdy/client/app/framework/servicehandler/PhotoServiceHandler$2;->val$photoUri:Landroid/net/Uri;

    invoke-static {v0, v1}, Landroid/provider/ContactsContract$Contacts;->openContactPhotoInputStream(Landroid/content/ContentResolver;Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v0

    return-object v0
.end method
