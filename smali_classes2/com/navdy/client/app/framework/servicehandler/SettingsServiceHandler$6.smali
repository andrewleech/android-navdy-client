.class Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler$6;
.super Ljava/lang/Object;
.source "SettingsServiceHandler.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler;->onNavigationPreferencesUpdate(Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler;

.field final synthetic val$navigationPreferencesUpdate:Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler;Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler;

    .prologue
    .line 168
    iput-object p1, p0, Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler$6;->this$0:Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler;

    iput-object p2, p0, Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler$6;->val$navigationPreferencesUpdate:Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 12

    .prologue
    .line 171
    iget-object v9, p0, Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler$6;->val$navigationPreferencesUpdate:Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate;

    if-eqz v9, :cond_0

    iget-object v9, p0, Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler$6;->val$navigationPreferencesUpdate:Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate;

    iget-object v9, v9, Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate;->preferences:Lcom/navdy/service/library/events/preferences/NavigationPreferences;

    if-nez v9, :cond_2

    .line 172
    :cond_0
    sget-object v9, Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v10, "Received a NavigationPreferencesUpdate with null preferences!"

    invoke-virtual {v9, v10}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 221
    :cond_1
    :goto_0
    return-void

    .line 176
    :cond_2
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v7

    .line 177
    .local v7, "sharedPrefs":Landroid/content/SharedPreferences;
    if-eqz v7, :cond_1

    .line 184
    const-string v9, "nav_serial_number"

    const-wide/16 v10, 0x0

    invoke-interface {v7, v9, v10, v11}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    .line 186
    .local v4, "navSerialNumber":J
    const-wide/16 v10, 0x1

    add-long/2addr v4, v10

    .line 189
    invoke-interface {v7}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    .line 191
    .local v3, "editor":Landroid/content/SharedPreferences$Editor;
    iget-object v9, p0, Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler$6;->val$navigationPreferencesUpdate:Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate;

    iget-object v6, v9, Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate;->preferences:Lcom/navdy/service/library/events/preferences/NavigationPreferences;

    .line 192
    .local v6, "p":Lcom/navdy/service/library/events/preferences/NavigationPreferences;
    iget-object v9, v6, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->routingType:Lcom/navdy/service/library/events/preferences/NavigationPreferences$RoutingType;

    sget-object v10, Lcom/navdy/service/library/events/preferences/NavigationPreferences$RoutingType;->ROUTING_SHORTEST:Lcom/navdy/service/library/events/preferences/NavigationPreferences$RoutingType;

    if-ne v9, v10, :cond_4

    const/4 v8, 0x1

    .line 193
    .local v8, "shortestRouteIsOn":Z
    :goto_1
    iget-object v9, v6, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->rerouteForTraffic:Lcom/navdy/service/library/events/preferences/NavigationPreferences$RerouteForTraffic;

    sget-object v10, Lcom/navdy/service/library/events/preferences/NavigationPreferences$RerouteForTraffic;->REROUTE_AUTOMATIC:Lcom/navdy/service/library/events/preferences/NavigationPreferences$RerouteForTraffic;

    if-ne v9, v10, :cond_5

    const/4 v2, 0x1

    .line 195
    .local v2, "autoRecalcIsOn":Z
    :goto_2
    const-string v9, "nav_serial_number"

    invoke-interface {v3, v9, v4, v5}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 196
    iget-object v9, p0, Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler$6;->this$0:Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler;

    const-string v10, "nav_route_calculation"

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v11

    invoke-static {v9, v3, v10, v11}, Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler;->access$100(Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler;Landroid/content/SharedPreferences$Editor;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 197
    iget-object v9, p0, Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler$6;->this$0:Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler;

    const-string v10, "nav_auto_recalc"

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v11

    invoke-static {v9, v3, v10, v11}, Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler;->access$100(Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler;Landroid/content/SharedPreferences$Editor;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 198
    iget-object v9, p0, Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler$6;->this$0:Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler;

    const-string v10, "nav_highways"

    iget-object v11, v6, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->allowHighways:Ljava/lang/Boolean;

    invoke-static {v9, v3, v10, v11}, Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler;->access$100(Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler;Landroid/content/SharedPreferences$Editor;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 199
    iget-object v9, p0, Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler$6;->this$0:Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler;

    const-string v10, "nav_toll_roads"

    iget-object v11, v6, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->allowTollRoads:Ljava/lang/Boolean;

    invoke-static {v9, v3, v10, v11}, Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler;->access$100(Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler;Landroid/content/SharedPreferences$Editor;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 200
    iget-object v9, p0, Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler$6;->this$0:Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler;

    const-string v10, "nav_ferries"

    iget-object v11, v6, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->allowFerries:Ljava/lang/Boolean;

    invoke-static {v9, v3, v10, v11}, Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler;->access$100(Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler;Landroid/content/SharedPreferences$Editor;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 201
    iget-object v9, p0, Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler$6;->this$0:Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler;

    const-string v10, "nav_tunnels"

    iget-object v11, v6, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->allowTunnels:Ljava/lang/Boolean;

    invoke-static {v9, v3, v10, v11}, Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler;->access$100(Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler;Landroid/content/SharedPreferences$Editor;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 202
    iget-object v9, p0, Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler$6;->this$0:Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler;

    const-string v10, "nav_unpaved_roads"

    iget-object v11, v6, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->allowUnpavedRoads:Ljava/lang/Boolean;

    invoke-static {v9, v3, v10, v11}, Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler;->access$100(Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler;Landroid/content/SharedPreferences$Editor;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 203
    iget-object v9, p0, Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler$6;->this$0:Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler;

    const-string v10, "nav_auto_trains"

    iget-object v11, v6, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->allowAutoTrains:Ljava/lang/Boolean;

    invoke-static {v9, v3, v10, v11}, Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler;->access$100(Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler;Landroid/content/SharedPreferences$Editor;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 204
    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 208
    iget-object v9, v6, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->spokenTurnByTurn:Ljava/lang/Boolean;

    if-nez v9, :cond_3

    iget-object v9, v6, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->spokenSpeedLimitWarnings:Ljava/lang/Boolean;

    if-eqz v9, :cond_1

    .line 210
    :cond_3
    const-string v9, "audio_serial_number"

    const-wide/16 v10, 0x0

    invoke-interface {v7, v9, v10, v11}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    .line 212
    .local v0, "audioSerialNumber":J
    const-wide/16 v10, 0x1

    add-long/2addr v0, v10

    .line 215
    const-string v9, "audio_serial_number"

    invoke-interface {v3, v9, v0, v1}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 216
    iget-object v9, p0, Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler$6;->this$0:Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler;

    const-string v10, "audio_turn_by_turn_instructions"

    iget-object v11, v6, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->spokenTurnByTurn:Ljava/lang/Boolean;

    invoke-static {v9, v3, v10, v11}, Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler;->access$100(Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler;Landroid/content/SharedPreferences$Editor;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 217
    iget-object v9, p0, Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler$6;->this$0:Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler;

    const-string v10, "audio_speed_warnings"

    iget-object v11, v6, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->spokenSpeedLimitWarnings:Ljava/lang/Boolean;

    invoke-static {v9, v3, v10, v11}, Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler;->access$100(Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler;Landroid/content/SharedPreferences$Editor;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 218
    iget-object v9, p0, Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler$6;->this$0:Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler;

    const-string v10, "audio_camera_warnings"

    iget-object v11, v6, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->spokenCameraWarnings:Ljava/lang/Boolean;

    invoke-static {v9, v3, v10, v11}, Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler;->access$100(Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler;Landroid/content/SharedPreferences$Editor;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 219
    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto/16 :goto_0

    .line 192
    .end local v0    # "audioSerialNumber":J
    .end local v2    # "autoRecalcIsOn":Z
    .end local v8    # "shortestRouteIsOn":Z
    :cond_4
    const/4 v8, 0x0

    goto/16 :goto_1

    .line 193
    .restart local v8    # "shortestRouteIsOn":Z
    :cond_5
    const/4 v2, 0x0

    goto/16 :goto_2
.end method
