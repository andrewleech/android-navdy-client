.class Lcom/navdy/client/app/ui/glances/GlancesFragment$8;
.super Ljava/lang/Object;
.source "GlancesFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/glances/GlancesFragment;->showEnableGlancesDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/glances/GlancesFragment;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/glances/GlancesFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/glances/GlancesFragment;

    .prologue
    .line 298
    iput-object p1, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment$8;->this$0:Lcom/navdy/client/app/ui/glances/GlancesFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 3
    .param p1, "dialogInterface"    # Landroid/content/DialogInterface;

    .prologue
    const/4 v2, 0x0

    .line 301
    iget-object v0, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment$8;->this$0:Lcom/navdy/client/app/ui/glances/GlancesFragment;

    invoke-static {v0}, Lcom/navdy/client/app/ui/glances/GlancesFragment;->access$1000(Lcom/navdy/client/app/ui/glances/GlancesFragment;)Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "onCancel"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 302
    iget-object v0, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment$8;->this$0:Lcom/navdy/client/app/ui/glances/GlancesFragment;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/ui/glances/GlancesFragment;->setGlances(Ljava/lang/Boolean;)V

    .line 303
    iget-object v0, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment$8;->this$0:Lcom/navdy/client/app/ui/glances/GlancesFragment;

    invoke-static {v0}, Lcom/navdy/client/app/ui/glances/GlancesFragment;->access$900(Lcom/navdy/client/app/ui/glances/GlancesFragment;)Landroid/widget/Switch;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/Switch;->setChecked(Z)V

    .line 304
    return-void
.end method
