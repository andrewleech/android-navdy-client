.class public Lcom/navdy/client/app/ui/glances/GlancesFragment;
.super Lcom/navdy/client/app/ui/base/BaseSupportFragment;
.source "GlancesFragment.java"


# static fields
.field private static final DISABLED_ALPHA_LEVEL:F = 0.5f

.field public static final VERBOSE:Z


# instance fields
.field private allowGlances:Landroid/widget/Switch;

.field private enableGlancesLearnMoreOnClickListener:Landroid/view/View$OnClickListener;

.field private glancesAreEnabled:Z

.field private glancesAreReadAloud:Z

.field private glancesOnCheckedChangeListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

.field private glancesShowContent:Z

.field private readAloud:Landroid/widget/RadioButton;

.field private readAloudAndShowContent:Landroid/widget/RadioButton;

.field private readAloudAndShowContentLayout:Landroid/widget/LinearLayout;

.field private readAloudAndShowContentOnClickListener:Landroid/view/View$OnClickListener;

.field private readAloudLayout:Landroid/widget/LinearLayout;

.field private rootView:Landroid/view/View;

.field private showContent:Landroid/widget/RadioButton;

.field private showContentLayout:Landroid/widget/LinearLayout;

.field private testGlanceButtonOnClickListener:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 38
    invoke-direct {p0}, Lcom/navdy/client/app/ui/base/BaseSupportFragment;-><init>()V

    .line 44
    iput-boolean v1, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment;->glancesAreEnabled:Z

    .line 45
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment;->glancesAreReadAloud:Z

    .line 46
    iput-boolean v1, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment;->glancesShowContent:Z

    .line 206
    new-instance v0, Lcom/navdy/client/app/ui/glances/GlancesFragment$3;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/ui/glances/GlancesFragment$3;-><init>(Lcom/navdy/client/app/ui/glances/GlancesFragment;)V

    iput-object v0, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment;->glancesOnCheckedChangeListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    .line 217
    new-instance v0, Lcom/navdy/client/app/ui/glances/GlancesFragment$4;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/ui/glances/GlancesFragment$4;-><init>(Lcom/navdy/client/app/ui/glances/GlancesFragment;)V

    iput-object v0, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment;->readAloudAndShowContentOnClickListener:Landroid/view/View$OnClickListener;

    .line 257
    new-instance v0, Lcom/navdy/client/app/ui/glances/GlancesFragment$5;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/ui/glances/GlancesFragment$5;-><init>(Lcom/navdy/client/app/ui/glances/GlancesFragment;)V

    iput-object v0, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment;->enableGlancesLearnMoreOnClickListener:Landroid/view/View$OnClickListener;

    .line 265
    new-instance v0, Lcom/navdy/client/app/ui/glances/GlancesFragment$6;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/ui/glances/GlancesFragment$6;-><init>(Lcom/navdy/client/app/ui/glances/GlancesFragment;)V

    iput-object v0, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment;->testGlanceButtonOnClickListener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method static synthetic access$000(Lcom/navdy/client/app/ui/glances/GlancesFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/glances/GlancesFragment;

    .prologue
    .line 38
    iget-boolean v0, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment;->glancesAreEnabled:Z

    return v0
.end method

.method static synthetic access$100(Lcom/navdy/client/app/ui/glances/GlancesFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/glances/GlancesFragment;

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/navdy/client/app/ui/glances/GlancesFragment;->increaseSerialAndSendAllGlancesPreferencesToHud()V

    return-void
.end method

.method static synthetic access$1000(Lcom/navdy/client/app/ui/glances/GlancesFragment;)Lcom/navdy/service/library/log/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/glances/GlancesFragment;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/navdy/client/app/ui/glances/GlancesFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/glances/GlancesFragment;

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/navdy/client/app/ui/glances/GlancesFragment;->initAllSwitches()V

    return-void
.end method

.method static synthetic access$200(Lcom/navdy/client/app/ui/glances/GlancesFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/glances/GlancesFragment;

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/navdy/client/app/ui/glances/GlancesFragment;->showEnableGlancesDialog()V

    return-void
.end method

.method static synthetic access$300(Lcom/navdy/client/app/ui/glances/GlancesFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/glances/GlancesFragment;

    .prologue
    .line 38
    iget-boolean v0, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment;->glancesAreReadAloud:Z

    return v0
.end method

.method static synthetic access$302(Lcom/navdy/client/app/ui/glances/GlancesFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/glances/GlancesFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 38
    iput-boolean p1, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment;->glancesAreReadAloud:Z

    return p1
.end method

.method static synthetic access$400(Lcom/navdy/client/app/ui/glances/GlancesFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/glances/GlancesFragment;

    .prologue
    .line 38
    iget-boolean v0, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment;->glancesShowContent:Z

    return v0
.end method

.method static synthetic access$402(Lcom/navdy/client/app/ui/glances/GlancesFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/glances/GlancesFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 38
    iput-boolean p1, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment;->glancesShowContent:Z

    return p1
.end method

.method static synthetic access$500(Lcom/navdy/client/app/ui/glances/GlancesFragment;)Landroid/widget/RadioButton;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/glances/GlancesFragment;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment;->readAloud:Landroid/widget/RadioButton;

    return-object v0
.end method

.method static synthetic access$600(Lcom/navdy/client/app/ui/glances/GlancesFragment;)Landroid/widget/RadioButton;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/glances/GlancesFragment;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment;->showContent:Landroid/widget/RadioButton;

    return-object v0
.end method

.method static synthetic access$700(Lcom/navdy/client/app/ui/glances/GlancesFragment;)Landroid/widget/RadioButton;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/glances/GlancesFragment;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment;->readAloudAndShowContent:Landroid/widget/RadioButton;

    return-object v0
.end method

.method static synthetic access$800(Lcom/navdy/client/app/ui/glances/GlancesFragment;)Lcom/navdy/service/library/log/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/glances/GlancesFragment;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$900(Lcom/navdy/client/app/ui/glances/GlancesFragment;)Landroid/widget/Switch;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/glances/GlancesFragment;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment;->allowGlances:Landroid/widget/Switch;

    return-object v0
.end method

.method private getGlanceSwitchClickListener(Ljava/lang/String;)Landroid/view/View$OnClickListener;
    .locals 1
    .param p1, "pkg"    # Ljava/lang/String;

    .prologue
    .line 193
    new-instance v0, Lcom/navdy/client/app/ui/glances/GlancesFragment$2;

    invoke-direct {v0, p0, p1}, Lcom/navdy/client/app/ui/glances/GlancesFragment$2;-><init>(Lcom/navdy/client/app/ui/glances/GlancesFragment;Ljava/lang/String;)V

    return-object v0
.end method

.method private increaseSerialAndSendAllGlancesPreferencesToHud()V
    .locals 6

    .prologue
    .line 276
    const-string v1, "glances_serial_number"

    invoke-static {v1}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->incrementSerialNumber(Ljava/lang/String;)J

    move-result-wide v2

    .line 277
    .local v2, "serialNumber":J
    iget-boolean v1, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment;->glancesAreEnabled:Z

    iget-boolean v4, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment;->glancesAreReadAloud:Z

    iget-boolean v5, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment;->glancesShowContent:Z

    invoke-static {v2, v3, v1, v4, v5}, Lcom/navdy/client/app/ui/glances/GlanceUtils;->buildGlancesPreferences(JZZZ)Lcom/navdy/service/library/events/preferences/NotificationPreferences;

    move-result-object v0

    .line 280
    .local v0, "notifPrefs":Lcom/navdy/service/library/events/preferences/NotificationPreferences;
    invoke-static {v0}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->sendGlancesSettingsToTheHud(Lcom/navdy/service/library/events/preferences/NotificationPreferences;)Z

    .line 281
    return-void
.end method

.method private initAllSwitches()V
    .locals 8

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    const/high16 v5, 0x3f800000    # 1.0f

    const/high16 v6, 0x3f000000    # 0.5f

    .line 139
    iget-object v7, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment;->readAloud:Landroid/widget/RadioButton;

    iget-boolean v2, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment;->glancesAreReadAloud:Z

    if-eqz v2, :cond_2

    iget-boolean v2, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment;->glancesShowContent:Z

    if-nez v2, :cond_2

    move v2, v3

    :goto_0
    invoke-virtual {v7, v2}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 140
    iget-object v2, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment;->readAloud:Landroid/widget/RadioButton;

    iget-boolean v7, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment;->glancesAreEnabled:Z

    invoke-virtual {v2, v7}, Landroid/widget/RadioButton;->setEnabled(Z)V

    .line 141
    iget-object v7, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment;->readAloudLayout:Landroid/widget/LinearLayout;

    iget-boolean v2, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment;->glancesAreEnabled:Z

    if-eqz v2, :cond_3

    move v2, v5

    :goto_1
    invoke-virtual {v7, v2}, Landroid/widget/LinearLayout;->setAlpha(F)V

    .line 142
    iget-object v7, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment;->showContent:Landroid/widget/RadioButton;

    iget-boolean v2, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment;->glancesAreReadAloud:Z

    if-nez v2, :cond_4

    iget-boolean v2, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment;->glancesShowContent:Z

    if-eqz v2, :cond_4

    move v2, v3

    :goto_2
    invoke-virtual {v7, v2}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 143
    iget-object v2, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment;->showContent:Landroid/widget/RadioButton;

    iget-boolean v7, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment;->glancesAreEnabled:Z

    invoke-virtual {v2, v7}, Landroid/widget/RadioButton;->setEnabled(Z)V

    .line 144
    iget-object v7, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment;->showContentLayout:Landroid/widget/LinearLayout;

    iget-boolean v2, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment;->glancesAreEnabled:Z

    if-eqz v2, :cond_5

    move v2, v5

    :goto_3
    invoke-virtual {v7, v2}, Landroid/widget/LinearLayout;->setAlpha(F)V

    .line 145
    iget-object v2, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment;->readAloudAndShowContent:Landroid/widget/RadioButton;

    iget-boolean v7, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment;->glancesAreReadAloud:Z

    if-eqz v7, :cond_6

    iget-boolean v7, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment;->glancesShowContent:Z

    if-eqz v7, :cond_6

    :goto_4
    invoke-virtual {v2, v3}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 146
    iget-object v2, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment;->readAloudAndShowContent:Landroid/widget/RadioButton;

    iget-boolean v3, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment;->glancesAreEnabled:Z

    invoke-virtual {v2, v3}, Landroid/widget/RadioButton;->setEnabled(Z)V

    .line 147
    iget-object v3, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment;->readAloudAndShowContentLayout:Landroid/widget/LinearLayout;

    iget-boolean v2, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment;->glancesAreEnabled:Z

    if-eqz v2, :cond_7

    move v2, v5

    :goto_5
    invoke-virtual {v3, v2}, Landroid/widget/LinearLayout;->setAlpha(F)V

    .line 150
    const v2, 0x7f1002cb

    const-string v3, "com.navdy.phone"

    invoke-direct {p0, v2, v3}, Lcom/navdy/client/app/ui/glances/GlancesFragment;->initSwitch(ILjava/lang/String;)V

    .line 151
    const v2, 0x7f1002e1

    const-string v3, "com.navdy.sms"

    invoke-direct {p0, v2, v3}, Lcom/navdy/client/app/ui/glances/GlancesFragment;->initSwitch(ILjava/lang/String;)V

    .line 152
    const v2, 0x7f1002cd

    const-string v3, "com.navdy.fuel"

    invoke-direct {p0, v2, v3}, Lcom/navdy/client/app/ui/glances/GlancesFragment;->initSwitch(ILjava/lang/String;)V

    .line 153
    const v2, 0x7f1002cf

    const-string v3, "com.navdy.music"

    invoke-direct {p0, v2, v3}, Lcom/navdy/client/app/ui/glances/GlancesFragment;->initSwitch(ILjava/lang/String;)V

    .line 154
    const v2, 0x7f1002d1

    const-string v3, "com.navdy.traffic"

    invoke-direct {p0, v2, v3}, Lcom/navdy/client/app/ui/glances/GlancesFragment;->initSwitch(ILjava/lang/String;)V

    .line 157
    const v2, 0x7f1002d7

    const-string v3, "com.google.android.gm"

    invoke-direct {p0, v2, v3}, Lcom/navdy/client/app/ui/glances/GlancesFragment;->initSwitch(ILjava/lang/String;)V

    .line 158
    const v2, 0x7f1002d9

    const-string v3, "com.google.android.calendar"

    invoke-direct {p0, v2, v3}, Lcom/navdy/client/app/ui/glances/GlancesFragment;->initSwitch(ILjava/lang/String;)V

    .line 159
    const v2, 0x7f1002db

    const-string v3, "com.google.android.talk"

    invoke-direct {p0, v2, v3}, Lcom/navdy/client/app/ui/glances/GlancesFragment;->initSwitch(ILjava/lang/String;)V

    .line 160
    const v2, 0x7f1002df

    const-string v3, "com.Slack"

    invoke-direct {p0, v2, v3}, Lcom/navdy/client/app/ui/glances/GlancesFragment;->initSwitch(ILjava/lang/String;)V

    .line 161
    const v2, 0x7f1002e5

    const-string v3, "com.whatsapp"

    invoke-direct {p0, v2, v3}, Lcom/navdy/client/app/ui/glances/GlancesFragment;->initSwitch(ILjava/lang/String;)V

    .line 162
    const v2, 0x7f1002dd

    const-string v3, "com.facebook.orca"

    invoke-direct {p0, v2, v3}, Lcom/navdy/client/app/ui/glances/GlancesFragment;->initSwitch(ILjava/lang/String;)V

    .line 163
    const v2, 0x7f1002d5

    const-string v3, "com.facebook.katana"

    invoke-direct {p0, v2, v3}, Lcom/navdy/client/app/ui/glances/GlancesFragment;->initSwitch(ILjava/lang/String;)V

    .line 164
    const v2, 0x7f1002e3

    const-string v3, "com.twitter.android"

    invoke-direct {p0, v2, v3}, Lcom/navdy/client/app/ui/glances/GlancesFragment;->initSwitch(ILjava/lang/String;)V

    .line 170
    iget-object v2, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment;->rootView:Landroid/view/View;

    const v3, 0x7f1002e6

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    .line 171
    .local v0, "notificationGlances":Landroid/widget/RelativeLayout;
    if-eqz v0, :cond_0

    .line 172
    iget-boolean v2, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment;->glancesAreEnabled:Z

    if-eqz v2, :cond_8

    move v2, v5

    :goto_6
    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setAlpha(F)V

    .line 175
    :cond_0
    iget-object v2, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment;->rootView:Landroid/view/View;

    const v3, 0x7f1002e8

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 176
    .local v1, "otherGlances":Landroid/widget/TextView;
    if-eqz v1, :cond_1

    .line 177
    iget-boolean v2, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment;->glancesAreEnabled:Z

    if-eqz v2, :cond_9

    :goto_7
    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setAlpha(F)V

    .line 179
    :cond_1
    return-void

    .end local v0    # "notificationGlances":Landroid/widget/RelativeLayout;
    .end local v1    # "otherGlances":Landroid/widget/TextView;
    :cond_2
    move v2, v4

    .line 139
    goto/16 :goto_0

    :cond_3
    move v2, v6

    .line 141
    goto/16 :goto_1

    :cond_4
    move v2, v4

    .line 142
    goto/16 :goto_2

    :cond_5
    move v2, v6

    .line 144
    goto/16 :goto_3

    :cond_6
    move v3, v4

    .line 145
    goto/16 :goto_4

    :cond_7
    move v2, v6

    .line 147
    goto/16 :goto_5

    .restart local v0    # "notificationGlances":Landroid/widget/RelativeLayout;
    :cond_8
    move v2, v6

    .line 172
    goto :goto_6

    .restart local v1    # "otherGlances":Landroid/widget/TextView;
    :cond_9
    move v5, v6

    .line 177
    goto :goto_7
.end method

.method private initSwitch(ILjava/lang/String;)V
    .locals 3
    .param p1, "switchId"    # I
    .param p2, "appPackage"    # Ljava/lang/String;

    .prologue
    .line 182
    iget-object v2, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment;->rootView:Landroid/view/View;

    invoke-virtual {v2, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Switch;

    .line 183
    .local v0, "aSwitch":Landroid/widget/Switch;
    invoke-static {p2}, Lcom/navdy/client/app/ui/glances/GlanceUtils;->isThisGlanceEnabledInItself(Ljava/lang/String;)Z

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/Switch;->setChecked(Z)V

    .line 184
    iget-boolean v2, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment;->glancesAreEnabled:Z

    invoke-virtual {v0, v2}, Landroid/widget/Switch;->setEnabled(Z)V

    .line 185
    invoke-virtual {v0}, Landroid/widget/Switch;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    .line 186
    .local v1, "parent":Landroid/view/ViewParent;
    if-eqz v1, :cond_0

    instance-of v2, v1, Landroid/view/View;

    if-eqz v2, :cond_0

    .line 187
    check-cast v1, Landroid/view/View;

    .end local v1    # "parent":Landroid/view/ViewParent;
    iget-boolean v2, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment;->glancesAreEnabled:Z

    if-eqz v2, :cond_1

    const/high16 v2, 0x3f800000    # 1.0f

    :goto_0
    invoke-virtual {v1, v2}, Landroid/view/View;->setAlpha(F)V

    .line 189
    :cond_0
    invoke-direct {p0, p2}, Lcom/navdy/client/app/ui/glances/GlancesFragment;->getGlanceSwitchClickListener(Ljava/lang/String;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/Switch;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 190
    return-void

    .line 187
    :cond_1
    const/high16 v2, 0x3f000000    # 0.5f

    goto :goto_0
.end method

.method private showEnableGlancesDialog()V
    .locals 7

    .prologue
    .line 284
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/glances/GlancesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    .line 285
    .local v0, "homescreenActivity":Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;
    if-eqz v0, :cond_0

    .line 286
    const v1, 0x7f0804f2

    const v2, 0x7f0801e1

    const v3, 0x7f080155

    const v4, 0x7f0800e5

    new-instance v5, Lcom/navdy/client/app/ui/glances/GlancesFragment$7;

    invoke-direct {v5, p0}, Lcom/navdy/client/app/ui/glances/GlancesFragment$7;-><init>(Lcom/navdy/client/app/ui/glances/GlancesFragment;)V

    new-instance v6, Lcom/navdy/client/app/ui/glances/GlancesFragment$8;

    invoke-direct {v6, p0}, Lcom/navdy/client/app/ui/glances/GlancesFragment$8;-><init>(Lcom/navdy/client/app/ui/glances/GlancesFragment;)V

    invoke-virtual/range {v0 .. v6}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->showQuestionDialog(IIIILandroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnCancelListener;)V

    .line 307
    :cond_0
    return-void
.end method

.method private startHighlightAnimation(Landroid/view/View;)V
    .locals 4
    .param p1, "glances"    # Landroid/view/View;

    .prologue
    .line 128
    if-eqz p1, :cond_0

    .line 129
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const v1, 0x3dcccccd    # 0.1f

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 130
    .local v0, "alphaAnimation":Landroid/view/animation/AlphaAnimation;
    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 131
    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setRepeatCount(I)V

    .line 132
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setRepeatMode(I)V

    .line 133
    invoke-virtual {p1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 135
    .end local v0    # "alphaAnimation":Landroid/view/animation/AlphaAnimation;
    :cond_0
    return-void
.end method


# virtual methods
.method public highlightGlanceSwitch()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 117
    iget-object v1, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment;->rootView:Landroid/view/View;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment;->baseActivity:Lcom/navdy/client/app/ui/base/BaseActivity;

    if-eqz v1, :cond_0

    .line 119
    iget-object v1, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment;->rootView:Landroid/view/View;

    invoke-virtual {v1, v2, v2}, Landroid/view/View;->scrollTo(II)V

    .line 122
    iget-object v1, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment;->rootView:Landroid/view/View;

    const v2, 0x7f1002bc

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 123
    .local v0, "glances":Landroid/view/View;
    invoke-direct {p0, v0}, Lcom/navdy/client/app/ui/glances/GlancesFragment;->startHighlightAnimation(Landroid/view/View;)V

    .line 125
    .end local v0    # "glances":Landroid/view/View;
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v6, 0x0

    .line 58
    const v4, 0x7f0300ad

    invoke-virtual {p1, v4, p2, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment;->rootView:Landroid/view/View;

    .line 60
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v2

    .line 61
    .local v2, "sharedPrefs":Landroid/content/SharedPreferences;
    if-eqz v2, :cond_0

    .line 62
    const-string v4, "glances"

    invoke-interface {v2, v4, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    iput-boolean v4, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment;->glancesAreEnabled:Z

    .line 64
    const-string v4, "glances_read_aloud"

    const/4 v5, 0x1

    invoke-interface {v2, v4, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    iput-boolean v4, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment;->glancesAreReadAloud:Z

    .line 66
    const-string v4, "glances_show_content"

    invoke-interface {v2, v4, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    iput-boolean v4, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment;->glancesShowContent:Z

    .line 71
    :cond_0
    iget-object v4, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment;->rootView:Landroid/view/View;

    const v5, 0x7f1002bc

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Switch;

    iput-object v4, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment;->allowGlances:Landroid/widget/Switch;

    .line 72
    iget-object v4, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment;->allowGlances:Landroid/widget/Switch;

    iget-boolean v5, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment;->glancesAreEnabled:Z

    invoke-virtual {v4, v5}, Landroid/widget/Switch;->setChecked(Z)V

    .line 73
    iget-object v4, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment;->allowGlances:Landroid/widget/Switch;

    iget-object v5, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment;->glancesOnCheckedChangeListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v4, v5}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 75
    iget-object v4, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment;->rootView:Landroid/view/View;

    const v5, 0x7f1002bf

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    iput-object v4, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment;->readAloudLayout:Landroid/widget/LinearLayout;

    .line 76
    iget-object v4, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment;->rootView:Landroid/view/View;

    const v5, 0x7f1002c1

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/RadioButton;

    iput-object v4, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment;->readAloud:Landroid/widget/RadioButton;

    .line 77
    iget-object v4, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment;->readAloudLayout:Landroid/widget/LinearLayout;

    iget-object v5, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment;->readAloudAndShowContentOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 78
    iget-object v4, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment;->readAloud:Landroid/widget/RadioButton;

    iget-object v5, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment;->readAloudAndShowContentOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v4, v5}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 80
    iget-object v4, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment;->rootView:Landroid/view/View;

    const v5, 0x7f1002c2

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    iput-object v4, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment;->showContentLayout:Landroid/widget/LinearLayout;

    .line 81
    iget-object v4, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment;->rootView:Landroid/view/View;

    const v5, 0x7f1002c4

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/RadioButton;

    iput-object v4, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment;->showContent:Landroid/widget/RadioButton;

    .line 82
    iget-object v4, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment;->showContentLayout:Landroid/widget/LinearLayout;

    iget-object v5, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment;->readAloudAndShowContentOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 83
    iget-object v4, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment;->showContent:Landroid/widget/RadioButton;

    iget-object v5, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment;->readAloudAndShowContentOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v4, v5}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 85
    iget-object v4, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment;->rootView:Landroid/view/View;

    const v5, 0x7f1002c5

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    iput-object v4, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment;->readAloudAndShowContentLayout:Landroid/widget/LinearLayout;

    .line 86
    iget-object v4, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment;->rootView:Landroid/view/View;

    const v5, 0x7f1002c7

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/RadioButton;

    iput-object v4, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment;->readAloudAndShowContent:Landroid/widget/RadioButton;

    .line 87
    iget-object v4, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment;->readAloudAndShowContentLayout:Landroid/widget/LinearLayout;

    iget-object v5, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment;->readAloudAndShowContentOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 88
    iget-object v4, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment;->readAloudAndShowContent:Landroid/widget/RadioButton;

    iget-object v5, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment;->readAloudAndShowContentOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v4, v5}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 91
    iget-object v4, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment;->rootView:Landroid/view/View;

    const v5, 0x7f1002bd

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 92
    .local v0, "glancesSwitchDescription":Landroid/widget/TextView;
    const v4, 0x7f0800b8

    invoke-static {v4}, Lcom/navdy/client/app/framework/util/StringUtils;->fromHtml(I)Landroid/text/Spanned;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 93
    iget-object v4, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment;->enableGlancesLearnMoreOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 96
    iget-object v4, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment;->rootView:Landroid/view/View;

    const v5, 0x7f1002be

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 97
    .local v3, "testGlanceButton":Landroid/widget/TextView;
    iget-object v4, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment;->testGlanceButtonOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 99
    invoke-direct {p0}, Lcom/navdy/client/app/ui/glances/GlancesFragment;->initAllSwitches()V

    .line 101
    iget-object v4, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment;->rootView:Landroid/view/View;

    const v5, 0x7f1002e6

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    .line 102
    .local v1, "notificationGlances":Landroid/widget/RelativeLayout;
    if-eqz v1, :cond_1

    .line 103
    new-instance v4, Lcom/navdy/client/app/ui/glances/GlancesFragment$1;

    invoke-direct {v4, p0}, Lcom/navdy/client/app/ui/glances/GlancesFragment$1;-><init>(Lcom/navdy/client/app/ui/glances/GlancesFragment;)V

    invoke-virtual {v1, v4}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 113
    :cond_1
    iget-object v4, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment;->rootView:Landroid/view/View;

    return-object v4
.end method

.method public setGlances(Ljava/lang/Boolean;)V
    .locals 7
    .param p1, "glancesAreEnabled"    # Ljava/lang/Boolean;

    .prologue
    const/4 v6, 0x0

    .line 314
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    iput-boolean v3, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment;->glancesAreEnabled:Z

    .line 316
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 318
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    .line 319
    .local v1, "sharedPreferences":Landroid/content/SharedPreferences;
    const-string v3, "user_enabled_glances_once_before"

    .line 320
    invoke-interface {v1, v3, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    .line 322
    .local v2, "userEnabledGlancesOnceBefore":Z
    if-nez v2, :cond_0

    .line 324
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string v4, "user_enabled_glances_once_before"

    const/4 v5, 0x1

    .line 325
    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    .line 326
    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 328
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/glances/GlancesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 329
    .local v0, "activity":Landroid/support/v4/app/FragmentActivity;
    instance-of v3, v0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    if-eqz v3, :cond_0

    .line 330
    check-cast v0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    .end local v0    # "activity":Landroid/support/v4/app/FragmentActivity;
    invoke-virtual {v0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->rebuildSuggestions()V

    .line 335
    .end local v1    # "sharedPreferences":Landroid/content/SharedPreferences;
    .end local v2    # "userEnabledGlancesOnceBefore":Z
    :cond_0
    new-instance v3, Lcom/navdy/client/app/ui/glances/GlancesFragment$9;

    invoke-direct {v3, p0, p1}, Lcom/navdy/client/app/ui/glances/GlancesFragment$9;-><init>(Lcom/navdy/client/app/ui/glances/GlancesFragment;Ljava/lang/Boolean;)V

    new-array v4, v6, [Ljava/lang/Void;

    .line 350
    invoke-virtual {v3, v4}, Lcom/navdy/client/app/ui/glances/GlancesFragment$9;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 351
    return-void
.end method
