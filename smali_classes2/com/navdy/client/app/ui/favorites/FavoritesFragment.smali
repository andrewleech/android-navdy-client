.class public Lcom/navdy/client/app/ui/favorites/FavoritesFragment;
.super Lcom/navdy/client/app/ui/base/BaseSupportFragment;
.source "FavoritesFragment.java"


# instance fields
.field public favoritesAdapter:Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;

.field private imageCache:Lcom/navdy/client/app/framework/util/ImageCache;

.field private final navdyRouteHandler:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

.field public recyclerView:Landroid/support/v7/widget/RecyclerView;

.field public splash:Landroid/widget/RelativeLayout;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/navdy/client/app/ui/base/BaseSupportFragment;-><init>()V

    .line 40
    new-instance v0, Lcom/navdy/client/app/framework/util/ImageCache;

    invoke-direct {v0}, Lcom/navdy/client/app/framework/util/ImageCache;-><init>()V

    iput-object v0, p0, Lcom/navdy/client/app/ui/favorites/FavoritesFragment;->imageCache:Lcom/navdy/client/app/framework/util/ImageCache;

    .line 43
    invoke-static {}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->getInstance()Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/ui/favorites/FavoritesFragment;->navdyRouteHandler:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

    .line 44
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/client/app/ui/favorites/FavoritesFragment;)Lcom/navdy/service/library/log/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/favorites/FavoritesFragment;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/navdy/client/app/ui/favorites/FavoritesFragment;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$100(Lcom/navdy/client/app/ui/favorites/FavoritesFragment;)Lcom/navdy/service/library/log/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/favorites/FavoritesFragment;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/navdy/client/app/ui/favorites/FavoritesFragment;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/client/app/ui/favorites/FavoritesFragment;)Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/favorites/FavoritesFragment;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/navdy/client/app/ui/favorites/FavoritesFragment;->navdyRouteHandler:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

    return-object v0
.end method


# virtual methods
.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 48
    const v6, 0x7f0300ac

    const/4 v7, 0x0

    invoke-virtual {p1, v6, p2, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v5

    .line 50
    .local v5, "rootView":Landroid/view/View;
    const v6, 0x7f1002bb

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/support/v7/widget/RecyclerView;

    iput-object v6, p0, Lcom/navdy/client/app/ui/favorites/FavoritesFragment;->recyclerView:Landroid/support/v7/widget/RecyclerView;

    .line 52
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/favorites/FavoritesFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 53
    .local v0, "context":Landroid/content/Context;
    new-instance v4, Landroid/support/v7/widget/LinearLayoutManager;

    invoke-direct {v4, v0}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    .line 54
    .local v4, "layoutManager":Landroid/support/v7/widget/LinearLayoutManager;
    const/4 v6, 0x1

    invoke-virtual {v4, v6}, Landroid/support/v7/widget/LinearLayoutManager;->setOrientation(I)V

    .line 55
    iget-object v6, p0, Lcom/navdy/client/app/ui/favorites/FavoritesFragment;->recyclerView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v6, v4}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 56
    new-instance v6, Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;

    invoke-direct {v6, v0}, Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;-><init>(Landroid/content/Context;)V

    iput-object v6, p0, Lcom/navdy/client/app/ui/favorites/FavoritesFragment;->favoritesAdapter:Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;

    .line 57
    iget-object v6, p0, Lcom/navdy/client/app/ui/favorites/FavoritesFragment;->recyclerView:Landroid/support/v7/widget/RecyclerView;

    iget-object v7, p0, Lcom/navdy/client/app/ui/favorites/FavoritesFragment;->favoritesAdapter:Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;

    invoke-virtual {v6, v7}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 59
    iget-object v6, p0, Lcom/navdy/client/app/ui/favorites/FavoritesFragment;->favoritesAdapter:Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;

    new-instance v7, Lcom/navdy/client/app/ui/favorites/FavoritesFragment$1;

    invoke-direct {v7, p0}, Lcom/navdy/client/app/ui/favorites/FavoritesFragment$1;-><init>(Lcom/navdy/client/app/ui/favorites/FavoritesFragment;)V

    invoke-virtual {v6, v7}, Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;->setClickListener(Landroid/view/View$OnClickListener;)V

    .line 101
    new-instance v3, Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter$ItemTouchHelperCallback;

    iget-object v6, p0, Lcom/navdy/client/app/ui/favorites/FavoritesFragment;->favoritesAdapter:Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;

    invoke-direct {v3, v6}, Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter$ItemTouchHelperCallback;-><init>(Lcom/navdy/client/app/ui/favorites/ItemTouchHelperAdapter;)V

    .line 103
    .local v3, "itemTouchHelperCallback":Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter$ItemTouchHelperCallback;
    new-instance v2, Landroid/support/v7/widget/helper/ItemTouchHelper;

    invoke-direct {v2, v3}, Landroid/support/v7/widget/helper/ItemTouchHelper;-><init>(Landroid/support/v7/widget/helper/ItemTouchHelper$Callback;)V

    .line 104
    .local v2, "itemTouchHelper":Landroid/support/v7/widget/helper/ItemTouchHelper;
    iget-object v6, p0, Lcom/navdy/client/app/ui/favorites/FavoritesFragment;->recyclerView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2, v6}, Landroid/support/v7/widget/helper/ItemTouchHelper;->attachToRecyclerView(Landroid/support/v7/widget/RecyclerView;)V

    .line 105
    const v6, 0x7f1002b9

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/RelativeLayout;

    iput-object v6, p0, Lcom/navdy/client/app/ui/favorites/FavoritesFragment;->splash:Landroid/widget/RelativeLayout;

    .line 106
    const v6, 0x7f1000b1

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 107
    .local v1, "illustration":Landroid/widget/ImageView;
    if-eqz v1, :cond_0

    .line 108
    const v6, 0x7f0201e1

    iget-object v7, p0, Lcom/navdy/client/app/ui/favorites/FavoritesFragment;->imageCache:Lcom/navdy/client/app/framework/util/ImageCache;

    invoke-static {v1, v6, v7}, Lcom/navdy/client/app/framework/util/ImageUtils;->loadImage(Landroid/widget/ImageView;ILcom/navdy/client/app/framework/util/ImageCache;)V

    .line 111
    :cond_0
    return-object v5
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 125
    invoke-super {p0}, Lcom/navdy/client/app/ui/base/BaseSupportFragment;->onDestroy()V

    .line 126
    iget-object v0, p0, Lcom/navdy/client/app/ui/favorites/FavoritesFragment;->favoritesAdapter:Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;

    if-eqz v0, :cond_0

    .line 127
    iget-object v0, p0, Lcom/navdy/client/app/ui/favorites/FavoritesFragment;->favoritesAdapter:Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;

    invoke-virtual {v0}, Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;->close()V

    .line 129
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/ui/favorites/FavoritesFragment;->imageCache:Lcom/navdy/client/app/framework/util/ImageCache;

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/util/ImageCache;->clearCache()V

    .line 130
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 116
    invoke-super {p0}, Lcom/navdy/client/app/ui/base/BaseSupportFragment;->onResume()V

    .line 117
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/favorites/FavoritesFragment;->showOrHideSplashScreen()V

    .line 118
    iget-object v0, p0, Lcom/navdy/client/app/ui/favorites/FavoritesFragment;->favoritesAdapter:Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;

    if-eqz v0, :cond_0

    .line 119
    iget-object v0, p0, Lcom/navdy/client/app/ui/favorites/FavoritesFragment;->favoritesAdapter:Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;

    invoke-virtual {v0}, Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;->setupDynamicShortcuts()V

    .line 121
    :cond_0
    return-void
.end method

.method public showOrHideSplashScreen()V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 133
    invoke-static {p0}, Lcom/navdy/client/app/ui/favorites/FavoritesFragment;->isEnding(Landroid/support/v4/app/Fragment;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 150
    :cond_0
    :goto_0
    return-void

    .line 137
    :cond_1
    const/4 v0, 0x0

    .line 138
    .local v0, "itemCount":I
    iget-object v1, p0, Lcom/navdy/client/app/ui/favorites/FavoritesFragment;->favoritesAdapter:Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;

    if-eqz v1, :cond_2

    .line 139
    iget-object v1, p0, Lcom/navdy/client/app/ui/favorites/FavoritesFragment;->favoritesAdapter:Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;

    invoke-virtual {v1}, Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;->getItemCount()I

    move-result v0

    .line 141
    :cond_2
    iget-object v1, p0, Lcom/navdy/client/app/ui/favorites/FavoritesFragment;->recyclerView:Landroid/support/v7/widget/RecyclerView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/navdy/client/app/ui/favorites/FavoritesFragment;->splash:Landroid/widget/RelativeLayout;

    if-eqz v1, :cond_0

    .line 142
    if-lez v0, :cond_3

    .line 143
    iget-object v1, p0, Lcom/navdy/client/app/ui/favorites/FavoritesFragment;->recyclerView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 144
    iget-object v1, p0, Lcom/navdy/client/app/ui/favorites/FavoritesFragment;->splash:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_0

    .line 146
    :cond_3
    iget-object v1, p0, Lcom/navdy/client/app/ui/favorites/FavoritesFragment;->recyclerView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v3}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 147
    iget-object v1, p0, Lcom/navdy/client/app/ui/favorites/FavoritesFragment;->splash:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_0
.end method
