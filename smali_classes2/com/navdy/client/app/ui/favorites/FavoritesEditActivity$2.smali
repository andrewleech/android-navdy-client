.class Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity$2;
.super Ljava/lang/Object;
.source "FavoritesEditActivity.java"

# interfaces
.implements Lcom/google/android/gms/maps/OnMapReadyCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;

    .prologue
    .line 143
    iput-object p1, p0, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity$2;->this$0:Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMapReady(Lcom/google/android/gms/maps/GoogleMap;)V
    .locals 10
    .param p1, "googleMap"    # Lcom/google/android/gms/maps/GoogleMap;

    .prologue
    const/high16 v9, 0x41600000    # 14.0f

    const/4 v8, 0x0

    .line 146
    const/4 v3, 0x0

    .line 147
    .local v3, "position":Lcom/google/android/gms/maps/model/LatLng;
    iget-object v4, p0, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity$2;->this$0:Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;

    invoke-static {v4}, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;->access$000(Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;)Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 148
    iget-object v4, p0, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity$2;->this$0:Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;

    invoke-static {v4}, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;->access$000(Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;)Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v4

    invoke-virtual {v4}, Lcom/navdy/client/app/framework/models/Destination;->hasValidDisplayCoordinates()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 149
    new-instance v3, Lcom/google/android/gms/maps/model/LatLng;

    .end local v3    # "position":Lcom/google/android/gms/maps/model/LatLng;
    iget-object v4, p0, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity$2;->this$0:Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;

    invoke-static {v4}, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;->access$000(Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;)Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v4

    iget-wide v4, v4, Lcom/navdy/client/app/framework/models/Destination;->displayLat:D

    iget-object v6, p0, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity$2;->this$0:Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;

    invoke-static {v6}, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;->access$000(Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;)Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v6

    iget-wide v6, v6, Lcom/navdy/client/app/framework/models/Destination;->displayLong:D

    invoke-direct {v3, v4, v5, v6, v7}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    .line 155
    .restart local v3    # "position":Lcom/google/android/gms/maps/model/LatLng;
    :cond_0
    :goto_0
    if-eqz v3, :cond_2

    .line 156
    iget-object v4, p0, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity$2;->this$0:Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;

    invoke-static {v4}, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;->access$200(Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;)Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    move-result-object v4

    invoke-virtual {v4, v3, v9, v8}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->moveMap(Lcom/google/android/gms/maps/model/LatLng;FZ)V

    .line 159
    iget-object v4, p0, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity$2;->this$0:Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;

    invoke-static {v4}, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;->access$000(Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;)Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v4

    invoke-virtual {v4}, Lcom/navdy/client/app/framework/models/Destination;->getPinAsset()I

    move-result v2

    .line 160
    .local v2, "pinAsset":I
    new-instance v1, Lcom/google/android/gms/maps/model/MarkerOptions;

    invoke-direct {v1}, Lcom/google/android/gms/maps/model/MarkerOptions;-><init>()V

    .line 161
    .local v1, "markerOptions":Lcom/google/android/gms/maps/model/MarkerOptions;
    iget-object v4, p0, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity$2;->this$0:Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;

    invoke-static {v4}, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;->access$000(Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;)Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v4

    invoke-virtual {v4}, Lcom/navdy/client/app/framework/models/Destination;->getFavoriteLabel()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/google/android/gms/maps/model/MarkerOptions;->title(Ljava/lang/String;)Lcom/google/android/gms/maps/model/MarkerOptions;

    .line 162
    invoke-virtual {v1, v8}, Lcom/google/android/gms/maps/model/MarkerOptions;->draggable(Z)Lcom/google/android/gms/maps/model/MarkerOptions;

    .line 163
    invoke-virtual {v1, v3}, Lcom/google/android/gms/maps/model/MarkerOptions;->position(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/MarkerOptions;

    .line 164
    invoke-static {v2}, Lcom/google/android/gms/maps/model/BitmapDescriptorFactory;->fromResource(I)Lcom/google/android/gms/maps/model/BitmapDescriptor;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/google/android/gms/maps/model/MarkerOptions;->icon(Lcom/google/android/gms/maps/model/BitmapDescriptor;)Lcom/google/android/gms/maps/model/MarkerOptions;

    .line 165
    invoke-virtual {p1, v1}, Lcom/google/android/gms/maps/GoogleMap;->addMarker(Lcom/google/android/gms/maps/model/MarkerOptions;)Lcom/google/android/gms/maps/model/Marker;

    .line 167
    new-instance v0, Landroid/location/Location;

    const-string v4, ""

    invoke-direct {v0, v4}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    .line 168
    .local v0, "location":Landroid/location/Location;
    iget-wide v4, v3, Lcom/google/android/gms/maps/model/LatLng;->latitude:D

    invoke-virtual {v0, v4, v5}, Landroid/location/Location;->setLatitude(D)V

    .line 169
    iget-wide v4, v3, Lcom/google/android/gms/maps/model/LatLng;->longitude:D

    invoke-virtual {v0, v4, v5}, Landroid/location/Location;->setLongitude(D)V

    .line 171
    iget-object v4, p0, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity$2;->this$0:Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;

    invoke-static {v4}, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;->access$200(Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;)Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    move-result-object v4

    invoke-virtual {v4, v0, v9, v8}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->moveMap(Landroid/location/Location;FZ)V

    .line 176
    .end local v0    # "location":Landroid/location/Location;
    .end local v1    # "markerOptions":Lcom/google/android/gms/maps/model/MarkerOptions;
    .end local v2    # "pinAsset":I
    :goto_1
    return-void

    .line 150
    :cond_1
    iget-object v4, p0, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity$2;->this$0:Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;

    invoke-static {v4}, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;->access$000(Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;)Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v4

    invoke-virtual {v4}, Lcom/navdy/client/app/framework/models/Destination;->hasValidNavCoordinates()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 151
    new-instance v3, Lcom/google/android/gms/maps/model/LatLng;

    .end local v3    # "position":Lcom/google/android/gms/maps/model/LatLng;
    iget-object v4, p0, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity$2;->this$0:Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;

    invoke-static {v4}, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;->access$000(Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;)Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v4

    iget-wide v4, v4, Lcom/navdy/client/app/framework/models/Destination;->navigationLat:D

    iget-object v6, p0, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity$2;->this$0:Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;

    invoke-static {v6}, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;->access$000(Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;)Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v6

    iget-wide v6, v6, Lcom/navdy/client/app/framework/models/Destination;->navigationLong:D

    invoke-direct {v3, v4, v5, v6, v7}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    .restart local v3    # "position":Lcom/google/android/gms/maps/model/LatLng;
    goto :goto_0

    .line 173
    :cond_2
    iget-object v4, p0, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity$2;->this$0:Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;

    invoke-static {v4}, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;->access$200(Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;)Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    move-result-object v4

    invoke-virtual {v4, v8}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->centerOnLastKnownLocation(Z)V

    goto :goto_1
.end method
