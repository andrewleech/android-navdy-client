.class Lcom/navdy/client/app/ui/MainActivity$4;
.super Ljava/lang/Object;
.source "MainActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/MainActivity;->initAndStartTheApp(J)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/MainActivity;

.field final synthetic val$finalRemainingTime:I


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/MainActivity;I)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/MainActivity;

    .prologue
    .line 117
    iput-object p1, p0, Lcom/navdy/client/app/ui/MainActivity$4;->this$0:Lcom/navdy/client/app/ui/MainActivity;

    iput p2, p0, Lcom/navdy/client/app/ui/MainActivity$4;->val$finalRemainingTime:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 121
    :try_start_0
    iget-object v1, p0, Lcom/navdy/client/app/ui/MainActivity$4;->this$0:Lcom/navdy/client/app/ui/MainActivity;

    invoke-static {v1}, Lcom/navdy/client/app/ui/MainActivity;->access$300(Lcom/navdy/client/app/ui/MainActivity;)Lcom/navdy/client/app/framework/AppInstance;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/client/app/framework/AppInstance;->initializeApp()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 137
    iget-object v1, p0, Lcom/navdy/client/app/ui/MainActivity$4;->this$0:Lcom/navdy/client/app/ui/MainActivity;

    invoke-static {v1}, Lcom/navdy/client/app/ui/MainActivity;->access$700(Lcom/navdy/client/app/ui/MainActivity;)Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/client/app/ui/MainActivity$4;->this$0:Lcom/navdy/client/app/ui/MainActivity;

    invoke-static {v2}, Lcom/navdy/client/app/ui/MainActivity;->access$600(Lcom/navdy/client/app/ui/MainActivity;)Ljava/lang/Runnable;

    move-result-object v2

    iget v3, p0, Lcom/navdy/client/app/ui/MainActivity$4;->val$finalRemainingTime:I

    int-to-long v4, v3

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 138
    :goto_0
    return-void

    .line 122
    :catch_0
    move-exception v0

    .line 123
    .local v0, "t":Ljava/lang/Throwable;
    iget-object v1, p0, Lcom/navdy/client/app/ui/MainActivity$4;->this$0:Lcom/navdy/client/app/ui/MainActivity;

    invoke-static {v1}, Lcom/navdy/client/app/ui/MainActivity;->access$400(Lcom/navdy/client/app/ui/MainActivity;)Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    .line 126
    iget-object v1, p0, Lcom/navdy/client/app/ui/MainActivity$4;->this$0:Lcom/navdy/client/app/ui/MainActivity;

    invoke-static {v1}, Lcom/navdy/client/app/ui/MainActivity;->access$500(Lcom/navdy/client/app/ui/MainActivity;)Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/navdy/client/app/ui/MainActivity$4$1;

    invoke-direct {v2, p0}, Lcom/navdy/client/app/ui/MainActivity$4$1;-><init>(Lcom/navdy/client/app/ui/MainActivity$4;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method
