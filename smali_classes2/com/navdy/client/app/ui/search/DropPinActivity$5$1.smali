.class Lcom/navdy/client/app/ui/search/DropPinActivity$5$1;
.super Ljava/lang/Object;
.source "DropPinActivity.java"

# interfaces
.implements Lcom/google/android/gms/maps/GoogleMap$OnMapClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/search/DropPinActivity$5;->onMapReady(Lcom/google/android/gms/maps/GoogleMap;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/navdy/client/app/ui/search/DropPinActivity$5;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/search/DropPinActivity$5;)V
    .locals 0
    .param p1, "this$1"    # Lcom/navdy/client/app/ui/search/DropPinActivity$5;

    .prologue
    .line 293
    iput-object p1, p0, Lcom/navdy/client/app/ui/search/DropPinActivity$5$1;->this$1:Lcom/navdy/client/app/ui/search/DropPinActivity$5;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMapClick(Lcom/google/android/gms/maps/model/LatLng;)V
    .locals 6
    .param p1, "latLng"    # Lcom/google/android/gms/maps/model/LatLng;

    .prologue
    .line 296
    if-eqz p1, :cond_0

    .line 297
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/DropPinActivity$5$1;->this$1:Lcom/navdy/client/app/ui/search/DropPinActivity$5;

    iget-object v0, v0, Lcom/navdy/client/app/ui/search/DropPinActivity$5;->this$0:Lcom/navdy/client/app/ui/search/DropPinActivity;

    invoke-static {v0}, Lcom/navdy/client/app/ui/search/DropPinActivity;->access$400(Lcom/navdy/client/app/ui/search/DropPinActivity;)Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v2, p1, Lcom/google/android/gms/maps/model/LatLng;->latitude:D

    iget-wide v4, p1, Lcom/google/android/gms/maps/model/LatLng;->longitude:D

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    const/high16 v2, 0x41600000    # 14.0f

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->moveMap(Lcom/google/android/gms/maps/model/LatLng;FZ)V

    .line 299
    :cond_0
    return-void
.end method
