.class Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback$2;
.super Ljava/lang/Object;
.source "SuggestionsFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback;->deleteSuggestionInMenu(Lcom/navdy/client/app/framework/models/Suggestion;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback;

.field final synthetic val$suggestion:Lcom/navdy/client/app/framework/models/Suggestion;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback;Lcom/navdy/client/app/framework/models/Suggestion;)V
    .locals 0
    .param p1, "this$1"    # Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback;

    .prologue
    .line 1132
    iput-object p1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback$2;->this$1:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback;

    iput-object p2, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback$2;->val$suggestion:Lcom/navdy/client/app/framework/models/Suggestion;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 1135
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback$2;->val$suggestion:Lcom/navdy/client/app/framework/models/Suggestion;

    iget-object v0, v0, Lcom/navdy/client/app/framework/models/Suggestion;->destination:Lcom/navdy/client/app/framework/models/Destination;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/navdy/client/app/framework/models/Destination;->doNotSuggest:Z

    .line 1136
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->reset(Landroid/content/Context;)V

    .line 1137
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback$2;->val$suggestion:Lcom/navdy/client/app/framework/models/Suggestion;

    iget-object v0, v0, Lcom/navdy/client/app/framework/models/Suggestion;->destination:Lcom/navdy/client/app/framework/models/Destination;

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/models/Destination;->updateDoNotSuggestAsync()V

    .line 1138
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback$2;->this$1:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback;

    iget-object v0, v0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback;->this$0:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    iget-object v0, v0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->suggestionAdapter:Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;

    if-eqz v0, :cond_0

    .line 1139
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback$2;->this$1:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback;

    iget-object v0, v0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback;->this$0:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    iget-object v0, v0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->suggestionAdapter:Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;

    invoke-virtual {v0}, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->rebuildSuggestions()V

    .line 1141
    :cond_0
    return-void
.end method
