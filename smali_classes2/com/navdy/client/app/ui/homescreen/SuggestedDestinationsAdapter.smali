.class public Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;
.super Landroid/support/v7/widget/RecyclerView$Adapter;
.source "SuggestedDestinationsAdapter.java"

# interfaces
.implements Landroid/view/View$OnLongClickListener;
.implements Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v7/widget/RecyclerView$Adapter",
        "<",
        "Landroid/support/v7/widget/RecyclerView$ViewHolder;",
        ">;",
        "Landroid/view/View$OnLongClickListener;",
        "Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteListener;"
    }
.end annotation


# static fields
.field private static final DESELECTED_SUGGESTION_ELEVATION:F = 0.0f

.field private static final SELECTED_SUGGESTION_ELEVATION:F = 8.0f

.field public static final SUGGESTION_LIST_MAX_SIZE:I = 0x14

.field private static final VERBOSE:Z

.field private static final logger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private activeTripCardView:Lcom/navdy/client/app/ui/customviews/ActiveTripCardView;

.field private final context:Landroid/content/Context;

.field googleMapHeight:I

.field hereMapHeight:I

.field private listener:Lcom/navdy/client/app/framework/util/CustomItemClickListener;

.field private longClickListener:Lcom/navdy/client/app/framework/util/CustomSuggestionsLongItemClickListener;

.field private final navdyRouteHandler:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

.field offlineBannerHeight:I

.field private pendingTripCardView:Lcom/navdy/client/app/ui/customviews/PendingTripCardView;

.field private recyclerView:Landroid/support/v7/widget/RecyclerView;

.field private selectedItem:Landroid/view/View;

.field private selectedItemPosition:I

.field private selectedSuggestionType:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

.field private suggestions:Ljava/util/ArrayList;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/client/app/framework/models/Suggestion;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 45
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->logger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;III)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "googleMapHeight"    # I
    .param p3, "hereMapHeight"    # I
    .param p4, "offlineBannerHeight"    # I

    .prologue
    const/4 v2, -0x1

    .line 77
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$Adapter;-><init>()V

    .line 58
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0x14

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->suggestions:Ljava/util/ArrayList;

    .line 64
    iput v2, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->selectedItemPosition:I

    .line 68
    iput v2, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->googleMapHeight:I

    .line 69
    iput v2, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->hereMapHeight:I

    .line 70
    iput v2, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->offlineBannerHeight:I

    .line 78
    iput-object p1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->context:Landroid/content/Context;

    .line 80
    invoke-static {}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->getInstance()Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->navdyRouteHandler:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

    .line 81
    iput p2, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->googleMapHeight:I

    .line 82
    iput p3, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->hereMapHeight:I

    .line 83
    iput p4, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->offlineBannerHeight:I

    .line 85
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->rebuildSuggestions()V

    .line 86
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;

    .prologue
    .line 41
    iget v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->selectedItemPosition:I

    return v0
.end method

.method static synthetic access$100(Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;)Lcom/navdy/client/app/framework/util/CustomItemClickListener;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->listener:Lcom/navdy/client/app/framework/util/CustomItemClickListener;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;ZLandroid/view/View;ILcom/navdy/client/app/framework/models/Suggestion;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;
    .param p1, "x1"    # Z
    .param p2, "x2"    # Landroid/view/View;
    .param p3, "x3"    # I
    .param p4, "x4"    # Lcom/navdy/client/app/framework/models/Suggestion;

    .prologue
    .line 41
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->triggerLongClick(ZLandroid/view/View;ILcom/navdy/client/app/framework/models/Suggestion;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;)Lcom/navdy/client/app/framework/util/CustomSuggestionsLongItemClickListener;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->longClickListener:Lcom/navdy/client/app/framework/util/CustomSuggestionsLongItemClickListener;

    return-object v0
.end method

.method private addActiveTripCard(Lcom/navdy/client/app/framework/models/Destination;)V
    .locals 1
    .param p1, "destination"    # Lcom/navdy/client/app/framework/models/Destination;

    .prologue
    .line 844
    sget-object v0, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->ACTIVE_TRIP:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    invoke-direct {p0, p1, v0}, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->addTripCard(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;)V

    .line 845
    return-void
.end method

.method private addPendingTripCard(Lcom/navdy/client/app/framework/models/Destination;)V
    .locals 1
    .param p1, "destination"    # Lcom/navdy/client/app/framework/models/Destination;

    .prologue
    .line 848
    sget-object v0, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->PENDING_TRIP:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    invoke-direct {p0, p1, v0}, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->addTripCard(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;)V

    .line 849
    return-void
.end method

.method private addTripCard(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;)V
    .locals 1
    .param p1, "destination"    # Lcom/navdy/client/app/framework/models/Destination;
    .param p2, "suggestionType"    # Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;
    .annotation build Landroid/support/annotation/MainThread;
    .end annotation

    .prologue
    .line 853
    invoke-static {}, Lcom/navdy/client/app/framework/util/SystemUtils;->ensureOnMainThread()V

    .line 859
    new-instance v0, Lcom/navdy/client/app/framework/models/Suggestion;

    invoke-direct {v0, p1, p2}, Lcom/navdy/client/app/framework/models/Suggestion;-><init>(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;)V

    .line 860
    .local v0, "tripSuggestion":Lcom/navdy/client/app/framework/models/Suggestion;
    invoke-static {v0}, Lcom/navdy/client/app/framework/util/SuggestionManager;->setTripAsync(Lcom/navdy/client/app/framework/models/Suggestion;)V

    .line 861
    return-void
.end method

.method private clearAnimationIfRunning(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V
    .locals 1
    .param p1, "holder"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;

    .prologue
    .line 257
    if-eqz p1, :cond_0

    instance-of v0, p1, Lcom/navdy/client/app/ui/homescreen/ContentLoadingViewHolder;

    if-eqz v0, :cond_0

    .line 258
    check-cast p1, Lcom/navdy/client/app/ui/homescreen/ContentLoadingViewHolder;

    .end local p1    # "holder":Landroid/support/v7/widget/RecyclerView$ViewHolder;
    invoke-virtual {p1}, Lcom/navdy/client/app/ui/homescreen/ContentLoadingViewHolder;->stopAnimation()V

    .line 260
    :cond_0
    return-void
.end method

.method private firstItemIs(Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;)Z
    .locals 2
    .param p1, "suggestionType"    # Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;
    .annotation build Landroid/support/annotation/MainThread;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 884
    invoke-static {}, Lcom/navdy/client/app/framework/util/SystemUtils;->ensureOnMainThread()V

    .line 886
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->suggestions:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->suggestions:Ljava/util/ArrayList;

    .line 887
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->suggestions:Ljava/util/ArrayList;

    .line 888
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/framework/models/Suggestion;

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/models/Suggestion;->getType()Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    move-result-object v0

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method private firstItemIsActiveTrip()Z
    .locals 1

    .prologue
    .line 875
    sget-object v0, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->ACTIVE_TRIP:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    invoke-direct {p0, v0}, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->firstItemIs(Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;)Z

    move-result v0

    return v0
.end method

.method private firstItemIsPendingTrip()Z
    .locals 1

    .prologue
    .line 879
    sget-object v0, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->PENDING_TRIP:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    invoke-direct {p0, v0}, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->firstItemIs(Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;)Z

    move-result v0

    return v0
.end method

.method private getIllustrationView(Landroid/view/View;)Landroid/widget/ImageView;
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 837
    if-nez p1, :cond_0

    .line 838
    const/4 v0, 0x0

    .line 840
    :goto_0
    return-object v0

    :cond_0
    const v0, 0x7f1000b1

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    goto :goto_0
.end method

.method private isNotSuggestionType(Landroid/support/v7/widget/RecyclerView$ViewHolder;)Z
    .locals 1
    .param p1, "viewHolder"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;

    .prologue
    .line 562
    instance-of v0, p1, Lcom/navdy/client/app/ui/homescreen/SuggestionsViewHolder;

    if-nez v0, :cond_0

    instance-of v0, p1, Lcom/navdy/client/app/ui/homescreen/ActiveTripViewHolder;

    if-nez v0, :cond_0

    instance-of v0, p1, Lcom/navdy/client/app/ui/homescreen/PendingTripViewHolder;

    if-nez v0, :cond_0

    instance-of v0, p1, Lcom/navdy/client/app/ui/homescreen/SectionHeaderViewHolder;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isRoutedSuggestion(I)Z
    .locals 1
    .param p1, "viewType"    # I

    .prologue
    .line 372
    sget-object v0, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->CALENDAR:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->ordinal()I

    move-result v0

    if-eq p1, v0, :cond_0

    sget-object v0, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->RECOMMENDATION:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    .line 373
    invoke-virtual {v0}, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->ordinal()I

    move-result v0

    if-ne p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isValidIndex(I)Z
    .locals 1
    .param p1, "position"    # I
    .annotation build Landroid/support/annotation/MainThread;
    .end annotation

    .prologue
    .line 553
    invoke-static {}, Lcom/navdy/client/app/framework/util/SystemUtils;->ensureOnMainThread()V

    .line 555
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->suggestions:Ljava/util/ArrayList;

    .line 556
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->suggestions:Ljava/util/ArrayList;

    .line 557
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->suggestions:Ljava/util/ArrayList;

    .line 558
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isValidSuggestion(I)Z
    .locals 1
    .param p1, "suggestionPosition"    # I
    .annotation build Landroid/support/annotation/MainThread;
    .end annotation

    .prologue
    .line 570
    invoke-static {}, Lcom/navdy/client/app/framework/util/SystemUtils;->ensureOnMainThread()V

    .line 572
    invoke-direct {p0, p1}, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->isValidIndex(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->suggestions:Ljava/util/ArrayList;

    .line 573
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/framework/models/Suggestion;

    iget-object v0, v0, Lcom/navdy/client/app/framework/models/Suggestion;->destination:Lcom/navdy/client/app/framework/models/Destination;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private onBindActiveTrip(Lcom/navdy/client/app/ui/homescreen/ActiveTripViewHolder;Lcom/navdy/client/app/framework/models/Suggestion;)V
    .locals 2
    .param p1, "activeTripViewHolder"    # Lcom/navdy/client/app/ui/homescreen/ActiveTripViewHolder;
    .param p2, "suggestion"    # Lcom/navdy/client/app/framework/models/Suggestion;

    .prologue
    .line 577
    iget-object v0, p1, Lcom/navdy/client/app/ui/homescreen/ActiveTripViewHolder;->itemView:Landroid/view/View;

    new-instance v1, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter$2;

    invoke-direct {v1, p0, p1}, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter$2;-><init>(Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;Lcom/navdy/client/app/ui/homescreen/ActiveTripViewHolder;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 584
    invoke-virtual {p1}, Lcom/navdy/client/app/ui/homescreen/ActiveTripViewHolder;->getAdapterPosition()I

    move-result v0

    iget v1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->selectedItemPosition:I

    if-ne v0, v1, :cond_1

    .line 585
    iget-object v0, p1, Lcom/navdy/client/app/ui/homescreen/ActiveTripViewHolder;->itemView:Landroid/view/View;

    sget-object v1, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->ACTIVE_TRIP:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    invoke-direct {p0, v0, v1}, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->setViewToSelectedMode(Landroid/view/View;Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;)V

    .line 593
    :goto_0
    iget-object v0, p1, Lcom/navdy/client/app/ui/homescreen/ActiveTripViewHolder;->itemView:Landroid/view/View;

    new-instance v1, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter$3;

    invoke-direct {v1, p0, p1, p2}, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter$3;-><init>(Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;Lcom/navdy/client/app/ui/homescreen/ActiveTripViewHolder;Lcom/navdy/client/app/framework/models/Suggestion;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 607
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->longClickListener:Lcom/navdy/client/app/framework/util/CustomSuggestionsLongItemClickListener;

    if-eqz v0, :cond_0

    .line 608
    iget-object v0, p1, Lcom/navdy/client/app/ui/homescreen/ActiveTripViewHolder;->itemView:Landroid/view/View;

    new-instance v1, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter$4;

    invoke-direct {v1, p0, p1, p2}, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter$4;-><init>(Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;Lcom/navdy/client/app/ui/homescreen/ActiveTripViewHolder;Lcom/navdy/client/app/framework/models/Suggestion;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 618
    :cond_0
    return-void

    .line 588
    :cond_1
    iget-object v0, p1, Lcom/navdy/client/app/ui/homescreen/ActiveTripViewHolder;->itemView:Landroid/view/View;

    sget-object v1, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->ACTIVE_TRIP:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    invoke-direct {p0, v0, v1}, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->setViewToNormalMode(Landroid/view/View;Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;)V

    goto :goto_0
.end method

.method private onBindPendingTrip(Lcom/navdy/client/app/ui/homescreen/PendingTripViewHolder;Lcom/navdy/client/app/framework/models/Suggestion;)V
    .locals 2
    .param p1, "pendingTripViewHolder"    # Lcom/navdy/client/app/ui/homescreen/PendingTripViewHolder;
    .param p2, "suggestion"    # Lcom/navdy/client/app/framework/models/Suggestion;

    .prologue
    .line 621
    if-eqz p2, :cond_0

    iget-object v0, p2, Lcom/navdy/client/app/framework/models/Suggestion;->destination:Lcom/navdy/client/app/framework/models/Destination;

    if-nez v0, :cond_1

    .line 663
    :cond_0
    :goto_0
    return-void

    .line 625
    :cond_1
    invoke-virtual {p2}, Lcom/navdy/client/app/framework/models/Suggestion;->isPendingTrip()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 626
    invoke-virtual {p1}, Lcom/navdy/client/app/ui/homescreen/PendingTripViewHolder;->getAdapterPosition()I

    move-result v0

    iget v1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->selectedItemPosition:I

    if-ne v0, v1, :cond_3

    .line 627
    iget-object v0, p1, Lcom/navdy/client/app/ui/homescreen/PendingTripViewHolder;->itemView:Landroid/view/View;

    sget-object v1, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->PENDING_TRIP:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    invoke-direct {p0, v0, v1}, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->setViewToSelectedMode(Landroid/view/View;Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;)V

    .line 636
    :cond_2
    :goto_1
    iget-object v0, p1, Lcom/navdy/client/app/ui/homescreen/PendingTripViewHolder;->itemView:Landroid/view/View;

    new-instance v1, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter$5;

    invoke-direct {v1, p0, p1, p2}, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter$5;-><init>(Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;Lcom/navdy/client/app/ui/homescreen/PendingTripViewHolder;Lcom/navdy/client/app/framework/models/Suggestion;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 652
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->longClickListener:Lcom/navdy/client/app/framework/util/CustomSuggestionsLongItemClickListener;

    if-eqz v0, :cond_0

    .line 653
    iget-object v0, p1, Lcom/navdy/client/app/ui/homescreen/PendingTripViewHolder;->itemView:Landroid/view/View;

    new-instance v1, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter$6;

    invoke-direct {v1, p0, p1, p2}, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter$6;-><init>(Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;Lcom/navdy/client/app/ui/homescreen/PendingTripViewHolder;Lcom/navdy/client/app/framework/models/Suggestion;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    goto :goto_0

    .line 630
    :cond_3
    iget-object v0, p1, Lcom/navdy/client/app/ui/homescreen/PendingTripViewHolder;->itemView:Landroid/view/View;

    sget-object v1, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->PENDING_TRIP:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    invoke-direct {p0, v0, v1}, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->setViewToNormalMode(Landroid/view/View;Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;)V

    goto :goto_1
.end method

.method private onBindSectionHeader(Lcom/navdy/client/app/ui/homescreen/SectionHeaderViewHolder;Lcom/navdy/client/app/framework/models/Suggestion;I)V
    .locals 2
    .param p1, "sectionHeaderViewHolder"    # Lcom/navdy/client/app/ui/homescreen/SectionHeaderViewHolder;
    .param p2, "suggestion"    # Lcom/navdy/client/app/framework/models/Suggestion;
    .param p3, "suggestionPosition"    # I

    .prologue
    .line 668
    iget-object v0, p1, Lcom/navdy/client/app/ui/homescreen/SectionHeaderViewHolder;->title:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    iget-object v0, p2, Lcom/navdy/client/app/framework/models/Suggestion;->destination:Lcom/navdy/client/app/framework/models/Destination;

    if-eqz v0, :cond_0

    .line 671
    iget-object v0, p1, Lcom/navdy/client/app/ui/homescreen/SectionHeaderViewHolder;->title:Landroid/widget/TextView;

    iget-object v1, p2, Lcom/navdy/client/app/framework/models/Suggestion;->destination:Lcom/navdy/client/app/framework/models/Destination;

    iget-object v1, v1, Lcom/navdy/client/app/framework/models/Destination;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 673
    :cond_0
    if-nez p3, :cond_1

    .line 674
    iget-object v0, p1, Lcom/navdy/client/app/ui/homescreen/SectionHeaderViewHolder;->chevron:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 678
    :goto_0
    return-void

    .line 676
    :cond_1
    iget-object v0, p1, Lcom/navdy/client/app/ui/homescreen/SectionHeaderViewHolder;->chevron:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method private onBindSuggestion(Lcom/navdy/client/app/ui/homescreen/SuggestionsViewHolder;Lcom/navdy/client/app/framework/models/Suggestion;)V
    .locals 4
    .param p1, "suggestionViewHolder"    # Lcom/navdy/client/app/ui/homescreen/SuggestionsViewHolder;
    .param p2, "suggestion"    # Lcom/navdy/client/app/framework/models/Suggestion;

    .prologue
    const/4 v3, 0x0

    .line 682
    invoke-direct {p0, p2, p1}, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->onBindSuggestionIconIllustration(Lcom/navdy/client/app/framework/models/Suggestion;Lcom/navdy/client/app/ui/homescreen/SuggestionsViewHolder;)V

    .line 685
    invoke-virtual {p2}, Lcom/navdy/client/app/framework/models/Suggestion;->shouldShowRightChevron()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 686
    iget-object v1, p1, Lcom/navdy/client/app/ui/homescreen/SuggestionsViewHolder;->rightChevron:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 692
    :goto_0
    iget-object v1, p1, Lcom/navdy/client/app/ui/homescreen/SuggestionsViewHolder;->infoButton:Landroid/widget/ImageButton;

    const v2, 0x7f0200f7

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 695
    invoke-direct {p0, p2, p1}, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->onBindSuggestionNameAndAddress(Lcom/navdy/client/app/framework/models/Suggestion;Lcom/navdy/client/app/ui/homescreen/SuggestionsViewHolder;)V

    .line 698
    invoke-virtual {p2}, Lcom/navdy/client/app/framework/models/Suggestion;->shouldSendToHud()Z

    move-result v0

    .line 701
    .local v0, "shouldSendToHud":Z
    iget-object v1, p1, Lcom/navdy/client/app/ui/homescreen/SuggestionsViewHolder;->separator:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 704
    invoke-direct {p0, p1, p2, p1, v0}, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->onBindSuggestionClickListeners(Lcom/navdy/client/app/ui/homescreen/SuggestionsViewHolder;Lcom/navdy/client/app/framework/models/Suggestion;Lcom/navdy/client/app/ui/homescreen/SuggestionsViewHolder;Z)V

    .line 705
    return-void

    .line 688
    .end local v0    # "shouldSendToHud":Z
    :cond_0
    iget-object v1, p1, Lcom/navdy/client/app/ui/homescreen/SuggestionsViewHolder;->rightChevron:Landroid/widget/ImageView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method private onBindSuggestionClickListeners(Lcom/navdy/client/app/ui/homescreen/SuggestionsViewHolder;Lcom/navdy/client/app/framework/models/Suggestion;Lcom/navdy/client/app/ui/homescreen/SuggestionsViewHolder;Z)V
    .locals 8
    .param p1, "viewHolder"    # Lcom/navdy/client/app/ui/homescreen/SuggestionsViewHolder;
    .param p2, "suggestion"    # Lcom/navdy/client/app/framework/models/Suggestion;
    .param p3, "suggestionViewHolder"    # Lcom/navdy/client/app/ui/homescreen/SuggestionsViewHolder;
    .param p4, "shouldSendToHud"    # Z

    .prologue
    .line 750
    iget-object v4, p1, Lcom/navdy/client/app/ui/homescreen/SuggestionsViewHolder;->background:Landroid/view/View;

    .line 752
    .local v4, "background":Landroid/view/View;
    new-instance v0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter$7;

    move-object v1, p0

    move-object v2, p1

    move v3, p4

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter$7;-><init>(Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;Lcom/navdy/client/app/ui/homescreen/SuggestionsViewHolder;ZLandroid/view/View;Lcom/navdy/client/app/framework/models/Suggestion;)V

    .line 765
    .local v0, "clickListener":Landroid/view/View$OnClickListener;
    iget-object v1, p3, Lcom/navdy/client/app/ui/homescreen/SuggestionsViewHolder;->row:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 766
    iget-object v1, p3, Lcom/navdy/client/app/ui/homescreen/SuggestionsViewHolder;->infoButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 767
    iget-object v7, p3, Lcom/navdy/client/app/ui/homescreen/SuggestionsViewHolder;->row:Landroid/view/View;

    new-instance v1, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter$8;

    move-object v2, p0

    move v3, p4

    move-object v5, p1

    move-object v6, p2

    invoke-direct/range {v1 .. v6}, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter$8;-><init>(Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;ZLandroid/view/View;Lcom/navdy/client/app/ui/homescreen/SuggestionsViewHolder;Lcom/navdy/client/app/framework/models/Suggestion;)V

    invoke-virtual {v7, v1}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 773
    return-void
.end method

.method private onBindSuggestionIconIllustration(Lcom/navdy/client/app/framework/models/Suggestion;Lcom/navdy/client/app/ui/homescreen/SuggestionsViewHolder;)V
    .locals 5
    .param p1, "suggestion"    # Lcom/navdy/client/app/framework/models/Suggestion;
    .param p2, "suggestionViewHolder"    # Lcom/navdy/client/app/ui/homescreen/SuggestionsViewHolder;

    .prologue
    const/4 v2, 0x0

    .line 708
    invoke-virtual {p2}, Lcom/navdy/client/app/ui/homescreen/SuggestionsViewHolder;->getAdapterPosition()I

    move-result v3

    iget v4, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->selectedItemPosition:I

    if-ne v3, v4, :cond_0

    const/4 v1, 0x1

    .line 709
    .local v1, "isSelected":Z
    :goto_0
    invoke-virtual {p1, v1}, Lcom/navdy/client/app/framework/models/Suggestion;->getBadgeAsset(Z)I

    move-result v0

    .line 710
    .local v0, "badgeAsset":I
    if-lez v0, :cond_1

    .line 711
    iget-object v3, p2, Lcom/navdy/client/app/ui/homescreen/SuggestionsViewHolder;->icon:Landroid/widget/ImageView;

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 715
    :goto_1
    iget-object v3, p2, Lcom/navdy/client/app/ui/homescreen/SuggestionsViewHolder;->background:Landroid/view/View;

    invoke-virtual {p1}, Lcom/navdy/client/app/framework/models/Suggestion;->getType()Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    move-result-object v4

    invoke-direct {p0, v3, v4, v2}, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->setBackground(Landroid/view/View;Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;Z)V

    .line 716
    return-void

    .end local v0    # "badgeAsset":I
    .end local v1    # "isSelected":Z
    :cond_0
    move v1, v2

    .line 708
    goto :goto_0

    .line 713
    .restart local v0    # "badgeAsset":I
    .restart local v1    # "isSelected":Z
    :cond_1
    iget-object v3, p2, Lcom/navdy/client/app/ui/homescreen/SuggestionsViewHolder;->icon:Landroid/widget/ImageView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_1
.end method

.method private onBindSuggestionNameAndAddress(Lcom/navdy/client/app/framework/models/Suggestion;Lcom/navdy/client/app/ui/homescreen/SuggestionsViewHolder;)V
    .locals 8
    .param p1, "suggestion"    # Lcom/navdy/client/app/framework/models/Suggestion;
    .param p2, "suggestionViewHolder"    # Lcom/navdy/client/app/ui/homescreen/SuggestionsViewHolder;

    .prologue
    const/4 v7, 0x0

    .line 719
    invoke-virtual {p1}, Lcom/navdy/client/app/framework/models/Suggestion;->hasInfoButton()Z

    move-result v2

    if-nez v2, :cond_0

    .line 720
    iget-object v2, p2, Lcom/navdy/client/app/ui/homescreen/SuggestionsViewHolder;->infoButton:Landroid/widget/ImageButton;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 722
    :cond_0
    iget-object v2, p2, Lcom/navdy/client/app/ui/homescreen/SuggestionsViewHolder;->icon:Landroid/widget/ImageView;

    invoke-virtual {v2, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 724
    iget-object v2, p1, Lcom/navdy/client/app/framework/models/Suggestion;->destination:Lcom/navdy/client/app/framework/models/Destination;

    if-eqz v2, :cond_1

    .line 725
    invoke-virtual {p1}, Lcom/navdy/client/app/framework/models/Suggestion;->isTip()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 726
    iget-object v2, p2, Lcom/navdy/client/app/ui/homescreen/SuggestionsViewHolder;->firstLine:Landroid/widget/TextView;

    iget-object v3, p1, Lcom/navdy/client/app/framework/models/Suggestion;->destination:Lcom/navdy/client/app/framework/models/Destination;

    iget-object v3, v3, Lcom/navdy/client/app/framework/models/Destination;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 727
    iget-object v2, p2, Lcom/navdy/client/app/ui/homescreen/SuggestionsViewHolder;->secondLine:Landroid/widget/TextView;

    iget-object v3, p1, Lcom/navdy/client/app/framework/models/Suggestion;->destination:Lcom/navdy/client/app/framework/models/Destination;

    invoke-virtual {v3}, Lcom/navdy/client/app/framework/models/Destination;->getAddressForDisplay()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 737
    :goto_0
    invoke-virtual {p1}, Lcom/navdy/client/app/framework/models/Suggestion;->isCalendar()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 738
    iget-object v2, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->context:Landroid/content/Context;

    const v3, 0x7f08016c

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p1, Lcom/navdy/client/app/framework/models/Suggestion;->event:Lcom/navdy/client/app/framework/models/CalendarEvent;

    .line 739
    invoke-virtual {v5}, Lcom/navdy/client/app/framework/models/CalendarEvent;->getStartTime()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    const/4 v5, 0x1

    iget-object v6, p1, Lcom/navdy/client/app/framework/models/Suggestion;->event:Lcom/navdy/client/app/framework/models/CalendarEvent;

    .line 740
    invoke-virtual {v6}, Lcom/navdy/client/app/framework/models/CalendarEvent;->getEndTime()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    .line 738
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 741
    .local v0, "eventTime":Ljava/lang/String;
    iget-object v2, p2, Lcom/navdy/client/app/ui/homescreen/SuggestionsViewHolder;->thirdLine:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 742
    iget-object v2, p2, Lcom/navdy/client/app/ui/homescreen/SuggestionsViewHolder;->thirdLine:Landroid/widget/TextView;

    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 747
    .end local v0    # "eventTime":Ljava/lang/String;
    :cond_1
    :goto_1
    return-void

    .line 728
    :cond_2
    invoke-virtual {p1}, Lcom/navdy/client/app/framework/models/Suggestion;->isCalendar()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p1, Lcom/navdy/client/app/framework/models/Suggestion;->event:Lcom/navdy/client/app/framework/models/CalendarEvent;

    if-eqz v2, :cond_3

    iget-object v2, p1, Lcom/navdy/client/app/framework/models/Suggestion;->event:Lcom/navdy/client/app/framework/models/CalendarEvent;

    iget-object v2, v2, Lcom/navdy/client/app/framework/models/CalendarEvent;->displayName:Ljava/lang/String;

    invoke-static {v2}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 729
    iget-object v2, p2, Lcom/navdy/client/app/ui/homescreen/SuggestionsViewHolder;->firstLine:Landroid/widget/TextView;

    iget-object v3, p1, Lcom/navdy/client/app/framework/models/Suggestion;->event:Lcom/navdy/client/app/framework/models/CalendarEvent;

    iget-object v3, v3, Lcom/navdy/client/app/framework/models/CalendarEvent;->displayName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 730
    iget-object v2, p2, Lcom/navdy/client/app/ui/homescreen/SuggestionsViewHolder;->secondLine:Landroid/widget/TextView;

    iget-object v3, p1, Lcom/navdy/client/app/framework/models/Suggestion;->destination:Lcom/navdy/client/app/framework/models/Destination;

    invoke-virtual {v3}, Lcom/navdy/client/app/framework/models/Destination;->getAddressForDisplay()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 732
    :cond_3
    iget-object v2, p1, Lcom/navdy/client/app/framework/models/Suggestion;->destination:Lcom/navdy/client/app/framework/models/Destination;

    invoke-virtual {v2}, Lcom/navdy/client/app/framework/models/Destination;->getTitleAndSubtitle()Landroid/util/Pair;

    move-result-object v1

    .line 733
    .local v1, "splitAddress":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v3, p2, Lcom/navdy/client/app/ui/homescreen/SuggestionsViewHolder;->firstLine:Landroid/widget/TextView;

    iget-object v2, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 734
    iget-object v3, p2, Lcom/navdy/client/app/ui/homescreen/SuggestionsViewHolder;->secondLine:Landroid/widget/TextView;

    iget-object v2, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 744
    .end local v1    # "splitAddress":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_4
    iget-object v2, p2, Lcom/navdy/client/app/ui/homescreen/SuggestionsViewHolder;->thirdLine:Landroid/widget/TextView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1
.end method

.method private removeTripCard()V
    .locals 1
    .annotation build Landroid/support/annotation/MainThread;
    .end annotation

    .prologue
    .line 865
    invoke-static {}, Lcom/navdy/client/app/framework/util/SystemUtils;->ensureOnMainThread()V

    .line 871
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/navdy/client/app/framework/util/SuggestionManager;->setTripAsync(Lcom/navdy/client/app/framework/models/Suggestion;)V

    .line 872
    return-void
.end method

.method private setBackground(Landroid/view/View;Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;Z)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "type"    # Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;
    .param p3, "selected"    # Z

    .prologue
    .line 790
    if-eqz p1, :cond_0

    sget-object v0, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->ACTIVE_TRIP:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    if-ne p2, v0, :cond_1

    .line 803
    :cond_0
    :goto_0
    return-void

    .line 794
    :cond_1
    if-eqz p3, :cond_2

    .line 796
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0f0065

    invoke-static {v0, v1}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v0

    .line 795
    invoke-virtual {p1, v0}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_0

    .line 800
    :cond_2
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0f00cc

    invoke-static {v0, v1}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v0

    .line 799
    invoke-virtual {p1, v0}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_0
.end method

.method private setViewToNormalMode(Landroid/view/View;Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;
    .param p2, "suggestionType"    # Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    .prologue
    .line 824
    if-nez p1, :cond_0

    .line 834
    :goto_0
    return-void

    .line 827
    :cond_0
    const/4 v2, 0x0

    invoke-direct {p0, p1, p2, v2}, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->setBackground(Landroid/view/View;Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;Z)V

    .line 828
    invoke-direct {p0, p1}, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->getIllustrationView(Landroid/view/View;)Landroid/widget/ImageView;

    move-result-object v0

    .line 829
    .local v0, "illustration":Landroid/widget/ImageView;
    if-eqz v0, :cond_1

    .line 830
    invoke-virtual {v0}, Landroid/widget/ImageView;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/Drawable;

    .line 831
    .local v1, "image":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 833
    .end local v1    # "image":Landroid/graphics/drawable/Drawable;
    :cond_1
    const/4 v2, 0x0

    invoke-static {p1, v2}, Landroid/support/v4/view/ViewCompat;->setElevation(Landroid/view/View;F)V

    goto :goto_0
.end method

.method private setViewToSelectedMode(Landroid/view/View;Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "suggestionType"    # Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    .prologue
    .line 807
    if-nez p1, :cond_1

    .line 821
    :cond_0
    :goto_0
    return-void

    .line 810
    :cond_1
    const/4 v1, 0x1

    invoke-direct {p0, p1, p2, v1}, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->setBackground(Landroid/view/View;Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;Z)V

    .line 811
    invoke-direct {p0, p1}, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->getIllustrationView(Landroid/view/View;)Landroid/widget/ImageView;

    move-result-object v0

    .line 812
    .local v0, "illustration":Landroid/widget/ImageView;
    if-eqz v0, :cond_2

    .line 813
    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 814
    const v1, 0x7f0201c9

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 816
    :cond_2
    sget-object v1, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->PENDING_TRIP:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    if-eq p2, v1, :cond_0

    sget-object v1, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->ACTIVE_TRIP:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    if-eq p2, v1, :cond_0

    .line 819
    const/high16 v1, 0x41000000    # 8.0f

    invoke-static {p1, v1}, Landroid/support/v4/view/ViewCompat;->setElevation(Landroid/view/View;F)V

    goto :goto_0
.end method

.method private triggerLongClick(ZLandroid/view/View;ILcom/navdy/client/app/framework/models/Suggestion;)Z
    .locals 1
    .param p1, "shouldSendToHud"    # Z
    .param p2, "background"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "suggestion"    # Lcom/navdy/client/app/framework/models/Suggestion;

    .prologue
    .line 779
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->longClickListener:Lcom/navdy/client/app/framework/util/CustomSuggestionsLongItemClickListener;

    if-eqz v0, :cond_1

    if-nez p1, :cond_0

    .line 781
    invoke-virtual {p4}, Lcom/navdy/client/app/framework/models/Suggestion;->isPendingTrip()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 782
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->longClickListener:Lcom/navdy/client/app/framework/util/CustomSuggestionsLongItemClickListener;

    invoke-interface {v0, p2, p3, p4}, Lcom/navdy/client/app/framework/util/CustomSuggestionsLongItemClickListener;->onItemLongClick(Landroid/view/View;ILcom/navdy/client/app/framework/models/Suggestion;)Z

    .line 783
    const/4 v0, 0x1

    .line 785
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method clearAllAnimations()V
    .locals 7

    .prologue
    .line 223
    sget-object v5, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "Clearing all animations in the suggestion adapter."

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 226
    iget-object v5, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->recyclerView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v5}, Landroid/support/v7/widget/RecyclerView;->getChildCount()I

    move-result v0

    .line 227
    .local v0, "count":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v0, :cond_0

    .line 229
    :try_start_0
    iget-object v5, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->recyclerView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v5, v2}, Landroid/support/v7/widget/RecyclerView;->findViewHolderForAdapterPosition(I)Landroid/support/v7/widget/RecyclerView$ViewHolder;

    move-result-object v1

    .line 230
    .local v1, "holder":Landroid/support/v7/widget/RecyclerView$ViewHolder;
    invoke-direct {p0, v1}, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->clearAnimationIfRunning(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 227
    .end local v1    # "holder":Landroid/support/v7/widget/RecyclerView$ViewHolder;
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 231
    :catch_0
    move-exception v4

    .line 232
    .local v4, "t":Ljava/lang/Throwable;
    sget-object v5, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "Unable to clear animations"

    invoke-virtual {v5, v6, v4}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 237
    .end local v4    # "t":Ljava/lang/Throwable;
    :cond_0
    iget-object v5, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->recyclerView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v5}, Landroid/support/v7/widget/RecyclerView;->getRecycledViewPool()Landroid/support/v7/widget/RecyclerView$RecycledViewPool;

    move-result-object v3

    .line 238
    .local v3, "pool":Landroid/support/v7/widget/RecyclerView$RecycledViewPool;
    if-eqz v3, :cond_1

    .line 240
    :goto_2
    sget-object v5, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->LOADING:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    invoke-virtual {v5}, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->ordinal()I

    move-result v5

    invoke-virtual {v3, v5}, Landroid/support/v7/widget/RecyclerView$RecycledViewPool;->getRecycledView(I)Landroid/support/v7/widget/RecyclerView$ViewHolder;

    move-result-object v1

    .line 241
    .restart local v1    # "holder":Landroid/support/v7/widget/RecyclerView$ViewHolder;
    if-eqz v1, :cond_1

    .line 242
    invoke-direct {p0, v1}, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->clearAnimationIfRunning(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V

    goto :goto_2

    .line 249
    .end local v1    # "holder":Landroid/support/v7/widget/RecyclerView$ViewHolder;
    :cond_1
    return-void
.end method

.method clickActiveTripCard()V
    .locals 1

    .prologue
    .line 187
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->activeTripCardView:Lcom/navdy/client/app/ui/customviews/ActiveTripCardView;

    if-eqz v0, :cond_0

    .line 188
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->activeTripCardView:Lcom/navdy/client/app/ui/customviews/ActiveTripCardView;

    invoke-virtual {v0}, Lcom/navdy/client/app/ui/customviews/ActiveTripCardView;->handleOnClick()V

    .line 190
    :cond_0
    return-void
.end method

.method clickPendingTripCard(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 181
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->pendingTripCardView:Lcom/navdy/client/app/ui/customviews/PendingTripCardView;

    if-eqz v0, :cond_0

    .line 182
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->pendingTripCardView:Lcom/navdy/client/app/ui/customviews/PendingTripCardView;

    invoke-virtual {v0, p1}, Lcom/navdy/client/app/ui/customviews/PendingTripCardView;->handleOnClick(Landroid/content/Context;)V

    .line 184
    :cond_0
    return-void
.end method

.method endSelectionMode()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 174
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->selectedItem:Landroid/view/View;

    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->selectedSuggestionType:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    invoke-direct {p0, v0, v1}, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->setViewToNormalMode(Landroid/view/View;Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;)V

    .line 175
    const/4 v0, -0x1

    iput v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->selectedItemPosition:I

    .line 176
    iput-object v2, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->selectedItem:Landroid/view/View;

    .line 177
    iput-object v2, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->selectedSuggestionType:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    .line 178
    return-void
.end method

.method getGoogleMapHeight()I
    .locals 3

    .prologue
    .line 89
    invoke-static {}, Lcom/navdy/client/app/framework/AppInstance;->getInstance()Lcom/navdy/client/app/framework/AppInstance;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/client/app/framework/AppInstance;->canReachInternet()Z

    move-result v0

    .line 90
    .local v0, "canReachInternet":Z
    iget v2, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->googleMapHeight:I

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    sub-int v1, v2, v1

    return v1

    :cond_0
    iget v1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->offlineBannerHeight:I

    goto :goto_0
.end method

.method getHereMapHeight()I
    .locals 3

    .prologue
    .line 94
    invoke-static {}, Lcom/navdy/client/app/framework/AppInstance;->getInstance()Lcom/navdy/client/app/framework/AppInstance;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/client/app/framework/AppInstance;->canReachInternet()Z

    move-result v0

    .line 95
    .local v0, "canReachInternet":Z
    iget v2, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->hereMapHeight:I

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    sub-int v1, v2, v1

    return v1

    :cond_0
    iget v1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->offlineBannerHeight:I

    goto :goto_0
.end method

.method public getItem(I)Lcom/navdy/client/app/framework/models/Suggestion;
    .locals 1
    .param p1, "position"    # I
    .annotation build Landroid/support/annotation/MainThread;
    .end annotation

    .prologue
    .line 145
    invoke-static {}, Lcom/navdy/client/app/framework/util/SystemUtils;->ensureOnMainThread()V

    .line 147
    add-int/lit8 p1, p1, -0x1

    .line 148
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->suggestions:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 149
    :cond_0
    const/4 v0, 0x0

    .line 151
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->suggestions:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/framework/models/Suggestion;

    goto :goto_0
.end method

.method public getItemCount()I
    .locals 1
    .annotation build Landroid/support/annotation/MainThread;
    .end annotation

    .prologue
    .line 461
    invoke-static {}, Lcom/navdy/client/app/framework/util/SystemUtils;->ensureOnMainThread()V

    .line 462
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->suggestions:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x2

    return v0
.end method

.method public getItemViewType(I)I
    .locals 1
    .param p1, "position"    # I
    .annotation build Landroid/support/annotation/MainThread;
    .end annotation

    .prologue
    .line 468
    invoke-static {}, Lcom/navdy/client/app/framework/util/SystemUtils;->ensureOnMainThread()V

    .line 470
    if-nez p1, :cond_2

    .line 471
    invoke-direct {p0}, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->firstItemIsActiveTrip()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->firstItemIsPendingTrip()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 472
    :cond_0
    sget-object v0, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->HERE_HEADER:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->ordinal()I

    move-result v0

    .line 491
    :goto_0
    return v0

    .line 474
    :cond_1
    sget-object v0, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->GOOGLE_HEADER:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->ordinal()I

    move-result v0

    goto :goto_0

    .line 478
    :cond_2
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->getItemCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_5

    .line 479
    invoke-direct {p0}, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->firstItemIsActiveTrip()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-direct {p0}, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->firstItemIsPendingTrip()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 480
    :cond_3
    sget-object v0, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->HERE_FOOTER:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->ordinal()I

    move-result v0

    goto :goto_0

    .line 482
    :cond_4
    sget-object v0, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->GOOGLE_FOOTER:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->ordinal()I

    move-result v0

    goto :goto_0

    .line 486
    :cond_5
    add-int/lit8 p1, p1, -0x1

    .line 488
    invoke-direct {p0, p1}, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->isValidIndex(I)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 489
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->suggestions:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/framework/models/Suggestion;

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/models/Suggestion;->getType()Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->ordinal()I

    move-result v0

    goto :goto_0

    .line 491
    :cond_6
    sget-object v0, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->RECENT:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->ordinal()I

    move-result v0

    goto :goto_0
.end method

.method public onAttachedToRecyclerView(Landroid/support/v7/widget/RecyclerView;)V
    .locals 1
    .param p1, "recyclerView"    # Landroid/support/v7/widget/RecyclerView;

    .prologue
    .line 196
    invoke-super {p0, p1}, Landroid/support/v7/widget/RecyclerView$Adapter;->onAttachedToRecyclerView(Landroid/support/v7/widget/RecyclerView;)V

    .line 197
    iput-object p1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->recyclerView:Landroid/support/v7/widget/RecyclerView;

    .line 198
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->navdyRouteHandler:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

    invoke-virtual {v0, p0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->addListener(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteListener;)V

    .line 199
    return-void
.end method

.method public onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 8
    .param p1, "viewHolder"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .param p2, "unreliablePosition"    # I
    .annotation build Landroid/support/annotation/MainThread;
    .end annotation

    .prologue
    const/4 v7, -0x1

    .line 379
    invoke-static {}, Lcom/navdy/client/app/framework/util/SystemUtils;->ensureOnMainThread()V

    .line 381
    if-eqz p1, :cond_0

    iget-object v5, p1, Landroid/support/v7/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    if-nez v5, :cond_2

    .line 382
    :cond_0
    sget-object v5, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "onBindViewHolder called on a null viewHolder !!!"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 456
    .end local p1    # "viewHolder":Landroid/support/v7/widget/RecyclerView$ViewHolder;
    :cond_1
    :goto_0
    return-void

    .line 386
    .restart local p1    # "viewHolder":Landroid/support/v7/widget/RecyclerView$ViewHolder;
    :cond_2
    add-int/lit8 v3, p2, -0x1

    .line 389
    .local v3, "suggestionPosition":I
    invoke-direct {p0, v3}, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->isValidSuggestion(I)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 390
    invoke-direct {p0, p1}, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->isNotSuggestionType(Landroid/support/v7/widget/RecyclerView$ViewHolder;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 393
    :cond_3
    instance-of v5, p1, Lcom/navdy/client/app/ui/homescreen/ContentLoadingViewHolder;

    if-eqz v5, :cond_4

    move-object v5, p1

    .line 394
    check-cast v5, Lcom/navdy/client/app/ui/homescreen/ContentLoadingViewHolder;

    invoke-virtual {v5}, Lcom/navdy/client/app/ui/homescreen/ContentLoadingViewHolder;->startAnimation()V

    .line 398
    :cond_4
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView$ViewHolder;->getItemViewType()I

    move-result v5

    sget-object v6, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->GOOGLE_HEADER:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    invoke-virtual {v6}, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->ordinal()I

    move-result v6

    if-ne v5, v6, :cond_5

    .line 399
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {p0}, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->getGoogleMapHeight()I

    move-result v5

    invoke-direct {v0, v7, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 400
    .local v0, "params":Landroid/view/ViewGroup$LayoutParams;
    iget-object v5, p1, Landroid/support/v7/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {v5, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 403
    .end local v0    # "params":Landroid/view/ViewGroup$LayoutParams;
    :cond_5
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView$ViewHolder;->getItemViewType()I

    move-result v5

    sget-object v6, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->HERE_HEADER:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    invoke-virtual {v6}, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->ordinal()I

    move-result v6

    if-ne v5, v6, :cond_6

    .line 404
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {p0}, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->getHereMapHeight()I

    move-result v5

    invoke-direct {v0, v7, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 405
    .restart local v0    # "params":Landroid/view/ViewGroup$LayoutParams;
    iget-object v5, p1, Landroid/support/v7/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {v5, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 408
    .end local v0    # "params":Landroid/view/ViewGroup$LayoutParams;
    :cond_6
    iget-object v5, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->listener:Lcom/navdy/client/app/framework/util/CustomItemClickListener;

    if-eqz v5, :cond_1

    .line 409
    iget-object v5, p1, Landroid/support/v7/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    new-instance v6, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter$1;

    invoke-direct {v6, p0, p1}, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter$1;-><init>(Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;Landroid/support/v7/widget/RecyclerView$ViewHolder;)V

    invoke-virtual {v5, v6}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 422
    :cond_7
    iget-object v5, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->suggestions:Ljava/util/ArrayList;

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/navdy/client/app/framework/models/Suggestion;

    .line 425
    .local v2, "suggestion":Lcom/navdy/client/app/framework/models/Suggestion;
    instance-of v5, p1, Lcom/navdy/client/app/ui/homescreen/ActiveTripViewHolder;

    if-eqz v5, :cond_8

    .line 426
    check-cast p1, Lcom/navdy/client/app/ui/homescreen/ActiveTripViewHolder;

    .end local p1    # "viewHolder":Landroid/support/v7/widget/RecyclerView$ViewHolder;
    invoke-direct {p0, p1, v2}, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->onBindActiveTrip(Lcom/navdy/client/app/ui/homescreen/ActiveTripViewHolder;Lcom/navdy/client/app/framework/models/Suggestion;)V

    goto :goto_0

    .line 431
    .restart local p1    # "viewHolder":Landroid/support/v7/widget/RecyclerView$ViewHolder;
    :cond_8
    instance-of v5, p1, Lcom/navdy/client/app/ui/homescreen/PendingTripViewHolder;

    if-eqz v5, :cond_9

    .line 432
    sget-object v5, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "pendingTripViewHolder found: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v2, Lcom/navdy/client/app/framework/models/Suggestion;->destination:Lcom/navdy/client/app/framework/models/Destination;

    iget-object v7, v7, Lcom/navdy/client/app/framework/models/Destination;->name:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " at adapter position: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 433
    check-cast p1, Lcom/navdy/client/app/ui/homescreen/PendingTripViewHolder;

    .end local p1    # "viewHolder":Landroid/support/v7/widget/RecyclerView$ViewHolder;
    invoke-direct {p0, p1, v2}, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->onBindPendingTrip(Lcom/navdy/client/app/ui/homescreen/PendingTripViewHolder;Lcom/navdy/client/app/framework/models/Suggestion;)V

    goto/16 :goto_0

    .line 438
    .restart local p1    # "viewHolder":Landroid/support/v7/widget/RecyclerView$ViewHolder;
    :cond_9
    instance-of v5, p1, Lcom/navdy/client/app/ui/homescreen/SectionHeaderViewHolder;

    if-eqz v5, :cond_a

    .line 439
    check-cast p1, Lcom/navdy/client/app/ui/homescreen/SectionHeaderViewHolder;

    .end local p1    # "viewHolder":Landroid/support/v7/widget/RecyclerView$ViewHolder;
    invoke-direct {p0, p1, v2, v3}, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->onBindSectionHeader(Lcom/navdy/client/app/ui/homescreen/SectionHeaderViewHolder;Lcom/navdy/client/app/framework/models/Suggestion;I)V

    goto/16 :goto_0

    .line 444
    .restart local p1    # "viewHolder":Landroid/support/v7/widget/RecyclerView$ViewHolder;
    :cond_a
    instance-of v5, p1, Lcom/navdy/client/app/ui/homescreen/SuggestionsViewHolder;

    if-eqz v5, :cond_1

    move-object v4, p1

    .line 445
    check-cast v4, Lcom/navdy/client/app/ui/homescreen/SuggestionsViewHolder;

    .line 446
    .local v4, "suggestionsViewHolder":Lcom/navdy/client/app/ui/homescreen/SuggestionsViewHolder;
    invoke-direct {p0, v4, v2}, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->onBindSuggestion(Lcom/navdy/client/app/ui/homescreen/SuggestionsViewHolder;Lcom/navdy/client/app/framework/models/Suggestion;)V

    .line 448
    instance-of v5, p1, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;

    if-eqz v5, :cond_b

    move-object v1, p1

    .line 449
    check-cast v1, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;

    .line 451
    .local v1, "routedSuggestionsViewHolder":Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;
    invoke-virtual {v1, v2}, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;->onBindViewHolder(Lcom/navdy/client/app/framework/models/Suggestion;)V

    goto/16 :goto_0

    .line 453
    .end local v1    # "routedSuggestionsViewHolder":Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;
    :cond_b
    iget-object v5, v4, Lcom/navdy/client/app/ui/homescreen/SuggestionsViewHolder;->subIllustrationText:Lcom/navdy/client/app/ui/customviews/UnitSystemTextView;

    const/16 v6, 0x8

    invoke-virtual {v5, v6}, Lcom/navdy/client/app/ui/customviews/UnitSystemTextView;->setVisibility(I)V

    goto/16 :goto_0
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 8
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "viewType"    # I

    .prologue
    const/4 v7, -0x1

    const/4 v6, 0x0

    .line 267
    sget-object v4, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->LOADING:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    invoke-virtual {v4}, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->ordinal()I

    move-result v4

    if-ne p2, v4, :cond_0

    .line 269
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    const v5, 0x7f0300b5

    .line 270
    invoke-virtual {v4, v5, p1, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 271
    .local v3, "v":Landroid/view/View;
    new-instance v4, Lcom/navdy/client/app/ui/homescreen/ContentLoadingViewHolder;

    invoke-direct {v4, v3}, Lcom/navdy/client/app/ui/homescreen/ContentLoadingViewHolder;-><init>(Landroid/view/View;)V

    .line 368
    .end local v3    # "v":Landroid/view/View;
    :goto_0
    return-object v4

    .line 275
    :cond_0
    sget-object v4, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->GOOGLE_HEADER:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    invoke-virtual {v4}, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->ordinal()I

    move-result v4

    if-ne p2, v4, :cond_1

    .line 277
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    const v5, 0x7f03009f

    .line 278
    invoke-virtual {v4, v5, p1, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 279
    .restart local v3    # "v":Landroid/view/View;
    new-instance v4, Landroid/widget/RelativeLayout$LayoutParams;

    .line 281
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->getGoogleMapHeight()I

    move-result v5

    invoke-direct {v4, v7, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 279
    invoke-virtual {v3, v4}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 282
    new-instance v4, Lcom/navdy/client/app/ui/homescreen/SuggestionsViewHolder;

    invoke-direct {v4, v3}, Lcom/navdy/client/app/ui/homescreen/SuggestionsViewHolder;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 285
    .end local v3    # "v":Landroid/view/View;
    :cond_1
    sget-object v4, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->HERE_HEADER:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    invoke-virtual {v4}, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->ordinal()I

    move-result v4

    if-ne p2, v4, :cond_2

    .line 287
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    const v5, 0x7f0300a0

    .line 288
    invoke-virtual {v4, v5, p1, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 289
    .restart local v3    # "v":Landroid/view/View;
    new-instance v4, Landroid/widget/RelativeLayout$LayoutParams;

    .line 291
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->getHereMapHeight()I

    move-result v5

    invoke-direct {v4, v7, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 289
    invoke-virtual {v3, v4}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 292
    new-instance v4, Lcom/navdy/client/app/ui/homescreen/SuggestionsViewHolder;

    invoke-direct {v4, v3}, Lcom/navdy/client/app/ui/homescreen/SuggestionsViewHolder;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 295
    .end local v3    # "v":Landroid/view/View;
    :cond_2
    sget-object v4, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->GOOGLE_FOOTER:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    invoke-virtual {v4}, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->ordinal()I

    move-result v4

    if-eq p2, v4, :cond_3

    sget-object v4, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->HERE_FOOTER:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    .line 296
    invoke-virtual {v4}, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->ordinal()I

    move-result v4

    if-ne p2, v4, :cond_6

    .line 298
    :cond_3
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    const v5, 0x7f0300ff

    .line 299
    invoke-virtual {v4, v5, p1, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 301
    .restart local v3    # "v":Landroid/view/View;
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->getGoogleMapHeight()I

    move-result v1

    .line 302
    .local v1, "footerHeight":I
    sget-object v4, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->HERE_FOOTER:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    invoke-virtual {v4}, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->ordinal()I

    move-result v4

    if-ne p2, v4, :cond_4

    .line 303
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->getHereMapHeight()I

    move-result v1

    .line 305
    :cond_4
    iget-object v4, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->context:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 306
    .local v2, "resources":Landroid/content/res/Resources;
    if-eqz v2, :cond_5

    .line 307
    const v4, 0x7f0b0131

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    add-int/2addr v1, v4

    .line 309
    :cond_5
    new-instance v4, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v4, v7, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v3, v4}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 313
    new-instance v4, Lcom/navdy/client/app/ui/homescreen/SectionHeaderViewHolder;

    invoke-direct {v4, v3}, Lcom/navdy/client/app/ui/homescreen/SectionHeaderViewHolder;-><init>(Landroid/view/View;)V

    goto/16 :goto_0

    .line 318
    .end local v1    # "footerHeight":I
    .end local v2    # "resources":Landroid/content/res/Resources;
    .end local v3    # "v":Landroid/view/View;
    :cond_6
    sget-object v4, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->ACTIVE_TRIP:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    invoke-virtual {v4}, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->ordinal()I

    move-result v4

    if-ne p2, v4, :cond_9

    .line 319
    iget-object v4, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->activeTripCardView:Lcom/navdy/client/app/ui/customviews/ActiveTripCardView;

    if-nez v4, :cond_7

    .line 324
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    const v5, 0x7f03001c

    .line 325
    invoke-virtual {v4, v5, p1, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/navdy/client/app/ui/customviews/ActiveTripCardView;

    iput-object v4, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->activeTripCardView:Lcom/navdy/client/app/ui/customviews/ActiveTripCardView;

    .line 327
    :cond_7
    iget-object v4, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->activeTripCardView:Lcom/navdy/client/app/ui/customviews/ActiveTripCardView;

    const v5, 0x7f1000b8

    invoke-virtual {v4, v5}, Lcom/navdy/client/app/ui/customviews/ActiveTripCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 328
    .local v0, "chevron":Landroid/widget/ImageView;
    if-eqz v0, :cond_8

    .line 329
    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 332
    :cond_8
    new-instance v4, Lcom/navdy/client/app/ui/homescreen/ActiveTripViewHolder;

    iget-object v5, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->activeTripCardView:Lcom/navdy/client/app/ui/customviews/ActiveTripCardView;

    invoke-direct {v4, v5}, Lcom/navdy/client/app/ui/homescreen/ActiveTripViewHolder;-><init>(Landroid/view/View;)V

    goto/16 :goto_0

    .line 335
    .end local v0    # "chevron":Landroid/widget/ImageView;
    :cond_9
    sget-object v4, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->PENDING_TRIP:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    invoke-virtual {v4}, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->ordinal()I

    move-result v4

    if-ne p2, v4, :cond_b

    .line 336
    iget-object v4, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->pendingTripCardView:Lcom/navdy/client/app/ui/customviews/PendingTripCardView;

    if-nez v4, :cond_a

    .line 341
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    const v5, 0x7f0300cd

    .line 342
    invoke-virtual {v4, v5, p1, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/navdy/client/app/ui/customviews/PendingTripCardView;

    iput-object v4, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->pendingTripCardView:Lcom/navdy/client/app/ui/customviews/PendingTripCardView;

    .line 344
    :cond_a
    sget-object v4, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "viewType for Pending Route found: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 345
    new-instance v4, Lcom/navdy/client/app/ui/homescreen/PendingTripViewHolder;

    iget-object v5, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->pendingTripCardView:Lcom/navdy/client/app/ui/customviews/PendingTripCardView;

    invoke-direct {v4, v5}, Lcom/navdy/client/app/ui/homescreen/PendingTripViewHolder;-><init>(Landroid/view/View;)V

    goto/16 :goto_0

    .line 350
    :cond_b
    sget-object v4, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->SECTION_HEADER:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    invoke-virtual {v4}, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->ordinal()I

    move-result v4

    if-ne p2, v4, :cond_c

    .line 352
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    const v5, 0x7f030100

    .line 353
    invoke-virtual {v4, v5, p1, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 355
    .restart local v3    # "v":Landroid/view/View;
    new-instance v4, Lcom/navdy/client/app/ui/homescreen/SectionHeaderViewHolder;

    invoke-direct {v4, v3}, Lcom/navdy/client/app/ui/homescreen/SectionHeaderViewHolder;-><init>(Landroid/view/View;)V

    goto/16 :goto_0

    .line 359
    .end local v3    # "v":Landroid/view/View;
    :cond_c
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    const v5, 0x7f0300b2

    .line 360
    invoke-virtual {v4, v5, p1, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 364
    .restart local v3    # "v":Landroid/view/View;
    invoke-direct {p0, p2}, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->isRoutedSuggestion(I)Z

    move-result v4

    if-eqz v4, :cond_d

    .line 365
    new-instance v4, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;

    invoke-direct {v4, v3}, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;-><init>(Landroid/view/View;)V

    goto/16 :goto_0

    .line 368
    :cond_d
    new-instance v4, Lcom/navdy/client/app/ui/homescreen/SuggestionsViewHolder;

    invoke-direct {v4, v3}, Lcom/navdy/client/app/ui/homescreen/SuggestionsViewHolder;-><init>(Landroid/view/View;)V

    goto/16 :goto_0
.end method

.method public onDetachedFromRecyclerView(Landroid/support/v7/widget/RecyclerView;)V
    .locals 1
    .param p1, "recyclerView"    # Landroid/support/v7/widget/RecyclerView;

    .prologue
    .line 203
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->navdyRouteHandler:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

    invoke-virtual {v0, p0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->removeListener(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteListener;)V

    .line 204
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->clearAllAnimations()V

    .line 205
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->stopRefreshingAllEtas()V

    .line 206
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->recyclerView:Landroid/support/v7/widget/RecyclerView;

    .line 207
    invoke-super {p0, p1}, Landroid/support/v7/widget/RecyclerView$Adapter;->onDetachedFromRecyclerView(Landroid/support/v7/widget/RecyclerView;)V

    .line 208
    return-void
.end method

.method public onFailedToRecycleView(Landroid/support/v7/widget/RecyclerView$ViewHolder;)Z
    .locals 1
    .param p1, "holder"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;

    .prologue
    .line 218
    invoke-direct {p0, p1}, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->clearAnimationIfRunning(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V

    .line 219
    invoke-super {p0, p1}, Landroid/support/v7/widget/RecyclerView$Adapter;->onFailedToRecycleView(Landroid/support/v7/widget/RecyclerView$ViewHolder;)Z

    move-result v0

    return v0
.end method

.method public onLongClick(Landroid/view/View;)Z
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 499
    const/4 v0, 0x0

    return v0
.end method

.method public onPendingRouteCalculated(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$Error;Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)V
    .locals 0
    .param p1, "error"    # Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$Error;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "pendingRoute"    # Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 513
    return-void
.end method

.method public onPendingRouteCalculating(Lcom/navdy/client/app/framework/models/Destination;)V
    .locals 0
    .param p1, "destination"    # Lcom/navdy/client/app/framework/models/Destination;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 507
    invoke-direct {p0, p1}, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->addPendingTripCard(Lcom/navdy/client/app/framework/models/Destination;)V

    .line 508
    return-void
.end method

.method public onReroute()V
    .locals 0

    .prologue
    .line 538
    return-void
.end method

.method public onRouteArrived(Lcom/navdy/client/app/framework/models/Destination;)V
    .locals 0
    .param p1, "destination"    # Lcom/navdy/client/app/framework/models/Destination;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 541
    return-void
.end method

.method public onRouteCalculated(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$Error;Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)V
    .locals 0
    .param p1, "error"    # Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$Error;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "route"    # Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 523
    return-void
.end method

.method public onRouteCalculating(Lcom/navdy/client/app/framework/models/Destination;)V
    .locals 0
    .param p1, "destination"    # Lcom/navdy/client/app/framework/models/Destination;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 518
    return-void
.end method

.method public onRouteStarted(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)V
    .locals 1
    .param p1, "route"    # Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 528
    invoke-virtual {p1}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->getDestination()Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->addActiveTripCard(Lcom/navdy/client/app/framework/models/Destination;)V

    .line 529
    return-void
.end method

.method public onStopRoute()V
    .locals 0

    .prologue
    .line 546
    invoke-direct {p0}, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->removeTripCard()V

    .line 547
    return-void
.end method

.method onSuggestionBuildComplete(Ljava/util/ArrayList;)V
    .locals 0
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/client/app/framework/models/Suggestion;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 104
    .local p1, "suggestions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/client/app/framework/models/Suggestion;>;"
    iput-object p1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->suggestions:Ljava/util/ArrayList;

    .line 105
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->notifyDataSetChanged()V

    .line 106
    return-void
.end method

.method public onTripProgress(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)V
    .locals 0
    .param p1, "progress"    # Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 534
    return-void
.end method

.method public onViewRecycled(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V
    .locals 0
    .param p1, "holder"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;

    .prologue
    .line 212
    invoke-direct {p0, p1}, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->clearAnimationIfRunning(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V

    .line 213
    invoke-super {p0, p1}, Landroid/support/v7/widget/RecyclerView$Adapter;->onViewRecycled(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V

    .line 214
    return-void
.end method

.method rebuildSuggestions()V
    .locals 2
    .annotation build Landroid/support/annotation/MainThread;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 112
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->stopRefreshingAllEtas()V

    .line 114
    invoke-static {}, Lcom/navdy/client/app/framework/util/SystemUtils;->ensureOnMainThread()V

    .line 120
    iput-object v1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->selectedItem:Landroid/view/View;

    .line 121
    const/4 v0, -0x1

    iput v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->selectedItemPosition:I

    .line 122
    iput-object v1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->selectedSuggestionType:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    .line 125
    invoke-static {}, Lcom/navdy/client/app/framework/util/SuggestionManager;->getEmptySuggestionState()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->suggestions:Ljava/util/ArrayList;

    .line 126
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->notifyDataSetChanged()V

    .line 128
    invoke-static {}, Lcom/navdy/client/app/framework/util/SuggestionManager;->rebuildSuggestionListAndSendToHudAsync()V

    .line 130
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->pendingTripCardView:Lcom/navdy/client/app/ui/customviews/PendingTripCardView;

    if-eqz v0, :cond_0

    .line 131
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->pendingTripCardView:Lcom/navdy/client/app/ui/customviews/PendingTripCardView;

    invoke-static {}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->getInstance()Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->getCurrentDestination()Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/ui/customviews/PendingTripCardView;->setNameAddressAndIcon(Lcom/navdy/client/app/framework/models/Destination;)V

    .line 133
    :cond_0
    return-void
.end method

.method setClickListener(Lcom/navdy/client/app/framework/util/CustomItemClickListener;)V
    .locals 0
    .param p1, "onClickListener"    # Lcom/navdy/client/app/framework/util/CustomItemClickListener;

    .prologue
    .line 136
    iput-object p1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->listener:Lcom/navdy/client/app/framework/util/CustomItemClickListener;

    .line 137
    return-void
.end method

.method setSelectedSuggestion(Landroid/view/View;Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;I)V
    .locals 2
    .param p1, "selectedCardRow"    # Landroid/view/View;
    .param p2, "suggestionType"    # Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;
    .param p3, "actionModePosition"    # I

    .prologue
    .line 158
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->selectedItem:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 159
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->selectedItem:Landroid/view/View;

    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->selectedSuggestionType:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    invoke-direct {p0, v0, v1}, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->setViewToNormalMode(Landroid/view/View;Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;)V

    .line 163
    :cond_0
    iput-object p1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->selectedItem:Landroid/view/View;

    .line 164
    iput p3, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->selectedItemPosition:I

    .line 165
    iput-object p2, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->selectedSuggestionType:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    .line 168
    if-eqz p1, :cond_1

    .line 169
    invoke-direct {p0, p1, p2}, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->setViewToSelectedMode(Landroid/view/View;Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;)V

    .line 171
    :cond_1
    return-void
.end method

.method setSuggestionLongClickListener(Lcom/navdy/client/app/framework/util/CustomSuggestionsLongItemClickListener;)V
    .locals 0
    .param p1, "onLongClickListener"    # Lcom/navdy/client/app/framework/util/CustomSuggestionsLongItemClickListener;

    .prologue
    .line 140
    iput-object p1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->longClickListener:Lcom/navdy/client/app/framework/util/CustomSuggestionsLongItemClickListener;

    .line 141
    return-void
.end method

.method stopRefreshingAllEtas()V
    .locals 2

    .prologue
    .line 252
    sget-object v0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Removing all ETA refreshers"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 253
    sget-object v0, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;->handler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 254
    return-void
.end method
