.class Lcom/navdy/client/app/ui/homescreen/SectionHeaderViewHolder;
.super Landroid/support/v7/widget/RecyclerView$ViewHolder;
.source "SectionHeaderViewHolder.java"


# instance fields
.field chevron:Landroid/widget/ImageView;

.field title:Landroid/widget/TextView;


# direct methods
.method constructor <init>(Landroid/view/View;)V
    .locals 1
    .param p1, "itemView"    # Landroid/view/View;

    .prologue
    .line 20
    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 21
    const v0, 0x7f10007f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SectionHeaderViewHolder;->title:Landroid/widget/TextView;

    .line 22
    const v0, 0x7f1000b8

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SectionHeaderViewHolder;->chevron:Landroid/widget/ImageView;

    .line 23
    return-void
.end method
