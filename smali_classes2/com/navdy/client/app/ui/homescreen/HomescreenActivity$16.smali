.class Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$16;
.super Landroid/os/AsyncTask;
.source "HomescreenActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->handleIntent(Landroid/content/Intent;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lcom/navdy/client/app/framework/models/Destination;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

.field final synthetic val$callingIntent:Landroid/content/Intent;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;Landroid/content/Intent;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    .prologue
    .line 1172
    iput-object p1, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$16;->this$0:Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    iput-object p2, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$16;->val$callingIntent:Landroid/content/Intent;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Lcom/navdy/client/app/framework/models/Destination;
    .locals 4
    .param p1, "voids"    # [Ljava/lang/Void;

    .prologue
    .line 1175
    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$16;->val$callingIntent:Landroid/content/Intent;

    const-string v2, "extra_destination"

    const/4 v3, -0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 1176
    .local v0, "id":I
    invoke-static {v0}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getThisDestination(I)Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v1

    return-object v1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1172
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$16;->doInBackground([Ljava/lang/Void;)Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Lcom/navdy/client/app/framework/models/Destination;)V
    .locals 1
    .param p1, "destination"    # Lcom/navdy/client/app/framework/models/Destination;

    .prologue
    .line 1181
    if-eqz p1, :cond_0

    .line 1182
    invoke-static {}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->getInstance()Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->requestNewRoute(Lcom/navdy/client/app/framework/models/Destination;)V

    .line 1184
    :cond_0
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1172
    check-cast p1, Lcom/navdy/client/app/framework/models/Destination;

    invoke-virtual {p0, p1}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$16;->onPostExecute(Lcom/navdy/client/app/framework/models/Destination;)V

    return-void
.end method
