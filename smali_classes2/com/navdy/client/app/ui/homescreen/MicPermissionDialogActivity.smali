.class public Lcom/navdy/client/app/ui/homescreen/MicPermissionDialogActivity;
.super Lcom/navdy/client/app/ui/base/BaseActivity;
.source "MicPermissionDialogActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/navdy/client/app/ui/base/BaseActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public onButtonClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 24
    new-instance v1, Lcom/navdy/client/app/ui/homescreen/MicPermissionDialogActivity$1;

    invoke-direct {v1, p0, p1}, Lcom/navdy/client/app/ui/homescreen/MicPermissionDialogActivity$1;-><init>(Lcom/navdy/client/app/ui/homescreen/MicPermissionDialogActivity;Landroid/view/View;)V

    new-instance v2, Lcom/navdy/client/app/ui/homescreen/MicPermissionDialogActivity$2;

    invoke-direct {v2, p0}, Lcom/navdy/client/app/ui/homescreen/MicPermissionDialogActivity$2;-><init>(Lcom/navdy/client/app/ui/homescreen/MicPermissionDialogActivity;)V

    invoke-virtual {p0, v1, v2}, Lcom/navdy/client/app/ui/homescreen/MicPermissionDialogActivity;->requestMicrophonePermission(Ljava/lang/Runnable;Ljava/lang/Runnable;)V

    .line 47
    const v1, 0x7f100143

    invoke-virtual {p0, v1}, Lcom/navdy/client/app/ui/homescreen/MicPermissionDialogActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    .line 48
    .local v0, "dialog":Landroid/widget/RelativeLayout;
    if-eqz v0, :cond_0

    .line 52
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setAlpha(F)V

    .line 54
    :cond_0
    return-void
.end method

.method public onCloseClick(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 57
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/homescreen/MicPermissionDialogActivity;->isActivityDestroyed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 58
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/homescreen/MicPermissionDialogActivity;->finish()V

    .line 60
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 18
    invoke-super {p0, p1}, Lcom/navdy/client/app/ui/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 20
    const v0, 0x7f030051

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/homescreen/MicPermissionDialogActivity;->setContentView(I)V

    .line 21
    return-void
.end method
