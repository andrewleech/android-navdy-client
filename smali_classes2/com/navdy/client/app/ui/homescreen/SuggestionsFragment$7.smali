.class Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$7;
.super Ljava/lang/Object;
.source "SuggestionsFragment.java"

# interfaces
.implements Lcom/google/android/gms/maps/OnMapReadyCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->onRouteArrived(Lcom/navdy/client/app/framework/models/Destination;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

.field final synthetic val$destination:Lcom/navdy/client/app/framework/models/Destination;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;Lcom/navdy/client/app/framework/models/Destination;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    .prologue
    .line 614
    iput-object p1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$7;->this$0:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    iput-object p2, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$7;->val$destination:Lcom/navdy/client/app/framework/models/Destination;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMapReady(Lcom/google/android/gms/maps/GoogleMap;)V
    .locals 6
    .param p1, "googleMap"    # Lcom/google/android/gms/maps/GoogleMap;

    .prologue
    .line 617
    iget-object v2, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$7;->val$destination:Lcom/navdy/client/app/framework/models/Destination;

    invoke-virtual {v2}, Lcom/navdy/client/app/framework/models/Destination;->getRecentPinAsset()I

    move-result v2

    invoke-static {v2}, Lcom/google/android/gms/maps/model/BitmapDescriptorFactory;->fromResource(I)Lcom/google/android/gms/maps/model/BitmapDescriptor;

    move-result-object v0

    .line 618
    .local v0, "icon":Lcom/google/android/gms/maps/model/BitmapDescriptor;
    new-instance v1, Lcom/google/android/gms/maps/model/LatLng;

    iget-object v2, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$7;->val$destination:Lcom/navdy/client/app/framework/models/Destination;

    invoke-virtual {v2}, Lcom/navdy/client/app/framework/models/Destination;->getDisplayLat()D

    move-result-wide v2

    iget-object v4, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$7;->val$destination:Lcom/navdy/client/app/framework/models/Destination;

    invoke-virtual {v4}, Lcom/navdy/client/app/framework/models/Destination;->getDisplayLng()D

    move-result-wide v4

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    .line 620
    .local v1, "position":Lcom/google/android/gms/maps/model/LatLng;
    iget-object v2, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$7;->this$0:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    invoke-static {v2}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->access$400(Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;)Lcom/google/android/gms/maps/model/Marker;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 621
    iget-object v2, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$7;->this$0:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    invoke-static {v2}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->access$400(Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;)Lcom/google/android/gms/maps/model/Marker;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/android/gms/maps/model/Marker;->setIcon(Lcom/google/android/gms/maps/model/BitmapDescriptor;)V

    .line 622
    iget-object v2, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$7;->this$0:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    invoke-static {v2}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->access$400(Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;)Lcom/google/android/gms/maps/model/Marker;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/android/gms/maps/model/Marker;->setPosition(Lcom/google/android/gms/maps/model/LatLng;)V

    .line 630
    :goto_0
    return-void

    .line 624
    :cond_0
    iget-object v2, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$7;->this$0:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    new-instance v3, Lcom/google/android/gms/maps/model/MarkerOptions;

    invoke-direct {v3}, Lcom/google/android/gms/maps/model/MarkerOptions;-><init>()V

    .line 625
    invoke-virtual {v3, v1}, Lcom/google/android/gms/maps/model/MarkerOptions;->position(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/MarkerOptions;

    move-result-object v3

    .line 626
    invoke-static {}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->access$500()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/gms/maps/model/MarkerOptions;->title(Ljava/lang/String;)Lcom/google/android/gms/maps/model/MarkerOptions;

    move-result-object v3

    .line 627
    invoke-virtual {v3, v0}, Lcom/google/android/gms/maps/model/MarkerOptions;->icon(Lcom/google/android/gms/maps/model/BitmapDescriptor;)Lcom/google/android/gms/maps/model/MarkerOptions;

    move-result-object v3

    .line 624
    invoke-virtual {p1, v3}, Lcom/google/android/gms/maps/GoogleMap;->addMarker(Lcom/google/android/gms/maps/model/MarkerOptions;)Lcom/google/android/gms/maps/model/Marker;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->access$402(Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;Lcom/google/android/gms/maps/model/Marker;)Lcom/google/android/gms/maps/model/Marker;

    goto :goto_0
.end method
