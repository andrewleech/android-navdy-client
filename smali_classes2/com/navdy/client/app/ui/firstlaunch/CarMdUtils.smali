.class Lcom/navdy/client/app/ui/firstlaunch/CarMdUtils;
.super Ljava/lang/Object;
.source "CarMdUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/app/ui/firstlaunch/CarMdUtils$ObdLocation;
    }
.end annotation


# static fields
.field private static final CACHE_TIMEOUT:J

.field private static final CRIME_PHOTO_BUCKET:Ljava/lang/String; = "navdy-prod-release"

.field private static final CRIME_PHOTO_KEY_BASE:Ljava/lang/String; = "obd-locations/"

.field private static final FILE_EXTRENSION:Ljava/lang/String; = ".jpg"

.field private static final NB_CELLS_PER_LINE:I = 0x6

.field private static volatile cachedYearMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/navdy/client/app/ui/firstlaunch/CarMdUtils$ObdLocation;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private static handler:Landroid/os/Handler;

.field private static logger:Lcom/navdy/service/library/log/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 39
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/ui/firstlaunch/CarMdUtils;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/app/ui/firstlaunch/CarMdUtils;->logger:Lcom/navdy/service/library/log/Logger;

    .line 46
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x5

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/navdy/client/app/ui/firstlaunch/CarMdUtils;->CACHE_TIMEOUT:J

    .line 48
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lcom/navdy/client/app/ui/firstlaunch/CarMdUtils;->handler:Landroid/os/Handler;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 38
    sget-object v0, Lcom/navdy/client/app/ui/firstlaunch/CarMdUtils;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$100()V
    .locals 0

    .prologue
    .line 38
    invoke-static {}, Lcom/navdy/client/app/ui/firstlaunch/CarMdUtils;->clearCache()V

    return-void
.end method

.method static declared-synchronized buildCarList()Ljava/util/HashMap;
    .locals 26
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/navdy/client/app/ui/firstlaunch/CarMdUtils$ObdLocation;",
            ">;>;>;"
        }
    .end annotation

    .prologue
    .line 112
    const-class v23, Lcom/navdy/client/app/ui/firstlaunch/CarMdUtils;

    monitor-enter v23

    :try_start_0
    invoke-static {}, Lcom/navdy/client/app/framework/util/SystemUtils;->ensureNotOnMainThread()V

    .line 114
    sget-object v22, Lcom/navdy/client/app/ui/firstlaunch/CarMdUtils;->cachedYearMap:Ljava/util/HashMap;

    if-eqz v22, :cond_0

    .line 115
    invoke-static {}, Lcom/navdy/client/app/ui/firstlaunch/CarMdUtils;->resetClearingTimer()V

    .line 116
    sget-object v21, Lcom/navdy/client/app/ui/firstlaunch/CarMdUtils;->cachedYearMap:Ljava/util/HashMap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 205
    .local v4, "context":Landroid/content/Context;
    .local v7, "inputStream":Ljava/io/InputStream;
    .local v8, "ioException":Ljava/io/IOException;
    .local v19, "sc":Ljava/util/Scanner;
    .local v21, "yearMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/HashMap<Ljava/lang/String;Ljava/util/HashMap<Ljava/lang/String;Lcom/navdy/client/app/ui/firstlaunch/CarMdUtils$ObdLocation;>;>;>;"
    :goto_0
    monitor-exit v23

    return-object v21

    .line 119
    .end local v4    # "context":Landroid/content/Context;
    .end local v7    # "inputStream":Ljava/io/InputStream;
    .end local v8    # "ioException":Ljava/io/IOException;
    .end local v19    # "sc":Ljava/util/Scanner;
    .end local v21    # "yearMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/HashMap<Ljava/lang/String;Ljava/util/HashMap<Ljava/lang/String;Lcom/navdy/client/app/ui/firstlaunch/CarMdUtils$ObdLocation;>;>;>;"
    :cond_0
    const/4 v7, 0x0

    .line 120
    .restart local v7    # "inputStream":Ljava/io/InputStream;
    const/16 v18, 0x0

    .line 121
    .local v18, "sc":Ljava/util/Scanner;
    :try_start_1
    new-instance v21, Ljava/util/HashMap;

    invoke-direct/range {v21 .. v21}, Ljava/util/HashMap;-><init>()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 124
    .restart local v21    # "yearMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/HashMap<Ljava/lang/String;Ljava/util/HashMap<Ljava/lang/String;Lcom/navdy/client/app/ui/firstlaunch/CarMdUtils$ObdLocation;>;>;>;"
    :try_start_2
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v4

    .line 125
    .restart local v4    # "context":Landroid/content/Context;
    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v22

    const v24, 0x7f07000f

    move-object/from16 v0, v22

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v7

    .line 126
    new-instance v19, Ljava/util/Scanner;

    const-string v22, "UTF-8"

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-direct {v0, v7, v1}, Ljava/util/Scanner;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 129
    .end local v18    # "sc":Ljava/util/Scanner;
    .restart local v19    # "sc":Ljava/util/Scanner;
    :try_start_3
    invoke-virtual/range {v19 .. v19}, Ljava/util/Scanner;->hasNextLine()Z

    move-result v22

    if-eqz v22, :cond_1

    .line 130
    invoke-virtual/range {v19 .. v19}, Ljava/util/Scanner;->nextLine()Ljava/lang/String;

    .line 133
    :cond_1
    :goto_1
    invoke-virtual/range {v19 .. v19}, Ljava/util/Scanner;->hasNextLine()Z

    move-result v22

    if-eqz v22, :cond_7

    .line 134
    invoke-virtual/range {v19 .. v19}, Ljava/util/Scanner;->nextLine()Ljava/lang/String;

    move-result-object v9

    .line 137
    .local v9, "line":Ljava/lang/String;
    const-string v22, "\"\"\"Driver Side - Left of Steering Wheel, above Hood Release\"\"\""

    move-object/from16 v0, v22

    invoke-virtual {v9, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v22

    if-eqz v22, :cond_2

    .line 138
    const-string v22, "\"\"\"Driver Side - Left of Steering Wheel, above Hood Release\"\"\""

    const-string v24, "Driver Side - Left of Steering Wheel above Hood Release"

    move-object/from16 v0, v22

    move-object/from16 v1, v24

    invoke-virtual {v9, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v9

    .line 140
    :cond_2
    const-string v22, ","

    const/16 v24, -0x1

    move-object/from16 v0, v22

    move/from16 v1, v24

    invoke-virtual {v9, v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v3

    .line 141
    .local v3, "cells":[Ljava/lang/String;
    array-length v0, v3

    move/from16 v22, v0

    const/16 v24, 0x6

    move/from16 v0, v22

    move/from16 v1, v24

    if-ge v0, v1, :cond_3

    .line 142
    new-instance v22, Ljava/lang/RuntimeException;

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "Unable to parse the obd car data! Not enough cells in this line:"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v22

    move-object/from16 v1, v24

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v22
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 197
    .end local v3    # "cells":[Ljava/lang/String;
    .end local v9    # "line":Ljava/lang/String;
    :catchall_0
    move-exception v22

    move-object/from16 v18, v19

    .end local v4    # "context":Landroid/content/Context;
    .end local v19    # "sc":Ljava/util/Scanner;
    .restart local v18    # "sc":Ljava/util/Scanner;
    :goto_2
    :try_start_4
    invoke-static {v7}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 198
    invoke-static/range {v18 .. v18}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v22
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 112
    .end local v18    # "sc":Ljava/util/Scanner;
    .end local v21    # "yearMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/HashMap<Ljava/lang/String;Ljava/util/HashMap<Ljava/lang/String;Lcom/navdy/client/app/ui/firstlaunch/CarMdUtils$ObdLocation;>;>;>;"
    :catchall_1
    move-exception v22

    monitor-exit v23

    throw v22

    .line 144
    .restart local v3    # "cells":[Ljava/lang/String;
    .restart local v4    # "context":Landroid/content/Context;
    .restart local v9    # "line":Ljava/lang/String;
    .restart local v19    # "sc":Ljava/util/Scanner;
    .restart local v21    # "yearMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/HashMap<Ljava/lang/String;Ljava/util/HashMap<Ljava/lang/String;Lcom/navdy/client/app/ui/firstlaunch/CarMdUtils$ObdLocation;>;>;>;"
    :cond_3
    const/16 v22, 0x0

    :try_start_5
    aget-object v11, v3, v22

    .line 145
    .local v11, "make":Ljava/lang/String;
    const/16 v22, 0x1

    aget-object v13, v3, v22

    .line 146
    .local v13, "model":Ljava/lang/String;
    const/16 v22, 0x2

    aget-object v20, v3, v22

    .line 147
    .local v20, "year":Ljava/lang/String;
    const/16 v22, 0x3

    aget-object v17, v3, v22

    .line 148
    .local v17, "position":Ljava/lang/String;
    const/16 v22, 0x4

    aget-object v2, v3, v22

    .line 149
    .local v2, "accessNote":Ljava/lang/String;
    const/16 v22, 0x5

    aget-object v15, v3, v22
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 153
    .local v15, "note":Ljava/lang/String;
    :try_start_6
    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move-result v22

    add-int/lit8 v10, v22, -0x1

    .line 158
    .local v10, "location":I
    :goto_3
    :try_start_7
    new-instance v16, Lcom/navdy/client/app/ui/firstlaunch/CarMdUtils$ObdLocation;

    move-object/from16 v0, v16

    invoke-direct {v0, v10, v2, v15}, Lcom/navdy/client/app/ui/firstlaunch/CarMdUtils$ObdLocation;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    .line 159
    .local v16, "obdLocation":Lcom/navdy/client/app/ui/firstlaunch/CarMdUtils$ObdLocation;
    new-instance v14, Ljava/util/HashMap;

    invoke-direct {v14}, Ljava/util/HashMap;-><init>()V

    .line 160
    .local v14, "modelMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/navdy/client/app/ui/firstlaunch/CarMdUtils$ObdLocation;>;"
    move-object/from16 v0, v21

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/util/HashMap;

    .line 162
    .local v12, "makeMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/HashMap<Ljava/lang/String;Lcom/navdy/client/app/ui/firstlaunch/CarMdUtils$ObdLocation;>;>;"
    if-nez v12, :cond_5

    .line 164
    new-instance v12, Ljava/util/HashMap;

    .end local v12    # "makeMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/HashMap<Ljava/lang/String;Lcom/navdy/client/app/ui/firstlaunch/CarMdUtils$ObdLocation;>;>;"
    invoke-direct {v12}, Ljava/util/HashMap;-><init>()V

    .line 187
    .restart local v12    # "makeMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/HashMap<Ljava/lang/String;Lcom/navdy/client/app/ui/firstlaunch/CarMdUtils$ObdLocation;>;>;"
    :cond_4
    :goto_4
    move-object/from16 v0, v16

    invoke-virtual {v14, v13, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 188
    invoke-virtual {v12, v11, v14}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 189
    move-object/from16 v0, v21

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v12}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 154
    .end local v10    # "location":I
    .end local v12    # "makeMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/HashMap<Ljava/lang/String;Lcom/navdy/client/app/ui/firstlaunch/CarMdUtils$ObdLocation;>;>;"
    .end local v14    # "modelMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/navdy/client/app/ui/firstlaunch/CarMdUtils$ObdLocation;>;"
    .end local v16    # "obdLocation":Lcom/navdy/client/app/ui/firstlaunch/CarMdUtils$ObdLocation;
    :catch_0
    move-exception v5

    .line 155
    .local v5, "e":Ljava/lang/Exception;
    sget v10, Lcom/navdy/client/app/ui/firstlaunch/CarInfoActivity;->DEFAULT_OBD_LOCATION:I

    .restart local v10    # "location":I
    goto :goto_3

    .line 167
    .end local v5    # "e":Ljava/lang/Exception;
    .restart local v12    # "makeMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/HashMap<Ljava/lang/String;Lcom/navdy/client/app/ui/firstlaunch/CarMdUtils$ObdLocation;>;>;"
    .restart local v14    # "modelMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/navdy/client/app/ui/firstlaunch/CarMdUtils$ObdLocation;>;"
    .restart local v16    # "obdLocation":Lcom/navdy/client/app/ui/firstlaunch/CarMdUtils$ObdLocation;
    :cond_5
    invoke-virtual {v12, v11}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    .end local v14    # "modelMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/navdy/client/app/ui/firstlaunch/CarMdUtils$ObdLocation;>;"
    check-cast v14, Ljava/util/HashMap;

    .line 168
    .restart local v14    # "modelMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/navdy/client/app/ui/firstlaunch/CarMdUtils$ObdLocation;>;"
    if-nez v14, :cond_6

    .line 170
    new-instance v14, Ljava/util/HashMap;

    .end local v14    # "modelMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/navdy/client/app/ui/firstlaunch/CarMdUtils$ObdLocation;>;"
    invoke-direct {v14}, Ljava/util/HashMap;-><init>()V

    .restart local v14    # "modelMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/navdy/client/app/ui/firstlaunch/CarMdUtils$ObdLocation;>;"
    goto :goto_4

    .line 173
    :cond_6
    invoke-virtual {v14, v13}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/navdy/client/app/ui/firstlaunch/CarMdUtils$ObdLocation;

    .line 174
    .local v6, "existingLocation":Lcom/navdy/client/app/ui/firstlaunch/CarMdUtils$ObdLocation;
    if-eqz v6, :cond_4

    .line 176
    new-instance v22, Ljava/lang/RuntimeException;

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "Duplicates in the list of obd locations for: year["

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, "]"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, " make["

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, "]"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, " model["

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, "]"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, " location["

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, "]"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, " accessNote["

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, "]"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, " note["

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, "]"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v22

    move-object/from16 v1, v24

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v22

    .line 192
    .end local v2    # "accessNote":Ljava/lang/String;
    .end local v3    # "cells":[Ljava/lang/String;
    .end local v6    # "existingLocation":Lcom/navdy/client/app/ui/firstlaunch/CarMdUtils$ObdLocation;
    .end local v9    # "line":Ljava/lang/String;
    .end local v10    # "location":I
    .end local v11    # "make":Ljava/lang/String;
    .end local v12    # "makeMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/HashMap<Ljava/lang/String;Lcom/navdy/client/app/ui/firstlaunch/CarMdUtils$ObdLocation;>;>;"
    .end local v13    # "model":Ljava/lang/String;
    .end local v14    # "modelMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/navdy/client/app/ui/firstlaunch/CarMdUtils$ObdLocation;>;"
    .end local v15    # "note":Ljava/lang/String;
    .end local v16    # "obdLocation":Lcom/navdy/client/app/ui/firstlaunch/CarMdUtils$ObdLocation;
    .end local v17    # "position":Ljava/lang/String;
    .end local v20    # "year":Ljava/lang/String;
    :cond_7
    invoke-virtual/range {v19 .. v19}, Ljava/util/Scanner;->ioException()Ljava/io/IOException;

    move-result-object v8

    .line 193
    .restart local v8    # "ioException":Ljava/io/IOException;
    if-eqz v8, :cond_8

    .line 194
    new-instance v22, Ljava/lang/RuntimeException;

    const-string v24, "Unable to parse the obd car data!"

    move-object/from16 v0, v22

    move-object/from16 v1, v24

    invoke-direct {v0, v1, v8}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v22
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 197
    :cond_8
    :try_start_8
    invoke-static {v7}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 198
    invoke-static/range {v19 .. v19}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 201
    sput-object v21, Lcom/navdy/client/app/ui/firstlaunch/CarMdUtils;->cachedYearMap:Ljava/util/HashMap;

    .line 202
    invoke-static {}, Lcom/navdy/client/app/ui/firstlaunch/CarMdUtils;->resetClearingTimer()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto/16 :goto_0

    .line 197
    .end local v4    # "context":Landroid/content/Context;
    .end local v8    # "ioException":Ljava/io/IOException;
    .end local v19    # "sc":Ljava/util/Scanner;
    .restart local v18    # "sc":Ljava/util/Scanner;
    :catchall_2
    move-exception v22

    goto/16 :goto_2
.end method

.method private static declared-synchronized clearCache()V
    .locals 2

    .prologue
    .line 302
    const-class v0, Lcom/navdy/client/app/ui/firstlaunch/CarMdUtils;

    monitor-enter v0

    const/4 v1, 0x0

    :try_start_0
    sput-object v1, Lcom/navdy/client/app/ui/firstlaunch/CarMdUtils;->cachedYearMap:Ljava/util/HashMap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 303
    monitor-exit v0

    return-void

    .line 302
    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method static downloadObdCrimePhoto(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)V
    .locals 9
    .param p0, "year"    # Ljava/lang/String;
    .param p1, "make"    # Ljava/lang/String;
    .param p2, "model"    # Ljava/lang/String;
    .param p3, "callback"    # Ljava/lang/Runnable;

    .prologue
    .line 73
    invoke-static {p0, p1, p2}, Lcom/navdy/client/app/ui/firstlaunch/CarMdUtils;->getCrimePhotoKey(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 75
    .local v3, "key":Ljava/lang/String;
    invoke-static {}, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->createS3Client()Lcom/amazonaws/services/s3/AmazonS3Client;

    move-result-object v0

    .line 77
    .local v0, "amazonS3Client":Lcom/amazonaws/services/s3/AmazonS3Client;
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    .line 78
    .local v1, "context":Landroid/content/Context;
    new-instance v6, Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferUtility;

    invoke-direct {v6, v0, v1}, Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferUtility;-><init>(Lcom/amazonaws/services/s3/AmazonS3;Landroid/content/Context;)V

    .line 80
    .local v6, "transferUtility":Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferUtility;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "obdImage"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 82
    .local v2, "filePath":Ljava/lang/String;
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 83
    .local v4, "outputFile":Ljava/io/File;
    const-string v7, "navdy-prod-release"

    .line 84
    invoke-virtual {v6, v7, v3, v4}, Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferUtility;->download(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferObserver;

    move-result-object v5

    .line 86
    .local v5, "transferObserver":Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferObserver;
    new-instance v7, Lcom/navdy/client/app/ui/firstlaunch/CarMdUtils$1;

    invoke-direct {v7, p3, v3}, Lcom/navdy/client/app/ui/firstlaunch/CarMdUtils$1;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    invoke-virtual {v5, v7}, Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferObserver;->setTransferListener(Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferListener;)V

    .line 107
    return-void
.end method

.method private static getCrimePhotoKey(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "year"    # Ljava/lang/String;
    .param p1, "make"    # Ljava/lang/String;
    .param p2, "model"    # Ljava/lang/String;

    .prologue
    .line 65
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "obd-locations/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p0, p1, p2}, Lcom/navdy/client/app/ui/firstlaunch/CarMdUtils;->getVehicleName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".jpg"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static getLocalizedAccessNote(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "original"    # Ljava/lang/String;

    .prologue
    .line 209
    invoke-static {p0}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 222
    .end local p0    # "original":Ljava/lang/String;
    .local v0, "context":Landroid/content/Context;
    .local v1, "res":Landroid/content/res/Resources;
    :goto_0
    return-object p0

    .line 213
    .end local v0    # "context":Landroid/content/Context;
    .end local v1    # "res":Landroid/content/res/Resources;
    .restart local p0    # "original":Ljava/lang/String;
    :cond_0
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 214
    .restart local v0    # "context":Landroid/content/Context;
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 216
    .restart local v1    # "res":Landroid/content/res/Resources;
    const/4 v2, -0x1

    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    :cond_1
    :goto_1
    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 218
    :pswitch_0
    const v2, 0x7f080117

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 216
    :sswitch_0
    const-string v3, "covered"

    invoke-virtual {p0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v2, 0x0

    goto :goto_1

    :sswitch_1
    const-string v3, "uncovered"

    invoke-virtual {p0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v2, 0x1

    goto :goto_1

    .line 220
    :pswitch_1
    const v2, 0x7f0804d9

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 216
    nop

    :sswitch_data_0
    .sparse-switch
        -0x60d6ae23 -> :sswitch_1
        0x39214e96 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static getLocalizedNote(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "original"    # Ljava/lang/String;

    .prologue
    .line 227
    invoke-static {p0}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 270
    .end local p0    # "original":Ljava/lang/String;
    .local v0, "context":Landroid/content/Context;
    .local v1, "res":Landroid/content/res/Resources;
    :goto_0
    return-object p0

    .line 231
    .end local v0    # "context":Landroid/content/Context;
    .end local v1    # "res":Landroid/content/res/Resources;
    .restart local p0    # "original":Ljava/lang/String;
    :cond_0
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 232
    .restart local v0    # "context":Landroid/content/Context;
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 234
    .restart local v1    # "res":Landroid/content/res/Resources;
    const/4 v2, -0x1

    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    :cond_1
    :goto_1
    packed-switch v2, :pswitch_data_0

    .line 272
    new-instance v2, Ljava/lang/RuntimeException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unknown location: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 234
    :sswitch_0
    const-string v3, "Driver Side - Left of Steering Wheel above Hood Release"

    invoke-virtual {p0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v2, 0x0

    goto :goto_1

    :sswitch_1
    const-string v3, "Center Compartment - Next to Hand Brake"

    invoke-virtual {p0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v2, 0x1

    goto :goto_1

    :sswitch_2
    const-string v3, "Center Compartment - Under Armrest"

    invoke-virtual {p0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v2, 0x2

    goto :goto_1

    :sswitch_3
    const-string v3, "Center Console  -  Below Radio & A/C Controls"

    invoke-virtual {p0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v2, 0x3

    goto :goto_1

    :sswitch_4
    const-string v3, "Center Console - Above Climate Control"

    invoke-virtual {p0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v2, 0x4

    goto :goto_1

    :sswitch_5
    const-string v3, "Center Console - Behind Ashtray"

    invoke-virtual {p0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v2, 0x5

    goto :goto_1

    :sswitch_6
    const-string v3, "Center Console - Behind Coin Tray"

    invoke-virtual {p0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v2, 0x6

    goto :goto_1

    :sswitch_7
    const-string v3, "Center Console - Behind Fuse Box Cover"

    invoke-virtual {p0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v2, 0x7

    goto :goto_1

    :sswitch_8
    const-string v3, "Center Console - Right Side Of Console"

    invoke-virtual {p0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    const/16 v2, 0x8

    goto :goto_1

    :sswitch_9
    const-string v3, "Center Console - Right Side of Radio"

    invoke-virtual {p0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    const/16 v2, 0x9

    goto/16 :goto_1

    :sswitch_a
    const-string v3, "Driver Side - Kick Panel Behind Fuse Box Cover"

    invoke-virtual {p0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    const/16 v2, 0xa

    goto/16 :goto_1

    :sswitch_b
    const-string v3, "Driver Side - Left Side of Center Console"

    invoke-virtual {p0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    const/16 v2, 0xb

    goto/16 :goto_1

    :sswitch_c
    const-string v3, "Driver Side - Right Side of Steering Wheel"

    invoke-virtual {p0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    const/16 v2, 0xc

    goto/16 :goto_1

    :sswitch_d
    const-string v3, "Driver Side - Under Lower Left Side of Dashboard"

    invoke-virtual {p0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    const/16 v2, 0xd

    goto/16 :goto_1

    :sswitch_e
    const-string v3, "Driver Side - Under Lower Right Side of Dashboard"

    invoke-virtual {p0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    const/16 v2, 0xe

    goto/16 :goto_1

    :sswitch_f
    const-string v3, "Driver Side - Under Steering Wheel Column"

    invoke-virtual {p0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    const/16 v2, 0xf

    goto/16 :goto_1

    :sswitch_10
    const-string v3, "Passenger Side - Under Lower Left Side of Glove Compartment"

    invoke-virtual {p0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    const/16 v2, 0x10

    goto/16 :goto_1

    :sswitch_11
    const-string v3, "Rear Center Console -  Left of Ashtray"

    invoke-virtual {p0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    const/16 v2, 0x11

    goto/16 :goto_1

    .line 236
    :pswitch_0
    const v2, 0x7f080142

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto/16 :goto_0

    .line 238
    :pswitch_1
    const v2, 0x7f0800f2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto/16 :goto_0

    .line 240
    :pswitch_2
    const v2, 0x7f0800f3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto/16 :goto_0

    .line 242
    :pswitch_3
    const v2, 0x7f0800f4

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto/16 :goto_0

    .line 244
    :pswitch_4
    const v2, 0x7f0800f5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto/16 :goto_0

    .line 246
    :pswitch_5
    const v2, 0x7f0800f6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto/16 :goto_0

    .line 248
    :pswitch_6
    const v2, 0x7f0800f7

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto/16 :goto_0

    .line 250
    :pswitch_7
    const v2, 0x7f0800f8

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto/16 :goto_0

    .line 252
    :pswitch_8
    const v2, 0x7f0800f9

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto/16 :goto_0

    .line 254
    :pswitch_9
    const v2, 0x7f0800fa

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto/16 :goto_0

    .line 256
    :pswitch_a
    const v2, 0x7f080141

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto/16 :goto_0

    .line 258
    :pswitch_b
    const v2, 0x7f080143

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto/16 :goto_0

    .line 260
    :pswitch_c
    const v2, 0x7f080144

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto/16 :goto_0

    .line 262
    :pswitch_d
    const v2, 0x7f080145

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto/16 :goto_0

    .line 264
    :pswitch_e
    const v2, 0x7f080146

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto/16 :goto_0

    .line 266
    :pswitch_f
    const v2, 0x7f080147

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto/16 :goto_0

    .line 268
    :pswitch_10
    const v2, 0x7f080330

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto/16 :goto_0

    .line 270
    :pswitch_11
    const v2, 0x7f0803bd

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto/16 :goto_0

    .line 234
    :sswitch_data_0
    .sparse-switch
        -0x70d03570 -> :sswitch_3
        -0x6d108dc8 -> :sswitch_4
        -0x56e2d9da -> :sswitch_0
        -0x4a4f5b02 -> :sswitch_1
        -0x42e7fd24 -> :sswitch_2
        -0x336d3869 -> :sswitch_a
        -0x2b3e9f14 -> :sswitch_8
        -0x13baa43a -> :sswitch_6
        0x14329c21 -> :sswitch_b
        0x1f8c0446 -> :sswitch_d
        0x2cfbb707 -> :sswitch_11
        0x425b983e -> :sswitch_f
        0x4bb09b94 -> :sswitch_c
        0x678dd342 -> :sswitch_7
        0x6a1bbf25 -> :sswitch_e
        0x70c80cb0 -> :sswitch_9
        0x7b37924f -> :sswitch_10
        0x7dcc9f2d -> :sswitch_5
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
    .end packed-switch
.end method

.method static getObdLocation(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/HashMap;)Lcom/navdy/client/app/ui/firstlaunch/CarMdUtils$ObdLocation;
    .locals 4
    .param p0, "year"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p1, "make"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "model"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/util/HashMap;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/navdy/client/app/ui/firstlaunch/CarMdUtils$ObdLocation;",
            ">;>;>;)",
            "Lcom/navdy/client/app/ui/firstlaunch/CarMdUtils$ObdLocation;"
        }
    .end annotation

    .prologue
    .local p3, "yearMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/HashMap<Ljava/lang/String;Ljava/util/HashMap<Ljava/lang/String;Lcom/navdy/client/app/ui/firstlaunch/CarMdUtils$ObdLocation;>;>;>;"
    const/4 v2, 0x0

    .line 314
    if-eqz p1, :cond_0

    if-eqz p0, :cond_0

    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    invoke-virtual {p3}, Ljava/util/HashMap;->size()I

    move-result v3

    if-nez v3, :cond_1

    .line 328
    :cond_0
    :goto_0
    return-object v2

    .line 318
    :cond_1
    invoke-virtual {p3, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    .line 319
    .local v0, "makeMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/HashMap<Ljava/lang/String;Lcom/navdy/client/app/ui/firstlaunch/CarMdUtils$ObdLocation;>;>;"
    if-eqz v0, :cond_0

    .line 323
    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/HashMap;

    .line 324
    .local v1, "modelMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/navdy/client/app/ui/firstlaunch/CarMdUtils$ObdLocation;>;"
    if-eqz v1, :cond_0

    .line 328
    invoke-virtual {v1, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/navdy/client/app/ui/firstlaunch/CarMdUtils$ObdLocation;

    goto :goto_0
.end method

.method private static getVehicleName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "year"    # Ljava/lang/String;
    .param p1, "make"    # Ljava/lang/String;
    .param p2, "model"    # Ljava/lang/String;

    .prologue
    .line 69
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "\\s"

    const-string v2, "-"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static declared-synchronized hasCachedCarList()Z
    .locals 2

    .prologue
    .line 306
    const-class v1, Lcom/navdy/client/app/ui/firstlaunch/CarMdUtils;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/navdy/client/app/ui/firstlaunch/CarMdUtils;->cachedYearMap:Ljava/util/HashMap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static declared-synchronized resetClearingTimer()V
    .locals 6

    .prologue
    .line 287
    const-class v1, Lcom/navdy/client/app/ui/firstlaunch/CarMdUtils;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/navdy/client/app/ui/firstlaunch/CarMdUtils;->handler:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 289
    sget-object v0, Lcom/navdy/client/app/ui/firstlaunch/CarMdUtils;->handler:Landroid/os/Handler;

    new-instance v2, Lcom/navdy/client/app/ui/firstlaunch/CarMdUtils$2;

    invoke-direct {v2}, Lcom/navdy/client/app/ui/firstlaunch/CarMdUtils$2;-><init>()V

    sget-wide v4, Lcom/navdy/client/app/ui/firstlaunch/CarMdUtils;->CACHE_TIMEOUT:J

    invoke-virtual {v0, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 299
    monitor-exit v1

    return-void

    .line 287
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
