.class Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity$3$2$1;
.super Ljava/lang/Object;
.source "VideoPlayerActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity$3$2;->onTimedText(Landroid/media/MediaPlayer;Landroid/media/TimedText;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$2:Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity$3$2;

.field final synthetic val$text:Landroid/media/TimedText;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity$3$2;Landroid/media/TimedText;)V
    .locals 0
    .param p1, "this$2"    # Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity$3$2;

    .prologue
    .line 118
    iput-object p1, p0, Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity$3$2$1;->this$2:Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity$3$2;

    iput-object p2, p0, Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity$3$2$1;->val$text:Landroid/media/TimedText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 121
    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity$3$2$1;->val$text:Landroid/media/TimedText;

    invoke-virtual {v1}, Landroid/media/TimedText;->getText()Ljava/lang/String;

    move-result-object v0

    .line 122
    .local v0, "subtitleLine":Ljava/lang/String;
    invoke-static {v0}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 124
    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 125
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 127
    :cond_0
    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 128
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 130
    :cond_1
    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity$3$2$1;->this$2:Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity$3$2;

    iget-object v1, v1, Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity$3$2;->this$1:Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity$3;

    iget-object v1, v1, Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity$3;->this$0:Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity;

    invoke-static {v1}, Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity;->access$200(Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 131
    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity$3$2$1;->this$2:Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity$3$2;

    iget-object v1, v1, Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity$3$2;->this$1:Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity$3;

    iget-object v1, v1, Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity$3;->this$0:Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity;

    invoke-static {v1}, Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity;->access$200(Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 135
    :goto_0
    return-void

    .line 133
    :cond_2
    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity$3$2$1;->this$2:Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity$3$2;

    iget-object v1, v1, Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity$3$2;->this$1:Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity$3;

    iget-object v1, v1, Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity$3;->this$0:Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity;

    invoke-static {v1}, Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity;->access$200(Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity;)Landroid/widget/TextView;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method
