.class public Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;
.super Landroid/support/v4/app/FragmentStatePagerAdapter;
.source "InstallPagerAdapter.java"


# static fields
.field private static final MEDIUM_TALL_MOUNT_FLOW:[I
    .annotation build Landroid/support/annotation/LayoutRes;
    .end annotation
.end field

.field private static final SHORT_MOUNT_FLOW:[I
    .annotation build Landroid/support/annotation/LayoutRes;
    .end annotation
.end field

.field static final STEP_LENS_CHECK:I = 0x2

.field private static final STEP_MEDIUM_TALL_MOUNT_LOCATE_OBD:I = 0x3

.field private static final STEP_SHORT_MOUNT_LOCATE_OBD:I = 0x4


# instance fields
.field private currentFlow:[I

.field private hasPickedMount:Z

.field private final logger:Lcom/navdy/service/library/log/Logger;

.field private final sharedPreferences:Landroid/content/SharedPreferences;

.field private weHaveCarInfo:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;->SHORT_MOUNT_FLOW:[I

    .line 41
    const/4 v0, 0x7

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;->MEDIUM_TALL_MOUNT_FLOW:[I

    return-void

    .line 30
    :array_0
    .array-data 4
        0x7f030074
        0x7f030076
        0x7f030070
        0x7f030075
        0x7f030071
        0x7f030077
        0x7f030078
        0x7f03006e
    .end array-data

    .line 41
    :array_1
    .array-data 4
        0x7f030074
        0x7f030072
        0x7f030070
        0x7f030071
        0x7f030077
        0x7f030078
        0x7f03006e
    .end array-data
.end method

.method constructor <init>(Landroid/support/v4/app/FragmentManager;)V
    .locals 3
    .param p1, "manager"    # Landroid/support/v4/app/FragmentManager;

    .prologue
    .line 57
    invoke-direct {p0, p1}, Landroid/support/v4/app/FragmentStatePagerAdapter;-><init>(Landroid/support/v4/app/FragmentManager;)V

    .line 24
    new-instance v1, Lcom/navdy/service/library/log/Logger;

    const-class v2, Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;

    invoke-direct {v1, v2}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    iput-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;->logger:Lcom/navdy/service/library/log/Logger;

    .line 51
    sget-object v1, Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;->SHORT_MOUNT_FLOW:[I

    iput-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;->currentFlow:[I

    .line 52
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;->hasPickedMount:Z

    .line 53
    invoke-static {}, Lcom/navdy/client/app/tracking/Tracker;->weHaveCarInfo()Z

    move-result v1

    iput-boolean v1, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;->weHaveCarInfo:Z

    .line 58
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;->sharedPreferences:Landroid/content/SharedPreferences;

    .line 59
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;->getCurrentMountType()Lcom/navdy/client/app/framework/models/MountInfo$MountType;

    move-result-object v0

    .line 60
    .local v0, "currentMountType":Lcom/navdy/client/app/framework/models/MountInfo$MountType;
    invoke-static {v0}, Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;->getInstallFlow(Lcom/navdy/client/app/framework/models/MountInfo$MountType;)[I

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;->currentFlow:[I

    .line 61
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;->notifyDataSetChanged()V

    .line 62
    return-void
.end method

.method static getCurrentMountType(Landroid/content/SharedPreferences;)Lcom/navdy/client/app/framework/models/MountInfo$MountType;
    .locals 3
    .param p0, "sharedPreferences"    # Landroid/content/SharedPreferences;

    .prologue
    .line 152
    if-eqz p0, :cond_0

    .line 154
    :try_start_0
    const-string v1, "mount_type"

    sget-object v2, Lcom/navdy/client/app/framework/models/MountInfo$MountType;->SHORT:Lcom/navdy/client/app/framework/models/MountInfo$MountType;

    .line 156
    invoke-virtual {v2}, Lcom/navdy/client/app/framework/models/MountInfo$MountType;->getValue()Ljava/lang/String;

    move-result-object v2

    .line 154
    invoke-interface {p0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 157
    .local v0, "mountTypeValue":Ljava/lang/String;
    invoke-static {v0}, Lcom/navdy/client/app/framework/models/MountInfo$MountType;->getMountTypeForValue(Ljava/lang/String;)Lcom/navdy/client/app/framework/models/MountInfo$MountType;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 162
    .end local v0    # "mountTypeValue":Ljava/lang/String;
    :goto_0
    return-object v1

    .line 158
    :catch_0
    move-exception v1

    .line 162
    :cond_0
    sget-object v1, Lcom/navdy/client/app/framework/models/MountInfo$MountType;->SHORT:Lcom/navdy/client/app/framework/models/MountInfo$MountType;

    goto :goto_0
.end method

.method static getInstallFlow(Lcom/navdy/client/app/framework/models/MountInfo$MountType;)[I
    .locals 1
    .param p0, "mountType"    # Lcom/navdy/client/app/framework/models/MountInfo$MountType;

    .prologue
    .line 174
    sget-object v0, Lcom/navdy/client/app/framework/models/MountInfo$MountType;->SHORT:Lcom/navdy/client/app/framework/models/MountInfo$MountType;

    if-ne p0, v0, :cond_0

    .line 175
    sget-object v0, Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;->SHORT_MOUNT_FLOW:[I

    .line 177
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;->MEDIUM_TALL_MOUNT_FLOW:[I

    goto :goto_0
.end method

.method static getPositionForStep(I[I)I
    .locals 2
    .param p0, "step"    # I
    .param p1, "currentFlow"    # [I

    .prologue
    .line 95
    if-eqz p1, :cond_1

    .line 96
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_1

    .line 97
    aget v1, p1, v0

    if-ne p0, v1, :cond_0

    .line 102
    .end local v0    # "i":I
    :goto_1
    return v0

    .line 96
    .restart local v0    # "i":I
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 102
    .end local v0    # "i":I
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method static getStepForCurrentPosition(ILcom/navdy/client/app/framework/models/MountInfo$MountType;)I
    .locals 1
    .param p0, "currentPosition"    # I
    .param p1, "currentMountType"    # Lcom/navdy/client/app/framework/models/MountInfo$MountType;

    .prologue
    .line 88
    sget-object v0, Lcom/navdy/client/app/framework/models/MountInfo$MountType;->SHORT:Lcom/navdy/client/app/framework/models/MountInfo$MountType;

    if-ne p1, v0, :cond_0

    .line 89
    sget-object v0, Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;->SHORT_MOUNT_FLOW:[I

    aget v0, v0, p0

    .line 91
    :goto_0
    return v0

    :cond_0
    sget-object v0, Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;->MEDIUM_TALL_MOUNT_FLOW:[I

    aget v0, v0, p0

    goto :goto_0
.end method


# virtual methods
.method public destroyItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 3
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I
    .param p3, "object"    # Ljava/lang/Object;

    .prologue
    .line 138
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/FragmentStatePagerAdapter;->destroyItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V

    .line 139
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Destroying item at position "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 140
    return-void
.end method

.method public getCount()I
    .locals 4

    .prologue
    .line 73
    const/4 v0, 0x0

    .line 74
    .local v0, "hasSkippedInstall":Z
    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;->sharedPreferences:Landroid/content/SharedPreferences;

    if-eqz v1, :cond_0

    .line 75
    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v2, "has_skipped_install"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 78
    :cond_0
    iget-boolean v1, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;->hasPickedMount:Z

    if-nez v1, :cond_1

    if-eqz v0, :cond_4

    .line 79
    :cond_1
    iget-boolean v1, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;->weHaveCarInfo:Z

    if-nez v1, :cond_2

    if-eqz v0, :cond_3

    .line 80
    :cond_2
    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;->currentFlow:[I

    array-length v1, v1

    .line 84
    :goto_0
    return v1

    .line 82
    :cond_3
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;->getLocateObdStepIndex()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 84
    :cond_4
    const/4 v1, 0x3

    goto :goto_0
.end method

.method getCurrentMountType()Lcom/navdy/client/app/framework/models/MountInfo$MountType;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-static {v0}, Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;->getCurrentMountType(Landroid/content/SharedPreferences;)Lcom/navdy/client/app/framework/models/MountInfo$MountType;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getItem(I)Landroid/support/v4/app/Fragment;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 22
    invoke-virtual {p0, p1}, Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;->getItem(I)Lcom/navdy/client/app/ui/firstlaunch/InstallCardFragment;

    move-result-object v0

    return-object v0
.end method

.method public getItem(I)Lcom/navdy/client/app/ui/firstlaunch/InstallCardFragment;
    .locals 3
    .param p1, "position"    # I
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 110
    iget-object v2, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;->currentFlow:[I

    aget v1, v2, p1

    .line 111
    .local v1, "layout":I
    packed-switch v1, :pswitch_data_0

    .line 113
    :pswitch_0
    new-instance v0, Lcom/navdy/client/app/ui/firstlaunch/InstallCardFragment;

    invoke-direct {v0}, Lcom/navdy/client/app/ui/firstlaunch/InstallCardFragment;-><init>()V

    .line 114
    .local v0, "f":Lcom/navdy/client/app/ui/firstlaunch/InstallCardFragment;
    invoke-virtual {v0, v1}, Lcom/navdy/client/app/ui/firstlaunch/InstallCardFragment;->setLayoutId(I)V

    .line 115
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;->getCurrentMountType()Lcom/navdy/client/app/framework/models/MountInfo$MountType;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/navdy/client/app/ui/firstlaunch/InstallCardFragment;->setMountType(Lcom/navdy/client/app/framework/models/MountInfo$MountType;)V

    .line 133
    :goto_0
    return-object v0

    .line 120
    .end local v0    # "f":Lcom/navdy/client/app/ui/firstlaunch/InstallCardFragment;
    :pswitch_1
    new-instance v0, Lcom/navdy/client/app/ui/firstlaunch/InstallMountFragment;

    invoke-direct {v0}, Lcom/navdy/client/app/ui/firstlaunch/InstallMountFragment;-><init>()V

    .line 121
    .restart local v0    # "f":Lcom/navdy/client/app/ui/firstlaunch/InstallCardFragment;
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;->getCurrentMountType()Lcom/navdy/client/app/framework/models/MountInfo$MountType;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/navdy/client/app/ui/firstlaunch/InstallCardFragment;->setMountType(Lcom/navdy/client/app/framework/models/MountInfo$MountType;)V

    goto :goto_0

    .line 125
    .end local v0    # "f":Lcom/navdy/client/app/ui/firstlaunch/InstallCardFragment;
    :pswitch_2
    new-instance v0, Lcom/navdy/client/app/ui/firstlaunch/InstallLensCheckFragment;

    invoke-direct {v0}, Lcom/navdy/client/app/ui/firstlaunch/InstallLensCheckFragment;-><init>()V

    .line 126
    .restart local v0    # "f":Lcom/navdy/client/app/ui/firstlaunch/InstallCardFragment;
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;->getCurrentMountType()Lcom/navdy/client/app/framework/models/MountInfo$MountType;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/navdy/client/app/ui/firstlaunch/InstallCardFragment;->setMountType(Lcom/navdy/client/app/framework/models/MountInfo$MountType;)V

    goto :goto_0

    .line 130
    .end local v0    # "f":Lcom/navdy/client/app/ui/firstlaunch/InstallCardFragment;
    :pswitch_3
    new-instance v0, Lcom/navdy/client/app/ui/firstlaunch/InstallLocateObdFragment;

    invoke-direct {v0}, Lcom/navdy/client/app/ui/firstlaunch/InstallLocateObdFragment;-><init>()V

    .restart local v0    # "f":Lcom/navdy/client/app/ui/firstlaunch/InstallCardFragment;
    goto :goto_0

    .line 111
    nop

    :pswitch_data_0
    .packed-switch 0x7f030070
        :pswitch_2
        :pswitch_3
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method getLocateObdStepIndex()I
    .locals 2

    .prologue
    .line 65
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;->getCurrentMountType()Lcom/navdy/client/app/framework/models/MountInfo$MountType;

    move-result-object v0

    sget-object v1, Lcom/navdy/client/app/framework/models/MountInfo$MountType;->SHORT:Lcom/navdy/client/app/framework/models/MountInfo$MountType;

    if-ne v0, v1, :cond_0

    .line 66
    const/4 v0, 0x4

    .line 68
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x3

    goto :goto_0
.end method

.method getScreenAtPosition(I)Ljava/lang/String;
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 188
    invoke-virtual {p0, p1}, Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;->getItem(I)Lcom/navdy/client/app/ui/firstlaunch/InstallCardFragment;

    move-result-object v0

    .line 189
    .local v0, "f":Lcom/navdy/client/app/ui/firstlaunch/InstallCardFragment;
    invoke-virtual {v0}, Lcom/navdy/client/app/ui/firstlaunch/InstallCardFragment;->getScreen()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method isOnStepAfterCablePicker(I)Z
    .locals 6
    .param p1, "currentItem"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 197
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    .line 198
    .local v1, "sharedPreferences":Landroid/content/SharedPreferences;
    const-string v4, "power_cable_selection"

    const-string v5, "OBD"

    invoke-interface {v1, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 202
    .local v0, "selectedCable":Ljava/lang/String;
    const-string v4, "CLA"

    invoke-static {v0, v4}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 203
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;->getLocateObdStepIndex()I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    if-ne p1, v4, :cond_1

    .line 205
    :cond_0
    :goto_0
    return v2

    :cond_1
    move v2, v3

    .line 203
    goto :goto_0

    .line 205
    :cond_2
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;->getLocateObdStepIndex()I

    move-result v4

    if-eq p1, v4, :cond_0

    move v2, v3

    goto :goto_0
.end method

.method isOnStepAfterLocatingObd(I)Z
    .locals 5
    .param p1, "currentItem"    # I

    .prologue
    const/4 v2, 0x0

    .line 209
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    .line 210
    .local v1, "sharedPreferences":Landroid/content/SharedPreferences;
    const-string v3, "power_cable_selection"

    const-string v4, "OBD"

    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 214
    .local v0, "selectedCable":Ljava/lang/String;
    const-string v3, "CLA"

    invoke-static {v0, v3}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 217
    :cond_0
    :goto_0
    return v2

    :cond_1
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;->getLocateObdStepIndex()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    if-ne p1, v3, :cond_0

    const/4 v2, 0x1

    goto :goto_0
.end method

.method isOnStepBeforeCablePicker(I)Z
    .locals 1
    .param p1, "currentItem"    # I

    .prologue
    .line 193
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;->getLocateObdStepIndex()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public notifyDataSetChanged()V
    .locals 1

    .prologue
    .line 183
    invoke-static {}, Lcom/navdy/client/app/tracking/Tracker;->weHaveCarInfo()Z

    move-result v0

    iput-boolean v0, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;->weHaveCarInfo:Z

    .line 184
    invoke-super {p0}, Landroid/support/v4/app/FragmentStatePagerAdapter;->notifyDataSetChanged()V

    .line 185
    return-void
.end method

.method setCurrentMountType(Lcom/navdy/client/app/framework/models/MountInfo$MountType;)V
    .locals 3
    .param p1, "currentMountType"    # Lcom/navdy/client/app/framework/models/MountInfo$MountType;

    .prologue
    .line 166
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;->sharedPreferences:Landroid/content/SharedPreferences;

    if-eqz v0, :cond_0

    .line 167
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "mount_type"

    invoke-virtual {p1}, Lcom/navdy/client/app/framework/models/MountInfo$MountType;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 169
    :cond_0
    invoke-static {p1}, Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;->getInstallFlow(Lcom/navdy/client/app/framework/models/MountInfo$MountType;)[I

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;->currentFlow:[I

    .line 170
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;->setHasPickedMount(Z)V

    .line 171
    return-void
.end method

.method setHasPickedMount(Z)V
    .locals 0
    .param p1, "hasPickedMount"    # Z

    .prologue
    .line 143
    iput-boolean p1, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;->hasPickedMount:Z

    .line 144
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;->notifyDataSetChanged()V

    .line 145
    return-void
.end method
