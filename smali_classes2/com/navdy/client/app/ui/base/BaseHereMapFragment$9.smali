.class Lcom/navdy/client/app/ui/base/BaseHereMapFragment$9;
.super Ljava/lang/Object;
.source "BaseHereMapFragment.java"

# interfaces
.implements Lcom/navdy/client/app/ui/base/BaseHereMapFragment$OnHereMapFragmentInitialized;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->centerOnUserLocation(Lcom/here/android/mpa/mapping/Map$Animation;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

.field final synthetic val$animation:Lcom/here/android/mpa/mapping/Map$Animation;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/base/BaseHereMapFragment;Lcom/here/android/mpa/mapping/Map$Animation;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    .prologue
    .line 329
    iput-object p1, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$9;->this$0:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    iput-object p2, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$9;->val$animation:Lcom/here/android/mpa/mapping/Map$Animation;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Lcom/navdy/client/app/ui/base/BaseHereMapFragment$Error;)V
    .locals 0
    .param p1, "error"    # Lcom/navdy/client/app/ui/base/BaseHereMapFragment$Error;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 350
    return-void
.end method

.method public onInit(Lcom/here/android/mpa/mapping/Map;)V
    .locals 8
    .param p1, "hereMap"    # Lcom/here/android/mpa/mapping/Map;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 332
    iget-object v3, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$9;->this$0:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    invoke-static {v3}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->access$1400(Lcom/navdy/client/app/ui/base/BaseHereMapFragment;)Lcom/navdy/client/app/framework/location/NavdyLocationManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->getPhoneCoordinates()Lcom/navdy/service/library/events/location/Coordinate;

    move-result-object v2

    .line 333
    .local v2, "phoneLocation":Lcom/navdy/service/library/events/location/Coordinate;
    iget-object v3, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$9;->this$0:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    invoke-static {v3}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->access$1400(Lcom/navdy/client/app/ui/base/BaseHereMapFragment;)Lcom/navdy/client/app/framework/location/NavdyLocationManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->getCarCoordinates()Lcom/navdy/service/library/events/location/Coordinate;

    move-result-object v0

    .line 334
    .local v0, "carLocation":Lcom/navdy/service/library/events/location/Coordinate;
    const/4 v1, 0x0

    .line 336
    .local v1, "newCenter":Lcom/here/android/mpa/common/GeoCoordinate;
    if-eqz v2, :cond_1

    .line 337
    new-instance v1, Lcom/here/android/mpa/common/GeoCoordinate;

    .end local v1    # "newCenter":Lcom/here/android/mpa/common/GeoCoordinate;
    iget-object v3, v2, Lcom/navdy/service/library/events/location/Coordinate;->latitude:Ljava/lang/Double;

    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    iget-object v3, v2, Lcom/navdy/service/library/events/location/Coordinate;->longitude:Ljava/lang/Double;

    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    invoke-direct {v1, v4, v5, v6, v7}, Lcom/here/android/mpa/common/GeoCoordinate;-><init>(DD)V

    .line 342
    .restart local v1    # "newCenter":Lcom/here/android/mpa/common/GeoCoordinate;
    :cond_0
    :goto_0
    if-eqz v1, :cond_2

    .line 343
    iget-object v3, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$9;->this$0:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    iget-object v4, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$9;->val$animation:Lcom/here/android/mpa/mapping/Map$Animation;

    invoke-static {v3, v1, v4}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->access$1500(Lcom/navdy/client/app/ui/base/BaseHereMapFragment;Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/mapping/Map$Animation;)V

    .line 347
    :goto_1
    return-void

    .line 338
    :cond_1
    if-eqz v0, :cond_0

    .line 339
    new-instance v1, Lcom/here/android/mpa/common/GeoCoordinate;

    .end local v1    # "newCenter":Lcom/here/android/mpa/common/GeoCoordinate;
    iget-object v3, v0, Lcom/navdy/service/library/events/location/Coordinate;->latitude:Ljava/lang/Double;

    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    iget-object v3, v0, Lcom/navdy/service/library/events/location/Coordinate;->longitude:Ljava/lang/Double;

    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    invoke-direct {v1, v4, v5, v6, v7}, Lcom/here/android/mpa/common/GeoCoordinate;-><init>(DD)V

    .restart local v1    # "newCenter":Lcom/here/android/mpa/common/GeoCoordinate;
    goto :goto_0

    .line 345
    :cond_2
    invoke-static {}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    const-string v4, "could not center on user location, no location"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    goto :goto_1
.end method
