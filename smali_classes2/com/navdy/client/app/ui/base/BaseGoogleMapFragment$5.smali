.class Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$5;
.super Ljava/lang/Object;
.source "BaseGoogleMapFragment.java"

# interfaces
.implements Lcom/google/android/gms/maps/OnMapReadyCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->onLimitCellDataEvent(Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity$LimitCellDataEvent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

.field final synthetic val$event:Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity$LimitCellDataEvent;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity$LimitCellDataEvent;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    .prologue
    .line 269
    iput-object p1, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$5;->this$0:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    iput-object p2, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$5;->val$event:Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity$LimitCellDataEvent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMapReady(Lcom/google/android/gms/maps/GoogleMap;)V
    .locals 2
    .param p1, "googleMap"    # Lcom/google/android/gms/maps/GoogleMap;

    .prologue
    .line 272
    iget-object v1, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$5;->val$event:Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity$LimitCellDataEvent;

    iget-boolean v1, v1, Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity$LimitCellDataEvent;->hasLimitedCellData:Z

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 273
    .local v0, "shouldEnableTraffic":Z
    :goto_0
    invoke-virtual {p1, v0}, Lcom/google/android/gms/maps/GoogleMap;->setTrafficEnabled(Z)V

    .line 274
    return-void

    .line 272
    .end local v0    # "shouldEnableTraffic":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
