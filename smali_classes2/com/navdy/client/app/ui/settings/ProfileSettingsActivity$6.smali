.class Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity$6;
.super Landroid/os/AsyncTask;
.source "ProfileSettingsActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->onActivityResult(IILandroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;

.field final synthetic val$responseIntent:Landroid/content/Intent;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;Landroid/content/Intent;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;

    .prologue
    .line 219
    iput-object p1, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity$6;->this$0:Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;

    iput-object p2, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity$6;->val$responseIntent:Landroid/content/Intent;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 219
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity$6;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 9
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 228
    iget-object v7, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity$6;->val$responseIntent:Landroid/content/Intent;

    invoke-static {v7}, Lcom/soundcloud/android/crop/Crop;->getOutput(Landroid/content/Intent;)Landroid/net/Uri;

    move-result-object v1

    .line 230
    .local v1, "output":Landroid/net/Uri;
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 231
    .local v3, "resources":Landroid/content/res/Resources;
    const v7, 0x7f0b0092

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    .line 232
    .local v6, "width":I
    const v7, 0x7f0b008f

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 234
    .local v0, "height":I
    if-eqz v1, :cond_1

    .line 235
    new-instance v7, Ljava/io/File;

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v7, v6, v0}, Lcom/navdy/client/app/framework/util/ImageUtils;->getScaledBitmap(Ljava/io/File;II)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 241
    .local v2, "photoBitmap":Landroid/graphics/Bitmap;
    :goto_0
    if-eqz v2, :cond_0

    .line 242
    iget-object v7, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity$6;->this$0:Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;

    iput-object v2, v7, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->userPhoto:Landroid/graphics/Bitmap;

    .line 243
    iget-object v7, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity$6;->this$0:Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;

    const/4 v8, 0x1

    invoke-static {v7, v8}, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->access$302(Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;Z)Z

    .line 244
    invoke-static {v2}, Lcom/navdy/client/app/tracking/Tracker;->saveUserPhotoToInternalStorage(Landroid/graphics/Bitmap;)V

    .line 245
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->incrementDriverProfileSerial()V

    .line 246
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->sendDriverProfileToTheHudBasedOnSharedPrefValue()Z

    .line 249
    new-instance v5, Ljava/io/File;

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 250
    .local v5, "tempFile":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    move-result v4

    .line 251
    .local v4, "success":Z
    if-nez v4, :cond_0

    .line 252
    iget-object v7, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity$6;->this$0:Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;

    invoke-static {v7}, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->access$400(Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;)Lcom/navdy/service/library/log/Logger;

    move-result-object v7

    const-string v8, "Unable to delete temporary photo file."

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 255
    .end local v4    # "success":Z
    .end local v5    # "tempFile":Ljava/io/File;
    :cond_0
    const/4 v7, 0x0

    return-object v7

    .line 238
    .end local v2    # "photoBitmap":Landroid/graphics/Bitmap;
    :cond_1
    const/4 v2, 0x0

    .restart local v2    # "photoBitmap":Landroid/graphics/Bitmap;
    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 219
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity$6;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 3
    .param p1, "aVoid"    # Ljava/lang/Void;

    .prologue
    .line 260
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 261
    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity$6;->this$0:Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;

    invoke-virtual {v1}, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->hideProgressDialog()V

    .line 263
    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity$6;->this$0:Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;

    iget-object v1, v1, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->nameInput:Landroid/widget/EditText;

    if-eqz v1, :cond_0

    .line 264
    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity$6;->this$0:Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;

    iget-object v1, v1, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->nameInput:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 268
    .local v0, "fullName":Ljava/lang/String;
    :goto_0
    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity$6;->this$0:Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;

    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity$6;->this$0:Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;

    iget-object v2, v2, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->userPhoto:Landroid/graphics/Bitmap;

    invoke-static {v1, v2, v0}, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->access$500(Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;Landroid/graphics/Bitmap;Ljava/lang/String;)V

    .line 269
    return-void

    .line 266
    .end local v0    # "fullName":Ljava/lang/String;
    :cond_0
    const-string v0, ""

    .restart local v0    # "fullName":Ljava/lang/String;
    goto :goto_0
.end method

.method protected onPreExecute()V
    .locals 1

    .prologue
    .line 222
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 223
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity$6;->this$0:Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;

    invoke-virtual {v0}, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->showProgressDialog()V

    .line 224
    return-void
.end method
