.class public Lcom/navdy/client/app/ui/settings/CalendarTitleViewHolder;
.super Landroid/support/v7/widget/RecyclerView$ViewHolder;
.source "CalendarTitleViewHolder.java"


# instance fields
.field private name:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 16
    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 13
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/CalendarTitleViewHolder;->name:Landroid/widget/TextView;

    .line 17
    instance-of v0, p1, Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 18
    check-cast p1, Landroid/widget/TextView;

    .end local p1    # "v":Landroid/view/View;
    iput-object p1, p0, Lcom/navdy/client/app/ui/settings/CalendarTitleViewHolder;->name:Landroid/widget/TextView;

    .line 22
    :goto_0
    return-void

    .line 20
    .restart local p1    # "v":Landroid/view/View;
    :cond_0
    const v0, 0x7f100105

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/CalendarTitleViewHolder;->name:Landroid/widget/TextView;

    goto :goto_0
.end method


# virtual methods
.method public setName(Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/CalendarTitleViewHolder;->name:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 26
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/CalendarTitleViewHolder;->name:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 28
    :cond_0
    return-void
.end method
