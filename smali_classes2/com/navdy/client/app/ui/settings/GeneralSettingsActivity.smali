.class public Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity;
.super Lcom/navdy/client/app/ui/base/BaseEditActivity;
.source "GeneralSettingsActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity$LimitCellDataEvent;
    }
.end annotation


# instance fields
.field private final bus:Lcom/navdy/client/app/framework/util/BusProvider;

.field private gestureSwitch:Landroid/widget/Switch;

.field private googleNow:Landroid/widget/RadioButton;

.field private final i18nManager:Lcom/navdy/client/app/framework/i18n/I18nManager;

.field private limitCellDataSwitch:Landroid/widget/Switch;

.field private final onCheckedChangeListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

.field private placeSearch:Landroid/widget/RadioButton;

.field private final sharedPrefs:Landroid/content/SharedPreferences;

.field private smartSuggestionsSwitch:Landroid/widget/Switch;

.field private final unitSystemClickListener:Landroid/view/View$OnClickListener;

.field private unitSystemImperial:Landroid/widget/RadioButton;

.field private unitSystemMetric:Landroid/widget/RadioButton;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/navdy/client/app/ui/base/BaseEditActivity;-><init>()V

    .line 43
    new-instance v0, Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity$1;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity$1;-><init>(Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity;)V

    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity;->unitSystemClickListener:Landroid/view/View$OnClickListener;

    .line 50
    new-instance v0, Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity$2;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity$2;-><init>(Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity;)V

    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity;->onCheckedChangeListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    .line 58
    invoke-static {}, Lcom/navdy/client/app/framework/i18n/I18nManager;->getInstance()Lcom/navdy/client/app/framework/i18n/I18nManager;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity;->i18nManager:Lcom/navdy/client/app/framework/i18n/I18nManager;

    .line 59
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity;->sharedPrefs:Landroid/content/SharedPreferences;

    .line 60
    invoke-static {}, Lcom/navdy/client/app/framework/util/BusProvider;->getInstance()Lcom/navdy/client/app/framework/util/BusProvider;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity;->bus:Lcom/navdy/client/app/framework/util/BusProvider;

    .line 61
    return-void
.end method

.method static synthetic access$002(Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 29
    iput-boolean p1, p0, Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity;->somethingChanged:Z

    return p1
.end method

.method static synthetic access$102(Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 29
    iput-boolean p1, p0, Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity;->somethingChanged:Z

    return p1
.end method

.method static synthetic access$202(Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 29
    iput-boolean p1, p0, Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity;->somethingChanged:Z

    return p1
.end method

.method static synthetic access$302(Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 29
    iput-boolean p1, p0, Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity;->somethingChanged:Z

    return p1
.end method

.method private hideIfFound(I)V
    .locals 2
    .param p1, "resId"    # I
        .annotation build Landroid/support/annotation/IdRes;
        .end annotation
    .end param

    .prologue
    .line 191
    invoke-virtual {p0, p1}, Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 192
    .local v0, "v":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 193
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 195
    :cond_0
    return-void
.end method

.method private initDialLongPress()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 151
    const v3, 0x7f1003b6

    invoke-virtual {p0, v3}, Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RadioButton;

    iput-object v3, p0, Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity;->googleNow:Landroid/widget/RadioButton;

    .line 152
    const v3, 0x7f1003b7

    invoke-virtual {p0, v3}, Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RadioButton;

    iput-object v3, p0, Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity;->placeSearch:Landroid/widget/RadioButton;

    .line 154
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v2

    .line 156
    .local v2, "settingsPrefs":Landroid/content/SharedPreferences;
    const-string v3, "hud_dial_long_press_capable"

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 159
    .local v0, "capable":Z
    if-nez v0, :cond_0

    .line 160
    const v3, 0x7f1003b4

    invoke-direct {p0, v3}, Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity;->hideIfFound(I)V

    .line 161
    const v3, 0x7f1003b5

    invoke-direct {p0, v3}, Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity;->hideIfFound(I)V

    .line 162
    const v3, 0x7f1003b8

    invoke-direct {p0, v3}, Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity;->hideIfFound(I)V

    .line 188
    :goto_0
    return-void

    .line 166
    :cond_0
    const-string v3, "dial_long_press_action"

    sget v4, Lcom/navdy/client/app/ui/settings/SettingsConstants;->DIAL_LONG_PRESS_ACTION_DEFAULT:I

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 168
    .local v1, "dialLongPressInt":I
    sget-object v3, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DialLongPressAction;->DIAL_LONG_PRESS_PLACE_SEARCH:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DialLongPressAction;

    invoke-virtual {v3}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DialLongPressAction;->getValue()I

    move-result v3

    if-eq v1, v3, :cond_1

    .line 169
    iget-object v3, p0, Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity;->googleNow:Landroid/widget/RadioButton;

    invoke-virtual {v3, v5}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 174
    :goto_1
    iget-object v3, p0, Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity;->googleNow:Landroid/widget/RadioButton;

    new-instance v4, Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity$4;

    invoke-direct {v4, p0}, Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity$4;-><init>(Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity;)V

    invoke-virtual {v3, v4}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 181
    iget-object v3, p0, Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity;->placeSearch:Landroid/widget/RadioButton;

    new-instance v4, Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity$5;

    invoke-direct {v4, p0}, Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity$5;-><init>(Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity;)V

    invoke-virtual {v3, v4}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 171
    :cond_1
    iget-object v3, p0, Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity;->placeSearch:Landroid/widget/RadioButton;

    invoke-virtual {v3, v5}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_1
.end method

.method private initGesturesSettings()V
    .locals 4

    .prologue
    .line 198
    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity;->sharedPrefs:Landroid/content/SharedPreferences;

    const-string v2, "hud_gesture"

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 200
    .local v0, "gesturesAreEnabled":Z
    const v1, 0x7f1003a6

    invoke-virtual {p0, v1}, Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Switch;

    iput-object v1, p0, Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity;->gestureSwitch:Landroid/widget/Switch;

    .line 202
    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity;->gestureSwitch:Landroid/widget/Switch;

    if-eqz v1, :cond_0

    .line 203
    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity;->gestureSwitch:Landroid/widget/Switch;

    invoke-virtual {v1, v0}, Landroid/widget/Switch;->setChecked(Z)V

    .line 204
    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity;->gestureSwitch:Landroid/widget/Switch;

    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity;->onCheckedChangeListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v1, v2}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 206
    :cond_0
    return-void
.end method

.method private initLimitCellularDataSettings()V
    .locals 3

    .prologue
    .line 209
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->isLimitingCellularData()Z

    move-result v0

    .line 210
    .local v0, "limitCellularDataEnabled":Z
    const v1, 0x7f1003ad

    invoke-virtual {p0, v1}, Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Switch;

    iput-object v1, p0, Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity;->limitCellDataSwitch:Landroid/widget/Switch;

    .line 212
    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity;->limitCellDataSwitch:Landroid/widget/Switch;

    if-eqz v1, :cond_0

    .line 213
    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity;->limitCellDataSwitch:Landroid/widget/Switch;

    invoke-virtual {v1, v0}, Landroid/widget/Switch;->setChecked(Z)V

    .line 214
    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity;->limitCellDataSwitch:Landroid/widget/Switch;

    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity;->onCheckedChangeListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v1, v2}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 216
    :cond_0
    return-void
.end method

.method private initSmartSuggestionsSettings()V
    .locals 4

    .prologue
    .line 219
    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity;->sharedPrefs:Landroid/content/SharedPreferences;

    const-string v2, "recommendations_enabled"

    const/4 v3, 0x1

    .line 220
    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 222
    .local v0, "smartSuggestionsEnabled":Z
    const v1, 0x7f1003aa

    invoke-virtual {p0, v1}, Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Switch;

    iput-object v1, p0, Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity;->smartSuggestionsSwitch:Landroid/widget/Switch;

    .line 224
    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity;->smartSuggestionsSwitch:Landroid/widget/Switch;

    if-eqz v1, :cond_0

    .line 225
    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity;->smartSuggestionsSwitch:Landroid/widget/Switch;

    invoke-virtual {v1, v0}, Landroid/widget/Switch;->setChecked(Z)V

    .line 226
    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity;->smartSuggestionsSwitch:Landroid/widget/Switch;

    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity;->onCheckedChangeListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v1, v2}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 228
    :cond_0
    return-void
.end method

.method private initUnitSystemSettings()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 133
    const v1, 0x7f1003b2

    invoke-virtual {p0, v1}, Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    iput-object v1, p0, Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity;->unitSystemMetric:Landroid/widget/RadioButton;

    .line 134
    const v1, 0x7f1003b3

    invoke-virtual {p0, v1}, Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    iput-object v1, p0, Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity;->unitSystemImperial:Landroid/widget/RadioButton;

    .line 136
    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity;->i18nManager:Lcom/navdy/client/app/framework/i18n/I18nManager;

    invoke-virtual {v1}, Lcom/navdy/client/app/framework/i18n/I18nManager;->getUnitSystem()Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;

    move-result-object v0

    .line 138
    .local v0, "unitSystem":Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;
    sget-object v1, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;->UNIT_SYSTEM_METRIC:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;

    if-ne v0, v1, :cond_0

    .line 139
    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity;->unitSystemMetric:Landroid/widget/RadioButton;

    invoke-virtual {v1, v2}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 146
    :goto_0
    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity;->unitSystemMetric:Landroid/widget/RadioButton;

    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity;->unitSystemClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 147
    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity;->unitSystemImperial:Landroid/widget/RadioButton;

    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity;->unitSystemClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 148
    return-void

    .line 140
    :cond_0
    sget-object v1, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;->UNIT_SYSTEM_IMPERIAL:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;

    if-ne v0, v1, :cond_1

    .line 141
    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity;->unitSystemImperial:Landroid/widget/RadioButton;

    invoke-virtual {v1, v2}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_0

    .line 143
    :cond_1
    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "invalid unit system"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public onAllowGesturesClick(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 94
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity;->gestureSwitch:Landroid/widget/Switch;

    invoke-virtual {v0}, Landroid/widget/Switch;->toggle()V

    .line 95
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 67
    invoke-super {p0, p1}, Lcom/navdy/client/app/ui/base/BaseEditActivity;->onCreate(Landroid/os/Bundle;)V

    .line 68
    const v0, 0x7f0300f3

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity;->setContentView(I)V

    .line 70
    new-instance v0, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;-><init>(Lcom/navdy/client/app/ui/base/BaseToolbarActivity;)V

    const v1, 0x7f0802ba

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;->title(I)Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;->build()Landroid/support/v7/widget/Toolbar;

    .line 71
    return-void
.end method

.method public onLearnMoreClick(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 231
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/navdy/client/app/ui/settings/GestureDialogActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 232
    .local v0, "i":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity;->startActivity(Landroid/content/Intent;)V

    .line 233
    return-void
.end method

.method public onLimitCellularDataClick(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 90
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity;->limitCellDataSwitch:Landroid/widget/Switch;

    invoke-virtual {v0}, Landroid/widget/Switch;->toggle()V

    .line 91
    return-void
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 75
    invoke-super {p0}, Lcom/navdy/client/app/ui/base/BaseEditActivity;->onResume()V

    .line 76
    invoke-direct {p0}, Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity;->initUnitSystemSettings()V

    .line 77
    invoke-direct {p0}, Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity;->initDialLongPress()V

    .line 78
    invoke-direct {p0}, Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity;->initGesturesSettings()V

    .line 79
    invoke-direct {p0}, Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity;->initLimitCellularDataSettings()V

    .line 80
    invoke-direct {p0}, Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity;->initSmartSuggestionsSettings()V

    .line 81
    return-void
.end method

.method public onSmartSuggestionsClick(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 86
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity;->smartSuggestionsSwitch:Landroid/widget/Switch;

    invoke-virtual {v0}, Landroid/widget/Switch;->toggle()V

    .line 87
    return-void
.end method

.method protected saveChanges()V
    .locals 3

    .prologue
    .line 98
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity;->unitSystemMetric:Landroid/widget/RadioButton;

    invoke-virtual {v0}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 99
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity;->i18nManager:Lcom/navdy/client/app/framework/i18n/I18nManager;

    sget-object v1, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;->UNIT_SYSTEM_METRIC:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/framework/i18n/I18nManager;->setUnitSystem(Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;)V

    .line 106
    :goto_0
    const-string v0, "hud_gesture"

    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity;->gestureSwitch:Landroid/widget/Switch;

    .line 107
    invoke-virtual {v1}, Landroid/widget/Switch;->isChecked()Z

    move-result v1

    .line 106
    invoke-static {v0, v1}, Lcom/navdy/client/app/ui/glances/GlanceUtils;->saveGlancesConfigurationChanges(Ljava/lang/String;Z)V

    .line 108
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity;->gestureSwitch:Landroid/widget/Switch;

    invoke-virtual {v0}, Landroid/widget/Switch;->isChecked()Z

    move-result v0

    invoke-static {v0}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->sendHudSettings(Z)Z

    .line 110
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->isLimitingCellularData()Z

    move-result v0

    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity;->limitCellDataSwitch:Landroid/widget/Switch;

    invoke-virtual {v1}, Landroid/widget/Switch;->isChecked()Z

    move-result v1

    if-eq v0, v1, :cond_0

    .line 111
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity;->limitCellDataSwitch:Landroid/widget/Switch;

    invoke-virtual {v0}, Landroid/widget/Switch;->isChecked()Z

    move-result v0

    invoke-static {v0}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->setLimitCellularData(Z)V

    .line 112
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity;->bus:Lcom/navdy/client/app/framework/util/BusProvider;

    new-instance v1, Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity$LimitCellDataEvent;

    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity;->limitCellDataSwitch:Landroid/widget/Switch;

    invoke-virtual {v2}, Landroid/widget/Switch;->isChecked()Z

    move-result v2

    invoke-direct {v1, v2}, Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity$LimitCellDataEvent;-><init>(Z)V

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/framework/util/BusProvider;->post(Ljava/lang/Object;)V

    .line 115
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity;->sharedPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "recommendations_enabled"

    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity;->smartSuggestionsSwitch:Landroid/widget/Switch;

    .line 117
    invoke-virtual {v2}, Landroid/widget/Switch;->isChecked()Z

    move-result v2

    .line 116
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "dial_long_press_action"

    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity;->placeSearch:Landroid/widget/RadioButton;

    .line 119
    invoke-virtual {v0}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DialLongPressAction;->DIAL_LONG_PRESS_PLACE_SEARCH:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DialLongPressAction;

    .line 120
    invoke-virtual {v0}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DialLongPressAction;->getValue()I

    move-result v0

    .line 118
    :goto_1
    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 122
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 124
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity$3;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity$3;-><init>(Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 130
    return-void

    .line 100
    :cond_1
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity;->unitSystemImperial:Landroid/widget/RadioButton;

    invoke-virtual {v0}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 101
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity;->i18nManager:Lcom/navdy/client/app/framework/i18n/I18nManager;

    sget-object v1, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;->UNIT_SYSTEM_IMPERIAL:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/framework/i18n/I18nManager;->setUnitSystem(Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;)V

    goto :goto_0

    .line 103
    :cond_2
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "invalid state for radio group"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 120
    :cond_3
    sget-object v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DialLongPressAction;->DIAL_LONG_PRESS_VOICE_ASSISTANT:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DialLongPressAction;

    .line 121
    invoke-virtual {v0}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DialLongPressAction;->getValue()I

    move-result v0

    goto :goto_1
.end method
