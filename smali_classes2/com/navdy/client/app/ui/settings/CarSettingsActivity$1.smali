.class Lcom/navdy/client/app/ui/settings/CarSettingsActivity$1;
.super Ljava/lang/Object;
.source "CarSettingsActivity.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/ui/settings/CarSettingsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/settings/CarSettingsActivity;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/settings/CarSettingsActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/settings/CarSettingsActivity;

    .prologue
    .line 42
    iput-object p1, p0, Lcom/navdy/client/app/ui/settings/CarSettingsActivity$1;->this$0:Lcom/navdy/client/app/ui/settings/CarSettingsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 3
    .param p1, "compoundButton"    # Landroid/widget/CompoundButton;
    .param p2, "isEnabled"    # Z

    .prologue
    const/4 v2, 0x1

    .line 45
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/CarSettingsActivity$1;->this$0:Lcom/navdy/client/app/ui/settings/CarSettingsActivity;

    invoke-static {v0}, Lcom/navdy/client/app/ui/settings/CarSettingsActivity;->access$000(Lcom/navdy/client/app/ui/settings/CarSettingsActivity;)Landroid/widget/Switch;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setEnabled(Z)V

    .line 46
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/client/app/ui/settings/CarSettingsActivity$1$1;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/ui/settings/CarSettingsActivity$1$1;-><init>(Lcom/navdy/client/app/ui/settings/CarSettingsActivity$1;)V

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 98
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/CarSettingsActivity$1;->this$0:Lcom/navdy/client/app/ui/settings/CarSettingsActivity;

    invoke-static {v0}, Lcom/navdy/client/app/ui/settings/CarSettingsActivity;->access$000(Lcom/navdy/client/app/ui/settings/CarSettingsActivity;)Landroid/widget/Switch;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/Switch;->setEnabled(Z)V

    .line 99
    return-void
.end method
