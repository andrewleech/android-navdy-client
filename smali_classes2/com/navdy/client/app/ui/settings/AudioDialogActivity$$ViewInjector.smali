.class public Lcom/navdy/client/app/ui/settings/AudioDialogActivity$$ViewInjector;
.super Ljava/lang/Object;
.source "AudioDialogActivity$$ViewInjector.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static inject(Lbutterknife/ButterKnife$Finder;Lcom/navdy/client/app/ui/settings/AudioDialogActivity;Ljava/lang/Object;)V
    .locals 2
    .param p0, "finder"    # Lbutterknife/ButterKnife$Finder;
    .param p1, "target"    # Lcom/navdy/client/app/ui/settings/AudioDialogActivity;
    .param p2, "source"    # Ljava/lang/Object;

    .prologue
    .line 10
    const v1, 0x7f10007f

    invoke-virtual {p0, p2, v1}, Lbutterknife/ButterKnife$Finder;->findOptionalView(Ljava/lang/Object;I)Landroid/view/View;

    move-result-object v0

    .line 11
    .local v0, "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/client/app/ui/settings/AudioDialogActivity;->statusTitle:Landroid/widget/TextView;

    .line 12
    const v1, 0x7f100133

    invoke-virtual {p0, p2, v1}, Lbutterknife/ButterKnife$Finder;->findOptionalView(Ljava/lang/Object;I)Landroid/view/View;

    move-result-object v0

    .line 13
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/ImageView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/client/app/ui/settings/AudioDialogActivity;->statusImage:Landroid/widget/ImageView;

    .line 14
    const v1, 0x7f100135

    invoke-virtual {p0, p2, v1}, Lbutterknife/ButterKnife$Finder;->findOptionalView(Ljava/lang/Object;I)Landroid/view/View;

    move-result-object v0

    .line 15
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/ProgressBar;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/client/app/ui/settings/AudioDialogActivity;->volumeProgress:Landroid/widget/ProgressBar;

    .line 16
    const v1, 0x7f100134

    invoke-virtual {p0, p2, v1}, Lbutterknife/ButterKnife$Finder;->findOptionalView(Ljava/lang/Object;I)Landroid/view/View;

    move-result-object v0

    .line 17
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/client/app/ui/settings/AudioDialogActivity;->outputDeviceName:Landroid/widget/TextView;

    .line 18
    const v1, 0x7f100384

    invoke-virtual {p0, p2, v1}, Lbutterknife/ButterKnife$Finder;->findOptionalView(Ljava/lang/Object;I)Landroid/view/View;

    move-result-object v0

    .line 19
    .restart local v0    # "view":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 20
    new-instance v1, Lcom/navdy/client/app/ui/settings/AudioDialogActivity$$ViewInjector$1;

    invoke-direct {v1, p1}, Lcom/navdy/client/app/ui/settings/AudioDialogActivity$$ViewInjector$1;-><init>(Lcom/navdy/client/app/ui/settings/AudioDialogActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 29
    :cond_0
    const v1, 0x7f100385

    invoke-virtual {p0, p2, v1}, Lbutterknife/ButterKnife$Finder;->findOptionalView(Ljava/lang/Object;I)Landroid/view/View;

    move-result-object v0

    .line 30
    if-eqz v0, :cond_1

    .line 31
    new-instance v1, Lcom/navdy/client/app/ui/settings/AudioDialogActivity$$ViewInjector$2;

    invoke-direct {v1, p1}, Lcom/navdy/client/app/ui/settings/AudioDialogActivity$$ViewInjector$2;-><init>(Lcom/navdy/client/app/ui/settings/AudioDialogActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 40
    :cond_1
    return-void
.end method

.method public static reset(Lcom/navdy/client/app/ui/settings/AudioDialogActivity;)V
    .locals 1
    .param p0, "target"    # Lcom/navdy/client/app/ui/settings/AudioDialogActivity;

    .prologue
    const/4 v0, 0x0

    .line 43
    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/AudioDialogActivity;->statusTitle:Landroid/widget/TextView;

    .line 44
    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/AudioDialogActivity;->statusImage:Landroid/widget/ImageView;

    .line 45
    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/AudioDialogActivity;->volumeProgress:Landroid/widget/ProgressBar;

    .line 46
    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/AudioDialogActivity;->outputDeviceName:Landroid/widget/TextView;

    .line 47
    return-void
.end method
