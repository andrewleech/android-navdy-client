.class Lcom/navdy/client/app/ui/settings/SettingsUtils$SettingsLogger;
.super Ljava/lang/Object;
.source "SettingsUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/ui/settings/SettingsUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "SettingsLogger"
.end annotation


# instance fields
.field private customerPreferences:Landroid/content/SharedPreferences;

.field private sharedPreferences:Landroid/content/SharedPreferences;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 667
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 668
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/SettingsUtils$SettingsLogger;->sharedPreferences:Landroid/content/SharedPreferences;

    .line 669
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getCustomerPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/SettingsUtils$SettingsLogger;->customerPreferences:Landroid/content/SharedPreferences;

    .line 670
    return-void
.end method


# virtual methods
.method audioOutput()Ljava/lang/CharSequence;
    .locals 4

    .prologue
    .line 709
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "media_router"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/MediaRouter;

    .line 710
    .local v0, "mediaRouter":Landroid/media/MediaRouter;
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/media/MediaRouter;->getSelectedRoute(I)Landroid/media/MediaRouter$RouteInfo;

    move-result-object v1

    .line 711
    .local v1, "routeInfo":Landroid/media/MediaRouter$RouteInfo;
    invoke-virtual {v1}, Landroid/media/MediaRouter$RouteInfo;->getName()Ljava/lang/CharSequence;

    move-result-object v2

    return-object v2
.end method

.method carSetup()Z
    .locals 6

    .prologue
    .line 702
    iget-object v3, p0, Lcom/navdy/client/app/ui/settings/SettingsUtils$SettingsLogger;->customerPreferences:Landroid/content/SharedPreferences;

    const-string v4, "vehicle-year"

    const-string v5, ""

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 703
    .local v2, "carYear":Ljava/lang/String;
    iget-object v3, p0, Lcom/navdy/client/app/ui/settings/SettingsUtils$SettingsLogger;->customerPreferences:Landroid/content/SharedPreferences;

    const-string v4, "vehicle-make"

    const-string v5, ""

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 704
    .local v0, "carMake":Ljava/lang/String;
    iget-object v3, p0, Lcom/navdy/client/app/ui/settings/SettingsUtils$SettingsLogger;->customerPreferences:Landroid/content/SharedPreferences;

    const-string v4, "vehicle-model"

    const-string v5, ""

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 705
    .local v1, "carModel":Ljava/lang/String;
    invoke-static {v2}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {v0}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {v1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    const/4 v3, 0x1

    :goto_0
    return v3

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method displaySetup()Z
    .locals 1

    .prologue
    .line 699
    invoke-static {}, Lcom/navdy/client/app/framework/DeviceConnection;->isConnected()Z

    move-result v0

    return v0
.end method

.method glancesSetup()Z
    .locals 1

    .prologue
    .line 696
    invoke-static {}, Lcom/navdy/client/app/ui/base/BaseActivity;->weHaveNotificationPermission()Z

    move-result v0

    return v0
.end method

.method logBoolean(Landroid/content/SharedPreferences;Ljava/lang/String;Z)V
    .locals 3
    .param p1, "preferences"    # Landroid/content/SharedPreferences;
    .param p2, "key"    # Ljava/lang/String;
    .param p3, "defaultValue"    # Z

    .prologue
    .line 673
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1, p2, p3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 674
    return-void
.end method

.method logConfiguration()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 715
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "USER CONFIGURATION =========="

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 717
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "--- Client Version ---"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 718
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/SettingsUtils$SettingsLogger;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "last_build_version"

    const-string v2, "1.0"

    invoke-virtual {p0, v0, v1, v2}, Lcom/navdy/client/app/ui/settings/SettingsUtils$SettingsLogger;->logString(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;)V

    .line 719
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "App Version: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->getAppVersionString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 721
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "--- Navigation preferences ---"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 722
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/SettingsUtils$SettingsLogger;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "nav_route_calculation"

    invoke-virtual {p0, v0, v1, v5}, Lcom/navdy/client/app/ui/settings/SettingsUtils$SettingsLogger;->logBoolean(Landroid/content/SharedPreferences;Ljava/lang/String;Z)V

    .line 723
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/SettingsUtils$SettingsLogger;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "nav_auto_recalc"

    invoke-virtual {p0, v0, v1, v5}, Lcom/navdy/client/app/ui/settings/SettingsUtils$SettingsLogger;->logBoolean(Landroid/content/SharedPreferences;Ljava/lang/String;Z)V

    .line 724
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/SettingsUtils$SettingsLogger;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "nav_highways"

    invoke-virtual {p0, v0, v1, v4}, Lcom/navdy/client/app/ui/settings/SettingsUtils$SettingsLogger;->logBoolean(Landroid/content/SharedPreferences;Ljava/lang/String;Z)V

    .line 725
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/SettingsUtils$SettingsLogger;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "nav_toll_roads"

    invoke-virtual {p0, v0, v1, v4}, Lcom/navdy/client/app/ui/settings/SettingsUtils$SettingsLogger;->logBoolean(Landroid/content/SharedPreferences;Ljava/lang/String;Z)V

    .line 726
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/SettingsUtils$SettingsLogger;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "nav_ferries"

    invoke-virtual {p0, v0, v1, v4}, Lcom/navdy/client/app/ui/settings/SettingsUtils$SettingsLogger;->logBoolean(Landroid/content/SharedPreferences;Ljava/lang/String;Z)V

    .line 727
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/SettingsUtils$SettingsLogger;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "nav_tunnels"

    invoke-virtual {p0, v0, v1, v4}, Lcom/navdy/client/app/ui/settings/SettingsUtils$SettingsLogger;->logBoolean(Landroid/content/SharedPreferences;Ljava/lang/String;Z)V

    .line 728
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/SettingsUtils$SettingsLogger;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "nav_unpaved_roads"

    invoke-virtual {p0, v0, v1, v4}, Lcom/navdy/client/app/ui/settings/SettingsUtils$SettingsLogger;->logBoolean(Landroid/content/SharedPreferences;Ljava/lang/String;Z)V

    .line 729
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/SettingsUtils$SettingsLogger;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "nav_auto_trains"

    invoke-virtual {p0, v0, v1, v4}, Lcom/navdy/client/app/ui/settings/SettingsUtils$SettingsLogger;->logBoolean(Landroid/content/SharedPreferences;Ljava/lang/String;Z)V

    .line 731
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "--- Driver preferences ---"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 732
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/SettingsUtils$SettingsLogger;->customerPreferences:Landroid/content/SharedPreferences;

    const-string v1, "profile_serial_number"

    sget-object v2, Lcom/navdy/client/app/ui/settings/SettingsConstants$ProfilePreferences;->SERIAL_NUM_DEFAULT:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/navdy/client/app/ui/settings/SettingsUtils$SettingsLogger;->logLong(Landroid/content/SharedPreferences;Ljava/lang/String;J)V

    .line 733
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/SettingsUtils$SettingsLogger;->customerPreferences:Landroid/content/SharedPreferences;

    const-string v1, "fname"

    const-string v2, ""

    invoke-virtual {p0, v0, v1, v2}, Lcom/navdy/client/app/ui/settings/SettingsUtils$SettingsLogger;->logString(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;)V

    .line 735
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "--- Audio preferences ---"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 736
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/SettingsUtils$SettingsLogger;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "audio_all_audio"

    invoke-virtual {p0, v0, v1, v4}, Lcom/navdy/client/app/ui/settings/SettingsUtils$SettingsLogger;->logBoolean(Landroid/content/SharedPreferences;Ljava/lang/String;Z)V

    .line 737
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/SettingsUtils$SettingsLogger;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "audio_hud_volume"

    const/16 v2, 0x64

    invoke-virtual {p0, v0, v1, v2}, Lcom/navdy/client/app/ui/settings/SettingsUtils$SettingsLogger;->logInt(Landroid/content/SharedPreferences;Ljava/lang/String;I)V

    .line 738
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/SettingsUtils$SettingsLogger;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "audio_menu_feedback"

    invoke-virtual {p0, v0, v1, v4}, Lcom/navdy/client/app/ui/settings/SettingsUtils$SettingsLogger;->logBoolean(Landroid/content/SharedPreferences;Ljava/lang/String;Z)V

    .line 739
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/SettingsUtils$SettingsLogger;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "audio_notifications"

    invoke-virtual {p0, v0, v1, v4}, Lcom/navdy/client/app/ui/settings/SettingsUtils$SettingsLogger;->logBoolean(Landroid/content/SharedPreferences;Ljava/lang/String;Z)V

    .line 741
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/SettingsUtils$SettingsLogger;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "audio_phone_volume"

    const/16 v2, 0x4b

    invoke-virtual {p0, v0, v1, v2}, Lcom/navdy/client/app/ui/settings/SettingsUtils$SettingsLogger;->logInt(Landroid/content/SharedPreferences;Ljava/lang/String;I)V

    .line 742
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/SettingsUtils$SettingsLogger;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "audio_tts_volume"

    const v2, 0x3f19999a    # 0.6f

    invoke-virtual {p0, v0, v1, v2}, Lcom/navdy/client/app/ui/settings/SettingsUtils$SettingsLogger;->logFloat(Landroid/content/SharedPreferences;Ljava/lang/String;F)V

    .line 743
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/SettingsUtils$SettingsLogger;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "audio_play_welcome"

    invoke-virtual {p0, v0, v1, v5}, Lcom/navdy/client/app/ui/settings/SettingsUtils$SettingsLogger;->logBoolean(Landroid/content/SharedPreferences;Ljava/lang/String;Z)V

    .line 744
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/SettingsUtils$SettingsLogger;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "audio_turn_by_turn_instructions"

    invoke-virtual {p0, v0, v1, v4}, Lcom/navdy/client/app/ui/settings/SettingsUtils$SettingsLogger;->logBoolean(Landroid/content/SharedPreferences;Ljava/lang/String;Z)V

    .line 745
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/SettingsUtils$SettingsLogger;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "audio_voice"

    const-string v2, "en-US"

    invoke-virtual {p0, v0, v1, v2}, Lcom/navdy/client/app/ui/settings/SettingsUtils$SettingsLogger;->logString(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;)V

    .line 746
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/SettingsUtils$SettingsLogger;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "audio_speed_warnings"

    invoke-virtual {p0, v0, v1, v5}, Lcom/navdy/client/app/ui/settings/SettingsUtils$SettingsLogger;->logBoolean(Landroid/content/SharedPreferences;Ljava/lang/String;Z)V

    .line 747
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/SettingsUtils$SettingsLogger;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "audio_camera_warnings"

    invoke-virtual {p0, v0, v1, v4}, Lcom/navdy/client/app/ui/settings/SettingsUtils$SettingsLogger;->logBoolean(Landroid/content/SharedPreferences;Ljava/lang/String;Z)V

    .line 749
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "--- Current audio routing ---"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 750
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "output: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/navdy/client/app/ui/settings/SettingsUtils$SettingsLogger;->audioOutput()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 751
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "HFP connected: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->isHFPSpeakerConnected()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 752
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "A2DP connected: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->isA2DPSpeakerConnected()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 754
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "--- Software update ---"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 755
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/SettingsUtils$SettingsLogger;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "OTA_VERSION"

    const/4 v2, -0x1

    invoke-virtual {p0, v0, v1, v2}, Lcom/navdy/client/app/ui/settings/SettingsUtils$SettingsLogger;->logInt(Landroid/content/SharedPreferences;Ljava/lang/String;I)V

    .line 756
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/SettingsUtils$SettingsLogger;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "OTA_URL"

    const-string v2, ""

    invoke-virtual {p0, v0, v1, v2}, Lcom/navdy/client/app/ui/settings/SettingsUtils$SettingsLogger;->logString(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;)V

    .line 757
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/SettingsUtils$SettingsLogger;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "OTA_DESCRIPTION"

    const-string v2, ""

    invoke-virtual {p0, v0, v1, v2}, Lcom/navdy/client/app/ui/settings/SettingsUtils$SettingsLogger;->logString(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;)V

    .line 758
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/SettingsUtils$SettingsLogger;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "OTA_SIZE"

    const-wide/16 v2, 0x0

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/navdy/client/app/ui/settings/SettingsUtils$SettingsLogger;->logLong(Landroid/content/SharedPreferences;Ljava/lang/String;J)V

    .line 759
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/SettingsUtils$SettingsLogger;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "OTA_STATUS"

    sget-object v2, Lcom/navdy/client/app/ui/settings/SettingsConstants;->OTA_STATUS_DEFAULT:Ljava/lang/String;

    invoke-virtual {p0, v0, v1, v2}, Lcom/navdy/client/app/ui/settings/SettingsUtils$SettingsLogger;->logString(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;)V

    .line 760
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/SettingsUtils$SettingsLogger;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "OTA_IS_INCREMENTAL"

    invoke-virtual {p0, v0, v1, v5}, Lcom/navdy/client/app/ui/settings/SettingsUtils$SettingsLogger;->logBoolean(Landroid/content/SharedPreferences;Ljava/lang/String;Z)V

    .line 761
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/SettingsUtils$SettingsLogger;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "OTA_FROM_VERSION"

    invoke-virtual {p0, v0, v1, v5}, Lcom/navdy/client/app/ui/settings/SettingsUtils$SettingsLogger;->logInt(Landroid/content/SharedPreferences;Ljava/lang/String;I)V

    .line 762
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/SettingsUtils$SettingsLogger;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "FORCE_FULL_UPDATE"

    invoke-virtual {p0, v0, v1, v5}, Lcom/navdy/client/app/ui/settings/SettingsUtils$SettingsLogger;->logBoolean(Landroid/content/SharedPreferences;Ljava/lang/String;Z)V

    .line 763
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/SettingsUtils$SettingsLogger;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "OTA_META_DATA"

    const-string v2, ""

    invoke-virtual {p0, v0, v1, v2}, Lcom/navdy/client/app/ui/settings/SettingsUtils$SettingsLogger;->logString(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;)V

    .line 765
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "--- Glances ---"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 766
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/SettingsUtils$SettingsLogger;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "glances"

    invoke-virtual {p0, v0, v1, v5}, Lcom/navdy/client/app/ui/settings/SettingsUtils$SettingsLogger;->logBoolean(Landroid/content/SharedPreferences;Ljava/lang/String;Z)V

    .line 767
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/SettingsUtils$SettingsLogger;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "glances_read_aloud"

    invoke-virtual {p0, v0, v1, v4}, Lcom/navdy/client/app/ui/settings/SettingsUtils$SettingsLogger;->logBoolean(Landroid/content/SharedPreferences;Ljava/lang/String;Z)V

    .line 768
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/SettingsUtils$SettingsLogger;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "glances_show_content"

    invoke-virtual {p0, v0, v1, v5}, Lcom/navdy/client/app/ui/settings/SettingsUtils$SettingsLogger;->logBoolean(Landroid/content/SharedPreferences;Ljava/lang/String;Z)V

    .line 770
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "--- Onboarding completion ---"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 771
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "profile setup: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/navdy/client/app/ui/settings/SettingsUtils$SettingsLogger;->profileSetup()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 772
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "display setup: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/navdy/client/app/ui/settings/SettingsUtils$SettingsLogger;->displaySetup()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 773
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "car setup: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/navdy/client/app/ui/settings/SettingsUtils$SettingsLogger;->carSetup()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 774
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "glances setup: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/navdy/client/app/ui/settings/SettingsUtils$SettingsLogger;->glancesSetup()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 776
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "========== END USER CONFIGURATION"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 777
    return-void
.end method

.method logFloat(Landroid/content/SharedPreferences;Ljava/lang/String;F)V
    .locals 3
    .param p1, "preferences"    # Landroid/content/SharedPreferences;
    .param p2, "key"    # Ljava/lang/String;
    .param p3, "defaultValue"    # F

    .prologue
    .line 689
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1, p2, p3}, Landroid/content/SharedPreferences;->getFloat(Ljava/lang/String;F)F

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 690
    return-void
.end method

.method logInt(Landroid/content/SharedPreferences;Ljava/lang/String;I)V
    .locals 3
    .param p1, "preferences"    # Landroid/content/SharedPreferences;
    .param p2, "key"    # Ljava/lang/String;
    .param p3, "defaultValue"    # I

    .prologue
    .line 685
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1, p2, p3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 686
    return-void
.end method

.method logLong(Landroid/content/SharedPreferences;Ljava/lang/String;J)V
    .locals 5
    .param p1, "preferences"    # Landroid/content/SharedPreferences;
    .param p2, "key"    # Ljava/lang/String;
    .param p3, "defaultValue"    # J

    .prologue
    .line 681
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1, p2, p3, p4}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 682
    return-void
.end method

.method logString(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "preferences"    # Landroid/content/SharedPreferences;
    .param p2, "key"    # Ljava/lang/String;
    .param p3, "defaultValue"    # Ljava/lang/String;

    .prologue
    .line 677
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1, p2, p3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 678
    return-void
.end method

.method profileSetup()Z
    .locals 1

    .prologue
    .line 693
    invoke-static {}, Lcom/navdy/client/app/tracking/Tracker;->isUserRegistered()Z

    move-result v0

    return v0
.end method
