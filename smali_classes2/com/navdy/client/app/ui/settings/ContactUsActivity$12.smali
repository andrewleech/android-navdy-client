.class Lcom/navdy/client/app/ui/settings/ContactUsActivity$12;
.super Ljava/lang/Object;
.source "ContactUsActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/settings/ContactUsActivity;->showAttachPhotoDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/settings/ContactUsActivity;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/settings/ContactUsActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/settings/ContactUsActivity;

    .prologue
    .line 502
    iput-object p1, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity$12;->this$0:Lcom/navdy/client/app/ui/settings/ContactUsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1, "dialogInterface"    # Landroid/content/DialogInterface;
    .param p2, "i"    # I

    .prologue
    .line 505
    packed-switch p2, :pswitch_data_0

    .line 516
    :goto_0
    return-void

    .line 507
    :pswitch_0
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity$12;->this$0:Lcom/navdy/client/app/ui/settings/ContactUsActivity;

    invoke-static {v0}, Lcom/navdy/client/app/ui/PhotoUtils;->getPhotoFromGallery(Lcom/navdy/client/app/ui/base/BaseActivity;)V

    goto :goto_0

    .line 510
    :pswitch_1
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity$12;->this$0:Lcom/navdy/client/app/ui/settings/ContactUsActivity;

    invoke-static {}, Lcom/navdy/service/library/util/IOUtils;->getTempFilename()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->access$1002(Lcom/navdy/client/app/ui/settings/ContactUsActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 511
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity$12;->this$0:Lcom/navdy/client/app/ui/settings/ContactUsActivity;

    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity$12;->this$0:Lcom/navdy/client/app/ui/settings/ContactUsActivity;

    invoke-static {v1}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->access$1100(Lcom/navdy/client/app/ui/settings/ContactUsActivity;)Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity$12;->this$0:Lcom/navdy/client/app/ui/settings/ContactUsActivity;

    invoke-static {v2}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->access$1000(Lcom/navdy/client/app/ui/settings/ContactUsActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/navdy/client/app/ui/PhotoUtils;->takePhoto(Lcom/navdy/client/app/ui/base/BaseActivity;Lcom/navdy/service/library/log/Logger;Ljava/lang/String;)V

    goto :goto_0

    .line 505
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
