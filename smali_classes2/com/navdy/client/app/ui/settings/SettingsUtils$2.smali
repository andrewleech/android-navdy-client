.class final Lcom/navdy/client/app/ui/settings/SettingsUtils$2;
.super Ljava/lang/Object;
.source "SettingsUtils.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/settings/SettingsUtils;->setUnitSystem(Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$unitSystem:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;


# direct methods
.method constructor <init>(Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;)V
    .locals 0

    .prologue
    .line 269
    iput-object p1, p0, Lcom/navdy/client/app/ui/settings/SettingsUtils$2;->val$unitSystem:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 272
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getCustomerPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "unit-system"

    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/SettingsUtils$2;->val$unitSystem:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;

    .line 273
    invoke-virtual {v2}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;->name()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 274
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 276
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->sendDriverProfileToTheHudBasedOnSharedPrefValue()Z

    .line 277
    return-void
.end method
