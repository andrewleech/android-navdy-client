.class Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity$1;
.super Ljava/lang/Object;
.source "MessagingSettingsActivity.java"

# interfaces
.implements Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity$ReplyClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity;

    .prologue
    .line 62
    iput-object p1, p0, Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity$1;->this$0:Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onReplyClick(Ljava/lang/String;Landroid/view/View;)V
    .locals 4
    .param p1, "reply"    # Ljava/lang/String;
    .param p2, "v"    # Landroid/view/View;

    .prologue
    .line 65
    new-instance v1, Landroid/support/v7/widget/PopupMenu;

    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity$1;->this$0:Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity;

    invoke-direct {v1, v2, p2}, Landroid/support/v7/widget/PopupMenu;-><init>(Landroid/content/Context;Landroid/view/View;)V

    .line 66
    .local v1, "popup":Landroid/support/v7/widget/PopupMenu;
    new-instance v2, Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity$1$1;

    invoke-direct {v2, p0, v1, p1}, Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity$1$1;-><init>(Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity$1;Landroid/support/v7/widget/PopupMenu;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/PopupMenu;->setOnMenuItemClickListener(Landroid/support/v7/widget/PopupMenu$OnMenuItemClickListener;)V

    .line 88
    const v0, 0x7f11000a

    .line 89
    .local v0, "menuRes":I
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity$1;->this$0:Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity;

    invoke-static {v2}, Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity;->access$000(Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity;)Lcom/navdy/client/app/ui/settings/RepliesAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/client/app/ui/settings/RepliesAdapter;->getItemCount()I

    move-result v2

    const/4 v3, 0x3

    if-gt v2, v3, :cond_0

    .line 90
    const v0, 0x7f110009

    .line 92
    :cond_0
    invoke-virtual {v1}, Landroid/support/v7/widget/PopupMenu;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v2

    .line 93
    invoke-virtual {v1}, Landroid/support/v7/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 94
    invoke-virtual {v1}, Landroid/support/v7/widget/PopupMenu;->show()V

    .line 95
    return-void
.end method
