.class public Lcom/navdy/client/app/ui/settings/MessagingSettingsEditDialogActivity;
.super Lcom/navdy/client/app/ui/base/BaseActivity;
.source "MessagingSettingsEditDialogActivity.java"


# static fields
.field public static final ADD_CODE:I = 0x1091

.field public static final EDIT_CODE:I = 0x1092

.field public static final EXTRA_NEW_MESSAGE:Ljava/lang/String; = "extra_new_message"

.field public static final EXTRA_OLD_MESSAGE:Ljava/lang/String; = "extra_old_message"


# instance fields
.field private done:Landroid/widget/Button;

.field private input:Landroid/widget/EditText;

.field private oldMessage:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/navdy/client/app/ui/base/BaseActivity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/navdy/client/app/ui/settings/MessagingSettingsEditDialogActivity;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/settings/MessagingSettingsEditDialogActivity;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/MessagingSettingsEditDialogActivity;->done:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$100(Lcom/navdy/client/app/ui/settings/MessagingSettingsEditDialogActivity;)Landroid/widget/EditText;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/settings/MessagingSettingsEditDialogActivity;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/MessagingSettingsEditDialogActivity;->input:Landroid/widget/EditText;

    return-object v0
.end method


# virtual methods
.method public onCancelClick(Landroid/view/View;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 111
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/settings/MessagingSettingsEditDialogActivity;->finish()V

    .line 112
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/high16 v3, 0x40000

    .line 40
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/settings/MessagingSettingsEditDialogActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2, v3, v3}, Landroid/view/Window;->setFlags(II)V

    .line 42
    invoke-super {p0, p1}, Lcom/navdy/client/app/ui/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 43
    const v2, 0x7f030052

    invoke-virtual {p0, v2}, Lcom/navdy/client/app/ui/settings/MessagingSettingsEditDialogActivity;->setContentView(I)V

    .line 45
    const v2, 0x7f100144

    invoke-virtual {p0, v2}, Lcom/navdy/client/app/ui/settings/MessagingSettingsEditDialogActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    iput-object v2, p0, Lcom/navdy/client/app/ui/settings/MessagingSettingsEditDialogActivity;->input:Landroid/widget/EditText;

    .line 46
    const v2, 0x7f100145

    invoke-virtual {p0, v2}, Lcom/navdy/client/app/ui/settings/MessagingSettingsEditDialogActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/navdy/client/app/ui/settings/MessagingSettingsEditDialogActivity;->done:Landroid/widget/Button;

    .line 48
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/MessagingSettingsEditDialogActivity;->input:Landroid/widget/EditText;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/MessagingSettingsEditDialogActivity;->done:Landroid/widget/Button;

    if-nez v2, :cond_1

    .line 93
    :cond_0
    :goto_0
    return-void

    .line 51
    :cond_1
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/MessagingSettingsEditDialogActivity;->input:Landroid/widget/EditText;

    new-instance v3, Lcom/navdy/client/app/ui/settings/MessagingSettingsEditDialogActivity$1;

    invoke-direct {v3, p0}, Lcom/navdy/client/app/ui/settings/MessagingSettingsEditDialogActivity$1;-><init>(Lcom/navdy/client/app/ui/settings/MessagingSettingsEditDialogActivity;)V

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 79
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/settings/MessagingSettingsEditDialogActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 80
    .local v0, "intent":Landroid/content/Intent;
    if-eqz v0, :cond_0

    .line 81
    const-string v2, "extra_old_message"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/navdy/client/app/ui/settings/MessagingSettingsEditDialogActivity;->oldMessage:Ljava/lang/String;

    .line 82
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/MessagingSettingsEditDialogActivity;->oldMessage:Ljava/lang/String;

    invoke-static {v2}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 83
    const v2, 0x7f100139

    invoke-virtual {p0, v2}, Lcom/navdy/client/app/ui/settings/MessagingSettingsEditDialogActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 84
    .local v1, "title":Landroid/widget/TextView;
    if-eqz v1, :cond_2

    .line 85
    const v2, 0x7f0800ae

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 87
    :cond_2
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/MessagingSettingsEditDialogActivity;->done:Landroid/widget/Button;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0

    .line 89
    .end local v1    # "title":Landroid/widget/TextView;
    :cond_3
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/MessagingSettingsEditDialogActivity;->input:Landroid/widget/EditText;

    iget-object v3, p0, Lcom/navdy/client/app/ui/settings/MessagingSettingsEditDialogActivity;->oldMessage:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 90
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/MessagingSettingsEditDialogActivity;->input:Landroid/widget/EditText;

    iget-object v3, p0, Lcom/navdy/client/app/ui/settings/MessagingSettingsEditDialogActivity;->input:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->length()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setSelection(I)V

    goto :goto_0
.end method

.method public onDoneClick(Landroid/view/View;)V
    .locals 5
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 115
    iget-object v3, p0, Lcom/navdy/client/app/ui/settings/MessagingSettingsEditDialogActivity;->input:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 117
    .local v2, "message":Ljava/lang/String;
    const/16 v0, 0x1091

    .line 118
    .local v0, "code":I
    iget-object v3, p0, Lcom/navdy/client/app/ui/settings/MessagingSettingsEditDialogActivity;->oldMessage:Ljava/lang/String;

    invoke-static {v3}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 119
    const/16 v0, 0x1092

    .line 121
    :cond_0
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 122
    .local v1, "intent":Landroid/content/Intent;
    const-string v3, "extra_new_message"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 123
    const-string v3, "extra_old_message"

    iget-object v4, p0, Lcom/navdy/client/app/ui/settings/MessagingSettingsEditDialogActivity;->oldMessage:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 124
    invoke-virtual {p0, v0, v1}, Lcom/navdy/client/app/ui/settings/MessagingSettingsEditDialogActivity;->setResult(ILandroid/content/Intent;)V

    .line 125
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/settings/MessagingSettingsEditDialogActivity;->finish()V

    .line 126
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 97
    invoke-super {p0}, Lcom/navdy/client/app/ui/base/BaseActivity;->onResume()V

    .line 98
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/MessagingSettingsEditDialogActivity;->input:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->clearFocus()V

    .line 99
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/MessagingSettingsEditDialogActivity;->input:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 100
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/MessagingSettingsEditDialogActivity;->input:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/MessagingSettingsEditDialogActivity;->input:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    .line 101
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/settings/MessagingSettingsEditDialogActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 102
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 107
    const/4 v0, 0x1

    return v0
.end method
