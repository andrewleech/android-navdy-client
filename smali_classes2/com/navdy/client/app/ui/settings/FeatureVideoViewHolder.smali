.class public Lcom/navdy/client/app/ui/settings/FeatureVideoViewHolder;
.super Landroid/support/v7/widget/RecyclerView$ViewHolder;
.source "FeatureVideoViewHolder.java"


# instance fields
.field protected duration:Landroid/widget/TextView;

.field protected image:Landroid/widget/ImageView;

.field protected row:Landroid/view/View;

.field protected title:Landroid/widget/TextView;

.field protected watchedLayout:Landroid/widget/RelativeLayout;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1
    .param p1, "itemView"    # Landroid/view/View;

    .prologue
    .line 24
    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 25
    iput-object p1, p0, Lcom/navdy/client/app/ui/settings/FeatureVideoViewHolder;->row:Landroid/view/View;

    .line 26
    const v0, 0x7f100159

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/FeatureVideoViewHolder;->duration:Landroid/widget/TextView;

    .line 27
    const v0, 0x7f1000b1

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/FeatureVideoViewHolder;->image:Landroid/widget/ImageView;

    .line 28
    const v0, 0x7f10007f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/FeatureVideoViewHolder;->title:Landroid/widget/TextView;

    .line 29
    const v0, 0x7f100157

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/FeatureVideoViewHolder;->watchedLayout:Landroid/widget/RelativeLayout;

    .line 30
    return-void
.end method
