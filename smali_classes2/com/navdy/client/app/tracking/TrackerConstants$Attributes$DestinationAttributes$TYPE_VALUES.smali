.class public Lcom/navdy/client/app/tracking/TrackerConstants$Attributes$DestinationAttributes$TYPE_VALUES;
.super Ljava/lang/Object;
.source "TrackerConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/tracking/TrackerConstants$Attributes$DestinationAttributes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "TYPE_VALUES"
.end annotation


# static fields
.field public static final CALENDAR:Ljava/lang/String; = "Calendar"

.field public static final DROP_PIN:Ljava/lang/String; = "Drop_Pin"

.field public static final FAVORITE:Ljava/lang/String; = "Favorite"

.field public static final INTENT:Ljava/lang/String; = "Intent"

.field public static final RECENT:Ljava/lang/String; = "Recent"

.field public static final RECOMMENDATION:Ljava/lang/String; = "Recommendation"

.field public static final SEARCH_AUTOCOMPLETE:Ljava/lang/String; = "Search_Autocomplete"

.field public static final SEARCH_CONTACT:Ljava/lang/String; = "Search_Contact"

.field public static final SEARCH_MAP:Ljava/lang/String; = "Search_Map"

.field public static final SEARCH_QUICK:Ljava/lang/String; = "Search_Quick"

.field public static final SEARCH_RESULT:Ljava/lang/String; = "Search_Result"

.field public static final UNKNOWN:Ljava/lang/String; = "Unknown"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getSetDestinationTypeFromSuggestion(Lcom/navdy/client/app/framework/models/Suggestion;)Ljava/lang/String;
    .locals 2
    .param p0, "suggestion"    # Lcom/navdy/client/app/framework/models/Suggestion;

    .prologue
    .line 95
    if-nez p0, :cond_0

    .line 96
    const-string v0, "Unknown"

    .line 107
    :goto_0
    return-object v0

    .line 99
    :cond_0
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/models/Suggestion;->isRecommendation()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 100
    const-string v0, "Recommendation"

    goto :goto_0

    .line 103
    :cond_1
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/models/Suggestion;->getType()Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    move-result-object v0

    sget-object v1, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->CALENDAR:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    if-ne v0, v1, :cond_2

    .line 104
    const-string v0, "Calendar"

    goto :goto_0

    .line 107
    :cond_2
    const-string v0, "Recent"

    goto :goto_0
.end method
