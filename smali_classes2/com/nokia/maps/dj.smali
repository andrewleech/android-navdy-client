.class public interface abstract Lcom/nokia/maps/dj;
.super Ljava/lang/Object;
.source "PanoramaViewProxy.java"


# virtual methods
.method public abstract a(Lcom/here/android/mpa/common/OnScreenCaptureListener;)V
.end method

.method public abstract getPanorama()Lcom/here/android/mpa/streetlevel/StreetLevelModel;
.end method

.method public abstract getStreetLevelGesture()Lcom/here/android/mpa/streetlevel/StreetLevelGesture;
.end method

.method public abstract onPause()V
.end method

.method public abstract onResume()V
.end method

.method public abstract requestRender()V
.end method

.method public abstract setBlankStreetLevelImageVisible(Z)V
.end method

.method public abstract setPanorama(Lcom/here/android/mpa/streetlevel/StreetLevelModel;)V
.end method
