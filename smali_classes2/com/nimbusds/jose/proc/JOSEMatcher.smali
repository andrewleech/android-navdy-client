.class public Lcom/nimbusds/jose/proc/JOSEMatcher;
.super Ljava/lang/Object;
.source "JOSEMatcher.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/nimbusds/jose/proc/JOSEMatcher$Builder;
    }
.end annotation


# instance fields
.field private final algs:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/nimbusds/jose/Algorithm;",
            ">;"
        }
    .end annotation
.end field

.field private final classes:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lcom/nimbusds/jose/JOSEObject;",
            ">;>;"
        }
    .end annotation
.end field

.field private final encs:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/nimbusds/jose/EncryptionMethod;",
            ">;"
        }
    .end annotation
.end field

.field private final jkus:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/net/URI;",
            ">;"
        }
    .end annotation
.end field

.field private final kids:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lcom/nimbusds/jose/JOSEObject;",
            ">;>;",
            "Ljava/util/Set",
            "<",
            "Lcom/nimbusds/jose/Algorithm;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Lcom/nimbusds/jose/EncryptionMethod;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/net/URI;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 367
    .local p1, "classes":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Class<+Lcom/nimbusds/jose/JOSEObject;>;>;"
    .local p2, "algs":Ljava/util/Set;, "Ljava/util/Set<Lcom/nimbusds/jose/Algorithm;>;"
    .local p3, "encs":Ljava/util/Set;, "Ljava/util/Set<Lcom/nimbusds/jose/EncryptionMethod;>;"
    .local p4, "jkus":Ljava/util/Set;, "Ljava/util/Set<Ljava/net/URI;>;"
    .local p5, "kids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 373
    iput-object p1, p0, Lcom/nimbusds/jose/proc/JOSEMatcher;->classes:Ljava/util/Set;

    .line 374
    iput-object p2, p0, Lcom/nimbusds/jose/proc/JOSEMatcher;->algs:Ljava/util/Set;

    .line 375
    iput-object p3, p0, Lcom/nimbusds/jose/proc/JOSEMatcher;->encs:Ljava/util/Set;

    .line 376
    iput-object p4, p0, Lcom/nimbusds/jose/proc/JOSEMatcher;->jkus:Ljava/util/Set;

    .line 377
    iput-object p5, p0, Lcom/nimbusds/jose/proc/JOSEMatcher;->kids:Ljava/util/Set;

    .line 378
    return-void
.end method


# virtual methods
.method public getAlgorithms()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lcom/nimbusds/jose/Algorithm;",
            ">;"
        }
    .end annotation

    .prologue
    .line 399
    iget-object v0, p0, Lcom/nimbusds/jose/proc/JOSEMatcher;->algs:Ljava/util/Set;

    return-object v0
.end method

.method public getEncryptionMethods()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lcom/nimbusds/jose/EncryptionMethod;",
            ">;"
        }
    .end annotation

    .prologue
    .line 410
    iget-object v0, p0, Lcom/nimbusds/jose/proc/JOSEMatcher;->encs:Ljava/util/Set;

    return-object v0
.end method

.method public getJOSEClasses()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lcom/nimbusds/jose/JOSEObject;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 388
    iget-object v0, p0, Lcom/nimbusds/jose/proc/JOSEMatcher;->classes:Ljava/util/Set;

    return-object v0
.end method

.method public getJWKURLs()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/net/URI;",
            ">;"
        }
    .end annotation

    .prologue
    .line 421
    iget-object v0, p0, Lcom/nimbusds/jose/proc/JOSEMatcher;->jkus:Ljava/util/Set;

    return-object v0
.end method

.method public getKeyIDs()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 432
    iget-object v0, p0, Lcom/nimbusds/jose/proc/JOSEMatcher;->kids:Ljava/util/Set;

    return-object v0
.end method

.method public matches(Lcom/nimbusds/jose/JOSEObject;)Z
    .locals 8
    .param p1, "joseObject"    # Lcom/nimbusds/jose/JOSEObject;

    .prologue
    const/4 v6, 0x0

    .line 445
    iget-object v5, p0, Lcom/nimbusds/jose/proc/JOSEMatcher;->classes:Ljava/util/Set;

    if-eqz v5, :cond_2

    .line 447
    const/4 v4, 0x0

    .line 448
    .local v4, "pass":Z
    iget-object v5, p0, Lcom/nimbusds/jose/proc/JOSEMatcher;->classes:Ljava/util/Set;

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-nez v7, :cond_1

    .line 454
    if-nez v4, :cond_2

    move v5, v6

    .line 507
    .end local v4    # "pass":Z
    .end local p1    # "joseObject":Lcom/nimbusds/jose/JOSEObject;
    :goto_1
    return v5

    .line 448
    .restart local v4    # "pass":Z
    .restart local p1    # "joseObject":Lcom/nimbusds/jose/JOSEObject;
    :cond_1
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    .line 449
    .local v0, "c":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/nimbusds/jose/JOSEObject;>;"
    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 450
    const/4 v4, 0x1

    goto :goto_0

    .line 459
    .end local v0    # "c":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/nimbusds/jose/JOSEObject;>;"
    .end local v4    # "pass":Z
    :cond_2
    iget-object v5, p0, Lcom/nimbusds/jose/proc/JOSEMatcher;->algs:Ljava/util/Set;

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/nimbusds/jose/proc/JOSEMatcher;->algs:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/nimbusds/jose/JOSEObject;->getHeader()Lcom/nimbusds/jose/Header;

    move-result-object v7

    invoke-virtual {v7}, Lcom/nimbusds/jose/Header;->getAlgorithm()Lcom/nimbusds/jose/Algorithm;

    move-result-object v7

    invoke-interface {v5, v7}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    move v5, v6

    .line 460
    goto :goto_1

    .line 462
    :cond_3
    iget-object v5, p0, Lcom/nimbusds/jose/proc/JOSEMatcher;->encs:Ljava/util/Set;

    if-eqz v5, :cond_5

    .line 464
    instance-of v5, p1, Lcom/nimbusds/jose/JWEObject;

    if-nez v5, :cond_4

    move v5, v6

    .line 465
    goto :goto_1

    :cond_4
    move-object v2, p1

    .line 467
    check-cast v2, Lcom/nimbusds/jose/JWEObject;

    .line 469
    .local v2, "jweObject":Lcom/nimbusds/jose/JWEObject;
    iget-object v5, p0, Lcom/nimbusds/jose/proc/JOSEMatcher;->encs:Ljava/util/Set;

    invoke-virtual {v2}, Lcom/nimbusds/jose/JWEObject;->getHeader()Lcom/nimbusds/jose/JWEHeader;

    move-result-object v7

    invoke-virtual {v7}, Lcom/nimbusds/jose/JWEHeader;->getEncryptionMethod()Lcom/nimbusds/jose/EncryptionMethod;

    move-result-object v7

    invoke-interface {v5, v7}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_5

    move v5, v6

    .line 470
    goto :goto_1

    .line 473
    .end local v2    # "jweObject":Lcom/nimbusds/jose/JWEObject;
    :cond_5
    iget-object v5, p0, Lcom/nimbusds/jose/proc/JOSEMatcher;->jkus:Ljava/util/Set;

    if-eqz v5, :cond_8

    .line 477
    instance-of v5, p1, Lcom/nimbusds/jose/JWSObject;

    if-eqz v5, :cond_6

    move-object v5, p1

    .line 478
    check-cast v5, Lcom/nimbusds/jose/JWSObject;

    invoke-virtual {v5}, Lcom/nimbusds/jose/JWSObject;->getHeader()Lcom/nimbusds/jose/JWSHeader;

    move-result-object v5

    invoke-virtual {v5}, Lcom/nimbusds/jose/JWSHeader;->getJWKURL()Ljava/net/URI;

    move-result-object v1

    .line 486
    .local v1, "jku":Ljava/net/URI;
    :goto_2
    iget-object v5, p0, Lcom/nimbusds/jose/proc/JOSEMatcher;->jkus:Ljava/util/Set;

    invoke-interface {v5, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_8

    move v5, v6

    .line 487
    goto :goto_1

    .line 479
    .end local v1    # "jku":Ljava/net/URI;
    :cond_6
    instance-of v5, p1, Lcom/nimbusds/jose/JWEObject;

    if-eqz v5, :cond_7

    move-object v5, p1

    .line 480
    check-cast v5, Lcom/nimbusds/jose/JWEObject;

    invoke-virtual {v5}, Lcom/nimbusds/jose/JWEObject;->getHeader()Lcom/nimbusds/jose/JWEHeader;

    move-result-object v5

    invoke-virtual {v5}, Lcom/nimbusds/jose/JWEHeader;->getJWKURL()Ljava/net/URI;

    move-result-object v1

    .line 481
    .restart local v1    # "jku":Ljava/net/URI;
    goto :goto_2

    .line 483
    .end local v1    # "jku":Ljava/net/URI;
    :cond_7
    const/4 v1, 0x0

    .restart local v1    # "jku":Ljava/net/URI;
    goto :goto_2

    .line 490
    .end local v1    # "jku":Ljava/net/URI;
    :cond_8
    iget-object v5, p0, Lcom/nimbusds/jose/proc/JOSEMatcher;->kids:Ljava/util/Set;

    if-eqz v5, :cond_b

    .line 494
    instance-of v5, p1, Lcom/nimbusds/jose/JWSObject;

    if-eqz v5, :cond_9

    .line 495
    check-cast p1, Lcom/nimbusds/jose/JWSObject;

    .end local p1    # "joseObject":Lcom/nimbusds/jose/JOSEObject;
    invoke-virtual {p1}, Lcom/nimbusds/jose/JWSObject;->getHeader()Lcom/nimbusds/jose/JWSHeader;

    move-result-object v5

    invoke-virtual {v5}, Lcom/nimbusds/jose/JWSHeader;->getKeyID()Ljava/lang/String;

    move-result-object v3

    .line 503
    .local v3, "kid":Ljava/lang/String;
    :goto_3
    iget-object v5, p0, Lcom/nimbusds/jose/proc/JOSEMatcher;->kids:Ljava/util/Set;

    invoke-interface {v5, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_b

    move v5, v6

    .line 504
    goto/16 :goto_1

    .line 496
    .end local v3    # "kid":Ljava/lang/String;
    .restart local p1    # "joseObject":Lcom/nimbusds/jose/JOSEObject;
    :cond_9
    instance-of v5, p1, Lcom/nimbusds/jose/JWEObject;

    if-eqz v5, :cond_a

    .line 497
    check-cast p1, Lcom/nimbusds/jose/JWEObject;

    .end local p1    # "joseObject":Lcom/nimbusds/jose/JOSEObject;
    invoke-virtual {p1}, Lcom/nimbusds/jose/JWEObject;->getHeader()Lcom/nimbusds/jose/JWEHeader;

    move-result-object v5

    invoke-virtual {v5}, Lcom/nimbusds/jose/JWEHeader;->getKeyID()Ljava/lang/String;

    move-result-object v3

    .line 498
    .restart local v3    # "kid":Ljava/lang/String;
    goto :goto_3

    .line 500
    .end local v3    # "kid":Ljava/lang/String;
    .restart local p1    # "joseObject":Lcom/nimbusds/jose/JOSEObject;
    :cond_a
    const/4 v3, 0x0

    .restart local v3    # "kid":Ljava/lang/String;
    goto :goto_3

    .line 507
    .end local v3    # "kid":Ljava/lang/String;
    .end local p1    # "joseObject":Lcom/nimbusds/jose/JOSEObject;
    :cond_b
    const/4 v5, 0x1

    goto/16 :goto_1
.end method
