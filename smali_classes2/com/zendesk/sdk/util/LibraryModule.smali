.class public Lcom/zendesk/sdk/util/LibraryModule;
.super Ljava/lang/Object;
.source "LibraryModule.java"


# instance fields
.field private final gson:Lcom/google/gson/Gson;

.field private final okHttpClient:Lokhttp3/OkHttpClient;


# direct methods
.method constructor <init>(Lcom/google/gson/Gson;Lokhttp3/OkHttpClient;)V
    .locals 0
    .param p1, "gson"    # Lcom/google/gson/Gson;
    .param p2, "okHttpClient"    # Lokhttp3/OkHttpClient;

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/zendesk/sdk/util/LibraryModule;->gson:Lcom/google/gson/Gson;

    .line 19
    iput-object p2, p0, Lcom/zendesk/sdk/util/LibraryModule;->okHttpClient:Lokhttp3/OkHttpClient;

    .line 20
    return-void
.end method


# virtual methods
.method public getGson()Lcom/google/gson/Gson;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/zendesk/sdk/util/LibraryModule;->gson:Lcom/google/gson/Gson;

    return-object v0
.end method

.method public getOkHttpClient()Lokhttp3/OkHttpClient;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/zendesk/sdk/util/LibraryModule;->okHttpClient:Lokhttp3/OkHttpClient;

    return-object v0
.end method
