.class public Lcom/zendesk/sdk/util/ModuleInjector;
.super Ljava/lang/Object;
.source "ModuleInjector.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/util/LibraryModule;
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/network/impl/ApplicationScope;

    .prologue
    .line 14
    invoke-static {p0}, Lcom/zendesk/sdk/util/ModuleInjector;->injectLibraryModule(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/util/LibraryModule;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/network/impl/RestAdapterModule;
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/network/impl/ApplicationScope;

    .prologue
    .line 14
    invoke-static {p0}, Lcom/zendesk/sdk/util/ModuleInjector;->injectRestAdapterModule(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/network/impl/RestAdapterModule;

    move-result-object v0

    return-object v0
.end method

.method public static injectCachedLibraryModule(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/util/LibraryModule;
    .locals 2
    .param p0, "applicationScope"    # Lcom/zendesk/sdk/network/impl/ApplicationScope;

    .prologue
    .line 17
    invoke-static {p0}, Lcom/zendesk/sdk/util/ModuleInjector;->injectLibraryModuleCache(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/util/ScopeCache;

    move-result-object v0

    new-instance v1, Lcom/zendesk/sdk/util/ModuleInjector$1;

    invoke-direct {v1, p0}, Lcom/zendesk/sdk/util/ModuleInjector$1;-><init>(Lcom/zendesk/sdk/network/impl/ApplicationScope;)V

    invoke-virtual {v0, v1}, Lcom/zendesk/sdk/util/ScopeCache;->get(Lcom/zendesk/sdk/util/DependencyProvider;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/zendesk/sdk/util/LibraryModule;

    return-object v0
.end method

.method public static injectCachedRestAdapterModule(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/network/impl/RestAdapterModule;
    .locals 2
    .param p0, "applicationScope"    # Lcom/zendesk/sdk/network/impl/ApplicationScope;

    .prologue
    .line 27
    invoke-static {p0}, Lcom/zendesk/sdk/util/ModuleInjector;->injectRestAdapterModuleCache(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/util/ScopeCache;

    move-result-object v0

    new-instance v1, Lcom/zendesk/sdk/util/ModuleInjector$2;

    invoke-direct {v1, p0}, Lcom/zendesk/sdk/util/ModuleInjector$2;-><init>(Lcom/zendesk/sdk/network/impl/ApplicationScope;)V

    invoke-virtual {v0, v1}, Lcom/zendesk/sdk/util/ScopeCache;->get(Lcom/zendesk/sdk/util/DependencyProvider;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/zendesk/sdk/network/impl/RestAdapterModule;

    return-object v0
.end method

.method private static injectLibraryModule(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/util/LibraryModule;
    .locals 3
    .param p0, "applicationScope"    # Lcom/zendesk/sdk/network/impl/ApplicationScope;

    .prologue
    .line 52
    new-instance v0, Lcom/zendesk/sdk/util/LibraryModule;

    .line 53
    invoke-static {p0}, Lcom/zendesk/sdk/util/LibraryInjector;->injectGson(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/google/gson/Gson;

    move-result-object v1

    .line 54
    invoke-static {p0}, Lcom/zendesk/sdk/util/LibraryInjector;->injectOkHttpClient(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lokhttp3/OkHttpClient;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/zendesk/sdk/util/LibraryModule;-><init>(Lcom/google/gson/Gson;Lokhttp3/OkHttpClient;)V

    return-object v0
.end method

.method private static injectLibraryModuleCache(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/util/ScopeCache;
    .locals 1
    .param p0, "applicationScope"    # Lcom/zendesk/sdk/network/impl/ApplicationScope;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/zendesk/sdk/network/impl/ApplicationScope;",
            ")",
            "Lcom/zendesk/sdk/util/ScopeCache",
            "<",
            "Lcom/zendesk/sdk/util/LibraryModule;",
            ">;"
        }
    .end annotation

    .prologue
    .line 38
    invoke-virtual {p0}, Lcom/zendesk/sdk/network/impl/ApplicationScope;->getLibraryModuleCache()Lcom/zendesk/sdk/util/ScopeCache;

    move-result-object v0

    return-object v0
.end method

.method private static injectRestAdapterModule(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/network/impl/RestAdapterModule;
    .locals 2
    .param p0, "applicationScope"    # Lcom/zendesk/sdk/network/impl/ApplicationScope;

    .prologue
    .line 47
    new-instance v0, Lcom/zendesk/sdk/network/impl/RestAdapterModule;

    invoke-static {p0}, Lcom/zendesk/sdk/network/impl/RestAdapterInjector;->injectRestAdapter(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lretrofit2/Retrofit;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/zendesk/sdk/network/impl/RestAdapterModule;-><init>(Lretrofit2/Retrofit;)V

    return-object v0
.end method

.method private static injectRestAdapterModuleCache(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/util/ScopeCache;
    .locals 1
    .param p0, "applicationScope"    # Lcom/zendesk/sdk/network/impl/ApplicationScope;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/zendesk/sdk/network/impl/ApplicationScope;",
            ")",
            "Lcom/zendesk/sdk/util/ScopeCache",
            "<",
            "Lcom/zendesk/sdk/network/impl/RestAdapterModule;",
            ">;"
        }
    .end annotation

    .prologue
    .line 42
    invoke-virtual {p0}, Lcom/zendesk/sdk/network/impl/ApplicationScope;->getRestAdapterCache()Lcom/zendesk/sdk/util/ScopeCache;

    move-result-object v0

    return-object v0
.end method
