.class public Lcom/zendesk/sdk/rating/impl/NetworkRateMyAppRule;
.super Ljava/lang/Object;
.source "NetworkRateMyAppRule.java"

# interfaces
.implements Lcom/zendesk/sdk/rating/RateMyAppRule;


# instance fields
.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/zendesk/sdk/rating/impl/NetworkRateMyAppRule;->mContext:Landroid/content/Context;

    .line 25
    return-void

    .line 24
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public permitsShowOfDialog()Z
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/zendesk/sdk/rating/impl/NetworkRateMyAppRule;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/zendesk/sdk/util/NetworkUtils;->isConnected(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method
