.class public final Lcom/zendesk/sdk/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/zendesk/sdk/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final abc_action_bar_home_description:I = 0x7f080000

.field public static final abc_action_bar_home_description_format:I = 0x7f080001

.field public static final abc_action_bar_home_subtitle_description_format:I = 0x7f080002

.field public static final abc_action_bar_up_description:I = 0x7f080003

.field public static final abc_action_menu_overflow_description:I = 0x7f080004

.field public static final abc_action_mode_done:I = 0x7f080005

.field public static final abc_activity_chooser_view_see_all:I = 0x7f080006

.field public static final abc_activitychooserview_choose_application:I = 0x7f080007

.field public static final abc_capital_off:I = 0x7f080008

.field public static final abc_capital_on:I = 0x7f080009

.field public static final abc_font_family_body_1_material:I = 0x7f08051b

.field public static final abc_font_family_body_2_material:I = 0x7f08051c

.field public static final abc_font_family_button_material:I = 0x7f08051d

.field public static final abc_font_family_caption_material:I = 0x7f08051e

.field public static final abc_font_family_display_1_material:I = 0x7f08051f

.field public static final abc_font_family_display_2_material:I = 0x7f080520

.field public static final abc_font_family_display_3_material:I = 0x7f080521

.field public static final abc_font_family_display_4_material:I = 0x7f080522

.field public static final abc_font_family_headline_material:I = 0x7f080523

.field public static final abc_font_family_menu_material:I = 0x7f080524

.field public static final abc_font_family_subhead_material:I = 0x7f080525

.field public static final abc_font_family_title_material:I = 0x7f080526

.field public static final abc_search_hint:I = 0x7f08000a

.field public static final abc_searchview_description_clear:I = 0x7f08000b

.field public static final abc_searchview_description_query:I = 0x7f08000c

.field public static final abc_searchview_description_search:I = 0x7f08000d

.field public static final abc_searchview_description_submit:I = 0x7f08000e

.field public static final abc_searchview_description_voice:I = 0x7f08000f

.field public static final abc_shareactionprovider_share_with:I = 0x7f080010

.field public static final abc_shareactionprovider_share_with_application:I = 0x7f080011

.field public static final abc_toolbar_collapse_description:I = 0x7f080012

.field public static final app_name:I = 0x7f080529

.field public static final appbar_scrolling_view_behavior:I = 0x7f08052a

.field public static final articles_list_fragment_error_message:I = 0x7f080060

.field public static final articles_list_fragment_no_articles_found:I = 0x7f080061

.field public static final articles_search_results_list_fragment_contact_us:I = 0x7f080062

.field public static final articles_search_results_list_fragment_error_message:I = 0x7f080063

.field public static final articles_search_results_list_fragment_no_articles_found:I = 0x7f080064

.field public static final attachment_add_menu:I = 0x7f080065

.field public static final attachment_select_source_choose_existing:I = 0x7f080066

.field public static final attachment_select_source_new_photo:I = 0x7f080067

.field public static final attachment_upload_error_cancel:I = 0x7f080068

.field public static final attachment_upload_error_file_already_added:I = 0x7f080069

.field public static final attachment_upload_error_file_not_found:I = 0x7f08006a

.field public static final attachment_upload_error_file_too_big:I = 0x7f08006b

.field public static final attachment_upload_error_try_again:I = 0x7f08006c

.field public static final attachment_upload_error_upload_failed:I = 0x7f08006d

.field public static final belvedere_dialog_camera:I = 0x7f08052f

.field public static final belvedere_dialog_gallery:I = 0x7f080530

.field public static final belvedere_dialog_unknown:I = 0x7f080531

.field public static final belvedere_sample_camera:I = 0x7f080532

.field public static final belvedere_sample_gallery:I = 0x7f080533

.field public static final belvedere_sdk_fpa_suffix:I = 0x7f080534

.field public static final bottom_sheet_behavior:I = 0x7f080535

.field public static final categories_list_fragment_error_message:I = 0x7f08006e

.field public static final categories_list_fragment_no_categories_found:I = 0x7f08006f

.field public static final character_counter_pattern:I = 0x7f08053c

.field public static final contact_fragment_description_hint:I = 0x7f080070

.field public static final contact_fragment_email_hint:I = 0x7f080071

.field public static final contact_fragment_email_validation_error:I = 0x7f080072

.field public static final contact_fragment_request_subject:I = 0x7f080073

.field public static final contact_fragment_send_button_label:I = 0x7f080074

.field public static final contact_fragment_title:I = 0x7f080075

.field public static final guide_search_subtitle_format:I = 0x7f080543

.field public static final help_search_no_results_label:I = 0x7f080076

.field public static final help_see_all_articles_label:I = 0x7f080077

.field public static final help_see_all_n_articles_label:I = 0x7f080078

.field public static final network_activity_no_connectivity:I = 0x7f080079

.field public static final rate_my_app_dialog_dismiss_action_label:I = 0x7f08007a

.field public static final rate_my_app_dialog_feedback_description_label:I = 0x7f08007b

.field public static final rate_my_app_dialog_feedback_device_api_version:I = 0x7f08007c

.field public static final rate_my_app_dialog_feedback_device_memory:I = 0x7f08007d

.field public static final rate_my_app_dialog_feedback_device_model:I = 0x7f08007e

.field public static final rate_my_app_dialog_feedback_device_name:I = 0x7f08007f

.field public static final rate_my_app_dialog_feedback_device_os_version:I = 0x7f080080

.field public static final rate_my_app_dialog_feedback_request_subject:I = 0x7f080081

.field public static final rate_my_app_dialog_feedback_send_error_no_connectivity_toast:I = 0x7f080082

.field public static final rate_my_app_dialog_feedback_send_error_toast:I = 0x7f080083

.field public static final rate_my_app_dialog_feedback_send_success_toast:I = 0x7f080084

.field public static final rate_my_app_dialog_feedback_title_label:I = 0x7f080085

.field public static final rate_my_app_dialog_negative_action_label:I = 0x7f080086

.field public static final rate_my_app_dialog_positive_action_label:I = 0x7f080087

.field public static final rate_my_app_dialog_title_label:I = 0x7f080088

.field public static final rate_my_app_dialogue_feedback_cancel_button_label:I = 0x7f080089

.field public static final rate_my_app_dialogue_feedback_issue_hint:I = 0x7f08008a

.field public static final rate_my_app_dialogue_feedback_send_button_label:I = 0x7f08008b

.field public static final request_list_activity_title:I = 0x7f08008c

.field public static final request_list_fragment_error_message:I = 0x7f08008d

.field public static final request_list_fragment_no_requests:I = 0x7f08008e

.field public static final request_view_comment_entry_hint:I = 0x7f08008f

.field public static final retry_view_button_label:I = 0x7f080090

.field public static final row_request_unread_indicator_alt:I = 0x7f080091

.field public static final sections_list_fragment_error_message:I = 0x7f080092

.field public static final sections_list_fragment_no_sections_found:I = 0x7f080093

.field public static final status_bar_notification_info_overflow:I = 0x7f080040

.field public static final support_activity_title:I = 0x7f080094

.field public static final support_activity_unable_to_contact_support:I = 0x7f080095

.field public static final support_contact_menu:I = 0x7f080096

.field public static final support_conversations_menu:I = 0x7f080097

.field public static final support_list_search_hint:I = 0x7f080098

.field public static final title_activity_rate_my_app_dialogue_test:I = 0x7f08056f

.field public static final view_article_attachments_error:I = 0x7f080099

.field public static final view_article_seperator:I = 0x7f08009a

.field public static final view_request_agent_avatar_imageview_alt:I = 0x7f08009b

.field public static final view_request_load_comments_error:I = 0x7f08009c

.field public static final view_request_send_comment_error:I = 0x7f08009d


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1084
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
