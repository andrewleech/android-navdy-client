.class public final enum Lcom/zendesk/sdk/deeplinking/ZendeskDeepLinking;
.super Ljava/lang/Enum;
.source "ZendeskDeepLinking.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/zendesk/sdk/deeplinking/ZendeskDeepLinking;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/zendesk/sdk/deeplinking/ZendeskDeepLinking;

.field public static final enum INSTANCE:Lcom/zendesk/sdk/deeplinking/ZendeskDeepLinking;


# instance fields
.field private mActiveHandlers:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap",
            "<",
            "Lcom/zendesk/sdk/deeplinking/actions/ActionHandler;",
            "[",
            "Lcom/zendesk/sdk/deeplinking/actions/Action;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 38
    new-instance v0, Lcom/zendesk/sdk/deeplinking/ZendeskDeepLinking;

    const-string v1, "INSTANCE"

    invoke-direct {v0, v1, v2}, Lcom/zendesk/sdk/deeplinking/ZendeskDeepLinking;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/zendesk/sdk/deeplinking/ZendeskDeepLinking;->INSTANCE:Lcom/zendesk/sdk/deeplinking/ZendeskDeepLinking;

    .line 36
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/zendesk/sdk/deeplinking/ZendeskDeepLinking;

    sget-object v1, Lcom/zendesk/sdk/deeplinking/ZendeskDeepLinking;->INSTANCE:Lcom/zendesk/sdk/deeplinking/ZendeskDeepLinking;

    aput-object v1, v0, v2

    sput-object v0, Lcom/zendesk/sdk/deeplinking/ZendeskDeepLinking;->$VALUES:[Lcom/zendesk/sdk/deeplinking/ZendeskDeepLinking;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 46
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 47
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, Lcom/zendesk/sdk/deeplinking/ZendeskDeepLinking;->mActiveHandlers:Ljava/util/WeakHashMap;

    .line 48
    return-void
.end method

.method private canHandle(Lcom/zendesk/sdk/deeplinking/actions/ActionType;Lcom/zendesk/sdk/deeplinking/actions/ActionData;)Z
    .locals 1
    .param p1, "actionType"    # Lcom/zendesk/sdk/deeplinking/actions/ActionType;
    .param p2, "data"    # Lcom/zendesk/sdk/deeplinking/actions/ActionData;

    .prologue
    .line 311
    invoke-direct {p0, p1, p2}, Lcom/zendesk/sdk/deeplinking/ZendeskDeepLinking;->getCompatibleEntries(Lcom/zendesk/sdk/deeplinking/actions/ActionType;Lcom/zendesk/sdk/deeplinking/actions/ActionData;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private execute(Lcom/zendesk/sdk/deeplinking/actions/ActionType;Lcom/zendesk/sdk/deeplinking/actions/ActionData;)V
    .locals 8
    .param p1, "actionType"    # Lcom/zendesk/sdk/deeplinking/actions/ActionType;
    .param p2, "actionData"    # Lcom/zendesk/sdk/deeplinking/actions/ActionData;

    .prologue
    .line 278
    invoke-direct {p0, p1, p2}, Lcom/zendesk/sdk/deeplinking/ZendeskDeepLinking;->getCompatibleEntries(Lcom/zendesk/sdk/deeplinking/actions/ActionType;Lcom/zendesk/sdk/deeplinking/actions/ActionData;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 279
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/zendesk/sdk/deeplinking/actions/ActionHandler;[Lcom/zendesk/sdk/deeplinking/actions/Action;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lcom/zendesk/sdk/deeplinking/actions/Action;

    array-length v5, v2

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v5, :cond_0

    aget-object v0, v2, v3

    .line 280
    .local v0, "action":Lcom/zendesk/sdk/deeplinking/actions/Action;
    new-instance v6, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v7

    invoke-direct {v6, v7}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v7, Lcom/zendesk/sdk/deeplinking/ZendeskDeepLinking$1;

    invoke-direct {v7, p0, v0, v1, p2}, Lcom/zendesk/sdk/deeplinking/ZendeskDeepLinking$1;-><init>(Lcom/zendesk/sdk/deeplinking/ZendeskDeepLinking;Lcom/zendesk/sdk/deeplinking/actions/Action;Ljava/util/Map$Entry;Lcom/zendesk/sdk/deeplinking/actions/ActionData;)V

    invoke-virtual {v6, v7}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 279
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 289
    .end local v0    # "action":Lcom/zendesk/sdk/deeplinking/actions/Action;
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/zendesk/sdk/deeplinking/actions/ActionHandler;[Lcom/zendesk/sdk/deeplinking/actions/Action;>;"
    :cond_1
    return-void
.end method

.method private getCompatibleEntries(Lcom/zendesk/sdk/deeplinking/actions/ActionType;Lcom/zendesk/sdk/deeplinking/actions/ActionData;)Ljava/util/List;
    .locals 9
    .param p1, "actionType"    # Lcom/zendesk/sdk/deeplinking/actions/ActionType;
    .param p2, "actionData"    # Lcom/zendesk/sdk/deeplinking/actions/ActionData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/zendesk/sdk/deeplinking/actions/ActionType;",
            "Lcom/zendesk/sdk/deeplinking/actions/ActionData;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/util/Map$Entry",
            "<",
            "Lcom/zendesk/sdk/deeplinking/actions/ActionHandler;",
            "[",
            "Lcom/zendesk/sdk/deeplinking/actions/Action;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 293
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 295
    .local v2, "result":Ljava/util/List;, "Ljava/util/List<Ljava/util/Map$Entry<Lcom/zendesk/sdk/deeplinking/actions/ActionHandler;[Lcom/zendesk/sdk/deeplinking/actions/Action;>;>;"
    iget-object v4, p0, Lcom/zendesk/sdk/deeplinking/ZendeskDeepLinking;->mActiveHandlers:Ljava/util/WeakHashMap;

    invoke-virtual {v4}, Ljava/util/WeakHashMap;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 296
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/zendesk/sdk/deeplinking/actions/ActionHandler;[Lcom/zendesk/sdk/deeplinking/actions/Action;>;"
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 297
    .local v3, "validActions":Ljava/util/List;, "Ljava/util/List<Lcom/zendesk/sdk/deeplinking/actions/Action;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Lcom/zendesk/sdk/deeplinking/actions/Action;

    array-length v7, v4

    const/4 v5, 0x0

    :goto_1
    if-ge v5, v7, :cond_2

    aget-object v0, v4, v5

    .line 298
    .local v0, "action":Lcom/zendesk/sdk/deeplinking/actions/Action;
    invoke-virtual {v0}, Lcom/zendesk/sdk/deeplinking/actions/Action;->getActionType()Lcom/zendesk/sdk/deeplinking/actions/ActionType;

    move-result-object v8

    if-ne v8, p1, :cond_1

    invoke-virtual {v0, p2}, Lcom/zendesk/sdk/deeplinking/actions/Action;->canHandleData(Lcom/zendesk/sdk/deeplinking/actions/ActionData;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 299
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 297
    :cond_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 302
    .end local v0    # "action":Lcom/zendesk/sdk/deeplinking/actions/Action;
    :cond_2
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_0

    .line 303
    new-instance v4, Ljava/util/AbstractMap$SimpleEntry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v7

    new-array v7, v7, [Lcom/zendesk/sdk/deeplinking/actions/Action;

    invoke-interface {v3, v7}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v7

    invoke-direct {v4, v5, v7}, Ljava/util/AbstractMap$SimpleEntry;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 307
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/zendesk/sdk/deeplinking/actions/ActionHandler;[Lcom/zendesk/sdk/deeplinking/actions/Action;>;"
    .end local v3    # "validActions":Ljava/util/List;, "Ljava/util/List<Lcom/zendesk/sdk/deeplinking/actions/Action;>;"
    :cond_3
    return-object v2
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/zendesk/sdk/deeplinking/ZendeskDeepLinking;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 36
    const-class v0, Lcom/zendesk/sdk/deeplinking/ZendeskDeepLinking;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/zendesk/sdk/deeplinking/ZendeskDeepLinking;

    return-object v0
.end method

.method public static values()[Lcom/zendesk/sdk/deeplinking/ZendeskDeepLinking;
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lcom/zendesk/sdk/deeplinking/ZendeskDeepLinking;->$VALUES:[Lcom/zendesk/sdk/deeplinking/ZendeskDeepLinking;

    invoke-virtual {v0}, [Lcom/zendesk/sdk/deeplinking/ZendeskDeepLinking;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/zendesk/sdk/deeplinking/ZendeskDeepLinking;

    return-object v0
.end method


# virtual methods
.method public getArticleIntent(Landroid/content/Context;Lcom/zendesk/sdk/model/helpcenter/Article;Ljava/lang/String;Ljava/util/ArrayList;Landroid/content/Intent;)Landroid/content/Intent;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "article"    # Lcom/zendesk/sdk/model/helpcenter/Article;
    .param p3, "locale"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/util/ArrayList;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p5, "fallbackActivity"    # Landroid/content/Intent;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/zendesk/sdk/model/helpcenter/Article;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/Intent;",
            ">;",
            "Landroid/content/Intent;",
            ")",
            "Landroid/content/Intent;"
        }
    .end annotation

    .prologue
    .line 180
    .local p4, "backStackActivities":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/Intent;>;"
    new-instance v0, Lcom/zendesk/sdk/deeplinking/targets/ArticleConfiguration;

    invoke-direct {v0, p2, p3, p4, p5}, Lcom/zendesk/sdk/deeplinking/targets/ArticleConfiguration;-><init>(Lcom/zendesk/sdk/model/helpcenter/Article;Ljava/lang/String;Ljava/util/ArrayList;Landroid/content/Intent;)V

    .line 181
    .local v0, "articleConfiguration":Lcom/zendesk/sdk/deeplinking/targets/ArticleConfiguration;
    new-instance v1, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTargetArticle;

    invoke-direct {v1}, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTargetArticle;-><init>()V

    invoke-virtual {v1, p1, v0}, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTargetArticle;->getIntent(Landroid/content/Context;Lcom/zendesk/sdk/deeplinking/targets/TargetConfiguration;)Landroid/content/Intent;

    move-result-object v1

    return-object v1
.end method

.method public getArticleIntent(Landroid/content/Context;Lcom/zendesk/sdk/model/helpcenter/Article;Ljava/lang/String;Ljava/util/ArrayList;Landroid/content/Intent;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "article"    # Lcom/zendesk/sdk/model/helpcenter/Article;
    .param p3, "locale"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/util/ArrayList;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p5, "fallbackActivity"    # Landroid/content/Intent;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p6, "zendeskUrl"    # Ljava/lang/String;
    .param p7, "applicationId"    # Ljava/lang/String;
    .param p8, "oauthClientId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/zendesk/sdk/model/helpcenter/Article;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/Intent;",
            ">;",
            "Landroid/content/Intent;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Landroid/content/Intent;"
        }
    .end annotation

    .prologue
    .line 210
    .local p4, "backStackItemArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/Intent;>;"
    new-instance v2, Lcom/zendesk/sdk/deeplinking/targets/ArticleConfiguration;

    invoke-direct {v2, p2, p3, p4, p5}, Lcom/zendesk/sdk/deeplinking/targets/ArticleConfiguration;-><init>(Lcom/zendesk/sdk/model/helpcenter/Article;Ljava/lang/String;Ljava/util/ArrayList;Landroid/content/Intent;)V

    .line 211
    .local v2, "articleConfiguration":Lcom/zendesk/sdk/deeplinking/targets/ArticleConfiguration;
    new-instance v0, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTargetArticle;

    invoke-direct {v0}, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTargetArticle;-><init>()V

    move-object v1, p1

    move-object v3, p6

    move-object v4, p7

    move-object v5, p8

    invoke-virtual/range {v0 .. v5}, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTargetArticle;->getIntent(Landroid/content/Context;Lcom/zendesk/sdk/deeplinking/targets/TargetConfiguration;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public getArticleIntent(Landroid/content/Context;Lcom/zendesk/sdk/model/helpcenter/SimpleArticle;Ljava/lang/String;Ljava/util/ArrayList;Landroid/content/Intent;)Landroid/content/Intent;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "simpleArticle"    # Lcom/zendesk/sdk/model/helpcenter/SimpleArticle;
    .param p3, "locale"    # Ljava/lang/String;
    .param p5, "fallbackActivity"    # Landroid/content/Intent;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/zendesk/sdk/model/helpcenter/SimpleArticle;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/Intent;",
            ">;",
            "Landroid/content/Intent;",
            ")",
            "Landroid/content/Intent;"
        }
    .end annotation

    .prologue
    .line 241
    .local p4, "backStackActivities":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/Intent;>;"
    new-instance v0, Lcom/zendesk/sdk/deeplinking/targets/ArticleConfiguration;

    invoke-direct {v0, p2, p3, p4, p5}, Lcom/zendesk/sdk/deeplinking/targets/ArticleConfiguration;-><init>(Lcom/zendesk/sdk/model/helpcenter/SimpleArticle;Ljava/lang/String;Ljava/util/ArrayList;Landroid/content/Intent;)V

    .line 242
    .local v0, "articleConfiguration":Lcom/zendesk/sdk/deeplinking/targets/ArticleConfiguration;
    new-instance v1, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTargetArticle;

    invoke-direct {v1}, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTargetArticle;-><init>()V

    invoke-virtual {v1, p1, v0}, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTargetArticle;->getIntent(Landroid/content/Context;Lcom/zendesk/sdk/deeplinking/targets/TargetConfiguration;)Landroid/content/Intent;

    move-result-object v1

    return-object v1
.end method

.method public getArticleIntent(Landroid/content/Context;Lcom/zendesk/sdk/model/helpcenter/SimpleArticle;Ljava/lang/String;Ljava/util/ArrayList;Landroid/content/Intent;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "simpleArticle"    # Lcom/zendesk/sdk/model/helpcenter/SimpleArticle;
    .param p3, "locale"    # Ljava/lang/String;
    .param p5, "fallbackActivity"    # Landroid/content/Intent;
    .param p6, "zendeskUrl"    # Ljava/lang/String;
    .param p7, "applicationId"    # Ljava/lang/String;
    .param p8, "oauthClientId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/zendesk/sdk/model/helpcenter/SimpleArticle;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/Intent;",
            ">;",
            "Landroid/content/Intent;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Landroid/content/Intent;"
        }
    .end annotation

    .prologue
    .line 272
    .local p4, "backStackItemArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/Intent;>;"
    new-instance v2, Lcom/zendesk/sdk/deeplinking/targets/ArticleConfiguration;

    invoke-direct {v2, p2, p3, p4, p5}, Lcom/zendesk/sdk/deeplinking/targets/ArticleConfiguration;-><init>(Lcom/zendesk/sdk/model/helpcenter/SimpleArticle;Ljava/lang/String;Ljava/util/ArrayList;Landroid/content/Intent;)V

    .line 273
    .local v2, "articleConfiguration":Lcom/zendesk/sdk/deeplinking/targets/ArticleConfiguration;
    new-instance v0, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTargetArticle;

    invoke-direct {v0}, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTargetArticle;-><init>()V

    move-object v1, p1

    move-object v3, p6

    move-object v4, p7

    move-object v5, p8

    invoke-virtual/range {v0 .. v5}, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTargetArticle;->getIntent(Landroid/content/Context;Lcom/zendesk/sdk/deeplinking/targets/TargetConfiguration;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public getRequestIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Landroid/content/Intent;)Landroid/content/Intent;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "requestId"    # Ljava/lang/String;
    .param p3, "subject"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/util/ArrayList;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p5, "fallbackActivity"    # Landroid/content/Intent;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/Intent;",
            ">;",
            "Landroid/content/Intent;",
            ")",
            "Landroid/content/Intent;"
        }
    .end annotation

    .prologue
    .line 119
    .local p4, "backStackActivities":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/Intent;>;"
    new-instance v0, Lcom/zendesk/sdk/deeplinking/targets/RequestConfiguration;

    invoke-direct {v0, p2, p3, p4, p5}, Lcom/zendesk/sdk/deeplinking/targets/RequestConfiguration;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Landroid/content/Intent;)V

    .line 120
    .local v0, "requestConfiguration":Lcom/zendesk/sdk/deeplinking/targets/RequestConfiguration;
    new-instance v1, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTargetRequest;

    invoke-direct {v1}, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTargetRequest;-><init>()V

    invoke-virtual {v1, p1, v0}, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTargetRequest;->getIntent(Landroid/content/Context;Lcom/zendesk/sdk/deeplinking/targets/TargetConfiguration;)Landroid/content/Intent;

    move-result-object v1

    return-object v1
.end method

.method public getRequestIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Landroid/content/Intent;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "requestId"    # Ljava/lang/String;
    .param p3, "subject"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/util/ArrayList;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p5, "fallbackActivity"    # Landroid/content/Intent;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p6, "zendeskUrl"    # Ljava/lang/String;
    .param p7, "applicationId"    # Ljava/lang/String;
    .param p8, "oauthClientId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/Intent;",
            ">;",
            "Landroid/content/Intent;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Landroid/content/Intent;"
        }
    .end annotation

    .prologue
    .line 149
    .local p4, "backStackActivities":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/Intent;>;"
    new-instance v2, Lcom/zendesk/sdk/deeplinking/targets/RequestConfiguration;

    invoke-direct {v2, p2, p3, p4, p5}, Lcom/zendesk/sdk/deeplinking/targets/RequestConfiguration;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Landroid/content/Intent;)V

    .line 150
    .local v2, "requestConfiguration":Lcom/zendesk/sdk/deeplinking/targets/RequestConfiguration;
    new-instance v0, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTargetRequest;

    invoke-direct {v0}, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTargetRequest;-><init>()V

    move-object v1, p1

    move-object v3, p6

    move-object v4, p7

    move-object v5, p8

    invoke-virtual/range {v0 .. v5}, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTargetRequest;->getIntent(Landroid/content/Context;Lcom/zendesk/sdk/deeplinking/targets/TargetConfiguration;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public refreshComments(Ljava/lang/String;)Z
    .locals 3
    .param p1, "requestId"    # Ljava/lang/String;

    .prologue
    .line 83
    new-instance v0, Lcom/zendesk/sdk/deeplinking/actions/ActionRefreshComments$ActionRefreshCommentsData;

    invoke-direct {v0, p1}, Lcom/zendesk/sdk/deeplinking/actions/ActionRefreshComments$ActionRefreshCommentsData;-><init>(Ljava/lang/String;)V

    .line 84
    .local v0, "actionRefreshCommentsData":Lcom/zendesk/sdk/deeplinking/actions/ActionRefreshComments$ActionRefreshCommentsData;
    sget-object v2, Lcom/zendesk/sdk/deeplinking/actions/ActionType;->RELOAD_COMMENT_STREAM:Lcom/zendesk/sdk/deeplinking/actions/ActionType;

    invoke-direct {p0, v2, v0}, Lcom/zendesk/sdk/deeplinking/ZendeskDeepLinking;->canHandle(Lcom/zendesk/sdk/deeplinking/actions/ActionType;Lcom/zendesk/sdk/deeplinking/actions/ActionData;)Z

    move-result v1

    .line 86
    .local v1, "canHandle":Z
    if-eqz v1, :cond_0

    .line 87
    sget-object v2, Lcom/zendesk/sdk/deeplinking/actions/ActionType;->RELOAD_COMMENT_STREAM:Lcom/zendesk/sdk/deeplinking/actions/ActionType;

    invoke-direct {p0, v2, v0}, Lcom/zendesk/sdk/deeplinking/ZendeskDeepLinking;->execute(Lcom/zendesk/sdk/deeplinking/actions/ActionType;Lcom/zendesk/sdk/deeplinking/actions/ActionData;)V

    .line 90
    :cond_0
    return v1
.end method

.method public varargs registerAction(Lcom/zendesk/sdk/deeplinking/actions/ActionHandler;[Lcom/zendesk/sdk/deeplinking/actions/Action;)V
    .locals 1
    .param p1, "actionHandler"    # Lcom/zendesk/sdk/deeplinking/actions/ActionHandler;
    .param p2, "actions"    # [Lcom/zendesk/sdk/deeplinking/actions/Action;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/zendesk/sdk/deeplinking/ZendeskDeepLinking;->mActiveHandlers:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63
    return-void
.end method

.method public unregisterAction(Lcom/zendesk/sdk/deeplinking/actions/ActionHandler;)V
    .locals 1
    .param p1, "actionHandler"    # Lcom/zendesk/sdk/deeplinking/actions/ActionHandler;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/zendesk/sdk/deeplinking/ZendeskDeepLinking;->mActiveHandlers:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 72
    return-void
.end method
