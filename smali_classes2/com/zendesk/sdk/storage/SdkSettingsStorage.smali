.class public interface abstract Lcom/zendesk/sdk/storage/SdkSettingsStorage;
.super Ljava/lang/Object;
.source "SdkSettingsStorage.java"


# virtual methods
.method public abstract areSettingsUpToDate(JLjava/util/concurrent/TimeUnit;)Z
.end method

.method public abstract deleteStoredSettings()V
.end method

.method public abstract getStoredSettings()Lcom/zendesk/sdk/model/settings/SafeMobileSettings;
.end method

.method public abstract hasStoredSdkSettings()Z
.end method

.method public abstract setStoredSettings(Lcom/zendesk/sdk/model/settings/SafeMobileSettings;)V
.end method
