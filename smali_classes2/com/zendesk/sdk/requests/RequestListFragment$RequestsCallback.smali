.class Lcom/zendesk/sdk/requests/RequestListFragment$RequestsCallback;
.super Lcom/zendesk/service/ZendeskCallback;
.source "RequestListFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/zendesk/sdk/requests/RequestListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "RequestsCallback"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/zendesk/service/ZendeskCallback",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/zendesk/sdk/model/request/Request;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/zendesk/sdk/requests/RequestListFragment;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/requests/RequestListFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/zendesk/sdk/requests/RequestListFragment;

    .prologue
    .line 267
    iput-object p1, p0, Lcom/zendesk/sdk/requests/RequestListFragment$RequestsCallback;->this$0:Lcom/zendesk/sdk/requests/RequestListFragment;

    invoke-direct {p0}, Lcom/zendesk/service/ZendeskCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Lcom/zendesk/service/ErrorResponse;)V
    .locals 4
    .param p1, "error"    # Lcom/zendesk/service/ErrorResponse;

    .prologue
    const/4 v3, 0x0

    .line 312
    iget-object v0, p0, Lcom/zendesk/sdk/requests/RequestListFragment$RequestsCallback;->this$0:Lcom/zendesk/sdk/requests/RequestListFragment;

    invoke-static {v0}, Lcom/zendesk/sdk/requests/RequestListFragment;->access$000(Lcom/zendesk/sdk/requests/RequestListFragment;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 313
    invoke-static {}, Lcom/zendesk/sdk/requests/RequestListFragment;->access$300()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed to fetch requests: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1}, Lcom/zendesk/service/ErrorResponse;->getReason()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " status "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1}, Lcom/zendesk/service/ErrorResponse;->getStatus()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/zendesk/logger/Logger;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 314
    iget-object v0, p0, Lcom/zendesk/sdk/requests/RequestListFragment$RequestsCallback;->this$0:Lcom/zendesk/sdk/requests/RequestListFragment;

    sget-object v1, Lcom/zendesk/sdk/ui/LoadingState;->ERRORED:Lcom/zendesk/sdk/ui/LoadingState;

    invoke-static {v0, v1}, Lcom/zendesk/sdk/requests/RequestListFragment;->access$100(Lcom/zendesk/sdk/requests/RequestListFragment;Lcom/zendesk/sdk/ui/LoadingState;)V

    .line 316
    iget-object v0, p0, Lcom/zendesk/sdk/requests/RequestListFragment$RequestsCallback;->this$0:Lcom/zendesk/sdk/requests/RequestListFragment;

    invoke-static {v0}, Lcom/zendesk/sdk/requests/RequestListFragment;->access$200(Lcom/zendesk/sdk/requests/RequestListFragment;)Lcom/zendesk/sdk/network/RequestLoadingListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 317
    iget-object v0, p0, Lcom/zendesk/sdk/requests/RequestListFragment$RequestsCallback;->this$0:Lcom/zendesk/sdk/requests/RequestListFragment;

    invoke-static {v0}, Lcom/zendesk/sdk/requests/RequestListFragment;->access$200(Lcom/zendesk/sdk/requests/RequestListFragment;)Lcom/zendesk/sdk/network/RequestLoadingListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/zendesk/sdk/network/RequestLoadingListener;->onLoadError(Lcom/zendesk/service/ErrorResponse;)V

    .line 319
    :cond_0
    return-void
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 267
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/zendesk/sdk/requests/RequestListFragment$RequestsCallback;->onSuccess(Ljava/util/List;)V

    return-void
.end method

.method public onSuccess(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/model/request/Request;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 270
    .local p1, "requests":Ljava/util/List;, "Ljava/util/List<Lcom/zendesk/sdk/model/request/Request;>;"
    iget-object v2, p0, Lcom/zendesk/sdk/requests/RequestListFragment$RequestsCallback;->this$0:Lcom/zendesk/sdk/requests/RequestListFragment;

    invoke-static {v2}, Lcom/zendesk/sdk/requests/RequestListFragment;->access$000(Lcom/zendesk/sdk/requests/RequestListFragment;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 271
    iget-object v2, p0, Lcom/zendesk/sdk/requests/RequestListFragment$RequestsCallback;->this$0:Lcom/zendesk/sdk/requests/RequestListFragment;

    sget-object v3, Lcom/zendesk/sdk/ui/LoadingState;->DISPLAYING:Lcom/zendesk/sdk/ui/LoadingState;

    invoke-static {v2, v3}, Lcom/zendesk/sdk/requests/RequestListFragment;->access$100(Lcom/zendesk/sdk/requests/RequestListFragment;Lcom/zendesk/sdk/ui/LoadingState;)V

    .line 273
    iget-object v2, p0, Lcom/zendesk/sdk/requests/RequestListFragment$RequestsCallback;->this$0:Lcom/zendesk/sdk/requests/RequestListFragment;

    invoke-static {v2}, Lcom/zendesk/sdk/requests/RequestListFragment;->access$200(Lcom/zendesk/sdk/requests/RequestListFragment;)Lcom/zendesk/sdk/network/RequestLoadingListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 274
    const/4 v1, 0x0

    .line 276
    .local v1, "numberOfRequests":I
    invoke-static {p1}, Lcom/zendesk/util/CollectionUtils;->isEmpty(Ljava/util/Collection;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 277
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    .line 280
    :cond_0
    iget-object v2, p0, Lcom/zendesk/sdk/requests/RequestListFragment$RequestsCallback;->this$0:Lcom/zendesk/sdk/requests/RequestListFragment;

    invoke-static {v2}, Lcom/zendesk/sdk/requests/RequestListFragment;->access$200(Lcom/zendesk/sdk/requests/RequestListFragment;)Lcom/zendesk/sdk/network/RequestLoadingListener;

    move-result-object v2

    invoke-interface {v2, v1}, Lcom/zendesk/sdk/network/RequestLoadingListener;->onLoadFinished(I)V

    .line 283
    .end local v1    # "numberOfRequests":I
    :cond_1
    iget-object v2, p0, Lcom/zendesk/sdk/requests/RequestListFragment$RequestsCallback;->this$0:Lcom/zendesk/sdk/requests/RequestListFragment;

    invoke-virtual {v2}, Lcom/zendesk/sdk/requests/RequestListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    .line 285
    .local v0, "listView":Landroid/widget/ListView;
    if-eqz v0, :cond_2

    iget-object v2, p0, Lcom/zendesk/sdk/requests/RequestListFragment$RequestsCallback;->this$0:Lcom/zendesk/sdk/requests/RequestListFragment;

    invoke-virtual {v2}, Lcom/zendesk/sdk/requests/RequestListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 286
    iget-object v2, p0, Lcom/zendesk/sdk/requests/RequestListFragment$RequestsCallback;->this$0:Lcom/zendesk/sdk/requests/RequestListFragment;

    new-instance v3, Lcom/zendesk/sdk/requests/RequestsAdapter;

    iget-object v4, p0, Lcom/zendesk/sdk/requests/RequestListFragment$RequestsCallback;->this$0:Lcom/zendesk/sdk/requests/RequestListFragment;

    invoke-virtual {v4}, Lcom/zendesk/sdk/requests/RequestListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-direct {v3, v4, p1}, Lcom/zendesk/sdk/requests/RequestsAdapter;-><init>(Landroid/content/Context;Ljava/util/List;)V

    invoke-virtual {v2, v3}, Lcom/zendesk/sdk/requests/RequestListFragment;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 287
    new-instance v2, Lcom/zendesk/sdk/requests/RequestListFragment$RequestsCallback$1;

    invoke-direct {v2, p0, v0}, Lcom/zendesk/sdk/requests/RequestListFragment$RequestsCallback$1;-><init>(Lcom/zendesk/sdk/requests/RequestListFragment$RequestsCallback;Landroid/widget/ListView;)V

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 308
    :cond_2
    return-void
.end method
