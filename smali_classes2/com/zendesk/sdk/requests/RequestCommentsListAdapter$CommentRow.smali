.class Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRow;
.super Landroid/widget/RelativeLayout;
.source "RequestCommentsListAdapter.java"

# interfaces
.implements Lcom/zendesk/sdk/ui/ListRowView;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ViewConstructor"
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/zendesk/sdk/requests/RequestCommentsListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "CommentRow"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/RelativeLayout;",
        "Lcom/zendesk/sdk/ui/ListRowView",
        "<",
        "Lcom/zendesk/sdk/requests/CommentWithUser;",
        ">;"
    }
.end annotation


# static fields
.field private static final CREATED_AT_DATE_FORMAT:Ljava/lang/String; = "dd MMMM yyy"


# instance fields
.field private mAttachmentsContainer:Landroid/view/ViewGroup;

.field private mAvatarImageView:Landroid/widget/ImageView;

.field private mCommentDateTextView:Landroid/widget/TextView;

.field private final mContext:Landroid/content/Context;

.field private final mIdHolder:Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$IdHolder;

.field private mLoadImageCallback:Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$AttachmentOnClickListener;

.field private mNameTextView:Landroid/widget/TextView;

.field private mResponseTextView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$IdHolder;Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$AttachmentOnClickListener;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "idHolder"    # Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$IdHolder;
    .param p3, "attachmentOnClickListener"    # Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$AttachmentOnClickListener;

    .prologue
    .line 192
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 193
    iput-object p1, p0, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRow;->mContext:Landroid/content/Context;

    .line 194
    iput-object p2, p0, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRow;->mIdHolder:Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$IdHolder;

    .line 195
    iput-object p3, p0, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRow;->mLoadImageCallback:Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$AttachmentOnClickListener;

    .line 196
    invoke-direct {p0}, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRow;->initialise()V

    .line 197
    return-void
.end method

.method static synthetic access$200(Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRow;)Landroid/view/ViewGroup;
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRow;

    .prologue
    .line 173
    iget-object v0, p0, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRow;->mAttachmentsContainer:Landroid/view/ViewGroup;

    return-object v0
.end method

.method static synthetic access$300(Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRow;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRow;

    .prologue
    .line 173
    iget-object v0, p0, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRow;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$400(Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRow;)Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$AttachmentOnClickListener;
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRow;

    .prologue
    .line 173
    iget-object v0, p0, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRow;->mLoadImageCallback:Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$AttachmentOnClickListener;

    return-object v0
.end method

.method private initialise()V
    .locals 3

    .prologue
    .line 200
    iget-object v1, p0, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRow;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    iget-object v2, p0, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRow;->mIdHolder:Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$IdHolder;

    invoke-interface {v2}, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$IdHolder;->getContainerId()I

    move-result v2

    invoke-virtual {v1, v2, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 202
    .local v0, "container":Landroid/view/View;
    iget-object v1, p0, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRow;->mIdHolder:Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$IdHolder;

    invoke-interface {v1}, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$IdHolder;->getAvatarId()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRow;->mAvatarImageView:Landroid/widget/ImageView;

    .line 203
    iget-object v1, p0, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRow;->mIdHolder:Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$IdHolder;

    invoke-interface {v1}, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$IdHolder;->getNameId()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRow;->mNameTextView:Landroid/widget/TextView;

    .line 204
    iget-object v1, p0, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRow;->mIdHolder:Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$IdHolder;

    invoke-interface {v1}, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$IdHolder;->getResponseId()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRow;->mResponseTextView:Landroid/widget/TextView;

    .line 205
    iget-object v1, p0, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRow;->mIdHolder:Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$IdHolder;

    invoke-interface {v1}, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$IdHolder;->getDateId()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRow;->mCommentDateTextView:Landroid/widget/TextView;

    .line 206
    iget-object v1, p0, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRow;->mIdHolder:Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$IdHolder;

    invoke-interface {v1}, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$IdHolder;->getAttachmentsContainerId()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRow;->mAttachmentsContainer:Landroid/view/ViewGroup;

    .line 207
    return-void
.end method


# virtual methods
.method public bind(Lcom/zendesk/sdk/requests/CommentWithUser;)V
    .locals 9
    .param p1, "comment"    # Lcom/zendesk/sdk/requests/CommentWithUser;

    .prologue
    const/4 v8, 0x0

    .line 211
    iget-object v4, p0, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRow;->mNameTextView:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/zendesk/sdk/requests/CommentWithUser;->getAuthor()Lcom/zendesk/sdk/model/request/User;

    move-result-object v5

    invoke-virtual {v5}, Lcom/zendesk/sdk/model/request/User;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 212
    iget-object v4, p0, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRow;->mResponseTextView:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/zendesk/sdk/requests/CommentWithUser;->getComment()Lcom/zendesk/sdk/model/request/CommentResponse;

    move-result-object v5

    invoke-virtual {v5}, Lcom/zendesk/sdk/model/request/CommentResponse;->getBody()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 213
    iget-object v4, p0, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRow;->mCommentDateTextView:Landroid/widget/TextView;

    new-instance v5, Ljava/text/SimpleDateFormat;

    const-string v6, "dd MMMM yyy"

    .line 214
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 215
    invoke-virtual {p1}, Lcom/zendesk/sdk/requests/CommentWithUser;->getComment()Lcom/zendesk/sdk/model/request/CommentResponse;

    move-result-object v6

    invoke-virtual {v6}, Lcom/zendesk/sdk/model/request/CommentResponse;->getCreatedAt()Ljava/util/Date;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v5

    .line 213
    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 220
    sget-object v4, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRowAttachmentHelper;->heightMap:Ljava/util/Map;

    .line 221
    invoke-virtual {p1}, Lcom/zendesk/sdk/requests/CommentWithUser;->getComment()Lcom/zendesk/sdk/model/request/CommentResponse;

    move-result-object v5

    invoke-virtual {v5}, Lcom/zendesk/sdk/model/request/CommentResponse;->getId()Ljava/lang/Long;

    move-result-object v5

    .line 220
    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map;

    .line 223
    .local v3, "savedHeightMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Long;Ljava/lang/Integer;>;"
    if-eqz v3, :cond_1

    .line 225
    const/4 v2, 0x0

    .line 231
    .local v2, "savedHeight":I
    invoke-interface {v3}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 232
    .local v0, "attachmentId":Ljava/lang/Long;
    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    add-int/2addr v2, v4

    .line 233
    goto :goto_0

    .line 239
    .end local v0    # "attachmentId":Ljava/lang/Long;
    :cond_0
    iget-object v4, p0, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRow;->mAttachmentsContainer:Landroid/view/ViewGroup;

    new-instance v5, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v6, -0x1

    invoke-direct {v5, v6, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v4, v5}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 244
    .end local v2    # "savedHeight":I
    :cond_1
    invoke-virtual {p0}, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRow;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/zendesk/sdk/R$dimen;->view_request_comment_avatar_size:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    invoke-virtual {p0}, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRow;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    iget v5, v5, Landroid/util/DisplayMetrics;->density:F

    div-float/2addr v4, v5

    float-to-int v1, v4

    .line 246
    .local v1, "dp":I
    invoke-virtual {p1}, Lcom/zendesk/sdk/requests/CommentWithUser;->getAuthor()Lcom/zendesk/sdk/model/request/User;

    move-result-object v4

    invoke-virtual {v4}, Lcom/zendesk/sdk/model/request/User;->getPhoto()Lcom/zendesk/sdk/model/request/Attachment;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 248
    iget-object v4, p0, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRow;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/zendesk/sdk/network/impl/ZendeskPicassoProvider;->getInstance(Landroid/content/Context;)Lcom/squareup/picasso/Picasso;

    move-result-object v4

    .line 249
    invoke-virtual {p1}, Lcom/zendesk/sdk/requests/CommentWithUser;->getAuthor()Lcom/zendesk/sdk/model/request/User;

    move-result-object v5

    invoke-virtual {v5}, Lcom/zendesk/sdk/model/request/User;->getPhoto()Lcom/zendesk/sdk/model/request/Attachment;

    move-result-object v5

    invoke-virtual {v5}, Lcom/zendesk/sdk/model/request/Attachment;->getContentUrl()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/squareup/picasso/Picasso;->load(Ljava/lang/String;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v4

    sget v5, Lcom/zendesk/sdk/R$drawable;->zd_user_default_avatar:I

    .line 250
    invoke-virtual {v4, v5}, Lcom/squareup/picasso/RequestCreator;->error(I)Lcom/squareup/picasso/RequestCreator;

    move-result-object v4

    sget v5, Lcom/zendesk/sdk/R$drawable;->zd_user_default_avatar:I

    .line 251
    invoke-virtual {v4, v5}, Lcom/squareup/picasso/RequestCreator;->placeholder(I)Lcom/squareup/picasso/RequestCreator;

    move-result-object v4

    sget-object v5, Lcom/zendesk/sdk/ui/ZendeskPicassoTransformationFactory;->INSTANCE:Lcom/zendesk/sdk/ui/ZendeskPicassoTransformationFactory;

    mul-int/lit8 v6, v1, 0x2

    .line 252
    invoke-virtual {v5, v6, v8}, Lcom/zendesk/sdk/ui/ZendeskPicassoTransformationFactory;->getRoundedTransformation(II)Lcom/squareup/picasso/Transformation;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/squareup/picasso/RequestCreator;->transform(Lcom/squareup/picasso/Transformation;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v4

    iget-object v5, p0, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRow;->mAvatarImageView:Landroid/widget/ImageView;

    .line 253
    invoke-virtual {v4, v5}, Lcom/squareup/picasso/RequestCreator;->into(Landroid/widget/ImageView;)V

    .line 266
    :goto_1
    new-instance v4, Landroid/os/Handler;

    invoke-direct {v4}, Landroid/os/Handler;-><init>()V

    new-instance v5, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRow$1;

    invoke-direct {v5, p0, p1}, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRow$1;-><init>(Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRow;Lcom/zendesk/sdk/requests/CommentWithUser;)V

    invoke-virtual {v4, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 275
    return-void

    .line 255
    :cond_2
    iget-object v4, p0, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRow;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/zendesk/sdk/network/impl/ZendeskPicassoProvider;->getInstance(Landroid/content/Context;)Lcom/squareup/picasso/Picasso;

    move-result-object v4

    sget v5, Lcom/zendesk/sdk/R$drawable;->zd_user_default_avatar:I

    .line 256
    invoke-virtual {v4, v5}, Lcom/squareup/picasso/Picasso;->load(I)Lcom/squareup/picasso/RequestCreator;

    move-result-object v4

    sget-object v5, Lcom/zendesk/sdk/ui/ZendeskPicassoTransformationFactory;->INSTANCE:Lcom/zendesk/sdk/ui/ZendeskPicassoTransformationFactory;

    mul-int/lit8 v6, v1, 0x2

    .line 257
    invoke-virtual {v5, v6, v8}, Lcom/zendesk/sdk/ui/ZendeskPicassoTransformationFactory;->getRoundedTransformation(II)Lcom/squareup/picasso/Transformation;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/squareup/picasso/RequestCreator;->transform(Lcom/squareup/picasso/Transformation;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v4

    iget-object v5, p0, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRow;->mAvatarImageView:Landroid/widget/ImageView;

    .line 258
    invoke-virtual {v4, v5}, Lcom/squareup/picasso/RequestCreator;->into(Landroid/widget/ImageView;)V

    goto :goto_1
.end method

.method public bridge synthetic bind(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 172
    check-cast p1, Lcom/zendesk/sdk/requests/CommentWithUser;

    invoke-virtual {p0, p1}, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRow;->bind(Lcom/zendesk/sdk/requests/CommentWithUser;)V

    return-void
.end method

.method public getView()Landroid/view/View;
    .locals 0

    .prologue
    .line 279
    return-object p0
.end method
