.class Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRowAttachmentHelper$2;
.super Ljava/lang/Object;
.source "RequestCommentsListAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRowAttachmentHelper;->attachPhotoToCommentRow(Landroid/view/ViewGroup;Lcom/zendesk/sdk/model/request/CommentResponse;Landroid/content/Context;Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$AttachmentOnClickListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRowAttachmentHelper;

.field final synthetic val$attachment:Lcom/zendesk/sdk/model/request/Attachment;

.field final synthetic val$attachmentOnClickListener:Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$AttachmentOnClickListener;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRowAttachmentHelper;Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$AttachmentOnClickListener;Lcom/zendesk/sdk/model/request/Attachment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRowAttachmentHelper;

    .prologue
    .line 373
    iput-object p1, p0, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRowAttachmentHelper$2;->this$0:Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRowAttachmentHelper;

    iput-object p2, p0, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRowAttachmentHelper$2;->val$attachmentOnClickListener:Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$AttachmentOnClickListener;

    iput-object p3, p0, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRowAttachmentHelper$2;->val$attachment:Lcom/zendesk/sdk/model/request/Attachment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 376
    iget-object v0, p0, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRowAttachmentHelper$2;->val$attachmentOnClickListener:Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$AttachmentOnClickListener;

    iget-object v1, p0, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRowAttachmentHelper$2;->val$attachment:Lcom/zendesk/sdk/model/request/Attachment;

    invoke-interface {v0, p1, v1}, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$AttachmentOnClickListener;->onAttachmentClicked(Landroid/view/View;Lcom/zendesk/sdk/model/request/Attachment;)V

    .line 377
    return-void
.end method
