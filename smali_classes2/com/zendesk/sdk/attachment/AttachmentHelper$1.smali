.class final Lcom/zendesk/sdk/attachment/AttachmentHelper$1;
.super Ljava/lang/Object;
.source "AttachmentHelper.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/zendesk/sdk/attachment/AttachmentHelper;->showAttachmentTryAgainDialog(Landroid/content/Context;Lcom/zendesk/belvedere/BelvedereResult;Lcom/zendesk/service/ErrorResponse;Lcom/zendesk/sdk/attachment/ImageUploadHelper;Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$attachmentContainerHost:Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;

.field final synthetic val$file:Lcom/zendesk/belvedere/BelvedereResult;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;Lcom/zendesk/belvedere/BelvedereResult;)V
    .locals 0

    .prologue
    .line 81
    iput-object p1, p0, Lcom/zendesk/sdk/attachment/AttachmentHelper$1;->val$attachmentContainerHost:Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;

    iput-object p2, p0, Lcom/zendesk/sdk/attachment/AttachmentHelper$1;->val$file:Lcom/zendesk/belvedere/BelvedereResult;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 84
    iget-object v0, p0, Lcom/zendesk/sdk/attachment/AttachmentHelper$1;->val$attachmentContainerHost:Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;

    iget-object v1, p0, Lcom/zendesk/sdk/attachment/AttachmentHelper$1;->val$file:Lcom/zendesk/belvedere/BelvedereResult;

    .line 85
    invoke-virtual {v1}, Lcom/zendesk/belvedere/BelvedereResult;->getFile()Ljava/io/File;

    move-result-object v1

    .line 84
    invoke-virtual {v0, v1}, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;->removeAttachment(Ljava/io/File;)V

    .line 87
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 88
    return-void
.end method
