.class public interface abstract Lcom/zendesk/sdk/network/BaseProvider;
.super Ljava/lang/Object;
.source "BaseProvider.java"


# virtual methods
.method public abstract configureSdk(Lcom/zendesk/service/ZendeskCallback;)V
    .param p1    # Lcom/zendesk/service/ZendeskCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Lcom/zendesk/sdk/model/SdkConfiguration;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract getAccessToken(Lcom/zendesk/sdk/model/settings/SafeMobileSettings;Lcom/zendesk/service/ZendeskCallback;)V
    .param p1    # Lcom/zendesk/sdk/model/settings/SafeMobileSettings;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/zendesk/service/ZendeskCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/zendesk/sdk/model/settings/SafeMobileSettings;",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Lcom/zendesk/sdk/model/access/AccessToken;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract getSdkSettings(Lcom/zendesk/service/ZendeskCallback;)V
    .param p1    # Lcom/zendesk/service/ZendeskCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Lcom/zendesk/sdk/model/settings/SafeMobileSettings;",
            ">;)V"
        }
    .end annotation
.end method
