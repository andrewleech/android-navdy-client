.class Lcom/zendesk/sdk/network/impl/ZendeskAccessService;
.super Ljava/lang/Object;
.source "ZendeskAccessService.java"


# static fields
.field private static final AUTH_EXTRACTOR:Lcom/zendesk/service/RetrofitZendeskCallbackAdapter$RequestExtractor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/zendesk/service/RetrofitZendeskCallbackAdapter$RequestExtractor",
            "<",
            "Lcom/zendesk/sdk/model/access/AuthenticationResponse;",
            "Lcom/zendesk/sdk/model/access/AccessToken;",
            ">;"
        }
    .end annotation
.end field

.field private static final LOG_TAG:Ljava/lang/String; = "ZendeskAccessService"


# instance fields
.field private final accessService:Lcom/zendesk/sdk/network/AccessService;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    new-instance v0, Lcom/zendesk/sdk/network/impl/ZendeskAccessService$1;

    invoke-direct {v0}, Lcom/zendesk/sdk/network/impl/ZendeskAccessService$1;-><init>()V

    sput-object v0, Lcom/zendesk/sdk/network/impl/ZendeskAccessService;->AUTH_EXTRACTOR:Lcom/zendesk/service/RetrofitZendeskCallbackAdapter$RequestExtractor;

    return-void
.end method

.method public constructor <init>(Lcom/zendesk/sdk/network/AccessService;)V
    .locals 0
    .param p1, "accessService"    # Lcom/zendesk/sdk/network/AccessService;

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/zendesk/sdk/network/impl/ZendeskAccessService;->accessService:Lcom/zendesk/sdk/network/AccessService;

    .line 22
    return-void
.end method


# virtual methods
.method getAuthTokenViaJWT(Lcom/zendesk/sdk/model/access/Identity;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 4
    .param p1, "user"    # Lcom/zendesk/sdk/model/access/Identity;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/zendesk/sdk/model/access/Identity;",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Lcom/zendesk/sdk/model/access/AccessToken;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 39
    .local p2, "callback":Lcom/zendesk/service/ZendeskCallback;, "Lcom/zendesk/service/ZendeskCallback<Lcom/zendesk/sdk/model/access/AccessToken;>;"
    new-instance v0, Lcom/zendesk/sdk/model/access/AuthenticationRequestWrapper;

    invoke-direct {v0}, Lcom/zendesk/sdk/model/access/AuthenticationRequestWrapper;-><init>()V

    .line 40
    .local v0, "wrapper":Lcom/zendesk/sdk/model/access/AuthenticationRequestWrapper;
    invoke-virtual {v0, p1}, Lcom/zendesk/sdk/model/access/AuthenticationRequestWrapper;->setUser(Lcom/zendesk/sdk/model/access/Identity;)V

    .line 41
    iget-object v1, p0, Lcom/zendesk/sdk/network/impl/ZendeskAccessService;->accessService:Lcom/zendesk/sdk/network/AccessService;

    invoke-interface {v1, v0}, Lcom/zendesk/sdk/network/AccessService;->getAuthToken(Lcom/zendesk/sdk/model/access/AuthenticationRequestWrapper;)Lretrofit2/Call;

    move-result-object v1

    new-instance v2, Lcom/zendesk/service/RetrofitZendeskCallbackAdapter;

    sget-object v3, Lcom/zendesk/sdk/network/impl/ZendeskAccessService;->AUTH_EXTRACTOR:Lcom/zendesk/service/RetrofitZendeskCallbackAdapter$RequestExtractor;

    invoke-direct {v2, p2, v3}, Lcom/zendesk/service/RetrofitZendeskCallbackAdapter;-><init>(Lcom/zendesk/service/ZendeskCallback;Lcom/zendesk/service/RetrofitZendeskCallbackAdapter$RequestExtractor;)V

    invoke-interface {v1, v2}, Lretrofit2/Call;->enqueue(Lretrofit2/Callback;)V

    .line 42
    return-void
.end method

.method getAuthTokenViaMobileSDK(Lcom/zendesk/sdk/model/access/Identity;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 4
    .param p1, "user"    # Lcom/zendesk/sdk/model/access/Identity;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/zendesk/sdk/model/access/Identity;",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Lcom/zendesk/sdk/model/access/AccessToken;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 51
    .local p2, "callback":Lcom/zendesk/service/ZendeskCallback;, "Lcom/zendesk/service/ZendeskCallback<Lcom/zendesk/sdk/model/access/AccessToken;>;"
    new-instance v0, Lcom/zendesk/sdk/model/access/AuthenticationRequestWrapper;

    invoke-direct {v0}, Lcom/zendesk/sdk/model/access/AuthenticationRequestWrapper;-><init>()V

    .line 52
    .local v0, "wrapper":Lcom/zendesk/sdk/model/access/AuthenticationRequestWrapper;
    invoke-virtual {v0, p1}, Lcom/zendesk/sdk/model/access/AuthenticationRequestWrapper;->setUser(Lcom/zendesk/sdk/model/access/Identity;)V

    .line 53
    iget-object v1, p0, Lcom/zendesk/sdk/network/impl/ZendeskAccessService;->accessService:Lcom/zendesk/sdk/network/AccessService;

    invoke-interface {v1, v0}, Lcom/zendesk/sdk/network/AccessService;->getAuthTokenForAnonymous(Lcom/zendesk/sdk/model/access/AuthenticationRequestWrapper;)Lretrofit2/Call;

    move-result-object v1

    new-instance v2, Lcom/zendesk/service/RetrofitZendeskCallbackAdapter;

    sget-object v3, Lcom/zendesk/sdk/network/impl/ZendeskAccessService;->AUTH_EXTRACTOR:Lcom/zendesk/service/RetrofitZendeskCallbackAdapter$RequestExtractor;

    invoke-direct {v2, p2, v3}, Lcom/zendesk/service/RetrofitZendeskCallbackAdapter;-><init>(Lcom/zendesk/service/ZendeskCallback;Lcom/zendesk/service/RetrofitZendeskCallbackAdapter$RequestExtractor;)V

    invoke-interface {v1, v2}, Lretrofit2/Call;->enqueue(Lretrofit2/Callback;)V

    .line 54
    return-void
.end method
