.class Lcom/zendesk/sdk/network/impl/ZendeskBaseProvider$1;
.super Lcom/zendesk/service/ZendeskCallback;
.source "ZendeskBaseProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/zendesk/sdk/network/impl/ZendeskBaseProvider;->configureSdk(Lcom/zendesk/service/ZendeskCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/zendesk/service/ZendeskCallback",
        "<",
        "Lcom/zendesk/sdk/model/settings/SafeMobileSettings;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/zendesk/sdk/network/impl/ZendeskBaseProvider;

.field final synthetic val$callback:Lcom/zendesk/service/ZendeskCallback;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/network/impl/ZendeskBaseProvider;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 0
    .param p1, "this$0"    # Lcom/zendesk/sdk/network/impl/ZendeskBaseProvider;

    .prologue
    .line 53
    iput-object p1, p0, Lcom/zendesk/sdk/network/impl/ZendeskBaseProvider$1;->this$0:Lcom/zendesk/sdk/network/impl/ZendeskBaseProvider;

    iput-object p2, p0, Lcom/zendesk/sdk/network/impl/ZendeskBaseProvider$1;->val$callback:Lcom/zendesk/service/ZendeskCallback;

    invoke-direct {p0}, Lcom/zendesk/service/ZendeskCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Lcom/zendesk/service/ErrorResponse;)V
    .locals 1
    .param p1, "errorResponse"    # Lcom/zendesk/service/ErrorResponse;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskBaseProvider$1;->val$callback:Lcom/zendesk/service/ZendeskCallback;

    if-eqz v0, :cond_0

    .line 77
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskBaseProvider$1;->val$callback:Lcom/zendesk/service/ZendeskCallback;

    invoke-virtual {v0, p1}, Lcom/zendesk/service/ZendeskCallback;->onError(Lcom/zendesk/service/ErrorResponse;)V

    .line 79
    :cond_0
    return-void
.end method

.method public onSuccess(Lcom/zendesk/sdk/model/settings/SafeMobileSettings;)V
    .locals 2
    .param p1, "mobileSettings"    # Lcom/zendesk/sdk/model/settings/SafeMobileSettings;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskBaseProvider$1;->this$0:Lcom/zendesk/sdk/network/impl/ZendeskBaseProvider;

    new-instance v1, Lcom/zendesk/sdk/network/impl/ZendeskBaseProvider$1$1;

    invoke-direct {v1, p0, p1}, Lcom/zendesk/sdk/network/impl/ZendeskBaseProvider$1$1;-><init>(Lcom/zendesk/sdk/network/impl/ZendeskBaseProvider$1;Lcom/zendesk/sdk/model/settings/SafeMobileSettings;)V

    invoke-virtual {v0, p1, v1}, Lcom/zendesk/sdk/network/impl/ZendeskBaseProvider;->getAccessToken(Lcom/zendesk/sdk/model/settings/SafeMobileSettings;Lcom/zendesk/service/ZendeskCallback;)V

    .line 72
    return-void
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 53
    check-cast p1, Lcom/zendesk/sdk/model/settings/SafeMobileSettings;

    invoke-virtual {p0, p1}, Lcom/zendesk/sdk/network/impl/ZendeskBaseProvider$1;->onSuccess(Lcom/zendesk/sdk/model/settings/SafeMobileSettings;)V

    return-void
.end method
