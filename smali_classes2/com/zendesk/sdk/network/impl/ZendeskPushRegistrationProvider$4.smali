.class Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider$4;
.super Lcom/zendesk/sdk/network/impl/PassThroughErrorZendeskCallback;
.source "ZendeskPushRegistrationProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider;->internalRegister(Ljava/lang/String;Lcom/zendesk/sdk/model/push/PushRegistrationRequest;Lcom/zendesk/service/ZendeskCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/zendesk/sdk/network/impl/PassThroughErrorZendeskCallback",
        "<",
        "Lcom/zendesk/sdk/model/push/PushRegistrationResponseWrapper;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider;

.field final synthetic val$callback:Lcom/zendesk/service/ZendeskCallback;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider;Lcom/zendesk/service/ZendeskCallback;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 0
    .param p1, "this$0"    # Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider;
    .param p2, "callback"    # Lcom/zendesk/service/ZendeskCallback;

    .prologue
    .line 96
    iput-object p1, p0, Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider$4;->this$0:Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider;

    iput-object p3, p0, Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider$4;->val$callback:Lcom/zendesk/service/ZendeskCallback;

    invoke-direct {p0, p2}, Lcom/zendesk/sdk/network/impl/PassThroughErrorZendeskCallback;-><init>(Lcom/zendesk/service/ZendeskCallback;)V

    return-void
.end method


# virtual methods
.method public onSuccess(Lcom/zendesk/sdk/model/push/PushRegistrationResponseWrapper;)V
    .locals 2
    .param p1, "result"    # Lcom/zendesk/sdk/model/push/PushRegistrationResponseWrapper;

    .prologue
    .line 99
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider$4;->val$callback:Lcom/zendesk/service/ZendeskCallback;

    if-eqz v0, :cond_0

    .line 100
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider$4;->val$callback:Lcom/zendesk/service/ZendeskCallback;

    invoke-virtual {p1}, Lcom/zendesk/sdk/model/push/PushRegistrationResponseWrapper;->getRegistrationResponse()Lcom/zendesk/sdk/model/push/PushRegistrationResponse;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/zendesk/service/ZendeskCallback;->onSuccess(Ljava/lang/Object;)V

    .line 102
    :cond_0
    return-void
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 96
    check-cast p1, Lcom/zendesk/sdk/model/push/PushRegistrationResponseWrapper;

    invoke-virtual {p0, p1}, Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider$4;->onSuccess(Lcom/zendesk/sdk/model/push/PushRegistrationResponseWrapper;)V

    return-void
.end method
