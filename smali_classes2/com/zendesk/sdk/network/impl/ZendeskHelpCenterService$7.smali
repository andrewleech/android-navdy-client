.class Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService$7;
.super Ljava/lang/Object;
.source "ZendeskHelpCenterService.java"

# interfaces
.implements Lcom/zendesk/service/RetrofitZendeskCallbackAdapter$RequestExtractor;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService;->getAttachments(Ljava/lang/String;Ljava/util/Locale;Ljava/lang/Long;Lcom/zendesk/sdk/model/helpcenter/AttachmentType;Lcom/zendesk/service/ZendeskCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/zendesk/service/RetrofitZendeskCallbackAdapter$RequestExtractor",
        "<",
        "Lcom/zendesk/sdk/model/helpcenter/AttachmentResponse;",
        "Ljava/util/List",
        "<",
        "Lcom/zendesk/sdk/model/helpcenter/Attachment;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService;

    .prologue
    .line 290
    iput-object p1, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService$7;->this$0:Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic extract(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 290
    check-cast p1, Lcom/zendesk/sdk/model/helpcenter/AttachmentResponse;

    invoke-virtual {p0, p1}, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService$7;->extract(Lcom/zendesk/sdk/model/helpcenter/AttachmentResponse;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public extract(Lcom/zendesk/sdk/model/helpcenter/AttachmentResponse;)Ljava/util/List;
    .locals 1
    .param p1, "data"    # Lcom/zendesk/sdk/model/helpcenter/AttachmentResponse;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/zendesk/sdk/model/helpcenter/AttachmentResponse;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/model/helpcenter/Attachment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 293
    invoke-virtual {p1}, Lcom/zendesk/sdk/model/helpcenter/AttachmentResponse;->getArticleAttachments()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
