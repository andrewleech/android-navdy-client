.class Lcom/zendesk/sdk/network/impl/ProviderInjector;
.super Ljava/lang/Object;
.source "ProviderInjector.java"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static injectAccessProvider(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/network/AccessProvider;
    .locals 3
    .param p0, "applicationScope"    # Lcom/zendesk/sdk/network/impl/ApplicationScope;

    .prologue
    .line 22
    new-instance v0, Lcom/zendesk/sdk/network/impl/ZendeskAccessProvider;

    .line 23
    invoke-static {p0}, Lcom/zendesk/sdk/storage/StorageInjector;->injectCachedIdentityStorage(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/storage/IdentityStorage;

    move-result-object v1

    .line 24
    invoke-static {p0}, Lcom/zendesk/sdk/network/impl/ServiceInjector;->injectZendeskAccessService(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/network/impl/ZendeskAccessService;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/zendesk/sdk/network/impl/ZendeskAccessProvider;-><init>(Lcom/zendesk/sdk/storage/IdentityStorage;Lcom/zendesk/sdk/network/impl/ZendeskAccessService;)V

    return-object v0
.end method

.method static injectHelpCenterProvider(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/network/HelpCenterProvider;
    .locals 4
    .param p0, "applicationScope"    # Lcom/zendesk/sdk/network/impl/ApplicationScope;

    .prologue
    .line 48
    new-instance v0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;

    .line 49
    invoke-static {p0}, Lcom/zendesk/sdk/network/impl/ProviderInjector;->injectZendeskBaseProvider(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/network/BaseProvider;

    move-result-object v1

    .line 50
    invoke-static {p0}, Lcom/zendesk/sdk/network/impl/ServiceInjector;->injectZendeskHelpCenterService(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService;

    move-result-object v2

    .line 51
    invoke-static {p0}, Lcom/zendesk/sdk/storage/StorageInjector;->injectCachedHelpCenterSessionCache(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/storage/HelpCenterSessionCache;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;-><init>(Lcom/zendesk/sdk/network/BaseProvider;Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService;Lcom/zendesk/sdk/storage/HelpCenterSessionCache;)V

    return-object v0
.end method

.method public static injectNetworkInfoProvider(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/network/NetworkInfoProvider;
    .locals 2
    .param p0, "applicationScope"    # Lcom/zendesk/sdk/network/impl/ApplicationScope;

    .prologue
    .line 93
    new-instance v0, Lcom/zendesk/sdk/network/impl/ZendeskNetworkInfoProvider;

    invoke-virtual {p0}, Lcom/zendesk/sdk/network/impl/ApplicationScope;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/zendesk/sdk/network/impl/ZendeskNetworkInfoProvider;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public static injectProviderStore(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/network/impl/ProviderStore;
    .locals 9
    .param p0, "applicationScope"    # Lcom/zendesk/sdk/network/impl/ApplicationScope;

    .prologue
    .line 97
    new-instance v0, Lcom/zendesk/sdk/network/impl/ZendeskProviderStore;

    .line 98
    invoke-static {p0}, Lcom/zendesk/sdk/network/impl/ProviderInjector;->injectUserProvider(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/network/UserProvider;

    move-result-object v1

    .line 99
    invoke-static {p0}, Lcom/zendesk/sdk/network/impl/ProviderInjector;->injectHelpCenterProvider(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/network/HelpCenterProvider;

    move-result-object v2

    .line 100
    invoke-static {p0}, Lcom/zendesk/sdk/network/impl/ProviderInjector;->injectPushRegistrationProvider(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/network/PushRegistrationProvider;

    move-result-object v3

    .line 101
    invoke-static {p0}, Lcom/zendesk/sdk/network/impl/ProviderInjector;->injectRequestProvider(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/network/RequestProvider;

    move-result-object v4

    .line 102
    invoke-static {p0}, Lcom/zendesk/sdk/network/impl/ProviderInjector;->injectUploadProvider(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/network/UploadProvider;

    move-result-object v5

    .line 103
    invoke-static {p0}, Lcom/zendesk/sdk/network/impl/ProviderInjector;->injectSdkSettingsProvider(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/network/SdkSettingsProvider;

    move-result-object v6

    .line 104
    invoke-static {p0}, Lcom/zendesk/sdk/network/impl/ProviderInjector;->injectNetworkInfoProvider(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/network/NetworkInfoProvider;

    move-result-object v7

    .line 105
    invoke-static {p0}, Lcom/zendesk/sdk/network/impl/ProviderInjector;->injectZendeskSettingsHelper(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/network/SettingsHelper;

    move-result-object v8

    invoke-direct/range {v0 .. v8}, Lcom/zendesk/sdk/network/impl/ZendeskProviderStore;-><init>(Lcom/zendesk/sdk/network/UserProvider;Lcom/zendesk/sdk/network/HelpCenterProvider;Lcom/zendesk/sdk/network/PushRegistrationProvider;Lcom/zendesk/sdk/network/RequestProvider;Lcom/zendesk/sdk/network/UploadProvider;Lcom/zendesk/sdk/network/SdkSettingsProvider;Lcom/zendesk/sdk/network/NetworkInfoProvider;Lcom/zendesk/sdk/network/SettingsHelper;)V

    return-object v0
.end method

.method static injectPushRegistrationProvider(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/network/PushRegistrationProvider;
    .locals 4
    .param p0, "applicationScope"    # Lcom/zendesk/sdk/network/impl/ApplicationScope;

    .prologue
    .line 56
    new-instance v0, Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider;

    .line 57
    invoke-static {p0}, Lcom/zendesk/sdk/network/impl/ProviderInjector;->injectZendeskBaseProvider(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/network/BaseProvider;

    move-result-object v1

    .line 58
    invoke-static {p0}, Lcom/zendesk/sdk/network/impl/ServiceInjector;->injectZendeskPushRegistrationService(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationService;

    move-result-object v2

    .line 59
    invoke-static {p0}, Lcom/zendesk/sdk/storage/StorageInjector;->injectCachedIdentityStorage(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/storage/IdentityStorage;

    move-result-object v3

    invoke-interface {v3}, Lcom/zendesk/sdk/storage/IdentityStorage;->getIdentity()Lcom/zendesk/sdk/model/access/Identity;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider;-><init>(Lcom/zendesk/sdk/network/BaseProvider;Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationService;Lcom/zendesk/sdk/model/access/Identity;)V

    return-object v0
.end method

.method static injectRequestProvider(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/network/RequestProvider;
    .locals 5
    .param p0, "applicationScope"    # Lcom/zendesk/sdk/network/impl/ApplicationScope;

    .prologue
    .line 78
    new-instance v0, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider;

    .line 79
    invoke-static {p0}, Lcom/zendesk/sdk/network/impl/ProviderInjector;->injectZendeskBaseProvider(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/network/BaseProvider;

    move-result-object v1

    .line 80
    invoke-static {p0}, Lcom/zendesk/sdk/network/impl/ServiceInjector;->injectZendeskRequestService(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/network/impl/ZendeskRequestService;

    move-result-object v2

    .line 81
    invoke-static {p0}, Lcom/zendesk/sdk/storage/StorageInjector;->injectCachedIdentityStorage(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/storage/IdentityStorage;

    move-result-object v3

    invoke-interface {v3}, Lcom/zendesk/sdk/storage/IdentityStorage;->getIdentity()Lcom/zendesk/sdk/model/access/Identity;

    move-result-object v3

    .line 82
    invoke-static {p0}, Lcom/zendesk/sdk/storage/StorageInjector;->injectCachedRequestStorage(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/storage/RequestStorage;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider;-><init>(Lcom/zendesk/sdk/network/BaseProvider;Lcom/zendesk/sdk/network/impl/ZendeskRequestService;Lcom/zendesk/sdk/model/access/Identity;Lcom/zendesk/sdk/storage/RequestStorage;)V

    return-object v0
.end method

.method static injectSdkSettingsProvider(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/network/SdkSettingsProvider;
    .locals 6
    .param p0, "applicationScope"    # Lcom/zendesk/sdk/network/impl/ApplicationScope;

    .prologue
    .line 38
    new-instance v0, Lcom/zendesk/sdk/network/impl/ZendeskSdkSettingsProvider;

    .line 39
    invoke-static {p0}, Lcom/zendesk/sdk/network/impl/ServiceInjector;->injectZendeskSdkSettingsService(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/network/impl/ZendeskSdkSettingsService;

    move-result-object v1

    .line 40
    invoke-static {p0}, Lcom/zendesk/sdk/util/BaseInjector;->injectLocale(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Ljava/util/Locale;

    move-result-object v2

    .line 41
    invoke-static {p0}, Lcom/zendesk/sdk/storage/StorageInjector;->injectCachedSdkSettingsStorage(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/storage/SdkSettingsStorage;

    move-result-object v3

    .line 42
    invoke-static {p0}, Lcom/zendesk/sdk/util/BaseInjector;->injectAppId(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/zendesk/sdk/network/impl/HelpCenterLocaleConverter;

    invoke-direct {v5}, Lcom/zendesk/sdk/network/impl/HelpCenterLocaleConverter;-><init>()V

    invoke-direct/range {v0 .. v5}, Lcom/zendesk/sdk/network/impl/ZendeskSdkSettingsProvider;-><init>(Lcom/zendesk/sdk/network/impl/ZendeskSdkSettingsService;Ljava/util/Locale;Lcom/zendesk/sdk/storage/SdkSettingsStorage;Ljava/lang/String;Lcom/zendesk/sdk/network/impl/HelpCenterLocaleConverter;)V

    return-object v0
.end method

.method static injectStubProviderStore(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/network/impl/ProviderStore;
    .locals 1
    .param p0, "applicationScope"    # Lcom/zendesk/sdk/network/impl/ApplicationScope;

    .prologue
    .line 110
    new-instance v0, Lcom/zendesk/sdk/network/impl/StubProviderStore;

    invoke-direct {v0}, Lcom/zendesk/sdk/network/impl/StubProviderStore;-><init>()V

    return-object v0
.end method

.method static injectUploadProvider(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/network/UploadProvider;
    .locals 3
    .param p0, "applicationScope"    # Lcom/zendesk/sdk/network/impl/ApplicationScope;

    .prologue
    .line 64
    new-instance v0, Lcom/zendesk/sdk/network/impl/ZendeskUploadProvider;

    .line 65
    invoke-static {p0}, Lcom/zendesk/sdk/network/impl/ProviderInjector;->injectZendeskBaseProvider(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/network/BaseProvider;

    move-result-object v1

    .line 66
    invoke-static {p0}, Lcom/zendesk/sdk/network/impl/ServiceInjector;->injectZendeskUploadService(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/network/impl/ZendeskUploadService;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/zendesk/sdk/network/impl/ZendeskUploadProvider;-><init>(Lcom/zendesk/sdk/network/BaseProvider;Lcom/zendesk/sdk/network/impl/ZendeskUploadService;)V

    return-object v0
.end method

.method static injectUserProvider(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/network/UserProvider;
    .locals 3
    .param p0, "applicationScope"    # Lcom/zendesk/sdk/network/impl/ApplicationScope;

    .prologue
    .line 71
    new-instance v0, Lcom/zendesk/sdk/network/impl/ZendeskUserProvider;

    .line 72
    invoke-static {p0}, Lcom/zendesk/sdk/network/impl/ProviderInjector;->injectZendeskBaseProvider(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/network/BaseProvider;

    move-result-object v1

    .line 73
    invoke-static {p0}, Lcom/zendesk/sdk/network/impl/ServiceInjector;->injectZendeskUserService(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/network/impl/ZendeskUserService;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/zendesk/sdk/network/impl/ZendeskUserProvider;-><init>(Lcom/zendesk/sdk/network/BaseProvider;Lcom/zendesk/sdk/network/impl/ZendeskUserService;)V

    return-object v0
.end method

.method static injectZendeskBaseProvider(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/network/BaseProvider;
    .locals 5
    .param p0, "applicationScope"    # Lcom/zendesk/sdk/network/impl/ApplicationScope;

    .prologue
    .line 29
    new-instance v0, Lcom/zendesk/sdk/network/impl/ZendeskBaseProvider;

    .line 30
    invoke-static {p0}, Lcom/zendesk/sdk/network/impl/ProviderInjector;->injectAccessProvider(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/network/AccessProvider;

    move-result-object v1

    .line 31
    invoke-static {p0}, Lcom/zendesk/sdk/storage/StorageInjector;->injectCachedSdkSettingsStorage(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/storage/SdkSettingsStorage;

    move-result-object v2

    .line 32
    invoke-static {p0}, Lcom/zendesk/sdk/storage/StorageInjector;->injectCachedIdentityStorage(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/storage/IdentityStorage;

    move-result-object v3

    .line 33
    invoke-static {p0}, Lcom/zendesk/sdk/network/impl/ProviderInjector;->injectSdkSettingsProvider(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/network/SdkSettingsProvider;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/zendesk/sdk/network/impl/ZendeskBaseProvider;-><init>(Lcom/zendesk/sdk/network/AccessProvider;Lcom/zendesk/sdk/storage/SdkSettingsStorage;Lcom/zendesk/sdk/storage/IdentityStorage;Lcom/zendesk/sdk/network/SdkSettingsProvider;)V

    return-object v0
.end method

.method static injectZendeskSettingsHelper(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/network/SettingsHelper;
    .locals 2
    .param p0, "applicationScope"    # Lcom/zendesk/sdk/network/impl/ApplicationScope;

    .prologue
    .line 87
    new-instance v0, Lcom/zendesk/sdk/network/impl/ZendeskSettingsHelper;

    .line 88
    invoke-static {p0}, Lcom/zendesk/sdk/network/impl/ProviderInjector;->injectZendeskBaseProvider(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/network/BaseProvider;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/zendesk/sdk/network/impl/ZendeskSettingsHelper;-><init>(Lcom/zendesk/sdk/network/BaseProvider;)V

    return-object v0
.end method
