.class Lcom/zendesk/sdk/network/impl/StubProviderFactory;
.super Ljava/lang/Object;
.source "StubProviderFactory.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/zendesk/sdk/network/impl/StubProviderFactory$ZendeskCallbackInvocationHandler;
    }
.end annotation


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "StubProviderFactory"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static getStubProvider(Ljava/lang/Class;)Ljava/lang/Object;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TE;>;)TE;"
        }
    .end annotation

    .prologue
    .local p0, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TE;>;"
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 37
    const/4 v1, 0x0

    .line 40
    .local v1, "provider":Ljava/lang/Object;, "TE;"
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    aput-object p0, v4, v5

    invoke-static {v3, v4}, Ljava/lang/reflect/Proxy;->getProxyClass(Ljava/lang/ClassLoader;[Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v2

    .line 42
    .local v2, "proxyClass":Ljava/lang/Class;
    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, Ljava/lang/reflect/InvocationHandler;

    aput-object v5, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    new-instance v6, Lcom/zendesk/sdk/network/impl/StubProviderFactory$ZendeskCallbackInvocationHandler;

    invoke-direct {v6, p0}, Lcom/zendesk/sdk/network/impl/StubProviderFactory$ZendeskCallbackInvocationHandler;-><init>(Ljava/lang/Class;)V

    aput-object v6, v4, v5

    .line 43
    invoke-virtual {v3, v4}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 49
    .end local v1    # "provider":Ljava/lang/Object;, "TE;"
    .end local v2    # "proxyClass":Ljava/lang/Class;
    :goto_0
    return-object v1

    .line 45
    .restart local v1    # "provider":Ljava/lang/Object;, "TE;"
    :catch_0
    move-exception v0

    .line 46
    .local v0, "e":Ljava/lang/Exception;
    const-string v3, "StubProviderFactory"

    const-string v4, "Unable to create stub provider. Error: %s"

    new-array v5, v8, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v3, v4, v5}, Lcom/zendesk/logger/Logger;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method
