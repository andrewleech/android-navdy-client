.class Lcom/zendesk/sdk/network/impl/ZendeskUserService;
.super Ljava/lang/Object;
.source "ZendeskUserService.java"


# instance fields
.field private final userService:Lcom/zendesk/sdk/network/UserService;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/network/UserService;)V
    .locals 0
    .param p1, "userService"    # Lcom/zendesk/sdk/network/UserService;

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/zendesk/sdk/network/impl/ZendeskUserService;->userService:Lcom/zendesk/sdk/network/UserService;

    .line 22
    return-void
.end method


# virtual methods
.method addTags(Ljava/lang/String;Lcom/zendesk/sdk/model/request/UserTagRequest;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 2
    .param p1, "authorizationHeader"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "userTagRequest"    # Lcom/zendesk/sdk/model/request/UserTagRequest;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/zendesk/sdk/model/request/UserTagRequest;",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Lcom/zendesk/sdk/model/request/UserResponse;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 32
    .local p3, "callback":Lcom/zendesk/service/ZendeskCallback;, "Lcom/zendesk/service/ZendeskCallback<Lcom/zendesk/sdk/model/request/UserResponse;>;"
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskUserService;->userService:Lcom/zendesk/sdk/network/UserService;

    invoke-interface {v0, p1, p2}, Lcom/zendesk/sdk/network/UserService;->addTags(Ljava/lang/String;Lcom/zendesk/sdk/model/request/UserTagRequest;)Lretrofit2/Call;

    move-result-object v0

    new-instance v1, Lcom/zendesk/service/RetrofitZendeskCallbackAdapter;

    invoke-direct {v1, p3}, Lcom/zendesk/service/RetrofitZendeskCallbackAdapter;-><init>(Lcom/zendesk/service/ZendeskCallback;)V

    invoke-interface {v0, v1}, Lretrofit2/Call;->enqueue(Lretrofit2/Callback;)V

    .line 33
    return-void
.end method

.method deleteTags(Ljava/lang/String;Ljava/lang/String;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 2
    .param p1, "authorizationHeader"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "tags"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Lcom/zendesk/sdk/model/request/UserResponse;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 43
    .local p3, "callback":Lcom/zendesk/service/ZendeskCallback;, "Lcom/zendesk/service/ZendeskCallback<Lcom/zendesk/sdk/model/request/UserResponse;>;"
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskUserService;->userService:Lcom/zendesk/sdk/network/UserService;

    invoke-interface {v0, p1, p2}, Lcom/zendesk/sdk/network/UserService;->deleteTags(Ljava/lang/String;Ljava/lang/String;)Lretrofit2/Call;

    move-result-object v0

    new-instance v1, Lcom/zendesk/service/RetrofitZendeskCallbackAdapter;

    invoke-direct {v1, p3}, Lcom/zendesk/service/RetrofitZendeskCallbackAdapter;-><init>(Lcom/zendesk/service/ZendeskCallback;)V

    invoke-interface {v0, v1}, Lretrofit2/Call;->enqueue(Lretrofit2/Callback;)V

    .line 44
    return-void
.end method

.method getUser(Ljava/lang/String;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 2
    .param p1, "authorizationHeader"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Lcom/zendesk/sdk/model/request/UserResponse;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 74
    .local p2, "callback":Lcom/zendesk/service/ZendeskCallback;, "Lcom/zendesk/service/ZendeskCallback<Lcom/zendesk/sdk/model/request/UserResponse;>;"
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskUserService;->userService:Lcom/zendesk/sdk/network/UserService;

    invoke-interface {v0, p1}, Lcom/zendesk/sdk/network/UserService;->getUser(Ljava/lang/String;)Lretrofit2/Call;

    move-result-object v0

    new-instance v1, Lcom/zendesk/service/RetrofitZendeskCallbackAdapter;

    invoke-direct {v1, p2}, Lcom/zendesk/service/RetrofitZendeskCallbackAdapter;-><init>(Lcom/zendesk/service/ZendeskCallback;)V

    invoke-interface {v0, v1}, Lretrofit2/Call;->enqueue(Lretrofit2/Callback;)V

    .line 75
    return-void
.end method

.method getUserFields(Ljava/lang/String;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 2
    .param p1, "authorizationHeader"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Lcom/zendesk/sdk/model/request/UserFieldResponse;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 53
    .local p2, "callback":Lcom/zendesk/service/ZendeskCallback;, "Lcom/zendesk/service/ZendeskCallback<Lcom/zendesk/sdk/model/request/UserFieldResponse;>;"
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskUserService;->userService:Lcom/zendesk/sdk/network/UserService;

    invoke-interface {v0, p1}, Lcom/zendesk/sdk/network/UserService;->getUserFields(Ljava/lang/String;)Lretrofit2/Call;

    move-result-object v0

    new-instance v1, Lcom/zendesk/service/RetrofitZendeskCallbackAdapter;

    invoke-direct {v1, p2}, Lcom/zendesk/service/RetrofitZendeskCallbackAdapter;-><init>(Lcom/zendesk/service/ZendeskCallback;)V

    invoke-interface {v0, v1}, Lretrofit2/Call;->enqueue(Lretrofit2/Callback;)V

    .line 54
    return-void
.end method

.method setUserFields(Ljava/lang/String;Lcom/zendesk/sdk/model/request/UserFieldRequest;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 2
    .param p1, "authorizationHeader"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "userFieldRequest"    # Lcom/zendesk/sdk/model/request/UserFieldRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/zendesk/sdk/model/request/UserFieldRequest;",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Lcom/zendesk/sdk/model/request/UserResponse;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 64
    .local p3, "callback":Lcom/zendesk/service/ZendeskCallback;, "Lcom/zendesk/service/ZendeskCallback<Lcom/zendesk/sdk/model/request/UserResponse;>;"
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskUserService;->userService:Lcom/zendesk/sdk/network/UserService;

    invoke-interface {v0, p1, p2}, Lcom/zendesk/sdk/network/UserService;->setUserFields(Ljava/lang/String;Lcom/zendesk/sdk/model/request/UserFieldRequest;)Lretrofit2/Call;

    move-result-object v0

    new-instance v1, Lcom/zendesk/service/RetrofitZendeskCallbackAdapter;

    invoke-direct {v1, p3}, Lcom/zendesk/service/RetrofitZendeskCallbackAdapter;-><init>(Lcom/zendesk/service/ZendeskCallback;)V

    invoke-interface {v0, v1}, Lretrofit2/Call;->enqueue(Lretrofit2/Callback;)V

    .line 65
    return-void
.end method
