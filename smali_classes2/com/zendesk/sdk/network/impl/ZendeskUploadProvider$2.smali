.class Lcom/zendesk/sdk/network/impl/ZendeskUploadProvider$2;
.super Lcom/zendesk/sdk/network/impl/PassThroughErrorZendeskCallback;
.source "ZendeskUploadProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/zendesk/sdk/network/impl/ZendeskUploadProvider;->deleteAttachment(Ljava/lang/String;Lcom/zendesk/service/ZendeskCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/zendesk/sdk/network/impl/PassThroughErrorZendeskCallback",
        "<",
        "Lcom/zendesk/sdk/model/SdkConfiguration;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/zendesk/sdk/network/impl/ZendeskUploadProvider;

.field final synthetic val$callback:Lcom/zendesk/service/ZendeskCallback;

.field final synthetic val$token:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/network/impl/ZendeskUploadProvider;Lcom/zendesk/service/ZendeskCallback;Ljava/lang/String;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 0
    .param p1, "this$0"    # Lcom/zendesk/sdk/network/impl/ZendeskUploadProvider;
    .param p2, "callback"    # Lcom/zendesk/service/ZendeskCallback;

    .prologue
    .line 70
    iput-object p1, p0, Lcom/zendesk/sdk/network/impl/ZendeskUploadProvider$2;->this$0:Lcom/zendesk/sdk/network/impl/ZendeskUploadProvider;

    iput-object p3, p0, Lcom/zendesk/sdk/network/impl/ZendeskUploadProvider$2;->val$token:Ljava/lang/String;

    iput-object p4, p0, Lcom/zendesk/sdk/network/impl/ZendeskUploadProvider$2;->val$callback:Lcom/zendesk/service/ZendeskCallback;

    invoke-direct {p0, p2}, Lcom/zendesk/sdk/network/impl/PassThroughErrorZendeskCallback;-><init>(Lcom/zendesk/service/ZendeskCallback;)V

    return-void
.end method


# virtual methods
.method public onSuccess(Lcom/zendesk/sdk/model/SdkConfiguration;)V
    .locals 5
    .param p1, "sdkConfiguration"    # Lcom/zendesk/sdk/model/SdkConfiguration;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskUploadProvider$2;->this$0:Lcom/zendesk/sdk/network/impl/ZendeskUploadProvider;

    invoke-static {v0}, Lcom/zendesk/sdk/network/impl/ZendeskUploadProvider;->access$000(Lcom/zendesk/sdk/network/impl/ZendeskUploadProvider;)Lcom/zendesk/sdk/network/impl/ZendeskUploadService;

    move-result-object v0

    invoke-virtual {p1}, Lcom/zendesk/sdk/model/SdkConfiguration;->getBearerAuthorizationHeader()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/zendesk/sdk/network/impl/ZendeskUploadProvider$2;->val$token:Ljava/lang/String;

    new-instance v3, Lcom/zendesk/sdk/network/impl/ZendeskUploadProvider$2$1;

    iget-object v4, p0, Lcom/zendesk/sdk/network/impl/ZendeskUploadProvider$2;->val$callback:Lcom/zendesk/service/ZendeskCallback;

    invoke-direct {v3, p0, v4}, Lcom/zendesk/sdk/network/impl/ZendeskUploadProvider$2$1;-><init>(Lcom/zendesk/sdk/network/impl/ZendeskUploadProvider$2;Lcom/zendesk/service/ZendeskCallback;)V

    invoke-virtual {v0, v1, v2, v3}, Lcom/zendesk/sdk/network/impl/ZendeskUploadService;->deleteAttachment(Ljava/lang/String;Ljava/lang/String;Lcom/zendesk/service/ZendeskCallback;)V

    .line 82
    return-void
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 70
    check-cast p1, Lcom/zendesk/sdk/model/SdkConfiguration;

    invoke-virtual {p0, p1}, Lcom/zendesk/sdk/network/impl/ZendeskUploadProvider$2;->onSuccess(Lcom/zendesk/sdk/model/SdkConfiguration;)V

    return-void
.end method
