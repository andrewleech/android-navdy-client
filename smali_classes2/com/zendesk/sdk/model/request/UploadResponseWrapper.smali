.class public Lcom/zendesk/sdk/model/request/UploadResponseWrapper;
.super Ljava/lang/Object;
.source "UploadResponseWrapper.java"


# instance fields
.field private upload:Lcom/zendesk/sdk/model/request/UploadResponse;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getUpload()Lcom/zendesk/sdk/model/request/UploadResponse;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 25
    iget-object v0, p0, Lcom/zendesk/sdk/model/request/UploadResponseWrapper;->upload:Lcom/zendesk/sdk/model/request/UploadResponse;

    return-object v0
.end method
