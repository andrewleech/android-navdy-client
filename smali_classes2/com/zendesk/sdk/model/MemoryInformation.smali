.class public Lcom/zendesk/sdk/model/MemoryInformation;
.super Ljava/lang/Object;
.source "MemoryInformation.java"


# static fields
.field public static final BYTES_MULTIPLIER:D = 1024.0

.field public static final EXPECTED_TOKEN_COUNT:I = 0x3

.field private static final LOG_TAG:Ljava/lang/String;


# instance fields
.field private final mActivityManager:Landroid/app/ActivityManager;

.field private final mContext:Landroid/content/Context;

.field private mTotalMemory:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-class v0, Lcom/zendesk/sdk/model/MemoryInformation;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/zendesk/sdk/model/MemoryInformation;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    const-string v0, "activity"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    iput-object v0, p0, Lcom/zendesk/sdk/model/MemoryInformation;->mActivityManager:Landroid/app/ActivityManager;

    .line 43
    iput-object p1, p0, Lcom/zendesk/sdk/model/MemoryInformation;->mContext:Landroid/content/Context;

    .line 45
    invoke-virtual {p0}, Lcom/zendesk/sdk/model/MemoryInformation;->getTotalMemory()I

    move-result v0

    iput v0, p0, Lcom/zendesk/sdk/model/MemoryInformation;->mTotalMemory:I

    .line 46
    return-void
.end method

.method private bytesToMegabytes(J)I
    .locals 5
    .param p1, "bytes"    # J

    .prologue
    const-wide/high16 v2, 0x4090000000000000L    # 1024.0

    .line 200
    long-to-double v0, p1

    div-double/2addr v0, v2

    div-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method


# virtual methods
.method public formatMemoryUsage()Ljava/lang/String;
    .locals 6

    .prologue
    .line 63
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    iget-object v2, p0, Lcom/zendesk/sdk/model/MemoryInformation;->mContext:Landroid/content/Context;

    sget v3, Lcom/zendesk/sdk/R$string;->rate_my_app_dialog_feedback_device_memory:I

    .line 65
    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 66
    invoke-virtual {p0}, Lcom/zendesk/sdk/model/MemoryInformation;->getUsedMemory()I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget v5, p0, Lcom/zendesk/sdk/model/MemoryInformation;->mTotalMemory:I

    .line 67
    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 63
    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 69
    .local v0, "formatted":Ljava/lang/String;
    return-object v0
.end method

.method public getTotalMemory()I
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 107
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-lt v1, v2, :cond_0

    .line 108
    sget-object v1, Lcom/zendesk/sdk/model/MemoryInformation;->LOG_TAG:Ljava/lang/String;

    const-string v2, "Using getTotalMemoryApi() to determine memory"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 109
    invoke-virtual {p0}, Lcom/zendesk/sdk/model/MemoryInformation;->getTotalMemoryApi()I

    move-result v0

    .line 115
    .local v0, "totalMemoryMegabytes":I
    :goto_0
    return v0

    .line 111
    .end local v0    # "totalMemoryMegabytes":I
    :cond_0
    sget-object v1, Lcom/zendesk/sdk/model/MemoryInformation;->LOG_TAG:Ljava/lang/String;

    const-string v2, "Using getTotalMemoryCompat() to determine memory"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 112
    invoke-virtual {p0}, Lcom/zendesk/sdk/model/MemoryInformation;->getTotalMemoryCompat()I

    move-result v0

    .restart local v0    # "totalMemoryMegabytes":I
    goto :goto_0
.end method

.method public getTotalMemoryApi()I
    .locals 5
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 175
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-ge v2, v3, :cond_0

    .line 176
    sget-object v2, Lcom/zendesk/sdk/model/MemoryInformation;->LOG_TAG:Ljava/lang/String;

    const-string v3, "Sorry, this call is not available on your API level, please use getTotalMemory() instead"

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lcom/zendesk/logger/Logger;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 182
    :goto_0
    return v1

    .line 179
    :cond_0
    new-instance v0, Landroid/app/ActivityManager$MemoryInfo;

    invoke-direct {v0}, Landroid/app/ActivityManager$MemoryInfo;-><init>()V

    .line 180
    .local v0, "memoryInfo":Landroid/app/ActivityManager$MemoryInfo;
    iget-object v1, p0, Lcom/zendesk/sdk/model/MemoryInformation;->mActivityManager:Landroid/app/ActivityManager;

    invoke-virtual {v1, v0}, Landroid/app/ActivityManager;->getMemoryInfo(Landroid/app/ActivityManager$MemoryInfo;)V

    .line 182
    iget-wide v2, v0, Landroid/app/ActivityManager$MemoryInfo;->totalMem:J

    invoke-direct {p0, v2, v3}, Lcom/zendesk/sdk/model/MemoryInformation;->bytesToMegabytes(J)I

    move-result v1

    goto :goto_0
.end method

.method public getTotalMemoryCompat()I
    .locals 12

    .prologue
    const/4 v11, 0x0

    .line 129
    const-wide/16 v6, 0x0

    .line 131
    .local v6, "totalMemoryBytes":J
    const/4 v1, 0x0

    .line 132
    .local v1, "memoryInfoReader":Ljava/io/BufferedReader;
    const-string v4, ""

    .line 135
    .local v4, "totalMemory":Ljava/lang/String;
    :try_start_0
    new-instance v2, Ljava/io/BufferedReader;

    new-instance v5, Ljava/io/FileReader;

    const-string v8, "/proc/meminfo"

    invoke-direct {v5, v8}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, v5}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 136
    .end local v1    # "memoryInfoReader":Ljava/io/BufferedReader;
    .local v2, "memoryInfoReader":Ljava/io/BufferedReader;
    :try_start_1
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v4

    .line 140
    if-eqz v2, :cond_3

    .line 142
    :try_start_2
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v1, v2

    .line 149
    .end local v2    # "memoryInfoReader":Ljava/io/BufferedReader;
    .restart local v1    # "memoryInfoReader":Ljava/io/BufferedReader;
    :cond_0
    :goto_0
    new-instance v3, Ljava/util/StringTokenizer;

    invoke-direct {v3, v4}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;)V

    .line 155
    .local v3, "st":Ljava/util/StringTokenizer;
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->countTokens()I

    move-result v5

    const/4 v8, 0x3

    if-ne v5, v8, :cond_1

    .line 156
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    .line 157
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    const-wide/16 v10, 0x400

    mul-long v6, v8, v10

    .line 160
    :cond_1
    invoke-direct {p0, v6, v7}, Lcom/zendesk/sdk/model/MemoryInformation;->bytesToMegabytes(J)I

    move-result v5

    return v5

    .line 143
    .end local v1    # "memoryInfoReader":Ljava/io/BufferedReader;
    .end local v3    # "st":Ljava/util/StringTokenizer;
    .restart local v2    # "memoryInfoReader":Ljava/io/BufferedReader;
    :catch_0
    move-exception v0

    .line 144
    .local v0, "e":Ljava/io/IOException;
    sget-object v5, Lcom/zendesk/sdk/model/MemoryInformation;->LOG_TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Failed to close /proc/meminfo file stream: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    new-array v9, v11, [Ljava/lang/Object;

    invoke-static {v5, v8, v0, v9}, Lcom/zendesk/logger/Logger;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;[Ljava/lang/Object;)V

    move-object v1, v2

    .line 145
    .end local v2    # "memoryInfoReader":Ljava/io/BufferedReader;
    .restart local v1    # "memoryInfoReader":Ljava/io/BufferedReader;
    goto :goto_0

    .line 137
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 138
    .restart local v0    # "e":Ljava/io/IOException;
    :goto_1
    :try_start_3
    sget-object v5, Lcom/zendesk/sdk/model/MemoryInformation;->LOG_TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Failed to determine total memory from /proc/meminfo: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/Object;

    invoke-static {v5, v8, v0, v9}, Lcom/zendesk/logger/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;[Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 140
    if-eqz v1, :cond_0

    .line 142
    :try_start_4
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 143
    :catch_2
    move-exception v0

    .line 144
    sget-object v5, Lcom/zendesk/sdk/model/MemoryInformation;->LOG_TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Failed to close /proc/meminfo file stream: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    new-array v9, v11, [Ljava/lang/Object;

    invoke-static {v5, v8, v0, v9}, Lcom/zendesk/logger/Logger;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 140
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v5

    :goto_2
    if-eqz v1, :cond_2

    .line 142
    :try_start_5
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 145
    :cond_2
    :goto_3
    throw v5

    .line 143
    :catch_3
    move-exception v0

    .line 144
    .restart local v0    # "e":Ljava/io/IOException;
    sget-object v8, Lcom/zendesk/sdk/model/MemoryInformation;->LOG_TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Failed to close /proc/meminfo file stream: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    new-array v10, v11, [Ljava/lang/Object;

    invoke-static {v8, v9, v0, v10}, Lcom/zendesk/logger/Logger;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;[Ljava/lang/Object;)V

    goto :goto_3

    .line 140
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "memoryInfoReader":Ljava/io/BufferedReader;
    .restart local v2    # "memoryInfoReader":Ljava/io/BufferedReader;
    :catchall_1
    move-exception v5

    move-object v1, v2

    .end local v2    # "memoryInfoReader":Ljava/io/BufferedReader;
    .restart local v1    # "memoryInfoReader":Ljava/io/BufferedReader;
    goto :goto_2

    .line 137
    .end local v1    # "memoryInfoReader":Ljava/io/BufferedReader;
    .restart local v2    # "memoryInfoReader":Ljava/io/BufferedReader;
    :catch_4
    move-exception v0

    move-object v1, v2

    .end local v2    # "memoryInfoReader":Ljava/io/BufferedReader;
    .restart local v1    # "memoryInfoReader":Ljava/io/BufferedReader;
    goto :goto_1

    .end local v1    # "memoryInfoReader":Ljava/io/BufferedReader;
    .restart local v2    # "memoryInfoReader":Ljava/io/BufferedReader;
    :cond_3
    move-object v1, v2

    .end local v2    # "memoryInfoReader":Ljava/io/BufferedReader;
    .restart local v1    # "memoryInfoReader":Ljava/io/BufferedReader;
    goto/16 :goto_0
.end method

.method public getUsedMemory()I
    .locals 4

    .prologue
    .line 78
    new-instance v1, Landroid/app/ActivityManager$MemoryInfo;

    invoke-direct {v1}, Landroid/app/ActivityManager$MemoryInfo;-><init>()V

    .line 80
    .local v1, "memoryInfo":Landroid/app/ActivityManager$MemoryInfo;
    iget-object v2, p0, Lcom/zendesk/sdk/model/MemoryInformation;->mActivityManager:Landroid/app/ActivityManager;

    invoke-virtual {v2, v1}, Landroid/app/ActivityManager;->getMemoryInfo(Landroid/app/ActivityManager$MemoryInfo;)V

    .line 82
    iget-wide v2, v1, Landroid/app/ActivityManager$MemoryInfo;->availMem:J

    invoke-direct {p0, v2, v3}, Lcom/zendesk/sdk/model/MemoryInformation;->bytesToMegabytes(J)I

    move-result v0

    .line 84
    .local v0, "availableMemoryMegabytes":I
    iget v2, p0, Lcom/zendesk/sdk/model/MemoryInformation;->mTotalMemory:I

    sub-int/2addr v2, v0

    return v2
.end method

.method public isLowMemory()Z
    .locals 2

    .prologue
    .line 192
    new-instance v0, Landroid/app/ActivityManager$MemoryInfo;

    invoke-direct {v0}, Landroid/app/ActivityManager$MemoryInfo;-><init>()V

    .line 194
    .local v0, "memoryInfo":Landroid/app/ActivityManager$MemoryInfo;
    iget-object v1, p0, Lcom/zendesk/sdk/model/MemoryInformation;->mActivityManager:Landroid/app/ActivityManager;

    invoke-virtual {v1, v0}, Landroid/app/ActivityManager;->getMemoryInfo(Landroid/app/ActivityManager$MemoryInfo;)V

    .line 196
    iget-boolean v1, v0, Landroid/app/ActivityManager$MemoryInfo;->lowMemory:Z

    return v1
.end method
