.class public final enum Lcom/zendesk/sdk/model/helpcenter/SortOrder;
.super Ljava/lang/Enum;
.source "SortOrder.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/zendesk/sdk/model/helpcenter/SortOrder;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/zendesk/sdk/model/helpcenter/SortOrder;

.field public static final enum ASCENDING:Lcom/zendesk/sdk/model/helpcenter/SortOrder;

.field public static final enum DESCENDING:Lcom/zendesk/sdk/model/helpcenter/SortOrder;


# instance fields
.field private final apiValue:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 10
    new-instance v0, Lcom/zendesk/sdk/model/helpcenter/SortOrder;

    const-string v1, "ASCENDING"

    const-string v2, "asc"

    invoke-direct {v0, v1, v3, v2}, Lcom/zendesk/sdk/model/helpcenter/SortOrder;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/zendesk/sdk/model/helpcenter/SortOrder;->ASCENDING:Lcom/zendesk/sdk/model/helpcenter/SortOrder;

    new-instance v0, Lcom/zendesk/sdk/model/helpcenter/SortOrder;

    const-string v1, "DESCENDING"

    const-string v2, "desc"

    invoke-direct {v0, v1, v4, v2}, Lcom/zendesk/sdk/model/helpcenter/SortOrder;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/zendesk/sdk/model/helpcenter/SortOrder;->DESCENDING:Lcom/zendesk/sdk/model/helpcenter/SortOrder;

    .line 8
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/zendesk/sdk/model/helpcenter/SortOrder;

    sget-object v1, Lcom/zendesk/sdk/model/helpcenter/SortOrder;->ASCENDING:Lcom/zendesk/sdk/model/helpcenter/SortOrder;

    aput-object v1, v0, v3

    sget-object v1, Lcom/zendesk/sdk/model/helpcenter/SortOrder;->DESCENDING:Lcom/zendesk/sdk/model/helpcenter/SortOrder;

    aput-object v1, v0, v4

    sput-object v0, Lcom/zendesk/sdk/model/helpcenter/SortOrder;->$VALUES:[Lcom/zendesk/sdk/model/helpcenter/SortOrder;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "apiValue"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 14
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 15
    iput-object p3, p0, Lcom/zendesk/sdk/model/helpcenter/SortOrder;->apiValue:Ljava/lang/String;

    .line 16
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/zendesk/sdk/model/helpcenter/SortOrder;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 8
    const-class v0, Lcom/zendesk/sdk/model/helpcenter/SortOrder;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/zendesk/sdk/model/helpcenter/SortOrder;

    return-object v0
.end method

.method public static values()[Lcom/zendesk/sdk/model/helpcenter/SortOrder;
    .locals 1

    .prologue
    .line 8
    sget-object v0, Lcom/zendesk/sdk/model/helpcenter/SortOrder;->$VALUES:[Lcom/zendesk/sdk/model/helpcenter/SortOrder;

    invoke-virtual {v0}, [Lcom/zendesk/sdk/model/helpcenter/SortOrder;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/zendesk/sdk/model/helpcenter/SortOrder;

    return-object v0
.end method


# virtual methods
.method public getApiValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/SortOrder;->apiValue:Ljava/lang/String;

    return-object v0
.end method
