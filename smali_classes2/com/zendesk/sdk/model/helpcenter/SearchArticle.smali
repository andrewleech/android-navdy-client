.class public Lcom/zendesk/sdk/model/helpcenter/SearchArticle;
.super Ljava/lang/Object;
.source "SearchArticle.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private final mArticle:Lcom/zendesk/sdk/model/helpcenter/Article;

.field private final mCategory:Lcom/zendesk/sdk/model/helpcenter/Category;

.field private final mSection:Lcom/zendesk/sdk/model/helpcenter/Section;


# direct methods
.method public constructor <init>(Lcom/zendesk/sdk/model/helpcenter/Article;Lcom/zendesk/sdk/model/helpcenter/Section;Lcom/zendesk/sdk/model/helpcenter/Category;)V
    .locals 0
    .param p1, "article"    # Lcom/zendesk/sdk/model/helpcenter/Article;
    .param p2, "section"    # Lcom/zendesk/sdk/model/helpcenter/Section;
    .param p3, "category"    # Lcom/zendesk/sdk/model/helpcenter/Category;

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/zendesk/sdk/model/helpcenter/SearchArticle;->mArticle:Lcom/zendesk/sdk/model/helpcenter/Article;

    .line 32
    iput-object p2, p0, Lcom/zendesk/sdk/model/helpcenter/SearchArticle;->mSection:Lcom/zendesk/sdk/model/helpcenter/Section;

    .line 33
    iput-object p3, p0, Lcom/zendesk/sdk/model/helpcenter/SearchArticle;->mCategory:Lcom/zendesk/sdk/model/helpcenter/Category;

    .line 34
    return-void
.end method


# virtual methods
.method public getArticle()Lcom/zendesk/sdk/model/helpcenter/Article;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/SearchArticle;->mArticle:Lcom/zendesk/sdk/model/helpcenter/Article;

    return-object v0
.end method

.method public getCategory()Lcom/zendesk/sdk/model/helpcenter/Category;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/SearchArticle;->mCategory:Lcom/zendesk/sdk/model/helpcenter/Category;

    return-object v0
.end method

.method public getSection()Lcom/zendesk/sdk/model/helpcenter/Section;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/SearchArticle;->mSection:Lcom/zendesk/sdk/model/helpcenter/Section;

    return-object v0
.end method
