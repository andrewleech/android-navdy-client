.class public Lcom/zendesk/sdk/model/helpcenter/ListArticleQuery;
.super Ljava/lang/Object;
.source "ListArticleQuery.java"


# instance fields
.field private mInclude:Ljava/lang/String;

.field private mLabelNames:Ljava/lang/String;

.field private mLocale:Ljava/util/Locale;

.field private mPage:Ljava/lang/Integer;

.field private mResultsPerPage:Ljava/lang/Integer;

.field private mSortBy:Lcom/zendesk/sdk/model/helpcenter/SortBy;

.field private mSortOrder:Lcom/zendesk/sdk/model/helpcenter/SortOrder;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getInclude()Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/ListArticleQuery;->mInclude:Ljava/lang/String;

    return-object v0
.end method

.method public getLabelNames()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/ListArticleQuery;->mLabelNames:Ljava/lang/String;

    return-object v0
.end method

.method public getLocale()Ljava/util/Locale;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/ListArticleQuery;->mLocale:Ljava/util/Locale;

    return-object v0
.end method

.method public getPage()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/ListArticleQuery;->mPage:Ljava/lang/Integer;

    return-object v0
.end method

.method public getResultsPerPage()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/ListArticleQuery;->mResultsPerPage:Ljava/lang/Integer;

    return-object v0
.end method

.method public getSortBy()Lcom/zendesk/sdk/model/helpcenter/SortBy;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/ListArticleQuery;->mSortBy:Lcom/zendesk/sdk/model/helpcenter/SortBy;

    return-object v0
.end method

.method public getSortOrder()Lcom/zendesk/sdk/model/helpcenter/SortOrder;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/ListArticleQuery;->mSortOrder:Lcom/zendesk/sdk/model/helpcenter/SortOrder;

    return-object v0
.end method

.method public setInclude(Ljava/lang/String;)V
    .locals 0
    .param p1, "include"    # Ljava/lang/String;

    .prologue
    .line 126
    iput-object p1, p0, Lcom/zendesk/sdk/model/helpcenter/ListArticleQuery;->mInclude:Ljava/lang/String;

    .line 127
    return-void
.end method

.method public setLabelNames(Ljava/lang/String;)V
    .locals 0
    .param p1, "labelNames"    # Ljava/lang/String;

    .prologue
    .line 108
    iput-object p1, p0, Lcom/zendesk/sdk/model/helpcenter/ListArticleQuery;->mLabelNames:Ljava/lang/String;

    .line 109
    return-void
.end method

.method public setLocale(Ljava/util/Locale;)V
    .locals 0
    .param p1, "locale"    # Ljava/util/Locale;

    .prologue
    .line 117
    iput-object p1, p0, Lcom/zendesk/sdk/model/helpcenter/ListArticleQuery;->mLocale:Ljava/util/Locale;

    .line 118
    return-void
.end method

.method public setPage(Ljava/lang/Integer;)V
    .locals 0
    .param p1, "page"    # Ljava/lang/Integer;

    .prologue
    .line 153
    iput-object p1, p0, Lcom/zendesk/sdk/model/helpcenter/ListArticleQuery;->mPage:Ljava/lang/Integer;

    .line 154
    return-void
.end method

.method public setResultsPerPage(Ljava/lang/Integer;)V
    .locals 0
    .param p1, "resultsPerPage"    # Ljava/lang/Integer;

    .prologue
    .line 162
    iput-object p1, p0, Lcom/zendesk/sdk/model/helpcenter/ListArticleQuery;->mResultsPerPage:Ljava/lang/Integer;

    .line 163
    return-void
.end method

.method public setSortBy(Lcom/zendesk/sdk/model/helpcenter/SortBy;)V
    .locals 0
    .param p1, "sortBy"    # Lcom/zendesk/sdk/model/helpcenter/SortBy;

    .prologue
    .line 135
    iput-object p1, p0, Lcom/zendesk/sdk/model/helpcenter/ListArticleQuery;->mSortBy:Lcom/zendesk/sdk/model/helpcenter/SortBy;

    .line 136
    return-void
.end method

.method public setSortOrder(Lcom/zendesk/sdk/model/helpcenter/SortOrder;)V
    .locals 0
    .param p1, "sortOrder"    # Lcom/zendesk/sdk/model/helpcenter/SortOrder;

    .prologue
    .line 144
    iput-object p1, p0, Lcom/zendesk/sdk/model/helpcenter/ListArticleQuery;->mSortOrder:Lcom/zendesk/sdk/model/helpcenter/SortOrder;

    .line 145
    return-void
.end method
