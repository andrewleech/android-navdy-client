.class Lcom/zendesk/sdk/support/SupportActivity$2;
.super Ljava/lang/Object;
.source "SupportActivity.java"

# interfaces
.implements Landroid/support/v4/app/FragmentManager$OnBackStackChangedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/zendesk/sdk/support/SupportActivity;->addOnBackStackChangedListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/zendesk/sdk/support/SupportActivity;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/support/SupportActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/zendesk/sdk/support/SupportActivity;

    .prologue
    .line 120
    iput-object p1, p0, Lcom/zendesk/sdk/support/SupportActivity$2;->this$0:Lcom/zendesk/sdk/support/SupportActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onBackStackChanged()V
    .locals 2

    .prologue
    .line 123
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportActivity$2;->this$0:Lcom/zendesk/sdk/support/SupportActivity;

    invoke-static {v0}, Lcom/zendesk/sdk/support/SupportActivity;->access$000(Lcom/zendesk/sdk/support/SupportActivity;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->isHidden()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 124
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportActivity$2;->this$0:Lcom/zendesk/sdk/support/SupportActivity;

    invoke-virtual {v0}, Lcom/zendesk/sdk/support/SupportActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    .line 125
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    iget-object v1, p0, Lcom/zendesk/sdk/support/SupportActivity$2;->this$0:Lcom/zendesk/sdk/support/SupportActivity;

    .line 126
    invoke-static {v1}, Lcom/zendesk/sdk/support/SupportActivity;->access$000(Lcom/zendesk/sdk/support/SupportActivity;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentTransaction;->show(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 127
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 129
    :cond_0
    return-void
.end method
