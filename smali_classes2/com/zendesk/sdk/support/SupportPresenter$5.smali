.class Lcom/zendesk/sdk/support/SupportPresenter$5;
.super Lcom/zendesk/service/ZendeskCallback;
.source "SupportPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/zendesk/sdk/support/SupportPresenter;->determineFirstScreen(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/zendesk/service/ZendeskCallback",
        "<",
        "Lcom/zendesk/sdk/model/settings/SafeMobileSettings;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/zendesk/sdk/support/SupportPresenter;

.field final synthetic val$savedInstanceState:Landroid/os/Bundle;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/support/SupportPresenter;Landroid/os/Bundle;)V
    .locals 0
    .param p1, "this$0"    # Lcom/zendesk/sdk/support/SupportPresenter;

    .prologue
    .line 302
    iput-object p1, p0, Lcom/zendesk/sdk/support/SupportPresenter$5;->this$0:Lcom/zendesk/sdk/support/SupportPresenter;

    iput-object p2, p0, Lcom/zendesk/sdk/support/SupportPresenter$5;->val$savedInstanceState:Landroid/os/Bundle;

    invoke-direct {p0}, Lcom/zendesk/service/ZendeskCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Lcom/zendesk/service/ErrorResponse;)V
    .locals 3
    .param p1, "errorResponse"    # Lcom/zendesk/service/ErrorResponse;

    .prologue
    .line 384
    const-string v0, "SupportPresenter"

    const-string v1, "Failed to get mobile settings. Cannot determine start screen."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/zendesk/logger/Logger;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 385
    const-string v0, "SupportPresenter"

    invoke-static {v0, p1}, Lcom/zendesk/logger/Logger;->e(Ljava/lang/String;Lcom/zendesk/service/ErrorResponse;)V

    .line 386
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportPresenter$5;->this$0:Lcom/zendesk/sdk/support/SupportPresenter;

    invoke-static {v0}, Lcom/zendesk/sdk/support/SupportPresenter;->access$000(Lcom/zendesk/sdk/support/SupportPresenter;)Lcom/zendesk/sdk/support/SupportMvp$View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 387
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportPresenter$5;->this$0:Lcom/zendesk/sdk/support/SupportPresenter;

    invoke-static {v0}, Lcom/zendesk/sdk/support/SupportPresenter;->access$000(Lcom/zendesk/sdk/support/SupportPresenter;)Lcom/zendesk/sdk/support/SupportMvp$View;

    move-result-object v0

    invoke-interface {v0}, Lcom/zendesk/sdk/support/SupportMvp$View;->hideLoadingState()V

    .line 388
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportPresenter$5;->this$0:Lcom/zendesk/sdk/support/SupportPresenter;

    invoke-static {v0}, Lcom/zendesk/sdk/support/SupportPresenter;->access$000(Lcom/zendesk/sdk/support/SupportPresenter;)Lcom/zendesk/sdk/support/SupportMvp$View;

    move-result-object v0

    invoke-interface {v0}, Lcom/zendesk/sdk/support/SupportMvp$View;->exitActivity()V

    .line 398
    :goto_0
    return-void

    .line 390
    :cond_0
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportPresenter$5;->this$0:Lcom/zendesk/sdk/support/SupportPresenter;

    invoke-static {v0}, Lcom/zendesk/sdk/support/SupportPresenter;->access$200(Lcom/zendesk/sdk/support/SupportPresenter;)Ljava/util/Set;

    move-result-object v0

    new-instance v1, Lcom/zendesk/sdk/support/SupportPresenter$5$6;

    invoke-direct {v1, p0}, Lcom/zendesk/sdk/support/SupportPresenter$5$6;-><init>(Lcom/zendesk/sdk/support/SupportPresenter$5;)V

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public onSuccess(Lcom/zendesk/sdk/model/settings/SafeMobileSettings;)V
    .locals 4
    .param p1, "safeMobileSettings"    # Lcom/zendesk/sdk/model/settings/SafeMobileSettings;

    .prologue
    const/4 v3, 0x0

    .line 306
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportPresenter$5;->this$0:Lcom/zendesk/sdk/support/SupportPresenter;

    invoke-static {v0}, Lcom/zendesk/sdk/support/SupportPresenter;->access$000(Lcom/zendesk/sdk/support/SupportPresenter;)Lcom/zendesk/sdk/support/SupportMvp$View;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 307
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportPresenter$5;->this$0:Lcom/zendesk/sdk/support/SupportPresenter;

    invoke-static {v0}, Lcom/zendesk/sdk/support/SupportPresenter;->access$000(Lcom/zendesk/sdk/support/SupportPresenter;)Lcom/zendesk/sdk/support/SupportMvp$View;

    move-result-object v0

    invoke-interface {v0}, Lcom/zendesk/sdk/support/SupportMvp$View;->hideLoadingState()V

    .line 316
    :goto_0
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportPresenter$5;->this$0:Lcom/zendesk/sdk/support/SupportPresenter;

    invoke-static {v0, p1}, Lcom/zendesk/sdk/support/SupportPresenter;->access$402(Lcom/zendesk/sdk/support/SupportPresenter;Lcom/zendesk/sdk/model/settings/SafeMobileSettings;)Lcom/zendesk/sdk/model/settings/SafeMobileSettings;

    .line 318
    invoke-virtual {p1}, Lcom/zendesk/sdk/model/settings/SafeMobileSettings;->isHelpCenterEnabled()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 320
    const-string v0, "SupportPresenter"

    const-string v1, "Help center is enabled.  starting with Help Center"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 322
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportPresenter$5;->this$0:Lcom/zendesk/sdk/support/SupportPresenter;

    invoke-static {v0}, Lcom/zendesk/sdk/support/SupportPresenter;->access$000(Lcom/zendesk/sdk/support/SupportPresenter;)Lcom/zendesk/sdk/support/SupportMvp$View;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 323
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportPresenter$5;->this$0:Lcom/zendesk/sdk/support/SupportPresenter;

    invoke-static {v0}, Lcom/zendesk/sdk/support/SupportPresenter;->access$000(Lcom/zendesk/sdk/support/SupportPresenter;)Lcom/zendesk/sdk/support/SupportMvp$View;

    move-result-object v0

    invoke-interface {v0}, Lcom/zendesk/sdk/support/SupportMvp$View;->startWithHelp()V

    .line 333
    :goto_1
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportPresenter$5;->val$savedInstanceState:Landroid/os/Bundle;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportPresenter$5;->val$savedInstanceState:Landroid/os/Bundle;

    const-string v1, "fab_visibility"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 334
    const-string v0, "SupportPresenter"

    const-string v1, "Saved instance states that we should show the contact FAB"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 335
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportPresenter$5;->this$0:Lcom/zendesk/sdk/support/SupportPresenter;

    invoke-static {v0}, Lcom/zendesk/sdk/support/SupportPresenter;->access$000(Lcom/zendesk/sdk/support/SupportPresenter;)Lcom/zendesk/sdk/support/SupportMvp$View;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 336
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportPresenter$5;->this$0:Lcom/zendesk/sdk/support/SupportPresenter;

    invoke-static {v0}, Lcom/zendesk/sdk/support/SupportPresenter;->access$000(Lcom/zendesk/sdk/support/SupportPresenter;)Lcom/zendesk/sdk/support/SupportMvp$View;

    move-result-object v0

    invoke-interface {v0}, Lcom/zendesk/sdk/support/SupportMvp$View;->showContactUsButton()V

    .line 380
    :cond_0
    :goto_2
    return-void

    .line 309
    :cond_1
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportPresenter$5;->this$0:Lcom/zendesk/sdk/support/SupportPresenter;

    invoke-static {v0}, Lcom/zendesk/sdk/support/SupportPresenter;->access$200(Lcom/zendesk/sdk/support/SupportPresenter;)Ljava/util/Set;

    move-result-object v0

    new-instance v1, Lcom/zendesk/sdk/support/SupportPresenter$5$1;

    invoke-direct {v1, p0}, Lcom/zendesk/sdk/support/SupportPresenter$5$1;-><init>(Lcom/zendesk/sdk/support/SupportPresenter$5;)V

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 325
    :cond_2
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportPresenter$5;->this$0:Lcom/zendesk/sdk/support/SupportPresenter;

    invoke-static {v0}, Lcom/zendesk/sdk/support/SupportPresenter;->access$200(Lcom/zendesk/sdk/support/SupportPresenter;)Ljava/util/Set;

    move-result-object v0

    new-instance v1, Lcom/zendesk/sdk/support/SupportPresenter$5$2;

    invoke-direct {v1, p0}, Lcom/zendesk/sdk/support/SupportPresenter$5$2;-><init>(Lcom/zendesk/sdk/support/SupportPresenter$5;)V

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 338
    :cond_3
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportPresenter$5;->this$0:Lcom/zendesk/sdk/support/SupportPresenter;

    invoke-static {v0}, Lcom/zendesk/sdk/support/SupportPresenter;->access$200(Lcom/zendesk/sdk/support/SupportPresenter;)Ljava/util/Set;

    move-result-object v0

    new-instance v1, Lcom/zendesk/sdk/support/SupportPresenter$5$3;

    invoke-direct {v1, p0}, Lcom/zendesk/sdk/support/SupportPresenter$5$3;-><init>(Lcom/zendesk/sdk/support/SupportPresenter$5;)V

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 348
    :cond_4
    const-string v0, "SupportPresenter"

    const-string v1, "Help center is disabled"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 350
    invoke-virtual {p1}, Lcom/zendesk/sdk/model/settings/SafeMobileSettings;->isConversationsEnabled()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 351
    const-string v0, "SupportPresenter"

    const-string v1, "Starting with conversations"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 352
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportPresenter$5;->this$0:Lcom/zendesk/sdk/support/SupportPresenter;

    invoke-static {v0}, Lcom/zendesk/sdk/support/SupportPresenter;->access$000(Lcom/zendesk/sdk/support/SupportPresenter;)Lcom/zendesk/sdk/support/SupportMvp$View;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 353
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportPresenter$5;->this$0:Lcom/zendesk/sdk/support/SupportPresenter;

    invoke-static {v0}, Lcom/zendesk/sdk/support/SupportPresenter;->access$000(Lcom/zendesk/sdk/support/SupportPresenter;)Lcom/zendesk/sdk/support/SupportMvp$View;

    move-result-object v0

    invoke-interface {v0}, Lcom/zendesk/sdk/support/SupportMvp$View;->showRequestList()V

    .line 354
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportPresenter$5;->this$0:Lcom/zendesk/sdk/support/SupportPresenter;

    invoke-static {v0}, Lcom/zendesk/sdk/support/SupportPresenter;->access$000(Lcom/zendesk/sdk/support/SupportPresenter;)Lcom/zendesk/sdk/support/SupportMvp$View;

    move-result-object v0

    invoke-interface {v0}, Lcom/zendesk/sdk/support/SupportMvp$View;->exitActivity()V

    goto :goto_2

    .line 356
    :cond_5
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportPresenter$5;->this$0:Lcom/zendesk/sdk/support/SupportPresenter;

    invoke-static {v0}, Lcom/zendesk/sdk/support/SupportPresenter;->access$200(Lcom/zendesk/sdk/support/SupportPresenter;)Ljava/util/Set;

    move-result-object v0

    new-instance v1, Lcom/zendesk/sdk/support/SupportPresenter$5$4;

    invoke-direct {v1, p0}, Lcom/zendesk/sdk/support/SupportPresenter$5$4;-><init>(Lcom/zendesk/sdk/support/SupportPresenter$5;)V

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 365
    :cond_6
    const-string v0, "SupportPresenter"

    const-string v1, "Starting with contact"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 366
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportPresenter$5;->this$0:Lcom/zendesk/sdk/support/SupportPresenter;

    invoke-static {v0}, Lcom/zendesk/sdk/support/SupportPresenter;->access$000(Lcom/zendesk/sdk/support/SupportPresenter;)Lcom/zendesk/sdk/support/SupportMvp$View;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 367
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportPresenter$5;->this$0:Lcom/zendesk/sdk/support/SupportPresenter;

    invoke-static {v0}, Lcom/zendesk/sdk/support/SupportPresenter;->access$000(Lcom/zendesk/sdk/support/SupportPresenter;)Lcom/zendesk/sdk/support/SupportMvp$View;

    move-result-object v0

    invoke-interface {v0}, Lcom/zendesk/sdk/support/SupportMvp$View;->showContactZendesk()V

    .line 368
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportPresenter$5;->this$0:Lcom/zendesk/sdk/support/SupportPresenter;

    invoke-static {v0}, Lcom/zendesk/sdk/support/SupportPresenter;->access$000(Lcom/zendesk/sdk/support/SupportPresenter;)Lcom/zendesk/sdk/support/SupportMvp$View;

    move-result-object v0

    invoke-interface {v0}, Lcom/zendesk/sdk/support/SupportMvp$View;->exitActivity()V

    goto/16 :goto_2

    .line 370
    :cond_7
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportPresenter$5;->this$0:Lcom/zendesk/sdk/support/SupportPresenter;

    invoke-static {v0}, Lcom/zendesk/sdk/support/SupportPresenter;->access$200(Lcom/zendesk/sdk/support/SupportPresenter;)Ljava/util/Set;

    move-result-object v0

    new-instance v1, Lcom/zendesk/sdk/support/SupportPresenter$5$5;

    invoke-direct {v1, p0}, Lcom/zendesk/sdk/support/SupportPresenter$5$5;-><init>(Lcom/zendesk/sdk/support/SupportPresenter$5;)V

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 302
    check-cast p1, Lcom/zendesk/sdk/model/settings/SafeMobileSettings;

    invoke-virtual {p0, p1}, Lcom/zendesk/sdk/support/SupportPresenter$5;->onSuccess(Lcom/zendesk/sdk/model/settings/SafeMobileSettings;)V

    return-void
.end method
