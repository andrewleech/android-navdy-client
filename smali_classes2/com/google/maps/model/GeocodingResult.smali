.class public Lcom/google/maps/model/GeocodingResult;
.super Ljava/lang/Object;
.source "GeocodingResult.java"


# instance fields
.field public addressComponents:[Lcom/google/maps/model/AddressComponent;

.field public formattedAddress:Ljava/lang/String;

.field public geometry:Lcom/google/maps/model/Geometry;

.field public partialMatch:Z

.field public placeId:Ljava/lang/String;

.field public postcodeLocalities:[Ljava/lang/String;

.field public types:[Lcom/google/maps/model/AddressType;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
