.class public final enum Lcom/google/maps/model/AddressComponentType;
.super Ljava/lang/Enum;
.source "AddressComponentType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/maps/model/AddressComponentType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/maps/model/AddressComponentType;

.field public static final enum ADMINISTRATIVE_AREA_LEVEL_1:Lcom/google/maps/model/AddressComponentType;

.field public static final enum ADMINISTRATIVE_AREA_LEVEL_2:Lcom/google/maps/model/AddressComponentType;

.field public static final enum ADMINISTRATIVE_AREA_LEVEL_3:Lcom/google/maps/model/AddressComponentType;

.field public static final enum ADMINISTRATIVE_AREA_LEVEL_4:Lcom/google/maps/model/AddressComponentType;

.field public static final enum ADMINISTRATIVE_AREA_LEVEL_5:Lcom/google/maps/model/AddressComponentType;

.field public static final enum AIRPORT:Lcom/google/maps/model/AddressComponentType;

.field public static final enum BUS_STATION:Lcom/google/maps/model/AddressComponentType;

.field public static final enum COLLOQUIAL_AREA:Lcom/google/maps/model/AddressComponentType;

.field public static final enum COUNTRY:Lcom/google/maps/model/AddressComponentType;

.field public static final enum ESTABLISHMENT:Lcom/google/maps/model/AddressComponentType;

.field public static final enum FLOOR:Lcom/google/maps/model/AddressComponentType;

.field public static final enum INTERSECTION:Lcom/google/maps/model/AddressComponentType;

.field public static final enum LOCALITY:Lcom/google/maps/model/AddressComponentType;

.field public static final enum NATURAL_FEATURE:Lcom/google/maps/model/AddressComponentType;

.field public static final enum NEIGHBORHOOD:Lcom/google/maps/model/AddressComponentType;

.field public static final enum PARK:Lcom/google/maps/model/AddressComponentType;

.field public static final enum PARKING:Lcom/google/maps/model/AddressComponentType;

.field public static final enum POINT_OF_INTEREST:Lcom/google/maps/model/AddressComponentType;

.field public static final enum POLITICAL:Lcom/google/maps/model/AddressComponentType;

.field public static final enum POSTAL_CODE:Lcom/google/maps/model/AddressComponentType;

.field public static final enum POSTAL_CODE_SUFFIX:Lcom/google/maps/model/AddressComponentType;

.field public static final enum POSTAL_TOWN:Lcom/google/maps/model/AddressComponentType;

.field public static final enum POST_BOX:Lcom/google/maps/model/AddressComponentType;

.field public static final enum PREMISE:Lcom/google/maps/model/AddressComponentType;

.field public static final enum ROOM:Lcom/google/maps/model/AddressComponentType;

.field public static final enum ROUTE:Lcom/google/maps/model/AddressComponentType;

.field public static final enum STREET_ADDRESS:Lcom/google/maps/model/AddressComponentType;

.field public static final enum STREET_NUMBER:Lcom/google/maps/model/AddressComponentType;

.field public static final enum SUBLOCALITY:Lcom/google/maps/model/AddressComponentType;

.field public static final enum SUBLOCALITY_LEVEL_1:Lcom/google/maps/model/AddressComponentType;

.field public static final enum SUBLOCALITY_LEVEL_2:Lcom/google/maps/model/AddressComponentType;

.field public static final enum SUBLOCALITY_LEVEL_3:Lcom/google/maps/model/AddressComponentType;

.field public static final enum SUBLOCALITY_LEVEL_4:Lcom/google/maps/model/AddressComponentType;

.field public static final enum SUBLOCALITY_LEVEL_5:Lcom/google/maps/model/AddressComponentType;

.field public static final enum SUBPREMISE:Lcom/google/maps/model/AddressComponentType;

.field public static final enum TRAIN_STATION:Lcom/google/maps/model/AddressComponentType;

.field public static final enum TRANSIT_STATION:Lcom/google/maps/model/AddressComponentType;

.field public static final enum UNKNOWN:Lcom/google/maps/model/AddressComponentType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 28
    new-instance v0, Lcom/google/maps/model/AddressComponentType;

    const-string v1, "STREET_ADDRESS"

    invoke-direct {v0, v1, v3}, Lcom/google/maps/model/AddressComponentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/maps/model/AddressComponentType;->STREET_ADDRESS:Lcom/google/maps/model/AddressComponentType;

    .line 33
    new-instance v0, Lcom/google/maps/model/AddressComponentType;

    const-string v1, "ROUTE"

    invoke-direct {v0, v1, v4}, Lcom/google/maps/model/AddressComponentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/maps/model/AddressComponentType;->ROUTE:Lcom/google/maps/model/AddressComponentType;

    .line 38
    new-instance v0, Lcom/google/maps/model/AddressComponentType;

    const-string v1, "INTERSECTION"

    invoke-direct {v0, v1, v5}, Lcom/google/maps/model/AddressComponentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/maps/model/AddressComponentType;->INTERSECTION:Lcom/google/maps/model/AddressComponentType;

    .line 44
    new-instance v0, Lcom/google/maps/model/AddressComponentType;

    const-string v1, "POLITICAL"

    invoke-direct {v0, v1, v6}, Lcom/google/maps/model/AddressComponentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/maps/model/AddressComponentType;->POLITICAL:Lcom/google/maps/model/AddressComponentType;

    .line 50
    new-instance v0, Lcom/google/maps/model/AddressComponentType;

    const-string v1, "COUNTRY"

    invoke-direct {v0, v1, v7}, Lcom/google/maps/model/AddressComponentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/maps/model/AddressComponentType;->COUNTRY:Lcom/google/maps/model/AddressComponentType;

    .line 57
    new-instance v0, Lcom/google/maps/model/AddressComponentType;

    const-string v1, "ADMINISTRATIVE_AREA_LEVEL_1"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/maps/model/AddressComponentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/maps/model/AddressComponentType;->ADMINISTRATIVE_AREA_LEVEL_1:Lcom/google/maps/model/AddressComponentType;

    .line 64
    new-instance v0, Lcom/google/maps/model/AddressComponentType;

    const-string v1, "ADMINISTRATIVE_AREA_LEVEL_2"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/google/maps/model/AddressComponentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/maps/model/AddressComponentType;->ADMINISTRATIVE_AREA_LEVEL_2:Lcom/google/maps/model/AddressComponentType;

    .line 71
    new-instance v0, Lcom/google/maps/model/AddressComponentType;

    const-string v1, "ADMINISTRATIVE_AREA_LEVEL_3"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/google/maps/model/AddressComponentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/maps/model/AddressComponentType;->ADMINISTRATIVE_AREA_LEVEL_3:Lcom/google/maps/model/AddressComponentType;

    .line 78
    new-instance v0, Lcom/google/maps/model/AddressComponentType;

    const-string v1, "ADMINISTRATIVE_AREA_LEVEL_4"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/google/maps/model/AddressComponentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/maps/model/AddressComponentType;->ADMINISTRATIVE_AREA_LEVEL_4:Lcom/google/maps/model/AddressComponentType;

    .line 85
    new-instance v0, Lcom/google/maps/model/AddressComponentType;

    const-string v1, "ADMINISTRATIVE_AREA_LEVEL_5"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/google/maps/model/AddressComponentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/maps/model/AddressComponentType;->ADMINISTRATIVE_AREA_LEVEL_5:Lcom/google/maps/model/AddressComponentType;

    .line 90
    new-instance v0, Lcom/google/maps/model/AddressComponentType;

    const-string v1, "COLLOQUIAL_AREA"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/google/maps/model/AddressComponentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/maps/model/AddressComponentType;->COLLOQUIAL_AREA:Lcom/google/maps/model/AddressComponentType;

    .line 95
    new-instance v0, Lcom/google/maps/model/AddressComponentType;

    const-string v1, "LOCALITY"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/google/maps/model/AddressComponentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/maps/model/AddressComponentType;->LOCALITY:Lcom/google/maps/model/AddressComponentType;

    .line 102
    new-instance v0, Lcom/google/maps/model/AddressComponentType;

    const-string v1, "SUBLOCALITY"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/google/maps/model/AddressComponentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/maps/model/AddressComponentType;->SUBLOCALITY:Lcom/google/maps/model/AddressComponentType;

    .line 103
    new-instance v0, Lcom/google/maps/model/AddressComponentType;

    const-string v1, "SUBLOCALITY_LEVEL_1"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/google/maps/model/AddressComponentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/maps/model/AddressComponentType;->SUBLOCALITY_LEVEL_1:Lcom/google/maps/model/AddressComponentType;

    .line 104
    new-instance v0, Lcom/google/maps/model/AddressComponentType;

    const-string v1, "SUBLOCALITY_LEVEL_2"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/google/maps/model/AddressComponentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/maps/model/AddressComponentType;->SUBLOCALITY_LEVEL_2:Lcom/google/maps/model/AddressComponentType;

    .line 105
    new-instance v0, Lcom/google/maps/model/AddressComponentType;

    const-string v1, "SUBLOCALITY_LEVEL_3"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/google/maps/model/AddressComponentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/maps/model/AddressComponentType;->SUBLOCALITY_LEVEL_3:Lcom/google/maps/model/AddressComponentType;

    .line 106
    new-instance v0, Lcom/google/maps/model/AddressComponentType;

    const-string v1, "SUBLOCALITY_LEVEL_4"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/google/maps/model/AddressComponentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/maps/model/AddressComponentType;->SUBLOCALITY_LEVEL_4:Lcom/google/maps/model/AddressComponentType;

    .line 107
    new-instance v0, Lcom/google/maps/model/AddressComponentType;

    const-string v1, "SUBLOCALITY_LEVEL_5"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/google/maps/model/AddressComponentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/maps/model/AddressComponentType;->SUBLOCALITY_LEVEL_5:Lcom/google/maps/model/AddressComponentType;

    .line 113
    new-instance v0, Lcom/google/maps/model/AddressComponentType;

    const-string v1, "NEIGHBORHOOD"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/google/maps/model/AddressComponentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/maps/model/AddressComponentType;->NEIGHBORHOOD:Lcom/google/maps/model/AddressComponentType;

    .line 119
    new-instance v0, Lcom/google/maps/model/AddressComponentType;

    const-string v1, "PREMISE"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lcom/google/maps/model/AddressComponentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/maps/model/AddressComponentType;->PREMISE:Lcom/google/maps/model/AddressComponentType;

    .line 125
    new-instance v0, Lcom/google/maps/model/AddressComponentType;

    const-string v1, "SUBPREMISE"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lcom/google/maps/model/AddressComponentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/maps/model/AddressComponentType;->SUBPREMISE:Lcom/google/maps/model/AddressComponentType;

    .line 131
    new-instance v0, Lcom/google/maps/model/AddressComponentType;

    const-string v1, "POSTAL_CODE"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Lcom/google/maps/model/AddressComponentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/maps/model/AddressComponentType;->POSTAL_CODE:Lcom/google/maps/model/AddressComponentType;

    .line 137
    new-instance v0, Lcom/google/maps/model/AddressComponentType;

    const-string v1, "POSTAL_CODE_SUFFIX"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, Lcom/google/maps/model/AddressComponentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/maps/model/AddressComponentType;->POSTAL_CODE_SUFFIX:Lcom/google/maps/model/AddressComponentType;

    .line 142
    new-instance v0, Lcom/google/maps/model/AddressComponentType;

    const-string v1, "NATURAL_FEATURE"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, Lcom/google/maps/model/AddressComponentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/maps/model/AddressComponentType;->NATURAL_FEATURE:Lcom/google/maps/model/AddressComponentType;

    .line 147
    new-instance v0, Lcom/google/maps/model/AddressComponentType;

    const-string v1, "AIRPORT"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2}, Lcom/google/maps/model/AddressComponentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/maps/model/AddressComponentType;->AIRPORT:Lcom/google/maps/model/AddressComponentType;

    .line 152
    new-instance v0, Lcom/google/maps/model/AddressComponentType;

    const-string v1, "PARK"

    const/16 v2, 0x19

    invoke-direct {v0, v1, v2}, Lcom/google/maps/model/AddressComponentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/maps/model/AddressComponentType;->PARK:Lcom/google/maps/model/AddressComponentType;

    .line 159
    new-instance v0, Lcom/google/maps/model/AddressComponentType;

    const-string v1, "POINT_OF_INTEREST"

    const/16 v2, 0x1a

    invoke-direct {v0, v1, v2}, Lcom/google/maps/model/AddressComponentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/maps/model/AddressComponentType;->POINT_OF_INTEREST:Lcom/google/maps/model/AddressComponentType;

    .line 164
    new-instance v0, Lcom/google/maps/model/AddressComponentType;

    const-string v1, "FLOOR"

    const/16 v2, 0x1b

    invoke-direct {v0, v1, v2}, Lcom/google/maps/model/AddressComponentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/maps/model/AddressComponentType;->FLOOR:Lcom/google/maps/model/AddressComponentType;

    .line 169
    new-instance v0, Lcom/google/maps/model/AddressComponentType;

    const-string v1, "ESTABLISHMENT"

    const/16 v2, 0x1c

    invoke-direct {v0, v1, v2}, Lcom/google/maps/model/AddressComponentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/maps/model/AddressComponentType;->ESTABLISHMENT:Lcom/google/maps/model/AddressComponentType;

    .line 174
    new-instance v0, Lcom/google/maps/model/AddressComponentType;

    const-string v1, "PARKING"

    const/16 v2, 0x1d

    invoke-direct {v0, v1, v2}, Lcom/google/maps/model/AddressComponentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/maps/model/AddressComponentType;->PARKING:Lcom/google/maps/model/AddressComponentType;

    .line 179
    new-instance v0, Lcom/google/maps/model/AddressComponentType;

    const-string v1, "POST_BOX"

    const/16 v2, 0x1e

    invoke-direct {v0, v1, v2}, Lcom/google/maps/model/AddressComponentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/maps/model/AddressComponentType;->POST_BOX:Lcom/google/maps/model/AddressComponentType;

    .line 185
    new-instance v0, Lcom/google/maps/model/AddressComponentType;

    const-string v1, "POSTAL_TOWN"

    const/16 v2, 0x1f

    invoke-direct {v0, v1, v2}, Lcom/google/maps/model/AddressComponentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/maps/model/AddressComponentType;->POSTAL_TOWN:Lcom/google/maps/model/AddressComponentType;

    .line 190
    new-instance v0, Lcom/google/maps/model/AddressComponentType;

    const-string v1, "ROOM"

    const/16 v2, 0x20

    invoke-direct {v0, v1, v2}, Lcom/google/maps/model/AddressComponentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/maps/model/AddressComponentType;->ROOM:Lcom/google/maps/model/AddressComponentType;

    .line 195
    new-instance v0, Lcom/google/maps/model/AddressComponentType;

    const-string v1, "STREET_NUMBER"

    const/16 v2, 0x21

    invoke-direct {v0, v1, v2}, Lcom/google/maps/model/AddressComponentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/maps/model/AddressComponentType;->STREET_NUMBER:Lcom/google/maps/model/AddressComponentType;

    .line 200
    new-instance v0, Lcom/google/maps/model/AddressComponentType;

    const-string v1, "BUS_STATION"

    const/16 v2, 0x22

    invoke-direct {v0, v1, v2}, Lcom/google/maps/model/AddressComponentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/maps/model/AddressComponentType;->BUS_STATION:Lcom/google/maps/model/AddressComponentType;

    .line 205
    new-instance v0, Lcom/google/maps/model/AddressComponentType;

    const-string v1, "TRAIN_STATION"

    const/16 v2, 0x23

    invoke-direct {v0, v1, v2}, Lcom/google/maps/model/AddressComponentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/maps/model/AddressComponentType;->TRAIN_STATION:Lcom/google/maps/model/AddressComponentType;

    .line 210
    new-instance v0, Lcom/google/maps/model/AddressComponentType;

    const-string v1, "TRANSIT_STATION"

    const/16 v2, 0x24

    invoke-direct {v0, v1, v2}, Lcom/google/maps/model/AddressComponentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/maps/model/AddressComponentType;->TRANSIT_STATION:Lcom/google/maps/model/AddressComponentType;

    .line 216
    new-instance v0, Lcom/google/maps/model/AddressComponentType;

    const-string v1, "UNKNOWN"

    const/16 v2, 0x25

    invoke-direct {v0, v1, v2}, Lcom/google/maps/model/AddressComponentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/maps/model/AddressComponentType;->UNKNOWN:Lcom/google/maps/model/AddressComponentType;

    .line 23
    const/16 v0, 0x26

    new-array v0, v0, [Lcom/google/maps/model/AddressComponentType;

    sget-object v1, Lcom/google/maps/model/AddressComponentType;->STREET_ADDRESS:Lcom/google/maps/model/AddressComponentType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/maps/model/AddressComponentType;->ROUTE:Lcom/google/maps/model/AddressComponentType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/maps/model/AddressComponentType;->INTERSECTION:Lcom/google/maps/model/AddressComponentType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/maps/model/AddressComponentType;->POLITICAL:Lcom/google/maps/model/AddressComponentType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/maps/model/AddressComponentType;->COUNTRY:Lcom/google/maps/model/AddressComponentType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/maps/model/AddressComponentType;->ADMINISTRATIVE_AREA_LEVEL_1:Lcom/google/maps/model/AddressComponentType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/maps/model/AddressComponentType;->ADMINISTRATIVE_AREA_LEVEL_2:Lcom/google/maps/model/AddressComponentType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/maps/model/AddressComponentType;->ADMINISTRATIVE_AREA_LEVEL_3:Lcom/google/maps/model/AddressComponentType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/maps/model/AddressComponentType;->ADMINISTRATIVE_AREA_LEVEL_4:Lcom/google/maps/model/AddressComponentType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/maps/model/AddressComponentType;->ADMINISTRATIVE_AREA_LEVEL_5:Lcom/google/maps/model/AddressComponentType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/maps/model/AddressComponentType;->COLLOQUIAL_AREA:Lcom/google/maps/model/AddressComponentType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/maps/model/AddressComponentType;->LOCALITY:Lcom/google/maps/model/AddressComponentType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/google/maps/model/AddressComponentType;->SUBLOCALITY:Lcom/google/maps/model/AddressComponentType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/google/maps/model/AddressComponentType;->SUBLOCALITY_LEVEL_1:Lcom/google/maps/model/AddressComponentType;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/google/maps/model/AddressComponentType;->SUBLOCALITY_LEVEL_2:Lcom/google/maps/model/AddressComponentType;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/google/maps/model/AddressComponentType;->SUBLOCALITY_LEVEL_3:Lcom/google/maps/model/AddressComponentType;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/google/maps/model/AddressComponentType;->SUBLOCALITY_LEVEL_4:Lcom/google/maps/model/AddressComponentType;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/google/maps/model/AddressComponentType;->SUBLOCALITY_LEVEL_5:Lcom/google/maps/model/AddressComponentType;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/google/maps/model/AddressComponentType;->NEIGHBORHOOD:Lcom/google/maps/model/AddressComponentType;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/google/maps/model/AddressComponentType;->PREMISE:Lcom/google/maps/model/AddressComponentType;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/google/maps/model/AddressComponentType;->SUBPREMISE:Lcom/google/maps/model/AddressComponentType;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/google/maps/model/AddressComponentType;->POSTAL_CODE:Lcom/google/maps/model/AddressComponentType;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/google/maps/model/AddressComponentType;->POSTAL_CODE_SUFFIX:Lcom/google/maps/model/AddressComponentType;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/google/maps/model/AddressComponentType;->NATURAL_FEATURE:Lcom/google/maps/model/AddressComponentType;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/google/maps/model/AddressComponentType;->AIRPORT:Lcom/google/maps/model/AddressComponentType;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/google/maps/model/AddressComponentType;->PARK:Lcom/google/maps/model/AddressComponentType;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/google/maps/model/AddressComponentType;->POINT_OF_INTEREST:Lcom/google/maps/model/AddressComponentType;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/google/maps/model/AddressComponentType;->FLOOR:Lcom/google/maps/model/AddressComponentType;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/google/maps/model/AddressComponentType;->ESTABLISHMENT:Lcom/google/maps/model/AddressComponentType;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/google/maps/model/AddressComponentType;->PARKING:Lcom/google/maps/model/AddressComponentType;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/google/maps/model/AddressComponentType;->POST_BOX:Lcom/google/maps/model/AddressComponentType;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/google/maps/model/AddressComponentType;->POSTAL_TOWN:Lcom/google/maps/model/AddressComponentType;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/google/maps/model/AddressComponentType;->ROOM:Lcom/google/maps/model/AddressComponentType;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/google/maps/model/AddressComponentType;->STREET_NUMBER:Lcom/google/maps/model/AddressComponentType;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/google/maps/model/AddressComponentType;->BUS_STATION:Lcom/google/maps/model/AddressComponentType;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/google/maps/model/AddressComponentType;->TRAIN_STATION:Lcom/google/maps/model/AddressComponentType;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lcom/google/maps/model/AddressComponentType;->TRANSIT_STATION:Lcom/google/maps/model/AddressComponentType;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lcom/google/maps/model/AddressComponentType;->UNKNOWN:Lcom/google/maps/model/AddressComponentType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/maps/model/AddressComponentType;->$VALUES:[Lcom/google/maps/model/AddressComponentType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/maps/model/AddressComponentType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 23
    const-class v0, Lcom/google/maps/model/AddressComponentType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/maps/model/AddressComponentType;

    return-object v0
.end method

.method public static values()[Lcom/google/maps/model/AddressComponentType;
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/google/maps/model/AddressComponentType;->$VALUES:[Lcom/google/maps/model/AddressComponentType;

    invoke-virtual {v0}, [Lcom/google/maps/model/AddressComponentType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/maps/model/AddressComponentType;

    return-object v0
.end method
