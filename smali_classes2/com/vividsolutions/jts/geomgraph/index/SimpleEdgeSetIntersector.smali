.class public Lcom/vividsolutions/jts/geomgraph/index/SimpleEdgeSetIntersector;
.super Lcom/vividsolutions/jts/geomgraph/index/EdgeSetIntersector;
.source "SimpleEdgeSetIntersector.java"


# instance fields
.field nOverlaps:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/vividsolutions/jts/geomgraph/index/EdgeSetIntersector;-><init>()V

    .line 56
    return-void
.end method

.method private computeIntersects(Lcom/vividsolutions/jts/geomgraph/Edge;Lcom/vividsolutions/jts/geomgraph/Edge;Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;)V
    .locals 5
    .param p1, "e0"    # Lcom/vividsolutions/jts/geomgraph/Edge;
    .param p2, "e1"    # Lcom/vividsolutions/jts/geomgraph/Edge;
    .param p3, "si"    # Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;

    .prologue
    .line 93
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geomgraph/Edge;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v2

    .line 94
    .local v2, "pts0":[Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-virtual {p2}, Lcom/vividsolutions/jts/geomgraph/Edge;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v3

    .line 95
    .local v3, "pts1":[Lcom/vividsolutions/jts/geom/Coordinate;
    const/4 v0, 0x0

    .local v0, "i0":I
    :goto_0
    array-length v4, v2

    add-int/lit8 v4, v4, -0x1

    if-ge v0, v4, :cond_1

    .line 96
    const/4 v1, 0x0

    .local v1, "i1":I
    :goto_1
    array-length v4, v3

    add-int/lit8 v4, v4, -0x1

    if-ge v1, v4, :cond_0

    .line 97
    invoke-virtual {p3, p1, v0, p2, v1}, Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;->addIntersections(Lcom/vividsolutions/jts/geomgraph/Edge;ILcom/vividsolutions/jts/geomgraph/Edge;I)V

    .line 96
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 95
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 100
    .end local v1    # "i1":I
    :cond_1
    return-void
.end method


# virtual methods
.method public computeIntersections(Ljava/util/List;Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;Z)V
    .locals 5
    .param p1, "edges"    # Ljava/util/List;
    .param p2, "si"    # Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;
    .param p3, "testAllSegments"    # Z

    .prologue
    .line 60
    const/4 v4, 0x0

    iput v4, p0, Lcom/vividsolutions/jts/geomgraph/index/SimpleEdgeSetIntersector;->nOverlaps:I

    .line 62
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i0":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 63
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geomgraph/Edge;

    .line 64
    .local v0, "edge0":Lcom/vividsolutions/jts/geomgraph/Edge;
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i1":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 65
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vividsolutions/jts/geomgraph/Edge;

    .line 66
    .local v1, "edge1":Lcom/vividsolutions/jts/geomgraph/Edge;
    if-nez p3, :cond_2

    if-eq v0, v1, :cond_1

    .line 67
    :cond_2
    invoke-direct {p0, v0, v1, p2}, Lcom/vividsolutions/jts/geomgraph/index/SimpleEdgeSetIntersector;->computeIntersects(Lcom/vividsolutions/jts/geomgraph/Edge;Lcom/vividsolutions/jts/geomgraph/Edge;Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;)V

    goto :goto_0

    .line 70
    .end local v0    # "edge0":Lcom/vividsolutions/jts/geomgraph/Edge;
    .end local v1    # "edge1":Lcom/vividsolutions/jts/geomgraph/Edge;
    .end local v3    # "i1":Ljava/util/Iterator;
    :cond_3
    return-void
.end method

.method public computeIntersections(Ljava/util/List;Ljava/util/List;Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;)V
    .locals 5
    .param p1, "edges0"    # Ljava/util/List;
    .param p2, "edges1"    # Ljava/util/List;
    .param p3, "si"    # Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;

    .prologue
    .line 75
    const/4 v4, 0x0

    iput v4, p0, Lcom/vividsolutions/jts/geomgraph/index/SimpleEdgeSetIntersector;->nOverlaps:I

    .line 77
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i0":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 78
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geomgraph/Edge;

    .line 79
    .local v0, "edge0":Lcom/vividsolutions/jts/geomgraph/Edge;
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i1":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 80
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vividsolutions/jts/geomgraph/Edge;

    .line 81
    .local v1, "edge1":Lcom/vividsolutions/jts/geomgraph/Edge;
    invoke-direct {p0, v0, v1, p3}, Lcom/vividsolutions/jts/geomgraph/index/SimpleEdgeSetIntersector;->computeIntersects(Lcom/vividsolutions/jts/geomgraph/Edge;Lcom/vividsolutions/jts/geomgraph/Edge;Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;)V

    goto :goto_0

    .line 84
    .end local v0    # "edge0":Lcom/vividsolutions/jts/geomgraph/Edge;
    .end local v1    # "edge1":Lcom/vividsolutions/jts/geomgraph/Edge;
    .end local v3    # "i1":Ljava/util/Iterator;
    :cond_1
    return-void
.end method
