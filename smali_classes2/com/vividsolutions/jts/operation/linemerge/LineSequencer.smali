.class public Lcom/vividsolutions/jts/operation/linemerge/LineSequencer;
.super Ljava/lang/Object;
.source "LineSequencer.java"


# instance fields
.field private factory:Lcom/vividsolutions/jts/geom/GeometryFactory;

.field private graph:Lcom/vividsolutions/jts/operation/linemerge/LineMergeGraph;

.field private isRun:Z

.field private isSequenceable:Z

.field private lineCount:I

.field private sequencedGeometry:Lcom/vividsolutions/jts/geom/Geometry;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 140
    new-instance v0, Lcom/vividsolutions/jts/operation/linemerge/LineMergeGraph;

    invoke-direct {v0}, Lcom/vividsolutions/jts/operation/linemerge/LineMergeGraph;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/linemerge/LineSequencer;->graph:Lcom/vividsolutions/jts/operation/linemerge/LineMergeGraph;

    .line 142
    new-instance v0, Lcom/vividsolutions/jts/geom/GeometryFactory;

    invoke-direct {v0}, Lcom/vividsolutions/jts/geom/GeometryFactory;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/linemerge/LineSequencer;->factory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    .line 143
    iput v1, p0, Lcom/vividsolutions/jts/operation/linemerge/LineSequencer;->lineCount:I

    .line 145
    iput-boolean v1, p0, Lcom/vividsolutions/jts/operation/linemerge/LineSequencer;->isRun:Z

    .line 146
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/linemerge/LineSequencer;->sequencedGeometry:Lcom/vividsolutions/jts/geom/Geometry;

    .line 147
    iput-boolean v1, p0, Lcom/vividsolutions/jts/operation/linemerge/LineSequencer;->isSequenceable:Z

    return-void
.end method

.method static synthetic access$000(Lcom/vividsolutions/jts/operation/linemerge/LineSequencer;Lcom/vividsolutions/jts/geom/LineString;)V
    .locals 0
    .param p0, "x0"    # Lcom/vividsolutions/jts/operation/linemerge/LineSequencer;
    .param p1, "x1"    # Lcom/vividsolutions/jts/geom/LineString;

    .prologue
    .line 84
    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/operation/linemerge/LineSequencer;->addLine(Lcom/vividsolutions/jts/geom/LineString;)V

    return-void
.end method

.method private addLine(Lcom/vividsolutions/jts/geom/LineString;)V
    .locals 1
    .param p1, "lineString"    # Lcom/vividsolutions/jts/geom/LineString;

    .prologue
    .line 182
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/linemerge/LineSequencer;->factory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    if-nez v0, :cond_0

    .line 183
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/LineString;->getFactory()Lcom/vividsolutions/jts/geom/GeometryFactory;

    move-result-object v0

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/linemerge/LineSequencer;->factory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    .line 185
    :cond_0
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/linemerge/LineSequencer;->graph:Lcom/vividsolutions/jts/operation/linemerge/LineMergeGraph;

    invoke-virtual {v0, p1}, Lcom/vividsolutions/jts/operation/linemerge/LineMergeGraph;->addEdge(Lcom/vividsolutions/jts/geom/LineString;)V

    .line 186
    iget v0, p0, Lcom/vividsolutions/jts/operation/linemerge/LineSequencer;->lineCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/vividsolutions/jts/operation/linemerge/LineSequencer;->lineCount:I

    .line 187
    return-void
.end method

.method private addReverseSubpath(Lcom/vividsolutions/jts/planargraph/DirectedEdge;Ljava/util/ListIterator;Z)V
    .locals 5
    .param p1, "de"    # Lcom/vividsolutions/jts/planargraph/DirectedEdge;
    .param p2, "lit"    # Ljava/util/ListIterator;
    .param p3, "expectedClosed"    # Z

    .prologue
    const/4 v3, 0x1

    .line 321
    invoke-virtual {p1}, Lcom/vividsolutions/jts/planargraph/DirectedEdge;->getToNode()Lcom/vividsolutions/jts/planargraph/Node;

    move-result-object v0

    .line 323
    .local v0, "endNode":Lcom/vividsolutions/jts/planargraph/Node;
    const/4 v1, 0x0

    .line 325
    .local v1, "fromNode":Lcom/vividsolutions/jts/planargraph/Node;
    :goto_0
    invoke-virtual {p1}, Lcom/vividsolutions/jts/planargraph/DirectedEdge;->getSym()Lcom/vividsolutions/jts/planargraph/DirectedEdge;

    move-result-object v4

    invoke-interface {p2, v4}, Ljava/util/ListIterator;->add(Ljava/lang/Object;)V

    .line 326
    invoke-virtual {p1}, Lcom/vividsolutions/jts/planargraph/DirectedEdge;->getEdge()Lcom/vividsolutions/jts/planargraph/Edge;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/vividsolutions/jts/planargraph/Edge;->setVisited(Z)V

    .line 327
    invoke-virtual {p1}, Lcom/vividsolutions/jts/planargraph/DirectedEdge;->getFromNode()Lcom/vividsolutions/jts/planargraph/Node;

    move-result-object v1

    .line 328
    invoke-static {v1}, Lcom/vividsolutions/jts/operation/linemerge/LineSequencer;->findUnvisitedBestOrientedDE(Lcom/vividsolutions/jts/planargraph/Node;)Lcom/vividsolutions/jts/planargraph/DirectedEdge;

    move-result-object v2

    .line 330
    .local v2, "unvisitedOutDE":Lcom/vividsolutions/jts/planargraph/DirectedEdge;
    if-nez v2, :cond_1

    .line 334
    if-eqz p3, :cond_0

    .line 336
    if-ne v1, v0, :cond_2

    :goto_1
    const-string v4, "path not contiguous"

    invoke-static {v3, v4}, Lcom/vividsolutions/jts/util/Assert;->isTrue(ZLjava/lang/String;)V

    .line 338
    :cond_0
    return-void

    .line 332
    :cond_1
    invoke-virtual {v2}, Lcom/vividsolutions/jts/planargraph/DirectedEdge;->getSym()Lcom/vividsolutions/jts/planargraph/DirectedEdge;

    move-result-object p1

    .line 333
    goto :goto_0

    .line 336
    :cond_2
    const/4 v3, 0x0

    goto :goto_1
.end method

.method private buildSequencedGeometry(Ljava/util/List;)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 10
    .param p1, "sequences"    # Ljava/util/List;

    .prologue
    .line 444
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 446
    .local v6, "lines":Ljava/util/List;
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i1":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_2

    .line 447
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/List;

    .line 448
    .local v7, "seq":Ljava/util/List;
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i2":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 449
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/planargraph/DirectedEdge;

    .line 450
    .local v0, "de":Lcom/vividsolutions/jts/planargraph/DirectedEdge;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/planargraph/DirectedEdge;->getEdge()Lcom/vividsolutions/jts/planargraph/Edge;

    move-result-object v1

    check-cast v1, Lcom/vividsolutions/jts/operation/linemerge/LineMergeEdge;

    .line 451
    .local v1, "e":Lcom/vividsolutions/jts/operation/linemerge/LineMergeEdge;
    invoke-virtual {v1}, Lcom/vividsolutions/jts/operation/linemerge/LineMergeEdge;->getLine()Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v4

    .line 453
    .local v4, "line":Lcom/vividsolutions/jts/geom/LineString;
    move-object v5, v4

    .line 454
    .local v5, "lineToAdd":Lcom/vividsolutions/jts/geom/LineString;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/planargraph/DirectedEdge;->getEdgeDirection()Z

    move-result v8

    if-nez v8, :cond_1

    invoke-virtual {v4}, Lcom/vividsolutions/jts/geom/LineString;->isClosed()Z

    move-result v8

    if-nez v8, :cond_1

    .line 455
    invoke-static {v4}, Lcom/vividsolutions/jts/operation/linemerge/LineSequencer;->reverse(Lcom/vividsolutions/jts/geom/LineString;)Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v5

    .line 457
    :cond_1
    invoke-interface {v6, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 460
    .end local v0    # "de":Lcom/vividsolutions/jts/planargraph/DirectedEdge;
    .end local v1    # "e":Lcom/vividsolutions/jts/operation/linemerge/LineMergeEdge;
    .end local v3    # "i2":Ljava/util/Iterator;
    .end local v4    # "line":Lcom/vividsolutions/jts/geom/LineString;
    .end local v5    # "lineToAdd":Lcom/vividsolutions/jts/geom/LineString;
    .end local v7    # "seq":Ljava/util/List;
    :cond_2
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v8

    if-nez v8, :cond_3

    .line 461
    iget-object v8, p0, Lcom/vividsolutions/jts/operation/linemerge/LineSequencer;->factory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    const/4 v9, 0x0

    new-array v9, v9, [Lcom/vividsolutions/jts/geom/LineString;

    invoke-virtual {v8, v9}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createMultiLineString([Lcom/vividsolutions/jts/geom/LineString;)Lcom/vividsolutions/jts/geom/MultiLineString;

    move-result-object v8

    .line 462
    :goto_1
    return-object v8

    :cond_3
    iget-object v8, p0, Lcom/vividsolutions/jts/operation/linemerge/LineSequencer;->factory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    invoke-virtual {v8, v6}, Lcom/vividsolutions/jts/geom/GeometryFactory;->buildGeometry(Ljava/util/Collection;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v8

    goto :goto_1
.end method

.method private computeSequence()V
    .locals 6

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 213
    iget-boolean v2, p0, Lcom/vividsolutions/jts/operation/linemerge/LineSequencer;->isRun:Z

    if-eqz v2, :cond_1

    .line 228
    :cond_0
    :goto_0
    return-void

    .line 214
    :cond_1
    iput-boolean v3, p0, Lcom/vividsolutions/jts/operation/linemerge/LineSequencer;->isRun:Z

    .line 216
    invoke-direct {p0}, Lcom/vividsolutions/jts/operation/linemerge/LineSequencer;->findSequences()Ljava/util/List;

    move-result-object v1

    .line 217
    .local v1, "sequences":Ljava/util/List;
    if-eqz v1, :cond_0

    .line 220
    invoke-direct {p0, v1}, Lcom/vividsolutions/jts/operation/linemerge/LineSequencer;->buildSequencedGeometry(Ljava/util/List;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v2

    iput-object v2, p0, Lcom/vividsolutions/jts/operation/linemerge/LineSequencer;->sequencedGeometry:Lcom/vividsolutions/jts/geom/Geometry;

    .line 221
    iput-boolean v3, p0, Lcom/vividsolutions/jts/operation/linemerge/LineSequencer;->isSequenceable:Z

    .line 223
    iget-object v2, p0, Lcom/vividsolutions/jts/operation/linemerge/LineSequencer;->sequencedGeometry:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-virtual {v2}, Lcom/vividsolutions/jts/geom/Geometry;->getNumGeometries()I

    move-result v0

    .line 224
    .local v0, "finalLineCount":I
    iget v2, p0, Lcom/vividsolutions/jts/operation/linemerge/LineSequencer;->lineCount:I

    if-ne v2, v0, :cond_4

    move v2, v3

    :goto_1
    const-string v5, "Lines were missing from result"

    invoke-static {v2, v5}, Lcom/vividsolutions/jts/util/Assert;->isTrue(ZLjava/lang/String;)V

    .line 225
    iget-object v2, p0, Lcom/vividsolutions/jts/operation/linemerge/LineSequencer;->sequencedGeometry:Lcom/vividsolutions/jts/geom/Geometry;

    instance-of v2, v2, Lcom/vividsolutions/jts/geom/LineString;

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/vividsolutions/jts/operation/linemerge/LineSequencer;->sequencedGeometry:Lcom/vividsolutions/jts/geom/Geometry;

    instance-of v2, v2, Lcom/vividsolutions/jts/geom/MultiLineString;

    if-eqz v2, :cond_3

    :cond_2
    move v4, v3

    :cond_3
    const-string v2, "Result is not lineal"

    invoke-static {v4, v2}, Lcom/vividsolutions/jts/util/Assert;->isTrue(ZLjava/lang/String;)V

    goto :goto_0

    :cond_4
    move v2, v4

    .line 224
    goto :goto_1
.end method

.method private static findLowestDegreeNode(Lcom/vividsolutions/jts/planargraph/Subgraph;)Lcom/vividsolutions/jts/planargraph/Node;
    .locals 5
    .param p0, "graph"    # Lcom/vividsolutions/jts/planargraph/Subgraph;

    .prologue
    .line 342
    const v1, 0x7fffffff

    .line 343
    .local v1, "minDegree":I
    const/4 v2, 0x0

    .line 344
    .local v2, "minDegreeNode":Lcom/vividsolutions/jts/planargraph/Node;
    invoke-virtual {p0}, Lcom/vividsolutions/jts/planargraph/Subgraph;->nodeIterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 345
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vividsolutions/jts/planargraph/Node;

    .line 346
    .local v3, "node":Lcom/vividsolutions/jts/planargraph/Node;
    if-eqz v2, :cond_1

    invoke-virtual {v3}, Lcom/vividsolutions/jts/planargraph/Node;->getDegree()I

    move-result v4

    if-ge v4, v1, :cond_0

    .line 347
    :cond_1
    invoke-virtual {v3}, Lcom/vividsolutions/jts/planargraph/Node;->getDegree()I

    move-result v1

    .line 348
    move-object v2, v3

    goto :goto_0

    .line 351
    .end local v3    # "node":Lcom/vividsolutions/jts/planargraph/Node;
    :cond_2
    return-object v2
.end method

.method private findSequence(Lcom/vividsolutions/jts/planargraph/Subgraph;)Ljava/util/List;
    .locals 10
    .param p1, "graph"    # Lcom/vividsolutions/jts/planargraph/Subgraph;

    .prologue
    const/4 v9, 0x0

    .line 269
    invoke-virtual {p1}, Lcom/vividsolutions/jts/planargraph/Subgraph;->edgeIterator()Ljava/util/Iterator;

    move-result-object v8

    invoke-static {v8, v9}, Lcom/vividsolutions/jts/planargraph/GraphComponent;->setVisited(Ljava/util/Iterator;Z)V

    .line 271
    invoke-static {p1}, Lcom/vividsolutions/jts/operation/linemerge/LineSequencer;->findLowestDegreeNode(Lcom/vividsolutions/jts/planargraph/Subgraph;)Lcom/vividsolutions/jts/planargraph/Node;

    move-result-object v6

    .line 272
    .local v6, "startNode":Lcom/vividsolutions/jts/planargraph/Node;
    invoke-virtual {v6}, Lcom/vividsolutions/jts/planargraph/Node;->getOutEdges()Lcom/vividsolutions/jts/planargraph/DirectedEdgeStar;

    move-result-object v8

    invoke-virtual {v8}, Lcom/vividsolutions/jts/planargraph/DirectedEdgeStar;->iterator()Ljava/util/Iterator;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vividsolutions/jts/planargraph/DirectedEdge;

    .line 273
    .local v4, "startDE":Lcom/vividsolutions/jts/planargraph/DirectedEdge;
    invoke-virtual {v4}, Lcom/vividsolutions/jts/planargraph/DirectedEdge;->getSym()Lcom/vividsolutions/jts/planargraph/DirectedEdge;

    move-result-object v5

    .line 275
    .local v5, "startDESym":Lcom/vividsolutions/jts/planargraph/DirectedEdge;
    new-instance v3, Ljava/util/LinkedList;

    invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V

    .line 276
    .local v3, "seq":Ljava/util/List;
    invoke-interface {v3}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v0

    .line 277
    .local v0, "lit":Ljava/util/ListIterator;
    invoke-direct {p0, v5, v0, v9}, Lcom/vividsolutions/jts/operation/linemerge/LineSequencer;->addReverseSubpath(Lcom/vividsolutions/jts/planargraph/DirectedEdge;Ljava/util/ListIterator;Z)V

    .line 278
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/ListIterator;->hasPrevious()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 279
    invoke-interface {v0}, Ljava/util/ListIterator;->previous()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vividsolutions/jts/planargraph/DirectedEdge;

    .line 280
    .local v2, "prev":Lcom/vividsolutions/jts/planargraph/DirectedEdge;
    invoke-virtual {v2}, Lcom/vividsolutions/jts/planargraph/DirectedEdge;->getFromNode()Lcom/vividsolutions/jts/planargraph/Node;

    move-result-object v8

    invoke-static {v8}, Lcom/vividsolutions/jts/operation/linemerge/LineSequencer;->findUnvisitedBestOrientedDE(Lcom/vividsolutions/jts/planargraph/Node;)Lcom/vividsolutions/jts/planargraph/DirectedEdge;

    move-result-object v7

    .line 281
    .local v7, "unvisitedOutDE":Lcom/vividsolutions/jts/planargraph/DirectedEdge;
    if-eqz v7, :cond_0

    .line 282
    invoke-virtual {v7}, Lcom/vividsolutions/jts/planargraph/DirectedEdge;->getSym()Lcom/vividsolutions/jts/planargraph/DirectedEdge;

    move-result-object v8

    const/4 v9, 0x1

    invoke-direct {p0, v8, v0, v9}, Lcom/vividsolutions/jts/operation/linemerge/LineSequencer;->addReverseSubpath(Lcom/vividsolutions/jts/planargraph/DirectedEdge;Ljava/util/ListIterator;Z)V

    goto :goto_0

    .line 290
    .end local v2    # "prev":Lcom/vividsolutions/jts/planargraph/DirectedEdge;
    .end local v7    # "unvisitedOutDE":Lcom/vividsolutions/jts/planargraph/DirectedEdge;
    :cond_1
    invoke-direct {p0, v3}, Lcom/vividsolutions/jts/operation/linemerge/LineSequencer;->orient(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    .line 291
    .local v1, "orientedSeq":Ljava/util/List;
    return-object v1
.end method

.method private findSequences()Ljava/util/List;
    .locals 7

    .prologue
    .line 232
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 233
    .local v3, "sequences":Ljava/util/List;
    new-instance v0, Lcom/vividsolutions/jts/planargraph/algorithm/ConnectedSubgraphFinder;

    iget-object v6, p0, Lcom/vividsolutions/jts/operation/linemerge/LineSequencer;->graph:Lcom/vividsolutions/jts/operation/linemerge/LineMergeGraph;

    invoke-direct {v0, v6}, Lcom/vividsolutions/jts/planargraph/algorithm/ConnectedSubgraphFinder;-><init>(Lcom/vividsolutions/jts/planargraph/PlanarGraph;)V

    .line 234
    .local v0, "csFinder":Lcom/vividsolutions/jts/planargraph/algorithm/ConnectedSubgraphFinder;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/planargraph/algorithm/ConnectedSubgraphFinder;->getConnectedSubgraphs()Ljava/util/List;

    move-result-object v5

    .line 235
    .local v5, "subgraphs":Ljava/util/List;
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 236
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vividsolutions/jts/planargraph/Subgraph;

    .line 237
    .local v4, "subgraph":Lcom/vividsolutions/jts/planargraph/Subgraph;
    invoke-direct {p0, v4}, Lcom/vividsolutions/jts/operation/linemerge/LineSequencer;->hasSequence(Lcom/vividsolutions/jts/planargraph/Subgraph;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 238
    invoke-direct {p0, v4}, Lcom/vividsolutions/jts/operation/linemerge/LineSequencer;->findSequence(Lcom/vividsolutions/jts/planargraph/Subgraph;)Ljava/util/List;

    move-result-object v2

    .line 239
    .local v2, "seq":Ljava/util/List;
    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 243
    .end local v2    # "seq":Ljava/util/List;
    :cond_0
    const/4 v3, 0x0

    .line 246
    .end local v3    # "sequences":Ljava/util/List;
    .end local v4    # "subgraph":Lcom/vividsolutions/jts/planargraph/Subgraph;
    :cond_1
    return-object v3
.end method

.method private static findUnvisitedBestOrientedDE(Lcom/vividsolutions/jts/planargraph/Node;)Lcom/vividsolutions/jts/planargraph/DirectedEdge;
    .locals 5
    .param p0, "node"    # Lcom/vividsolutions/jts/planargraph/Node;

    .prologue
    .line 303
    const/4 v3, 0x0

    .line 304
    .local v3, "wellOrientedDE":Lcom/vividsolutions/jts/planargraph/DirectedEdge;
    const/4 v2, 0x0

    .line 305
    .local v2, "unvisitedDE":Lcom/vividsolutions/jts/planargraph/DirectedEdge;
    invoke-virtual {p0}, Lcom/vividsolutions/jts/planargraph/Node;->getOutEdges()Lcom/vividsolutions/jts/planargraph/DirectedEdgeStar;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vividsolutions/jts/planargraph/DirectedEdgeStar;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 306
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/planargraph/DirectedEdge;

    .line 307
    .local v0, "de":Lcom/vividsolutions/jts/planargraph/DirectedEdge;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/planargraph/DirectedEdge;->getEdge()Lcom/vividsolutions/jts/planargraph/Edge;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vividsolutions/jts/planargraph/Edge;->isVisited()Z

    move-result v4

    if-nez v4, :cond_0

    .line 308
    move-object v2, v0

    .line 309
    invoke-virtual {v0}, Lcom/vividsolutions/jts/planargraph/DirectedEdge;->getEdgeDirection()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 310
    move-object v3, v0

    goto :goto_0

    .line 313
    .end local v0    # "de":Lcom/vividsolutions/jts/planargraph/DirectedEdge;
    :cond_1
    if-eqz v3, :cond_2

    .line 315
    .end local v3    # "wellOrientedDE":Lcom/vividsolutions/jts/planargraph/DirectedEdge;
    :goto_1
    return-object v3

    .restart local v3    # "wellOrientedDE":Lcom/vividsolutions/jts/planargraph/DirectedEdge;
    :cond_2
    move-object v3, v2

    goto :goto_1
.end method

.method private hasSequence(Lcom/vividsolutions/jts/planargraph/Subgraph;)Z
    .locals 5
    .param p1, "graph"    # Lcom/vividsolutions/jts/planargraph/Subgraph;

    .prologue
    const/4 v3, 0x1

    .line 258
    const/4 v2, 0x0

    .line 259
    .local v2, "oddDegreeCount":I
    invoke-virtual {p1}, Lcom/vividsolutions/jts/planargraph/Subgraph;->nodeIterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 260
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vividsolutions/jts/planargraph/Node;

    .line 261
    .local v1, "node":Lcom/vividsolutions/jts/planargraph/Node;
    invoke-virtual {v1}, Lcom/vividsolutions/jts/planargraph/Node;->getDegree()I

    move-result v4

    rem-int/lit8 v4, v4, 0x2

    if-ne v4, v3, :cond_0

    .line 262
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 264
    .end local v1    # "node":Lcom/vividsolutions/jts/planargraph/Node;
    :cond_1
    const/4 v4, 0x2

    if-gt v2, v4, :cond_2

    :goto_1
    return v3

    :cond_2
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public static isSequenced(Lcom/vividsolutions/jts/geom/Geometry;)Z
    .locals 11
    .param p0, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 105
    instance-of v10, p0, Lcom/vividsolutions/jts/geom/MultiLineString;

    if-nez v10, :cond_1

    .line 137
    :cond_0
    :goto_0
    return v8

    :cond_1
    move-object v5, p0

    .line 109
    check-cast v5, Lcom/vividsolutions/jts/geom/MultiLineString;

    .line 111
    .local v5, "mls":Lcom/vividsolutions/jts/geom/MultiLineString;
    new-instance v6, Ljava/util/TreeSet;

    invoke-direct {v6}, Ljava/util/TreeSet;-><init>()V

    .line 113
    .local v6, "prevSubgraphNodes":Ljava/util/Set;
    const/4 v3, 0x0

    .line 114
    .local v3, "lastNode":Lcom/vividsolutions/jts/geom/Coordinate;
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 115
    .local v0, "currNodes":Ljava/util/List;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    invoke-virtual {v5}, Lcom/vividsolutions/jts/geom/MultiLineString;->getNumGeometries()I

    move-result v10

    if-ge v2, v10, :cond_0

    .line 116
    invoke-virtual {v5, v2}, Lcom/vividsolutions/jts/geom/MultiLineString;->getGeometryN(I)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v4

    check-cast v4, Lcom/vividsolutions/jts/geom/LineString;

    .line 117
    .local v4, "line":Lcom/vividsolutions/jts/geom/LineString;
    invoke-virtual {v4, v9}, Lcom/vividsolutions/jts/geom/LineString;->getCoordinateN(I)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v7

    .line 118
    .local v7, "startNode":Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-virtual {v4}, Lcom/vividsolutions/jts/geom/LineString;->getNumPoints()I

    move-result v10

    add-int/lit8 v10, v10, -0x1

    invoke-virtual {v4, v10}, Lcom/vividsolutions/jts/geom/LineString;->getCoordinateN(I)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    .line 123
    .local v1, "endNode":Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-interface {v6, v7}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    move v8, v9

    goto :goto_0

    .line 124
    :cond_2
    invoke-interface {v6, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    move v8, v9

    goto :goto_0

    .line 126
    :cond_3
    if-eqz v3, :cond_4

    .line 127
    invoke-virtual {v7, v3}, Lcom/vividsolutions/jts/geom/Coordinate;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_4

    .line 129
    invoke-interface {v6, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 130
    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 133
    :cond_4
    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 134
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 135
    move-object v3, v1

    .line 115
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method private orient(Ljava/util/List;)Ljava/util/List;
    .locals 9
    .param p1, "seq"    # Ljava/util/List;

    .prologue
    const/4 v3, 0x0

    const/4 v7, 0x1

    .line 374
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vividsolutions/jts/planargraph/DirectedEdge;

    .line 375
    .local v5, "startEdge":Lcom/vividsolutions/jts/planargraph/DirectedEdge;
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    invoke-interface {p1, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/planargraph/DirectedEdge;

    .line 376
    .local v0, "endEdge":Lcom/vividsolutions/jts/planargraph/DirectedEdge;
    invoke-virtual {v5}, Lcom/vividsolutions/jts/planargraph/DirectedEdge;->getFromNode()Lcom/vividsolutions/jts/planargraph/Node;

    move-result-object v6

    .line 377
    .local v6, "startNode":Lcom/vividsolutions/jts/planargraph/Node;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/planargraph/DirectedEdge;->getToNode()Lcom/vividsolutions/jts/planargraph/Node;

    move-result-object v1

    .line 379
    .local v1, "endNode":Lcom/vividsolutions/jts/planargraph/Node;
    const/4 v2, 0x0

    .line 380
    .local v2, "flipSeq":Z
    invoke-virtual {v6}, Lcom/vividsolutions/jts/planargraph/Node;->getDegree()I

    move-result v8

    if-eq v8, v7, :cond_0

    invoke-virtual {v1}, Lcom/vividsolutions/jts/planargraph/Node;->getDegree()I

    move-result v8

    if-ne v8, v7, :cond_1

    :cond_0
    move v3, v7

    .line 383
    .local v3, "hasDegree1Node":Z
    :cond_1
    if-eqz v3, :cond_4

    .line 384
    const/4 v4, 0x0

    .line 388
    .local v4, "hasObviousStartNode":Z
    invoke-virtual {v0}, Lcom/vividsolutions/jts/planargraph/DirectedEdge;->getToNode()Lcom/vividsolutions/jts/planargraph/Node;

    move-result-object v8

    invoke-virtual {v8}, Lcom/vividsolutions/jts/planargraph/Node;->getDegree()I

    move-result v8

    if-ne v8, v7, :cond_2

    invoke-virtual {v0}, Lcom/vividsolutions/jts/planargraph/DirectedEdge;->getEdgeDirection()Z

    move-result v8

    if-nez v8, :cond_2

    .line 389
    const/4 v4, 0x1

    .line 390
    const/4 v2, 0x1

    .line 392
    :cond_2
    invoke-virtual {v5}, Lcom/vividsolutions/jts/planargraph/DirectedEdge;->getFromNode()Lcom/vividsolutions/jts/planargraph/Node;

    move-result-object v8

    invoke-virtual {v8}, Lcom/vividsolutions/jts/planargraph/Node;->getDegree()I

    move-result v8

    if-ne v8, v7, :cond_3

    invoke-virtual {v5}, Lcom/vividsolutions/jts/planargraph/DirectedEdge;->getEdgeDirection()Z

    move-result v8

    if-ne v8, v7, :cond_3

    .line 393
    const/4 v4, 0x1

    .line 394
    const/4 v2, 0x0

    .line 398
    :cond_3
    if-nez v4, :cond_4

    .line 400
    invoke-virtual {v5}, Lcom/vividsolutions/jts/planargraph/DirectedEdge;->getFromNode()Lcom/vividsolutions/jts/planargraph/Node;

    move-result-object v8

    invoke-virtual {v8}, Lcom/vividsolutions/jts/planargraph/Node;->getDegree()I

    move-result v8

    if-ne v8, v7, :cond_4

    .line 401
    const/4 v2, 0x1

    .line 411
    .end local v4    # "hasObviousStartNode":Z
    :cond_4
    if-eqz v2, :cond_5

    .line 412
    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/operation/linemerge/LineSequencer;->reverse(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    .line 413
    .end local p1    # "seq":Ljava/util/List;
    :cond_5
    return-object p1
.end method

.method private static reverse(Lcom/vividsolutions/jts/geom/LineString;)Lcom/vividsolutions/jts/geom/LineString;
    .locals 7
    .param p0, "line"    # Lcom/vividsolutions/jts/geom/LineString;

    .prologue
    .line 467
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/LineString;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v2

    .line 468
    .local v2, "pts":[Lcom/vividsolutions/jts/geom/Coordinate;
    array-length v4, v2

    new-array v3, v4, [Lcom/vividsolutions/jts/geom/Coordinate;

    .line 469
    .local v3, "revPts":[Lcom/vividsolutions/jts/geom/Coordinate;
    array-length v1, v2

    .line 470
    .local v1, "len":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 471
    add-int/lit8 v4, v1, -0x1

    sub-int/2addr v4, v0

    new-instance v5, Lcom/vividsolutions/jts/geom/Coordinate;

    aget-object v6, v2, v0

    invoke-direct {v5, v6}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(Lcom/vividsolutions/jts/geom/Coordinate;)V

    aput-object v5, v3, v4

    .line 470
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 473
    :cond_0
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/LineString;->getFactory()Lcom/vividsolutions/jts/geom/GeometryFactory;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createLineString([Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v4

    return-object v4
.end method

.method private reverse(Ljava/util/List;)Ljava/util/List;
    .locals 4
    .param p1, "seq"    # Ljava/util/List;

    .prologue
    .line 426
    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    .line 427
    .local v2, "newSeq":Ljava/util/LinkedList;
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 428
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/planargraph/DirectedEdge;

    .line 429
    .local v0, "de":Lcom/vividsolutions/jts/planargraph/DirectedEdge;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/planargraph/DirectedEdge;->getSym()Lcom/vividsolutions/jts/planargraph/DirectedEdge;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    goto :goto_0

    .line 431
    .end local v0    # "de":Lcom/vividsolutions/jts/planargraph/DirectedEdge;
    :cond_0
    return-object v2
.end method

.method public static sequence(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 2
    .param p0, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 88
    new-instance v0, Lcom/vividsolutions/jts/operation/linemerge/LineSequencer;

    invoke-direct {v0}, Lcom/vividsolutions/jts/operation/linemerge/LineSequencer;-><init>()V

    .line 89
    .local v0, "sequencer":Lcom/vividsolutions/jts/operation/linemerge/LineSequencer;
    invoke-virtual {v0, p0}, Lcom/vividsolutions/jts/operation/linemerge/LineSequencer;->add(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 90
    invoke-virtual {v0}, Lcom/vividsolutions/jts/operation/linemerge/LineSequencer;->getSequencedLineStrings()Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v1

    return-object v1
.end method


# virtual methods
.method public add(Lcom/vividsolutions/jts/geom/Geometry;)V
    .locals 1
    .param p1, "geometry"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 172
    new-instance v0, Lcom/vividsolutions/jts/operation/linemerge/LineSequencer$1;

    invoke-direct {v0, p0}, Lcom/vividsolutions/jts/operation/linemerge/LineSequencer$1;-><init>(Lcom/vividsolutions/jts/operation/linemerge/LineSequencer;)V

    invoke-virtual {p1, v0}, Lcom/vividsolutions/jts/geom/Geometry;->apply(Lcom/vividsolutions/jts/geom/GeometryComponentFilter;)V

    .line 179
    return-void
.end method

.method public add(Ljava/util/Collection;)V
    .locals 3
    .param p1, "geometries"    # Ljava/util/Collection;

    .prologue
    .line 158
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 159
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geom/Geometry;

    .line 160
    .local v0, "geometry":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-virtual {p0, v0}, Lcom/vividsolutions/jts/operation/linemerge/LineSequencer;->add(Lcom/vividsolutions/jts/geom/Geometry;)V

    goto :goto_0

    .line 162
    .end local v0    # "geometry":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_0
    return-void
.end method

.method public getSequencedLineStrings()Lcom/vividsolutions/jts/geom/Geometry;
    .locals 1

    .prologue
    .line 208
    invoke-direct {p0}, Lcom/vividsolutions/jts/operation/linemerge/LineSequencer;->computeSequence()V

    .line 209
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/linemerge/LineSequencer;->sequencedGeometry:Lcom/vividsolutions/jts/geom/Geometry;

    return-object v0
.end method

.method public isSequenceable()Z
    .locals 1

    .prologue
    .line 197
    invoke-direct {p0}, Lcom/vividsolutions/jts/operation/linemerge/LineSequencer;->computeSequence()V

    .line 198
    iget-boolean v0, p0, Lcom/vividsolutions/jts/operation/linemerge/LineSequencer;->isSequenceable:Z

    return v0
.end method
