.class Lcom/vividsolutions/jts/operation/distance/IndexedFacetDistance$FacetSequenceDistance;
.super Ljava/lang/Object;
.source "IndexedFacetDistance.java"

# interfaces
.implements Lcom/vividsolutions/jts/index/strtree/ItemDistance;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vividsolutions/jts/operation/distance/IndexedFacetDistance;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "FacetSequenceDistance"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 177
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/vividsolutions/jts/operation/distance/IndexedFacetDistance$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vividsolutions/jts/operation/distance/IndexedFacetDistance$1;

    .prologue
    .line 177
    invoke-direct {p0}, Lcom/vividsolutions/jts/operation/distance/IndexedFacetDistance$FacetSequenceDistance;-><init>()V

    return-void
.end method


# virtual methods
.method public distance(Lcom/vividsolutions/jts/index/strtree/ItemBoundable;Lcom/vividsolutions/jts/index/strtree/ItemBoundable;)D
    .locals 4
    .param p1, "item1"    # Lcom/vividsolutions/jts/index/strtree/ItemBoundable;
    .param p2, "item2"    # Lcom/vividsolutions/jts/index/strtree/ItemBoundable;

    .prologue
    .line 181
    invoke-virtual {p1}, Lcom/vividsolutions/jts/index/strtree/ItemBoundable;->getItem()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/operation/distance/FacetSequence;

    .line 182
    .local v0, "fs1":Lcom/vividsolutions/jts/operation/distance/FacetSequence;
    invoke-virtual {p2}, Lcom/vividsolutions/jts/index/strtree/ItemBoundable;->getItem()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vividsolutions/jts/operation/distance/FacetSequence;

    .line 183
    .local v1, "fs2":Lcom/vividsolutions/jts/operation/distance/FacetSequence;
    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/operation/distance/FacetSequence;->distance(Lcom/vividsolutions/jts/operation/distance/FacetSequence;)D

    move-result-wide v2

    return-wide v2
.end method
