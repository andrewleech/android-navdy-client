.class public Lcom/vividsolutions/jts/operation/distance/FacetSequenceTreeBuilder;
.super Ljava/lang/Object;
.source "FacetSequenceTreeBuilder.java"


# static fields
.field private static final FACET_SEQUENCE_SIZE:I = 0x6

.field private static final STR_TREE_NODE_CAPACITY:I = 0x4


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/vividsolutions/jts/geom/CoordinateSequence;Ljava/util/List;)V
    .locals 0
    .param p0, "x0"    # Lcom/vividsolutions/jts/geom/CoordinateSequence;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 48
    invoke-static {p0, p1}, Lcom/vividsolutions/jts/operation/distance/FacetSequenceTreeBuilder;->addFacetSequences(Lcom/vividsolutions/jts/geom/CoordinateSequence;Ljava/util/List;)V

    return-void
.end method

.method private static addFacetSequences(Lcom/vividsolutions/jts/geom/CoordinateSequence;Ljava/util/List;)V
    .locals 5
    .param p0, "pts"    # Lcom/vividsolutions/jts/geom/CoordinateSequence;
    .param p1, "sections"    # Ljava/util/List;

    .prologue
    .line 93
    const/4 v1, 0x0

    .line 94
    .local v1, "i":I
    invoke-interface {p0}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->size()I

    move-result v3

    .line 95
    .local v3, "size":I
    :goto_0
    add-int/lit8 v4, v3, -0x1

    if-gt v1, v4, :cond_1

    .line 96
    add-int/lit8 v4, v1, 0x6

    add-int/lit8 v0, v4, 0x1

    .line 99
    .local v0, "end":I
    add-int/lit8 v4, v3, -0x1

    if-lt v0, v4, :cond_0

    .line 100
    move v0, v3

    .line 101
    :cond_0
    new-instance v2, Lcom/vividsolutions/jts/operation/distance/FacetSequence;

    invoke-direct {v2, p0, v1, v0}, Lcom/vividsolutions/jts/operation/distance/FacetSequence;-><init>(Lcom/vividsolutions/jts/geom/CoordinateSequence;II)V

    .line 102
    .local v2, "sect":Lcom/vividsolutions/jts/operation/distance/FacetSequence;
    invoke-interface {p1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 103
    add-int/lit8 v1, v1, 0x6

    .line 104
    goto :goto_0

    .line 105
    .end local v0    # "end":I
    .end local v2    # "sect":Lcom/vividsolutions/jts/operation/distance/FacetSequence;
    :cond_1
    return-void
.end method

.method public static build(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/index/strtree/STRtree;
    .locals 5
    .param p0, "g"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 56
    new-instance v3, Lcom/vividsolutions/jts/index/strtree/STRtree;

    const/4 v4, 0x4

    invoke-direct {v3, v4}, Lcom/vividsolutions/jts/index/strtree/STRtree;-><init>(I)V

    .line 57
    .local v3, "tree":Lcom/vividsolutions/jts/index/strtree/STRtree;
    invoke-static {p0}, Lcom/vividsolutions/jts/operation/distance/FacetSequenceTreeBuilder;->computeFacetSequences(Lcom/vividsolutions/jts/geom/Geometry;)Ljava/util/List;

    move-result-object v2

    .line 58
    .local v2, "sections":Ljava/util/List;
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 59
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vividsolutions/jts/operation/distance/FacetSequence;

    .line 60
    .local v1, "section":Lcom/vividsolutions/jts/operation/distance/FacetSequence;
    invoke-virtual {v1}, Lcom/vividsolutions/jts/operation/distance/FacetSequence;->getEnvelope()Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v4

    invoke-virtual {v3, v4, v1}, Lcom/vividsolutions/jts/index/strtree/STRtree;->insert(Lcom/vividsolutions/jts/geom/Envelope;Ljava/lang/Object;)V

    goto :goto_0

    .line 62
    .end local v1    # "section":Lcom/vividsolutions/jts/operation/distance/FacetSequence;
    :cond_0
    invoke-virtual {v3}, Lcom/vividsolutions/jts/index/strtree/STRtree;->build()V

    .line 63
    return-object v3
.end method

.method private static computeFacetSequences(Lcom/vividsolutions/jts/geom/Geometry;)Ljava/util/List;
    .locals 2
    .param p0, "g"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 73
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 75
    .local v0, "sections":Ljava/util/List;
    new-instance v1, Lcom/vividsolutions/jts/operation/distance/FacetSequenceTreeBuilder$1;

    invoke-direct {v1, v0}, Lcom/vividsolutions/jts/operation/distance/FacetSequenceTreeBuilder$1;-><init>(Ljava/util/List;)V

    invoke-virtual {p0, v1}, Lcom/vividsolutions/jts/geom/Geometry;->apply(Lcom/vividsolutions/jts/geom/GeometryComponentFilter;)V

    .line 89
    return-object v0
.end method
