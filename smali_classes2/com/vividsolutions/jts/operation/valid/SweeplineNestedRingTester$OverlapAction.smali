.class Lcom/vividsolutions/jts/operation/valid/SweeplineNestedRingTester$OverlapAction;
.super Ljava/lang/Object;
.source "SweeplineNestedRingTester.java"

# interfaces
.implements Lcom/vividsolutions/jts/index/sweepline/SweepLineOverlapAction;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vividsolutions/jts/operation/valid/SweeplineNestedRingTester;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "OverlapAction"
.end annotation


# instance fields
.field isNonNested:Z

.field final synthetic this$0:Lcom/vividsolutions/jts/operation/valid/SweeplineNestedRingTester;


# direct methods
.method constructor <init>(Lcom/vividsolutions/jts/operation/valid/SweeplineNestedRingTester;)V
    .locals 1

    .prologue
    .line 113
    iput-object p1, p0, Lcom/vividsolutions/jts/operation/valid/SweeplineNestedRingTester$OverlapAction;->this$0:Lcom/vividsolutions/jts/operation/valid/SweeplineNestedRingTester;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 116
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vividsolutions/jts/operation/valid/SweeplineNestedRingTester$OverlapAction;->isNonNested:Z

    return-void
.end method


# virtual methods
.method public overlap(Lcom/vividsolutions/jts/index/sweepline/SweepLineInterval;Lcom/vividsolutions/jts/index/sweepline/SweepLineInterval;)V
    .locals 3
    .param p1, "s0"    # Lcom/vividsolutions/jts/index/sweepline/SweepLineInterval;
    .param p2, "s1"    # Lcom/vividsolutions/jts/index/sweepline/SweepLineInterval;

    .prologue
    .line 120
    invoke-virtual {p1}, Lcom/vividsolutions/jts/index/sweepline/SweepLineInterval;->getItem()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geom/LinearRing;

    .line 121
    .local v0, "innerRing":Lcom/vividsolutions/jts/geom/LinearRing;
    invoke-virtual {p2}, Lcom/vividsolutions/jts/index/sweepline/SweepLineInterval;->getItem()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vividsolutions/jts/geom/LinearRing;

    .line 122
    .local v1, "searchRing":Lcom/vividsolutions/jts/geom/LinearRing;
    if-ne v0, v1, :cond_1

    .line 126
    :cond_0
    :goto_0
    return-void

    .line 124
    :cond_1
    iget-object v2, p0, Lcom/vividsolutions/jts/operation/valid/SweeplineNestedRingTester$OverlapAction;->this$0:Lcom/vividsolutions/jts/operation/valid/SweeplineNestedRingTester;

    invoke-static {v2, v0, v1}, Lcom/vividsolutions/jts/operation/valid/SweeplineNestedRingTester;->access$000(Lcom/vividsolutions/jts/operation/valid/SweeplineNestedRingTester;Lcom/vividsolutions/jts/geom/LinearRing;Lcom/vividsolutions/jts/geom/LinearRing;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 125
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/vividsolutions/jts/operation/valid/SweeplineNestedRingTester$OverlapAction;->isNonNested:Z

    goto :goto_0
.end method
