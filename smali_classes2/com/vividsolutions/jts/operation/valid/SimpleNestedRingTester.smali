.class public Lcom/vividsolutions/jts/operation/valid/SimpleNestedRingTester;
.super Ljava/lang/Object;
.source "SimpleNestedRingTester.java"


# instance fields
.field private graph:Lcom/vividsolutions/jts/geomgraph/GeometryGraph;

.field private nestedPt:Lcom/vividsolutions/jts/geom/Coordinate;

.field private rings:Ljava/util/List;


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geomgraph/GeometryGraph;)V
    .locals 1
    .param p1, "graph"    # Lcom/vividsolutions/jts/geomgraph/GeometryGraph;

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/valid/SimpleNestedRingTester;->rings:Ljava/util/List;

    .line 58
    iput-object p1, p0, Lcom/vividsolutions/jts/operation/valid/SimpleNestedRingTester;->graph:Lcom/vividsolutions/jts/geomgraph/GeometryGraph;

    .line 59
    return-void
.end method


# virtual methods
.method public add(Lcom/vividsolutions/jts/geom/LinearRing;)V
    .locals 1
    .param p1, "ring"    # Lcom/vividsolutions/jts/geom/LinearRing;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/valid/SimpleNestedRingTester;->rings:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 64
    return-void
.end method

.method public getNestedPoint()Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/valid/SimpleNestedRingTester;->nestedPt:Lcom/vividsolutions/jts/geom/Coordinate;

    return-object v0
.end method

.method public isNonNested()Z
    .locals 12

    .prologue
    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 70
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v8, p0, Lcom/vividsolutions/jts/operation/valid/SimpleNestedRingTester;->rings:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    if-ge v0, v8, :cond_4

    .line 71
    iget-object v8, p0, Lcom/vividsolutions/jts/operation/valid/SimpleNestedRingTester;->rings:Ljava/util/List;

    invoke-interface {v8, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vividsolutions/jts/geom/LinearRing;

    .line 72
    .local v1, "innerRing":Lcom/vividsolutions/jts/geom/LinearRing;
    invoke-virtual {v1}, Lcom/vividsolutions/jts/geom/LinearRing;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v3

    .line 74
    .local v3, "innerRingPts":[Lcom/vividsolutions/jts/geom/Coordinate;
    const/4 v5, 0x0

    .local v5, "j":I
    :goto_1
    iget-object v8, p0, Lcom/vividsolutions/jts/operation/valid/SimpleNestedRingTester;->rings:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    if-ge v5, v8, :cond_3

    .line 75
    iget-object v8, p0, Lcom/vividsolutions/jts/operation/valid/SimpleNestedRingTester;->rings:Ljava/util/List;

    invoke-interface {v8, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/vividsolutions/jts/geom/LinearRing;

    .line 76
    .local v6, "searchRing":Lcom/vividsolutions/jts/geom/LinearRing;
    invoke-virtual {v6}, Lcom/vividsolutions/jts/geom/LinearRing;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v7

    .line 78
    .local v7, "searchRingPts":[Lcom/vividsolutions/jts/geom/Coordinate;
    if-ne v1, v6, :cond_1

    .line 74
    :cond_0
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 81
    :cond_1
    invoke-virtual {v1}, Lcom/vividsolutions/jts/geom/LinearRing;->getEnvelopeInternal()Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v8

    invoke-virtual {v6}, Lcom/vividsolutions/jts/geom/LinearRing;->getEnvelopeInternal()Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v11

    invoke-virtual {v8, v11}, Lcom/vividsolutions/jts/geom/Envelope;->intersects(Lcom/vividsolutions/jts/geom/Envelope;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 84
    iget-object v8, p0, Lcom/vividsolutions/jts/operation/valid/SimpleNestedRingTester;->graph:Lcom/vividsolutions/jts/geomgraph/GeometryGraph;

    invoke-static {v3, v6, v8}, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->findPtNotNode([Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/LinearRing;Lcom/vividsolutions/jts/geomgraph/GeometryGraph;)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v2

    .line 85
    .local v2, "innerRingPt":Lcom/vividsolutions/jts/geom/Coordinate;
    if-eqz v2, :cond_2

    move v8, v9

    :goto_2
    const-string v11, "Unable to find a ring point not a node of the search ring"

    invoke-static {v8, v11}, Lcom/vividsolutions/jts/util/Assert;->isTrue(ZLjava/lang/String;)V

    .line 88
    invoke-static {v2, v7}, Lcom/vividsolutions/jts/algorithm/CGAlgorithms;->isPointInRing(Lcom/vividsolutions/jts/geom/Coordinate;[Lcom/vividsolutions/jts/geom/Coordinate;)Z

    move-result v4

    .line 89
    .local v4, "isInside":Z
    if-eqz v4, :cond_0

    .line 90
    iput-object v2, p0, Lcom/vividsolutions/jts/operation/valid/SimpleNestedRingTester;->nestedPt:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 95
    .end local v1    # "innerRing":Lcom/vividsolutions/jts/geom/LinearRing;
    .end local v2    # "innerRingPt":Lcom/vividsolutions/jts/geom/Coordinate;
    .end local v3    # "innerRingPts":[Lcom/vividsolutions/jts/geom/Coordinate;
    .end local v4    # "isInside":Z
    .end local v5    # "j":I
    .end local v6    # "searchRing":Lcom/vividsolutions/jts/geom/LinearRing;
    .end local v7    # "searchRingPts":[Lcom/vividsolutions/jts/geom/Coordinate;
    :goto_3
    return v10

    .restart local v1    # "innerRing":Lcom/vividsolutions/jts/geom/LinearRing;
    .restart local v2    # "innerRingPt":Lcom/vividsolutions/jts/geom/Coordinate;
    .restart local v3    # "innerRingPts":[Lcom/vividsolutions/jts/geom/Coordinate;
    .restart local v5    # "j":I
    .restart local v6    # "searchRing":Lcom/vividsolutions/jts/geom/LinearRing;
    .restart local v7    # "searchRingPts":[Lcom/vividsolutions/jts/geom/Coordinate;
    :cond_2
    move v8, v10

    .line 85
    goto :goto_2

    .line 70
    .end local v2    # "innerRingPt":Lcom/vividsolutions/jts/geom/Coordinate;
    .end local v6    # "searchRing":Lcom/vividsolutions/jts/geom/LinearRing;
    .end local v7    # "searchRingPts":[Lcom/vividsolutions/jts/geom/Coordinate;
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .end local v1    # "innerRing":Lcom/vividsolutions/jts/geom/LinearRing;
    .end local v3    # "innerRingPts":[Lcom/vividsolutions/jts/geom/Coordinate;
    .end local v5    # "j":I
    :cond_4
    move v10, v9

    .line 95
    goto :goto_3
.end method
