.class public Lcom/vividsolutions/jts/operation/union/PointGeometryUnion;
.super Ljava/lang/Object;
.source "PointGeometryUnion.java"


# instance fields
.field private geomFact:Lcom/vividsolutions/jts/geom/GeometryFactory;

.field private otherGeom:Lcom/vividsolutions/jts/geom/Geometry;

.field private pointGeom:Lcom/vividsolutions/jts/geom/Geometry;


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geom/Puntal;Lcom/vividsolutions/jts/geom/Geometry;)V
    .locals 1
    .param p1, "pointGeom"    # Lcom/vividsolutions/jts/geom/Puntal;
    .param p2, "otherGeom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    check-cast p1, Lcom/vividsolutions/jts/geom/Geometry;

    .end local p1    # "pointGeom":Lcom/vividsolutions/jts/geom/Puntal;
    iput-object p1, p0, Lcom/vividsolutions/jts/operation/union/PointGeometryUnion;->pointGeom:Lcom/vividsolutions/jts/geom/Geometry;

    .line 64
    iput-object p2, p0, Lcom/vividsolutions/jts/operation/union/PointGeometryUnion;->otherGeom:Lcom/vividsolutions/jts/geom/Geometry;

    .line 65
    invoke-virtual {p2}, Lcom/vividsolutions/jts/geom/Geometry;->getFactory()Lcom/vividsolutions/jts/geom/GeometryFactory;

    move-result-object v0

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/union/PointGeometryUnion;->geomFact:Lcom/vividsolutions/jts/geom/GeometryFactory;

    .line 66
    return-void
.end method

.method public static union(Lcom/vividsolutions/jts/geom/Puntal;Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 2
    .param p0, "pointGeom"    # Lcom/vividsolutions/jts/geom/Puntal;
    .param p1, "otherGeom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 53
    new-instance v0, Lcom/vividsolutions/jts/operation/union/PointGeometryUnion;

    invoke-direct {v0, p0, p1}, Lcom/vividsolutions/jts/operation/union/PointGeometryUnion;-><init>(Lcom/vividsolutions/jts/geom/Puntal;Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 54
    .local v0, "unioner":Lcom/vividsolutions/jts/operation/union/PointGeometryUnion;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/operation/union/PointGeometryUnion;->union()Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v1

    return-object v1
.end method


# virtual methods
.method public union()Lcom/vividsolutions/jts/geom/Geometry;
    .locals 10

    .prologue
    .line 70
    new-instance v5, Lcom/vividsolutions/jts/algorithm/PointLocator;

    invoke-direct {v5}, Lcom/vividsolutions/jts/algorithm/PointLocator;-><init>()V

    .line 72
    .local v5, "locater":Lcom/vividsolutions/jts/algorithm/PointLocator;
    new-instance v2, Ljava/util/TreeSet;

    invoke-direct {v2}, Ljava/util/TreeSet;-><init>()V

    .line 74
    .local v2, "exteriorCoords":Ljava/util/Set;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget-object v8, p0, Lcom/vividsolutions/jts/operation/union/PointGeometryUnion;->pointGeom:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-virtual {v8}, Lcom/vividsolutions/jts/geom/Geometry;->getNumGeometries()I

    move-result v8

    if-ge v3, v8, :cond_1

    .line 75
    iget-object v8, p0, Lcom/vividsolutions/jts/operation/union/PointGeometryUnion;->pointGeom:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-virtual {v8, v3}, Lcom/vividsolutions/jts/geom/Geometry;->getGeometryN(I)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v6

    check-cast v6, Lcom/vividsolutions/jts/geom/Point;

    .line 76
    .local v6, "point":Lcom/vividsolutions/jts/geom/Point;
    invoke-virtual {v6}, Lcom/vividsolutions/jts/geom/Point;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    .line 77
    .local v0, "coord":Lcom/vividsolutions/jts/geom/Coordinate;
    iget-object v8, p0, Lcom/vividsolutions/jts/operation/union/PointGeometryUnion;->otherGeom:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-virtual {v5, v0, v8}, Lcom/vividsolutions/jts/algorithm/PointLocator;->locate(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Geometry;)I

    move-result v4

    .line 78
    .local v4, "loc":I
    const/4 v8, 0x2

    if-ne v4, v8, :cond_0

    .line 79
    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 74
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 83
    .end local v0    # "coord":Lcom/vividsolutions/jts/geom/Coordinate;
    .end local v4    # "loc":I
    .end local v6    # "point":Lcom/vividsolutions/jts/geom/Point;
    :cond_1
    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v8

    if-nez v8, :cond_2

    .line 84
    iget-object v8, p0, Lcom/vividsolutions/jts/operation/union/PointGeometryUnion;->otherGeom:Lcom/vividsolutions/jts/geom/Geometry;

    .line 97
    :goto_1
    return-object v8

    .line 87
    :cond_2
    const/4 v7, 0x0

    .line 88
    .local v7, "ptComp":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-static {v2}, Lcom/vividsolutions/jts/geom/CoordinateArrays;->toCoordinateArray(Ljava/util/Collection;)[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    .line 89
    .local v1, "coords":[Lcom/vividsolutions/jts/geom/Coordinate;
    array-length v8, v1

    const/4 v9, 0x1

    if-ne v8, v9, :cond_3

    .line 90
    iget-object v8, p0, Lcom/vividsolutions/jts/operation/union/PointGeometryUnion;->geomFact:Lcom/vividsolutions/jts/geom/GeometryFactory;

    const/4 v9, 0x0

    aget-object v9, v1, v9

    invoke-virtual {v8, v9}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createPoint(Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/Point;

    move-result-object v7

    .line 97
    :goto_2
    iget-object v8, p0, Lcom/vividsolutions/jts/operation/union/PointGeometryUnion;->otherGeom:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-static {v7, v8}, Lcom/vividsolutions/jts/geom/util/GeometryCombiner;->combine(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v8

    goto :goto_1

    .line 93
    :cond_3
    iget-object v8, p0, Lcom/vividsolutions/jts/operation/union/PointGeometryUnion;->geomFact:Lcom/vividsolutions/jts/geom/GeometryFactory;

    invoke-virtual {v8, v1}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createMultiPoint([Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/MultiPoint;

    move-result-object v7

    goto :goto_2
.end method
