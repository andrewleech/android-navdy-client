.class public abstract Lcom/vividsolutions/jts/planargraph/PlanarGraph;
.super Ljava/lang/Object;
.source "PlanarGraph.java"


# instance fields
.field protected dirEdges:Ljava/util/Set;

.field protected edges:Ljava/util/Set;

.field protected nodeMap:Lcom/vividsolutions/jts/planargraph/NodeMap;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/planargraph/PlanarGraph;->edges:Ljava/util/Set;

    .line 56
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/planargraph/PlanarGraph;->dirEdges:Ljava/util/Set;

    .line 57
    new-instance v0, Lcom/vividsolutions/jts/planargraph/NodeMap;

    invoke-direct {v0}, Lcom/vividsolutions/jts/planargraph/NodeMap;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/planargraph/PlanarGraph;->nodeMap:Lcom/vividsolutions/jts/planargraph/NodeMap;

    .line 64
    return-void
.end method


# virtual methods
.method protected add(Lcom/vividsolutions/jts/planargraph/DirectedEdge;)V
    .locals 1
    .param p1, "dirEdge"    # Lcom/vividsolutions/jts/planargraph/DirectedEdge;

    .prologue
    .line 108
    iget-object v0, p0, Lcom/vividsolutions/jts/planargraph/PlanarGraph;->dirEdges:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 109
    return-void
.end method

.method protected add(Lcom/vividsolutions/jts/planargraph/Edge;)V
    .locals 1
    .param p1, "edge"    # Lcom/vividsolutions/jts/planargraph/Edge;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/vividsolutions/jts/planargraph/PlanarGraph;->edges:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 98
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/vividsolutions/jts/planargraph/Edge;->getDirEdge(I)Lcom/vividsolutions/jts/planargraph/DirectedEdge;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vividsolutions/jts/planargraph/PlanarGraph;->add(Lcom/vividsolutions/jts/planargraph/DirectedEdge;)V

    .line 99
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/vividsolutions/jts/planargraph/Edge;->getDirEdge(I)Lcom/vividsolutions/jts/planargraph/DirectedEdge;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vividsolutions/jts/planargraph/PlanarGraph;->add(Lcom/vividsolutions/jts/planargraph/DirectedEdge;)V

    .line 100
    return-void
.end method

.method protected add(Lcom/vividsolutions/jts/planargraph/Node;)V
    .locals 1
    .param p1, "node"    # Lcom/vividsolutions/jts/planargraph/Node;

    .prologue
    .line 87
    iget-object v0, p0, Lcom/vividsolutions/jts/planargraph/PlanarGraph;->nodeMap:Lcom/vividsolutions/jts/planargraph/NodeMap;

    invoke-virtual {v0, p1}, Lcom/vividsolutions/jts/planargraph/NodeMap;->add(Lcom/vividsolutions/jts/planargraph/Node;)Lcom/vividsolutions/jts/planargraph/Node;

    .line 88
    return-void
.end method

.method public contains(Lcom/vividsolutions/jts/planargraph/DirectedEdge;)Z
    .locals 1
    .param p1, "de"    # Lcom/vividsolutions/jts/planargraph/DirectedEdge;

    .prologue
    .line 137
    iget-object v0, p0, Lcom/vividsolutions/jts/planargraph/PlanarGraph;->dirEdges:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public contains(Lcom/vividsolutions/jts/planargraph/Edge;)Z
    .locals 1
    .param p1, "e"    # Lcom/vividsolutions/jts/planargraph/Edge;

    .prologue
    .line 126
    iget-object v0, p0, Lcom/vividsolutions/jts/planargraph/PlanarGraph;->edges:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public dirEdgeIterator()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/vividsolutions/jts/planargraph/PlanarGraph;->dirEdges:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public edgeIterator()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lcom/vividsolutions/jts/planargraph/PlanarGraph;->edges:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public findNode(Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/planargraph/Node;
    .locals 1
    .param p1, "pt"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/vividsolutions/jts/planargraph/PlanarGraph;->nodeMap:Lcom/vividsolutions/jts/planargraph/NodeMap;

    invoke-virtual {v0, p1}, Lcom/vividsolutions/jts/planargraph/NodeMap;->find(Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/planargraph/Node;

    move-result-object v0

    return-object v0
.end method

.method public findNodesOfDegree(I)Ljava/util/List;
    .locals 4
    .param p1, "degree"    # I

    .prologue
    .line 226
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 227
    .local v2, "nodesFound":Ljava/util/List;
    invoke-virtual {p0}, Lcom/vividsolutions/jts/planargraph/PlanarGraph;->nodeIterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 228
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vividsolutions/jts/planargraph/Node;

    .line 229
    .local v1, "node":Lcom/vividsolutions/jts/planargraph/Node;
    invoke-virtual {v1}, Lcom/vividsolutions/jts/planargraph/Node;->getDegree()I

    move-result v3

    if-ne v3, p1, :cond_0

    .line 230
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 232
    .end local v1    # "node":Lcom/vividsolutions/jts/planargraph/Node;
    :cond_1
    return-object v2
.end method

.method public getEdges()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/vividsolutions/jts/planargraph/PlanarGraph;->edges:Ljava/util/Set;

    return-object v0
.end method

.method public getNodes()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/vividsolutions/jts/planargraph/PlanarGraph;->nodeMap:Lcom/vividsolutions/jts/planargraph/NodeMap;

    invoke-virtual {v0}, Lcom/vividsolutions/jts/planargraph/NodeMap;->values()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public nodeIterator()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/vividsolutions/jts/planargraph/PlanarGraph;->nodeMap:Lcom/vividsolutions/jts/planargraph/NodeMap;

    invoke-virtual {v0}, Lcom/vividsolutions/jts/planargraph/NodeMap;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public remove(Lcom/vividsolutions/jts/planargraph/DirectedEdge;)V
    .locals 2
    .param p1, "de"    # Lcom/vividsolutions/jts/planargraph/DirectedEdge;

    .prologue
    .line 186
    invoke-virtual {p1}, Lcom/vividsolutions/jts/planargraph/DirectedEdge;->getSym()Lcom/vividsolutions/jts/planargraph/DirectedEdge;

    move-result-object v0

    .line 187
    .local v0, "sym":Lcom/vividsolutions/jts/planargraph/DirectedEdge;
    if-eqz v0, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/planargraph/DirectedEdge;->setSym(Lcom/vividsolutions/jts/planargraph/DirectedEdge;)V

    .line 189
    :cond_0
    invoke-virtual {p1}, Lcom/vividsolutions/jts/planargraph/DirectedEdge;->getFromNode()Lcom/vividsolutions/jts/planargraph/Node;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/vividsolutions/jts/planargraph/Node;->remove(Lcom/vividsolutions/jts/planargraph/DirectedEdge;)V

    .line 190
    invoke-virtual {p1}, Lcom/vividsolutions/jts/planargraph/DirectedEdge;->remove()V

    .line 191
    iget-object v1, p0, Lcom/vividsolutions/jts/planargraph/PlanarGraph;->dirEdges:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 192
    return-void
.end method

.method public remove(Lcom/vividsolutions/jts/planargraph/Edge;)V
    .locals 1
    .param p1, "edge"    # Lcom/vividsolutions/jts/planargraph/Edge;

    .prologue
    .line 173
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/vividsolutions/jts/planargraph/Edge;->getDirEdge(I)Lcom/vividsolutions/jts/planargraph/DirectedEdge;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vividsolutions/jts/planargraph/PlanarGraph;->remove(Lcom/vividsolutions/jts/planargraph/DirectedEdge;)V

    .line 174
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/vividsolutions/jts/planargraph/Edge;->getDirEdge(I)Lcom/vividsolutions/jts/planargraph/DirectedEdge;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vividsolutions/jts/planargraph/PlanarGraph;->remove(Lcom/vividsolutions/jts/planargraph/DirectedEdge;)V

    .line 175
    iget-object v0, p0, Lcom/vividsolutions/jts/planargraph/PlanarGraph;->edges:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 176
    invoke-virtual {p1}, Lcom/vividsolutions/jts/planargraph/Edge;->remove()V

    .line 177
    return-void
.end method

.method public remove(Lcom/vividsolutions/jts/planargraph/Node;)V
    .locals 7
    .param p1, "node"    # Lcom/vividsolutions/jts/planargraph/Node;

    .prologue
    .line 201
    invoke-virtual {p1}, Lcom/vividsolutions/jts/planargraph/Node;->getOutEdges()Lcom/vividsolutions/jts/planargraph/DirectedEdgeStar;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vividsolutions/jts/planargraph/DirectedEdgeStar;->getEdges()Ljava/util/List;

    move-result-object v3

    .line 202
    .local v3, "outEdges":Ljava/util/List;
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 203
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/planargraph/DirectedEdge;

    .line 204
    .local v0, "de":Lcom/vividsolutions/jts/planargraph/DirectedEdge;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/planargraph/DirectedEdge;->getSym()Lcom/vividsolutions/jts/planargraph/DirectedEdge;

    move-result-object v4

    .line 206
    .local v4, "sym":Lcom/vividsolutions/jts/planargraph/DirectedEdge;
    if-eqz v4, :cond_1

    invoke-virtual {p0, v4}, Lcom/vividsolutions/jts/planargraph/PlanarGraph;->remove(Lcom/vividsolutions/jts/planargraph/DirectedEdge;)V

    .line 208
    :cond_1
    iget-object v5, p0, Lcom/vividsolutions/jts/planargraph/PlanarGraph;->dirEdges:Ljava/util/Set;

    invoke-interface {v5, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 210
    invoke-virtual {v0}, Lcom/vividsolutions/jts/planargraph/DirectedEdge;->getEdge()Lcom/vividsolutions/jts/planargraph/Edge;

    move-result-object v1

    .line 211
    .local v1, "edge":Lcom/vividsolutions/jts/planargraph/Edge;
    if-eqz v1, :cond_0

    .line 212
    iget-object v5, p0, Lcom/vividsolutions/jts/planargraph/PlanarGraph;->edges:Ljava/util/Set;

    invoke-interface {v5, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 217
    .end local v0    # "de":Lcom/vividsolutions/jts/planargraph/DirectedEdge;
    .end local v1    # "edge":Lcom/vividsolutions/jts/planargraph/Edge;
    .end local v4    # "sym":Lcom/vividsolutions/jts/planargraph/DirectedEdge;
    :cond_2
    iget-object v5, p0, Lcom/vividsolutions/jts/planargraph/PlanarGraph;->nodeMap:Lcom/vividsolutions/jts/planargraph/NodeMap;

    invoke-virtual {p1}, Lcom/vividsolutions/jts/planargraph/Node;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/vividsolutions/jts/planargraph/NodeMap;->remove(Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/planargraph/Node;

    .line 218
    invoke-virtual {p1}, Lcom/vividsolutions/jts/planargraph/Node;->remove()V

    .line 219
    return-void
.end method
