.class public Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeTriangle;
.super Ljava/lang/Object;
.source "QuadEdgeTriangle.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeTriangle$QuadEdgeTriangleBuilderVisitor;
    }
.end annotation


# instance fields
.field private data:Ljava/lang/Object;

.field private edge:[Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;


# direct methods
.method public constructor <init>([Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;)V
    .locals 2
    .param p1, "edge"    # [Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    .prologue
    .line 154
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 155
    invoke-virtual {p1}, [Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    check-cast v1, [Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    iput-object v1, p0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeTriangle;->edge:[Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    .line 157
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v1, 0x3

    if-ge v0, v1, :cond_0

    .line 158
    aget-object v1, p1, v0

    invoke-virtual {v1, p0}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->setData(Ljava/lang/Object;)V

    .line 157
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 160
    :cond_0
    return-void
.end method

.method public static contains([Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;Lcom/vividsolutions/jts/geom/Coordinate;)Z
    .locals 5
    .param p0, "tri"    # [Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    .param p1, "pt"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 110
    const/4 v1, 0x4

    new-array v0, v1, [Lcom/vividsolutions/jts/geom/Coordinate;

    aget-object v1, p0, v2

    invoke-virtual {v1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->orig()Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    aput-object v1, v0, v2

    aget-object v1, p0, v3

    invoke-virtual {v1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->orig()Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    aput-object v1, v0, v3

    aget-object v1, p0, v4

    invoke-virtual {v1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->orig()Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    aput-object v1, v0, v4

    const/4 v1, 0x3

    aget-object v2, p0, v2

    invoke-virtual {v2}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->orig()Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v2

    aput-object v2, v0, v1

    .line 113
    .local v0, "ring":[Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-static {p1, v0}, Lcom/vividsolutions/jts/algorithm/CGAlgorithms;->isPointInRing(Lcom/vividsolutions/jts/geom/Coordinate;[Lcom/vividsolutions/jts/geom/Coordinate;)Z

    move-result v1

    return v1
.end method

.method public static contains([Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;Lcom/vividsolutions/jts/geom/Coordinate;)Z
    .locals 5
    .param p0, "tri"    # [Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;
    .param p1, "pt"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 94
    const/4 v1, 0x4

    new-array v0, v1, [Lcom/vividsolutions/jts/geom/Coordinate;

    aget-object v1, p0, v2

    invoke-virtual {v1}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    aput-object v1, v0, v2

    aget-object v1, p0, v3

    invoke-virtual {v1}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    aput-object v1, v0, v3

    aget-object v1, p0, v4

    invoke-virtual {v1}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    aput-object v1, v0, v4

    const/4 v1, 0x3

    aget-object v2, p0, v2

    invoke-virtual {v2}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v2

    aput-object v2, v0, v1

    .line 96
    .local v0, "ring":[Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-static {p1, v0}, Lcom/vividsolutions/jts/algorithm/CGAlgorithms;->isPointInRing(Lcom/vividsolutions/jts/geom/Coordinate;[Lcom/vividsolutions/jts/geom/Coordinate;)Z

    move-result v1

    return v1
.end method

.method public static createOn(Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;)Ljava/util/List;
    .locals 2
    .param p0, "subdiv"    # Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;

    .prologue
    .line 78
    new-instance v0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeTriangle$QuadEdgeTriangleBuilderVisitor;

    invoke-direct {v0}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeTriangle$QuadEdgeTriangleBuilderVisitor;-><init>()V

    .line 79
    .local v0, "visitor":Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeTriangle$QuadEdgeTriangleBuilderVisitor;
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->visitTriangles(Lcom/vividsolutions/jts/triangulate/quadedge/TriangleVisitor;Z)V

    .line 80
    invoke-virtual {v0}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeTriangle$QuadEdgeTriangleBuilderVisitor;->getTriangles()Ljava/util/List;

    move-result-object v1

    return-object v1
.end method

.method public static nextIndex(I)I
    .locals 1
    .param p0, "index"    # I

    .prologue
    .line 143
    add-int/lit8 v0, p0, 0x1

    rem-int/lit8 p0, v0, 0x3

    return p0
.end method

.method public static toPolygon([Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 8
    .param p0, "e"    # [Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 126
    const/4 v4, 0x4

    new-array v2, v4, [Lcom/vividsolutions/jts/geom/Coordinate;

    aget-object v4, p0, v5

    invoke-virtual {v4}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->orig()Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v4

    aput-object v4, v2, v5

    aget-object v4, p0, v6

    invoke-virtual {v4}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->orig()Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v4

    aput-object v4, v2, v6

    aget-object v4, p0, v7

    invoke-virtual {v4}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->orig()Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v4

    aput-object v4, v2, v7

    const/4 v4, 0x3

    aget-object v5, p0, v5

    invoke-virtual {v5}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->orig()Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v5

    aput-object v5, v2, v4

    .line 129
    .local v2, "ringPts":[Lcom/vividsolutions/jts/geom/Coordinate;
    new-instance v0, Lcom/vividsolutions/jts/geom/GeometryFactory;

    invoke-direct {v0}, Lcom/vividsolutions/jts/geom/GeometryFactory;-><init>()V

    .line 130
    .local v0, "fact":Lcom/vividsolutions/jts/geom/GeometryFactory;
    invoke-virtual {v0, v2}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createLinearRing([Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/LinearRing;

    move-result-object v1

    .line 131
    .local v1, "ring":Lcom/vividsolutions/jts/geom/LinearRing;
    const/4 v4, 0x0

    invoke-virtual {v0, v1, v4}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createPolygon(Lcom/vividsolutions/jts/geom/LinearRing;[Lcom/vividsolutions/jts/geom/LinearRing;)Lcom/vividsolutions/jts/geom/Polygon;

    move-result-object v3

    .line 132
    .local v3, "tri":Lcom/vividsolutions/jts/geom/Polygon;
    return-object v3
.end method

.method public static toPolygon([Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 8
    .param p0, "v"    # [Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 117
    const/4 v4, 0x4

    new-array v2, v4, [Lcom/vividsolutions/jts/geom/Coordinate;

    aget-object v4, p0, v5

    invoke-virtual {v4}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v4

    aput-object v4, v2, v5

    aget-object v4, p0, v6

    invoke-virtual {v4}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v4

    aput-object v4, v2, v6

    aget-object v4, p0, v7

    invoke-virtual {v4}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v4

    aput-object v4, v2, v7

    const/4 v4, 0x3

    aget-object v5, p0, v5

    invoke-virtual {v5}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v5

    aput-object v5, v2, v4

    .line 119
    .local v2, "ringPts":[Lcom/vividsolutions/jts/geom/Coordinate;
    new-instance v0, Lcom/vividsolutions/jts/geom/GeometryFactory;

    invoke-direct {v0}, Lcom/vividsolutions/jts/geom/GeometryFactory;-><init>()V

    .line 120
    .local v0, "fact":Lcom/vividsolutions/jts/geom/GeometryFactory;
    invoke-virtual {v0, v2}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createLinearRing([Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/LinearRing;

    move-result-object v1

    .line 121
    .local v1, "ring":Lcom/vividsolutions/jts/geom/LinearRing;
    const/4 v4, 0x0

    invoke-virtual {v0, v1, v4}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createPolygon(Lcom/vividsolutions/jts/geom/LinearRing;[Lcom/vividsolutions/jts/geom/LinearRing;)Lcom/vividsolutions/jts/geom/Polygon;

    move-result-object v3

    .line 122
    .local v3, "tri":Lcom/vividsolutions/jts/geom/Polygon;
    return-object v3
.end method


# virtual methods
.method public contains(Lcom/vividsolutions/jts/geom/Coordinate;)Z
    .locals 2
    .param p1, "pt"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 265
    invoke-virtual {p0}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeTriangle;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    .line 266
    .local v0, "ring":[Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-static {p1, v0}, Lcom/vividsolutions/jts/algorithm/CGAlgorithms;->isPointInRing(Lcom/vividsolutions/jts/geom/Coordinate;[Lcom/vividsolutions/jts/geom/Coordinate;)Z

    move-result v1

    return v1
.end method

.method public getAdjacentTriangleAcrossEdge(I)Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeTriangle;
    .locals 1
    .param p1, "edgeIndex"    # I

    .prologue
    .line 297
    invoke-virtual {p0, p1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeTriangle;->getEdge(I)Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->sym()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->getData()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeTriangle;

    return-object v0
.end method

.method public getAdjacentTriangleEdgeIndex(I)I
    .locals 2
    .param p1, "i"    # I

    .prologue
    .line 301
    invoke-virtual {p0, p1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeTriangle;->getAdjacentTriangleAcrossEdge(I)Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeTriangle;

    move-result-object v0

    invoke-virtual {p0, p1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeTriangle;->getEdge(I)Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->sym()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeTriangle;->getEdgeIndex(Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;)I

    move-result v0

    return v0
.end method

.method public getCoordinate(I)Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 1
    .param p1, "i"    # I

    .prologue
    .line 214
    iget-object v0, p0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeTriangle;->edge:[Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->orig()Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    return-object v0
.end method

.method public getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 5

    .prologue
    const/4 v4, 0x3

    .line 256
    const/4 v2, 0x4

    new-array v1, v2, [Lcom/vividsolutions/jts/geom/Coordinate;

    .line 257
    .local v1, "pts":[Lcom/vividsolutions/jts/geom/Coordinate;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v4, :cond_0

    .line 258
    iget-object v2, p0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeTriangle;->edge:[Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->orig()Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v2

    aput-object v2, v1, v0

    .line 257
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 260
    :cond_0
    new-instance v2, Lcom/vividsolutions/jts/geom/Coordinate;

    const/4 v3, 0x0

    aget-object v3, v1, v3

    invoke-direct {v2, v3}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(Lcom/vividsolutions/jts/geom/Coordinate;)V

    aput-object v2, v1, v4

    .line 261
    return-object v1
.end method

.method public getData()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeTriangle;->data:Ljava/lang/Object;

    return-object v0
.end method

.method public getEdge(I)Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    .locals 1
    .param p1, "i"    # I

    .prologue
    .line 193
    iget-object v0, p0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeTriangle;->edge:[Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public getEdgeIndex(Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;)I
    .locals 2
    .param p1, "e"    # Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    .prologue
    .line 226
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v1, 0x3

    if-ge v0, v1, :cond_1

    .line 227
    iget-object v1, p0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeTriangle;->edge:[Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    aget-object v1, v1, v0

    if-ne v1, p1, :cond_0

    .line 230
    .end local v0    # "i":I
    :goto_1
    return v0

    .line 226
    .restart local v0    # "i":I
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 230
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public getEdgeIndex(Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;)I
    .locals 2
    .param p1, "v"    # Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    .prologue
    .line 242
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v1, 0x3

    if-ge v0, v1, :cond_1

    .line 243
    iget-object v1, p0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeTriangle;->edge:[Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->orig()Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    move-result-object v1

    if-ne v1, p1, :cond_0

    .line 246
    .end local v0    # "i":I
    :goto_1
    return v0

    .line 242
    .restart local v0    # "i":I
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 246
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public getEdgeSegment(ILcom/vividsolutions/jts/geom/LineSegment;)V
    .locals 2
    .param p1, "i"    # I
    .param p2, "seg"    # Lcom/vividsolutions/jts/geom/LineSegment;

    .prologue
    .line 250
    iget-object v1, p0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeTriangle;->edge:[Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    aget-object v1, v1, p1

    invoke-virtual {v1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->orig()Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    iput-object v1, p2, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 251
    add-int/lit8 v1, p1, 0x1

    rem-int/lit8 v0, v1, 0x3

    .line 252
    .local v0, "nexti":I
    iget-object v1, p0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeTriangle;->edge:[Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->orig()Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    iput-object v1, p2, Lcom/vividsolutions/jts/geom/LineSegment;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 253
    return-void
.end method

.method public getEdges()[Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeTriangle;->edge:[Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    return-object v0
.end method

.method public getGeometry(Lcom/vividsolutions/jts/geom/GeometryFactory;)Lcom/vividsolutions/jts/geom/Polygon;
    .locals 3
    .param p1, "fact"    # Lcom/vividsolutions/jts/geom/GeometryFactory;

    .prologue
    .line 270
    invoke-virtual {p0}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeTriangle;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createLinearRing([Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/LinearRing;

    move-result-object v0

    .line 271
    .local v0, "ring":Lcom/vividsolutions/jts/geom/LinearRing;
    const/4 v2, 0x0

    invoke-virtual {p1, v0, v2}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createPolygon(Lcom/vividsolutions/jts/geom/LinearRing;[Lcom/vividsolutions/jts/geom/LinearRing;)Lcom/vividsolutions/jts/geom/Polygon;

    move-result-object v1

    .line 272
    .local v1, "tri":Lcom/vividsolutions/jts/geom/Polygon;
    return-object v1
.end method

.method public getNeighbours()[Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeTriangle;
    .locals 4

    .prologue
    const/4 v3, 0x3

    .line 336
    new-array v1, v3, [Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeTriangle;

    .line 337
    .local v1, "neigh":[Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeTriangle;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v3, :cond_0

    .line 338
    invoke-virtual {p0, v0}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeTriangle;->getEdge(I)Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->sym()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->getData()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeTriangle;

    aput-object v2, v1, v0

    .line 337
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 340
    :cond_0
    return-object v1
.end method

.method public getTrianglesAdjacentToVertex(I)Ljava/util/List;
    .locals 4
    .param p1, "vertexIndex"    # I

    .prologue
    .line 313
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 315
    .local v1, "adjTris":Ljava/util/List;
    invoke-virtual {p0, p1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeTriangle;->getEdge(I)Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v3

    .line 316
    .local v3, "start":Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    move-object v2, v3

    .line 318
    .local v2, "qe":Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    :cond_0
    invoke-virtual {v2}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->getData()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeTriangle;

    .line 319
    .local v0, "adjTri":Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeTriangle;
    if-eqz v0, :cond_1

    .line 320
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 322
    :cond_1
    invoke-virtual {v2}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->oNext()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v2

    .line 323
    if-ne v2, v3, :cond_0

    .line 325
    return-object v1
.end method

.method public getVertex(I)Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;
    .locals 1
    .param p1, "i"    # I

    .prologue
    .line 197
    iget-object v0, p0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeTriangle;->edge:[Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->orig()Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    move-result-object v0

    return-object v0
.end method

.method public getVertices()[Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;
    .locals 4

    .prologue
    const/4 v3, 0x3

    .line 206
    new-array v1, v3, [Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    .line 207
    .local v1, "vert":[Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v3, :cond_0

    .line 208
    invoke-virtual {p0, v0}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeTriangle;->getVertex(I)Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    move-result-object v2

    aput-object v2, v1, v0

    .line 207
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 210
    :cond_0
    return-object v1
.end method

.method public isBorder()Z
    .locals 2

    .prologue
    .line 285
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v1, 0x3

    if-ge v0, v1, :cond_1

    .line 286
    invoke-virtual {p0, v0}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeTriangle;->getAdjacentTriangleAcrossEdge(I)Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeTriangle;

    move-result-object v1

    if-nez v1, :cond_0

    .line 287
    const/4 v1, 0x1

    .line 289
    :goto_1
    return v1

    .line 285
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 289
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public isBorder(I)Z
    .locals 1
    .param p1, "i"    # I

    .prologue
    .line 293
    invoke-virtual {p0, p1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeTriangle;->getAdjacentTriangleAcrossEdge(I)Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeTriangle;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isLive()Z
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeTriangle;->edge:[Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public kill()V
    .locals 1

    .prologue
    .line 181
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeTriangle;->edge:[Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    .line 182
    return-void
.end method

.method public setData(Ljava/lang/Object;)V
    .locals 0
    .param p1, "data"    # Ljava/lang/Object;

    .prologue
    .line 168
    iput-object p1, p0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeTriangle;->data:Ljava/lang/Object;

    .line 169
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 276
    new-instance v0, Lcom/vividsolutions/jts/geom/GeometryFactory;

    invoke-direct {v0}, Lcom/vividsolutions/jts/geom/GeometryFactory;-><init>()V

    invoke-virtual {p0, v0}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeTriangle;->getGeometry(Lcom/vividsolutions/jts/geom/GeometryFactory;)Lcom/vividsolutions/jts/geom/Polygon;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/Polygon;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
