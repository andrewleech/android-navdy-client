.class public abstract Lcom/vividsolutions/jts/index/quadtree/NodeBase;
.super Ljava/lang/Object;
.source "NodeBase.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field protected items:Ljava/util/List;

.field protected subnode:[Lcom/vividsolutions/jts/index/quadtree/Node;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/index/quadtree/NodeBase;->items:Ljava/util/List;

    .line 84
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/vividsolutions/jts/index/quadtree/Node;

    iput-object v0, p0, Lcom/vividsolutions/jts/index/quadtree/NodeBase;->subnode:[Lcom/vividsolutions/jts/index/quadtree/Node;

    .line 87
    return-void
.end method

.method public static getSubnodeIndex(Lcom/vividsolutions/jts/geom/Envelope;DD)I
    .locals 5
    .param p0, "env"    # Lcom/vividsolutions/jts/geom/Envelope;
    .param p1, "centrex"    # D
    .param p3, "centrey"    # D

    .prologue
    .line 62
    const/4 v0, -0x1

    .line 63
    .local v0, "subnodeIndex":I
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/Envelope;->getMinX()D

    move-result-wide v2

    cmpl-double v1, v2, p1

    if-ltz v1, :cond_1

    .line 64
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/Envelope;->getMinY()D

    move-result-wide v2

    cmpl-double v1, v2, p3

    if-ltz v1, :cond_0

    const/4 v0, 0x3

    .line 65
    :cond_0
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/Envelope;->getMaxY()D

    move-result-wide v2

    cmpg-double v1, v2, p3

    if-gtz v1, :cond_1

    const/4 v0, 0x1

    .line 67
    :cond_1
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/Envelope;->getMaxX()D

    move-result-wide v2

    cmpg-double v1, v2, p1

    if-gtz v1, :cond_3

    .line 68
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/Envelope;->getMinY()D

    move-result-wide v2

    cmpl-double v1, v2, p3

    if-ltz v1, :cond_2

    const/4 v0, 0x2

    .line 69
    :cond_2
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/Envelope;->getMaxY()D

    move-result-wide v2

    cmpg-double v1, v2, p3

    if-gtz v1, :cond_3

    const/4 v0, 0x0

    .line 71
    :cond_3
    return v0
.end method

.method private visitItems(Lcom/vividsolutions/jts/geom/Envelope;Lcom/vividsolutions/jts/index/ItemVisitor;)V
    .locals 2
    .param p1, "searchEnv"    # Lcom/vividsolutions/jts/geom/Envelope;
    .param p2, "visitor"    # Lcom/vividsolutions/jts/index/ItemVisitor;

    .prologue
    .line 210
    iget-object v1, p0, Lcom/vividsolutions/jts/index/quadtree/NodeBase;->items:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 211
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {p2, v1}, Lcom/vividsolutions/jts/index/ItemVisitor;->visitItem(Ljava/lang/Object;)V

    goto :goto_0

    .line 213
    :cond_0
    return-void
.end method


# virtual methods
.method public add(Ljava/lang/Object;)V
    .locals 1
    .param p1, "item"    # Ljava/lang/Object;

    .prologue
    .line 95
    iget-object v0, p0, Lcom/vividsolutions/jts/index/quadtree/NodeBase;->items:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 98
    return-void
.end method

.method public addAllItems(Ljava/util/List;)Ljava/util/List;
    .locals 2
    .param p1, "resultItems"    # Ljava/util/List;

    .prologue
    .line 165
    iget-object v1, p0, Lcom/vividsolutions/jts/index/quadtree/NodeBase;->items:Ljava/util/List;

    invoke-interface {p1, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 166
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v1, 0x4

    if-ge v0, v1, :cond_1

    .line 167
    iget-object v1, p0, Lcom/vividsolutions/jts/index/quadtree/NodeBase;->subnode:[Lcom/vividsolutions/jts/index/quadtree/Node;

    aget-object v1, v1, v0

    if-eqz v1, :cond_0

    .line 168
    iget-object v1, p0, Lcom/vividsolutions/jts/index/quadtree/NodeBase;->subnode:[Lcom/vividsolutions/jts/index/quadtree/Node;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1}, Lcom/vividsolutions/jts/index/quadtree/Node;->addAllItems(Ljava/util/List;)Ljava/util/List;

    .line 166
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 171
    :cond_1
    return-object p1
.end method

.method public addAllItemsFromOverlapping(Lcom/vividsolutions/jts/geom/Envelope;Ljava/util/List;)V
    .locals 2
    .param p1, "searchEnv"    # Lcom/vividsolutions/jts/geom/Envelope;
    .param p2, "resultItems"    # Ljava/util/List;

    .prologue
    .line 177
    invoke-virtual {p0, p1}, Lcom/vividsolutions/jts/index/quadtree/NodeBase;->isSearchMatch(Lcom/vividsolutions/jts/geom/Envelope;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 189
    :cond_0
    return-void

    .line 182
    :cond_1
    iget-object v1, p0, Lcom/vividsolutions/jts/index/quadtree/NodeBase;->items:Ljava/util/List;

    invoke-interface {p2, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 184
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v1, 0x4

    if-ge v0, v1, :cond_0

    .line 185
    iget-object v1, p0, Lcom/vividsolutions/jts/index/quadtree/NodeBase;->subnode:[Lcom/vividsolutions/jts/index/quadtree/Node;

    aget-object v1, v1, v0

    if-eqz v1, :cond_2

    .line 186
    iget-object v1, p0, Lcom/vividsolutions/jts/index/quadtree/NodeBase;->subnode:[Lcom/vividsolutions/jts/index/quadtree/Node;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1, p2}, Lcom/vividsolutions/jts/index/quadtree/Node;->addAllItemsFromOverlapping(Lcom/vividsolutions/jts/geom/Envelope;Ljava/util/List;)V

    .line 184
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method depth()I
    .locals 4

    .prologue
    .line 219
    const/4 v1, 0x0

    .line 220
    .local v1, "maxSubDepth":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v3, 0x4

    if-ge v0, v3, :cond_1

    .line 221
    iget-object v3, p0, Lcom/vividsolutions/jts/index/quadtree/NodeBase;->subnode:[Lcom/vividsolutions/jts/index/quadtree/Node;

    aget-object v3, v3, v0

    if-eqz v3, :cond_0

    .line 222
    iget-object v3, p0, Lcom/vividsolutions/jts/index/quadtree/NodeBase;->subnode:[Lcom/vividsolutions/jts/index/quadtree/Node;

    aget-object v3, v3, v0

    invoke-virtual {v3}, Lcom/vividsolutions/jts/index/quadtree/Node;->depth()I

    move-result v2

    .line 223
    .local v2, "sqd":I
    if-le v2, v1, :cond_0

    .line 224
    move v1, v2

    .line 220
    .end local v2    # "sqd":I
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 227
    :cond_1
    add-int/lit8 v3, v1, 0x1

    return v3
.end method

.method public getItems()Ljava/util/List;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/vividsolutions/jts/index/quadtree/NodeBase;->items:Ljava/util/List;

    return-object v0
.end method

.method getNodeCount()I
    .locals 3

    .prologue
    .line 243
    const/4 v1, 0x0

    .line 244
    .local v1, "subSize":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v2, 0x4

    if-ge v0, v2, :cond_1

    .line 245
    iget-object v2, p0, Lcom/vividsolutions/jts/index/quadtree/NodeBase;->subnode:[Lcom/vividsolutions/jts/index/quadtree/Node;

    aget-object v2, v2, v0

    if-eqz v2, :cond_0

    .line 246
    iget-object v2, p0, Lcom/vividsolutions/jts/index/quadtree/NodeBase;->subnode:[Lcom/vividsolutions/jts/index/quadtree/Node;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/vividsolutions/jts/index/quadtree/Node;->size()I

    move-result v2

    add-int/2addr v1, v2

    .line 244
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 249
    :cond_1
    add-int/lit8 v2, v1, 0x1

    return v2
.end method

.method public hasChildren()Z
    .locals 2

    .prologue
    .line 139
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v1, 0x4

    if-ge v0, v1, :cond_1

    .line 140
    iget-object v1, p0, Lcom/vividsolutions/jts/index/quadtree/NodeBase;->subnode:[Lcom/vividsolutions/jts/index/quadtree/Node;

    aget-object v1, v1, v0

    if-eqz v1, :cond_0

    .line 141
    const/4 v1, 0x1

    .line 143
    :goto_1
    return v1

    .line 139
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 143
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public hasItems()Z
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/vividsolutions/jts/index/quadtree/NodeBase;->items:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isEmpty()Z
    .locals 3

    .prologue
    .line 148
    const/4 v1, 0x1

    .line 149
    .local v1, "isEmpty":Z
    iget-object v2, p0, Lcom/vividsolutions/jts/index/quadtree/NodeBase;->items:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v1, 0x0

    .line 150
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v2, 0x4

    if-ge v0, v2, :cond_2

    .line 151
    iget-object v2, p0, Lcom/vividsolutions/jts/index/quadtree/NodeBase;->subnode:[Lcom/vividsolutions/jts/index/quadtree/Node;

    aget-object v2, v2, v0

    if-eqz v2, :cond_1

    .line 152
    iget-object v2, p0, Lcom/vividsolutions/jts/index/quadtree/NodeBase;->subnode:[Lcom/vividsolutions/jts/index/quadtree/Node;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/vividsolutions/jts/index/quadtree/Node;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 153
    const/4 v1, 0x0

    .line 150
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 156
    :cond_2
    return v1
.end method

.method public isPrunable()Z
    .locals 1

    .prologue
    .line 134
    invoke-virtual {p0}, Lcom/vividsolutions/jts/index/quadtree/NodeBase;->hasChildren()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/vividsolutions/jts/index/quadtree/NodeBase;->hasItems()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected abstract isSearchMatch(Lcom/vividsolutions/jts/geom/Envelope;)Z
.end method

.method public remove(Lcom/vividsolutions/jts/geom/Envelope;Ljava/lang/Object;)Z
    .locals 4
    .param p1, "itemEnv"    # Lcom/vividsolutions/jts/geom/Envelope;
    .param p2, "item"    # Ljava/lang/Object;

    .prologue
    .line 110
    invoke-virtual {p0, p1}, Lcom/vividsolutions/jts/index/quadtree/NodeBase;->isSearchMatch(Lcom/vividsolutions/jts/geom/Envelope;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 111
    const/4 v0, 0x0

    .line 129
    :cond_0
    :goto_0
    return v0

    .line 113
    :cond_1
    const/4 v0, 0x0

    .line 114
    .local v0, "found":Z
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    const/4 v2, 0x4

    if-ge v1, v2, :cond_2

    .line 115
    iget-object v2, p0, Lcom/vividsolutions/jts/index/quadtree/NodeBase;->subnode:[Lcom/vividsolutions/jts/index/quadtree/Node;

    aget-object v2, v2, v1

    if-eqz v2, :cond_3

    .line 116
    iget-object v2, p0, Lcom/vividsolutions/jts/index/quadtree/NodeBase;->subnode:[Lcom/vividsolutions/jts/index/quadtree/Node;

    aget-object v2, v2, v1

    invoke-virtual {v2, p1, p2}, Lcom/vividsolutions/jts/index/quadtree/Node;->remove(Lcom/vividsolutions/jts/geom/Envelope;Ljava/lang/Object;)Z

    move-result v0

    .line 117
    if-eqz v0, :cond_3

    .line 119
    iget-object v2, p0, Lcom/vividsolutions/jts/index/quadtree/NodeBase;->subnode:[Lcom/vividsolutions/jts/index/quadtree/Node;

    aget-object v2, v2, v1

    invoke-virtual {v2}, Lcom/vividsolutions/jts/index/quadtree/Node;->isPrunable()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 120
    iget-object v2, p0, Lcom/vividsolutions/jts/index/quadtree/NodeBase;->subnode:[Lcom/vividsolutions/jts/index/quadtree/Node;

    const/4 v3, 0x0

    aput-object v3, v2, v1

    .line 126
    :cond_2
    if-nez v0, :cond_0

    .line 128
    iget-object v2, p0, Lcom/vividsolutions/jts/index/quadtree/NodeBase;->items:Ljava/util/List;

    invoke-interface {v2, p2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v0

    .line 129
    goto :goto_0

    .line 114
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method size()I
    .locals 3

    .prologue
    .line 232
    const/4 v1, 0x0

    .line 233
    .local v1, "subSize":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v2, 0x4

    if-ge v0, v2, :cond_1

    .line 234
    iget-object v2, p0, Lcom/vividsolutions/jts/index/quadtree/NodeBase;->subnode:[Lcom/vividsolutions/jts/index/quadtree/Node;

    aget-object v2, v2, v0

    if-eqz v2, :cond_0

    .line 235
    iget-object v2, p0, Lcom/vividsolutions/jts/index/quadtree/NodeBase;->subnode:[Lcom/vividsolutions/jts/index/quadtree/Node;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/vividsolutions/jts/index/quadtree/Node;->size()I

    move-result v2

    add-int/2addr v1, v2

    .line 233
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 238
    :cond_1
    iget-object v2, p0, Lcom/vividsolutions/jts/index/quadtree/NodeBase;->items:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/2addr v2, v1

    return v2
.end method

.method public visit(Lcom/vividsolutions/jts/geom/Envelope;Lcom/vividsolutions/jts/index/ItemVisitor;)V
    .locals 2
    .param p1, "searchEnv"    # Lcom/vividsolutions/jts/geom/Envelope;
    .param p2, "visitor"    # Lcom/vividsolutions/jts/index/ItemVisitor;

    .prologue
    .line 193
    invoke-virtual {p0, p1}, Lcom/vividsolutions/jts/index/quadtree/NodeBase;->isSearchMatch(Lcom/vividsolutions/jts/geom/Envelope;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 205
    :cond_0
    return-void

    .line 198
    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/vividsolutions/jts/index/quadtree/NodeBase;->visitItems(Lcom/vividsolutions/jts/geom/Envelope;Lcom/vividsolutions/jts/index/ItemVisitor;)V

    .line 200
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v1, 0x4

    if-ge v0, v1, :cond_0

    .line 201
    iget-object v1, p0, Lcom/vividsolutions/jts/index/quadtree/NodeBase;->subnode:[Lcom/vividsolutions/jts/index/quadtree/Node;

    aget-object v1, v1, v0

    if-eqz v1, :cond_2

    .line 202
    iget-object v1, p0, Lcom/vividsolutions/jts/index/quadtree/NodeBase;->subnode:[Lcom/vividsolutions/jts/index/quadtree/Node;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1, p2}, Lcom/vividsolutions/jts/index/quadtree/Node;->visit(Lcom/vividsolutions/jts/geom/Envelope;Lcom/vividsolutions/jts/index/ItemVisitor;)V

    .line 200
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
