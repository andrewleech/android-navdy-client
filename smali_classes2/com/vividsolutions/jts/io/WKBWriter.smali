.class public Lcom/vividsolutions/jts/io/WKBWriter;
.super Ljava/lang/Object;
.source "WKBWriter.java"


# instance fields
.field private buf:[B

.field private byteArrayOS:Ljava/io/ByteArrayOutputStream;

.field private byteArrayOutStream:Lcom/vividsolutions/jts/io/OutStream;

.field private byteOrder:I

.field private includeSRID:Z

.field private outputDimension:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 222
    const/4 v0, 0x2

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/vividsolutions/jts/io/WKBWriter;-><init>(II)V

    .line 223
    return-void
.end method

.method public constructor <init>(I)V
    .locals 1
    .param p1, "outputDimension"    # I

    .prologue
    .line 235
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/vividsolutions/jts/io/WKBWriter;-><init>(II)V

    .line 236
    return-void
.end method

.method public constructor <init>(II)V
    .locals 1
    .param p1, "outputDimension"    # I
    .param p2, "byteOrder"    # I

    .prologue
    .line 265
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/vividsolutions/jts/io/WKBWriter;-><init>(IIZ)V

    .line 266
    return-void
.end method

.method public constructor <init>(IIZ)V
    .locals 3
    .param p1, "outputDimension"    # I
    .param p2, "byteOrder"    # I
    .param p3, "includeSRID"    # Z

    .prologue
    const/4 v2, 0x2

    .line 280
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 209
    iput v2, p0, Lcom/vividsolutions/jts/io/WKBWriter;->outputDimension:I

    .line 211
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vividsolutions/jts/io/WKBWriter;->includeSRID:Z

    .line 212
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/io/WKBWriter;->byteArrayOS:Ljava/io/ByteArrayOutputStream;

    .line 213
    new-instance v0, Lcom/vividsolutions/jts/io/OutputStreamOutStream;

    iget-object v1, p0, Lcom/vividsolutions/jts/io/WKBWriter;->byteArrayOS:Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0, v1}, Lcom/vividsolutions/jts/io/OutputStreamOutStream;-><init>(Ljava/io/OutputStream;)V

    iput-object v0, p0, Lcom/vividsolutions/jts/io/WKBWriter;->byteArrayOutStream:Lcom/vividsolutions/jts/io/OutStream;

    .line 215
    const/16 v0, 0x8

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/vividsolutions/jts/io/WKBWriter;->buf:[B

    .line 281
    iput p1, p0, Lcom/vividsolutions/jts/io/WKBWriter;->outputDimension:I

    .line 282
    iput p2, p0, Lcom/vividsolutions/jts/io/WKBWriter;->byteOrder:I

    .line 283
    iput-boolean p3, p0, Lcom/vividsolutions/jts/io/WKBWriter;->includeSRID:Z

    .line 285
    if-lt p1, v2, :cond_0

    const/4 v0, 0x3

    if-le p1, v0, :cond_1

    .line 286
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Output dimension must be 2 or 3"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 287
    :cond_1
    return-void
.end method

.method public constructor <init>(IZ)V
    .locals 1
    .param p1, "outputDimension"    # I
    .param p2, "includeSRID"    # Z

    .prologue
    .line 251
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0, p2}, Lcom/vividsolutions/jts/io/WKBWriter;-><init>(IIZ)V

    .line 252
    return-void
.end method

.method public static bytesToHex([B)Ljava/lang/String;
    .locals 1
    .param p0, "bytes"    # [B

    .prologue
    .line 180
    invoke-static {p0}, Lcom/vividsolutions/jts/io/WKBWriter;->toHex([B)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static toHex([B)Ljava/lang/String;
    .locals 4
    .param p0, "bytes"    # [B

    .prologue
    .line 191
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 192
    .local v1, "buf":Ljava/lang/StringBuffer;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v3, p0

    if-ge v2, v3, :cond_0

    .line 193
    aget-byte v0, p0, v2

    .line 194
    .local v0, "b":B
    shr-int/lit8 v3, v0, 0x4

    and-int/lit8 v3, v3, 0xf

    invoke-static {v3}, Lcom/vividsolutions/jts/io/WKBWriter;->toHexDigit(I)C

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 195
    and-int/lit8 v3, v0, 0xf

    invoke-static {v3}, Lcom/vividsolutions/jts/io/WKBWriter;->toHexDigit(I)C

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 192
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 197
    .end local v0    # "b":B
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method private static toHexDigit(I)C
    .locals 3
    .param p0, "n"    # I

    .prologue
    .line 202
    if-ltz p0, :cond_0

    const/16 v0, 0xf

    if-le p0, v0, :cond_1

    .line 203
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Nibble value out of range: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 204
    :cond_1
    const/16 v0, 0x9

    if-gt p0, v0, :cond_2

    .line 205
    add-int/lit8 v0, p0, 0x30

    int-to-char v0, v0

    .line 206
    :goto_0
    return v0

    :cond_2
    add-int/lit8 v0, p0, -0xa

    add-int/lit8 v0, v0, 0x41

    int-to-char v0, v0

    goto :goto_0
.end method

.method private writeByteOrder(Lcom/vividsolutions/jts/io/OutStream;)V
    .locals 4
    .param p1, "os"    # Lcom/vividsolutions/jts/io/OutStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 382
    iget v0, p0, Lcom/vividsolutions/jts/io/WKBWriter;->byteOrder:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 383
    iget-object v0, p0, Lcom/vividsolutions/jts/io/WKBWriter;->buf:[B

    aput-byte v3, v0, v2

    .line 386
    :goto_0
    iget-object v0, p0, Lcom/vividsolutions/jts/io/WKBWriter;->buf:[B

    invoke-interface {p1, v0, v3}, Lcom/vividsolutions/jts/io/OutStream;->write([BI)V

    .line 387
    return-void

    .line 385
    :cond_0
    iget-object v0, p0, Lcom/vividsolutions/jts/io/WKBWriter;->buf:[B

    aput-byte v2, v0, v2

    goto :goto_0
.end method

.method private writeCoordinate(Lcom/vividsolutions/jts/geom/CoordinateSequence;ILcom/vividsolutions/jts/io/OutStream;)V
    .locals 8
    .param p1, "seq"    # Lcom/vividsolutions/jts/geom/CoordinateSequence;
    .param p2, "index"    # I
    .param p3, "os"    # Lcom/vividsolutions/jts/io/OutStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x3

    const/16 v6, 0x8

    .line 421
    invoke-interface {p1, p2}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->getX(I)D

    move-result-wide v2

    iget-object v4, p0, Lcom/vividsolutions/jts/io/WKBWriter;->buf:[B

    iget v5, p0, Lcom/vividsolutions/jts/io/WKBWriter;->byteOrder:I

    invoke-static {v2, v3, v4, v5}, Lcom/vividsolutions/jts/io/ByteOrderValues;->putDouble(D[BI)V

    .line 422
    iget-object v2, p0, Lcom/vividsolutions/jts/io/WKBWriter;->buf:[B

    invoke-interface {p3, v2, v6}, Lcom/vividsolutions/jts/io/OutStream;->write([BI)V

    .line 423
    invoke-interface {p1, p2}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->getY(I)D

    move-result-wide v2

    iget-object v4, p0, Lcom/vividsolutions/jts/io/WKBWriter;->buf:[B

    iget v5, p0, Lcom/vividsolutions/jts/io/WKBWriter;->byteOrder:I

    invoke-static {v2, v3, v4, v5}, Lcom/vividsolutions/jts/io/ByteOrderValues;->putDouble(D[BI)V

    .line 424
    iget-object v2, p0, Lcom/vividsolutions/jts/io/WKBWriter;->buf:[B

    invoke-interface {p3, v2, v6}, Lcom/vividsolutions/jts/io/OutStream;->write([BI)V

    .line 427
    iget v2, p0, Lcom/vividsolutions/jts/io/WKBWriter;->outputDimension:I

    if-lt v2, v7, :cond_1

    .line 429
    const-wide/high16 v0, 0x7ff8000000000000L    # NaN

    .line 430
    .local v0, "ordVal":D
    invoke-interface {p1}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->getDimension()I

    move-result v2

    if-lt v2, v7, :cond_0

    .line 431
    const/4 v2, 0x2

    invoke-interface {p1, p2, v2}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->getOrdinate(II)D

    move-result-wide v0

    .line 432
    :cond_0
    iget-object v2, p0, Lcom/vividsolutions/jts/io/WKBWriter;->buf:[B

    iget v3, p0, Lcom/vividsolutions/jts/io/WKBWriter;->byteOrder:I

    invoke-static {v0, v1, v2, v3}, Lcom/vividsolutions/jts/io/ByteOrderValues;->putDouble(D[BI)V

    .line 433
    iget-object v2, p0, Lcom/vividsolutions/jts/io/WKBWriter;->buf:[B

    invoke-interface {p3, v2, v6}, Lcom/vividsolutions/jts/io/OutStream;->write([BI)V

    .line 435
    .end local v0    # "ordVal":D
    :cond_1
    return-void
.end method

.method private writeCoordinateSequence(Lcom/vividsolutions/jts/geom/CoordinateSequence;ZLcom/vividsolutions/jts/io/OutStream;)V
    .locals 2
    .param p1, "seq"    # Lcom/vividsolutions/jts/geom/CoordinateSequence;
    .param p2, "writeSize"    # Z
    .param p3, "os"    # Lcom/vividsolutions/jts/io/OutStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 410
    if-eqz p2, :cond_0

    .line 411
    invoke-interface {p1}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->size()I

    move-result v1

    invoke-direct {p0, v1, p3}, Lcom/vividsolutions/jts/io/WKBWriter;->writeInt(ILcom/vividsolutions/jts/io/OutStream;)V

    .line 413
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-interface {p1}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 414
    invoke-direct {p0, p1, v0, p3}, Lcom/vividsolutions/jts/io/WKBWriter;->writeCoordinate(Lcom/vividsolutions/jts/geom/CoordinateSequence;ILcom/vividsolutions/jts/io/OutStream;)V

    .line 413
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 416
    :cond_1
    return-void
.end method

.method private writeGeometryCollection(ILcom/vividsolutions/jts/geom/GeometryCollection;Lcom/vividsolutions/jts/io/OutStream;)V
    .locals 2
    .param p1, "geometryType"    # I
    .param p2, "gc"    # Lcom/vividsolutions/jts/geom/GeometryCollection;
    .param p3, "os"    # Lcom/vividsolutions/jts/io/OutStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 372
    invoke-direct {p0, p3}, Lcom/vividsolutions/jts/io/WKBWriter;->writeByteOrder(Lcom/vividsolutions/jts/io/OutStream;)V

    .line 373
    invoke-direct {p0, p1, p2, p3}, Lcom/vividsolutions/jts/io/WKBWriter;->writeGeometryType(ILcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/io/OutStream;)V

    .line 374
    invoke-virtual {p2}, Lcom/vividsolutions/jts/geom/GeometryCollection;->getNumGeometries()I

    move-result v1

    invoke-direct {p0, v1, p3}, Lcom/vividsolutions/jts/io/WKBWriter;->writeInt(ILcom/vividsolutions/jts/io/OutStream;)V

    .line 375
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p2}, Lcom/vividsolutions/jts/geom/GeometryCollection;->getNumGeometries()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 376
    invoke-virtual {p2, v0}, Lcom/vividsolutions/jts/geom/GeometryCollection;->getGeometryN(I)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v1

    invoke-virtual {p0, v1, p3}, Lcom/vividsolutions/jts/io/WKBWriter;->write(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/io/OutStream;)V

    .line 375
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 378
    :cond_0
    return-void
.end method

.method private writeGeometryType(ILcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/io/OutStream;)V
    .locals 5
    .param p1, "geometryType"    # I
    .param p2, "g"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p3, "os"    # Lcom/vividsolutions/jts/io/OutStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 392
    iget v3, p0, Lcom/vividsolutions/jts/io/WKBWriter;->outputDimension:I

    const/4 v4, 0x3

    if-ne v3, v4, :cond_2

    const/high16 v0, -0x80000000

    .line 393
    .local v0, "flag3D":I
    :goto_0
    or-int v1, p1, v0

    .line 394
    .local v1, "typeInt":I
    iget-boolean v3, p0, Lcom/vividsolutions/jts/io/WKBWriter;->includeSRID:Z

    if-eqz v3, :cond_0

    const/high16 v2, 0x20000000

    :cond_0
    or-int/2addr v1, v2

    .line 395
    invoke-direct {p0, v1, p3}, Lcom/vividsolutions/jts/io/WKBWriter;->writeInt(ILcom/vividsolutions/jts/io/OutStream;)V

    .line 396
    iget-boolean v2, p0, Lcom/vividsolutions/jts/io/WKBWriter;->includeSRID:Z

    if-eqz v2, :cond_1

    .line 397
    invoke-virtual {p2}, Lcom/vividsolutions/jts/geom/Geometry;->getSRID()I

    move-result v2

    invoke-direct {p0, v2, p3}, Lcom/vividsolutions/jts/io/WKBWriter;->writeInt(ILcom/vividsolutions/jts/io/OutStream;)V

    .line 399
    :cond_1
    return-void

    .end local v0    # "flag3D":I
    .end local v1    # "typeInt":I
    :cond_2
    move v0, v2

    .line 392
    goto :goto_0
.end method

.method private writeInt(ILcom/vividsolutions/jts/io/OutStream;)V
    .locals 2
    .param p1, "intValue"    # I
    .param p2, "os"    # Lcom/vividsolutions/jts/io/OutStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 403
    iget-object v0, p0, Lcom/vividsolutions/jts/io/WKBWriter;->buf:[B

    iget v1, p0, Lcom/vividsolutions/jts/io/WKBWriter;->byteOrder:I

    invoke-static {p1, v0, v1}, Lcom/vividsolutions/jts/io/ByteOrderValues;->putInt(I[BI)V

    .line 404
    iget-object v0, p0, Lcom/vividsolutions/jts/io/WKBWriter;->buf:[B

    const/4 v1, 0x4

    invoke-interface {p2, v0, v1}, Lcom/vividsolutions/jts/io/OutStream;->write([BI)V

    .line 405
    return-void
.end method

.method private writeLineString(Lcom/vividsolutions/jts/geom/LineString;Lcom/vividsolutions/jts/io/OutStream;)V
    .locals 2
    .param p1, "line"    # Lcom/vividsolutions/jts/geom/LineString;
    .param p2, "os"    # Lcom/vividsolutions/jts/io/OutStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 352
    invoke-direct {p0, p2}, Lcom/vividsolutions/jts/io/WKBWriter;->writeByteOrder(Lcom/vividsolutions/jts/io/OutStream;)V

    .line 353
    const/4 v0, 0x2

    invoke-direct {p0, v0, p1, p2}, Lcom/vividsolutions/jts/io/WKBWriter;->writeGeometryType(ILcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/io/OutStream;)V

    .line 354
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/LineString;->getCoordinateSequence()Lcom/vividsolutions/jts/geom/CoordinateSequence;

    move-result-object v0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1, p2}, Lcom/vividsolutions/jts/io/WKBWriter;->writeCoordinateSequence(Lcom/vividsolutions/jts/geom/CoordinateSequence;ZLcom/vividsolutions/jts/io/OutStream;)V

    .line 355
    return-void
.end method

.method private writePoint(Lcom/vividsolutions/jts/geom/Point;Lcom/vividsolutions/jts/io/OutStream;)V
    .locals 2
    .param p1, "pt"    # Lcom/vividsolutions/jts/geom/Point;
    .param p2, "os"    # Lcom/vividsolutions/jts/io/OutStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 342
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Point;->getCoordinateSequence()Lcom/vividsolutions/jts/geom/CoordinateSequence;

    move-result-object v0

    invoke-interface {v0}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 343
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Empty Points cannot be represented in WKB"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 344
    :cond_0
    invoke-direct {p0, p2}, Lcom/vividsolutions/jts/io/WKBWriter;->writeByteOrder(Lcom/vividsolutions/jts/io/OutStream;)V

    .line 345
    const/4 v0, 0x1

    invoke-direct {p0, v0, p1, p2}, Lcom/vividsolutions/jts/io/WKBWriter;->writeGeometryType(ILcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/io/OutStream;)V

    .line 346
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Point;->getCoordinateSequence()Lcom/vividsolutions/jts/geom/CoordinateSequence;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1, p2}, Lcom/vividsolutions/jts/io/WKBWriter;->writeCoordinateSequence(Lcom/vividsolutions/jts/geom/CoordinateSequence;ZLcom/vividsolutions/jts/io/OutStream;)V

    .line 347
    return-void
.end method

.method private writePolygon(Lcom/vividsolutions/jts/geom/Polygon;Lcom/vividsolutions/jts/io/OutStream;)V
    .locals 3
    .param p1, "poly"    # Lcom/vividsolutions/jts/geom/Polygon;
    .param p2, "os"    # Lcom/vividsolutions/jts/io/OutStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 359
    invoke-direct {p0, p2}, Lcom/vividsolutions/jts/io/WKBWriter;->writeByteOrder(Lcom/vividsolutions/jts/io/OutStream;)V

    .line 360
    const/4 v1, 0x3

    invoke-direct {p0, v1, p1, p2}, Lcom/vividsolutions/jts/io/WKBWriter;->writeGeometryType(ILcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/io/OutStream;)V

    .line 361
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Polygon;->getNumInteriorRing()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-direct {p0, v1, p2}, Lcom/vividsolutions/jts/io/WKBWriter;->writeInt(ILcom/vividsolutions/jts/io/OutStream;)V

    .line 362
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Polygon;->getExteriorRing()Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vividsolutions/jts/geom/LineString;->getCoordinateSequence()Lcom/vividsolutions/jts/geom/CoordinateSequence;

    move-result-object v1

    invoke-direct {p0, v1, v2, p2}, Lcom/vividsolutions/jts/io/WKBWriter;->writeCoordinateSequence(Lcom/vividsolutions/jts/geom/CoordinateSequence;ZLcom/vividsolutions/jts/io/OutStream;)V

    .line 363
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Polygon;->getNumInteriorRing()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 364
    invoke-virtual {p1, v0}, Lcom/vividsolutions/jts/geom/Polygon;->getInteriorRingN(I)Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vividsolutions/jts/geom/LineString;->getCoordinateSequence()Lcom/vividsolutions/jts/geom/CoordinateSequence;

    move-result-object v1

    invoke-direct {p0, v1, v2, p2}, Lcom/vividsolutions/jts/io/WKBWriter;->writeCoordinateSequence(Lcom/vividsolutions/jts/geom/CoordinateSequence;ZLcom/vividsolutions/jts/io/OutStream;)V

    .line 363
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 367
    :cond_0
    return-void
.end method


# virtual methods
.method public write(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/io/OutStream;)V
    .locals 1
    .param p1, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p2, "os"    # Lcom/vividsolutions/jts/io/OutStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 316
    instance-of v0, p1, Lcom/vividsolutions/jts/geom/Point;

    if-eqz v0, :cond_0

    .line 317
    check-cast p1, Lcom/vividsolutions/jts/geom/Point;

    .end local p1    # "geom":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-direct {p0, p1, p2}, Lcom/vividsolutions/jts/io/WKBWriter;->writePoint(Lcom/vividsolutions/jts/geom/Point;Lcom/vividsolutions/jts/io/OutStream;)V

    .line 338
    :goto_0
    return-void

    .line 319
    .restart local p1    # "geom":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_0
    instance-of v0, p1, Lcom/vividsolutions/jts/geom/LineString;

    if-eqz v0, :cond_1

    .line 320
    check-cast p1, Lcom/vividsolutions/jts/geom/LineString;

    .end local p1    # "geom":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-direct {p0, p1, p2}, Lcom/vividsolutions/jts/io/WKBWriter;->writeLineString(Lcom/vividsolutions/jts/geom/LineString;Lcom/vividsolutions/jts/io/OutStream;)V

    goto :goto_0

    .line 321
    .restart local p1    # "geom":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_1
    instance-of v0, p1, Lcom/vividsolutions/jts/geom/Polygon;

    if-eqz v0, :cond_2

    .line 322
    check-cast p1, Lcom/vividsolutions/jts/geom/Polygon;

    .end local p1    # "geom":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-direct {p0, p1, p2}, Lcom/vividsolutions/jts/io/WKBWriter;->writePolygon(Lcom/vividsolutions/jts/geom/Polygon;Lcom/vividsolutions/jts/io/OutStream;)V

    goto :goto_0

    .line 323
    .restart local p1    # "geom":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_2
    instance-of v0, p1, Lcom/vividsolutions/jts/geom/MultiPoint;

    if-eqz v0, :cond_3

    .line 324
    const/4 v0, 0x4

    check-cast p1, Lcom/vividsolutions/jts/geom/MultiPoint;

    .end local p1    # "geom":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-direct {p0, v0, p1, p2}, Lcom/vividsolutions/jts/io/WKBWriter;->writeGeometryCollection(ILcom/vividsolutions/jts/geom/GeometryCollection;Lcom/vividsolutions/jts/io/OutStream;)V

    goto :goto_0

    .line 326
    .restart local p1    # "geom":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_3
    instance-of v0, p1, Lcom/vividsolutions/jts/geom/MultiLineString;

    if-eqz v0, :cond_4

    .line 327
    const/4 v0, 0x5

    check-cast p1, Lcom/vividsolutions/jts/geom/MultiLineString;

    .end local p1    # "geom":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-direct {p0, v0, p1, p2}, Lcom/vividsolutions/jts/io/WKBWriter;->writeGeometryCollection(ILcom/vividsolutions/jts/geom/GeometryCollection;Lcom/vividsolutions/jts/io/OutStream;)V

    goto :goto_0

    .line 329
    .restart local p1    # "geom":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_4
    instance-of v0, p1, Lcom/vividsolutions/jts/geom/MultiPolygon;

    if-eqz v0, :cond_5

    .line 330
    const/4 v0, 0x6

    check-cast p1, Lcom/vividsolutions/jts/geom/MultiPolygon;

    .end local p1    # "geom":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-direct {p0, v0, p1, p2}, Lcom/vividsolutions/jts/io/WKBWriter;->writeGeometryCollection(ILcom/vividsolutions/jts/geom/GeometryCollection;Lcom/vividsolutions/jts/io/OutStream;)V

    goto :goto_0

    .line 332
    .restart local p1    # "geom":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_5
    instance-of v0, p1, Lcom/vividsolutions/jts/geom/GeometryCollection;

    if-eqz v0, :cond_6

    .line 333
    const/4 v0, 0x7

    check-cast p1, Lcom/vividsolutions/jts/geom/GeometryCollection;

    .end local p1    # "geom":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-direct {p0, v0, p1, p2}, Lcom/vividsolutions/jts/io/WKBWriter;->writeGeometryCollection(ILcom/vividsolutions/jts/geom/GeometryCollection;Lcom/vividsolutions/jts/io/OutStream;)V

    goto :goto_0

    .line 336
    .restart local p1    # "geom":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_6
    const-string v0, "Unknown Geometry type"

    invoke-static {v0}, Lcom/vividsolutions/jts/util/Assert;->shouldNeverReachHere(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public write(Lcom/vividsolutions/jts/geom/Geometry;)[B
    .locals 4
    .param p1, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 298
    :try_start_0
    iget-object v1, p0, Lcom/vividsolutions/jts/io/WKBWriter;->byteArrayOS:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->reset()V

    .line 299
    iget-object v1, p0, Lcom/vividsolutions/jts/io/WKBWriter;->byteArrayOutStream:Lcom/vividsolutions/jts/io/OutStream;

    invoke-virtual {p0, p1, v1}, Lcom/vividsolutions/jts/io/WKBWriter;->write(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/io/OutStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 304
    iget-object v1, p0, Lcom/vividsolutions/jts/io/WKBWriter;->byteArrayOS:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    return-object v1

    .line 301
    :catch_0
    move-exception v0

    .line 302
    .local v0, "ex":Ljava/io/IOException;
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unexpected IO exception: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
.end method
