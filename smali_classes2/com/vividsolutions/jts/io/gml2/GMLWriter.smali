.class public Lcom/vividsolutions/jts/io/gml2/GMLWriter;
.super Ljava/lang/Object;
.source "GMLWriter.java"


# static fields
.field private static final coordinateSeparator:Ljava/lang/String; = ","

.field private static final tupleSeparator:Ljava/lang/String; = " "


# instance fields
.field private final INDENT:Ljava/lang/String;

.field private customElements:[Ljava/lang/String;

.field private emitNamespace:Z

.field private isRootTag:Z

.field private maxCoordinatesPerLine:I

.field private namespace:Ljava/lang/String;

.field private prefix:Ljava/lang/String;

.field private srsName:Ljava/lang/String;

.field private startingIndentIndex:I


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    const-string v0, "  "

    iput-object v0, p0, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->INDENT:Ljava/lang/String;

    .line 68
    iput v1, p0, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->startingIndentIndex:I

    .line 70
    const/16 v0, 0xa

    iput v0, p0, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->maxCoordinatesPerLine:I

    .line 72
    iput-boolean v1, p0, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->emitNamespace:Z

    .line 74
    iput-boolean v1, p0, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->isRootTag:Z

    .line 76
    const-string v0, "gml"

    iput-object v0, p0, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->prefix:Ljava/lang/String;

    .line 77
    const-string v0, "http://www.opengis.net/gml"

    iput-object v0, p0, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->namespace:Ljava/lang/String;

    .line 78
    iput-object v2, p0, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->srsName:Ljava/lang/String;

    .line 80
    iput-object v2, p0, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->customElements:[Ljava/lang/String;

    .line 92
    return-void
.end method

.method public constructor <init>(Z)V
    .locals 3
    .param p1, "emitNamespace"    # Z

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    const-string v0, "  "

    iput-object v0, p0, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->INDENT:Ljava/lang/String;

    .line 68
    iput v1, p0, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->startingIndentIndex:I

    .line 70
    const/16 v0, 0xa

    iput v0, p0, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->maxCoordinatesPerLine:I

    .line 72
    iput-boolean v1, p0, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->emitNamespace:Z

    .line 74
    iput-boolean v1, p0, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->isRootTag:Z

    .line 76
    const-string v0, "gml"

    iput-object v0, p0, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->prefix:Ljava/lang/String;

    .line 77
    const-string v0, "http://www.opengis.net/gml"

    iput-object v0, p0, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->namespace:Ljava/lang/String;

    .line 78
    iput-object v2, p0, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->srsName:Ljava/lang/String;

    .line 80
    iput-object v2, p0, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->customElements:[Ljava/lang/String;

    .line 102
    invoke-virtual {p0, p1}, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->setNamespace(Z)V

    .line 103
    return-void
.end method

.method private endGeomTag(Ljava/lang/String;Ljava/io/Writer;)V
    .locals 2
    .param p1, "geometryName"    # Ljava/lang/String;
    .param p2, "writer"    # Ljava/io/Writer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 475
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "</"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0}, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->prefix()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 476
    invoke-virtual {p2, p1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 477
    const-string v0, ">\n"

    invoke-virtual {p2, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 478
    return-void
.end method

.method private prefix()Ljava/lang/String;
    .locals 2

    .prologue
    .line 482
    iget-object v0, p0, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->prefix:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->prefix:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 483
    :cond_0
    const-string v0, ""

    .line 484
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->prefix:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private startGeomTag(Ljava/lang/String;Lcom/vividsolutions/jts/geom/Geometry;Ljava/io/Writer;)V
    .locals 3
    .param p1, "geometryName"    # Ljava/lang/String;
    .param p2, "g"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p3, "writer"    # Ljava/io/Writer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 435
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "<"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->prefix:Ljava/lang/String;

    if-eqz v0, :cond_0

    const-string v0, ""

    iget-object v2, p0, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->prefix:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const-string v0, ""

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 437
    invoke-virtual {p3, p1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 438
    invoke-direct {p0, p2, p3}, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->writeAttributes(Lcom/vividsolutions/jts/geom/Geometry;Ljava/io/Writer;)V

    .line 439
    const-string v0, ">\n"

    invoke-virtual {p3, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 440
    invoke-direct {p0, p2, p3}, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->writeCustomElements(Lcom/vividsolutions/jts/geom/Geometry;Ljava/io/Writer;)V

    .line 441
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->isRootTag:Z

    .line 442
    return-void

    .line 435
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->prefix:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ":"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private startLine(ILjava/io/Writer;)V
    .locals 2
    .param p1, "level"    # I
    .param p2, "writer"    # Ljava/io/Writer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 429
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, p1, :cond_0

    .line 430
    const-string v1, "  "

    invoke-virtual {p2, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 429
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 431
    :cond_0
    return-void
.end method

.method private write(Lcom/vividsolutions/jts/geom/Geometry;Ljava/io/Writer;I)V
    .locals 3
    .param p1, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p2, "writer"    # Ljava/io/Writer;
    .param p3, "level"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 216
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->isRootTag:Z

    .line 217
    instance-of v0, p1, Lcom/vividsolutions/jts/geom/Point;

    if-eqz v0, :cond_0

    .line 218
    check-cast p1, Lcom/vividsolutions/jts/geom/Point;

    .end local p1    # "geom":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-direct {p0, p1, p2, p3}, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->writePoint(Lcom/vividsolutions/jts/geom/Point;Ljava/io/Writer;I)V

    .line 236
    :goto_0
    invoke-virtual {p2}, Ljava/io/Writer;->flush()V

    .line 237
    return-void

    .line 219
    .restart local p1    # "geom":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_0
    instance-of v0, p1, Lcom/vividsolutions/jts/geom/LineString;

    if-eqz v0, :cond_1

    .line 220
    check-cast p1, Lcom/vividsolutions/jts/geom/LineString;

    .end local p1    # "geom":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-direct {p0, p1, p2, p3}, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->writeLineString(Lcom/vividsolutions/jts/geom/LineString;Ljava/io/Writer;I)V

    goto :goto_0

    .line 221
    .restart local p1    # "geom":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_1
    instance-of v0, p1, Lcom/vividsolutions/jts/geom/Polygon;

    if-eqz v0, :cond_2

    .line 222
    check-cast p1, Lcom/vividsolutions/jts/geom/Polygon;

    .end local p1    # "geom":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-direct {p0, p1, p2, p3}, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->writePolygon(Lcom/vividsolutions/jts/geom/Polygon;Ljava/io/Writer;I)V

    goto :goto_0

    .line 223
    .restart local p1    # "geom":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_2
    instance-of v0, p1, Lcom/vividsolutions/jts/geom/MultiPoint;

    if-eqz v0, :cond_3

    .line 224
    check-cast p1, Lcom/vividsolutions/jts/geom/MultiPoint;

    .end local p1    # "geom":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-direct {p0, p1, p2, p3}, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->writeMultiPoint(Lcom/vividsolutions/jts/geom/MultiPoint;Ljava/io/Writer;I)V

    goto :goto_0

    .line 225
    .restart local p1    # "geom":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_3
    instance-of v0, p1, Lcom/vividsolutions/jts/geom/MultiLineString;

    if-eqz v0, :cond_4

    .line 226
    check-cast p1, Lcom/vividsolutions/jts/geom/MultiLineString;

    .end local p1    # "geom":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-direct {p0, p1, p2, p3}, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->writeMultiLineString(Lcom/vividsolutions/jts/geom/MultiLineString;Ljava/io/Writer;I)V

    goto :goto_0

    .line 227
    .restart local p1    # "geom":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_4
    instance-of v0, p1, Lcom/vividsolutions/jts/geom/MultiPolygon;

    if-eqz v0, :cond_5

    .line 228
    check-cast p1, Lcom/vividsolutions/jts/geom/MultiPolygon;

    .end local p1    # "geom":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-direct {p0, p1, p2, p3}, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->writeMultiPolygon(Lcom/vividsolutions/jts/geom/MultiPolygon;Ljava/io/Writer;I)V

    goto :goto_0

    .line 229
    .restart local p1    # "geom":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_5
    instance-of v0, p1, Lcom/vividsolutions/jts/geom/GeometryCollection;

    if-eqz v0, :cond_6

    .line 230
    check-cast p1, Lcom/vividsolutions/jts/geom/GeometryCollection;

    .end local p1    # "geom":Lcom/vividsolutions/jts/geom/Geometry;
    iget v0, p0, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->startingIndentIndex:I

    invoke-direct {p0, p1, p2, v0}, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->writeGeometryCollection(Lcom/vividsolutions/jts/geom/GeometryCollection;Ljava/io/Writer;I)V

    goto :goto_0

    .line 233
    .restart local p1    # "geom":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_6
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unhandled geometry type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->getGeometryType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private write([Lcom/vividsolutions/jts/geom/Coordinate;Ljava/io/Writer;I)V
    .locals 6
    .param p1, "coords"    # [Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "writer"    # Ljava/io/Writer;
    .param p3, "level"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 386
    invoke-direct {p0, p3, p2}, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->startLine(ILjava/io/Writer;)V

    .line 387
    const-string v3, "coordinates"

    const/4 v4, 0x0

    invoke-direct {p0, v3, v4, p2}, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->startGeomTag(Ljava/lang/String;Lcom/vividsolutions/jts/geom/Geometry;Ljava/io/Writer;)V

    .line 389
    const/4 v0, 0x2

    .line 391
    .local v0, "dim":I
    array-length v3, p1

    if-lez v3, :cond_0

    .line 392
    const/4 v3, 0x0

    aget-object v3, p1, v3

    iget-wide v4, v3, Lcom/vividsolutions/jts/geom/Coordinate;->z:D

    invoke-static {v4, v5}, Ljava/lang/Double;->isNaN(D)Z

    move-result v3

    if-nez v3, :cond_0

    .line 393
    const/4 v0, 0x3

    .line 396
    :cond_0
    const/4 v2, 0x1

    .line 397
    .local v2, "isNewLine":Z
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v3, p1

    if-ge v1, v3, :cond_5

    .line 398
    if-eqz v2, :cond_1

    .line 399
    add-int/lit8 v3, p3, 0x1

    invoke-direct {p0, v3, p2}, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->startLine(ILjava/io/Writer;)V

    .line 400
    const/4 v2, 0x0

    .line 402
    :cond_1
    const/4 v3, 0x2

    if-ne v0, v3, :cond_4

    .line 403
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-object v4, p1, v1

    iget-wide v4, v4, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2, v3}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 404
    const-string v3, ","

    invoke-virtual {p2, v3}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 405
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-object v4, p1, v1

    iget-wide v4, v4, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2, v3}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 413
    :cond_2
    :goto_1
    const-string v3, " "

    invoke-virtual {p2, v3}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 416
    add-int/lit8 v3, v1, 0x1

    iget v4, p0, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->maxCoordinatesPerLine:I

    rem-int/2addr v3, v4

    if-nez v3, :cond_3

    array-length v3, p1

    add-int/lit8 v3, v3, -0x1

    if-ge v1, v3, :cond_3

    .line 417
    const-string v3, "\n"

    invoke-virtual {p2, v3}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 418
    const/4 v2, 0x1

    .line 397
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 406
    :cond_4
    const/4 v3, 0x3

    if-ne v0, v3, :cond_2

    .line 407
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-object v4, p1, v1

    iget-wide v4, v4, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2, v3}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 408
    const-string v3, ","

    invoke-virtual {p2, v3}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 409
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-object v4, p1, v1

    iget-wide v4, v4, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2, v3}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 410
    const-string v3, ","

    invoke-virtual {p2, v3}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 411
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-object v4, p1, v1

    iget-wide v4, v4, Lcom/vividsolutions/jts/geom/Coordinate;->z:D

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2, v3}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    goto :goto_1

    .line 421
    :cond_5
    if-nez v2, :cond_6

    .line 422
    const-string v3, "\n"

    invoke-virtual {p2, v3}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 424
    :cond_6
    invoke-direct {p0, p3, p2}, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->startLine(ILjava/io/Writer;)V

    .line 425
    const-string v3, "coordinates"

    invoke-direct {p0, v3, p2}, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->endGeomTag(Ljava/lang/String;Ljava/io/Writer;)V

    .line 426
    return-void
.end method

.method private writeAttributes(Lcom/vividsolutions/jts/geom/Geometry;Ljava/io/Writer;)V
    .locals 3
    .param p1, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p2, "writer"    # Ljava/io/Writer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 445
    if-nez p1, :cond_1

    .line 460
    :cond_0
    :goto_0
    return-void

    .line 447
    :cond_1
    iget-boolean v0, p0, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->isRootTag:Z

    if-eqz v0, :cond_0

    .line 450
    iget-boolean v0, p0, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->emitNamespace:Z

    if-eqz v0, :cond_3

    .line 451
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " xmlns"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->prefix:Ljava/lang/String;

    if-eqz v0, :cond_2

    const-string v0, ""

    iget-object v2, p0, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->prefix:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_2
    const-string v0, ""

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->namespace:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 455
    :cond_3
    iget-object v0, p0, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->srsName:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->srsName:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 456
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " srsName=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->srsName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    goto :goto_0

    .line 451
    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ":"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->prefix:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method private writeCustomElements(Lcom/vividsolutions/jts/geom/Geometry;Ljava/io/Writer;)V
    .locals 2
    .param p1, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p2, "writer"    # Ljava/io/Writer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 463
    if-nez p1, :cond_1

    .line 471
    :cond_0
    return-void

    .line 464
    :cond_1
    iget-boolean v1, p0, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->isRootTag:Z

    if-eqz v1, :cond_0

    .line 465
    iget-object v1, p0, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->customElements:[Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 467
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->customElements:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 468
    iget-object v1, p0, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->customElements:[Ljava/lang/String;

    aget-object v1, v1, v0

    invoke-virtual {p2, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 469
    const-string v1, "\n"

    invoke-virtual {p2, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 467
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private writeGeometryCollection(Lcom/vividsolutions/jts/geom/GeometryCollection;Ljava/io/Writer;I)V
    .locals 3
    .param p1, "gc"    # Lcom/vividsolutions/jts/geom/GeometryCollection;
    .param p2, "writer"    # Ljava/io/Writer;
    .param p3, "level"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 357
    invoke-direct {p0, p3, p2}, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->startLine(ILjava/io/Writer;)V

    .line 358
    const-string v1, "MultiGeometry"

    invoke-direct {p0, v1, p1, p2}, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->startGeomTag(Ljava/lang/String;Lcom/vividsolutions/jts/geom/Geometry;Ljava/io/Writer;)V

    .line 360
    const/4 v0, 0x0

    .local v0, "t":I
    :goto_0
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/GeometryCollection;->getNumGeometries()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 361
    add-int/lit8 v1, p3, 0x1

    invoke-direct {p0, v1, p2}, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->startLine(ILjava/io/Writer;)V

    .line 362
    const-string v1, "geometryMember"

    const/4 v2, 0x0

    invoke-direct {p0, v1, v2, p2}, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->startGeomTag(Ljava/lang/String;Lcom/vividsolutions/jts/geom/Geometry;Ljava/io/Writer;)V

    .line 364
    invoke-virtual {p1, v0}, Lcom/vividsolutions/jts/geom/GeometryCollection;->getGeometryN(I)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v1

    add-int/lit8 v2, p3, 0x2

    invoke-direct {p0, v1, p2, v2}, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->write(Lcom/vividsolutions/jts/geom/Geometry;Ljava/io/Writer;I)V

    .line 366
    add-int/lit8 v1, p3, 0x1

    invoke-direct {p0, v1, p2}, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->startLine(ILjava/io/Writer;)V

    .line 367
    const-string v1, "geometryMember"

    invoke-direct {p0, v1, p2}, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->endGeomTag(Ljava/lang/String;Ljava/io/Writer;)V

    .line 360
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 369
    :cond_0
    invoke-direct {p0, p3, p2}, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->startLine(ILjava/io/Writer;)V

    .line 370
    const-string v1, "MultiGeometry"

    invoke-direct {p0, v1, p2}, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->endGeomTag(Ljava/lang/String;Ljava/io/Writer;)V

    .line 371
    return-void
.end method

.method private writeLineString(Lcom/vividsolutions/jts/geom/LineString;Ljava/io/Writer;I)V
    .locals 2
    .param p1, "ls"    # Lcom/vividsolutions/jts/geom/LineString;
    .param p2, "writer"    # Ljava/io/Writer;
    .param p3, "level"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 253
    invoke-direct {p0, p3, p2}, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->startLine(ILjava/io/Writer;)V

    .line 254
    const-string v0, "LineString"

    invoke-direct {p0, v0, p1, p2}, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->startGeomTag(Ljava/lang/String;Lcom/vividsolutions/jts/geom/Geometry;Ljava/io/Writer;)V

    .line 256
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/LineString;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    add-int/lit8 v1, p3, 0x1

    invoke-direct {p0, v0, p2, v1}, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->write([Lcom/vividsolutions/jts/geom/Coordinate;Ljava/io/Writer;I)V

    .line 258
    invoke-direct {p0, p3, p2}, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->startLine(ILjava/io/Writer;)V

    .line 259
    const-string v0, "LineString"

    invoke-direct {p0, v0, p2}, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->endGeomTag(Ljava/lang/String;Ljava/io/Writer;)V

    .line 260
    return-void
.end method

.method private writeLinearRing(Lcom/vividsolutions/jts/geom/LinearRing;Ljava/io/Writer;I)V
    .locals 2
    .param p1, "lr"    # Lcom/vividsolutions/jts/geom/LinearRing;
    .param p2, "writer"    # Ljava/io/Writer;
    .param p3, "level"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 265
    invoke-direct {p0, p3, p2}, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->startLine(ILjava/io/Writer;)V

    .line 266
    const-string v0, "LinearRing"

    invoke-direct {p0, v0, p1, p2}, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->startGeomTag(Ljava/lang/String;Lcom/vividsolutions/jts/geom/Geometry;Ljava/io/Writer;)V

    .line 268
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/LinearRing;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    add-int/lit8 v1, p3, 0x1

    invoke-direct {p0, v0, p2, v1}, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->write([Lcom/vividsolutions/jts/geom/Coordinate;Ljava/io/Writer;I)V

    .line 270
    invoke-direct {p0, p3, p2}, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->startLine(ILjava/io/Writer;)V

    .line 271
    const-string v0, "LinearRing"

    invoke-direct {p0, v0, p2}, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->endGeomTag(Ljava/lang/String;Ljava/io/Writer;)V

    .line 272
    return-void
.end method

.method private writeMultiLineString(Lcom/vividsolutions/jts/geom/MultiLineString;Ljava/io/Writer;I)V
    .locals 3
    .param p1, "mls"    # Lcom/vividsolutions/jts/geom/MultiLineString;
    .param p2, "writer"    # Ljava/io/Writer;
    .param p3, "level"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 321
    invoke-direct {p0, p3, p2}, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->startLine(ILjava/io/Writer;)V

    .line 322
    const-string v1, "MultiLineString"

    invoke-direct {p0, v1, p1, p2}, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->startGeomTag(Ljava/lang/String;Lcom/vividsolutions/jts/geom/Geometry;Ljava/io/Writer;)V

    .line 324
    const/4 v0, 0x0

    .local v0, "t":I
    :goto_0
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/MultiLineString;->getNumGeometries()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 325
    add-int/lit8 v1, p3, 0x1

    invoke-direct {p0, v1, p2}, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->startLine(ILjava/io/Writer;)V

    .line 326
    const-string v1, "lineStringMember"

    const/4 v2, 0x0

    invoke-direct {p0, v1, v2, p2}, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->startGeomTag(Ljava/lang/String;Lcom/vividsolutions/jts/geom/Geometry;Ljava/io/Writer;)V

    .line 328
    invoke-virtual {p1, v0}, Lcom/vividsolutions/jts/geom/MultiLineString;->getGeometryN(I)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v1

    check-cast v1, Lcom/vividsolutions/jts/geom/LineString;

    add-int/lit8 v2, p3, 0x2

    invoke-direct {p0, v1, p2, v2}, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->writeLineString(Lcom/vividsolutions/jts/geom/LineString;Ljava/io/Writer;I)V

    .line 330
    add-int/lit8 v1, p3, 0x1

    invoke-direct {p0, v1, p2}, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->startLine(ILjava/io/Writer;)V

    .line 331
    const-string v1, "lineStringMember"

    invoke-direct {p0, v1, p2}, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->endGeomTag(Ljava/lang/String;Ljava/io/Writer;)V

    .line 324
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 333
    :cond_0
    invoke-direct {p0, p3, p2}, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->startLine(ILjava/io/Writer;)V

    .line 334
    const-string v1, "MultiLineString"

    invoke-direct {p0, v1, p2}, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->endGeomTag(Ljava/lang/String;Ljava/io/Writer;)V

    .line 335
    return-void
.end method

.method private writeMultiPoint(Lcom/vividsolutions/jts/geom/MultiPoint;Ljava/io/Writer;I)V
    .locals 3
    .param p1, "mp"    # Lcom/vividsolutions/jts/geom/MultiPoint;
    .param p2, "writer"    # Ljava/io/Writer;
    .param p3, "level"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 303
    invoke-direct {p0, p3, p2}, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->startLine(ILjava/io/Writer;)V

    .line 304
    const-string v1, "MultiPoint"

    invoke-direct {p0, v1, p1, p2}, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->startGeomTag(Ljava/lang/String;Lcom/vividsolutions/jts/geom/Geometry;Ljava/io/Writer;)V

    .line 306
    const/4 v0, 0x0

    .local v0, "t":I
    :goto_0
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/MultiPoint;->getNumGeometries()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 307
    add-int/lit8 v1, p3, 0x1

    invoke-direct {p0, v1, p2}, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->startLine(ILjava/io/Writer;)V

    .line 308
    const-string v1, "pointMember"

    const/4 v2, 0x0

    invoke-direct {p0, v1, v2, p2}, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->startGeomTag(Ljava/lang/String;Lcom/vividsolutions/jts/geom/Geometry;Ljava/io/Writer;)V

    .line 310
    invoke-virtual {p1, v0}, Lcom/vividsolutions/jts/geom/MultiPoint;->getGeometryN(I)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v1

    check-cast v1, Lcom/vividsolutions/jts/geom/Point;

    add-int/lit8 v2, p3, 0x2

    invoke-direct {p0, v1, p2, v2}, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->writePoint(Lcom/vividsolutions/jts/geom/Point;Ljava/io/Writer;I)V

    .line 312
    add-int/lit8 v1, p3, 0x1

    invoke-direct {p0, v1, p2}, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->startLine(ILjava/io/Writer;)V

    .line 313
    const-string v1, "pointMember"

    invoke-direct {p0, v1, p2}, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->endGeomTag(Ljava/lang/String;Ljava/io/Writer;)V

    .line 306
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 315
    :cond_0
    invoke-direct {p0, p3, p2}, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->startLine(ILjava/io/Writer;)V

    .line 316
    const-string v1, "MultiPoint"

    invoke-direct {p0, v1, p2}, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->endGeomTag(Ljava/lang/String;Ljava/io/Writer;)V

    .line 317
    return-void
.end method

.method private writeMultiPolygon(Lcom/vividsolutions/jts/geom/MultiPolygon;Ljava/io/Writer;I)V
    .locals 3
    .param p1, "mp"    # Lcom/vividsolutions/jts/geom/MultiPolygon;
    .param p2, "writer"    # Ljava/io/Writer;
    .param p3, "level"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 339
    invoke-direct {p0, p3, p2}, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->startLine(ILjava/io/Writer;)V

    .line 340
    const-string v1, "MultiPolygon"

    invoke-direct {p0, v1, p1, p2}, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->startGeomTag(Ljava/lang/String;Lcom/vividsolutions/jts/geom/Geometry;Ljava/io/Writer;)V

    .line 342
    const/4 v0, 0x0

    .local v0, "t":I
    :goto_0
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/MultiPolygon;->getNumGeometries()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 343
    add-int/lit8 v1, p3, 0x1

    invoke-direct {p0, v1, p2}, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->startLine(ILjava/io/Writer;)V

    .line 344
    const-string v1, "polygonMember"

    const/4 v2, 0x0

    invoke-direct {p0, v1, v2, p2}, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->startGeomTag(Ljava/lang/String;Lcom/vividsolutions/jts/geom/Geometry;Ljava/io/Writer;)V

    .line 346
    invoke-virtual {p1, v0}, Lcom/vividsolutions/jts/geom/MultiPolygon;->getGeometryN(I)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v1

    check-cast v1, Lcom/vividsolutions/jts/geom/Polygon;

    add-int/lit8 v2, p3, 0x2

    invoke-direct {p0, v1, p2, v2}, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->writePolygon(Lcom/vividsolutions/jts/geom/Polygon;Ljava/io/Writer;I)V

    .line 348
    add-int/lit8 v1, p3, 0x1

    invoke-direct {p0, v1, p2}, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->startLine(ILjava/io/Writer;)V

    .line 349
    const-string v1, "polygonMember"

    invoke-direct {p0, v1, p2}, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->endGeomTag(Ljava/lang/String;Ljava/io/Writer;)V

    .line 342
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 351
    :cond_0
    invoke-direct {p0, p3, p2}, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->startLine(ILjava/io/Writer;)V

    .line 352
    const-string v1, "MultiPolygon"

    invoke-direct {p0, v1, p2}, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->endGeomTag(Ljava/lang/String;Ljava/io/Writer;)V

    .line 353
    return-void
.end method

.method private writePoint(Lcom/vividsolutions/jts/geom/Point;Ljava/io/Writer;I)V
    .locals 3
    .param p1, "p"    # Lcom/vividsolutions/jts/geom/Point;
    .param p2, "writer"    # Ljava/io/Writer;
    .param p3, "level"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 241
    invoke-direct {p0, p3, p2}, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->startLine(ILjava/io/Writer;)V

    .line 242
    const-string v0, "Point"

    invoke-direct {p0, v0, p1, p2}, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->startGeomTag(Ljava/lang/String;Lcom/vividsolutions/jts/geom/Geometry;Ljava/io/Writer;)V

    .line 244
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/vividsolutions/jts/geom/Coordinate;

    const/4 v1, 0x0

    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Point;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v2

    aput-object v2, v0, v1

    add-int/lit8 v1, p3, 0x1

    invoke-direct {p0, v0, p2, v1}, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->write([Lcom/vividsolutions/jts/geom/Coordinate;Ljava/io/Writer;I)V

    .line 246
    invoke-direct {p0, p3, p2}, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->startLine(ILjava/io/Writer;)V

    .line 247
    const-string v0, "Point"

    invoke-direct {p0, v0, p2}, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->endGeomTag(Ljava/lang/String;Ljava/io/Writer;)V

    .line 248
    return-void
.end method

.method private writePolygon(Lcom/vividsolutions/jts/geom/Polygon;Ljava/io/Writer;I)V
    .locals 4
    .param p1, "p"    # Lcom/vividsolutions/jts/geom/Polygon;
    .param p2, "writer"    # Ljava/io/Writer;
    .param p3, "level"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 276
    invoke-direct {p0, p3, p2}, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->startLine(ILjava/io/Writer;)V

    .line 277
    const-string v1, "Polygon"

    invoke-direct {p0, v1, p1, p2}, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->startGeomTag(Ljava/lang/String;Lcom/vividsolutions/jts/geom/Geometry;Ljava/io/Writer;)V

    .line 279
    add-int/lit8 v1, p3, 0x1

    invoke-direct {p0, v1, p2}, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->startLine(ILjava/io/Writer;)V

    .line 280
    const-string v1, "outerBoundaryIs"

    invoke-direct {p0, v1, v3, p2}, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->startGeomTag(Ljava/lang/String;Lcom/vividsolutions/jts/geom/Geometry;Ljava/io/Writer;)V

    .line 282
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Polygon;->getExteriorRing()Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v1

    check-cast v1, Lcom/vividsolutions/jts/geom/LinearRing;

    add-int/lit8 v2, p3, 0x2

    invoke-direct {p0, v1, p2, v2}, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->writeLinearRing(Lcom/vividsolutions/jts/geom/LinearRing;Ljava/io/Writer;I)V

    .line 284
    add-int/lit8 v1, p3, 0x1

    invoke-direct {p0, v1, p2}, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->startLine(ILjava/io/Writer;)V

    .line 285
    const-string v1, "outerBoundaryIs"

    invoke-direct {p0, v1, p2}, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->endGeomTag(Ljava/lang/String;Ljava/io/Writer;)V

    .line 287
    const/4 v0, 0x0

    .local v0, "t":I
    :goto_0
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Polygon;->getNumInteriorRing()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 288
    add-int/lit8 v1, p3, 0x1

    invoke-direct {p0, v1, p2}, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->startLine(ILjava/io/Writer;)V

    .line 289
    const-string v1, "innerBoundaryIs"

    invoke-direct {p0, v1, v3, p2}, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->startGeomTag(Ljava/lang/String;Lcom/vividsolutions/jts/geom/Geometry;Ljava/io/Writer;)V

    .line 291
    invoke-virtual {p1, v0}, Lcom/vividsolutions/jts/geom/Polygon;->getInteriorRingN(I)Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v1

    check-cast v1, Lcom/vividsolutions/jts/geom/LinearRing;

    add-int/lit8 v2, p3, 0x2

    invoke-direct {p0, v1, p2, v2}, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->writeLinearRing(Lcom/vividsolutions/jts/geom/LinearRing;Ljava/io/Writer;I)V

    .line 293
    add-int/lit8 v1, p3, 0x1

    invoke-direct {p0, v1, p2}, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->startLine(ILjava/io/Writer;)V

    .line 294
    const-string v1, "innerBoundaryIs"

    invoke-direct {p0, v1, p2}, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->endGeomTag(Ljava/lang/String;Ljava/io/Writer;)V

    .line 287
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 297
    :cond_0
    invoke-direct {p0, p3, p2}, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->startLine(ILjava/io/Writer;)V

    .line 298
    const-string v1, "Polygon"

    invoke-direct {p0, v1, p2}, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->endGeomTag(Ljava/lang/String;Ljava/io/Writer;)V

    .line 299
    return-void
.end method


# virtual methods
.method public setCustomElements([Ljava/lang/String;)V
    .locals 0
    .param p1, "customElements"    # [Ljava/lang/String;

    .prologue
    .line 158
    iput-object p1, p0, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->customElements:[Ljava/lang/String;

    .line 159
    return-void
.end method

.method public setMaxCoordinatesPerLine(I)V
    .locals 2
    .param p1, "num"    # I

    .prologue
    .line 178
    const/4 v0, 0x1

    if-ge p1, v0, :cond_0

    .line 179
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    const-string v1, "Invalid coordinate count per line, must be > 0"

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 181
    :cond_0
    iput p1, p0, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->maxCoordinatesPerLine:I

    .line 182
    return-void
.end method

.method public setNamespace(Z)V
    .locals 0
    .param p1, "emitNamespace"    # Z

    .prologue
    .line 141
    iput-boolean p1, p0, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->emitNamespace:Z

    .line 142
    return-void
.end method

.method public setPrefix(Ljava/lang/String;)V
    .locals 0
    .param p1, "prefix"    # Ljava/lang/String;

    .prologue
    .line 114
    iput-object p1, p0, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->prefix:Ljava/lang/String;

    .line 115
    return-void
.end method

.method public setSrsName(Ljava/lang/String;)V
    .locals 0
    .param p1, "srsName"    # Ljava/lang/String;

    .prologue
    .line 129
    iput-object p1, p0, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->srsName:Ljava/lang/String;

    .line 130
    return-void
.end method

.method public setStartingIndentIndex(I)V
    .locals 0
    .param p1, "indent"    # I

    .prologue
    .line 167
    if-gez p1, :cond_0

    .line 168
    const/4 p1, 0x0

    .line 169
    :cond_0
    iput p1, p0, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->startingIndentIndex:I

    .line 170
    return-void
.end method

.method public write(Lcom/vividsolutions/jts/geom/Geometry;)Ljava/lang/String;
    .locals 3
    .param p1, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 192
    new-instance v1, Ljava/io/StringWriter;

    invoke-direct {v1}, Ljava/io/StringWriter;-><init>()V

    .line 194
    .local v1, "writer":Ljava/io/StringWriter;
    :try_start_0
    invoke-virtual {p0, p1, v1}, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->write(Lcom/vividsolutions/jts/geom/Geometry;Ljava/io/Writer;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 199
    :goto_0
    invoke-virtual {v1}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 196
    :catch_0
    move-exception v0

    .line 197
    .local v0, "ex":Ljava/io/IOException;
    invoke-static {}, Lcom/vividsolutions/jts/util/Assert;->shouldNeverReachHere()V

    goto :goto_0
.end method

.method public write(Lcom/vividsolutions/jts/geom/Geometry;Ljava/io/Writer;)V
    .locals 1
    .param p1, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p2, "writer"    # Ljava/io/Writer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 210
    iget v0, p0, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->startingIndentIndex:I

    invoke-direct {p0, p1, p2, v0}, Lcom/vividsolutions/jts/io/gml2/GMLWriter;->write(Lcom/vividsolutions/jts/geom/Geometry;Ljava/io/Writer;I)V

    .line 211
    return-void
.end method
