.class Lcom/vividsolutions/jts/io/gml2/GeometryStrategies$1;
.super Ljava/lang/Object;
.source "GeometryStrategies.java"

# interfaces
.implements Lcom/vividsolutions/jts/io/gml2/GeometryStrategies$ParseStrategy;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vividsolutions/jts/io/gml2/GeometryStrategies;->loadStrategies()Ljava/util/HashMap;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public parse(Lcom/vividsolutions/jts/io/gml2/GMLHandler$Handler;Lcom/vividsolutions/jts/geom/GeometryFactory;)Ljava/lang/Object;
    .locals 5
    .param p1, "arg"    # Lcom/vividsolutions/jts/io/gml2/GMLHandler$Handler;
    .param p2, "gf"    # Lcom/vividsolutions/jts/geom/GeometryFactory;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 81
    iget-object v3, p1, Lcom/vividsolutions/jts/io/gml2/GMLHandler$Handler;->children:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_0

    .line 82
    new-instance v3, Lorg/xml/sax/SAXException;

    const-string v4, "Cannot create a point without exactly one coordinate"

    invoke-direct {v3, v4}, Lorg/xml/sax/SAXException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 84
    :cond_0
    iget-object v3, p1, Lcom/vividsolutions/jts/io/gml2/GMLHandler$Handler;->attrs:Lorg/xml/sax/Attributes;

    invoke-virtual {p2}, Lcom/vividsolutions/jts/geom/GeometryFactory;->getSRID()I

    move-result v4

    invoke-static {v3, v4}, Lcom/vividsolutions/jts/io/gml2/GeometryStrategies;->getSrid(Lorg/xml/sax/Attributes;I)I

    move-result v2

    .line 86
    .local v2, "srid":I
    iget-object v3, p1, Lcom/vividsolutions/jts/io/gml2/GMLHandler$Handler;->children:Ljava/util/List;

    const/4 v4, 0x0

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 87
    .local v0, "c":Ljava/lang/Object;
    const/4 v1, 0x0

    .line 88
    .local v1, "p":Lcom/vividsolutions/jts/geom/Point;
    instance-of v3, v0, Lcom/vividsolutions/jts/geom/Coordinate;

    if-eqz v3, :cond_2

    .line 89
    check-cast v0, Lcom/vividsolutions/jts/geom/Coordinate;

    .end local v0    # "c":Ljava/lang/Object;
    invoke-virtual {p2, v0}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createPoint(Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/Point;

    move-result-object v1

    .line 93
    :goto_0
    invoke-virtual {v1}, Lcom/vividsolutions/jts/geom/Point;->getSRID()I

    move-result v3

    if-eq v3, v2, :cond_1

    .line 94
    invoke-virtual {v1, v2}, Lcom/vividsolutions/jts/geom/Point;->setSRID(I)V

    .line 96
    :cond_1
    return-object v1

    .line 91
    .restart local v0    # "c":Ljava/lang/Object;
    :cond_2
    check-cast v0, Lcom/vividsolutions/jts/geom/CoordinateSequence;

    .end local v0    # "c":Ljava/lang/Object;
    invoke-virtual {p2, v0}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createPoint(Lcom/vividsolutions/jts/geom/CoordinateSequence;)Lcom/vividsolutions/jts/geom/Point;

    move-result-object v1

    goto :goto_0
.end method
