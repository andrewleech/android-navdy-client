.class public Lcom/vividsolutions/jts/algorithm/RectangleLineIntersector;
.super Ljava/lang/Object;
.source "RectangleLineIntersector.java"


# instance fields
.field private diagDown0:Lcom/vividsolutions/jts/geom/Coordinate;

.field private diagDown1:Lcom/vividsolutions/jts/geom/Coordinate;

.field private diagUp0:Lcom/vividsolutions/jts/geom/Coordinate;

.field private diagUp1:Lcom/vividsolutions/jts/geom/Coordinate;

.field private li:Lcom/vividsolutions/jts/algorithm/LineIntersector;

.field private rectEnv:Lcom/vividsolutions/jts/geom/Envelope;


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geom/Envelope;)V
    .locals 6
    .param p1, "rectEnv"    # Lcom/vividsolutions/jts/geom/Envelope;

    .prologue
    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    new-instance v0, Lcom/vividsolutions/jts/algorithm/RobustLineIntersector;

    invoke-direct {v0}, Lcom/vividsolutions/jts/algorithm/RobustLineIntersector;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/algorithm/RectangleLineIntersector;->li:Lcom/vividsolutions/jts/algorithm/LineIntersector;

    .line 73
    iput-object p1, p0, Lcom/vividsolutions/jts/algorithm/RectangleLineIntersector;->rectEnv:Lcom/vividsolutions/jts/geom/Envelope;

    .line 80
    new-instance v0, Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Envelope;->getMinX()D

    move-result-wide v2

    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Envelope;->getMinY()D

    move-result-wide v4

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(DD)V

    iput-object v0, p0, Lcom/vividsolutions/jts/algorithm/RectangleLineIntersector;->diagUp0:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 81
    new-instance v0, Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Envelope;->getMaxX()D

    move-result-wide v2

    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Envelope;->getMaxY()D

    move-result-wide v4

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(DD)V

    iput-object v0, p0, Lcom/vividsolutions/jts/algorithm/RectangleLineIntersector;->diagUp1:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 82
    new-instance v0, Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Envelope;->getMinX()D

    move-result-wide v2

    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Envelope;->getMaxY()D

    move-result-wide v4

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(DD)V

    iput-object v0, p0, Lcom/vividsolutions/jts/algorithm/RectangleLineIntersector;->diagDown0:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 83
    new-instance v0, Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Envelope;->getMaxX()D

    move-result-wide v2

    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Envelope;->getMinY()D

    move-result-wide v4

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(DD)V

    iput-object v0, p0, Lcom/vividsolutions/jts/algorithm/RectangleLineIntersector;->diagDown1:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 84
    return-void
.end method


# virtual methods
.method public intersects(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)Z
    .locals 10
    .param p1, "p0"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "p1"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 102
    new-instance v1, Lcom/vividsolutions/jts/geom/Envelope;

    invoke-direct {v1, p1, p2}, Lcom/vividsolutions/jts/geom/Envelope;-><init>(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 103
    .local v1, "segEnv":Lcom/vividsolutions/jts/geom/Envelope;
    iget-object v5, p0, Lcom/vividsolutions/jts/algorithm/RectangleLineIntersector;->rectEnv:Lcom/vividsolutions/jts/geom/Envelope;

    invoke-virtual {v5, v1}, Lcom/vividsolutions/jts/geom/Envelope;->intersects(Lcom/vividsolutions/jts/geom/Envelope;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 161
    :cond_0
    :goto_0
    return v3

    .line 110
    :cond_1
    iget-object v5, p0, Lcom/vividsolutions/jts/algorithm/RectangleLineIntersector;->rectEnv:Lcom/vividsolutions/jts/geom/Envelope;

    invoke-virtual {v5, p1}, Lcom/vividsolutions/jts/geom/Envelope;->intersects(Lcom/vividsolutions/jts/geom/Coordinate;)Z

    move-result v5

    if-eqz v5, :cond_2

    move v3, v4

    goto :goto_0

    .line 111
    :cond_2
    iget-object v5, p0, Lcom/vividsolutions/jts/algorithm/RectangleLineIntersector;->rectEnv:Lcom/vividsolutions/jts/geom/Envelope;

    invoke-virtual {v5, p2}, Lcom/vividsolutions/jts/geom/Envelope;->intersects(Lcom/vividsolutions/jts/geom/Coordinate;)Z

    move-result v5

    if-eqz v5, :cond_3

    move v3, v4

    goto :goto_0

    .line 119
    :cond_3
    invoke-virtual {p1, p2}, Lcom/vividsolutions/jts/geom/Coordinate;->compareTo(Ljava/lang/Object;)I

    move-result v5

    if-lez v5, :cond_4

    .line 120
    move-object v2, p1

    .line 121
    .local v2, "tmp":Lcom/vividsolutions/jts/geom/Coordinate;
    move-object p1, p2

    .line 122
    move-object p2, v2

    .line 130
    .end local v2    # "tmp":Lcom/vividsolutions/jts/geom/Coordinate;
    :cond_4
    const/4 v0, 0x0

    .line 131
    .local v0, "isSegUpwards":Z
    iget-wide v6, p2, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    iget-wide v8, p1, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    cmpl-double v5, v6, v8

    if-lez v5, :cond_5

    .line 132
    const/4 v0, 0x1

    .line 153
    :cond_5
    if-eqz v0, :cond_6

    .line 154
    iget-object v5, p0, Lcom/vividsolutions/jts/algorithm/RectangleLineIntersector;->li:Lcom/vividsolutions/jts/algorithm/LineIntersector;

    iget-object v6, p0, Lcom/vividsolutions/jts/algorithm/RectangleLineIntersector;->diagDown0:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-object v7, p0, Lcom/vividsolutions/jts/algorithm/RectangleLineIntersector;->diagDown1:Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-virtual {v5, p1, p2, v6, v7}, Lcom/vividsolutions/jts/algorithm/LineIntersector;->computeIntersection(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 159
    :goto_1
    iget-object v5, p0, Lcom/vividsolutions/jts/algorithm/RectangleLineIntersector;->li:Lcom/vividsolutions/jts/algorithm/LineIntersector;

    invoke-virtual {v5}, Lcom/vividsolutions/jts/algorithm/LineIntersector;->hasIntersection()Z

    move-result v5

    if-eqz v5, :cond_0

    move v3, v4

    .line 160
    goto :goto_0

    .line 157
    :cond_6
    iget-object v5, p0, Lcom/vividsolutions/jts/algorithm/RectangleLineIntersector;->li:Lcom/vividsolutions/jts/algorithm/LineIntersector;

    iget-object v6, p0, Lcom/vividsolutions/jts/algorithm/RectangleLineIntersector;->diagUp0:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-object v7, p0, Lcom/vividsolutions/jts/algorithm/RectangleLineIntersector;->diagUp1:Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-virtual {v5, p1, p2, v6, v7}, Lcom/vividsolutions/jts/algorithm/LineIntersector;->computeIntersection(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)V

    goto :goto_1
.end method
