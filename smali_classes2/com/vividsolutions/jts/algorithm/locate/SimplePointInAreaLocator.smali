.class public Lcom/vividsolutions/jts/algorithm/locate/SimplePointInAreaLocator;
.super Ljava/lang/Object;
.source "SimplePointInAreaLocator.java"

# interfaces
.implements Lcom/vividsolutions/jts/algorithm/locate/PointOnGeometryLocator;


# instance fields
.field private geom:Lcom/vividsolutions/jts/geom/Geometry;


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geom/Geometry;)V
    .locals 0
    .param p1, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 121
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 122
    iput-object p1, p0, Lcom/vividsolutions/jts/algorithm/locate/SimplePointInAreaLocator;->geom:Lcom/vividsolutions/jts/geom/Geometry;

    .line 123
    return-void
.end method

.method private static containsPoint(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Geometry;)Z
    .locals 3
    .param p0, "p"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p1, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 75
    instance-of v2, p1, Lcom/vividsolutions/jts/geom/Polygon;

    if-eqz v2, :cond_0

    .line 76
    check-cast p1, Lcom/vividsolutions/jts/geom/Polygon;

    .end local p1    # "geom":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-static {p0, p1}, Lcom/vividsolutions/jts/algorithm/locate/SimplePointInAreaLocator;->containsPointInPolygon(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Polygon;)Z

    move-result v2

    .line 87
    :goto_0
    return v2

    .line 78
    .restart local p1    # "geom":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_0
    instance-of v2, p1, Lcom/vividsolutions/jts/geom/GeometryCollection;

    if-eqz v2, :cond_2

    .line 79
    new-instance v1, Lcom/vividsolutions/jts/geom/GeometryCollectionIterator;

    move-object v2, p1

    check-cast v2, Lcom/vividsolutions/jts/geom/GeometryCollection;

    invoke-direct {v1, v2}, Lcom/vividsolutions/jts/geom/GeometryCollectionIterator;-><init>(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 80
    .local v1, "geomi":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 81
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geom/Geometry;

    .line 82
    .local v0, "g2":Lcom/vividsolutions/jts/geom/Geometry;
    if-eq v0, p1, :cond_1

    .line 83
    invoke-static {p0, v0}, Lcom/vividsolutions/jts/algorithm/locate/SimplePointInAreaLocator;->containsPoint(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Geometry;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 84
    const/4 v2, 0x1

    goto :goto_0

    .line 87
    .end local v0    # "g2":Lcom/vividsolutions/jts/geom/Geometry;
    .end local v1    # "geomi":Ljava/util/Iterator;
    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static containsPointInPolygon(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Polygon;)Z
    .locals 5
    .param p0, "p"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p1, "poly"    # Lcom/vividsolutions/jts/geom/Polygon;

    .prologue
    const/4 v3, 0x0

    .line 92
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Polygon;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 100
    :cond_0
    :goto_0
    return v3

    .line 93
    :cond_1
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Polygon;->getExteriorRing()Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v2

    check-cast v2, Lcom/vividsolutions/jts/geom/LinearRing;

    .line 94
    .local v2, "shell":Lcom/vividsolutions/jts/geom/LinearRing;
    invoke-static {p0, v2}, Lcom/vividsolutions/jts/algorithm/locate/SimplePointInAreaLocator;->isPointInRing(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/LinearRing;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 96
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Polygon;->getNumInteriorRing()I

    move-result v4

    if-ge v1, v4, :cond_2

    .line 97
    invoke-virtual {p1, v1}, Lcom/vividsolutions/jts/geom/Polygon;->getInteriorRingN(I)Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geom/LinearRing;

    .line 98
    .local v0, "hole":Lcom/vividsolutions/jts/geom/LinearRing;
    invoke-static {p0, v0}, Lcom/vividsolutions/jts/algorithm/locate/SimplePointInAreaLocator;->isPointInRing(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/LinearRing;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 96
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 100
    .end local v0    # "hole":Lcom/vividsolutions/jts/geom/LinearRing;
    :cond_2
    const/4 v3, 0x1

    goto :goto_0
.end method

.method private static isPointInRing(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/LinearRing;)Z
    .locals 1
    .param p0, "p"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p1, "ring"    # Lcom/vividsolutions/jts/geom/LinearRing;

    .prologue
    .line 114
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/LinearRing;->getEnvelopeInternal()Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/vividsolutions/jts/geom/Envelope;->intersects(Lcom/vividsolutions/jts/geom/Coordinate;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 115
    const/4 v0, 0x0

    .line 116
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/LinearRing;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/vividsolutions/jts/algorithm/CGAlgorithms;->isPointInRing(Lcom/vividsolutions/jts/geom/Coordinate;[Lcom/vividsolutions/jts/geom/Coordinate;)Z

    move-result v0

    goto :goto_0
.end method

.method public static locate(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Geometry;)I
    .locals 2
    .param p0, "p"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p1, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    const/4 v0, 0x2

    .line 66
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 70
    :cond_0
    :goto_0
    return v0

    .line 68
    :cond_1
    invoke-static {p0, p1}, Lcom/vividsolutions/jts/algorithm/locate/SimplePointInAreaLocator;->containsPoint(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Geometry;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 69
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public locate(Lcom/vividsolutions/jts/geom/Coordinate;)I
    .locals 1
    .param p1, "p"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 126
    iget-object v0, p0, Lcom/vividsolutions/jts/algorithm/locate/SimplePointInAreaLocator;->geom:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-static {p1, v0}, Lcom/vividsolutions/jts/algorithm/locate/SimplePointInAreaLocator;->locate(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Geometry;)I

    move-result v0

    return v0
.end method
