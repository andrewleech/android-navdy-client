.class public Lcom/vividsolutions/jts/noding/FastNodingValidator;
.super Ljava/lang/Object;
.source "FastNodingValidator.java"


# instance fields
.field private findAllIntersections:Z

.field private isValid:Z

.field private li:Lcom/vividsolutions/jts/algorithm/LineIntersector;

.field private segInt:Lcom/vividsolutions/jts/noding/InteriorIntersectionFinder;

.field private segStrings:Ljava/util/Collection;


# direct methods
.method public constructor <init>(Ljava/util/Collection;)V
    .locals 1
    .param p1, "segStrings"    # Ljava/util/Collection;

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    new-instance v0, Lcom/vividsolutions/jts/algorithm/RobustLineIntersector;

    invoke-direct {v0}, Lcom/vividsolutions/jts/algorithm/RobustLineIntersector;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/noding/FastNodingValidator;->li:Lcom/vividsolutions/jts/algorithm/LineIntersector;

    .line 67
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vividsolutions/jts/noding/FastNodingValidator;->findAllIntersections:Z

    .line 68
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vividsolutions/jts/noding/FastNodingValidator;->segInt:Lcom/vividsolutions/jts/noding/InteriorIntersectionFinder;

    .line 69
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vividsolutions/jts/noding/FastNodingValidator;->isValid:Z

    .line 78
    iput-object p1, p0, Lcom/vividsolutions/jts/noding/FastNodingValidator;->segStrings:Ljava/util/Collection;

    .line 79
    return-void
.end method

.method private checkInteriorIntersections()V
    .locals 3

    .prologue
    .line 147
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/vividsolutions/jts/noding/FastNodingValidator;->isValid:Z

    .line 148
    new-instance v1, Lcom/vividsolutions/jts/noding/InteriorIntersectionFinder;

    iget-object v2, p0, Lcom/vividsolutions/jts/noding/FastNodingValidator;->li:Lcom/vividsolutions/jts/algorithm/LineIntersector;

    invoke-direct {v1, v2}, Lcom/vividsolutions/jts/noding/InteriorIntersectionFinder;-><init>(Lcom/vividsolutions/jts/algorithm/LineIntersector;)V

    iput-object v1, p0, Lcom/vividsolutions/jts/noding/FastNodingValidator;->segInt:Lcom/vividsolutions/jts/noding/InteriorIntersectionFinder;

    .line 149
    iget-object v1, p0, Lcom/vividsolutions/jts/noding/FastNodingValidator;->segInt:Lcom/vividsolutions/jts/noding/InteriorIntersectionFinder;

    iget-boolean v2, p0, Lcom/vividsolutions/jts/noding/FastNodingValidator;->findAllIntersections:Z

    invoke-virtual {v1, v2}, Lcom/vividsolutions/jts/noding/InteriorIntersectionFinder;->setFindAllIntersections(Z)V

    .line 150
    new-instance v0, Lcom/vividsolutions/jts/noding/MCIndexNoder;

    invoke-direct {v0}, Lcom/vividsolutions/jts/noding/MCIndexNoder;-><init>()V

    .line 151
    .local v0, "noder":Lcom/vividsolutions/jts/noding/MCIndexNoder;
    iget-object v1, p0, Lcom/vividsolutions/jts/noding/FastNodingValidator;->segInt:Lcom/vividsolutions/jts/noding/InteriorIntersectionFinder;

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/noding/MCIndexNoder;->setSegmentIntersector(Lcom/vividsolutions/jts/noding/SegmentIntersector;)V

    .line 152
    iget-object v1, p0, Lcom/vividsolutions/jts/noding/FastNodingValidator;->segStrings:Ljava/util/Collection;

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/noding/MCIndexNoder;->computeNodes(Ljava/util/Collection;)V

    .line 153
    iget-object v1, p0, Lcom/vividsolutions/jts/noding/FastNodingValidator;->segInt:Lcom/vividsolutions/jts/noding/InteriorIntersectionFinder;

    invoke-virtual {v1}, Lcom/vividsolutions/jts/noding/InteriorIntersectionFinder;->hasIntersection()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 154
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/vividsolutions/jts/noding/FastNodingValidator;->isValid:Z

    .line 157
    :cond_0
    return-void
.end method

.method private execute()V
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/vividsolutions/jts/noding/FastNodingValidator;->segInt:Lcom/vividsolutions/jts/noding/InteriorIntersectionFinder;

    if-eqz v0, :cond_0

    .line 138
    :goto_0
    return-void

    .line 137
    :cond_0
    invoke-direct {p0}, Lcom/vividsolutions/jts/noding/FastNodingValidator;->checkInteriorIntersections()V

    goto :goto_0
.end method


# virtual methods
.method public checkValid()V
    .locals 3

    .prologue
    .line 128
    invoke-direct {p0}, Lcom/vividsolutions/jts/noding/FastNodingValidator;->execute()V

    .line 129
    iget-boolean v0, p0, Lcom/vividsolutions/jts/noding/FastNodingValidator;->isValid:Z

    if-nez v0, :cond_0

    .line 130
    new-instance v0, Lcom/vividsolutions/jts/geom/TopologyException;

    invoke-virtual {p0}, Lcom/vividsolutions/jts/noding/FastNodingValidator;->getErrorMessage()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/vividsolutions/jts/noding/FastNodingValidator;->segInt:Lcom/vividsolutions/jts/noding/InteriorIntersectionFinder;

    invoke-virtual {v2}, Lcom/vividsolutions/jts/noding/InteriorIntersectionFinder;->getInteriorIntersection()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/vividsolutions/jts/geom/TopologyException;-><init>(Ljava/lang/String;Lcom/vividsolutions/jts/geom/Coordinate;)V

    throw v0

    .line 131
    :cond_0
    return-void
.end method

.method public getErrorMessage()Ljava/lang/String;
    .locals 4

    .prologue
    .line 111
    iget-boolean v1, p0, Lcom/vividsolutions/jts/noding/FastNodingValidator;->isValid:Z

    if-eqz v1, :cond_0

    const-string v1, "no intersections found"

    .line 114
    :goto_0
    return-object v1

    .line 113
    :cond_0
    iget-object v1, p0, Lcom/vividsolutions/jts/noding/FastNodingValidator;->segInt:Lcom/vividsolutions/jts/noding/InteriorIntersectionFinder;

    invoke-virtual {v1}, Lcom/vividsolutions/jts/noding/InteriorIntersectionFinder;->getIntersectionSegments()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    .line 114
    .local v0, "intSegs":[Lcom/vividsolutions/jts/geom/Coordinate;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "found non-noded intersection between "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x0

    aget-object v2, v0, v2

    const/4 v3, 0x1

    aget-object v3, v0, v3

    invoke-static {v2, v3}, Lcom/vividsolutions/jts/io/WKTWriter;->toLineString(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " and "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x2

    aget-object v2, v0, v2

    const/4 v3, 0x3

    aget-object v3, v0, v3

    invoke-static {v2, v3}, Lcom/vividsolutions/jts/io/WKTWriter;->toLineString(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public getIntersections()Ljava/util/List;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/vividsolutions/jts/noding/FastNodingValidator;->segInt:Lcom/vividsolutions/jts/noding/InteriorIntersectionFinder;

    invoke-virtual {v0}, Lcom/vividsolutions/jts/noding/InteriorIntersectionFinder;->getIntersections()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public isValid()Z
    .locals 1

    .prologue
    .line 99
    invoke-direct {p0}, Lcom/vividsolutions/jts/noding/FastNodingValidator;->execute()V

    .line 100
    iget-boolean v0, p0, Lcom/vividsolutions/jts/noding/FastNodingValidator;->isValid:Z

    return v0
.end method

.method public setFindAllIntersections(Z)V
    .locals 0
    .param p1, "findAllIntersections"    # Z

    .prologue
    .line 83
    iput-boolean p1, p0, Lcom/vividsolutions/jts/noding/FastNodingValidator;->findAllIntersections:Z

    .line 84
    return-void
.end method
