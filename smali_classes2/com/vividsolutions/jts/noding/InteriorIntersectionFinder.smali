.class public Lcom/vividsolutions/jts/noding/InteriorIntersectionFinder;
.super Ljava/lang/Object;
.source "InteriorIntersectionFinder.java"

# interfaces
.implements Lcom/vividsolutions/jts/noding/SegmentIntersector;


# instance fields
.field private findAllIntersections:Z

.field private intSegments:[Lcom/vividsolutions/jts/geom/Coordinate;

.field private interiorIntersection:Lcom/vividsolutions/jts/geom/Coordinate;

.field private intersections:Ljava/util/List;

.field private isCheckEndSegmentsOnly:Z

.field private li:Lcom/vividsolutions/jts/algorithm/LineIntersector;


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/algorithm/LineIntersector;)V
    .locals 2
    .param p1, "li"    # Lcom/vividsolutions/jts/algorithm/LineIntersector;

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput-boolean v0, p0, Lcom/vividsolutions/jts/noding/InteriorIntersectionFinder;->findAllIntersections:Z

    .line 52
    iput-boolean v0, p0, Lcom/vividsolutions/jts/noding/InteriorIntersectionFinder;->isCheckEndSegmentsOnly:Z

    .line 54
    iput-object v1, p0, Lcom/vividsolutions/jts/noding/InteriorIntersectionFinder;->interiorIntersection:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 55
    iput-object v1, p0, Lcom/vividsolutions/jts/noding/InteriorIntersectionFinder;->intSegments:[Lcom/vividsolutions/jts/geom/Coordinate;

    .line 56
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/noding/InteriorIntersectionFinder;->intersections:Ljava/util/List;

    .line 66
    iput-object p1, p0, Lcom/vividsolutions/jts/noding/InteriorIntersectionFinder;->li:Lcom/vividsolutions/jts/algorithm/LineIntersector;

    .line 67
    iput-object v1, p0, Lcom/vividsolutions/jts/noding/InteriorIntersectionFinder;->interiorIntersection:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 68
    return-void
.end method

.method private isEndSegment(Lcom/vividsolutions/jts/noding/SegmentString;I)Z
    .locals 2
    .param p1, "segStr"    # Lcom/vividsolutions/jts/noding/SegmentString;
    .param p2, "index"    # I

    .prologue
    const/4 v0, 0x1

    .line 187
    if-nez p2, :cond_1

    .line 189
    :cond_0
    :goto_0
    return v0

    .line 188
    :cond_1
    invoke-interface {p1}, Lcom/vividsolutions/jts/noding/SegmentString;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x2

    if-ge p2, v1, :cond_0

    .line 189
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getInteriorIntersection()Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/vividsolutions/jts/noding/InteriorIntersectionFinder;->interiorIntersection:Lcom/vividsolutions/jts/geom/Coordinate;

    return-object v0
.end method

.method public getIntersectionSegments()[Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/vividsolutions/jts/noding/InteriorIntersectionFinder;->intSegments:[Lcom/vividsolutions/jts/geom/Coordinate;

    return-object v0
.end method

.method public getIntersections()Ljava/util/List;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/vividsolutions/jts/noding/InteriorIntersectionFinder;->intersections:Ljava/util/List;

    return-object v0
.end method

.method public hasIntersection()Z
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/vividsolutions/jts/noding/InteriorIntersectionFinder;->interiorIntersection:Lcom/vividsolutions/jts/geom/Coordinate;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isDone()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 194
    iget-boolean v1, p0, Lcom/vividsolutions/jts/noding/InteriorIntersectionFinder;->findAllIntersections:Z

    if-eqz v1, :cond_1

    .line 195
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/vividsolutions/jts/noding/InteriorIntersectionFinder;->interiorIntersection:Lcom/vividsolutions/jts/geom/Coordinate;

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public processIntersections(Lcom/vividsolutions/jts/noding/SegmentString;ILcom/vividsolutions/jts/noding/SegmentString;I)V
    .locals 9
    .param p1, "e0"    # Lcom/vividsolutions/jts/noding/SegmentString;
    .param p2, "segIndex0"    # I
    .param p3, "e1"    # Lcom/vividsolutions/jts/noding/SegmentString;
    .param p4, "segIndex1"    # I

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 139
    invoke-virtual {p0}, Lcom/vividsolutions/jts/noding/InteriorIntersectionFinder;->hasIntersection()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 175
    :cond_0
    :goto_0
    return-void

    .line 143
    :cond_1
    if-ne p1, p3, :cond_2

    if-eq p2, p4, :cond_0

    .line 149
    :cond_2
    iget-boolean v7, p0, Lcom/vividsolutions/jts/noding/InteriorIntersectionFinder;->isCheckEndSegmentsOnly:Z

    if-eqz v7, :cond_4

    .line 150
    invoke-direct {p0, p1, p2}, Lcom/vividsolutions/jts/noding/InteriorIntersectionFinder;->isEndSegment(Lcom/vividsolutions/jts/noding/SegmentString;I)Z

    move-result v7

    if-nez v7, :cond_3

    invoke-direct {p0, p3, p4}, Lcom/vividsolutions/jts/noding/InteriorIntersectionFinder;->isEndSegment(Lcom/vividsolutions/jts/noding/SegmentString;I)Z

    move-result v7

    if-eqz v7, :cond_5

    :cond_3
    move v0, v6

    .line 151
    .local v0, "isEndSegPresent":Z
    :goto_1
    if-eqz v0, :cond_0

    .line 155
    .end local v0    # "isEndSegPresent":Z
    :cond_4
    invoke-interface {p1}, Lcom/vividsolutions/jts/noding/SegmentString;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v7

    aget-object v1, v7, p2

    .line 156
    .local v1, "p00":Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-interface {p1}, Lcom/vividsolutions/jts/noding/SegmentString;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v7

    add-int/lit8 v8, p2, 0x1

    aget-object v2, v7, v8

    .line 157
    .local v2, "p01":Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-interface {p3}, Lcom/vividsolutions/jts/noding/SegmentString;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v7

    aget-object v3, v7, p4

    .line 158
    .local v3, "p10":Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-interface {p3}, Lcom/vividsolutions/jts/noding/SegmentString;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v7

    add-int/lit8 v8, p4, 0x1

    aget-object v4, v7, v8

    .line 160
    .local v4, "p11":Lcom/vividsolutions/jts/geom/Coordinate;
    iget-object v7, p0, Lcom/vividsolutions/jts/noding/InteriorIntersectionFinder;->li:Lcom/vividsolutions/jts/algorithm/LineIntersector;

    invoke-virtual {v7, v1, v2, v3, v4}, Lcom/vividsolutions/jts/algorithm/LineIntersector;->computeIntersection(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 163
    iget-object v7, p0, Lcom/vividsolutions/jts/noding/InteriorIntersectionFinder;->li:Lcom/vividsolutions/jts/algorithm/LineIntersector;

    invoke-virtual {v7}, Lcom/vividsolutions/jts/algorithm/LineIntersector;->hasIntersection()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 164
    iget-object v7, p0, Lcom/vividsolutions/jts/noding/InteriorIntersectionFinder;->li:Lcom/vividsolutions/jts/algorithm/LineIntersector;

    invoke-virtual {v7}, Lcom/vividsolutions/jts/algorithm/LineIntersector;->isInteriorIntersection()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 165
    const/4 v7, 0x4

    new-array v7, v7, [Lcom/vividsolutions/jts/geom/Coordinate;

    iput-object v7, p0, Lcom/vividsolutions/jts/noding/InteriorIntersectionFinder;->intSegments:[Lcom/vividsolutions/jts/geom/Coordinate;

    .line 166
    iget-object v7, p0, Lcom/vividsolutions/jts/noding/InteriorIntersectionFinder;->intSegments:[Lcom/vividsolutions/jts/geom/Coordinate;

    aput-object v1, v7, v5

    .line 167
    iget-object v7, p0, Lcom/vividsolutions/jts/noding/InteriorIntersectionFinder;->intSegments:[Lcom/vividsolutions/jts/geom/Coordinate;

    aput-object v2, v7, v6

    .line 168
    iget-object v6, p0, Lcom/vividsolutions/jts/noding/InteriorIntersectionFinder;->intSegments:[Lcom/vividsolutions/jts/geom/Coordinate;

    const/4 v7, 0x2

    aput-object v3, v6, v7

    .line 169
    iget-object v6, p0, Lcom/vividsolutions/jts/noding/InteriorIntersectionFinder;->intSegments:[Lcom/vividsolutions/jts/geom/Coordinate;

    const/4 v7, 0x3

    aput-object v4, v6, v7

    .line 171
    iget-object v6, p0, Lcom/vividsolutions/jts/noding/InteriorIntersectionFinder;->li:Lcom/vividsolutions/jts/algorithm/LineIntersector;

    invoke-virtual {v6, v5}, Lcom/vividsolutions/jts/algorithm/LineIntersector;->getIntersection(I)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v5

    iput-object v5, p0, Lcom/vividsolutions/jts/noding/InteriorIntersectionFinder;->interiorIntersection:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 172
    iget-object v5, p0, Lcom/vividsolutions/jts/noding/InteriorIntersectionFinder;->intersections:Ljava/util/List;

    iget-object v6, p0, Lcom/vividsolutions/jts/noding/InteriorIntersectionFinder;->interiorIntersection:Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .end local v1    # "p00":Lcom/vividsolutions/jts/geom/Coordinate;
    .end local v2    # "p01":Lcom/vividsolutions/jts/geom/Coordinate;
    .end local v3    # "p10":Lcom/vividsolutions/jts/geom/Coordinate;
    .end local v4    # "p11":Lcom/vividsolutions/jts/geom/Coordinate;
    :cond_5
    move v0, v5

    .line 150
    goto :goto_1
.end method

.method public setCheckEndSegmentsOnly(Z)V
    .locals 0
    .param p1, "isCheckEndSegmentsOnly"    # Z

    .prologue
    .line 91
    iput-boolean p1, p0, Lcom/vividsolutions/jts/noding/InteriorIntersectionFinder;->isCheckEndSegmentsOnly:Z

    .line 92
    return-void
.end method

.method public setFindAllIntersections(Z)V
    .locals 0
    .param p1, "findAllIntersections"    # Z

    .prologue
    .line 72
    iput-boolean p1, p0, Lcom/vividsolutions/jts/noding/InteriorIntersectionFinder;->findAllIntersections:Z

    .line 73
    return-void
.end method
