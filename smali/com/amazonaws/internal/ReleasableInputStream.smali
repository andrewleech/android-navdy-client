.class public Lcom/amazonaws/internal/ReleasableInputStream;
.super Lcom/amazonaws/internal/SdkFilterInputStream;
.source "ReleasableInputStream.java"

# interfaces
.implements Lcom/amazonaws/internal/Releasable;


# static fields
.field private static final log:Lorg/apache/commons/logging/Log;


# instance fields
.field private closeDisabled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const-class v0, Lcom/amazonaws/internal/ReleasableInputStream;

    .line 42
    invoke-static {v0}, Lorg/apache/commons/logging/LogFactory;->getLog(Ljava/lang/Class;)Lorg/apache/commons/logging/Log;

    move-result-object v0

    sput-object v0, Lcom/amazonaws/internal/ReleasableInputStream;->log:Lorg/apache/commons/logging/Log;

    .line 41
    return-void
.end method

.method protected constructor <init>(Ljava/io/InputStream;)V
    .locals 0
    .param p1, "is"    # Ljava/io/InputStream;

    .prologue
    .line 55
    invoke-direct {p0, p1}, Lcom/amazonaws/internal/SdkFilterInputStream;-><init>(Ljava/io/InputStream;)V

    .line 56
    return-void
.end method

.method private doRelease()V
    .locals 4

    .prologue
    .line 83
    :try_start_0
    iget-object v2, p0, Lcom/amazonaws/internal/ReleasableInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 89
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/amazonaws/internal/ReleasableInputStream;->in:Ljava/io/InputStream;

    instance-of v2, v2, Lcom/amazonaws/internal/Releasable;

    if-eqz v2, :cond_1

    .line 92
    iget-object v1, p0, Lcom/amazonaws/internal/ReleasableInputStream;->in:Ljava/io/InputStream;

    check-cast v1, Lcom/amazonaws/internal/Releasable;

    .line 93
    .local v1, "r":Lcom/amazonaws/internal/Releasable;
    invoke-interface {v1}, Lcom/amazonaws/internal/Releasable;->release()V

    .line 95
    .end local v1    # "r":Lcom/amazonaws/internal/Releasable;
    :cond_1
    invoke-virtual {p0}, Lcom/amazonaws/internal/ReleasableInputStream;->abortIfNeeded()V

    .line 96
    return-void

    .line 84
    :catch_0
    move-exception v0

    .line 85
    .local v0, "ex":Ljava/lang/Exception;
    sget-object v2, Lcom/amazonaws/internal/ReleasableInputStream;->log:Lorg/apache/commons/logging/Log;

    invoke-interface {v2}, Lorg/apache/commons/logging/Log;->isDebugEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 86
    sget-object v2, Lcom/amazonaws/internal/ReleasableInputStream;->log:Lorg/apache/commons/logging/Log;

    const-string v3, "FYI"

    invoke-interface {v2, v3, v0}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static wrap(Ljava/io/InputStream;)Lcom/amazonaws/internal/ReleasableInputStream;
    .locals 1
    .param p0, "is"    # Ljava/io/InputStream;

    .prologue
    .line 126
    instance-of v0, p0, Lcom/amazonaws/internal/ReleasableInputStream;

    if-eqz v0, :cond_0

    .line 128
    check-cast p0, Lcom/amazonaws/internal/ReleasableInputStream;

    .line 133
    .end local p0    # "is":Ljava/io/InputStream;
    :goto_0
    return-object p0

    .line 130
    .restart local p0    # "is":Ljava/io/InputStream;
    :cond_0
    instance-of v0, p0, Ljava/io/FileInputStream;

    if-eqz v0, :cond_1

    .line 131
    check-cast p0, Ljava/io/FileInputStream;

    .end local p0    # "is":Ljava/io/InputStream;
    invoke-static {p0}, Lcom/amazonaws/internal/ResettableInputStream;->newResettableInputStream(Ljava/io/FileInputStream;)Lcom/amazonaws/internal/ResettableInputStream;

    move-result-object p0

    goto :goto_0

    .line 133
    .restart local p0    # "is":Ljava/io/InputStream;
    :cond_1
    new-instance v0, Lcom/amazonaws/internal/ReleasableInputStream;

    invoke-direct {v0, p0}, Lcom/amazonaws/internal/ReleasableInputStream;-><init>(Ljava/io/InputStream;)V

    move-object p0, v0

    goto :goto_0
.end method


# virtual methods
.method public final close()V
    .locals 1

    .prologue
    .line 65
    iget-boolean v0, p0, Lcom/amazonaws/internal/ReleasableInputStream;->closeDisabled:Z

    if-nez v0, :cond_0

    .line 66
    invoke-direct {p0}, Lcom/amazonaws/internal/ReleasableInputStream;->doRelease()V

    .line 68
    :cond_0
    return-void
.end method

.method public final disableClose()Lcom/amazonaws/internal/ReleasableInputStream;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/amazonaws/internal/ReleasableInputStream;",
            ">()TT;"
        }
    .end annotation

    .prologue
    .line 112
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/amazonaws/internal/ReleasableInputStream;->closeDisabled:Z

    .line 115
    move-object v0, p0

    .line 116
    .local v0, "t":Lcom/amazonaws/internal/ReleasableInputStream;, "TT;"
    return-object v0
.end method

.method public final isCloseDisabled()Z
    .locals 1

    .prologue
    .line 104
    iget-boolean v0, p0, Lcom/amazonaws/internal/ReleasableInputStream;->closeDisabled:Z

    return v0
.end method

.method public final release()V
    .locals 0

    .prologue
    .line 75
    invoke-direct {p0}, Lcom/amazonaws/internal/ReleasableInputStream;->doRelease()V

    .line 76
    return-void
.end method
