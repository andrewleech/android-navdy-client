.class public Lcom/nimbusds/jwt/EncryptedJWT;
.super Lcom/nimbusds/jose/JWEObject;
.source "EncryptedJWT.java"

# interfaces
.implements Lcom/nimbusds/jwt/JWT;


# annotations
.annotation runtime Lnet/jcip/annotations/ThreadSafe;
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x1L


# direct methods
.method public constructor <init>(Lcom/nimbusds/jose/JWEHeader;Lcom/nimbusds/jwt/JWTClaimsSet;)V
    .locals 2
    .param p1, "header"    # Lcom/nimbusds/jose/JWEHeader;
    .param p2, "claimsSet"    # Lcom/nimbusds/jwt/JWTClaimsSet;

    .prologue
    .line 40
    new-instance v0, Lcom/nimbusds/jose/Payload;

    invoke-virtual {p2}, Lcom/nimbusds/jwt/JWTClaimsSet;->toJSONObject()Lnet/minidev/json/JSONObject;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/nimbusds/jose/Payload;-><init>(Lnet/minidev/json/JSONObject;)V

    invoke-direct {p0, p1, v0}, Lcom/nimbusds/jose/JWEObject;-><init>(Lcom/nimbusds/jose/JWEHeader;Lcom/nimbusds/jose/Payload;)V

    .line 41
    return-void
.end method

.method public constructor <init>(Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;)V
    .locals 0
    .param p1, "firstPart"    # Lcom/nimbusds/jose/util/Base64URL;
    .param p2, "secondPart"    # Lcom/nimbusds/jose/util/Base64URL;
    .param p3, "thirdPart"    # Lcom/nimbusds/jose/util/Base64URL;
    .param p4, "fourthPart"    # Lcom/nimbusds/jose/util/Base64URL;
    .param p5, "fifthPart"    # Lcom/nimbusds/jose/util/Base64URL;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .prologue
    .line 69
    invoke-direct/range {p0 .. p5}, Lcom/nimbusds/jose/JWEObject;-><init>(Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;)V

    .line 70
    return-void
.end method

.method public static bridge synthetic parse(Ljava/lang/String;)Lcom/nimbusds/jose/JWEObject;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-static {p0}, Lcom/nimbusds/jwt/EncryptedJWT;->parse(Ljava/lang/String;)Lcom/nimbusds/jwt/EncryptedJWT;

    move-result-object v0

    return-object v0
.end method

.method public static parse(Ljava/lang/String;)Lcom/nimbusds/jwt/EncryptedJWT;
    .locals 7
    .param p0, "s"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 107
    invoke-static {p0}, Lcom/nimbusds/jose/JOSEObject;->split(Ljava/lang/String;)[Lcom/nimbusds/jose/util/Base64URL;

    move-result-object v6

    .line 109
    .local v6, "parts":[Lcom/nimbusds/jose/util/Base64URL;
    array-length v0, v6

    const/4 v1, 0x5

    if-eq v0, v1, :cond_0

    .line 110
    new-instance v0, Ljava/text/ParseException;

    const-string v1, "Unexpected number of Base64URL parts, must be five"

    invoke-direct {v0, v1, v2}, Ljava/text/ParseException;-><init>(Ljava/lang/String;I)V

    throw v0

    .line 113
    :cond_0
    new-instance v0, Lcom/nimbusds/jwt/EncryptedJWT;

    aget-object v1, v6, v2

    const/4 v2, 0x1

    aget-object v2, v6, v2

    const/4 v3, 0x2

    aget-object v3, v6, v3

    const/4 v4, 0x3

    aget-object v4, v6, v4

    const/4 v5, 0x4

    aget-object v5, v6, v5

    invoke-direct/range {v0 .. v5}, Lcom/nimbusds/jwt/EncryptedJWT;-><init>(Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;)V

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getHeader()Lcom/nimbusds/jose/Header;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lcom/nimbusds/jwt/EncryptedJWT;->getHeader()Lcom/nimbusds/jose/JWEHeader;

    move-result-object v0

    return-object v0
.end method

.method public getJWTClaimsSet()Lcom/nimbusds/jwt/JWTClaimsSet;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .prologue
    .line 77
    invoke-virtual {p0}, Lcom/nimbusds/jwt/EncryptedJWT;->getPayload()Lcom/nimbusds/jose/Payload;

    move-result-object v1

    .line 79
    .local v1, "payload":Lcom/nimbusds/jose/Payload;
    if-nez v1, :cond_0

    .line 80
    const/4 v2, 0x0

    .line 89
    :goto_0
    return-object v2

    .line 83
    :cond_0
    invoke-virtual {v1}, Lcom/nimbusds/jose/Payload;->toJSONObject()Lnet/minidev/json/JSONObject;

    move-result-object v0

    .line 85
    .local v0, "json":Lnet/minidev/json/JSONObject;
    if-nez v0, :cond_1

    .line 86
    new-instance v2, Ljava/text/ParseException;

    const-string v3, "Payload of JWE object is not a valid JSON object"

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Ljava/text/ParseException;-><init>(Ljava/lang/String;I)V

    throw v2

    .line 89
    :cond_1
    invoke-static {v0}, Lcom/nimbusds/jwt/JWTClaimsSet;->parse(Lnet/minidev/json/JSONObject;)Lcom/nimbusds/jwt/JWTClaimsSet;

    move-result-object v2

    goto :goto_0
.end method
