.class Lcom/nimbusds/jose/crypto/AESCBC;
.super Ljava/lang/Object;
.source "AESCBC.java"


# annotations
.annotation runtime Lnet/jcip/annotations/ThreadSafe;
.end annotation


# static fields
.field public static final IV_BIT_LENGTH:I = 0x80


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 426
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static createAESCBCCipher(Ljavax/crypto/SecretKey;Z[BLjava/security/Provider;)Ljavax/crypto/Cipher;
    .locals 6
    .param p0, "secretKey"    # Ljavax/crypto/SecretKey;
    .param p1, "forEncryption"    # Z
    .param p2, "iv"    # [B
    .param p3, "provider"    # Ljava/security/Provider;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/nimbusds/jose/JOSEException;
        }
    .end annotation

    .prologue
    .line 84
    :try_start_0
    const-string v4, "AES/CBC/PKCS5Padding"

    invoke-static {v4, p3}, Lcom/nimbusds/jose/crypto/CipherHelper;->getInstance(Ljava/lang/String;Ljava/security/Provider;)Ljavax/crypto/Cipher;

    move-result-object v0

    .line 86
    .local v0, "cipher":Ljavax/crypto/Cipher;
    new-instance v3, Ljavax/crypto/spec/SecretKeySpec;

    invoke-interface {p0}, Ljavax/crypto/SecretKey;->getEncoded()[B

    move-result-object v4

    const-string v5, "AES"

    invoke-direct {v3, v4, v5}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    .line 88
    .local v3, "keyspec":Ljavax/crypto/spec/SecretKeySpec;
    new-instance v2, Ljavax/crypto/spec/IvParameterSpec;

    invoke-direct {v2, p2}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    .line 90
    .local v2, "ivSpec":Ljavax/crypto/spec/IvParameterSpec;
    if-eqz p1, :cond_0

    .line 92
    const/4 v4, 0x1

    invoke-virtual {v0, v4, v3, v2}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V

    .line 104
    :goto_0
    return-object v0

    .line 96
    :cond_0
    const/4 v4, 0x2

    invoke-virtual {v0, v4, v3, v2}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 99
    .end local v0    # "cipher":Ljavax/crypto/Cipher;
    .end local v2    # "ivSpec":Ljavax/crypto/spec/IvParameterSpec;
    .end local v3    # "keyspec":Ljavax/crypto/spec/SecretKeySpec;
    :catch_0
    move-exception v1

    .line 101
    .local v1, "e":Ljava/lang/Exception;
    new-instance v4, Lcom/nimbusds/jose/JOSEException;

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5, v1}, Lcom/nimbusds/jose/JOSEException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4
.end method

.method public static decrypt(Ljavax/crypto/SecretKey;[B[BLjava/security/Provider;)[B
    .locals 4
    .param p0, "secretKey"    # Ljavax/crypto/SecretKey;
    .param p1, "iv"    # [B
    .param p2, "cipherText"    # [B
    .param p3, "provider"    # Ljava/security/Provider;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/nimbusds/jose/JOSEException;
        }
    .end annotation

    .prologue
    .line 273
    const/4 v2, 0x0

    invoke-static {p0, v2, p1, p3}, Lcom/nimbusds/jose/crypto/AESCBC;->createAESCBCCipher(Ljavax/crypto/SecretKey;Z[BLjava/security/Provider;)Ljavax/crypto/Cipher;

    move-result-object v0

    .line 276
    .local v0, "cipher":Ljavax/crypto/Cipher;
    :try_start_0
    invoke-virtual {v0, p2}, Ljavax/crypto/Cipher;->doFinal([B)[B
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    return-object v2

    .line 278
    :catch_0
    move-exception v1

    .line 280
    .local v1, "e":Ljava/lang/Exception;
    new-instance v2, Lcom/nimbusds/jose/JOSEException;

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v1}, Lcom/nimbusds/jose/JOSEException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
.end method

.method public static decryptAuthenticated(Ljavax/crypto/SecretKey;[B[B[B[BLjava/security/Provider;Ljava/security/Provider;)[B
    .locals 11
    .param p0, "secretKey"    # Ljavax/crypto/SecretKey;
    .param p1, "iv"    # [B
    .param p2, "cipherText"    # [B
    .param p3, "aad"    # [B
    .param p4, "authTag"    # [B
    .param p5, "ceProvider"    # Ljava/security/Provider;
    .param p6, "macProvider"    # Ljava/security/Provider;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/nimbusds/jose/JOSEException;
        }
    .end annotation

    .prologue
    .line 321
    new-instance v2, Lcom/nimbusds/jose/crypto/CompositeKey;

    invoke-direct {v2, p0}, Lcom/nimbusds/jose/crypto/CompositeKey;-><init>(Ljavax/crypto/SecretKey;)V

    .line 324
    .local v2, "compositeKey":Lcom/nimbusds/jose/crypto/CompositeKey;
    invoke-static {p3}, Lcom/nimbusds/jose/crypto/AAD;->computeLength([B)[B

    move-result-object v1

    .line 327
    .local v1, "al":[B
    array-length v9, p3

    array-length v10, p1

    add-int/2addr v9, v10

    array-length v10, p2

    add-int/2addr v9, v10

    array-length v10, v1

    add-int v6, v9, v10

    .line 328
    .local v6, "hmacInputLength":I
    invoke-static {v6}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v9

    .line 329
    invoke-virtual {v9, p3}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    move-result-object v9

    .line 330
    invoke-virtual {v9, p1}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    move-result-object v9

    .line 331
    invoke-virtual {v9, p2}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    move-result-object v9

    .line 332
    invoke-virtual {v9, v1}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    move-result-object v9

    .line 333
    invoke-virtual {v9}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v5

    .line 334
    .local v5, "hmacInput":[B
    invoke-virtual {v2}, Lcom/nimbusds/jose/crypto/CompositeKey;->getMACKey()Ljavax/crypto/SecretKey;

    move-result-object v9

    move-object/from16 v0, p6

    invoke-static {v9, v5, v0}, Lcom/nimbusds/jose/crypto/HMAC;->compute(Ljavax/crypto/SecretKey;[BLjava/security/Provider;)[B

    move-result-object v4

    .line 336
    .local v4, "hmac":[B
    invoke-virtual {v2}, Lcom/nimbusds/jose/crypto/CompositeKey;->getTruncatedMACByteLength()I

    move-result v9

    invoke-static {v4, v9}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v3

    .line 338
    .local v3, "expectedAuthTag":[B
    const/4 v7, 0x1

    .line 340
    .local v7, "macCheckPassed":Z
    invoke-static {v3, p4}, Lcom/nimbusds/jose/crypto/ConstantTimeUtils;->areEqual([B[B)Z

    move-result v9

    if-nez v9, :cond_0

    .line 342
    const/4 v7, 0x0

    .line 345
    :cond_0
    invoke-virtual {v2}, Lcom/nimbusds/jose/crypto/CompositeKey;->getAESKey()Ljavax/crypto/SecretKey;

    move-result-object v9

    move-object/from16 v0, p5

    invoke-static {v9, p1, p2, v0}, Lcom/nimbusds/jose/crypto/AESCBC;->decrypt(Ljavax/crypto/SecretKey;[B[BLjava/security/Provider;)[B

    move-result-object v8

    .line 347
    .local v8, "plainText":[B
    if-nez v7, :cond_1

    .line 349
    new-instance v9, Lcom/nimbusds/jose/JOSEException;

    const-string v10, "MAC check failed"

    invoke-direct {v9, v10}, Lcom/nimbusds/jose/JOSEException;-><init>(Ljava/lang/String;)V

    throw v9

    .line 352
    :cond_1
    return-object v8
.end method

.method public static decryptWithConcatKDF(Lcom/nimbusds/jose/JWEHeader;Ljavax/crypto/SecretKey;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;Ljava/security/Provider;Ljava/security/Provider;)[B
    .locals 10
    .param p0, "header"    # Lcom/nimbusds/jose/JWEHeader;
    .param p1, "secretKey"    # Ljavax/crypto/SecretKey;
    .param p2, "encryptedKey"    # Lcom/nimbusds/jose/util/Base64URL;
    .param p3, "iv"    # Lcom/nimbusds/jose/util/Base64URL;
    .param p4, "cipherText"    # Lcom/nimbusds/jose/util/Base64URL;
    .param p5, "authTag"    # Lcom/nimbusds/jose/util/Base64URL;
    .param p6, "ceProvider"    # Ljava/security/Provider;
    .param p7, "macProvider"    # Ljava/security/Provider;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/nimbusds/jose/JOSEException;
        }
    .end annotation

    .prologue
    .line 387
    const/4 v3, 0x0

    .line 389
    .local v3, "epu":[B
    const-string v8, "epu"

    invoke-virtual {p0, v8}, Lcom/nimbusds/jose/JWEHeader;->getCustomParam(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    instance-of v8, v8, Ljava/lang/String;

    if-eqz v8, :cond_0

    .line 391
    new-instance v9, Lcom/nimbusds/jose/util/Base64URL;

    const-string v8, "epu"

    invoke-virtual {p0, v8}, Lcom/nimbusds/jose/JWEHeader;->getCustomParam(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-direct {v9, v8}, Lcom/nimbusds/jose/util/Base64URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Lcom/nimbusds/jose/util/Base64URL;->decode()[B

    move-result-object v3

    .line 394
    :cond_0
    const/4 v4, 0x0

    .line 396
    .local v4, "epv":[B
    const-string v8, "epv"

    invoke-virtual {p0, v8}, Lcom/nimbusds/jose/JWEHeader;->getCustomParam(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    instance-of v8, v8, Ljava/lang/String;

    if-eqz v8, :cond_1

    .line 398
    new-instance v9, Lcom/nimbusds/jose/util/Base64URL;

    const-string v8, "epv"

    invoke-virtual {p0, v8}, Lcom/nimbusds/jose/JWEHeader;->getCustomParam(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-direct {v9, v8}, Lcom/nimbusds/jose/util/Base64URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Lcom/nimbusds/jose/util/Base64URL;->decode()[B

    move-result-object v4

    .line 401
    :cond_1
    invoke-virtual {p0}, Lcom/nimbusds/jose/JWEHeader;->getEncryptionMethod()Lcom/nimbusds/jose/EncryptionMethod;

    move-result-object v8

    invoke-static {p1, v8, v3, v4}, Lcom/nimbusds/jose/crypto/LegacyConcatKDF;->generateCEK(Ljavax/crypto/SecretKey;Lcom/nimbusds/jose/EncryptionMethod;[B[B)Ljavax/crypto/SecretKey;

    move-result-object v1

    .line 403
    .local v1, "cekAlt":Ljavax/crypto/SecretKey;
    invoke-virtual {p3}, Lcom/nimbusds/jose/util/Base64URL;->decode()[B

    move-result-object v8

    invoke-virtual {p4}, Lcom/nimbusds/jose/util/Base64URL;->decode()[B

    move-result-object v9

    move-object/from16 v0, p6

    invoke-static {v1, v8, v9, v0}, Lcom/nimbusds/jose/crypto/AESCBC;->decrypt(Ljavax/crypto/SecretKey;[B[BLjava/security/Provider;)[B

    move-result-object v7

    .line 405
    .local v7, "plainText":[B
    invoke-virtual {p0}, Lcom/nimbusds/jose/JWEHeader;->getEncryptionMethod()Lcom/nimbusds/jose/EncryptionMethod;

    move-result-object v8

    invoke-static {p1, v8, v3, v4}, Lcom/nimbusds/jose/crypto/LegacyConcatKDF;->generateCIK(Ljavax/crypto/SecretKey;Lcom/nimbusds/jose/EncryptionMethod;[B[B)Ljavax/crypto/SecretKey;

    move-result-object v2

    .line 407
    .local v2, "cik":Ljavax/crypto/SecretKey;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/nimbusds/jose/JWEHeader;->toBase64URL()Lcom/nimbusds/jose/util/Base64URL;

    move-result-object v9

    invoke-virtual {v9}, Lcom/nimbusds/jose/util/Base64URL;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v9, "."

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 408
    invoke-virtual {p2}, Lcom/nimbusds/jose/util/Base64URL;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "."

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 409
    invoke-virtual {p3}, Lcom/nimbusds/jose/util/Base64URL;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "."

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 410
    invoke-virtual {p4}, Lcom/nimbusds/jose/util/Base64URL;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 407
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 412
    .local v6, "macInput":Ljava/lang/String;
    invoke-virtual {v6}, Ljava/lang/String;->getBytes()[B

    move-result-object v8

    move-object/from16 v0, p7

    invoke-static {v2, v8, v0}, Lcom/nimbusds/jose/crypto/HMAC;->compute(Ljavax/crypto/SecretKey;[BLjava/security/Provider;)[B

    move-result-object v5

    .line 414
    .local v5, "mac":[B
    invoke-virtual {p5}, Lcom/nimbusds/jose/util/Base64URL;->decode()[B

    move-result-object v8

    invoke-static {v8, v5}, Lcom/nimbusds/jose/crypto/ConstantTimeUtils;->areEqual([B[B)Z

    move-result v8

    if-nez v8, :cond_2

    .line 416
    new-instance v8, Lcom/nimbusds/jose/JOSEException;

    const-string v9, "HMAC integrity check failed"

    invoke-direct {v8, v9}, Lcom/nimbusds/jose/JOSEException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 419
    :cond_2
    return-object v7
.end method

.method public static encrypt(Ljavax/crypto/SecretKey;[B[BLjava/security/Provider;)[B
    .locals 4
    .param p0, "secretKey"    # Ljavax/crypto/SecretKey;
    .param p1, "iv"    # [B
    .param p2, "plainText"    # [B
    .param p3, "provider"    # Ljava/security/Provider;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/nimbusds/jose/JOSEException;
        }
    .end annotation

    .prologue
    .line 128
    const/4 v2, 0x1

    invoke-static {p0, v2, p1, p3}, Lcom/nimbusds/jose/crypto/AESCBC;->createAESCBCCipher(Ljavax/crypto/SecretKey;Z[BLjava/security/Provider;)Ljavax/crypto/Cipher;

    move-result-object v0

    .line 131
    .local v0, "cipher":Ljavax/crypto/Cipher;
    :try_start_0
    invoke-virtual {v0, p2}, Ljavax/crypto/Cipher;->doFinal([B)[B
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    return-object v2

    .line 133
    :catch_0
    move-exception v1

    .line 135
    .local v1, "e":Ljava/lang/Exception;
    new-instance v2, Lcom/nimbusds/jose/JOSEException;

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v1}, Lcom/nimbusds/jose/JOSEException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
.end method

.method public static encryptAuthenticated(Ljavax/crypto/SecretKey;[B[B[BLjava/security/Provider;Ljava/security/Provider;)Lcom/nimbusds/jose/crypto/AuthenticatedCipherText;
    .locals 9
    .param p0, "secretKey"    # Ljavax/crypto/SecretKey;
    .param p1, "iv"    # [B
    .param p2, "plainText"    # [B
    .param p3, "aad"    # [B
    .param p4, "ceProvider"    # Ljava/security/Provider;
    .param p5, "macProvider"    # Ljava/security/Provider;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/nimbusds/jose/JOSEException;
        }
    .end annotation

    .prologue
    .line 173
    new-instance v3, Lcom/nimbusds/jose/crypto/CompositeKey;

    invoke-direct {v3, p0}, Lcom/nimbusds/jose/crypto/CompositeKey;-><init>(Ljavax/crypto/SecretKey;)V

    .line 176
    .local v3, "compositeKey":Lcom/nimbusds/jose/crypto/CompositeKey;
    invoke-virtual {v3}, Lcom/nimbusds/jose/crypto/CompositeKey;->getAESKey()Ljavax/crypto/SecretKey;

    move-result-object v7

    invoke-static {v7, p1, p2, p4}, Lcom/nimbusds/jose/crypto/AESCBC;->encrypt(Ljavax/crypto/SecretKey;[B[BLjava/security/Provider;)[B

    move-result-object v2

    .line 179
    .local v2, "cipherText":[B
    invoke-static {p3}, Lcom/nimbusds/jose/crypto/AAD;->computeLength([B)[B

    move-result-object v0

    .line 182
    .local v0, "al":[B
    array-length v7, p3

    array-length v8, p1

    add-int/2addr v7, v8

    array-length v8, v2

    add-int/2addr v7, v8

    array-length v8, v0

    add-int v6, v7, v8

    .line 183
    .local v6, "hmacInputLength":I
    invoke-static {v6}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v7

    invoke-virtual {v7, p3}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v5

    .line 184
    .local v5, "hmacInput":[B
    invoke-virtual {v3}, Lcom/nimbusds/jose/crypto/CompositeKey;->getMACKey()Ljavax/crypto/SecretKey;

    move-result-object v7

    invoke-static {v7, v5, p5}, Lcom/nimbusds/jose/crypto/HMAC;->compute(Ljavax/crypto/SecretKey;[BLjava/security/Provider;)[B

    move-result-object v4

    .line 185
    .local v4, "hmac":[B
    invoke-virtual {v3}, Lcom/nimbusds/jose/crypto/CompositeKey;->getTruncatedMACByteLength()I

    move-result v7

    invoke-static {v4, v7}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v1

    .line 187
    .local v1, "authTag":[B
    new-instance v7, Lcom/nimbusds/jose/crypto/AuthenticatedCipherText;

    invoke-direct {v7, v2, v1}, Lcom/nimbusds/jose/crypto/AuthenticatedCipherText;-><init>([B[B)V

    return-object v7
.end method

.method public static encryptWithConcatKDF(Lcom/nimbusds/jose/JWEHeader;Ljavax/crypto/SecretKey;Lcom/nimbusds/jose/util/Base64URL;[B[BLjava/security/Provider;Ljava/security/Provider;)Lcom/nimbusds/jose/crypto/AuthenticatedCipherText;
    .locals 9
    .param p0, "header"    # Lcom/nimbusds/jose/JWEHeader;
    .param p1, "secretKey"    # Ljavax/crypto/SecretKey;
    .param p2, "encryptedKey"    # Lcom/nimbusds/jose/util/Base64URL;
    .param p3, "iv"    # [B
    .param p4, "plainText"    # [B
    .param p5, "ceProvider"    # Ljava/security/Provider;
    .param p6, "macProvider"    # Ljava/security/Provider;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/nimbusds/jose/JOSEException;
        }
    .end annotation

    .prologue
    .line 220
    const/4 v3, 0x0

    .line 222
    .local v3, "epu":[B
    const-string v7, "epu"

    invoke-virtual {p0, v7}, Lcom/nimbusds/jose/JWEHeader;->getCustomParam(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    instance-of v7, v7, Ljava/lang/String;

    if-eqz v7, :cond_0

    .line 224
    new-instance v8, Lcom/nimbusds/jose/util/Base64URL;

    const-string v7, "epu"

    invoke-virtual {p0, v7}, Lcom/nimbusds/jose/JWEHeader;->getCustomParam(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-direct {v8, v7}, Lcom/nimbusds/jose/util/Base64URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8}, Lcom/nimbusds/jose/util/Base64URL;->decode()[B

    move-result-object v3

    .line 227
    :cond_0
    const/4 v4, 0x0

    .line 229
    .local v4, "epv":[B
    const-string v7, "epv"

    invoke-virtual {p0, v7}, Lcom/nimbusds/jose/JWEHeader;->getCustomParam(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    instance-of v7, v7, Ljava/lang/String;

    if-eqz v7, :cond_1

    .line 231
    new-instance v8, Lcom/nimbusds/jose/util/Base64URL;

    const-string v7, "epv"

    invoke-virtual {p0, v7}, Lcom/nimbusds/jose/JWEHeader;->getCustomParam(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-direct {v8, v7}, Lcom/nimbusds/jose/util/Base64URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8}, Lcom/nimbusds/jose/util/Base64URL;->decode()[B

    move-result-object v4

    .line 235
    :cond_1
    invoke-virtual {p0}, Lcom/nimbusds/jose/JWEHeader;->getEncryptionMethod()Lcom/nimbusds/jose/EncryptionMethod;

    move-result-object v7

    invoke-static {p1, v7, v3, v4}, Lcom/nimbusds/jose/crypto/LegacyConcatKDF;->generateCEK(Ljavax/crypto/SecretKey;Lcom/nimbusds/jose/EncryptionMethod;[B[B)Ljavax/crypto/SecretKey;

    move-result-object v0

    .line 237
    .local v0, "altCEK":Ljavax/crypto/SecretKey;
    invoke-static {v0, p3, p4, p5}, Lcom/nimbusds/jose/crypto/AESCBC;->encrypt(Ljavax/crypto/SecretKey;[B[BLjava/security/Provider;)[B

    move-result-object v2

    .line 240
    .local v2, "cipherText":[B
    invoke-virtual {p0}, Lcom/nimbusds/jose/JWEHeader;->getEncryptionMethod()Lcom/nimbusds/jose/EncryptionMethod;

    move-result-object v7

    invoke-static {p1, v7, v3, v4}, Lcom/nimbusds/jose/crypto/LegacyConcatKDF;->generateCIK(Ljavax/crypto/SecretKey;Lcom/nimbusds/jose/EncryptionMethod;[B[B)Ljavax/crypto/SecretKey;

    move-result-object v1

    .line 242
    .local v1, "cik":Ljavax/crypto/SecretKey;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/nimbusds/jose/JWEHeader;->toBase64URL()Lcom/nimbusds/jose/util/Base64URL;

    move-result-object v8

    invoke-virtual {v8}, Lcom/nimbusds/jose/util/Base64URL;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v8, "."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 243
    invoke-virtual {p2}, Lcom/nimbusds/jose/util/Base64URL;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 244
    invoke-static {p3}, Lcom/nimbusds/jose/util/Base64URL;->encode([B)Lcom/nimbusds/jose/util/Base64URL;

    move-result-object v8

    invoke-virtual {v8}, Lcom/nimbusds/jose/util/Base64URL;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 245
    invoke-static {v2}, Lcom/nimbusds/jose/util/Base64URL;->encode([B)Lcom/nimbusds/jose/util/Base64URL;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 242
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 247
    .local v6, "macInput":Ljava/lang/String;
    invoke-virtual {v6}, Ljava/lang/String;->getBytes()[B

    move-result-object v7

    invoke-static {v1, v7, p6}, Lcom/nimbusds/jose/crypto/HMAC;->compute(Ljavax/crypto/SecretKey;[BLjava/security/Provider;)[B

    move-result-object v5

    .line 249
    .local v5, "mac":[B
    new-instance v7, Lcom/nimbusds/jose/crypto/AuthenticatedCipherText;

    invoke-direct {v7, v2, v5}, Lcom/nimbusds/jose/crypto/AuthenticatedCipherText;-><init>([B[B)V

    return-object v7
.end method

.method public static generateIV(Ljava/security/SecureRandom;)[B
    .locals 2
    .param p0, "randomGen"    # Ljava/security/SecureRandom;

    .prologue
    .line 56
    const/16 v1, 0x80

    invoke-static {v1}, Lcom/nimbusds/jose/util/ByteUtils;->byteLength(I)I

    move-result v1

    new-array v0, v1, [B

    .line 57
    .local v0, "bytes":[B
    invoke-virtual {p0, v0}, Ljava/security/SecureRandom;->nextBytes([B)V

    .line 58
    return-object v0
.end method
