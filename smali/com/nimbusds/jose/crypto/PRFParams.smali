.class final Lcom/nimbusds/jose/crypto/PRFParams;
.super Ljava/lang/Object;
.source "PRFParams.java"


# annotations
.annotation runtime Lnet/jcip/annotations/Immutable;
.end annotation


# instance fields
.field private final dkLen:I

.field private final jcaMacAlg:Ljava/lang/String;

.field private final macProvider:Ljava/security/Provider;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/security/Provider;I)V
    .locals 0
    .param p1, "jcaMacAlg"    # Ljava/lang/String;
    .param p2, "macProvider"    # Ljava/security/Provider;
    .param p3, "dkLen"    # I

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    iput-object p1, p0, Lcom/nimbusds/jose/crypto/PRFParams;->jcaMacAlg:Ljava/lang/String;

    .line 53
    iput-object p2, p0, Lcom/nimbusds/jose/crypto/PRFParams;->macProvider:Ljava/security/Provider;

    .line 54
    iput p3, p0, Lcom/nimbusds/jose/crypto/PRFParams;->dkLen:I

    .line 55
    return-void
.end method

.method public static resolve(Lcom/nimbusds/jose/JWEAlgorithm;Ljava/security/Provider;)Lcom/nimbusds/jose/crypto/PRFParams;
    .locals 4
    .param p0, "alg"    # Lcom/nimbusds/jose/JWEAlgorithm;
    .param p1, "macProvider"    # Ljava/security/Provider;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/nimbusds/jose/JOSEException;
        }
    .end annotation

    .prologue
    .line 111
    sget-object v2, Lcom/nimbusds/jose/JWEAlgorithm;->PBES2_HS256_A128KW:Lcom/nimbusds/jose/JWEAlgorithm;

    invoke-virtual {v2, p0}, Lcom/nimbusds/jose/JWEAlgorithm;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 112
    const-string v1, "HmacSHA256"

    .line 113
    .local v1, "jcaMagAlg":Ljava/lang/String;
    const/16 v0, 0x10

    .line 126
    .local v0, "dkLen":I
    :goto_0
    new-instance v2, Lcom/nimbusds/jose/crypto/PRFParams;

    invoke-direct {v2, v1, p1, v0}, Lcom/nimbusds/jose/crypto/PRFParams;-><init>(Ljava/lang/String;Ljava/security/Provider;I)V

    return-object v2

    .line 114
    .end local v0    # "dkLen":I
    .end local v1    # "jcaMagAlg":Ljava/lang/String;
    :cond_0
    sget-object v2, Lcom/nimbusds/jose/JWEAlgorithm;->PBES2_HS384_A192KW:Lcom/nimbusds/jose/JWEAlgorithm;

    invoke-virtual {v2, p0}, Lcom/nimbusds/jose/JWEAlgorithm;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 115
    const-string v1, "HmacSHA384"

    .line 116
    .restart local v1    # "jcaMagAlg":Ljava/lang/String;
    const/16 v0, 0x18

    .line 117
    .restart local v0    # "dkLen":I
    goto :goto_0

    .end local v0    # "dkLen":I
    .end local v1    # "jcaMagAlg":Ljava/lang/String;
    :cond_1
    sget-object v2, Lcom/nimbusds/jose/JWEAlgorithm;->PBES2_HS512_A256KW:Lcom/nimbusds/jose/JWEAlgorithm;

    invoke-virtual {v2, p0}, Lcom/nimbusds/jose/JWEAlgorithm;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 118
    const-string v1, "HmacSHA512"

    .line 119
    .restart local v1    # "jcaMagAlg":Ljava/lang/String;
    const/16 v0, 0x20

    .line 120
    .restart local v0    # "dkLen":I
    goto :goto_0

    .line 121
    .end local v0    # "dkLen":I
    .end local v1    # "jcaMagAlg":Ljava/lang/String;
    :cond_2
    new-instance v2, Lcom/nimbusds/jose/JOSEException;

    .line 123
    sget-object v3, Lcom/nimbusds/jose/crypto/PasswordBasedCryptoProvider;->SUPPORTED_ALGORITHMS:Ljava/util/Set;

    .line 121
    invoke-static {p0, v3}, Lcom/nimbusds/jose/crypto/AlgorithmSupportMessage;->unsupportedJWEAlgorithm(Lcom/nimbusds/jose/JWEAlgorithm;Ljava/util/Collection;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/nimbusds/jose/JOSEException;-><init>(Ljava/lang/String;)V

    throw v2
.end method


# virtual methods
.method public getDerivedKeyByteLength()I
    .locals 1

    .prologue
    .line 87
    iget v0, p0, Lcom/nimbusds/jose/crypto/PRFParams;->dkLen:I

    return v0
.end method

.method public getMACAlgorithm()Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/nimbusds/jose/crypto/PRFParams;->jcaMacAlg:Ljava/lang/String;

    return-object v0
.end method

.method public getMacProvider()Ljava/security/Provider;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/nimbusds/jose/crypto/PRFParams;->macProvider:Ljava/security/Provider;

    return-object v0
.end method
