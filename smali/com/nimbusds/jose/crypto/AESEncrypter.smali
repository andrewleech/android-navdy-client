.class public Lcom/nimbusds/jose/crypto/AESEncrypter;
.super Lcom/nimbusds/jose/crypto/AESCryptoProvider;
.source "AESEncrypter.java"

# interfaces
.implements Lcom/nimbusds/jose/JWEEncrypter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/nimbusds/jose/crypto/AESEncrypter$AlgFamily;
    }
.end annotation

.annotation runtime Lnet/jcip/annotations/ThreadSafe;
.end annotation


# direct methods
.method public constructor <init>(Lcom/nimbusds/jose/jwk/OctetSequenceKey;)V
    .locals 1
    .param p1, "octJWK"    # Lcom/nimbusds/jose/jwk/OctetSequenceKey;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/nimbusds/jose/KeyLengthException;
        }
    .end annotation

    .prologue
    .line 104
    const-string v0, "AES"

    invoke-virtual {p1, v0}, Lcom/nimbusds/jose/jwk/OctetSequenceKey;->toSecretKey(Ljava/lang/String;)Ljavax/crypto/SecretKey;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/nimbusds/jose/crypto/AESEncrypter;-><init>(Ljavax/crypto/SecretKey;)V

    .line 105
    return-void
.end method

.method public constructor <init>(Ljavax/crypto/SecretKey;)V
    .locals 0
    .param p1, "kek"    # Ljavax/crypto/SecretKey;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/nimbusds/jose/KeyLengthException;
        }
    .end annotation

    .prologue
    .line 72
    invoke-direct {p0, p1}, Lcom/nimbusds/jose/crypto/AESCryptoProvider;-><init>(Ljavax/crypto/SecretKey;)V

    .line 73
    return-void
.end method

.method public constructor <init>([B)V
    .locals 2
    .param p1, "keyBytes"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/nimbusds/jose/KeyLengthException;
        }
    .end annotation

    .prologue
    .line 87
    new-instance v0, Ljavax/crypto/spec/SecretKeySpec;

    const-string v1, "AES"

    invoke-direct {v0, p1, v1}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/nimbusds/jose/crypto/AESEncrypter;-><init>(Ljavax/crypto/SecretKey;)V

    .line 88
    return-void
.end method


# virtual methods
.method public encrypt(Lcom/nimbusds/jose/JWEHeader;[B)Lcom/nimbusds/jose/JWECryptoParts;
    .locals 12
    .param p1, "header"    # Lcom/nimbusds/jose/JWEHeader;
    .param p2, "clearText"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/nimbusds/jose/JOSEException;
        }
    .end annotation

    .prologue
    const/16 v11, 0x100

    const/16 v10, 0xc0

    const/16 v9, 0x80

    .line 112
    invoke-virtual {p1}, Lcom/nimbusds/jose/JWEHeader;->getAlgorithm()Lcom/nimbusds/jose/JWEAlgorithm;

    move-result-object v0

    .line 117
    .local v0, "alg":Lcom/nimbusds/jose/JWEAlgorithm;
    sget-object v8, Lcom/nimbusds/jose/JWEAlgorithm;->A128KW:Lcom/nimbusds/jose/JWEAlgorithm;

    invoke-virtual {v0, v8}, Lcom/nimbusds/jose/JWEAlgorithm;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 119
    invoke-virtual {p0}, Lcom/nimbusds/jose/crypto/AESEncrypter;->getKey()Ljavax/crypto/SecretKey;

    move-result-object v8

    invoke-interface {v8}, Ljavax/crypto/SecretKey;->getEncoded()[B

    move-result-object v8

    invoke-static {v8}, Lcom/nimbusds/jose/util/ByteUtils;->bitLength([B)I

    move-result v8

    if-eq v8, v9, :cond_0

    .line 120
    new-instance v8, Lcom/nimbusds/jose/KeyLengthException;

    const-string v9, "The Key Encryption Key (KEK) length must be 128 bits for A128KW encryption"

    invoke-direct {v8, v9}, Lcom/nimbusds/jose/KeyLengthException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 122
    :cond_0
    sget-object v1, Lcom/nimbusds/jose/crypto/AESEncrypter$AlgFamily;->AESKW:Lcom/nimbusds/jose/crypto/AESEncrypter$AlgFamily;

    .line 169
    .local v1, "algFamily":Lcom/nimbusds/jose/crypto/AESEncrypter$AlgFamily;
    :goto_0
    invoke-virtual {p1}, Lcom/nimbusds/jose/JWEHeader;->getEncryptionMethod()Lcom/nimbusds/jose/EncryptionMethod;

    move-result-object v4

    .line 170
    .local v4, "enc":Lcom/nimbusds/jose/EncryptionMethod;
    invoke-virtual {p0}, Lcom/nimbusds/jose/crypto/AESEncrypter;->getJCAContext()Lcom/nimbusds/jose/jca/JWEJCAContext;

    move-result-object v8

    invoke-virtual {v8}, Lcom/nimbusds/jose/jca/JWEJCAContext;->getSecureRandom()Ljava/security/SecureRandom;

    move-result-object v8

    invoke-static {v4, v8}, Lcom/nimbusds/jose/crypto/ContentCryptoProvider;->generateCEK(Lcom/nimbusds/jose/EncryptionMethod;Ljava/security/SecureRandom;)Ljavax/crypto/SecretKey;

    move-result-object v3

    .line 172
    .local v3, "cek":Ljavax/crypto/SecretKey;
    sget-object v8, Lcom/nimbusds/jose/crypto/AESEncrypter$AlgFamily;->AESKW:Lcom/nimbusds/jose/crypto/AESEncrypter$AlgFamily;

    invoke-virtual {v8, v1}, Lcom/nimbusds/jose/crypto/AESEncrypter$AlgFamily;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_c

    .line 174
    invoke-virtual {p0}, Lcom/nimbusds/jose/crypto/AESEncrypter;->getKey()Ljavax/crypto/SecretKey;

    move-result-object v8

    invoke-virtual {p0}, Lcom/nimbusds/jose/crypto/AESEncrypter;->getJCAContext()Lcom/nimbusds/jose/jca/JWEJCAContext;

    move-result-object v9

    invoke-virtual {v9}, Lcom/nimbusds/jose/jca/JWEJCAContext;->getKeyEncryptionProvider()Ljava/security/Provider;

    move-result-object v9

    invoke-static {v3, v8, v9}, Lcom/nimbusds/jose/crypto/AESKW;->wrapCEK(Ljavax/crypto/SecretKey;Ljavax/crypto/SecretKey;Ljava/security/Provider;)[B

    move-result-object v8

    invoke-static {v8}, Lcom/nimbusds/jose/util/Base64URL;->encode([B)Lcom/nimbusds/jose/util/Base64URL;

    move-result-object v5

    .line 175
    .local v5, "encryptedKey":Lcom/nimbusds/jose/util/Base64URL;
    move-object v7, p1

    .line 193
    .local v7, "updatedHeader":Lcom/nimbusds/jose/JWEHeader;
    :goto_1
    invoke-virtual {p0}, Lcom/nimbusds/jose/crypto/AESEncrypter;->getJCAContext()Lcom/nimbusds/jose/jca/JWEJCAContext;

    move-result-object v8

    invoke-static {v7, p2, v3, v5, v8}, Lcom/nimbusds/jose/crypto/ContentCryptoProvider;->encrypt(Lcom/nimbusds/jose/JWEHeader;[BLjavax/crypto/SecretKey;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/jca/JWEJCAContext;)Lcom/nimbusds/jose/JWECryptoParts;

    move-result-object v8

    return-object v8

    .line 124
    .end local v1    # "algFamily":Lcom/nimbusds/jose/crypto/AESEncrypter$AlgFamily;
    .end local v3    # "cek":Ljavax/crypto/SecretKey;
    .end local v4    # "enc":Lcom/nimbusds/jose/EncryptionMethod;
    .end local v5    # "encryptedKey":Lcom/nimbusds/jose/util/Base64URL;
    .end local v7    # "updatedHeader":Lcom/nimbusds/jose/JWEHeader;
    :cond_1
    sget-object v8, Lcom/nimbusds/jose/JWEAlgorithm;->A192KW:Lcom/nimbusds/jose/JWEAlgorithm;

    invoke-virtual {v0, v8}, Lcom/nimbusds/jose/JWEAlgorithm;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 126
    invoke-virtual {p0}, Lcom/nimbusds/jose/crypto/AESEncrypter;->getKey()Ljavax/crypto/SecretKey;

    move-result-object v8

    invoke-interface {v8}, Ljavax/crypto/SecretKey;->getEncoded()[B

    move-result-object v8

    invoke-static {v8}, Lcom/nimbusds/jose/util/ByteUtils;->bitLength([B)I

    move-result v8

    if-eq v8, v10, :cond_2

    .line 127
    new-instance v8, Lcom/nimbusds/jose/KeyLengthException;

    const-string v9, "The Key Encryption Key (KEK) length must be 192 bits for A192KW encryption"

    invoke-direct {v8, v9}, Lcom/nimbusds/jose/KeyLengthException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 129
    :cond_2
    sget-object v1, Lcom/nimbusds/jose/crypto/AESEncrypter$AlgFamily;->AESKW:Lcom/nimbusds/jose/crypto/AESEncrypter$AlgFamily;

    .line 131
    .restart local v1    # "algFamily":Lcom/nimbusds/jose/crypto/AESEncrypter$AlgFamily;
    goto :goto_0

    .end local v1    # "algFamily":Lcom/nimbusds/jose/crypto/AESEncrypter$AlgFamily;
    :cond_3
    sget-object v8, Lcom/nimbusds/jose/JWEAlgorithm;->A256KW:Lcom/nimbusds/jose/JWEAlgorithm;

    invoke-virtual {v0, v8}, Lcom/nimbusds/jose/JWEAlgorithm;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 133
    invoke-virtual {p0}, Lcom/nimbusds/jose/crypto/AESEncrypter;->getKey()Ljavax/crypto/SecretKey;

    move-result-object v8

    invoke-interface {v8}, Ljavax/crypto/SecretKey;->getEncoded()[B

    move-result-object v8

    invoke-static {v8}, Lcom/nimbusds/jose/util/ByteUtils;->bitLength([B)I

    move-result v8

    if-eq v8, v11, :cond_4

    .line 134
    new-instance v8, Lcom/nimbusds/jose/KeyLengthException;

    const-string v9, "The Key Encryption Key (KEK) length must be 256 bits for A256KW encryption"

    invoke-direct {v8, v9}, Lcom/nimbusds/jose/KeyLengthException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 136
    :cond_4
    sget-object v1, Lcom/nimbusds/jose/crypto/AESEncrypter$AlgFamily;->AESKW:Lcom/nimbusds/jose/crypto/AESEncrypter$AlgFamily;

    .line 138
    .restart local v1    # "algFamily":Lcom/nimbusds/jose/crypto/AESEncrypter$AlgFamily;
    goto :goto_0

    .end local v1    # "algFamily":Lcom/nimbusds/jose/crypto/AESEncrypter$AlgFamily;
    :cond_5
    sget-object v8, Lcom/nimbusds/jose/JWEAlgorithm;->A128GCMKW:Lcom/nimbusds/jose/JWEAlgorithm;

    invoke-virtual {v0, v8}, Lcom/nimbusds/jose/JWEAlgorithm;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 140
    invoke-virtual {p0}, Lcom/nimbusds/jose/crypto/AESEncrypter;->getKey()Ljavax/crypto/SecretKey;

    move-result-object v8

    invoke-interface {v8}, Ljavax/crypto/SecretKey;->getEncoded()[B

    move-result-object v8

    invoke-static {v8}, Lcom/nimbusds/jose/util/ByteUtils;->bitLength([B)I

    move-result v8

    if-eq v8, v9, :cond_6

    .line 141
    new-instance v8, Lcom/nimbusds/jose/KeyLengthException;

    const-string v9, "The Key Encryption Key (KEK) length must be 128 bits for A128GCMKW encryption"

    invoke-direct {v8, v9}, Lcom/nimbusds/jose/KeyLengthException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 143
    :cond_6
    sget-object v1, Lcom/nimbusds/jose/crypto/AESEncrypter$AlgFamily;->AESGCMKW:Lcom/nimbusds/jose/crypto/AESEncrypter$AlgFamily;

    .line 145
    .restart local v1    # "algFamily":Lcom/nimbusds/jose/crypto/AESEncrypter$AlgFamily;
    goto/16 :goto_0

    .end local v1    # "algFamily":Lcom/nimbusds/jose/crypto/AESEncrypter$AlgFamily;
    :cond_7
    sget-object v8, Lcom/nimbusds/jose/JWEAlgorithm;->A192GCMKW:Lcom/nimbusds/jose/JWEAlgorithm;

    invoke-virtual {v0, v8}, Lcom/nimbusds/jose/JWEAlgorithm;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_9

    .line 147
    invoke-virtual {p0}, Lcom/nimbusds/jose/crypto/AESEncrypter;->getKey()Ljavax/crypto/SecretKey;

    move-result-object v8

    invoke-interface {v8}, Ljavax/crypto/SecretKey;->getEncoded()[B

    move-result-object v8

    invoke-static {v8}, Lcom/nimbusds/jose/util/ByteUtils;->bitLength([B)I

    move-result v8

    if-eq v8, v10, :cond_8

    .line 148
    new-instance v8, Lcom/nimbusds/jose/KeyLengthException;

    const-string v9, "The Key Encryption Key (KEK) length must be 192 bits for A192GCMKW encryption"

    invoke-direct {v8, v9}, Lcom/nimbusds/jose/KeyLengthException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 150
    :cond_8
    sget-object v1, Lcom/nimbusds/jose/crypto/AESEncrypter$AlgFamily;->AESGCMKW:Lcom/nimbusds/jose/crypto/AESEncrypter$AlgFamily;

    .line 152
    .restart local v1    # "algFamily":Lcom/nimbusds/jose/crypto/AESEncrypter$AlgFamily;
    goto/16 :goto_0

    .end local v1    # "algFamily":Lcom/nimbusds/jose/crypto/AESEncrypter$AlgFamily;
    :cond_9
    sget-object v8, Lcom/nimbusds/jose/JWEAlgorithm;->A256GCMKW:Lcom/nimbusds/jose/JWEAlgorithm;

    invoke-virtual {v0, v8}, Lcom/nimbusds/jose/JWEAlgorithm;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_b

    .line 154
    invoke-virtual {p0}, Lcom/nimbusds/jose/crypto/AESEncrypter;->getKey()Ljavax/crypto/SecretKey;

    move-result-object v8

    invoke-interface {v8}, Ljavax/crypto/SecretKey;->getEncoded()[B

    move-result-object v8

    invoke-static {v8}, Lcom/nimbusds/jose/util/ByteUtils;->bitLength([B)I

    move-result v8

    if-eq v8, v11, :cond_a

    .line 155
    new-instance v8, Lcom/nimbusds/jose/KeyLengthException;

    const-string v9, "The Key Encryption Key (KEK) length must be 256 bits for A256GCMKW encryption"

    invoke-direct {v8, v9}, Lcom/nimbusds/jose/KeyLengthException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 157
    :cond_a
    sget-object v1, Lcom/nimbusds/jose/crypto/AESEncrypter$AlgFamily;->AESGCMKW:Lcom/nimbusds/jose/crypto/AESEncrypter$AlgFamily;

    .line 159
    .restart local v1    # "algFamily":Lcom/nimbusds/jose/crypto/AESEncrypter$AlgFamily;
    goto/16 :goto_0

    .line 161
    .end local v1    # "algFamily":Lcom/nimbusds/jose/crypto/AESEncrypter$AlgFamily;
    :cond_b
    new-instance v8, Lcom/nimbusds/jose/JOSEException;

    sget-object v9, Lcom/nimbusds/jose/crypto/AESEncrypter;->SUPPORTED_ALGORITHMS:Ljava/util/Set;

    invoke-static {v0, v9}, Lcom/nimbusds/jose/crypto/AlgorithmSupportMessage;->unsupportedJWEAlgorithm(Lcom/nimbusds/jose/JWEAlgorithm;Ljava/util/Collection;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Lcom/nimbusds/jose/JOSEException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 177
    .restart local v1    # "algFamily":Lcom/nimbusds/jose/crypto/AESEncrypter$AlgFamily;
    .restart local v3    # "cek":Ljavax/crypto/SecretKey;
    .restart local v4    # "enc":Lcom/nimbusds/jose/EncryptionMethod;
    :cond_c
    sget-object v8, Lcom/nimbusds/jose/crypto/AESEncrypter$AlgFamily;->AESGCMKW:Lcom/nimbusds/jose/crypto/AESEncrypter$AlgFamily;

    invoke-virtual {v8, v1}, Lcom/nimbusds/jose/crypto/AESEncrypter$AlgFamily;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_d

    .line 179
    invoke-virtual {p0}, Lcom/nimbusds/jose/crypto/AESEncrypter;->getJCAContext()Lcom/nimbusds/jose/jca/JWEJCAContext;

    move-result-object v8

    invoke-virtual {v8}, Lcom/nimbusds/jose/jca/JWEJCAContext;->getSecureRandom()Ljava/security/SecureRandom;

    move-result-object v8

    invoke-static {v8}, Lcom/nimbusds/jose/crypto/AESGCM;->generateIV(Ljava/security/SecureRandom;)[B

    move-result-object v6

    .line 180
    .local v6, "keyIV":[B
    invoke-virtual {p0}, Lcom/nimbusds/jose/crypto/AESEncrypter;->getKey()Ljavax/crypto/SecretKey;

    move-result-object v8

    invoke-virtual {p0}, Lcom/nimbusds/jose/crypto/AESEncrypter;->getJCAContext()Lcom/nimbusds/jose/jca/JWEJCAContext;

    move-result-object v9

    invoke-virtual {v9}, Lcom/nimbusds/jose/jca/JWEJCAContext;->getKeyEncryptionProvider()Ljava/security/Provider;

    move-result-object v9

    invoke-static {v3, v6, v8, v9}, Lcom/nimbusds/jose/crypto/AESGCMKW;->encryptCEK(Ljavax/crypto/SecretKey;[BLjavax/crypto/SecretKey;Ljava/security/Provider;)Lcom/nimbusds/jose/crypto/AuthenticatedCipherText;

    move-result-object v2

    .line 181
    .local v2, "authCiphCEK":Lcom/nimbusds/jose/crypto/AuthenticatedCipherText;
    invoke-virtual {v2}, Lcom/nimbusds/jose/crypto/AuthenticatedCipherText;->getCipherText()[B

    move-result-object v8

    invoke-static {v8}, Lcom/nimbusds/jose/util/Base64URL;->encode([B)Lcom/nimbusds/jose/util/Base64URL;

    move-result-object v5

    .line 184
    .restart local v5    # "encryptedKey":Lcom/nimbusds/jose/util/Base64URL;
    new-instance v8, Lcom/nimbusds/jose/JWEHeader$Builder;

    invoke-direct {v8, p1}, Lcom/nimbusds/jose/JWEHeader$Builder;-><init>(Lcom/nimbusds/jose/JWEHeader;)V

    .line 185
    invoke-static {v6}, Lcom/nimbusds/jose/util/Base64URL;->encode([B)Lcom/nimbusds/jose/util/Base64URL;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/nimbusds/jose/JWEHeader$Builder;->iv(Lcom/nimbusds/jose/util/Base64URL;)Lcom/nimbusds/jose/JWEHeader$Builder;

    move-result-object v8

    .line 186
    invoke-virtual {v2}, Lcom/nimbusds/jose/crypto/AuthenticatedCipherText;->getAuthenticationTag()[B

    move-result-object v9

    invoke-static {v9}, Lcom/nimbusds/jose/util/Base64URL;->encode([B)Lcom/nimbusds/jose/util/Base64URL;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/nimbusds/jose/JWEHeader$Builder;->authTag(Lcom/nimbusds/jose/util/Base64URL;)Lcom/nimbusds/jose/JWEHeader$Builder;

    move-result-object v8

    .line 187
    invoke-virtual {v8}, Lcom/nimbusds/jose/JWEHeader$Builder;->build()Lcom/nimbusds/jose/JWEHeader;

    move-result-object v7

    .line 188
    .restart local v7    # "updatedHeader":Lcom/nimbusds/jose/JWEHeader;
    goto/16 :goto_1

    .line 190
    .end local v2    # "authCiphCEK":Lcom/nimbusds/jose/crypto/AuthenticatedCipherText;
    .end local v5    # "encryptedKey":Lcom/nimbusds/jose/util/Base64URL;
    .end local v6    # "keyIV":[B
    .end local v7    # "updatedHeader":Lcom/nimbusds/jose/JWEHeader;
    :cond_d
    new-instance v8, Lcom/nimbusds/jose/JOSEException;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Unexpected JWE algorithm: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Lcom/nimbusds/jose/JOSEException;-><init>(Ljava/lang/String;)V

    throw v8
.end method

.method public bridge synthetic getJCAContext()Lcom/nimbusds/jose/jca/JWEJCAContext;
    .locals 1

    .prologue
    .line 1
    invoke-super {p0}, Lcom/nimbusds/jose/crypto/AESCryptoProvider;->getJCAContext()Lcom/nimbusds/jose/jca/JWEJCAContext;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getKey()Ljavax/crypto/SecretKey;
    .locals 1

    .prologue
    .line 1
    invoke-super {p0}, Lcom/nimbusds/jose/crypto/AESCryptoProvider;->getKey()Ljavax/crypto/SecretKey;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic supportedEncryptionMethods()Ljava/util/Set;
    .locals 1

    .prologue
    .line 1
    invoke-super {p0}, Lcom/nimbusds/jose/crypto/AESCryptoProvider;->supportedEncryptionMethods()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic supportedJWEAlgorithms()Ljava/util/Set;
    .locals 1

    .prologue
    .line 1
    invoke-super {p0}, Lcom/nimbusds/jose/crypto/AESCryptoProvider;->supportedJWEAlgorithms()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method
