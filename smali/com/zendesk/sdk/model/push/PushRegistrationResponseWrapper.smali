.class public Lcom/zendesk/sdk/model/push/PushRegistrationResponseWrapper;
.super Ljava/lang/Object;
.source "PushRegistrationResponseWrapper.java"


# instance fields
.field private registrationResponse:Lcom/zendesk/sdk/model/push/PushRegistrationResponse;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "push_notification_device"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getRegistrationResponse()Lcom/zendesk/sdk/model/push/PushRegistrationResponse;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 23
    iget-object v0, p0, Lcom/zendesk/sdk/model/push/PushRegistrationResponseWrapper;->registrationResponse:Lcom/zendesk/sdk/model/push/PushRegistrationResponse;

    return-object v0
.end method
