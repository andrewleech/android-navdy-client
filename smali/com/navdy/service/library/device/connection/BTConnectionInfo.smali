.class public Lcom/navdy/service/library/device/connection/BTConnectionInfo;
.super Lcom/navdy/service/library/device/connection/ConnectionInfo;
.source "BTConnectionInfo.java"


# instance fields
.field mAddress:Lcom/navdy/service/library/device/connection/ServiceAddress;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "address"
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/navdy/service/library/device/NavdyDeviceId;Lcom/navdy/service/library/device/connection/ServiceAddress;Lcom/navdy/service/library/device/connection/ConnectionType;)V
    .locals 0
    .param p1, "id"    # Lcom/navdy/service/library/device/NavdyDeviceId;
    .param p2, "address"    # Lcom/navdy/service/library/device/connection/ServiceAddress;
    .param p3, "connectionType"    # Lcom/navdy/service/library/device/connection/ConnectionType;

    .prologue
    .line 16
    invoke-direct {p0, p1, p3}, Lcom/navdy/service/library/device/connection/ConnectionInfo;-><init>(Lcom/navdy/service/library/device/NavdyDeviceId;Lcom/navdy/service/library/device/connection/ConnectionType;)V

    .line 17
    iput-object p2, p0, Lcom/navdy/service/library/device/connection/BTConnectionInfo;->mAddress:Lcom/navdy/service/library/device/connection/ServiceAddress;

    .line 18
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/device/NavdyDeviceId;Ljava/util/UUID;Lcom/navdy/service/library/device/connection/ConnectionType;)V
    .locals 3
    .param p1, "id"    # Lcom/navdy/service/library/device/NavdyDeviceId;
    .param p2, "serviceUUID"    # Ljava/util/UUID;
    .param p3, "connectionType"    # Lcom/navdy/service/library/device/connection/ConnectionType;

    .prologue
    .line 21
    invoke-direct {p0, p1, p3}, Lcom/navdy/service/library/device/connection/ConnectionInfo;-><init>(Lcom/navdy/service/library/device/NavdyDeviceId;Lcom/navdy/service/library/device/connection/ConnectionType;)V

    .line 22
    invoke-virtual {p1}, Lcom/navdy/service/library/device/NavdyDeviceId;->getBluetoothAddress()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 23
    new-instance v0, Lcom/navdy/service/library/device/connection/ServiceAddress;

    invoke-virtual {p1}, Lcom/navdy/service/library/device/NavdyDeviceId;->getBluetoothAddress()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/navdy/service/library/device/connection/ServiceAddress;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/navdy/service/library/device/connection/BTConnectionInfo;->mAddress:Lcom/navdy/service/library/device/connection/ServiceAddress;

    .line 27
    return-void

    .line 25
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "BT address missing"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 34
    if-ne p0, p1, :cond_1

    const/4 v1, 0x1

    .line 40
    :cond_0
    :goto_0
    return v1

    .line 35
    :cond_1
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-ne v2, v3, :cond_0

    .line 36
    invoke-super {p0, p1}, Lcom/navdy/service/library/device/connection/ConnectionInfo;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 38
    check-cast v0, Lcom/navdy/service/library/device/connection/BTConnectionInfo;

    .line 40
    .local v0, "that":Lcom/navdy/service/library/device/connection/BTConnectionInfo;
    iget-object v1, p0, Lcom/navdy/service/library/device/connection/BTConnectionInfo;->mAddress:Lcom/navdy/service/library/device/connection/ServiceAddress;

    iget-object v2, v0, Lcom/navdy/service/library/device/connection/BTConnectionInfo;->mAddress:Lcom/navdy/service/library/device/connection/ServiceAddress;

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/device/connection/ServiceAddress;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public getAddress()Lcom/navdy/service/library/device/connection/ServiceAddress;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/navdy/service/library/device/connection/BTConnectionInfo;->mAddress:Lcom/navdy/service/library/device/connection/ServiceAddress;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 45
    invoke-super {p0}, Lcom/navdy/service/library/device/connection/ConnectionInfo;->hashCode()I

    move-result v0

    .line 46
    .local v0, "result":I
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/navdy/service/library/device/connection/BTConnectionInfo;->mAddress:Lcom/navdy/service/library/device/connection/ServiceAddress;

    invoke-virtual {v2}, Lcom/navdy/service/library/device/connection/ServiceAddress;->hashCode()I

    move-result v2

    add-int v0, v1, v2

    .line 47
    return v0
.end method
