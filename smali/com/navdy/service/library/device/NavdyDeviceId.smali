.class public Lcom/navdy/service/library/device/NavdyDeviceId;
.super Ljava/lang/Object;
.source "NavdyDeviceId.java"


# annotations
.annotation runtime Lcom/google/gson/annotations/JsonAdapter;
    value = Lcom/navdy/service/library/device/NavdyDeviceId$Adapter;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/device/NavdyDeviceId$Adapter;,
        Lcom/navdy/service/library/device/NavdyDeviceId$Type;
    }
.end annotation


# static fields
.field protected static final ATTRIBUTE_NAME:Ljava/lang/String; = "N"

.field protected static final ATTRIBUTE_SEPARATOR:Ljava/lang/String; = ";"

.field protected static final RESERVED_CHARACTER_REGEX:Ljava/lang/String; = ";|/"

.field public static final UNKNOWN_ID:Lcom/navdy/service/library/device/NavdyDeviceId;

.field protected static final VALUE_SEPARATOR:Ljava/lang/String; = "/"

.field static sThisDeviceId:Lcom/navdy/service/library/device/NavdyDeviceId;


# instance fields
.field protected mDeviceId:Ljava/lang/String;

.field protected mDeviceIdType:Lcom/navdy/service/library/device/NavdyDeviceId$Type;

.field protected mDeviceName:Ljava/lang/String;

.field protected mSerializedDeviceId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 29
    new-instance v0, Lcom/navdy/service/library/device/NavdyDeviceId;

    const-string v1, "UNK/FF:FF:FF:FF:FF:FF;N/Unknown"

    invoke-direct {v0, v1}, Lcom/navdy/service/library/device/NavdyDeviceId;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/service/library/device/NavdyDeviceId;->UNKNOWN_ID:Lcom/navdy/service/library/device/NavdyDeviceId;

    return-void
.end method

.method public constructor <init>(Landroid/bluetooth/BluetoothDevice;)V
    .locals 3
    .param p1, "device"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 79
    sget-object v1, Lcom/navdy/service/library/device/NavdyDeviceId$Type;->BT:Lcom/navdy/service/library/device/NavdyDeviceId$Type;

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "unknown"

    :goto_0
    invoke-direct {p0, v1, v2, v0}, Lcom/navdy/service/library/device/NavdyDeviceId;-><init>(Lcom/navdy/service/library/device/NavdyDeviceId$Type;Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    return-void

    .line 79
    :cond_0
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/navdy/service/library/device/NavdyDeviceId$Type;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "type"    # Lcom/navdy/service/library/device/NavdyDeviceId$Type;
    .param p2, "id"    # Ljava/lang/String;
    .param p3, "deviceName"    # Ljava/lang/String;

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    iput-object p1, p0, Lcom/navdy/service/library/device/NavdyDeviceId;->mDeviceIdType:Lcom/navdy/service/library/device/NavdyDeviceId$Type;

    .line 73
    invoke-virtual {p0, p2}, Lcom/navdy/service/library/device/NavdyDeviceId;->normalizeDeviceIdString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/device/NavdyDeviceId;->mDeviceId:Ljava/lang/String;

    .line 74
    invoke-virtual {p0, p3}, Lcom/navdy/service/library/device/NavdyDeviceId;->normalizeDeviceName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/device/NavdyDeviceId;->mDeviceName:Ljava/lang/String;

    .line 75
    invoke-direct {p0}, Lcom/navdy/service/library/device/NavdyDeviceId;->createSerializedDeviceId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/device/NavdyDeviceId;->mSerializedDeviceId:Ljava/lang/String;

    .line 76
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "serializedDeviceId"    # Ljava/lang/String;

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    if-eqz p1, :cond_0

    const-string v0, ""

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 62
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cannot create empty NavdyDeviceId"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 65
    :cond_1
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/device/NavdyDeviceId;->parseDeviceIdString(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 66
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid device ID string."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 68
    :cond_2
    invoke-direct {p0}, Lcom/navdy/service/library/device/NavdyDeviceId;->createSerializedDeviceId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/device/NavdyDeviceId;->mSerializedDeviceId:Ljava/lang/String;

    .line 69
    return-void
.end method

.method private createSerializedDeviceId()Ljava/lang/String;
    .locals 3

    .prologue
    .line 109
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/navdy/service/library/device/NavdyDeviceId;->mDeviceIdType:Lcom/navdy/service/library/device/NavdyDeviceId$Type;

    invoke-virtual {v2}, Lcom/navdy/service/library/device/NavdyDeviceId$Type;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/service/library/device/NavdyDeviceId;->mDeviceId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 110
    .local v0, "deviceId":Ljava/lang/String;
    iget-object v1, p0, Lcom/navdy/service/library/device/NavdyDeviceId;->mDeviceName:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 111
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ";N/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/service/library/device/NavdyDeviceId;->mDeviceName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 113
    :cond_0
    return-object v0
.end method

.method public static getThisDevice(Landroid/content/Context;)Lcom/navdy/service/library/device/NavdyDeviceId;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 192
    sget-object v4, Lcom/navdy/service/library/device/NavdyDeviceId;->sThisDeviceId:Lcom/navdy/service/library/device/NavdyDeviceId;

    if-eqz v4, :cond_0

    sget-object v4, Lcom/navdy/service/library/device/NavdyDeviceId;->sThisDeviceId:Lcom/navdy/service/library/device/NavdyDeviceId;

    sget-object v5, Lcom/navdy/service/library/device/NavdyDeviceId;->UNKNOWN_ID:Lcom/navdy/service/library/device/NavdyDeviceId;

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/device/NavdyDeviceId;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 193
    :cond_0
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v1

    .line 196
    .local v1, "btAdapter":Landroid/bluetooth/BluetoothAdapter;
    if-nez v1, :cond_3

    .line 197
    if-eqz p0, :cond_1

    .line 198
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "android_id"

    invoke-static {v4, v5}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 200
    .local v0, "androidId":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 201
    sget-object v4, Lcom/navdy/service/library/device/NavdyDeviceId;->UNKNOWN_ID:Lcom/navdy/service/library/device/NavdyDeviceId;

    sput-object v4, Lcom/navdy/service/library/device/NavdyDeviceId;->sThisDeviceId:Lcom/navdy/service/library/device/NavdyDeviceId;

    .line 220
    .end local v0    # "androidId":Ljava/lang/String;
    .end local v1    # "btAdapter":Landroid/bluetooth/BluetoothAdapter;
    :cond_1
    :goto_0
    sget-object v4, Lcom/navdy/service/library/device/NavdyDeviceId;->sThisDeviceId:Lcom/navdy/service/library/device/NavdyDeviceId;

    return-object v4

    .line 203
    .restart local v0    # "androidId":Ljava/lang/String;
    .restart local v1    # "btAdapter":Landroid/bluetooth/BluetoothAdapter;
    :cond_2
    new-instance v4, Lcom/navdy/service/library/device/NavdyDeviceId;

    sget-object v5, Lcom/navdy/service/library/device/NavdyDeviceId$Type;->EMU:Lcom/navdy/service/library/device/NavdyDeviceId$Type;

    const-string v6, "emulator"

    invoke-direct {v4, v5, v0, v6}, Lcom/navdy/service/library/device/NavdyDeviceId;-><init>(Lcom/navdy/service/library/device/NavdyDeviceId$Type;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v4, Lcom/navdy/service/library/device/NavdyDeviceId;->sThisDeviceId:Lcom/navdy/service/library/device/NavdyDeviceId;

    goto :goto_0

    .line 207
    .end local v0    # "androidId":Ljava/lang/String;
    :cond_3
    invoke-virtual {v1}, Landroid/bluetooth/BluetoothAdapter;->getAddress()Ljava/lang/String;

    move-result-object v2

    .line 209
    .local v2, "btAddr":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 210
    sget-object v4, Lcom/navdy/service/library/device/NavdyDeviceId;->UNKNOWN_ID:Lcom/navdy/service/library/device/NavdyDeviceId;

    sput-object v4, Lcom/navdy/service/library/device/NavdyDeviceId;->sThisDeviceId:Lcom/navdy/service/library/device/NavdyDeviceId;

    goto :goto_0

    .line 212
    :cond_4
    invoke-virtual {v1}, Landroid/bluetooth/BluetoothAdapter;->getName()Ljava/lang/String;

    move-result-object v3

    .line 215
    .local v3, "deviceName":Ljava/lang/String;
    new-instance v4, Lcom/navdy/service/library/device/NavdyDeviceId;

    sget-object v5, Lcom/navdy/service/library/device/NavdyDeviceId$Type;->BT:Lcom/navdy/service/library/device/NavdyDeviceId$Type;

    invoke-direct {v4, v5, v2, v3}, Lcom/navdy/service/library/device/NavdyDeviceId;-><init>(Lcom/navdy/service/library/device/NavdyDeviceId$Type;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v4, Lcom/navdy/service/library/device/NavdyDeviceId;->sThisDeviceId:Lcom/navdy/service/library/device/NavdyDeviceId;

    goto :goto_0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 84
    if-ne p0, p1, :cond_1

    .line 89
    :cond_0
    :goto_0
    return v1

    .line 85
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    :cond_2
    move v1, v2

    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 87
    check-cast v0, Lcom/navdy/service/library/device/NavdyDeviceId;

    .line 89
    .local v0, "deviceId":Lcom/navdy/service/library/device/NavdyDeviceId;
    iget-object v3, p0, Lcom/navdy/service/library/device/NavdyDeviceId;->mDeviceId:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/device/NavdyDeviceId;->mDeviceId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/navdy/service/library/device/NavdyDeviceId;->mDeviceIdType:Lcom/navdy/service/library/device/NavdyDeviceId$Type;

    iget-object v4, v0, Lcom/navdy/service/library/device/NavdyDeviceId;->mDeviceIdType:Lcom/navdy/service/library/device/NavdyDeviceId$Type;

    if-eq v3, v4, :cond_0

    :cond_4
    move v1, v2

    goto :goto_0
.end method

.method public getBluetoothAddress()Ljava/lang/String;
    .locals 2

    .prologue
    .line 170
    iget-object v0, p0, Lcom/navdy/service/library/device/NavdyDeviceId;->mDeviceIdType:Lcom/navdy/service/library/device/NavdyDeviceId$Type;

    sget-object v1, Lcom/navdy/service/library/device/NavdyDeviceId$Type;->BT:Lcom/navdy/service/library/device/NavdyDeviceId$Type;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/navdy/service/library/device/NavdyDeviceId;->mDeviceIdType:Lcom/navdy/service/library/device/NavdyDeviceId$Type;

    sget-object v1, Lcom/navdy/service/library/device/NavdyDeviceId$Type;->EA:Lcom/navdy/service/library/device/NavdyDeviceId$Type;

    if-ne v0, v1, :cond_1

    .line 171
    :cond_0
    iget-object v0, p0, Lcom/navdy/service/library/device/NavdyDeviceId;->mDeviceId:Ljava/lang/String;

    .line 173
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getDeviceName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 182
    iget-object v0, p0, Lcom/navdy/service/library/device/NavdyDeviceId;->mDeviceName:Ljava/lang/String;

    return-object v0
.end method

.method public getDisplayName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 178
    iget-object v0, p0, Lcom/navdy/service/library/device/NavdyDeviceId;->mDeviceName:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/navdy/service/library/device/NavdyDeviceId;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/navdy/service/library/device/NavdyDeviceId;->mDeviceName:Ljava/lang/String;

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 95
    iget-object v1, p0, Lcom/navdy/service/library/device/NavdyDeviceId;->mDeviceId:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 96
    .local v0, "result":I
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/navdy/service/library/device/NavdyDeviceId;->mDeviceIdType:Lcom/navdy/service/library/device/NavdyDeviceId$Type;

    invoke-virtual {v2}, Lcom/navdy/service/library/device/NavdyDeviceId$Type;->hashCode()I

    move-result v2

    add-int v0, v1, v2

    .line 97
    return v0
.end method

.method protected normalizeDeviceIdString(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "deviceId"    # Ljava/lang/String;

    .prologue
    .line 116
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p1, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected normalizeDeviceName(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "deviceName"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 120
    if-nez p1, :cond_1

    move-object v0, v1

    .line 129
    :cond_0
    :goto_0
    return-object v0

    .line 124
    :cond_1
    const-string v2, ";|/"

    const-string v3, ""

    invoke-virtual {p1, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 125
    .local v0, "name":Ljava/lang/String;
    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move-object v0, v1

    .line 126
    goto :goto_0
.end method

.method protected parseDeviceIdString(Ljava/lang/String;)Z
    .locals 12
    .param p1, "deviceIdString"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 133
    const-string v8, ";"

    invoke-virtual {p1, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 135
    .local v1, "attributes":[Ljava/lang/String;
    array-length v8, v1

    if-nez v8, :cond_1

    .line 162
    :cond_0
    :goto_0
    return v6

    .line 139
    :cond_1
    array-length v9, v1

    move v8, v6

    :goto_1
    if-ge v8, v9, :cond_3

    aget-object v4, v1, v8

    .line 140
    .local v4, "unparsedAttribute":Ljava/lang/String;
    const-string v10, "/"

    invoke-virtual {v4, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 142
    .local v3, "idParts":[Ljava/lang/String;
    array-length v10, v3

    const/4 v11, 0x2

    if-lt v10, v11, :cond_0

    .line 146
    aget-object v0, v3, v6

    .line 147
    .local v0, "attributeText":Ljava/lang/String;
    aget-object v5, v3, v7

    .line 149
    .local v5, "valueText":Ljava/lang/String;
    const-string v10, "N"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 150
    invoke-virtual {p0, v5}, Lcom/navdy/service/library/device/NavdyDeviceId;->normalizeDeviceName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    iput-object v10, p0, Lcom/navdy/service/library/device/NavdyDeviceId;->mDeviceName:Ljava/lang/String;

    .line 139
    :goto_2
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 153
    :cond_2
    const/4 v10, 0x0

    :try_start_0
    aget-object v10, v3, v10

    invoke-static {v10}, Lcom/navdy/service/library/device/NavdyDeviceId$Type;->valueOf(Ljava/lang/String;)Lcom/navdy/service/library/device/NavdyDeviceId$Type;

    move-result-object v10

    iput-object v10, p0, Lcom/navdy/service/library/device/NavdyDeviceId;->mDeviceIdType:Lcom/navdy/service/library/device/NavdyDeviceId$Type;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 158
    aget-object v10, v3, v7

    invoke-virtual {p0, v10}, Lcom/navdy/service/library/device/NavdyDeviceId;->normalizeDeviceIdString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    iput-object v10, p0, Lcom/navdy/service/library/device/NavdyDeviceId;->mDeviceId:Ljava/lang/String;

    goto :goto_2

    .line 154
    :catch_0
    move-exception v2

    .line 155
    .local v2, "e":Ljava/lang/IllegalArgumentException;
    new-instance v6, Ljava/lang/IllegalArgumentException;

    const-string v7, "Invalid device ID type"

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    .end local v0    # "attributeText":Ljava/lang/String;
    .end local v2    # "e":Ljava/lang/IllegalArgumentException;
    .end local v3    # "idParts":[Ljava/lang/String;
    .end local v4    # "unparsedAttribute":Ljava/lang/String;
    .end local v5    # "valueText":Ljava/lang/String;
    :cond_3
    move v6, v7

    .line 162
    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/navdy/service/library/device/NavdyDeviceId;->mSerializedDeviceId:Ljava/lang/String;

    return-object v0
.end method
