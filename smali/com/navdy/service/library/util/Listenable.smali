.class public abstract Lcom/navdy/service/library/util/Listenable;
.super Ljava/lang/Object;
.source "Listenable.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/util/Listenable$EventDispatcher;,
        Lcom/navdy/service/library/util/Listenable$Listener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/navdy/service/library/util/Listenable$Listener;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final listenerLock:Ljava/lang/Object;

.field private final logger:Lcom/navdy/service/library/log/Logger;

.field protected mListeners:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<TT;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 40
    .local p0, "this":Lcom/navdy/service/library/util/Listenable;, "Lcom/navdy/service/library/util/Listenable<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/navdy/service/library/util/Listenable;->logger:Lcom/navdy/service/library/log/Logger;

    .line 38
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/navdy/service/library/util/Listenable;->listenerLock:Ljava/lang/Object;

    .line 41
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/navdy/service/library/util/Listenable;->mListeners:Ljava/util/HashSet;

    .line 42
    return-void
.end method


# virtual methods
.method public addListener(Lcom/navdy/service/library/util/Listenable$Listener;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)Z"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/navdy/service/library/util/Listenable;, "Lcom/navdy/service/library/util/Listenable<TT;>;"
    .local p1, "newListener":Lcom/navdy/service/library/util/Listenable$Listener;, "TT;"
    const/4 v3, 0x0

    .line 50
    if-nez p1, :cond_0

    .line 51
    iget-object v4, p0, Lcom/navdy/service/library/util/Listenable;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "attempted to add null listener"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 72
    :goto_0
    return v3

    .line 55
    :cond_0
    iget-object v4, p0, Lcom/navdy/service/library/util/Listenable;->listenerLock:Ljava/lang/Object;

    monitor-enter v4

    .line 56
    :try_start_0
    iget-object v5, p0, Lcom/navdy/service/library/util/Listenable;->mListeners:Ljava/util/HashSet;

    invoke-virtual {v5}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 58
    .local v0, "iterator":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 59
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/ref/WeakReference;

    .line 61
    .local v2, "listenerRef":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<TT;>;"
    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/service/library/util/Listenable$Listener;

    .line 63
    .local v1, "listener":Lcom/navdy/service/library/util/Listenable$Listener;, "TT;"
    if-nez v1, :cond_2

    .line 64
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    goto :goto_1

    .line 71
    .end local v0    # "iterator":Ljava/util/Iterator;
    .end local v1    # "listener":Lcom/navdy/service/library/util/Listenable$Listener;, "TT;"
    .end local v2    # "listenerRef":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<TT;>;"
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .line 65
    .restart local v0    # "iterator":Ljava/util/Iterator;
    .restart local v1    # "listener":Lcom/navdy/service/library/util/Listenable$Listener;, "TT;"
    .restart local v2    # "listenerRef":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<TT;>;"
    :cond_2
    :try_start_1
    invoke-virtual {v1, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 66
    monitor-exit v4

    goto :goto_0

    .line 70
    .end local v1    # "listener":Lcom/navdy/service/library/util/Listenable$Listener;, "TT;"
    .end local v2    # "listenerRef":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<TT;>;"
    :cond_3
    iget-object v3, p0, Lcom/navdy/service/library/util/Listenable;->mListeners:Ljava/util/HashSet;

    new-instance v5, Ljava/lang/ref/WeakReference;

    invoke-direct {v5, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v3, v5}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 71
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 72
    const/4 v3, 0x1

    goto :goto_0
.end method

.method protected dispatchToListeners(Lcom/navdy/service/library/util/Listenable$EventDispatcher;)V
    .locals 6
    .param p1, "dispatcher"    # Lcom/navdy/service/library/util/Listenable$EventDispatcher;

    .prologue
    .line 113
    .local p0, "this":Lcom/navdy/service/library/util/Listenable;, "Lcom/navdy/service/library/util/Listenable<TT;>;"
    iget-object v5, p0, Lcom/navdy/service/library/util/Listenable;->listenerLock:Ljava/lang/Object;

    monitor-enter v5

    .line 114
    :try_start_0
    iget-object v4, p0, Lcom/navdy/service/library/util/Listenable;->mListeners:Ljava/util/HashSet;

    invoke-virtual {v4}, Ljava/util/HashSet;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashSet;

    .line 115
    .local v0, "clonedListeners":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/ref/WeakReference<TT;>;>;"
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 117
    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 119
    .local v1, "iterator":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 120
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/ref/WeakReference;

    .line 122
    .local v3, "listenerRef":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<TT;>;"
    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/navdy/service/library/util/Listenable$Listener;

    .line 124
    .local v2, "listener":Lcom/navdy/service/library/util/Listenable$Listener;, "TT;"
    if-nez v2, :cond_0

    .line 125
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 115
    .end local v0    # "clonedListeners":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/ref/WeakReference<TT;>;>;"
    .end local v1    # "iterator":Ljava/util/Iterator;
    .end local v2    # "listener":Lcom/navdy/service/library/util/Listenable$Listener;, "TT;"
    .end local v3    # "listenerRef":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<TT;>;"
    :catchall_0
    move-exception v4

    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v4

    .line 127
    .restart local v0    # "clonedListeners":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/ref/WeakReference<TT;>;>;"
    .restart local v1    # "iterator":Ljava/util/Iterator;
    .restart local v2    # "listener":Lcom/navdy/service/library/util/Listenable$Listener;, "TT;"
    .restart local v3    # "listenerRef":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<TT;>;"
    :cond_0
    invoke-interface {p1, p0, v2}, Lcom/navdy/service/library/util/Listenable$EventDispatcher;->dispatchEvent(Lcom/navdy/service/library/util/Listenable;Lcom/navdy/service/library/util/Listenable$Listener;)V

    goto :goto_0

    .line 130
    .end local v2    # "listener":Lcom/navdy/service/library/util/Listenable$Listener;, "TT;"
    .end local v3    # "listenerRef":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<TT;>;"
    :cond_1
    return-void
.end method

.method public removeListener(Lcom/navdy/service/library/util/Listenable$Listener;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)Z"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/navdy/service/library/util/Listenable;, "Lcom/navdy/service/library/util/Listenable<TT;>;"
    .local p1, "listenerToRemove":Lcom/navdy/service/library/util/Listenable$Listener;, "TT;"
    const/4 v3, 0x0

    .line 81
    if-nez p1, :cond_0

    .line 82
    iget-object v4, p0, Lcom/navdy/service/library/util/Listenable;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "attempted to remove null listener"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 103
    :goto_0
    return v3

    .line 86
    :cond_0
    iget-object v4, p0, Lcom/navdy/service/library/util/Listenable;->listenerLock:Ljava/lang/Object;

    monitor-enter v4

    .line 87
    :try_start_0
    iget-object v5, p0, Lcom/navdy/service/library/util/Listenable;->mListeners:Ljava/util/HashSet;

    invoke-virtual {v5}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 89
    .local v0, "iterator":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 90
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/ref/WeakReference;

    .line 92
    .local v2, "listenerRef":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<TT;>;"
    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/service/library/util/Listenable$Listener;

    .line 94
    .local v1, "listener":Lcom/navdy/service/library/util/Listenable$Listener;, "TT;"
    if-nez v1, :cond_2

    .line 95
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    goto :goto_1

    .line 101
    .end local v0    # "iterator":Ljava/util/Iterator;
    .end local v1    # "listener":Lcom/navdy/service/library/util/Listenable$Listener;, "TT;"
    .end local v2    # "listenerRef":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<TT;>;"
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .line 96
    .restart local v0    # "iterator":Ljava/util/Iterator;
    .restart local v1    # "listener":Lcom/navdy/service/library/util/Listenable$Listener;, "TT;"
    .restart local v2    # "listenerRef":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<TT;>;"
    :cond_2
    :try_start_1
    invoke-virtual {v1, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 97
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    .line 98
    const/4 v3, 0x1

    monitor-exit v4

    goto :goto_0

    .line 101
    .end local v1    # "listener":Lcom/navdy/service/library/util/Listenable$Listener;, "TT;"
    .end local v2    # "listenerRef":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<TT;>;"
    :cond_3
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method
