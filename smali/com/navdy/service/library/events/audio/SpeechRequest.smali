.class public final Lcom/navdy/service/library/events/audio/SpeechRequest;
.super Lcom/squareup/wire/Message;
.source "SpeechRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/audio/SpeechRequest$Category;,
        Lcom/navdy/service/library/events/audio/SpeechRequest$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_CATEGORY:Lcom/navdy/service/library/events/audio/SpeechRequest$Category;

.field public static final DEFAULT_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_LOCALE:Ljava/lang/String; = ""

.field public static final DEFAULT_SENDSTATUS:Ljava/lang/Boolean;

.field public static final DEFAULT_WORDS:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final category:Lcom/navdy/service/library/events/audio/SpeechRequest$Category;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x2
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x3
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final locale:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x5
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final sendStatus:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x4
        type = .enum Lcom/squareup/wire/Message$Datatype;->BOOL:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final words:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x1
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/navdy/service/library/events/audio/SpeechRequest$Category;->SPEECH_TURN_BY_TURN:Lcom/navdy/service/library/events/audio/SpeechRequest$Category;

    sput-object v0, Lcom/navdy/service/library/events/audio/SpeechRequest;->DEFAULT_CATEGORY:Lcom/navdy/service/library/events/audio/SpeechRequest$Category;

    .line 23
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/audio/SpeechRequest;->DEFAULT_SENDSTATUS:Ljava/lang/Boolean;

    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/audio/SpeechRequest$Builder;)V
    .locals 6
    .param p1, "builder"    # Lcom/navdy/service/library/events/audio/SpeechRequest$Builder;

    .prologue
    .line 66
    iget-object v1, p1, Lcom/navdy/service/library/events/audio/SpeechRequest$Builder;->words:Ljava/lang/String;

    iget-object v2, p1, Lcom/navdy/service/library/events/audio/SpeechRequest$Builder;->category:Lcom/navdy/service/library/events/audio/SpeechRequest$Category;

    iget-object v3, p1, Lcom/navdy/service/library/events/audio/SpeechRequest$Builder;->id:Ljava/lang/String;

    iget-object v4, p1, Lcom/navdy/service/library/events/audio/SpeechRequest$Builder;->sendStatus:Ljava/lang/Boolean;

    iget-object v5, p1, Lcom/navdy/service/library/events/audio/SpeechRequest$Builder;->locale:Ljava/lang/String;

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/navdy/service/library/events/audio/SpeechRequest;-><init>(Ljava/lang/String;Lcom/navdy/service/library/events/audio/SpeechRequest$Category;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;)V

    .line 67
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/audio/SpeechRequest;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 68
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/audio/SpeechRequest$Builder;Lcom/navdy/service/library/events/audio/SpeechRequest$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/audio/SpeechRequest$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/audio/SpeechRequest$1;

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/audio/SpeechRequest;-><init>(Lcom/navdy/service/library/events/audio/SpeechRequest$Builder;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/navdy/service/library/events/audio/SpeechRequest$Category;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;)V
    .locals 0
    .param p1, "words"    # Ljava/lang/String;
    .param p2, "category"    # Lcom/navdy/service/library/events/audio/SpeechRequest$Category;
    .param p3, "id"    # Ljava/lang/String;
    .param p4, "sendStatus"    # Ljava/lang/Boolean;
    .param p5, "locale"    # Ljava/lang/String;

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 58
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/SpeechRequest;->words:Ljava/lang/String;

    .line 59
    iput-object p2, p0, Lcom/navdy/service/library/events/audio/SpeechRequest;->category:Lcom/navdy/service/library/events/audio/SpeechRequest$Category;

    .line 60
    iput-object p3, p0, Lcom/navdy/service/library/events/audio/SpeechRequest;->id:Ljava/lang/String;

    .line 61
    iput-object p4, p0, Lcom/navdy/service/library/events/audio/SpeechRequest;->sendStatus:Ljava/lang/Boolean;

    .line 62
    iput-object p5, p0, Lcom/navdy/service/library/events/audio/SpeechRequest;->locale:Ljava/lang/String;

    .line 63
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 72
    if-ne p1, p0, :cond_1

    .line 79
    :cond_0
    :goto_0
    return v1

    .line 73
    :cond_1
    instance-of v3, p1, Lcom/navdy/service/library/events/audio/SpeechRequest;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 74
    check-cast v0, Lcom/navdy/service/library/events/audio/SpeechRequest;

    .line 75
    .local v0, "o":Lcom/navdy/service/library/events/audio/SpeechRequest;
    iget-object v3, p0, Lcom/navdy/service/library/events/audio/SpeechRequest;->words:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/SpeechRequest;->words:Ljava/lang/String;

    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/audio/SpeechRequest;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/audio/SpeechRequest;->category:Lcom/navdy/service/library/events/audio/SpeechRequest$Category;

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/SpeechRequest;->category:Lcom/navdy/service/library/events/audio/SpeechRequest$Category;

    .line 76
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/audio/SpeechRequest;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/audio/SpeechRequest;->id:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/SpeechRequest;->id:Ljava/lang/String;

    .line 77
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/audio/SpeechRequest;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/audio/SpeechRequest;->sendStatus:Ljava/lang/Boolean;

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/SpeechRequest;->sendStatus:Ljava/lang/Boolean;

    .line 78
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/audio/SpeechRequest;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/audio/SpeechRequest;->locale:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/SpeechRequest;->locale:Ljava/lang/String;

    .line 79
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/audio/SpeechRequest;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 84
    iget v0, p0, Lcom/navdy/service/library/events/audio/SpeechRequest;->hashCode:I

    .line 85
    .local v0, "result":I
    if-nez v0, :cond_1

    .line 86
    iget-object v2, p0, Lcom/navdy/service/library/events/audio/SpeechRequest;->words:Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/SpeechRequest;->words:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 87
    :goto_0
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/SpeechRequest;->category:Lcom/navdy/service/library/events/audio/SpeechRequest$Category;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/SpeechRequest;->category:Lcom/navdy/service/library/events/audio/SpeechRequest$Category;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/audio/SpeechRequest$Category;->hashCode()I

    move-result v2

    :goto_1
    add-int v0, v3, v2

    .line 88
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/SpeechRequest;->id:Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/SpeechRequest;->id:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_2
    add-int v0, v3, v2

    .line 89
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/SpeechRequest;->sendStatus:Ljava/lang/Boolean;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/SpeechRequest;->sendStatus:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :goto_3
    add-int v0, v3, v2

    .line 90
    mul-int/lit8 v2, v0, 0x25

    iget-object v3, p0, Lcom/navdy/service/library/events/audio/SpeechRequest;->locale:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/navdy/service/library/events/audio/SpeechRequest;->locale:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 91
    iput v0, p0, Lcom/navdy/service/library/events/audio/SpeechRequest;->hashCode:I

    .line 93
    :cond_1
    return v0

    :cond_2
    move v0, v1

    .line 86
    goto :goto_0

    :cond_3
    move v2, v1

    .line 87
    goto :goto_1

    :cond_4
    move v2, v1

    .line 88
    goto :goto_2

    :cond_5
    move v2, v1

    .line 89
    goto :goto_3
.end method
