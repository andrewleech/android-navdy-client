.class public final Lcom/navdy/service/library/events/connection/ConnectionRequest;
.super Lcom/squareup/wire/Message;
.source "ConnectionRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/connection/ConnectionRequest$Action;,
        Lcom/navdy/service/library/events/connection/ConnectionRequest$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_ACTION:Lcom/navdy/service/library/events/connection/ConnectionRequest$Action;

.field public static final DEFAULT_REMOTEDEVICEID:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final action:Lcom/navdy/service/library/events/connection/ConnectionRequest$Action;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x1
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final remoteDeviceId:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x2
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/navdy/service/library/events/connection/ConnectionRequest$Action;->CONNECTION_START_SEARCH:Lcom/navdy/service/library/events/connection/ConnectionRequest$Action;

    sput-object v0, Lcom/navdy/service/library/events/connection/ConnectionRequest;->DEFAULT_ACTION:Lcom/navdy/service/library/events/connection/ConnectionRequest$Action;

    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/connection/ConnectionRequest$Action;Ljava/lang/String;)V
    .locals 0
    .param p1, "action"    # Lcom/navdy/service/library/events/connection/ConnectionRequest$Action;
    .param p2, "remoteDeviceId"    # Ljava/lang/String;

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/navdy/service/library/events/connection/ConnectionRequest;->action:Lcom/navdy/service/library/events/connection/ConnectionRequest$Action;

    .line 36
    iput-object p2, p0, Lcom/navdy/service/library/events/connection/ConnectionRequest;->remoteDeviceId:Ljava/lang/String;

    .line 37
    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/connection/ConnectionRequest$Builder;)V
    .locals 2
    .param p1, "builder"    # Lcom/navdy/service/library/events/connection/ConnectionRequest$Builder;

    .prologue
    .line 40
    iget-object v0, p1, Lcom/navdy/service/library/events/connection/ConnectionRequest$Builder;->action:Lcom/navdy/service/library/events/connection/ConnectionRequest$Action;

    iget-object v1, p1, Lcom/navdy/service/library/events/connection/ConnectionRequest$Builder;->remoteDeviceId:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/navdy/service/library/events/connection/ConnectionRequest;-><init>(Lcom/navdy/service/library/events/connection/ConnectionRequest$Action;Ljava/lang/String;)V

    .line 41
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/connection/ConnectionRequest;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 42
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/connection/ConnectionRequest$Builder;Lcom/navdy/service/library/events/connection/ConnectionRequest$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/connection/ConnectionRequest$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/connection/ConnectionRequest$1;

    .prologue
    .line 18
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/connection/ConnectionRequest;-><init>(Lcom/navdy/service/library/events/connection/ConnectionRequest$Builder;)V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 46
    if-ne p1, p0, :cond_1

    .line 50
    :cond_0
    :goto_0
    return v1

    .line 47
    :cond_1
    instance-of v3, p1, Lcom/navdy/service/library/events/connection/ConnectionRequest;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 48
    check-cast v0, Lcom/navdy/service/library/events/connection/ConnectionRequest;

    .line 49
    .local v0, "o":Lcom/navdy/service/library/events/connection/ConnectionRequest;
    iget-object v3, p0, Lcom/navdy/service/library/events/connection/ConnectionRequest;->action:Lcom/navdy/service/library/events/connection/ConnectionRequest$Action;

    iget-object v4, v0, Lcom/navdy/service/library/events/connection/ConnectionRequest;->action:Lcom/navdy/service/library/events/connection/ConnectionRequest$Action;

    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/connection/ConnectionRequest;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/connection/ConnectionRequest;->remoteDeviceId:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/connection/ConnectionRequest;->remoteDeviceId:Ljava/lang/String;

    .line 50
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/connection/ConnectionRequest;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 55
    iget v0, p0, Lcom/navdy/service/library/events/connection/ConnectionRequest;->hashCode:I

    .line 56
    .local v0, "result":I
    if-nez v0, :cond_1

    .line 57
    iget-object v2, p0, Lcom/navdy/service/library/events/connection/ConnectionRequest;->action:Lcom/navdy/service/library/events/connection/ConnectionRequest$Action;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/navdy/service/library/events/connection/ConnectionRequest;->action:Lcom/navdy/service/library/events/connection/ConnectionRequest$Action;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/connection/ConnectionRequest$Action;->hashCode()I

    move-result v0

    .line 58
    :goto_0
    mul-int/lit8 v2, v0, 0x25

    iget-object v3, p0, Lcom/navdy/service/library/events/connection/ConnectionRequest;->remoteDeviceId:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/navdy/service/library/events/connection/ConnectionRequest;->remoteDeviceId:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 59
    iput v0, p0, Lcom/navdy/service/library/events/connection/ConnectionRequest;->hashCode:I

    .line 61
    :cond_1
    return v0

    :cond_2
    move v0, v1

    .line 57
    goto :goto_0
.end method
