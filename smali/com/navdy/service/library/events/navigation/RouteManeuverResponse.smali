.class public final Lcom/navdy/service/library/events/navigation/RouteManeuverResponse;
.super Lcom/squareup/wire/Message;
.source "RouteManeuverResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/navigation/RouteManeuverResponse$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_MANEUVERS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/navigation/RouteManeuver;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_STATUS:Lcom/navdy/service/library/events/RequestStatus;

.field public static final DEFAULT_STATUSDETAIL:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final maneuvers:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REPEATED:Lcom/squareup/wire/Message$Label;
        messageType = Lcom/navdy/service/library/events/navigation/RouteManeuver;
        tag = 0x3
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/navigation/RouteManeuver;",
            ">;"
        }
    .end annotation
.end field

.field public final status:Lcom/navdy/service/library/events/RequestStatus;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x1
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final statusDetail:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x2
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_SUCCESS:Lcom/navdy/service/library/events/RequestStatus;

    sput-object v0, Lcom/navdy/service/library/events/navigation/RouteManeuverResponse;->DEFAULT_STATUS:Lcom/navdy/service/library/events/RequestStatus;

    .line 24
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/navigation/RouteManeuverResponse;->DEFAULT_MANEUVERS:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;Ljava/util/List;)V
    .locals 1
    .param p1, "status"    # Lcom/navdy/service/library/events/RequestStatus;
    .param p2, "statusDetail"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/service/library/events/RequestStatus;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/navigation/RouteManeuver;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 44
    .local p3, "maneuvers":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/navigation/RouteManeuver;>;"
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 45
    iput-object p1, p0, Lcom/navdy/service/library/events/navigation/RouteManeuverResponse;->status:Lcom/navdy/service/library/events/RequestStatus;

    .line 46
    iput-object p2, p0, Lcom/navdy/service/library/events/navigation/RouteManeuverResponse;->statusDetail:Ljava/lang/String;

    .line 47
    invoke-static {p3}, Lcom/navdy/service/library/events/navigation/RouteManeuverResponse;->immutableCopyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/navigation/RouteManeuverResponse;->maneuvers:Ljava/util/List;

    .line 48
    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/navigation/RouteManeuverResponse$Builder;)V
    .locals 3
    .param p1, "builder"    # Lcom/navdy/service/library/events/navigation/RouteManeuverResponse$Builder;

    .prologue
    .line 51
    iget-object v0, p1, Lcom/navdy/service/library/events/navigation/RouteManeuverResponse$Builder;->status:Lcom/navdy/service/library/events/RequestStatus;

    iget-object v1, p1, Lcom/navdy/service/library/events/navigation/RouteManeuverResponse$Builder;->statusDetail:Ljava/lang/String;

    iget-object v2, p1, Lcom/navdy/service/library/events/navigation/RouteManeuverResponse$Builder;->maneuvers:Ljava/util/List;

    invoke-direct {p0, v0, v1, v2}, Lcom/navdy/service/library/events/navigation/RouteManeuverResponse;-><init>(Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;Ljava/util/List;)V

    .line 52
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/navigation/RouteManeuverResponse;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 53
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/navigation/RouteManeuverResponse$Builder;Lcom/navdy/service/library/events/navigation/RouteManeuverResponse$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/navigation/RouteManeuverResponse$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/navigation/RouteManeuverResponse$1;

    .prologue
    .line 19
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/navigation/RouteManeuverResponse;-><init>(Lcom/navdy/service/library/events/navigation/RouteManeuverResponse$Builder;)V

    return-void
.end method

.method static synthetic access$000(Ljava/util/List;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Ljava/util/List;

    .prologue
    .line 19
    invoke-static {p0}, Lcom/navdy/service/library/events/navigation/RouteManeuverResponse;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 57
    if-ne p1, p0, :cond_1

    .line 62
    :cond_0
    :goto_0
    return v1

    .line 58
    :cond_1
    instance-of v3, p1, Lcom/navdy/service/library/events/navigation/RouteManeuverResponse;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 59
    check-cast v0, Lcom/navdy/service/library/events/navigation/RouteManeuverResponse;

    .line 60
    .local v0, "o":Lcom/navdy/service/library/events/navigation/RouteManeuverResponse;
    iget-object v3, p0, Lcom/navdy/service/library/events/navigation/RouteManeuverResponse;->status:Lcom/navdy/service/library/events/RequestStatus;

    iget-object v4, v0, Lcom/navdy/service/library/events/navigation/RouteManeuverResponse;->status:Lcom/navdy/service/library/events/RequestStatus;

    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/navigation/RouteManeuverResponse;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/navigation/RouteManeuverResponse;->statusDetail:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/navigation/RouteManeuverResponse;->statusDetail:Ljava/lang/String;

    .line 61
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/navigation/RouteManeuverResponse;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/navigation/RouteManeuverResponse;->maneuvers:Ljava/util/List;

    iget-object v4, v0, Lcom/navdy/service/library/events/navigation/RouteManeuverResponse;->maneuvers:Ljava/util/List;

    .line 62
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/navigation/RouteManeuverResponse;->equals(Ljava/util/List;Ljava/util/List;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 67
    iget v0, p0, Lcom/navdy/service/library/events/navigation/RouteManeuverResponse;->hashCode:I

    .line 68
    .local v0, "result":I
    if-nez v0, :cond_1

    .line 69
    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/RouteManeuverResponse;->status:Lcom/navdy/service/library/events/RequestStatus;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/RouteManeuverResponse;->status:Lcom/navdy/service/library/events/RequestStatus;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/RequestStatus;->hashCode()I

    move-result v0

    .line 70
    :goto_0
    mul-int/lit8 v2, v0, 0x25

    iget-object v3, p0, Lcom/navdy/service/library/events/navigation/RouteManeuverResponse;->statusDetail:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/navdy/service/library/events/navigation/RouteManeuverResponse;->statusDetail:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 71
    mul-int/lit8 v2, v0, 0x25

    iget-object v1, p0, Lcom/navdy/service/library/events/navigation/RouteManeuverResponse;->maneuvers:Ljava/util/List;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/navdy/service/library/events/navigation/RouteManeuverResponse;->maneuvers:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    :goto_1
    add-int v0, v2, v1

    .line 72
    iput v0, p0, Lcom/navdy/service/library/events/navigation/RouteManeuverResponse;->hashCode:I

    .line 74
    :cond_1
    return v0

    :cond_2
    move v0, v1

    .line 69
    goto :goto_0

    .line 71
    :cond_3
    const/4 v1, 0x1

    goto :goto_1
.end method
