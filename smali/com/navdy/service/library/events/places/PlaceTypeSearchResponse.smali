.class public final Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse;
.super Lcom/squareup/wire/Message;
.source "PlaceTypeSearchResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_DESTINATIONS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/destination/Destination;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_REQUEST_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_REQUEST_STATUS:Lcom/navdy/service/library/events/RequestStatus;

.field private static final serialVersionUID:J


# instance fields
.field public final destinations:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REPEATED:Lcom/squareup/wire/Message$Label;
        messageType = Lcom/navdy/service/library/events/destination/Destination;
        tag = 0x3
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/destination/Destination;",
            ">;"
        }
    .end annotation
.end field

.field public final request_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x1
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final request_status:Lcom/navdy/service/library/events/RequestStatus;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x2
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_SUCCESS:Lcom/navdy/service/library/events/RequestStatus;

    sput-object v0, Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse;->DEFAULT_REQUEST_STATUS:Lcom/navdy/service/library/events/RequestStatus;

    .line 25
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse;->DEFAULT_DESTINATIONS:Ljava/util/List;

    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse$Builder;)V
    .locals 3
    .param p1, "builder"    # Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse$Builder;

    .prologue
    .line 53
    iget-object v0, p1, Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse$Builder;->request_id:Ljava/lang/String;

    iget-object v1, p1, Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse$Builder;->request_status:Lcom/navdy/service/library/events/RequestStatus;

    iget-object v2, p1, Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse$Builder;->destinations:Ljava/util/List;

    invoke-direct {p0, v0, v1, v2}, Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse;-><init>(Ljava/lang/String;Lcom/navdy/service/library/events/RequestStatus;Ljava/util/List;)V

    .line 54
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 55
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse$Builder;Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse$1;

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse;-><init>(Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse$Builder;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/navdy/service/library/events/RequestStatus;Ljava/util/List;)V
    .locals 1
    .param p1, "request_id"    # Ljava/lang/String;
    .param p2, "request_status"    # Lcom/navdy/service/library/events/RequestStatus;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/navdy/service/library/events/RequestStatus;",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/destination/Destination;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 46
    .local p3, "destinations":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/destination/Destination;>;"
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 47
    iput-object p1, p0, Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse;->request_id:Ljava/lang/String;

    .line 48
    iput-object p2, p0, Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse;->request_status:Lcom/navdy/service/library/events/RequestStatus;

    .line 49
    invoke-static {p3}, Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse;->immutableCopyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse;->destinations:Ljava/util/List;

    .line 50
    return-void
.end method

.method static synthetic access$000(Ljava/util/List;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Ljava/util/List;

    .prologue
    .line 20
    invoke-static {p0}, Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 59
    if-ne p1, p0, :cond_1

    .line 64
    :cond_0
    :goto_0
    return v1

    .line 60
    :cond_1
    instance-of v3, p1, Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 61
    check-cast v0, Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse;

    .line 62
    .local v0, "o":Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse;
    iget-object v3, p0, Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse;->request_id:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse;->request_id:Ljava/lang/String;

    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse;->request_status:Lcom/navdy/service/library/events/RequestStatus;

    iget-object v4, v0, Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse;->request_status:Lcom/navdy/service/library/events/RequestStatus;

    .line 63
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse;->destinations:Ljava/util/List;

    iget-object v4, v0, Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse;->destinations:Ljava/util/List;

    .line 64
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse;->equals(Ljava/util/List;Ljava/util/List;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 69
    iget v0, p0, Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse;->hashCode:I

    .line 70
    .local v0, "result":I
    if-nez v0, :cond_1

    .line 71
    iget-object v2, p0, Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse;->request_id:Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse;->request_id:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 72
    :goto_0
    mul-int/lit8 v2, v0, 0x25

    iget-object v3, p0, Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse;->request_status:Lcom/navdy/service/library/events/RequestStatus;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse;->request_status:Lcom/navdy/service/library/events/RequestStatus;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/RequestStatus;->hashCode()I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 73
    mul-int/lit8 v2, v0, 0x25

    iget-object v1, p0, Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse;->destinations:Ljava/util/List;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse;->destinations:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    :goto_1
    add-int v0, v2, v1

    .line 74
    iput v0, p0, Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse;->hashCode:I

    .line 76
    :cond_1
    return v0

    :cond_2
    move v0, v1

    .line 71
    goto :goto_0

    .line 73
    :cond_3
    const/4 v1, 0x1

    goto :goto_1
.end method
