.class public final Lcom/navdy/service/library/events/places/PlacesSearchResult;
.super Lcom/squareup/wire/Message;
.source "PlacesSearchResult.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/places/PlacesSearchResult$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_ADDRESS:Ljava/lang/String; = ""

.field public static final DEFAULT_CATEGORY:Ljava/lang/String; = ""

.field public static final DEFAULT_DISTANCE:Ljava/lang/Double;

.field public static final DEFAULT_LABEL:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final address:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x4
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final category:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x5
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final destinationLocation:Lcom/navdy/service/library/events/location/Coordinate;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x3
    .end annotation
.end field

.field public final distance:Ljava/lang/Double;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x6
        type = .enum Lcom/squareup/wire/Message$Datatype;->DOUBLE:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final label:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x1
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final navigationPosition:Lcom/navdy/service/library/events/location/Coordinate;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 18
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/places/PlacesSearchResult;->DEFAULT_DISTANCE:Ljava/lang/Double;

    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/places/PlacesSearchResult$Builder;)V
    .locals 7
    .param p1, "builder"    # Lcom/navdy/service/library/events/places/PlacesSearchResult$Builder;

    .prologue
    .line 66
    iget-object v1, p1, Lcom/navdy/service/library/events/places/PlacesSearchResult$Builder;->label:Ljava/lang/String;

    iget-object v2, p1, Lcom/navdy/service/library/events/places/PlacesSearchResult$Builder;->navigationPosition:Lcom/navdy/service/library/events/location/Coordinate;

    iget-object v3, p1, Lcom/navdy/service/library/events/places/PlacesSearchResult$Builder;->destinationLocation:Lcom/navdy/service/library/events/location/Coordinate;

    iget-object v4, p1, Lcom/navdy/service/library/events/places/PlacesSearchResult$Builder;->address:Ljava/lang/String;

    iget-object v5, p1, Lcom/navdy/service/library/events/places/PlacesSearchResult$Builder;->category:Ljava/lang/String;

    iget-object v6, p1, Lcom/navdy/service/library/events/places/PlacesSearchResult$Builder;->distance:Ljava/lang/Double;

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/navdy/service/library/events/places/PlacesSearchResult;-><init>(Ljava/lang/String;Lcom/navdy/service/library/events/location/Coordinate;Lcom/navdy/service/library/events/location/Coordinate;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Double;)V

    .line 67
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/places/PlacesSearchResult;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 68
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/places/PlacesSearchResult$Builder;Lcom/navdy/service/library/events/places/PlacesSearchResult$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/places/PlacesSearchResult$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/places/PlacesSearchResult$1;

    .prologue
    .line 12
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/places/PlacesSearchResult;-><init>(Lcom/navdy/service/library/events/places/PlacesSearchResult$Builder;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/navdy/service/library/events/location/Coordinate;Lcom/navdy/service/library/events/location/Coordinate;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Double;)V
    .locals 0
    .param p1, "label"    # Ljava/lang/String;
    .param p2, "navigationPosition"    # Lcom/navdy/service/library/events/location/Coordinate;
    .param p3, "destinationLocation"    # Lcom/navdy/service/library/events/location/Coordinate;
    .param p4, "address"    # Ljava/lang/String;
    .param p5, "category"    # Ljava/lang/String;
    .param p6, "distance"    # Ljava/lang/Double;

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 57
    iput-object p1, p0, Lcom/navdy/service/library/events/places/PlacesSearchResult;->label:Ljava/lang/String;

    .line 58
    iput-object p2, p0, Lcom/navdy/service/library/events/places/PlacesSearchResult;->navigationPosition:Lcom/navdy/service/library/events/location/Coordinate;

    .line 59
    iput-object p3, p0, Lcom/navdy/service/library/events/places/PlacesSearchResult;->destinationLocation:Lcom/navdy/service/library/events/location/Coordinate;

    .line 60
    iput-object p4, p0, Lcom/navdy/service/library/events/places/PlacesSearchResult;->address:Ljava/lang/String;

    .line 61
    iput-object p5, p0, Lcom/navdy/service/library/events/places/PlacesSearchResult;->category:Ljava/lang/String;

    .line 62
    iput-object p6, p0, Lcom/navdy/service/library/events/places/PlacesSearchResult;->distance:Ljava/lang/Double;

    .line 63
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 72
    if-ne p1, p0, :cond_1

    .line 80
    :cond_0
    :goto_0
    return v1

    .line 73
    :cond_1
    instance-of v3, p1, Lcom/navdy/service/library/events/places/PlacesSearchResult;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 74
    check-cast v0, Lcom/navdy/service/library/events/places/PlacesSearchResult;

    .line 75
    .local v0, "o":Lcom/navdy/service/library/events/places/PlacesSearchResult;
    iget-object v3, p0, Lcom/navdy/service/library/events/places/PlacesSearchResult;->label:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/places/PlacesSearchResult;->label:Ljava/lang/String;

    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/places/PlacesSearchResult;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/places/PlacesSearchResult;->navigationPosition:Lcom/navdy/service/library/events/location/Coordinate;

    iget-object v4, v0, Lcom/navdy/service/library/events/places/PlacesSearchResult;->navigationPosition:Lcom/navdy/service/library/events/location/Coordinate;

    .line 76
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/places/PlacesSearchResult;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/places/PlacesSearchResult;->destinationLocation:Lcom/navdy/service/library/events/location/Coordinate;

    iget-object v4, v0, Lcom/navdy/service/library/events/places/PlacesSearchResult;->destinationLocation:Lcom/navdy/service/library/events/location/Coordinate;

    .line 77
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/places/PlacesSearchResult;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/places/PlacesSearchResult;->address:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/places/PlacesSearchResult;->address:Ljava/lang/String;

    .line 78
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/places/PlacesSearchResult;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/places/PlacesSearchResult;->category:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/places/PlacesSearchResult;->category:Ljava/lang/String;

    .line 79
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/places/PlacesSearchResult;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/places/PlacesSearchResult;->distance:Ljava/lang/Double;

    iget-object v4, v0, Lcom/navdy/service/library/events/places/PlacesSearchResult;->distance:Ljava/lang/Double;

    .line 80
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/places/PlacesSearchResult;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 85
    iget v0, p0, Lcom/navdy/service/library/events/places/PlacesSearchResult;->hashCode:I

    .line 86
    .local v0, "result":I
    if-nez v0, :cond_1

    .line 87
    iget-object v2, p0, Lcom/navdy/service/library/events/places/PlacesSearchResult;->label:Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/navdy/service/library/events/places/PlacesSearchResult;->label:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 88
    :goto_0
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/places/PlacesSearchResult;->navigationPosition:Lcom/navdy/service/library/events/location/Coordinate;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/navdy/service/library/events/places/PlacesSearchResult;->navigationPosition:Lcom/navdy/service/library/events/location/Coordinate;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/location/Coordinate;->hashCode()I

    move-result v2

    :goto_1
    add-int v0, v3, v2

    .line 89
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/places/PlacesSearchResult;->destinationLocation:Lcom/navdy/service/library/events/location/Coordinate;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/navdy/service/library/events/places/PlacesSearchResult;->destinationLocation:Lcom/navdy/service/library/events/location/Coordinate;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/location/Coordinate;->hashCode()I

    move-result v2

    :goto_2
    add-int v0, v3, v2

    .line 90
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/places/PlacesSearchResult;->address:Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/navdy/service/library/events/places/PlacesSearchResult;->address:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_3
    add-int v0, v3, v2

    .line 91
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/places/PlacesSearchResult;->category:Ljava/lang/String;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/navdy/service/library/events/places/PlacesSearchResult;->category:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_4
    add-int v0, v3, v2

    .line 92
    mul-int/lit8 v2, v0, 0x25

    iget-object v3, p0, Lcom/navdy/service/library/events/places/PlacesSearchResult;->distance:Ljava/lang/Double;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/navdy/service/library/events/places/PlacesSearchResult;->distance:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->hashCode()I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 93
    iput v0, p0, Lcom/navdy/service/library/events/places/PlacesSearchResult;->hashCode:I

    .line 95
    :cond_1
    return v0

    :cond_2
    move v0, v1

    .line 87
    goto :goto_0

    :cond_3
    move v2, v1

    .line 88
    goto :goto_1

    :cond_4
    move v2, v1

    .line 89
    goto :goto_2

    :cond_5
    move v2, v1

    .line 90
    goto :goto_3

    :cond_6
    move v2, v1

    .line 91
    goto :goto_4
.end method
