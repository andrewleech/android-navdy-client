.class public final Lcom/navdy/service/library/events/places/AutoCompleteRequest;
.super Lcom/squareup/wire/Message;
.source "AutoCompleteRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/places/AutoCompleteRequest$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_MAXRESULTS:Ljava/lang/Integer;

.field public static final DEFAULT_PARTIALSEARCH:Ljava/lang/String; = ""

.field public static final DEFAULT_SEARCHAREA:Ljava/lang/Integer;

.field private static final serialVersionUID:J


# instance fields
.field public final maxResults:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x3
        type = .enum Lcom/squareup/wire/Message$Datatype;->INT32:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final partialSearch:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x1
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final searchArea:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x2
        type = .enum Lcom/squareup/wire/Message$Datatype;->INT32:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 16
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/places/AutoCompleteRequest;->DEFAULT_SEARCHAREA:Ljava/lang/Integer;

    .line 17
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/places/AutoCompleteRequest;->DEFAULT_MAXRESULTS:Ljava/lang/Integer;

    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/places/AutoCompleteRequest$Builder;)V
    .locals 3
    .param p1, "builder"    # Lcom/navdy/service/library/events/places/AutoCompleteRequest$Builder;

    .prologue
    .line 44
    iget-object v0, p1, Lcom/navdy/service/library/events/places/AutoCompleteRequest$Builder;->partialSearch:Ljava/lang/String;

    iget-object v1, p1, Lcom/navdy/service/library/events/places/AutoCompleteRequest$Builder;->searchArea:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/navdy/service/library/events/places/AutoCompleteRequest$Builder;->maxResults:Ljava/lang/Integer;

    invoke-direct {p0, v0, v1, v2}, Lcom/navdy/service/library/events/places/AutoCompleteRequest;-><init>(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;)V

    .line 45
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/places/AutoCompleteRequest;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 46
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/places/AutoCompleteRequest$Builder;Lcom/navdy/service/library/events/places/AutoCompleteRequest$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/places/AutoCompleteRequest$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/places/AutoCompleteRequest$1;

    .prologue
    .line 12
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/places/AutoCompleteRequest;-><init>(Lcom/navdy/service/library/events/places/AutoCompleteRequest$Builder;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;)V
    .locals 0
    .param p1, "partialSearch"    # Ljava/lang/String;
    .param p2, "searchArea"    # Ljava/lang/Integer;
    .param p3, "maxResults"    # Ljava/lang/Integer;

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/navdy/service/library/events/places/AutoCompleteRequest;->partialSearch:Ljava/lang/String;

    .line 39
    iput-object p2, p0, Lcom/navdy/service/library/events/places/AutoCompleteRequest;->searchArea:Ljava/lang/Integer;

    .line 40
    iput-object p3, p0, Lcom/navdy/service/library/events/places/AutoCompleteRequest;->maxResults:Ljava/lang/Integer;

    .line 41
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 50
    if-ne p1, p0, :cond_1

    .line 55
    :cond_0
    :goto_0
    return v1

    .line 51
    :cond_1
    instance-of v3, p1, Lcom/navdy/service/library/events/places/AutoCompleteRequest;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 52
    check-cast v0, Lcom/navdy/service/library/events/places/AutoCompleteRequest;

    .line 53
    .local v0, "o":Lcom/navdy/service/library/events/places/AutoCompleteRequest;
    iget-object v3, p0, Lcom/navdy/service/library/events/places/AutoCompleteRequest;->partialSearch:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/places/AutoCompleteRequest;->partialSearch:Ljava/lang/String;

    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/places/AutoCompleteRequest;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/places/AutoCompleteRequest;->searchArea:Ljava/lang/Integer;

    iget-object v4, v0, Lcom/navdy/service/library/events/places/AutoCompleteRequest;->searchArea:Ljava/lang/Integer;

    .line 54
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/places/AutoCompleteRequest;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/places/AutoCompleteRequest;->maxResults:Ljava/lang/Integer;

    iget-object v4, v0, Lcom/navdy/service/library/events/places/AutoCompleteRequest;->maxResults:Ljava/lang/Integer;

    .line 55
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/places/AutoCompleteRequest;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 60
    iget v0, p0, Lcom/navdy/service/library/events/places/AutoCompleteRequest;->hashCode:I

    .line 61
    .local v0, "result":I
    if-nez v0, :cond_1

    .line 62
    iget-object v2, p0, Lcom/navdy/service/library/events/places/AutoCompleteRequest;->partialSearch:Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/navdy/service/library/events/places/AutoCompleteRequest;->partialSearch:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 63
    :goto_0
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/places/AutoCompleteRequest;->searchArea:Ljava/lang/Integer;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/navdy/service/library/events/places/AutoCompleteRequest;->searchArea:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->hashCode()I

    move-result v2

    :goto_1
    add-int v0, v3, v2

    .line 64
    mul-int/lit8 v2, v0, 0x25

    iget-object v3, p0, Lcom/navdy/service/library/events/places/AutoCompleteRequest;->maxResults:Ljava/lang/Integer;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/navdy/service/library/events/places/AutoCompleteRequest;->maxResults:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 65
    iput v0, p0, Lcom/navdy/service/library/events/places/AutoCompleteRequest;->hashCode:I

    .line 67
    :cond_1
    return v0

    :cond_2
    move v0, v1

    .line 62
    goto :goto_0

    :cond_3
    move v2, v1

    .line 63
    goto :goto_1
.end method
