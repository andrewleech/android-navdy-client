.class public final Lcom/navdy/service/library/events/obd/ObdStatusResponse;
.super Lcom/squareup/wire/Message;
.source "ObdStatusResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/obd/ObdStatusResponse$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_ISCONNECTED:Ljava/lang/Boolean;

.field public static final DEFAULT_SPEED:Ljava/lang/Integer;

.field public static final DEFAULT_STATUS:Lcom/navdy/service/library/events/RequestStatus;

.field public static final DEFAULT_VIN:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final isConnected:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x2
        type = .enum Lcom/squareup/wire/Message$Datatype;->BOOL:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final speed:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x4
        type = .enum Lcom/squareup/wire/Message$Datatype;->INT32:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final status:Lcom/navdy/service/library/events/RequestStatus;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x1
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final vin:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x3
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 21
    sget-object v0, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_SUCCESS:Lcom/navdy/service/library/events/RequestStatus;

    sput-object v0, Lcom/navdy/service/library/events/obd/ObdStatusResponse;->DEFAULT_STATUS:Lcom/navdy/service/library/events/RequestStatus;

    .line 22
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/obd/ObdStatusResponse;->DEFAULT_ISCONNECTED:Ljava/lang/Boolean;

    .line 24
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/obd/ObdStatusResponse;->DEFAULT_SPEED:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Integer;)V
    .locals 0
    .param p1, "status"    # Lcom/navdy/service/library/events/RequestStatus;
    .param p2, "isConnected"    # Ljava/lang/Boolean;
    .param p3, "vin"    # Ljava/lang/String;
    .param p4, "speed"    # Ljava/lang/Integer;

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 52
    iput-object p1, p0, Lcom/navdy/service/library/events/obd/ObdStatusResponse;->status:Lcom/navdy/service/library/events/RequestStatus;

    .line 53
    iput-object p2, p0, Lcom/navdy/service/library/events/obd/ObdStatusResponse;->isConnected:Ljava/lang/Boolean;

    .line 54
    iput-object p3, p0, Lcom/navdy/service/library/events/obd/ObdStatusResponse;->vin:Ljava/lang/String;

    .line 55
    iput-object p4, p0, Lcom/navdy/service/library/events/obd/ObdStatusResponse;->speed:Ljava/lang/Integer;

    .line 56
    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/obd/ObdStatusResponse$Builder;)V
    .locals 4
    .param p1, "builder"    # Lcom/navdy/service/library/events/obd/ObdStatusResponse$Builder;

    .prologue
    .line 59
    iget-object v0, p1, Lcom/navdy/service/library/events/obd/ObdStatusResponse$Builder;->status:Lcom/navdy/service/library/events/RequestStatus;

    iget-object v1, p1, Lcom/navdy/service/library/events/obd/ObdStatusResponse$Builder;->isConnected:Ljava/lang/Boolean;

    iget-object v2, p1, Lcom/navdy/service/library/events/obd/ObdStatusResponse$Builder;->vin:Ljava/lang/String;

    iget-object v3, p1, Lcom/navdy/service/library/events/obd/ObdStatusResponse$Builder;->speed:Ljava/lang/Integer;

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/navdy/service/library/events/obd/ObdStatusResponse;-><init>(Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 60
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/obd/ObdStatusResponse;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 61
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/obd/ObdStatusResponse$Builder;Lcom/navdy/service/library/events/obd/ObdStatusResponse$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/obd/ObdStatusResponse$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/obd/ObdStatusResponse$1;

    .prologue
    .line 18
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/obd/ObdStatusResponse;-><init>(Lcom/navdy/service/library/events/obd/ObdStatusResponse$Builder;)V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 65
    if-ne p1, p0, :cond_1

    .line 71
    :cond_0
    :goto_0
    return v1

    .line 66
    :cond_1
    instance-of v3, p1, Lcom/navdy/service/library/events/obd/ObdStatusResponse;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 67
    check-cast v0, Lcom/navdy/service/library/events/obd/ObdStatusResponse;

    .line 68
    .local v0, "o":Lcom/navdy/service/library/events/obd/ObdStatusResponse;
    iget-object v3, p0, Lcom/navdy/service/library/events/obd/ObdStatusResponse;->status:Lcom/navdy/service/library/events/RequestStatus;

    iget-object v4, v0, Lcom/navdy/service/library/events/obd/ObdStatusResponse;->status:Lcom/navdy/service/library/events/RequestStatus;

    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/obd/ObdStatusResponse;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/obd/ObdStatusResponse;->isConnected:Ljava/lang/Boolean;

    iget-object v4, v0, Lcom/navdy/service/library/events/obd/ObdStatusResponse;->isConnected:Ljava/lang/Boolean;

    .line 69
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/obd/ObdStatusResponse;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/obd/ObdStatusResponse;->vin:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/obd/ObdStatusResponse;->vin:Ljava/lang/String;

    .line 70
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/obd/ObdStatusResponse;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/obd/ObdStatusResponse;->speed:Ljava/lang/Integer;

    iget-object v4, v0, Lcom/navdy/service/library/events/obd/ObdStatusResponse;->speed:Ljava/lang/Integer;

    .line 71
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/obd/ObdStatusResponse;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 76
    iget v0, p0, Lcom/navdy/service/library/events/obd/ObdStatusResponse;->hashCode:I

    .line 77
    .local v0, "result":I
    if-nez v0, :cond_1

    .line 78
    iget-object v2, p0, Lcom/navdy/service/library/events/obd/ObdStatusResponse;->status:Lcom/navdy/service/library/events/RequestStatus;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/navdy/service/library/events/obd/ObdStatusResponse;->status:Lcom/navdy/service/library/events/RequestStatus;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/RequestStatus;->hashCode()I

    move-result v0

    .line 79
    :goto_0
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/obd/ObdStatusResponse;->isConnected:Ljava/lang/Boolean;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/navdy/service/library/events/obd/ObdStatusResponse;->isConnected:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :goto_1
    add-int v0, v3, v2

    .line 80
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/obd/ObdStatusResponse;->vin:Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/navdy/service/library/events/obd/ObdStatusResponse;->vin:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_2
    add-int v0, v3, v2

    .line 81
    mul-int/lit8 v2, v0, 0x25

    iget-object v3, p0, Lcom/navdy/service/library/events/obd/ObdStatusResponse;->speed:Ljava/lang/Integer;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/navdy/service/library/events/obd/ObdStatusResponse;->speed:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 82
    iput v0, p0, Lcom/navdy/service/library/events/obd/ObdStatusResponse;->hashCode:I

    .line 84
    :cond_1
    return v0

    :cond_2
    move v0, v1

    .line 78
    goto :goto_0

    :cond_3
    move v2, v1

    .line 79
    goto :goto_1

    :cond_4
    move v2, v1

    .line 80
    goto :goto_2
.end method
