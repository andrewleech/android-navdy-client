.class public final Lcom/navdy/service/library/events/preferences/NotificationPreferences;
.super Lcom/squareup/wire/Message;
.source "NotificationPreferences.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/preferences/NotificationPreferences$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_ENABLED:Ljava/lang/Boolean;

.field public static final DEFAULT_READALOUD:Ljava/lang/Boolean;

.field public static final DEFAULT_SERIAL_NUMBER:Ljava/lang/Long;

.field public static final DEFAULT_SETTINGS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/notification/NotificationSetting;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_SHOWCONTENT:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final enabled:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x3
        type = .enum Lcom/squareup/wire/Message$Datatype;->BOOL:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final readAloud:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x4
        type = .enum Lcom/squareup/wire/Message$Datatype;->BOOL:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final serial_number:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x1
        type = .enum Lcom/squareup/wire/Message$Datatype;->INT64:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final settings:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REPEATED:Lcom/squareup/wire/Message$Label;
        messageType = Lcom/navdy/service/library/events/notification/NotificationSetting;
        tag = 0x2
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/notification/NotificationSetting;",
            ">;"
        }
    .end annotation
.end field

.field public final showContent:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x5
        type = .enum Lcom/squareup/wire/Message$Datatype;->BOOL:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 23
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/preferences/NotificationPreferences;->DEFAULT_SERIAL_NUMBER:Ljava/lang/Long;

    .line 24
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/preferences/NotificationPreferences;->DEFAULT_SETTINGS:Ljava/util/List;

    .line 25
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/preferences/NotificationPreferences;->DEFAULT_ENABLED:Ljava/lang/Boolean;

    .line 26
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/preferences/NotificationPreferences;->DEFAULT_READALOUD:Ljava/lang/Boolean;

    .line 27
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/preferences/NotificationPreferences;->DEFAULT_SHOWCONTENT:Ljava/lang/Boolean;

    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/preferences/NotificationPreferences$Builder;)V
    .locals 6
    .param p1, "builder"    # Lcom/navdy/service/library/events/preferences/NotificationPreferences$Builder;

    .prologue
    .line 69
    iget-object v1, p1, Lcom/navdy/service/library/events/preferences/NotificationPreferences$Builder;->serial_number:Ljava/lang/Long;

    iget-object v2, p1, Lcom/navdy/service/library/events/preferences/NotificationPreferences$Builder;->settings:Ljava/util/List;

    iget-object v3, p1, Lcom/navdy/service/library/events/preferences/NotificationPreferences$Builder;->enabled:Ljava/lang/Boolean;

    iget-object v4, p1, Lcom/navdy/service/library/events/preferences/NotificationPreferences$Builder;->readAloud:Ljava/lang/Boolean;

    iget-object v5, p1, Lcom/navdy/service/library/events/preferences/NotificationPreferences$Builder;->showContent:Ljava/lang/Boolean;

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/navdy/service/library/events/preferences/NotificationPreferences;-><init>(Ljava/lang/Long;Ljava/util/List;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;)V

    .line 70
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/preferences/NotificationPreferences;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 71
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/preferences/NotificationPreferences$Builder;Lcom/navdy/service/library/events/preferences/NotificationPreferences$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/preferences/NotificationPreferences$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/preferences/NotificationPreferences$1;

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/preferences/NotificationPreferences;-><init>(Lcom/navdy/service/library/events/preferences/NotificationPreferences$Builder;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Long;Ljava/util/List;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;)V
    .locals 1
    .param p1, "serial_number"    # Ljava/lang/Long;
    .param p3, "enabled"    # Ljava/lang/Boolean;
    .param p4, "readAloud"    # Ljava/lang/Boolean;
    .param p5, "showContent"    # Ljava/lang/Boolean;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Long;",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/notification/NotificationSetting;",
            ">;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            ")V"
        }
    .end annotation

    .prologue
    .line 60
    .local p2, "settings":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/notification/NotificationSetting;>;"
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 61
    iput-object p1, p0, Lcom/navdy/service/library/events/preferences/NotificationPreferences;->serial_number:Ljava/lang/Long;

    .line 62
    invoke-static {p2}, Lcom/navdy/service/library/events/preferences/NotificationPreferences;->immutableCopyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/preferences/NotificationPreferences;->settings:Ljava/util/List;

    .line 63
    iput-object p3, p0, Lcom/navdy/service/library/events/preferences/NotificationPreferences;->enabled:Ljava/lang/Boolean;

    .line 64
    iput-object p4, p0, Lcom/navdy/service/library/events/preferences/NotificationPreferences;->readAloud:Ljava/lang/Boolean;

    .line 65
    iput-object p5, p0, Lcom/navdy/service/library/events/preferences/NotificationPreferences;->showContent:Ljava/lang/Boolean;

    .line 66
    return-void
.end method

.method static synthetic access$000(Ljava/util/List;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Ljava/util/List;

    .prologue
    .line 20
    invoke-static {p0}, Lcom/navdy/service/library/events/preferences/NotificationPreferences;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 75
    if-ne p1, p0, :cond_1

    .line 82
    :cond_0
    :goto_0
    return v1

    .line 76
    :cond_1
    instance-of v3, p1, Lcom/navdy/service/library/events/preferences/NotificationPreferences;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 77
    check-cast v0, Lcom/navdy/service/library/events/preferences/NotificationPreferences;

    .line 78
    .local v0, "o":Lcom/navdy/service/library/events/preferences/NotificationPreferences;
    iget-object v3, p0, Lcom/navdy/service/library/events/preferences/NotificationPreferences;->serial_number:Ljava/lang/Long;

    iget-object v4, v0, Lcom/navdy/service/library/events/preferences/NotificationPreferences;->serial_number:Ljava/lang/Long;

    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/preferences/NotificationPreferences;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/preferences/NotificationPreferences;->settings:Ljava/util/List;

    iget-object v4, v0, Lcom/navdy/service/library/events/preferences/NotificationPreferences;->settings:Ljava/util/List;

    .line 79
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/preferences/NotificationPreferences;->equals(Ljava/util/List;Ljava/util/List;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/preferences/NotificationPreferences;->enabled:Ljava/lang/Boolean;

    iget-object v4, v0, Lcom/navdy/service/library/events/preferences/NotificationPreferences;->enabled:Ljava/lang/Boolean;

    .line 80
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/preferences/NotificationPreferences;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/preferences/NotificationPreferences;->readAloud:Ljava/lang/Boolean;

    iget-object v4, v0, Lcom/navdy/service/library/events/preferences/NotificationPreferences;->readAloud:Ljava/lang/Boolean;

    .line 81
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/preferences/NotificationPreferences;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/preferences/NotificationPreferences;->showContent:Ljava/lang/Boolean;

    iget-object v4, v0, Lcom/navdy/service/library/events/preferences/NotificationPreferences;->showContent:Ljava/lang/Boolean;

    .line 82
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/preferences/NotificationPreferences;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 87
    iget v0, p0, Lcom/navdy/service/library/events/preferences/NotificationPreferences;->hashCode:I

    .line 88
    .local v0, "result":I
    if-nez v0, :cond_1

    .line 89
    iget-object v2, p0, Lcom/navdy/service/library/events/preferences/NotificationPreferences;->serial_number:Ljava/lang/Long;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/navdy/service/library/events/preferences/NotificationPreferences;->serial_number:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->hashCode()I

    move-result v0

    .line 90
    :goto_0
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/preferences/NotificationPreferences;->settings:Ljava/util/List;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/navdy/service/library/events/preferences/NotificationPreferences;->settings:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->hashCode()I

    move-result v2

    :goto_1
    add-int v0, v3, v2

    .line 91
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/preferences/NotificationPreferences;->enabled:Ljava/lang/Boolean;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/navdy/service/library/events/preferences/NotificationPreferences;->enabled:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :goto_2
    add-int v0, v3, v2

    .line 92
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/preferences/NotificationPreferences;->readAloud:Ljava/lang/Boolean;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/navdy/service/library/events/preferences/NotificationPreferences;->readAloud:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :goto_3
    add-int v0, v3, v2

    .line 93
    mul-int/lit8 v2, v0, 0x25

    iget-object v3, p0, Lcom/navdy/service/library/events/preferences/NotificationPreferences;->showContent:Ljava/lang/Boolean;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/navdy/service/library/events/preferences/NotificationPreferences;->showContent:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 94
    iput v0, p0, Lcom/navdy/service/library/events/preferences/NotificationPreferences;->hashCode:I

    .line 96
    :cond_1
    return v0

    :cond_2
    move v0, v1

    .line 89
    goto :goto_0

    .line 90
    :cond_3
    const/4 v2, 0x1

    goto :goto_1

    :cond_4
    move v2, v1

    .line 91
    goto :goto_2

    :cond_5
    move v2, v1

    .line 92
    goto :goto_3
.end method
