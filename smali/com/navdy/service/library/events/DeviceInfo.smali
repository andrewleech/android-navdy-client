.class public final Lcom/navdy/service/library/events/DeviceInfo;
.super Lcom/squareup/wire/Message;
.source "DeviceInfo.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/DeviceInfo$Platform;,
        Lcom/navdy/service/library/events/DeviceInfo$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_BUILDTYPE:Ljava/lang/String; = "user"

.field public static final DEFAULT_CLIENTVERSION:Ljava/lang/String; = ""

.field public static final DEFAULT_DEVICEID:Ljava/lang/String; = ""

.field public static final DEFAULT_DEVICEMAKE:Ljava/lang/String; = "unknown"

.field public static final DEFAULT_DEVICENAME:Ljava/lang/String; = ""

.field public static final DEFAULT_DEVICEUUID:Ljava/lang/String; = ""

.field public static final DEFAULT_FORCEFULLUPDATE:Ljava/lang/Boolean;

.field public static final DEFAULT_KERNELVERSION:Ljava/lang/String; = ""

.field public static final DEFAULT_LEGACYCAPABILITIES:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/LegacyCapability;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_MODEL:Ljava/lang/String; = ""

.field public static final DEFAULT_MUSICPLAYERS_OBSOLETE:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_PLATFORM:Lcom/navdy/service/library/events/DeviceInfo$Platform;

.field public static final DEFAULT_PROTOCOLVERSION:Ljava/lang/String; = ""

.field public static final DEFAULT_SYSTEMAPILEVEL:Ljava/lang/Integer;

.field public static final DEFAULT_SYSTEMVERSION:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final buildType:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0xb
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final capabilities:Lcom/navdy/service/library/events/Capabilities;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x10
    .end annotation
.end field

.field public final clientVersion:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x2
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final deviceId:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x1
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final deviceMake:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0xc
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final deviceName:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x4
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final deviceUuid:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x7
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final forceFullUpdate:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0xd
        type = .enum Lcom/squareup/wire/Message$Datatype;->BOOL:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final kernelVersion:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x9
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final legacyCapabilities:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        deprecated = true
        enumType = Lcom/navdy/service/library/events/LegacyCapability;
        label = .enum Lcom/squareup/wire/Message$Label;->REPEATED:Lcom/squareup/wire/Message$Label;
        tag = 0xe
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/LegacyCapability;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final model:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x6
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final musicPlayers_OBSOLETE:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        deprecated = true
        label = .enum Lcom/squareup/wire/Message$Label;->REPEATED:Lcom/squareup/wire/Message$Label;
        tag = 0xf
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final platform:Lcom/navdy/service/library/events/DeviceInfo$Platform;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0xa
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final protocolVersion:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x3
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final systemApiLevel:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x8
        type = .enum Lcom/squareup/wire/Message$Datatype;->INT32:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final systemVersion:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x5
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 35
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/DeviceInfo;->DEFAULT_SYSTEMAPILEVEL:Ljava/lang/Integer;

    .line 37
    sget-object v0, Lcom/navdy/service/library/events/DeviceInfo$Platform;->PLATFORM_iOS:Lcom/navdy/service/library/events/DeviceInfo$Platform;

    sput-object v0, Lcom/navdy/service/library/events/DeviceInfo;->DEFAULT_PLATFORM:Lcom/navdy/service/library/events/DeviceInfo$Platform;

    .line 40
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/DeviceInfo;->DEFAULT_FORCEFULLUPDATE:Ljava/lang/Boolean;

    .line 41
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/DeviceInfo;->DEFAULT_LEGACYCAPABILITIES:Ljava/util/List;

    .line 42
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/DeviceInfo;->DEFAULT_MUSICPLAYERS_OBSOLETE:Ljava/util/List;

    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/DeviceInfo$Builder;)V
    .locals 18
    .param p1, "builder"    # Lcom/navdy/service/library/events/DeviceInfo$Builder;

    .prologue
    .line 181
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/navdy/service/library/events/DeviceInfo$Builder;->deviceId:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/navdy/service/library/events/DeviceInfo$Builder;->clientVersion:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/navdy/service/library/events/DeviceInfo$Builder;->protocolVersion:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/navdy/service/library/events/DeviceInfo$Builder;->deviceName:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/navdy/service/library/events/DeviceInfo$Builder;->systemVersion:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v7, v0, Lcom/navdy/service/library/events/DeviceInfo$Builder;->model:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v8, v0, Lcom/navdy/service/library/events/DeviceInfo$Builder;->deviceUuid:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v9, v0, Lcom/navdy/service/library/events/DeviceInfo$Builder;->systemApiLevel:Ljava/lang/Integer;

    move-object/from16 v0, p1

    iget-object v10, v0, Lcom/navdy/service/library/events/DeviceInfo$Builder;->kernelVersion:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v11, v0, Lcom/navdy/service/library/events/DeviceInfo$Builder;->platform:Lcom/navdy/service/library/events/DeviceInfo$Platform;

    move-object/from16 v0, p1

    iget-object v12, v0, Lcom/navdy/service/library/events/DeviceInfo$Builder;->buildType:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v13, v0, Lcom/navdy/service/library/events/DeviceInfo$Builder;->deviceMake:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/navdy/service/library/events/DeviceInfo$Builder;->forceFullUpdate:Ljava/lang/Boolean;

    move-object/from16 v0, p1

    iget-object v15, v0, Lcom/navdy/service/library/events/DeviceInfo$Builder;->legacyCapabilities:Ljava/util/List;

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/navdy/service/library/events/DeviceInfo$Builder;->musicPlayers_OBSOLETE:Ljava/util/List;

    move-object/from16 v16, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/navdy/service/library/events/DeviceInfo$Builder;->capabilities:Lcom/navdy/service/library/events/Capabilities;

    move-object/from16 v17, v0

    move-object/from16 v1, p0

    invoke-direct/range {v1 .. v17}, Lcom/navdy/service/library/events/DeviceInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Lcom/navdy/service/library/events/DeviceInfo$Platform;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/util/List;Ljava/util/List;Lcom/navdy/service/library/events/Capabilities;)V

    .line 182
    invoke-virtual/range {p0 .. p1}, Lcom/navdy/service/library/events/DeviceInfo;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 183
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/DeviceInfo$Builder;Lcom/navdy/service/library/events/DeviceInfo$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/DeviceInfo$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/DeviceInfo$1;

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/DeviceInfo;-><init>(Lcom/navdy/service/library/events/DeviceInfo$Builder;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Lcom/navdy/service/library/events/DeviceInfo$Platform;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/util/List;Ljava/util/List;Lcom/navdy/service/library/events/Capabilities;)V
    .locals 2
    .param p1, "deviceId"    # Ljava/lang/String;
    .param p2, "clientVersion"    # Ljava/lang/String;
    .param p3, "protocolVersion"    # Ljava/lang/String;
    .param p4, "deviceName"    # Ljava/lang/String;
    .param p5, "systemVersion"    # Ljava/lang/String;
    .param p6, "model"    # Ljava/lang/String;
    .param p7, "deviceUuid"    # Ljava/lang/String;
    .param p8, "systemApiLevel"    # Ljava/lang/Integer;
    .param p9, "kernelVersion"    # Ljava/lang/String;
    .param p10, "platform"    # Lcom/navdy/service/library/events/DeviceInfo$Platform;
    .param p11, "buildType"    # Ljava/lang/String;
    .param p12, "deviceMake"    # Ljava/lang/String;
    .param p13, "forceFullUpdate"    # Ljava/lang/Boolean;
    .param p16, "capabilities"    # Lcom/navdy/service/library/events/Capabilities;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            "Lcom/navdy/service/library/events/DeviceInfo$Platform;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/LegacyCapability;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/navdy/service/library/events/Capabilities;",
            ")V"
        }
    .end annotation

    .prologue
    .line 161
    .local p14, "legacyCapabilities":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/LegacyCapability;>;"
    .local p15, "musicPlayers_OBSOLETE":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 162
    iput-object p1, p0, Lcom/navdy/service/library/events/DeviceInfo;->deviceId:Ljava/lang/String;

    .line 163
    iput-object p2, p0, Lcom/navdy/service/library/events/DeviceInfo;->clientVersion:Ljava/lang/String;

    .line 164
    iput-object p3, p0, Lcom/navdy/service/library/events/DeviceInfo;->protocolVersion:Ljava/lang/String;

    .line 165
    iput-object p4, p0, Lcom/navdy/service/library/events/DeviceInfo;->deviceName:Ljava/lang/String;

    .line 166
    iput-object p5, p0, Lcom/navdy/service/library/events/DeviceInfo;->systemVersion:Ljava/lang/String;

    .line 167
    iput-object p6, p0, Lcom/navdy/service/library/events/DeviceInfo;->model:Ljava/lang/String;

    .line 168
    iput-object p7, p0, Lcom/navdy/service/library/events/DeviceInfo;->deviceUuid:Ljava/lang/String;

    .line 169
    iput-object p8, p0, Lcom/navdy/service/library/events/DeviceInfo;->systemApiLevel:Ljava/lang/Integer;

    .line 170
    iput-object p9, p0, Lcom/navdy/service/library/events/DeviceInfo;->kernelVersion:Ljava/lang/String;

    .line 171
    iput-object p10, p0, Lcom/navdy/service/library/events/DeviceInfo;->platform:Lcom/navdy/service/library/events/DeviceInfo$Platform;

    .line 172
    iput-object p11, p0, Lcom/navdy/service/library/events/DeviceInfo;->buildType:Ljava/lang/String;

    .line 173
    iput-object p12, p0, Lcom/navdy/service/library/events/DeviceInfo;->deviceMake:Ljava/lang/String;

    .line 174
    iput-object p13, p0, Lcom/navdy/service/library/events/DeviceInfo;->forceFullUpdate:Ljava/lang/Boolean;

    .line 175
    invoke-static/range {p14 .. p14}, Lcom/navdy/service/library/events/DeviceInfo;->immutableCopyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/service/library/events/DeviceInfo;->legacyCapabilities:Ljava/util/List;

    .line 176
    invoke-static/range {p15 .. p15}, Lcom/navdy/service/library/events/DeviceInfo;->immutableCopyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/service/library/events/DeviceInfo;->musicPlayers_OBSOLETE:Ljava/util/List;

    .line 177
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/navdy/service/library/events/DeviceInfo;->capabilities:Lcom/navdy/service/library/events/Capabilities;

    .line 178
    return-void
.end method

.method static synthetic access$000(Ljava/util/List;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Ljava/util/List;

    .prologue
    .line 25
    invoke-static {p0}, Lcom/navdy/service/library/events/DeviceInfo;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Ljava/util/List;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Ljava/util/List;

    .prologue
    .line 25
    invoke-static {p0}, Lcom/navdy/service/library/events/DeviceInfo;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 187
    if-ne p1, p0, :cond_1

    .line 205
    :cond_0
    :goto_0
    return v1

    .line 188
    :cond_1
    instance-of v3, p1, Lcom/navdy/service/library/events/DeviceInfo;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 189
    check-cast v0, Lcom/navdy/service/library/events/DeviceInfo;

    .line 190
    .local v0, "o":Lcom/navdy/service/library/events/DeviceInfo;
    iget-object v3, p0, Lcom/navdy/service/library/events/DeviceInfo;->deviceId:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/DeviceInfo;->deviceId:Ljava/lang/String;

    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/DeviceInfo;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/DeviceInfo;->clientVersion:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/DeviceInfo;->clientVersion:Ljava/lang/String;

    .line 191
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/DeviceInfo;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/DeviceInfo;->protocolVersion:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/DeviceInfo;->protocolVersion:Ljava/lang/String;

    .line 192
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/DeviceInfo;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/DeviceInfo;->deviceName:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/DeviceInfo;->deviceName:Ljava/lang/String;

    .line 193
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/DeviceInfo;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/DeviceInfo;->systemVersion:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/DeviceInfo;->systemVersion:Ljava/lang/String;

    .line 194
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/DeviceInfo;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/DeviceInfo;->model:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/DeviceInfo;->model:Ljava/lang/String;

    .line 195
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/DeviceInfo;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/DeviceInfo;->deviceUuid:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/DeviceInfo;->deviceUuid:Ljava/lang/String;

    .line 196
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/DeviceInfo;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/DeviceInfo;->systemApiLevel:Ljava/lang/Integer;

    iget-object v4, v0, Lcom/navdy/service/library/events/DeviceInfo;->systemApiLevel:Ljava/lang/Integer;

    .line 197
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/DeviceInfo;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/DeviceInfo;->kernelVersion:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/DeviceInfo;->kernelVersion:Ljava/lang/String;

    .line 198
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/DeviceInfo;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/DeviceInfo;->platform:Lcom/navdy/service/library/events/DeviceInfo$Platform;

    iget-object v4, v0, Lcom/navdy/service/library/events/DeviceInfo;->platform:Lcom/navdy/service/library/events/DeviceInfo$Platform;

    .line 199
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/DeviceInfo;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/DeviceInfo;->buildType:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/DeviceInfo;->buildType:Ljava/lang/String;

    .line 200
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/DeviceInfo;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/DeviceInfo;->deviceMake:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/DeviceInfo;->deviceMake:Ljava/lang/String;

    .line 201
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/DeviceInfo;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/DeviceInfo;->forceFullUpdate:Ljava/lang/Boolean;

    iget-object v4, v0, Lcom/navdy/service/library/events/DeviceInfo;->forceFullUpdate:Ljava/lang/Boolean;

    .line 202
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/DeviceInfo;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/DeviceInfo;->legacyCapabilities:Ljava/util/List;

    iget-object v4, v0, Lcom/navdy/service/library/events/DeviceInfo;->legacyCapabilities:Ljava/util/List;

    .line 203
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/DeviceInfo;->equals(Ljava/util/List;Ljava/util/List;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/DeviceInfo;->musicPlayers_OBSOLETE:Ljava/util/List;

    iget-object v4, v0, Lcom/navdy/service/library/events/DeviceInfo;->musicPlayers_OBSOLETE:Ljava/util/List;

    .line 204
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/DeviceInfo;->equals(Ljava/util/List;Ljava/util/List;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/DeviceInfo;->capabilities:Lcom/navdy/service/library/events/Capabilities;

    iget-object v4, v0, Lcom/navdy/service/library/events/DeviceInfo;->capabilities:Lcom/navdy/service/library/events/Capabilities;

    .line 205
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/DeviceInfo;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto/16 :goto_0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 210
    iget v0, p0, Lcom/navdy/service/library/events/DeviceInfo;->hashCode:I

    .line 211
    .local v0, "result":I
    if-nez v0, :cond_2

    .line 212
    iget-object v2, p0, Lcom/navdy/service/library/events/DeviceInfo;->deviceId:Ljava/lang/String;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/navdy/service/library/events/DeviceInfo;->deviceId:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 213
    :goto_0
    mul-int/lit8 v4, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/DeviceInfo;->clientVersion:Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/navdy/service/library/events/DeviceInfo;->clientVersion:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_1
    add-int v0, v4, v2

    .line 214
    mul-int/lit8 v4, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/DeviceInfo;->protocolVersion:Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/navdy/service/library/events/DeviceInfo;->protocolVersion:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_2
    add-int v0, v4, v2

    .line 215
    mul-int/lit8 v4, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/DeviceInfo;->deviceName:Ljava/lang/String;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/navdy/service/library/events/DeviceInfo;->deviceName:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_3
    add-int v0, v4, v2

    .line 216
    mul-int/lit8 v4, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/DeviceInfo;->systemVersion:Ljava/lang/String;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/navdy/service/library/events/DeviceInfo;->systemVersion:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_4
    add-int v0, v4, v2

    .line 217
    mul-int/lit8 v4, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/DeviceInfo;->model:Ljava/lang/String;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/navdy/service/library/events/DeviceInfo;->model:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_5
    add-int v0, v4, v2

    .line 218
    mul-int/lit8 v4, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/DeviceInfo;->deviceUuid:Ljava/lang/String;

    if-eqz v2, :cond_9

    iget-object v2, p0, Lcom/navdy/service/library/events/DeviceInfo;->deviceUuid:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_6
    add-int v0, v4, v2

    .line 219
    mul-int/lit8 v4, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/DeviceInfo;->systemApiLevel:Ljava/lang/Integer;

    if-eqz v2, :cond_a

    iget-object v2, p0, Lcom/navdy/service/library/events/DeviceInfo;->systemApiLevel:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->hashCode()I

    move-result v2

    :goto_7
    add-int v0, v4, v2

    .line 220
    mul-int/lit8 v4, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/DeviceInfo;->kernelVersion:Ljava/lang/String;

    if-eqz v2, :cond_b

    iget-object v2, p0, Lcom/navdy/service/library/events/DeviceInfo;->kernelVersion:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_8
    add-int v0, v4, v2

    .line 221
    mul-int/lit8 v4, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/DeviceInfo;->platform:Lcom/navdy/service/library/events/DeviceInfo$Platform;

    if-eqz v2, :cond_c

    iget-object v2, p0, Lcom/navdy/service/library/events/DeviceInfo;->platform:Lcom/navdy/service/library/events/DeviceInfo$Platform;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/DeviceInfo$Platform;->hashCode()I

    move-result v2

    :goto_9
    add-int v0, v4, v2

    .line 222
    mul-int/lit8 v4, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/DeviceInfo;->buildType:Ljava/lang/String;

    if-eqz v2, :cond_d

    iget-object v2, p0, Lcom/navdy/service/library/events/DeviceInfo;->buildType:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_a
    add-int v0, v4, v2

    .line 223
    mul-int/lit8 v4, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/DeviceInfo;->deviceMake:Ljava/lang/String;

    if-eqz v2, :cond_e

    iget-object v2, p0, Lcom/navdy/service/library/events/DeviceInfo;->deviceMake:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_b
    add-int v0, v4, v2

    .line 224
    mul-int/lit8 v4, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/DeviceInfo;->forceFullUpdate:Ljava/lang/Boolean;

    if-eqz v2, :cond_f

    iget-object v2, p0, Lcom/navdy/service/library/events/DeviceInfo;->forceFullUpdate:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :goto_c
    add-int v0, v4, v2

    .line 225
    mul-int/lit8 v4, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/DeviceInfo;->legacyCapabilities:Ljava/util/List;

    if-eqz v2, :cond_10

    iget-object v2, p0, Lcom/navdy/service/library/events/DeviceInfo;->legacyCapabilities:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->hashCode()I

    move-result v2

    :goto_d
    add-int v0, v4, v2

    .line 226
    mul-int/lit8 v2, v0, 0x25

    iget-object v4, p0, Lcom/navdy/service/library/events/DeviceInfo;->musicPlayers_OBSOLETE:Ljava/util/List;

    if-eqz v4, :cond_0

    iget-object v3, p0, Lcom/navdy/service/library/events/DeviceInfo;->musicPlayers_OBSOLETE:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->hashCode()I

    move-result v3

    :cond_0
    add-int v0, v2, v3

    .line 227
    mul-int/lit8 v2, v0, 0x25

    iget-object v3, p0, Lcom/navdy/service/library/events/DeviceInfo;->capabilities:Lcom/navdy/service/library/events/Capabilities;

    if-eqz v3, :cond_1

    iget-object v1, p0, Lcom/navdy/service/library/events/DeviceInfo;->capabilities:Lcom/navdy/service/library/events/Capabilities;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/Capabilities;->hashCode()I

    move-result v1

    :cond_1
    add-int v0, v2, v1

    .line 228
    iput v0, p0, Lcom/navdy/service/library/events/DeviceInfo;->hashCode:I

    .line 230
    :cond_2
    return v0

    :cond_3
    move v0, v1

    .line 212
    goto/16 :goto_0

    :cond_4
    move v2, v1

    .line 213
    goto/16 :goto_1

    :cond_5
    move v2, v1

    .line 214
    goto/16 :goto_2

    :cond_6
    move v2, v1

    .line 215
    goto/16 :goto_3

    :cond_7
    move v2, v1

    .line 216
    goto/16 :goto_4

    :cond_8
    move v2, v1

    .line 217
    goto/16 :goto_5

    :cond_9
    move v2, v1

    .line 218
    goto/16 :goto_6

    :cond_a
    move v2, v1

    .line 219
    goto/16 :goto_7

    :cond_b
    move v2, v1

    .line 220
    goto :goto_8

    :cond_c
    move v2, v1

    .line 221
    goto :goto_9

    :cond_d
    move v2, v1

    .line 222
    goto :goto_a

    :cond_e
    move v2, v1

    .line 223
    goto :goto_b

    :cond_f
    move v2, v1

    .line 224
    goto :goto_c

    :cond_10
    move v2, v3

    .line 225
    goto :goto_d
.end method
