.class public Lcom/navdy/client/app/tracking/TrackerConstants$Event;
.super Ljava/lang/Object;
.source "TrackerConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/tracking/TrackerConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Event"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/app/tracking/TrackerConstants$Event$Audio;,
        Lcom/navdy/client/app/tracking/TrackerConstants$Event$Debug;,
        Lcom/navdy/client/app/tracking/TrackerConstants$Event$Settings;,
        Lcom/navdy/client/app/tracking/TrackerConstants$Event$Install;
    }
.end annotation


# static fields
.field public static final BATTERY_LOW:Ljava/lang/String; = "Battery_Low"

.field public static final CAR_INFO_CHANGED:Ljava/lang/String; = "Car_Info_Changed"

.field public static final DELETING_A_FAVORITE:Ljava/lang/String; = "Deleting_A_Favorite"

.field public static final GLANCES_CONFIGURED:Ljava/lang/String; = "Glances_Configured"

.field public static final HAS_KILLED:Ljava/lang/String; = "MapEngine_Forced_Crash"

.field public static final HAS_PHOENIX:Ljava/lang/String; = "MapEngine_Forced_Restart"

.field public static final HUD_CONNECTION_ESTABLISHED:Ljava/lang/String; = "Hud_Connection_Established"

.field public static final HUD_CONNECTION_LOST:Ljava/lang/String; = "Hud_Connection_Lost"

.field public static final HUD_VOICE_SEARCH_AMBIENT_NOISE_FAILURE:Ljava/lang/String; = "Hud_Voice_Search_ambient_noise_failure"

.field public static final MUSIC_PLAYLIST_COMPLETE_REINDEX:Ljava/lang/String; = "Music_Playlist_Complete_Reindex"

.field public static final NAVIGATE_USING_FAVORITES:Ljava/lang/String; = "Navigate_Using_Favorites"

.field public static final NAVIGATE_USING_GOOGLE_NOW:Ljava/lang/String; = "Navigate_Using_Google_Now"

.field public static final NAVIGATE_USING_HUD_VOICE_SEARCH:Ljava/lang/String; = "Navigate_Using_Hud_Voice_Search"

.field public static final NAVIGATE_USING_SEARCH_RESULTS:Ljava/lang/String; = "Navigate_Using_Search_Results"

.field public static final NO_END_POINT:Ljava/lang/String; = "No_End_Point"

.field public static final PAIR_PHONE_TO_DISPLAY:Ljava/lang/String; = "PairPhoneToDisplay"

.field public static final PERMISSION_REJECTED:Ljava/lang/String; = "Permission_Rejected"

.field public static final SAVING_A_FAVORITE:Ljava/lang/String; = "Saving_A_Favorite"

.field public static final SCREEN_VIEWED:Ljava/lang/String; = "Screen_Viewed"

.field public static final SEARCH_FOR_LOCATION:Ljava/lang/String; = "Search_For_Location"

.field public static final SEARCH_USING_DESTINATION_FINDER:Ljava/lang/String; = "Search_Using_Destination_Finder"

.field public static final SEARCH_USING_GOOGLE_NOW:Ljava/lang/String; = "Search_Using_Google_Now"

.field public static final SET_DESTINATION:Ljava/lang/String; = "Set_Destination"

.field public static final SET_HOME:Ljava/lang/String; = "Set_Home"

.field public static final SET_WORK:Ljava/lang/String; = "Set_Work"

.field public static final SUPPORT_TICKET_CREATED:Ljava/lang/String; = "Support_Ticket_Created"

.field public static final SWITCHED_TO_DEFAULT:Ljava/lang/String; = "Switched_to_default"

.field public static final SWITCHED_TO_PLACE_SEARCH:Ljava/lang/String; = "Switched_to_place_search"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 240
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
