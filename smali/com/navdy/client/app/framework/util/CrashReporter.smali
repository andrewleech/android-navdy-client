.class public final Lcom/navdy/client/app/framework/util/CrashReporter;
.super Ljava/lang/Object;
.source "CrashReporter.java"


# static fields
.field private static final sInstance:Lcom/navdy/client/app/framework/util/CrashReporter;

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;

.field private static volatile stopCrashReporting:Z


# instance fields
.field private installed:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 20
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/framework/util/CrashReporter;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/app/framework/util/CrashReporter;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 26
    new-instance v0, Lcom/navdy/client/app/framework/util/CrashReporter;

    invoke-direct {v0}, Lcom/navdy/client/app/framework/util/CrashReporter;-><init>()V

    sput-object v0, Lcom/navdy/client/app/framework/util/CrashReporter;->sInstance:Lcom/navdy/client/app/framework/util/CrashReporter;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/navdy/client/app/framework/util/CrashReporter;Ljava/lang/Thread;Ljava/lang/Throwable;Ljava/lang/Thread$UncaughtExceptionHandler;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/util/CrashReporter;
    .param p1, "x1"    # Ljava/lang/Thread;
    .param p2, "x2"    # Ljava/lang/Throwable;
    .param p3, "x3"    # Ljava/lang/Thread$UncaughtExceptionHandler;

    .prologue
    .line 18
    invoke-direct {p0, p1, p2, p3}, Lcom/navdy/client/app/framework/util/CrashReporter;->handleUncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;Ljava/lang/Thread$UncaughtExceptionHandler;)V

    return-void
.end method

.method public static getInstance()Lcom/navdy/client/app/framework/util/CrashReporter;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/navdy/client/app/framework/util/CrashReporter;->sInstance:Lcom/navdy/client/app/framework/util/CrashReporter;

    return-object v0
.end method

.method private handleUncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;Ljava/lang/Thread$UncaughtExceptionHandler;)V
    .locals 6
    .param p1, "thread"    # Ljava/lang/Thread;
    .param p2, "exception"    # Ljava/lang/Throwable;
    .param p3, "crashlyticsHandler"    # Ljava/lang/Thread$UncaughtExceptionHandler;

    .prologue
    .line 109
    sget-boolean v2, Lcom/navdy/client/app/framework/util/CrashReporter;->stopCrashReporting:Z

    if-eqz v2, :cond_1

    .line 110
    sget-object v2, Lcom/navdy/client/app/framework/util/CrashReporter;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "FATAL-reporting-turned-off"

    invoke-virtual {v2, v3, p2}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 124
    :cond_0
    :goto_0
    return-void

    .line 114
    :cond_1
    const/4 v2, 0x1

    sput-boolean v2, Lcom/navdy/client/app/framework/util/CrashReporter;->stopCrashReporting:Z

    .line 115
    const-string v1, "FATAL-CRASH"

    .line 116
    .local v1, "tag":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " Uncaught exception - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Thread;->getId()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ui thread id:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-virtual {v3}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Thread;->getId()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 117
    .local v0, "msg":Ljava/lang/String;
    invoke-static {v1, v0, p2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 118
    sget-object v2, Lcom/navdy/client/app/framework/util/CrashReporter;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v2, v0, p2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 119
    sget-object v2, Lcom/navdy/client/app/framework/util/CrashReporter;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "closing logger"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 120
    invoke-static {}, Lcom/navdy/service/library/log/Logger;->close()V

    .line 121
    if-eqz p3, :cond_0

    .line 122
    invoke-interface {p3, p1, p2}, Ljava/lang/Thread$UncaughtExceptionHandler;->uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private log(Ljava/lang/String;)V
    .locals 2
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 98
    iget-boolean v1, p0, Lcom/navdy/client/app/framework/util/CrashReporter;->installed:Z

    if-eqz v1, :cond_0

    .line 100
    :try_start_0
    invoke-static {p1}, Lcom/crashlytics/android/Crashlytics;->log(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 105
    :cond_0
    :goto_0
    return-void

    .line 101
    :catch_0
    move-exception v0

    .line 102
    .local v0, "t":Ljava/lang/Throwable;
    sget-object v1, Lcom/navdy/client/app/framework/util/CrashReporter;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v1, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized installCrashHandler(Landroid/content/Context;Ljava/lang/String;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "userId"    # Ljava/lang/String;

    .prologue
    .line 37
    monitor-enter p0

    :try_start_0
    iget-boolean v3, p0, Lcom/navdy/client/app/framework/util/CrashReporter;->installed:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v3, :cond_0

    .line 78
    :goto_0
    monitor-exit p0

    return-void

    .line 42
    :cond_0
    :try_start_1
    invoke-static {}, Ljava/lang/Thread;->getDefaultUncaughtExceptionHandler()Ljava/lang/Thread$UncaughtExceptionHandler;

    move-result-object v1

    .line 43
    .local v1, "defaultUncaughtHandler":Ljava/lang/Thread$UncaughtExceptionHandler;
    if-eqz v1, :cond_2

    .line 44
    sget-object v3, Lcom/navdy/client/app/framework/util/CrashReporter;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "default uncaught handler:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 50
    :goto_1
    new-instance v3, Lio/fabric/sdk/android/Fabric$Builder;

    invoke-direct {v3, p1}, Lio/fabric/sdk/android/Fabric$Builder;-><init>(Landroid/content/Context;)V

    const/4 v4, 0x2

    new-array v4, v4, [Lio/fabric/sdk/android/Kit;

    const/4 v5, 0x0

    new-instance v6, Lcom/crashlytics/android/Crashlytics;

    invoke-direct {v6}, Lcom/crashlytics/android/Crashlytics;-><init>()V

    aput-object v6, v4, v5

    const/4 v5, 0x1

    new-instance v6, Lcom/crashlytics/android/ndk/CrashlyticsNdk;

    invoke-direct {v6}, Lcom/crashlytics/android/ndk/CrashlyticsNdk;-><init>()V

    aput-object v6, v4, v5

    invoke-virtual {v3, v4}, Lio/fabric/sdk/android/Fabric$Builder;->kits([Lio/fabric/sdk/android/Kit;)Lio/fabric/sdk/android/Fabric$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lio/fabric/sdk/android/Fabric$Builder;->build()Lio/fabric/sdk/android/Fabric;

    move-result-object v2

    .line 51
    .local v2, "fabric":Lio/fabric/sdk/android/Fabric;
    invoke-static {v2}, Lio/fabric/sdk/android/Fabric;->with(Lio/fabric/sdk/android/Fabric;)Lio/fabric/sdk/android/Fabric;

    .line 52
    if-eqz p2, :cond_1

    .line 53
    invoke-static {p2}, Lcom/crashlytics/android/Crashlytics;->setUserIdentifier(Ljava/lang/String;)V

    .line 55
    :cond_1
    const-string v3, "Serial"

    sget-object v4, Landroid/os/Build;->SERIAL:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/crashlytics/android/Crashlytics;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    sget-object v3, Lcom/navdy/client/app/framework/util/CrashReporter;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "crashlytics installed with user:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 60
    invoke-static {}, Ljava/lang/Thread;->getDefaultUncaughtExceptionHandler()Ljava/lang/Thread$UncaughtExceptionHandler;

    move-result-object v0

    .line 61
    .local v0, "crashlyticsHandler":Ljava/lang/Thread$UncaughtExceptionHandler;
    if-eqz v0, :cond_3

    .line 62
    sget-object v3, Lcom/navdy/client/app/framework/util/CrashReporter;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "crashlytics uncaught handler:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 67
    :goto_2
    new-instance v3, Lcom/navdy/client/app/framework/util/CrashReporter$1;

    invoke-direct {v3, p0, v0}, Lcom/navdy/client/app/framework/util/CrashReporter$1;-><init>(Lcom/navdy/client/app/framework/util/CrashReporter;Ljava/lang/Thread$UncaughtExceptionHandler;)V

    invoke-static {v3}, Ljava/lang/Thread;->setDefaultUncaughtExceptionHandler(Ljava/lang/Thread$UncaughtExceptionHandler;)V

    .line 75
    new-instance v3, Lcom/navdy/client/app/framework/util/CrashlyticsAppender;

    invoke-direct {v3}, Lcom/navdy/client/app/framework/util/CrashlyticsAppender;-><init>()V

    invoke-static {v3}, Lcom/navdy/service/library/log/Logger;->addAppender(Lcom/navdy/service/library/log/LogAppender;)V

    .line 77
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/navdy/client/app/framework/util/CrashReporter;->installed:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 37
    .end local v0    # "crashlyticsHandler":Ljava/lang/Thread$UncaughtExceptionHandler;
    .end local v1    # "defaultUncaughtHandler":Ljava/lang/Thread$UncaughtExceptionHandler;
    .end local v2    # "fabric":Lio/fabric/sdk/android/Fabric;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    .line 46
    .restart local v1    # "defaultUncaughtHandler":Ljava/lang/Thread$UncaughtExceptionHandler;
    :cond_2
    :try_start_2
    sget-object v3, Lcom/navdy/client/app/framework/util/CrashReporter;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "default uncaught handler is null"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 64
    .restart local v0    # "crashlyticsHandler":Ljava/lang/Thread$UncaughtExceptionHandler;
    .restart local v2    # "fabric":Lio/fabric/sdk/android/Fabric;
    :cond_3
    sget-object v3, Lcom/navdy/client/app/framework/util/CrashReporter;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "crashlytics uncaught handler is null"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2
.end method

.method public reportNonFatalException(Ljava/lang/Throwable;)V
    .locals 2
    .param p1, "throwable"    # Ljava/lang/Throwable;

    .prologue
    .line 88
    iget-boolean v1, p0, Lcom/navdy/client/app/framework/util/CrashReporter;->installed:Z

    if-eqz v1, :cond_0

    .line 90
    :try_start_0
    invoke-static {p1}, Lcom/crashlytics/android/Crashlytics;->logException(Ljava/lang/Throwable;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 95
    :cond_0
    :goto_0
    return-void

    .line 91
    :catch_0
    move-exception v0

    .line 92
    .local v0, "t":Ljava/lang/Throwable;
    sget-object v1, Lcom/navdy/client/app/framework/util/CrashReporter;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v1, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public declared-synchronized setUser(Ljava/lang/String;)V
    .locals 1
    .param p1, "userId"    # Ljava/lang/String;

    .prologue
    .line 81
    monitor-enter p0

    if-eqz p1, :cond_0

    .line 82
    :try_start_0
    invoke-static {p1}, Lcom/crashlytics/android/Crashlytics;->setUserIdentifier(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 84
    :cond_0
    monitor-exit p0

    return-void

    .line 81
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
