.class public Lcom/navdy/client/app/framework/AppInstance;
.super Lcom/navdy/service/library/util/Listenable;
.source "AppInstance.java"

# interfaces
.implements Lcom/navdy/client/app/framework/service/ClientConnectionService$DeviceChangeBroadcaster$Listener;
.implements Lcom/google/android/gms/common/api/GoogleApiClient$ConnectionCallbacks;
.implements Lcom/google/android/gms/common/api/GoogleApiClient$OnConnectionFailedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/app/framework/AppInstance$Listener;,
        Lcom/navdy/client/app/framework/AppInstance$EventDispatcher;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/navdy/service/library/util/Listenable",
        "<",
        "Lcom/navdy/client/app/framework/AppInstance$Listener;",
        ">;",
        "Lcom/navdy/client/app/framework/service/ClientConnectionService$DeviceChangeBroadcaster$Listener;",
        "Lcom/google/android/gms/common/api/GoogleApiClient$ConnectionCallbacks;",
        "Lcom/google/android/gms/common/api/GoogleApiClient$OnConnectionFailedListener;"
    }
.end annotation


# static fields
.field private static final ACTIVITY_RECOGNITION_UPDATE_INTERVAL:J

.field private static final DATETIME_CHANGE_INTENT_FILTER:Landroid/content/IntentFilter;

.field private static final EMPTY:Ljava/lang/String; = ""

.field private static final REQUEST_CODE:I = 0x8707

.field private static final SUGGESTION_KICK_OFF_DELAY_AFTER_CONNECTED:J = 0x3a98L

.field private static final TRIP_DB_CHECK_KICK_OFF_DELAY_AFTER_CONNECTED:J = 0xea60L

.field private static volatile sInstance:Lcom/navdy/client/app/framework/AppInstance;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "StaticFieldLeak"
        }
    .end annotation
.end field

.field private static final sInstanceLock:Ljava/lang/Object;

.field public static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private busIsRegistered:Z

.field private calendarHandler:Lcom/navdy/client/app/framework/servicehandler/CalendarHandler;

.field private final datetimeUpdateReceiver:Landroid/content/BroadcastReceiver;

.field private volatile deviceConnected:Z

.field private isHudMapEngineReady:Z

.field private mApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

.field private mAppInitialized:Z

.field private mBatteryStatus:Lcom/navdy/client/app/framework/servicehandler/BatteryStatusManager;

.field private mConnectionService:Lcom/navdy/client/app/framework/service/ClientConnectionService;

.field private mContext:Landroid/content/Context;

.field public mLocationTransmitter:Lcom/navdy/client/app/framework/servicehandler/LocationTransmitter;

.field private mMainHandler:Landroid/os/Handler;

.field private mNavigationSessionState:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

.field mNetworkStatus:Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private mRemoteDeviceRegistry:Lcom/navdy/service/library/device/RemoteDeviceRegistry;

.field private serviceConnection:Landroid/content/ServiceConnection;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 79
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/framework/AppInstance;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/app/framework/AppInstance;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 83
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0xa

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/navdy/client/app/framework/AppInstance;->ACTIVITY_RECOGNITION_UPDATE_INTERVAL:J

    .line 88
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/navdy/client/app/framework/AppInstance;->sInstanceLock:Ljava/lang/Object;

    .line 118
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.DATE_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/client/app/framework/AppInstance;->DATETIME_CHANGE_INTENT_FILTER:Landroid/content/IntentFilter;

    .line 119
    sget-object v0, Lcom/navdy/client/app/framework/AppInstance;->DATETIME_CHANGE_INTENT_FILTER:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.TIME_SET"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 120
    sget-object v0, Lcom/navdy/client/app/framework/AppInstance;->DATETIME_CHANGE_INTENT_FILTER:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.TIMEZONE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 121
    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 125
    invoke-direct {p0}, Lcom/navdy/service/library/util/Listenable;-><init>()V

    .line 109
    iput-boolean v1, p0, Lcom/navdy/client/app/framework/AppInstance;->deviceConnected:Z

    .line 111
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/client/app/framework/AppInstance;->calendarHandler:Lcom/navdy/client/app/framework/servicehandler/CalendarHandler;

    .line 113
    iput-boolean v1, p0, Lcom/navdy/client/app/framework/AppInstance;->mAppInitialized:Z

    .line 123
    iput-boolean v1, p0, Lcom/navdy/client/app/framework/AppInstance;->busIsRegistered:Z

    .line 202
    new-instance v0, Lcom/navdy/client/app/framework/AppInstance$1;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/framework/AppInstance$1;-><init>(Lcom/navdy/client/app/framework/AppInstance;)V

    iput-object v0, p0, Lcom/navdy/client/app/framework/AppInstance;->serviceConnection:Landroid/content/ServiceConnection;

    .line 568
    new-instance v0, Lcom/navdy/client/app/framework/AppInstance$6;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/framework/AppInstance$6;-><init>(Lcom/navdy/client/app/framework/AppInstance;)V

    iput-object v0, p0, Lcom/navdy/client/app/framework/AppInstance;->datetimeUpdateReceiver:Landroid/content/BroadcastReceiver;

    .line 126
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/framework/AppInstance;->mContext:Landroid/content/Context;

    .line 127
    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/client/app/framework/AppInstance;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/navdy/client/app/framework/AppInstance;->mMainHandler:Landroid/os/Handler;

    .line 128
    sget-object v0, Lcom/navdy/service/library/events/navigation/NavigationSessionState;->NAV_SESSION_ENGINE_NOT_READY:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    iput-object v0, p0, Lcom/navdy/client/app/framework/AppInstance;->mNavigationSessionState:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    .line 129
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/navdy/client/app/framework/Injector;->inject(Landroid/content/Context;Ljava/lang/Object;)V

    .line 130
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/client/app/framework/AppInstance;)Lcom/navdy/client/app/framework/service/ClientConnectionService;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/framework/AppInstance;

    .prologue
    .line 75
    iget-object v0, p0, Lcom/navdy/client/app/framework/AppInstance;->mConnectionService:Lcom/navdy/client/app/framework/service/ClientConnectionService;

    return-object v0
.end method

.method static synthetic access$002(Lcom/navdy/client/app/framework/AppInstance;Lcom/navdy/client/app/framework/service/ClientConnectionService;)Lcom/navdy/client/app/framework/service/ClientConnectionService;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/AppInstance;
    .param p1, "x1"    # Lcom/navdy/client/app/framework/service/ClientConnectionService;

    .prologue
    .line 75
    iput-object p1, p0, Lcom/navdy/client/app/framework/AppInstance;->mConnectionService:Lcom/navdy/client/app/framework/service/ClientConnectionService;

    return-object p1
.end method

.method static synthetic access$100(Lcom/navdy/client/app/framework/AppInstance;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/framework/AppInstance;

    .prologue
    .line 75
    iget-object v0, p0, Lcom/navdy/client/app/framework/AppInstance;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/client/app/framework/AppInstance;Lcom/navdy/service/library/device/RemoteDevice;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/AppInstance;
    .param p1, "x1"    # Lcom/navdy/service/library/device/RemoteDevice;
    .param p2, "x2"    # Z

    .prologue
    .line 75
    invoke-direct {p0, p1, p2}, Lcom/navdy/client/app/framework/AppInstance;->sendCurrentTime(Lcom/navdy/service/library/device/RemoteDevice;Z)V

    return-void
.end method

.method public static getInstance()Lcom/navdy/client/app/framework/AppInstance;
    .locals 2

    .prologue
    .line 133
    sget-object v0, Lcom/navdy/client/app/framework/AppInstance;->sInstance:Lcom/navdy/client/app/framework/AppInstance;

    if-nez v0, :cond_1

    .line 134
    sget-object v1, Lcom/navdy/client/app/framework/AppInstance;->sInstanceLock:Ljava/lang/Object;

    monitor-enter v1

    .line 135
    :try_start_0
    sget-object v0, Lcom/navdy/client/app/framework/AppInstance;->sInstance:Lcom/navdy/client/app/framework/AppInstance;

    if-nez v0, :cond_0

    .line 136
    new-instance v0, Lcom/navdy/client/app/framework/AppInstance;

    invoke-direct {v0}, Lcom/navdy/client/app/framework/AppInstance;-><init>()V

    sput-object v0, Lcom/navdy/client/app/framework/AppInstance;->sInstance:Lcom/navdy/client/app/framework/AppInstance;

    .line 138
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 140
    :cond_1
    sget-object v0, Lcom/navdy/client/app/framework/AppInstance;->sInstance:Lcom/navdy/client/app/framework/AppInstance;

    return-object v0

    .line 138
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private getSystemVersion()Ljava/lang/String;
    .locals 2

    .prologue
    .line 552
    const/16 v1, 0x6b6

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    .line 553
    .local v0, "appVersion":Ljava/lang/String;
    sget-object v1, Landroid/os/Build$VERSION;->INCREMENTAL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 554
    const-string v1, "1.3.1718-e615013"

    .line 556
    :goto_0
    return-object v1

    :cond_0
    sget-object v1, Landroid/os/Build$VERSION;->INCREMENTAL:Ljava/lang/String;

    goto :goto_0
.end method

.method private initializeConnectionService()V
    .locals 5

    .prologue
    .line 177
    sget-object v2, Lcom/navdy/client/app/framework/AppInstance;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "init DeviceConnection"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 178
    invoke-static {}, Lcom/navdy/client/app/framework/DeviceConnection;->getInstance()Lcom/navdy/client/app/framework/DeviceConnection;

    .line 179
    sget-object v2, Lcom/navdy/client/app/framework/AppInstance;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Binding to client connection service"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 180
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/navdy/client/app/framework/AppInstance;->mContext:Landroid/content/Context;

    const-class v3, Lcom/navdy/client/app/framework/service/ClientConnectionService;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 181
    .local v1, "intent":Landroid/content/Intent;
    const-class v2, Lcom/navdy/client/app/framework/service/ClientConnectionService;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 183
    :try_start_0
    iget-object v2, p0, Lcom/navdy/client/app/framework/AppInstance;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/navdy/client/app/framework/AppInstance;->serviceConnection:Landroid/content/ServiceConnection;

    const/4 v4, 0x1

    invoke-virtual {v2, v1, v3, v4}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 188
    :goto_0
    return-void

    .line 184
    :catch_0
    move-exception v0

    .line 185
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 186
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/navdy/client/app/framework/AppInstance;->mAppInitialized:Z

    goto :goto_0
.end method

.method private initializeRegistry()V
    .locals 3

    .prologue
    .line 166
    iget-object v1, p0, Lcom/navdy/client/app/framework/AppInstance;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/navdy/service/library/device/RemoteDeviceRegistry;->getInstance(Landroid/content/Context;)Lcom/navdy/service/library/device/RemoteDeviceRegistry;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/client/app/framework/AppInstance;->mRemoteDeviceRegistry:Lcom/navdy/service/library/device/RemoteDeviceRegistry;

    .line 171
    new-instance v0, Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner;

    iget-object v1, p0, Lcom/navdy/client/app/framework/AppInstance;->mContext:Landroid/content/Context;

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner;-><init>(Landroid/content/Context;I)V

    .line 173
    .local v0, "scanner":Lcom/navdy/service/library/device/discovery/RemoteDeviceScanner;
    iget-object v1, p0, Lcom/navdy/client/app/framework/AppInstance;->mRemoteDeviceRegistry:Lcom/navdy/service/library/device/RemoteDeviceRegistry;

    invoke-virtual {v1, v0}, Lcom/navdy/service/library/device/RemoteDeviceRegistry;->addRemoteDeviceScanner(Lcom/navdy/service/library/device/discovery/RemoteDeviceScanner;)V

    .line 174
    return-void
.end method

.method private declared-synchronized removeActivityRecognitionUpdates()V
    .locals 6

    .prologue
    .line 407
    monitor-enter p0

    :try_start_0
    sget-object v3, Lcom/navdy/client/app/framework/AppInstance;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "GoogleApiClient : removing activity updates"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 408
    iget-object v3, p0, Lcom/navdy/client/app/framework/AppInstance;->mApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/navdy/client/app/framework/AppInstance;->mApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-virtual {v3}, Lcom/google/android/gms/common/api/GoogleApiClient;->isConnected()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 409
    new-instance v1, Landroid/content/Intent;

    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v3

    const-class v4, Lcom/navdy/client/app/service/ActivityRecognizedCallbackService;

    invoke-direct {v1, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 410
    .local v1, "intent":Landroid/content/Intent;
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x8707

    const/high16 v5, 0x8000000

    invoke-static {v3, v4, v1, v5}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 411
    .local v2, "pendingIntent":Landroid/app/PendingIntent;
    sget-object v3, Lcom/google/android/gms/location/ActivityRecognition;->ActivityRecognitionApi:Lcom/google/android/gms/location/ActivityRecognitionApi;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v3, :cond_0

    if-eqz v2, :cond_0

    .line 414
    :try_start_1
    sget-object v3, Lcom/google/android/gms/location/ActivityRecognition;->ActivityRecognitionApi:Lcom/google/android/gms/location/ActivityRecognitionApi;

    iget-object v4, p0, Lcom/navdy/client/app/framework/AppInstance;->mApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    .line 415
    invoke-interface {v3, v4, v2}, Lcom/google/android/gms/location/ActivityRecognitionApi;->removeActivityUpdates(Lcom/google/android/gms/common/api/GoogleApiClient;Landroid/app/PendingIntent;)Lcom/google/android/gms/common/api/PendingResult;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 421
    .end local v1    # "intent":Landroid/content/Intent;
    .end local v2    # "pendingIntent":Landroid/app/PendingIntent;
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 416
    .restart local v1    # "intent":Landroid/content/Intent;
    .restart local v2    # "pendingIntent":Landroid/app/PendingIntent;
    :catch_0
    move-exception v0

    .line 417
    .local v0, "e":Ljava/lang/NullPointerException;
    :try_start_2
    sget-object v3, Lcom/navdy/client/app/framework/AppInstance;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "Something went wrong while trying to remove activity recognition updates"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 407
    .end local v0    # "e":Ljava/lang/NullPointerException;
    .end local v1    # "intent":Landroid/content/Intent;
    .end local v2    # "pendingIntent":Landroid/app/PendingIntent;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method private declared-synchronized requestActivityRecognitionUpdates()V
    .locals 6

    .prologue
    .line 391
    monitor-enter p0

    :try_start_0
    sget-object v2, Lcom/navdy/client/app/framework/AppInstance;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "GoogleApiClient : requesting activity updates"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 392
    iget-object v2, p0, Lcom/navdy/client/app/framework/AppInstance;->mApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-virtual {v2}, Lcom/google/android/gms/common/api/GoogleApiClient;->isConnected()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 393
    sget-object v2, Lcom/navdy/client/app/framework/AppInstance;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "GoogleApiClient is already connected, requesting the updates"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 394
    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/navdy/client/app/service/ActivityRecognizedCallbackService;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 395
    .local v0, "intent":Landroid/content/Intent;
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x8707

    const/high16 v4, 0x8000000

    invoke-static {v2, v3, v0, v4}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 396
    .local v1, "pendingIntent":Landroid/app/PendingIntent;
    sget-object v2, Lcom/google/android/gms/location/ActivityRecognition;->ActivityRecognitionApi:Lcom/google/android/gms/location/ActivityRecognitionApi;

    if-eqz v2, :cond_0

    .line 397
    sget-object v2, Lcom/google/android/gms/location/ActivityRecognition;->ActivityRecognitionApi:Lcom/google/android/gms/location/ActivityRecognitionApi;

    iget-object v3, p0, Lcom/navdy/client/app/framework/AppInstance;->mApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    sget-wide v4, Lcom/navdy/client/app/framework/AppInstance;->ACTIVITY_RECOGNITION_UPDATE_INTERVAL:J

    invoke-interface {v2, v3, v4, v5, v1}, Lcom/google/android/gms/location/ActivityRecognitionApi;->requestActivityUpdates(Lcom/google/android/gms/common/api/GoogleApiClient;JLandroid/app/PendingIntent;)Lcom/google/android/gms/common/api/PendingResult;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 404
    .end local v0    # "intent":Landroid/content/Intent;
    .end local v1    # "pendingIntent":Landroid/app/PendingIntent;
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 402
    :cond_1
    :try_start_1
    sget-object v2, Lcom/navdy/client/app/framework/AppInstance;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "The GoogleApiClient is not connected yet.Waiting for it to be connected"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 391
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method private sendCurrentTime(Lcom/navdy/service/library/device/RemoteDevice;Z)V
    .locals 6
    .param p1, "remoteDevice"    # Lcom/navdy/service/library/device/RemoteDevice;
    .param p2, "is24HourFormat"    # Z

    .prologue
    .line 561
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 562
    .local v0, "calendar":Ljava/util/Calendar;
    new-instance v2, Lcom/navdy/service/library/events/settings/DateTimeConfiguration;

    .line 563
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    .line 564
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v4

    if-eqz p2, :cond_0

    sget-object v1, Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;->CLOCK_24_HOUR:Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;

    :goto_0
    invoke-direct {v2, v3, v4, v1}, Lcom/navdy/service/library/events/settings/DateTimeConfiguration;-><init>(Ljava/lang/Long;Ljava/lang/String;Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;)V

    .line 562
    invoke-virtual {p1, v2}, Lcom/navdy/service/library/device/RemoteDevice;->postEvent(Lcom/squareup/wire/Message;)Z

    .line 566
    return-void

    .line 564
    :cond_0
    sget-object v1, Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;->CLOCK_12_HOUR:Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;

    goto :goto_0
.end method

.method private sendPhoneInfo(Lcom/navdy/service/library/device/RemoteDevice;)V
    .locals 9
    .param p1, "device"    # Lcom/navdy/service/library/device/RemoteDevice;

    .prologue
    .line 509
    if-nez p1, :cond_0

    .line 510
    sget-object v6, Lcom/navdy/client/app/framework/AppInstance;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v7, "called setPhoneInfo with a null RemoteDevice!"

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 549
    :goto_0
    return-void

    .line 515
    :cond_0
    :try_start_0
    new-instance v1, Lcom/navdy/service/library/events/Capabilities$Builder;

    invoke-direct {v1}, Lcom/navdy/service/library/events/Capabilities$Builder;-><init>()V

    .line 516
    .local v1, "capabilitiesBuilder":Lcom/navdy/service/library/events/Capabilities$Builder;
    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/navdy/service/library/events/Capabilities$Builder;->placeTypeSearch(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/Capabilities$Builder;

    .line 517
    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/navdy/service/library/events/Capabilities$Builder;->voiceSearch(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/Capabilities$Builder;

    .line 518
    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/navdy/service/library/events/Capabilities$Builder;->navCoordsLookup(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/Capabilities$Builder;

    .line 519
    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/navdy/service/library/events/Capabilities$Builder;->cannedResponseToSms(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/Capabilities$Builder;

    .line 520
    invoke-virtual {v1}, Lcom/navdy/service/library/events/Capabilities$Builder;->build()Lcom/navdy/service/library/events/Capabilities;

    move-result-object v0

    .line 522
    .local v0, "capabilities":Lcom/navdy/service/library/events/Capabilities;
    iget-object v6, p0, Lcom/navdy/client/app/framework/AppInstance;->mContext:Landroid/content/Context;

    invoke-static {v6}, Lcom/navdy/service/library/device/NavdyDeviceId;->getThisDevice(Landroid/content/Context;)Lcom/navdy/service/library/device/NavdyDeviceId;

    move-result-object v2

    .line 523
    .local v2, "deviceId":Lcom/navdy/service/library/device/NavdyDeviceId;
    invoke-virtual {v2}, Lcom/navdy/service/library/device/NavdyDeviceId;->getDeviceName()Ljava/lang/String;

    move-result-object v3

    .line 524
    .local v3, "deviceName":Ljava/lang/String;
    if-nez v3, :cond_1

    .line 525
    const-string v3, ""

    .line 528
    :cond_1
    new-instance v6, Lcom/navdy/service/library/events/DeviceInfo$Builder;

    invoke-direct {v6}, Lcom/navdy/service/library/events/DeviceInfo$Builder;-><init>()V

    .line 529
    invoke-virtual {v2}, Lcom/navdy/service/library/device/NavdyDeviceId;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/events/DeviceInfo$Builder;->deviceId(Ljava/lang/String;)Lcom/navdy/service/library/events/DeviceInfo$Builder;

    move-result-object v6

    const-string v7, "1.3.1718-e615013"

    .line 530
    invoke-virtual {v6, v7}, Lcom/navdy/service/library/events/DeviceInfo$Builder;->clientVersion(Ljava/lang/String;)Lcom/navdy/service/library/events/DeviceInfo$Builder;

    move-result-object v6

    sget-object v7, Lcom/navdy/service/library/Version;->PROTOCOL_VERSION:Lcom/navdy/service/library/Version;

    .line 531
    invoke-virtual {v7}, Lcom/navdy/service/library/Version;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/events/DeviceInfo$Builder;->protocolVersion(Ljava/lang/String;)Lcom/navdy/service/library/events/DeviceInfo$Builder;

    move-result-object v6

    .line 532
    invoke-virtual {v6, v3}, Lcom/navdy/service/library/events/DeviceInfo$Builder;->deviceName(Ljava/lang/String;)Lcom/navdy/service/library/events/DeviceInfo$Builder;

    move-result-object v6

    .line 533
    invoke-direct {p0}, Lcom/navdy/client/app/framework/AppInstance;->getSystemVersion()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/events/DeviceInfo$Builder;->systemVersion(Ljava/lang/String;)Lcom/navdy/service/library/events/DeviceInfo$Builder;

    move-result-object v6

    sget-object v7, Landroid/os/Build;->MODEL:Ljava/lang/String;

    .line 534
    invoke-virtual {v6, v7}, Lcom/navdy/service/library/events/DeviceInfo$Builder;->model(Ljava/lang/String;)Lcom/navdy/service/library/events/DeviceInfo$Builder;

    move-result-object v6

    const-string v7, "UUID"

    .line 535
    invoke-virtual {v6, v7}, Lcom/navdy/service/library/events/DeviceInfo$Builder;->deviceUuid(Ljava/lang/String;)Lcom/navdy/service/library/events/DeviceInfo$Builder;

    move-result-object v6

    sget v7, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 536
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/events/DeviceInfo$Builder;->systemApiLevel(Ljava/lang/Integer;)Lcom/navdy/service/library/events/DeviceInfo$Builder;

    move-result-object v6

    const-string v7, "os.version"

    .line 537
    invoke-static {v7}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/events/DeviceInfo$Builder;->kernelVersion(Ljava/lang/String;)Lcom/navdy/service/library/events/DeviceInfo$Builder;

    move-result-object v6

    sget-object v7, Lcom/navdy/service/library/events/DeviceInfo$Platform;->PLATFORM_Android:Lcom/navdy/service/library/events/DeviceInfo$Platform;

    .line 538
    invoke-virtual {v6, v7}, Lcom/navdy/service/library/events/DeviceInfo$Builder;->platform(Lcom/navdy/service/library/events/DeviceInfo$Platform;)Lcom/navdy/service/library/events/DeviceInfo$Builder;

    move-result-object v6

    sget-object v7, Landroid/os/Build;->TYPE:Ljava/lang/String;

    .line 539
    invoke-virtual {v6, v7}, Lcom/navdy/service/library/events/DeviceInfo$Builder;->buildType(Ljava/lang/String;)Lcom/navdy/service/library/events/DeviceInfo$Builder;

    move-result-object v6

    sget-object v7, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    .line 540
    invoke-virtual {v6, v7}, Lcom/navdy/service/library/events/DeviceInfo$Builder;->deviceMake(Ljava/lang/String;)Lcom/navdy/service/library/events/DeviceInfo$Builder;

    move-result-object v6

    .line 541
    invoke-virtual {v6, v0}, Lcom/navdy/service/library/events/DeviceInfo$Builder;->capabilities(Lcom/navdy/service/library/events/Capabilities;)Lcom/navdy/service/library/events/DeviceInfo$Builder;

    move-result-object v6

    .line 542
    invoke-virtual {v6}, Lcom/navdy/service/library/events/DeviceInfo$Builder;->build()Lcom/navdy/service/library/events/DeviceInfo;

    move-result-object v4

    .line 544
    .local v4, "phoneInfo":Lcom/navdy/service/library/events/DeviceInfo;
    sget-object v6, Lcom/navdy/client/app/framework/AppInstance;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "sending device info:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 545
    invoke-virtual {p1, v4}, Lcom/navdy/service/library/device/RemoteDevice;->postEvent(Lcom/squareup/wire/Message;)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 546
    .end local v0    # "capabilities":Lcom/navdy/service/library/events/Capabilities;
    .end local v1    # "capabilitiesBuilder":Lcom/navdy/service/library/events/Capabilities$Builder;
    .end local v2    # "deviceId":Lcom/navdy/service/library/device/NavdyDeviceId;
    .end local v3    # "deviceName":Ljava/lang/String;
    .end local v4    # "phoneInfo":Lcom/navdy/service/library/events/DeviceInfo;
    :catch_0
    move-exception v5

    .line 547
    .local v5, "t":Ljava/lang/Throwable;
    sget-object v6, Lcom/navdy/client/app/framework/AppInstance;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v6, v5}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto/16 :goto_0
.end method


# virtual methods
.method public canReachInternet()Z
    .locals 1

    .prologue
    .line 428
    iget-object v0, p0, Lcom/navdy/client/app/framework/AppInstance;->mNetworkStatus:Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->canReachInternet()Z

    move-result v0

    return v0
.end method

.method public checkForNetwork()V
    .locals 1

    .prologue
    .line 432
    iget-object v0, p0, Lcom/navdy/client/app/framework/AppInstance;->mNetworkStatus:Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->checkForNetwork()Z

    .line 433
    return-void
.end method

.method public getNavigationSessionState()Lcom/navdy/service/library/events/navigation/NavigationSessionState;
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/navdy/client/app/framework/AppInstance;->mNavigationSessionState:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    return-object v0
.end method

.method public getRemoteDevice()Lcom/navdy/service/library/device/RemoteDevice;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 228
    iget-object v0, p0, Lcom/navdy/client/app/framework/AppInstance;->mConnectionService:Lcom/navdy/client/app/framework/service/ClientConnectionService;

    if-eqz v0, :cond_0

    .line 229
    iget-object v0, p0, Lcom/navdy/client/app/framework/AppInstance;->mConnectionService:Lcom/navdy/client/app/framework/service/ClientConnectionService;

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/service/ClientConnectionService;->getRemoteDevice()Lcom/navdy/service/library/device/RemoteDevice;

    move-result-object v0

    .line 230
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public declared-synchronized initializeApp()V
    .locals 2

    .prologue
    .line 144
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/navdy/client/app/framework/AppInstance;->mAppInitialized:Z

    if-nez v0, :cond_0

    .line 145
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/client/app/framework/AppInstance;->mAppInitialized:Z

    .line 146
    invoke-direct {p0}, Lcom/navdy/client/app/framework/AppInstance;->initializeRegistry()V

    .line 147
    invoke-direct {p0}, Lcom/navdy/client/app/framework/AppInstance;->initializeConnectionService()V

    .line 148
    iget-object v0, p0, Lcom/navdy/client/app/framework/AppInstance;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/navdy/client/app/framework/service/TaskRemovalService;->startService(Landroid/content/Context;)V

    .line 149
    new-instance v0, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;

    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;-><init>(Landroid/content/Context;)V

    sget-object v1, Lcom/google/android/gms/location/ActivityRecognition;->API:Lcom/google/android/gms/common/api/Api;

    .line 150
    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;->addApi(Lcom/google/android/gms/common/api/Api;)Lcom/google/android/gms/common/api/GoogleApiClient$Builder;

    move-result-object v0

    .line 151
    invoke-virtual {v0, p0}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;->addConnectionCallbacks(Lcom/google/android/gms/common/api/GoogleApiClient$ConnectionCallbacks;)Lcom/google/android/gms/common/api/GoogleApiClient$Builder;

    move-result-object v0

    .line 152
    invoke-virtual {v0, p0}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;->addOnConnectionFailedListener(Lcom/google/android/gms/common/api/GoogleApiClient$OnConnectionFailedListener;)Lcom/google/android/gms/common/api/GoogleApiClient$Builder;

    move-result-object v0

    .line 153
    invoke-virtual {v0}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;->build()Lcom/google/android/gms/common/api/GoogleApiClient;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/framework/AppInstance;->mApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    .line 154
    iget-object v0, p0, Lcom/navdy/client/app/framework/AppInstance;->mApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/GoogleApiClient;->connect()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 156
    :cond_0
    monitor-exit p0

    return-void

    .line 144
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public initializeDevice()V
    .locals 2

    .prologue
    .line 159
    iget-object v1, p0, Lcom/navdy/client/app/framework/AppInstance;->mRemoteDeviceRegistry:Lcom/navdy/service/library/device/RemoteDeviceRegistry;

    invoke-virtual {v1}, Lcom/navdy/service/library/device/RemoteDeviceRegistry;->getDefaultConnectionInfo()Lcom/navdy/service/library/device/connection/ConnectionInfo;

    move-result-object v0

    .line 160
    .local v0, "connectionInfo":Lcom/navdy/service/library/device/connection/ConnectionInfo;
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/navdy/client/app/framework/AppInstance;->mConnectionService:Lcom/navdy/client/app/framework/service/ClientConnectionService;

    if-eqz v1, :cond_0

    .line 161
    iget-object v1, p0, Lcom/navdy/client/app/framework/AppInstance;->mConnectionService:Lcom/navdy/client/app/framework/service/ClientConnectionService;

    invoke-virtual {v1, v0}, Lcom/navdy/client/app/framework/service/ClientConnectionService;->connect(Lcom/navdy/service/library/device/connection/ConnectionInfo;)V

    .line 163
    :cond_0
    return-void
.end method

.method public isDeviceConnected()Z
    .locals 1

    .prologue
    .line 424
    iget-boolean v0, p0, Lcom/navdy/client/app/framework/AppInstance;->deviceConnected:Z

    return v0
.end method

.method public isHudMapEngineReady()Z
    .locals 1

    .prologue
    .line 238
    iget-boolean v0, p0, Lcom/navdy/client/app/framework/AppInstance;->isHudMapEngineReady:Z

    return v0
.end method

.method public declared-synchronized onConnected(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "bundle"    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 581
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/navdy/client/app/framework/AppInstance;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "GoogleApiClient : connected "

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 582
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/AppInstance;->isDeviceConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 583
    sget-object v0, Lcom/navdy/client/app/framework/AppInstance;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Remote device is already connected"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 584
    invoke-direct {p0}, Lcom/navdy/client/app/framework/AppInstance;->requestActivityRecognitionUpdates()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 586
    :cond_0
    monitor-exit p0

    return-void

    .line 581
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public onConnectionFailed(Lcom/google/android/gms/common/ConnectionResult;)V
    .locals 3
    .param p1, "connectionResult"    # Lcom/google/android/gms/common/ConnectionResult;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 595
    sget-object v0, Lcom/navdy/client/app/framework/AppInstance;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "GoogleApiClient : onConnectionFailed , Connection result : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 596
    return-void
.end method

.method public onConnectionSuspended(I)V
    .locals 3
    .param p1, "i"    # I

    .prologue
    .line 590
    sget-object v0, Lcom/navdy/client/app/framework/AppInstance;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "GoogleApiClient : onConnectionSuspended , Connection :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 591
    return-void
.end method

.method public onDeviceChanged(Lcom/navdy/service/library/device/RemoteDevice;)V
    .locals 3
    .param p1, "newDevice"    # Lcom/navdy/service/library/device/RemoteDevice;

    .prologue
    .line 491
    sget-object v0, Lcom/navdy/client/app/framework/AppInstance;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DeviceChanged: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 492
    new-instance v0, Lcom/navdy/client/app/framework/AppInstance$5;

    invoke-direct {v0, p0, p1}, Lcom/navdy/client/app/framework/AppInstance$5;-><init>(Lcom/navdy/client/app/framework/AppInstance;Lcom/navdy/service/library/device/RemoteDevice;)V

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/framework/AppInstance;->dispatchToListeners(Lcom/navdy/service/library/util/Listenable$EventDispatcher;)V

    .line 498
    return-void
.end method

.method onDeviceConnectedFirstResponder(Lcom/navdy/service/library/device/RemoteDevice;)V
    .locals 6
    .param p1, "device"    # Lcom/navdy/service/library/device/RemoteDevice;

    .prologue
    const/4 v5, 0x1

    .line 269
    iput-boolean v5, p0, Lcom/navdy/client/app/framework/AppInstance;->deviceConnected:Z

    .line 271
    iget-boolean v2, p0, Lcom/navdy/client/app/framework/AppInstance;->busIsRegistered:Z

    if-nez v2, :cond_0

    .line 272
    iput-boolean v5, p0, Lcom/navdy/client/app/framework/AppInstance;->busIsRegistered:Z

    .line 273
    invoke-static {}, Lcom/navdy/client/app/framework/util/BusProvider;->getInstance()Lcom/navdy/client/app/framework/util/BusProvider;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcom/navdy/client/app/framework/util/BusProvider;->register(Ljava/lang/Object;)V

    .line 277
    :cond_0
    invoke-static {}, Lcom/navdy/client/app/framework/util/BusProvider;->getInstance()Lcom/navdy/client/app/framework/util/BusProvider;

    move-result-object v2

    new-instance v3, Lcom/navdy/client/app/framework/DeviceConnection$DeviceConnectedEvent;

    invoke-direct {v3, p1}, Lcom/navdy/client/app/framework/DeviceConnection$DeviceConnectedEvent;-><init>(Lcom/navdy/service/library/device/RemoteDevice;)V

    invoke-virtual {v2, v3}, Lcom/navdy/client/app/framework/util/BusProvider;->post(Ljava/lang/Object;)V

    .line 280
    invoke-direct {p0}, Lcom/navdy/client/app/framework/AppInstance;->requestActivityRecognitionUpdates()V

    .line 281
    if-eqz p1, :cond_1

    .line 282
    invoke-direct {p0, p1}, Lcom/navdy/client/app/framework/AppInstance;->sendPhoneInfo(Lcom/navdy/service/library/device/RemoteDevice;)V

    .line 283
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v0

    .line 284
    .local v0, "is24HourFormat":Z
    invoke-direct {p0, p1, v0}, Lcom/navdy/client/app/framework/AppInstance;->sendCurrentTime(Lcom/navdy/service/library/device/RemoteDevice;Z)V

    .line 286
    .end local v0    # "is24HourFormat":Z
    :cond_1
    iget-object v2, p0, Lcom/navdy/client/app/framework/AppInstance;->mLocationTransmitter:Lcom/navdy/client/app/framework/servicehandler/LocationTransmitter;

    if-nez v2, :cond_2

    .line 287
    new-instance v2, Lcom/navdy/client/app/framework/servicehandler/LocationTransmitter;

    iget-object v3, p0, Lcom/navdy/client/app/framework/AppInstance;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3, p1}, Lcom/navdy/client/app/framework/servicehandler/LocationTransmitter;-><init>(Landroid/content/Context;Lcom/navdy/service/library/device/RemoteDevice;)V

    iput-object v2, p0, Lcom/navdy/client/app/framework/AppInstance;->mLocationTransmitter:Lcom/navdy/client/app/framework/servicehandler/LocationTransmitter;

    .line 289
    :cond_2
    iget-object v2, p0, Lcom/navdy/client/app/framework/AppInstance;->mBatteryStatus:Lcom/navdy/client/app/framework/servicehandler/BatteryStatusManager;

    if-nez v2, :cond_3

    .line 290
    new-instance v2, Lcom/navdy/client/app/framework/servicehandler/BatteryStatusManager;

    invoke-direct {v2, p1}, Lcom/navdy/client/app/framework/servicehandler/BatteryStatusManager;-><init>(Lcom/navdy/service/library/device/RemoteDevice;)V

    iput-object v2, p0, Lcom/navdy/client/app/framework/AppInstance;->mBatteryStatus:Lcom/navdy/client/app/framework/servicehandler/BatteryStatusManager;

    .line 292
    :cond_3
    iget-object v2, p0, Lcom/navdy/client/app/framework/AppInstance;->mNetworkStatus:Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;

    invoke-virtual {v2, p1}, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->onRemoteDeviceConnected(Lcom/navdy/service/library/device/RemoteDevice;)V

    .line 293
    iget-object v2, p0, Lcom/navdy/client/app/framework/AppInstance;->mLocationTransmitter:Lcom/navdy/client/app/framework/servicehandler/LocationTransmitter;

    invoke-virtual {v2}, Lcom/navdy/client/app/framework/servicehandler/LocationTransmitter;->start()Z

    .line 294
    iget-object v2, p0, Lcom/navdy/client/app/framework/AppInstance;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/navdy/client/app/framework/AppInstance;->datetimeUpdateReceiver:Landroid/content/BroadcastReceiver;

    sget-object v4, Lcom/navdy/client/app/framework/AppInstance;->DATETIME_CHANGE_INTENT_FILTER:Landroid/content/IntentFilter;

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 295
    iget-object v2, p0, Lcom/navdy/client/app/framework/AppInstance;->mBatteryStatus:Lcom/navdy/client/app/framework/servicehandler/BatteryStatusManager;

    invoke-virtual {v2}, Lcom/navdy/client/app/framework/servicehandler/BatteryStatusManager;->register()V

    .line 296
    const-string v2, "Hud_Connection_Established"

    invoke-static {v2}, Lcom/navdy/client/app/tracking/Tracker;->tagEvent(Ljava/lang/String;)V

    .line 297
    const-string v2, "Mobile_App_Version"

    const-string v3, "1.3.1718-e615013"

    invoke-static {v2, v3}, Lcom/navdy/client/app/framework/LocalyticsManager;->setProfileAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 301
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    .line 302
    .local v1, "sharedPreferences":Landroid/content/SharedPreferences;
    const-string v2, "recommendations_enabled"

    invoke-interface {v1, v2, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 304
    iget-object v2, p0, Lcom/navdy/client/app/framework/AppInstance;->mMainHandler:Landroid/os/Handler;

    new-instance v3, Lcom/navdy/client/app/framework/AppInstance$3;

    invoke-direct {v3, p0}, Lcom/navdy/client/app/framework/AppInstance$3;-><init>(Lcom/navdy/client/app/framework/AppInstance;)V

    const-wide/16 v4, 0x3a98

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 327
    :cond_4
    iget-object v2, p0, Lcom/navdy/client/app/framework/AppInstance;->calendarHandler:Lcom/navdy/client/app/framework/servicehandler/CalendarHandler;

    if-nez v2, :cond_5

    .line 328
    new-instance v2, Lcom/navdy/client/app/framework/servicehandler/CalendarHandler;

    iget-object v3, p0, Lcom/navdy/client/app/framework/AppInstance;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/navdy/client/app/framework/servicehandler/CalendarHandler;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/navdy/client/app/framework/AppInstance;->calendarHandler:Lcom/navdy/client/app/framework/servicehandler/CalendarHandler;

    .line 330
    :cond_5
    iget-object v2, p0, Lcom/navdy/client/app/framework/AppInstance;->calendarHandler:Lcom/navdy/client/app/framework/servicehandler/CalendarHandler;

    invoke-virtual {v2}, Lcom/navdy/client/app/framework/servicehandler/CalendarHandler;->registerObserver()V

    .line 331
    invoke-static {p1}, Lcom/navdy/client/app/ui/homescreen/CalendarUtils;->sendCalendarsToHud(Lcom/navdy/service/library/device/RemoteDevice;)V

    .line 333
    invoke-static {}, Lcom/navdy/client/app/framework/util/SupportTicketService;->submitTicketsIfConditionsAreMet()V

    .line 334
    return-void
.end method

.method onDeviceDisconnectedFirstResponder(Lcom/navdy/service/library/device/RemoteDevice;Lcom/navdy/service/library/device/connection/Connection$DisconnectCause;)V
    .locals 6
    .param p1, "device"    # Lcom/navdy/service/library/device/RemoteDevice;
    .param p2, "cause"    # Lcom/navdy/service/library/device/connection/Connection$DisconnectCause;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 338
    sget-object v2, Lcom/navdy/service/library/events/navigation/NavigationSessionState;->NAV_SESSION_STOPPED:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    iput-object v2, p0, Lcom/navdy/client/app/framework/AppInstance;->mNavigationSessionState:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    .line 339
    iput-boolean v4, p0, Lcom/navdy/client/app/framework/AppInstance;->deviceConnected:Z

    .line 340
    invoke-direct {p0}, Lcom/navdy/client/app/framework/AppInstance;->removeActivityRecognitionUpdates()V

    .line 341
    iput-boolean v4, p0, Lcom/navdy/client/app/framework/AppInstance;->isHudMapEngineReady:Z

    .line 344
    invoke-static {}, Lcom/navdy/client/app/framework/util/BusProvider;->getInstance()Lcom/navdy/client/app/framework/util/BusProvider;

    move-result-object v2

    new-instance v3, Lcom/navdy/client/app/framework/DeviceConnection$DeviceDisconnectedEvent;

    invoke-direct {v3, p1, p2}, Lcom/navdy/client/app/framework/DeviceConnection$DeviceDisconnectedEvent;-><init>(Lcom/navdy/service/library/device/RemoteDevice;Lcom/navdy/service/library/device/connection/Connection$DisconnectCause;)V

    invoke-virtual {v2, v3}, Lcom/navdy/client/app/framework/util/BusProvider;->post(Ljava/lang/Object;)V

    .line 348
    iget-object v2, p0, Lcom/navdy/client/app/framework/AppInstance;->mBatteryStatus:Lcom/navdy/client/app/framework/servicehandler/BatteryStatusManager;

    if-eqz v2, :cond_0

    .line 349
    iget-object v2, p0, Lcom/navdy/client/app/framework/AppInstance;->mBatteryStatus:Lcom/navdy/client/app/framework/servicehandler/BatteryStatusManager;

    invoke-virtual {v2}, Lcom/navdy/client/app/framework/servicehandler/BatteryStatusManager;->unregister()V

    .line 350
    iput-object v5, p0, Lcom/navdy/client/app/framework/AppInstance;->mBatteryStatus:Lcom/navdy/client/app/framework/servicehandler/BatteryStatusManager;

    .line 354
    :cond_0
    iget-object v2, p0, Lcom/navdy/client/app/framework/AppInstance;->mNetworkStatus:Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;

    invoke-virtual {v2}, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->onRemoteDeviceDisconnected()V

    .line 355
    iget-object v2, p0, Lcom/navdy/client/app/framework/AppInstance;->mLocationTransmitter:Lcom/navdy/client/app/framework/servicehandler/LocationTransmitter;

    if-eqz v2, :cond_1

    .line 356
    iget-object v2, p0, Lcom/navdy/client/app/framework/AppInstance;->mLocationTransmitter:Lcom/navdy/client/app/framework/servicehandler/LocationTransmitter;

    invoke-virtual {v2}, Lcom/navdy/client/app/framework/servicehandler/LocationTransmitter;->stop()Z

    .line 357
    iput-object v5, p0, Lcom/navdy/client/app/framework/AppInstance;->mLocationTransmitter:Lcom/navdy/client/app/framework/servicehandler/LocationTransmitter;

    .line 363
    :cond_1
    :try_start_0
    invoke-virtual {p1}, Lcom/navdy/service/library/device/RemoteDevice;->getDeviceId()Lcom/navdy/service/library/device/NavdyDeviceId;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/service/library/device/NavdyDeviceId;->getDisplayName()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 367
    .local v1, "showName":Ljava/lang/String;
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Disconnected: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2, v4}, Lcom/navdy/client/app/framework/AppInstance;->showToast(Ljava/lang/String;Z)V

    .line 371
    :try_start_1
    sget-object v2, Lcom/navdy/client/app/framework/AppInstance;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "unregistering datetimeUpdateReceiver"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 372
    iget-object v2, p0, Lcom/navdy/client/app/framework/AppInstance;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/navdy/client/app/framework/AppInstance;->datetimeUpdateReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    .line 376
    :goto_1
    const-string v2, "Hud_Connection_Lost"

    invoke-static {v2}, Lcom/navdy/client/app/tracking/Tracker;->tagEvent(Ljava/lang/String;)V

    .line 379
    iget-object v2, p0, Lcom/navdy/client/app/framework/AppInstance;->calendarHandler:Lcom/navdy/client/app/framework/servicehandler/CalendarHandler;

    if-eqz v2, :cond_2

    .line 380
    iget-object v2, p0, Lcom/navdy/client/app/framework/AppInstance;->calendarHandler:Lcom/navdy/client/app/framework/servicehandler/CalendarHandler;

    invoke-virtual {v2}, Lcom/navdy/client/app/framework/servicehandler/CalendarHandler;->unregisterObserver()V

    .line 384
    :cond_2
    iget-boolean v2, p0, Lcom/navdy/client/app/framework/AppInstance;->busIsRegistered:Z

    if-eqz v2, :cond_3

    .line 385
    iput-boolean v4, p0, Lcom/navdy/client/app/framework/AppInstance;->busIsRegistered:Z

    .line 386
    invoke-static {}, Lcom/navdy/client/app/framework/util/BusProvider;->getInstance()Lcom/navdy/client/app/framework/util/BusProvider;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcom/navdy/client/app/framework/util/BusProvider;->unregister(Ljava/lang/Object;)V

    .line 388
    :cond_3
    return-void

    .line 364
    .end local v1    # "showName":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 365
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "?"

    .restart local v1    # "showName":Ljava/lang/String;
    goto :goto_0

    .line 373
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v0

    .line 374
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    sget-object v2, Lcom/navdy/client/app/framework/AppInstance;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Cannot unregister datetime update receiver"

    invoke-virtual {v2, v3, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public onDeviceInfo(Lcom/navdy/service/library/events/DeviceInfo;)V
    .locals 4
    .param p1, "deviceInfo"    # Lcom/navdy/service/library/events/DeviceInfo;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 446
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/AppInstance;->getRemoteDevice()Lcom/navdy/service/library/device/RemoteDevice;

    move-result-object v0

    .line 447
    .local v0, "device":Lcom/navdy/service/library/device/RemoteDevice;
    if-eqz v0, :cond_0

    .line 448
    invoke-virtual {v0, p1}, Lcom/navdy/service/library/device/RemoteDevice;->setDeviceInfo(Lcom/navdy/service/library/events/DeviceInfo;)V

    .line 453
    :goto_0
    sget-object v1, Lcom/navdy/client/app/framework/AppInstance;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Display DeviceInfo: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 455
    iget-object v1, p1, Lcom/navdy/service/library/events/DeviceInfo;->capabilities:Lcom/navdy/service/library/events/Capabilities;

    if-eqz v1, :cond_1

    .line 456
    iget-object v1, p1, Lcom/navdy/service/library/events/DeviceInfo;->capabilities:Lcom/navdy/service/library/events/Capabilities;

    invoke-static {v1}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->checkCapabilities(Lcom/navdy/service/library/events/Capabilities;)V

    .line 462
    :goto_1
    const-string v1, "Hud_Serial_Number"

    iget-object v2, p1, Lcom/navdy/service/library/events/DeviceInfo;->deviceUuid:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/navdy/client/app/framework/LocalyticsManager;->setProfileAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 464
    const-string v1, "Hud_Version_Number"

    iget-object v2, p1, Lcom/navdy/service/library/events/DeviceInfo;->systemVersion:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/navdy/client/app/framework/LocalyticsManager;->setProfileAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 467
    invoke-static {p1}, Lcom/navdy/client/ota/OTAUpdateService;->bPersistDeviceInfo(Lcom/navdy/service/library/events/DeviceInfo;)V

    .line 468
    iget-object v1, p0, Lcom/navdy/client/app/framework/AppInstance;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/navdy/client/ota/OTAUpdateService;->startService(Landroid/content/Context;)V

    .line 469
    invoke-static {}, Lcom/navdy/client/app/framework/util/BusProvider;->getInstance()Lcom/navdy/client/app/framework/util/BusProvider;

    move-result-object v1

    new-instance v2, Lcom/navdy/client/app/framework/DeviceConnection$DeviceInfoEvent;

    invoke-direct {v2, p1}, Lcom/navdy/client/app/framework/DeviceConnection$DeviceInfoEvent;-><init>(Lcom/navdy/service/library/events/DeviceInfo;)V

    invoke-virtual {v1, v2}, Lcom/navdy/client/app/framework/util/BusProvider;->post(Ljava/lang/Object;)V

    .line 470
    return-void

    .line 450
    :cond_0
    sget-object v1, Lcom/navdy/client/app/framework/AppInstance;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Unable to get remote device in order to update device info!"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 459
    :cond_1
    iget-object v1, p1, Lcom/navdy/service/library/events/DeviceInfo;->legacyCapabilities:Ljava/util/List;

    invoke-static {v1}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->checkLegacyCapabilityList(Ljava/util/List;)V

    goto :goto_1
.end method

.method public onNavigationSessionStatusEvent(Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent;)V
    .locals 2
    .param p1, "status"    # Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 437
    iget-object v0, p1, Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent;->sessionState:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    iput-object v0, p0, Lcom/navdy/client/app/framework/AppInstance;->mNavigationSessionState:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    .line 439
    iget-object v0, p1, Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent;->sessionState:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    sget-object v1, Lcom/navdy/service/library/events/navigation/NavigationSessionState;->NAV_SESSION_ENGINE_READY:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    if-ne v0, v1, :cond_0

    .line 440
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/client/app/framework/AppInstance;->isHudMapEngineReady:Z

    .line 442
    :cond_0
    return-void
.end method

.method public onTransmitLocation(Lcom/navdy/service/library/events/location/TransmitLocation;)V
    .locals 2
    .param p1, "transmitLocation"    # Lcom/navdy/service/library/events/location/TransmitLocation;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 474
    if-nez p1, :cond_0

    .line 475
    sget-object v0, Lcom/navdy/client/app/framework/AppInstance;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "location transmission event with a null transmit location extension !!! WTF !!!"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 486
    :goto_0
    return-void

    .line 479
    :cond_0
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v1, p1, Lcom/navdy/service/library/events/location/TransmitLocation;->sendLocation:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 480
    sget-object v0, Lcom/navdy/client/app/framework/AppInstance;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "starting location transmission"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 481
    iget-object v0, p0, Lcom/navdy/client/app/framework/AppInstance;->mLocationTransmitter:Lcom/navdy/client/app/framework/servicehandler/LocationTransmitter;

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/servicehandler/LocationTransmitter;->start()Z

    goto :goto_0

    .line 483
    :cond_1
    sget-object v0, Lcom/navdy/client/app/framework/AppInstance;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "stopping location transmission"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 484
    iget-object v0, p0, Lcom/navdy/client/app/framework/AppInstance;->mLocationTransmitter:Lcom/navdy/client/app/framework/servicehandler/LocationTransmitter;

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/servicehandler/LocationTransmitter;->stop()Z

    goto :goto_0
.end method

.method public setLocationTransmitter(Lcom/navdy/client/app/framework/servicehandler/LocationTransmitter;)V
    .locals 2
    .param p1, "transmitter"    # Lcom/navdy/client/app/framework/servicehandler/LocationTransmitter;

    .prologue
    .line 191
    iget-object v1, p0, Lcom/navdy/client/app/framework/AppInstance;->mLocationTransmitter:Lcom/navdy/client/app/framework/servicehandler/LocationTransmitter;

    if-eqz v1, :cond_0

    .line 192
    iget-object v1, p0, Lcom/navdy/client/app/framework/AppInstance;->mLocationTransmitter:Lcom/navdy/client/app/framework/servicehandler/LocationTransmitter;

    invoke-virtual {v1}, Lcom/navdy/client/app/framework/servicehandler/LocationTransmitter;->stop()Z

    .line 195
    :cond_0
    iput-object p1, p0, Lcom/navdy/client/app/framework/AppInstance;->mLocationTransmitter:Lcom/navdy/client/app/framework/servicehandler/LocationTransmitter;

    .line 196
    iget-object v1, p0, Lcom/navdy/client/app/framework/AppInstance;->mConnectionService:Lcom/navdy/client/app/framework/service/ClientConnectionService;

    invoke-virtual {v1}, Lcom/navdy/client/app/framework/service/ClientConnectionService;->getRemoteDevice()Lcom/navdy/service/library/device/RemoteDevice;

    move-result-object v0

    .line 197
    .local v0, "device":Lcom/navdy/service/library/device/RemoteDevice;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/navdy/service/library/device/RemoteDevice;->isConnected()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/navdy/client/app/framework/AppInstance;->mLocationTransmitter:Lcom/navdy/client/app/framework/servicehandler/LocationTransmitter;

    if-eqz v1, :cond_1

    .line 198
    iget-object v1, p0, Lcom/navdy/client/app/framework/AppInstance;->mLocationTransmitter:Lcom/navdy/client/app/framework/servicehandler/LocationTransmitter;

    invoke-virtual {v1}, Lcom/navdy/client/app/framework/servicehandler/LocationTransmitter;->start()Z

    .line 199
    :cond_1
    return-void
.end method

.method public showToast(Ljava/lang/String;Z)V
    .locals 1
    .param p1, "toastText"    # Ljava/lang/String;
    .param p2, "big"    # Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 243
    const/4 v0, 0x1

    invoke-virtual {p0, p1, p2, v0}, Lcom/navdy/client/app/framework/AppInstance;->showToast(Ljava/lang/String;ZI)V

    .line 244
    return-void
.end method

.method public showToast(Ljava/lang/String;ZI)V
    .locals 2
    .param p1, "toastText"    # Ljava/lang/String;
    .param p2, "big"    # Z
    .param p3, "toastDuration"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 248
    iget-object v0, p0, Lcom/navdy/client/app/framework/AppInstance;->mMainHandler:Landroid/os/Handler;

    new-instance v1, Lcom/navdy/client/app/framework/AppInstance$2;

    invoke-direct {v1, p0, p1, p3, p2}, Lcom/navdy/client/app/framework/AppInstance$2;-><init>(Lcom/navdy/client/app/framework/AppInstance;Ljava/lang/String;IZ)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 266
    return-void
.end method
