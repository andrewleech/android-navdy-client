.class public Lcom/navdy/client/app/framework/search/NavdySearch;
.super Ljava/lang/Object;
.source "NavdySearch.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/app/framework/search/NavdySearch$SearchCallback;
    }
.end annotation


# static fields
.field private static final HUD_SEARCH_TIMEOUT:J

.field public static final MAX_RESULTS_FROM_HUD:I = 0x1e

.field public static final SEARCH_AREA_VALUE:Ljava/lang/Integer;


# instance fields
.field private handler:Landroid/os/Handler;

.field private lastHudQuery:Ljava/lang/String;

.field private logger:Lcom/navdy/service/library/log/Logger;

.field private searchCallback:Lcom/navdy/client/app/framework/search/NavdySearch$SearchCallback;

.field private searchResults:Lcom/navdy/client/app/framework/search/SearchResults;

.field private withContactPhotos:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 42
    const/4 v0, 0x0

    sput-object v0, Lcom/navdy/client/app/framework/search/NavdySearch;->SEARCH_AREA_VALUE:Ljava/lang/Integer;

    .line 54
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/navdy/client/app/framework/search/NavdySearch;->HUD_SEARCH_TIMEOUT:J

    return-void
.end method

.method public constructor <init>(Lcom/navdy/client/app/framework/search/NavdySearch$SearchCallback;)V
    .locals 2
    .param p1, "searchCallback"    # Lcom/navdy/client/app/framework/search/NavdySearch$SearchCallback;

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/framework/search/NavdySearch;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/navdy/client/app/framework/search/NavdySearch;->logger:Lcom/navdy/service/library/log/Logger;

    .line 48
    new-instance v0, Lcom/navdy/client/app/framework/search/SearchResults;

    invoke-direct {v0}, Lcom/navdy/client/app/framework/search/SearchResults;-><init>()V

    iput-object v0, p0, Lcom/navdy/client/app/framework/search/NavdySearch;->searchResults:Lcom/navdy/client/app/framework/search/SearchResults;

    .line 49
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/client/app/framework/search/NavdySearch;->searchCallback:Lcom/navdy/client/app/framework/search/NavdySearch$SearchCallback;

    .line 50
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/client/app/framework/search/NavdySearch;->withContactPhotos:Z

    .line 52
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/navdy/client/app/framework/search/NavdySearch;->handler:Landroid/os/Handler;

    .line 57
    iput-object p1, p0, Lcom/navdy/client/app/framework/search/NavdySearch;->searchCallback:Lcom/navdy/client/app/framework/search/NavdySearch$SearchCallback;

    .line 58
    return-void
.end method

.method public constructor <init>(Lcom/navdy/client/app/framework/search/NavdySearch$SearchCallback;Z)V
    .locals 0
    .param p1, "searchCallback"    # Lcom/navdy/client/app/framework/search/NavdySearch$SearchCallback;
    .param p2, "withContactPhotos"    # Z

    .prologue
    .line 61
    invoke-direct {p0, p1}, Lcom/navdy/client/app/framework/search/NavdySearch;-><init>(Lcom/navdy/client/app/framework/search/NavdySearch$SearchCallback;)V

    .line 62
    iput-boolean p2, p0, Lcom/navdy/client/app/framework/search/NavdySearch;->withContactPhotos:Z

    .line 63
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/client/app/framework/search/NavdySearch;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/framework/search/NavdySearch;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/navdy/client/app/framework/search/NavdySearch;->lastHudQuery:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/navdy/client/app/framework/search/NavdySearch;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/search/NavdySearch;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/navdy/client/app/framework/search/NavdySearch;->addMatchingContactsAndDbDestinationsToSearchResults(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$200(Lcom/navdy/client/app/framework/search/NavdySearch;)Lcom/navdy/service/library/log/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/framework/search/NavdySearch;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/navdy/client/app/framework/search/NavdySearch;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$300(Lcom/navdy/client/app/framework/search/NavdySearch;)Lcom/navdy/client/app/framework/search/SearchResults;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/framework/search/NavdySearch;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/navdy/client/app/framework/search/NavdySearch;->searchResults:Lcom/navdy/client/app/framework/search/SearchResults;

    return-object v0
.end method

.method static synthetic access$400(Lcom/navdy/client/app/framework/search/NavdySearch;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/search/NavdySearch;

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/navdy/client/app/framework/search/NavdySearch;->finishSearch()V

    return-void
.end method

.method static synthetic access$500(Lcom/navdy/client/app/framework/search/NavdySearch;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/framework/search/NavdySearch;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/navdy/client/app/framework/search/NavdySearch;->cleanUpNearestQuery(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private addMatchingContactsAndDbDestinationsToSearchResults(Ljava/lang/String;)V
    .locals 17
    .param p1, "query"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 157
    invoke-static {}, Lcom/navdy/client/app/framework/util/ContactsManager;->getInstance()Lcom/navdy/client/app/framework/util/ContactsManager;

    move-result-object v7

    .line 160
    .local v7, "contactsManager":Lcom/navdy/client/app/framework/util/ContactsManager;
    invoke-direct/range {p0 .. p1}, Lcom/navdy/client/app/framework/search/NavdySearch;->cleanUpContactQuery(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 161
    .local v4, "contactQuery":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/navdy/client/app/framework/search/NavdySearch;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "The cleaned up query for contact is: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 163
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/navdy/client/app/framework/search/NavdySearch;->withContactPhotos:Z

    if-eqz v14, :cond_3

    .line 164
    sget-object v14, Lcom/navdy/client/app/framework/util/ContactsManager$Attributes;->ADDRESS:Lcom/navdy/client/app/framework/util/ContactsManager$Attributes;

    .line 166
    invoke-static {v14}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v14

    .line 165
    invoke-virtual {v7, v4, v14}, Lcom/navdy/client/app/framework/util/ContactsManager;->getContactsAndLoadPhotos(Ljava/lang/String;Ljava/util/EnumSet;)Ljava/util/List;

    move-result-object v5

    .line 173
    .local v5, "contacts":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/ContactModel;>;"
    :goto_0
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v8

    .line 174
    .local v8, "context":Landroid/content/Context;
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, " "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const v15, 0x7f0804a7

    invoke-virtual {v8, v15}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 175
    .local v1, "and":Ljava/lang/String;
    invoke-virtual {v4, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v14

    if-eqz v14, :cond_5

    .line 178
    const-string v14, " & "

    invoke-virtual {v4, v1, v14}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 179
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/navdy/client/app/framework/search/NavdySearch;->withContactPhotos:Z

    if-eqz v14, :cond_4

    .line 180
    sget-object v14, Lcom/navdy/client/app/framework/util/ContactsManager$Attributes;->ADDRESS:Lcom/navdy/client/app/framework/util/ContactsManager$Attributes;

    .line 182
    invoke-static {v14}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v14

    .line 181
    invoke-virtual {v7, v4, v14}, Lcom/navdy/client/app/framework/util/ContactsManager;->getContactsAndLoadPhotos(Ljava/lang/String;Ljava/util/EnumSet;)Ljava/util/List;

    move-result-object v6

    .line 189
    .local v6, "contacts2":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/ContactModel;>;"
    :goto_1
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :cond_0
    :goto_2
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-eqz v15, :cond_5

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/navdy/client/app/framework/models/ContactModel;

    .line 190
    .local v3, "contact2":Lcom/navdy/client/app/framework/models/ContactModel;
    const/4 v13, 0x0

    .line 191
    .local v13, "found":Z
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :cond_1
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v16

    if-eqz v16, :cond_2

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/navdy/client/app/framework/models/ContactModel;

    .line 192
    .local v2, "contact1":Lcom/navdy/client/app/framework/models/ContactModel;
    invoke-virtual {v2, v3}, Lcom/navdy/client/app/framework/models/ContactModel;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_1

    .line 193
    const/4 v13, 0x1

    .line 197
    .end local v2    # "contact1":Lcom/navdy/client/app/framework/models/ContactModel;
    :cond_2
    if-nez v13, :cond_0

    .line 198
    invoke-interface {v5, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 168
    .end local v1    # "and":Ljava/lang/String;
    .end local v3    # "contact2":Lcom/navdy/client/app/framework/models/ContactModel;
    .end local v5    # "contacts":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/ContactModel;>;"
    .end local v6    # "contacts2":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/ContactModel;>;"
    .end local v8    # "context":Landroid/content/Context;
    .end local v13    # "found":Z
    :cond_3
    sget-object v14, Lcom/navdy/client/app/framework/util/ContactsManager$Attributes;->ADDRESS:Lcom/navdy/client/app/framework/util/ContactsManager$Attributes;

    .line 170
    invoke-static {v14}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v14

    .line 169
    invoke-virtual {v7, v4, v14}, Lcom/navdy/client/app/framework/util/ContactsManager;->getContactsWithoutLoadingPhotos(Ljava/lang/String;Ljava/util/EnumSet;)Ljava/util/List;

    move-result-object v5

    .restart local v5    # "contacts":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/ContactModel;>;"
    goto :goto_0

    .line 184
    .restart local v1    # "and":Ljava/lang/String;
    .restart local v8    # "context":Landroid/content/Context;
    :cond_4
    sget-object v14, Lcom/navdy/client/app/framework/util/ContactsManager$Attributes;->ADDRESS:Lcom/navdy/client/app/framework/util/ContactsManager$Attributes;

    .line 186
    invoke-static {v14}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v14

    .line 185
    invoke-virtual {v7, v4, v14}, Lcom/navdy/client/app/framework/util/ContactsManager;->getContactsWithoutLoadingPhotos(Ljava/lang/String;Ljava/util/EnumSet;)Ljava/util/List;

    move-result-object v6

    .restart local v6    # "contacts2":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/ContactModel;>;"
    goto :goto_1

    .line 203
    .end local v6    # "contacts2":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/ContactModel;>;"
    :cond_5
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/navdy/client/app/framework/search/NavdySearch;->searchResults:Lcom/navdy/client/app/framework/search/SearchResults;

    invoke-virtual {v14, v5}, Lcom/navdy/client/app/framework/search/SearchResults;->setContacts(Ljava/util/List;)V

    .line 207
    invoke-static/range {p1 .. p1}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getDestinationsFromSearchQuery(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v11

    .line 209
    .local v11, "destinationsFromDatabase":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/client/app/framework/models/Destination;>;"
    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v14

    if-eqz v14, :cond_6

    .line 211
    const-string v14, " & "

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v14}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 213
    .local v9, "dbDestinationQuery":Ljava/lang/String;
    invoke-static {v9}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getDestinationsFromSearchQuery(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v12

    .line 215
    .local v12, "destinationsFromDatabase2":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/client/app/framework/models/Destination;>;"
    if-eqz v11, :cond_7

    .line 217
    invoke-static {v11, v12}, Lcom/navdy/client/app/framework/models/Destination;->mergeAndDeduplicateLists(Ljava/util/List;Ljava/util/List;)Ljava/util/List;

    move-result-object v11

    .end local v11    # "destinationsFromDatabase":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/client/app/framework/models/Destination;>;"
    check-cast v11, Ljava/util/ArrayList;

    .line 227
    .end local v9    # "dbDestinationQuery":Ljava/lang/String;
    .end local v12    # "destinationsFromDatabase2":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/client/app/framework/models/Destination;>;"
    .restart local v11    # "destinationsFromDatabase":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/client/app/framework/models/Destination;>;"
    :cond_6
    :goto_3
    if-eqz v11, :cond_8

    .line 228
    invoke-virtual {v11}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :goto_4
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-eqz v15, :cond_8

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/navdy/client/app/framework/models/Destination;

    .line 229
    .local v10, "destination":Lcom/navdy/client/app/framework/models/Destination;
    iget v15, v10, Lcom/navdy/client/app/framework/models/Destination;->id:I

    invoke-static {v15}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getCacheKeysForDestinationId(I)Ljava/util/ArrayList;

    move-result-object v15

    iput-object v15, v10, Lcom/navdy/client/app/framework/models/Destination;->rawAddressVariations:Ljava/util/ArrayList;

    goto :goto_4

    .line 221
    .end local v10    # "destination":Lcom/navdy/client/app/framework/models/Destination;
    .restart local v9    # "dbDestinationQuery":Ljava/lang/String;
    .restart local v12    # "destinationsFromDatabase2":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/client/app/framework/models/Destination;>;"
    :cond_7
    move-object v11, v12

    goto :goto_3

    .line 233
    .end local v9    # "dbDestinationQuery":Ljava/lang/String;
    .end local v12    # "destinationsFromDatabase2":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/client/app/framework/models/Destination;>;"
    :cond_8
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/navdy/client/app/framework/search/NavdySearch;->searchResults:Lcom/navdy/client/app/framework/search/SearchResults;

    invoke-virtual {v14, v11}, Lcom/navdy/client/app/framework/search/SearchResults;->setDestinationsFromDatabase(Ljava/util/ArrayList;)V

    .line 234
    return-void
.end method

.method private cleanUpContactQuery(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "query"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 238
    const v1, 0x7f070003

    invoke-static {v1, p1}, Lcom/navdy/client/app/framework/util/VoiceCommandUtils;->getMatchingVoiceCommand(ILjava/lang/String;)Lcom/navdy/client/app/framework/util/VoiceCommand;

    move-result-object v0

    .line 239
    .local v0, "vc":Lcom/navdy/client/app/framework/util/VoiceCommand;
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/navdy/client/app/framework/util/VoiceCommand;->query:Ljava/lang/String;

    invoke-static {v1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 242
    .end local p1    # "query":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object p1

    .restart local p1    # "query":Ljava/lang/String;
    :cond_1
    iget-object p1, v0, Lcom/navdy/client/app/framework/util/VoiceCommand;->query:Ljava/lang/String;

    goto :goto_0
.end method

.method private cleanUpNearestQuery(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "query"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 247
    const v1, 0x7f07000c

    invoke-static {v1, p1}, Lcom/navdy/client/app/framework/util/VoiceCommandUtils;->getMatchingVoiceCommand(ILjava/lang/String;)Lcom/navdy/client/app/framework/util/VoiceCommand;

    move-result-object v0

    .line 248
    .local v0, "vc":Lcom/navdy/client/app/framework/util/VoiceCommand;
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/navdy/client/app/framework/util/VoiceCommand;->query:Ljava/lang/String;

    invoke-static {v1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 251
    .end local p1    # "query":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object p1

    .restart local p1    # "query":Ljava/lang/String;
    :cond_1
    iget-object p1, v0, Lcom/navdy/client/app/framework/util/VoiceCommand;->query:Ljava/lang/String;

    goto :goto_0
.end method

.method private finishSearch()V
    .locals 2

    .prologue
    .line 145
    iget-object v0, p0, Lcom/navdy/client/app/framework/search/NavdySearch;->searchCallback:Lcom/navdy/client/app/framework/search/NavdySearch$SearchCallback;

    if-eqz v0, :cond_0

    .line 146
    iget-object v0, p0, Lcom/navdy/client/app/framework/search/NavdySearch;->searchCallback:Lcom/navdy/client/app/framework/search/NavdySearch$SearchCallback;

    iget-object v1, p0, Lcom/navdy/client/app/framework/search/NavdySearch;->searchResults:Lcom/navdy/client/app/framework/search/SearchResults;

    invoke-interface {v0, v1}, Lcom/navdy/client/app/framework/search/NavdySearch$SearchCallback;->onSearchCompleted(Lcom/navdy/client/app/framework/search/SearchResults;)V

    .line 148
    :cond_0
    return-void
.end method

.method public static getDestinationListFromGoogleTextSearchResults(Ljava/util/List;)Ljava/util/ArrayList;
    .locals 5
    .param p0    # Ljava/util/List;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/client/app/framework/models/Destination;",
            ">;"
        }
    .end annotation

    .prologue
    .line 369
    .local p0, "googleTextSearchResults":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 370
    .local v1, "destinations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/client/app/framework/models/Destination;>;"
    if-eqz p0, :cond_0

    .line 371
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;

    .line 372
    .local v2, "googleTextSearchDestinationResult":Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;
    new-instance v0, Lcom/navdy/client/app/framework/models/Destination;

    invoke-direct {v0}, Lcom/navdy/client/app/framework/models/Destination;-><init>()V

    .line 373
    .local v0, "destination":Lcom/navdy/client/app/framework/models/Destination;
    sget-object v4, Lcom/navdy/client/app/framework/models/Destination$SearchType;->TEXT_SEARCH:Lcom/navdy/client/app/framework/models/Destination$SearchType;

    invoke-virtual {v2, v4, v0}, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->toModelDestinationObject(Lcom/navdy/client/app/framework/models/Destination$SearchType;Lcom/navdy/client/app/framework/models/Destination;)V

    .line 374
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 377
    .end local v0    # "destination":Lcom/navdy/client/app/framework/models/Destination;
    .end local v2    # "googleTextSearchDestinationResult":Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;
    :cond_0
    return-object v1
.end method

.method static getDestinationListFromPlaceSearchResults(Ljava/util/List;)Ljava/util/ArrayList;
    .locals 5
    .param p0    # Ljava/util/List;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/places/PlacesSearchResult;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/client/app/framework/models/Destination;",
            ">;"
        }
    .end annotation

    .prologue
    .line 356
    .local p0, "placesSearchResults":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/places/PlacesSearchResult;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 357
    .local v1, "destinations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/client/app/framework/models/Destination;>;"
    if-eqz p0, :cond_0

    .line 358
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/navdy/service/library/events/places/PlacesSearchResult;

    .line 359
    .local v2, "placesSearchResult":Lcom/navdy/service/library/events/places/PlacesSearchResult;
    new-instance v0, Lcom/navdy/client/app/framework/models/Destination;

    invoke-direct {v0}, Lcom/navdy/client/app/framework/models/Destination;-><init>()V

    .line 360
    .local v0, "destination":Lcom/navdy/client/app/framework/models/Destination;
    invoke-static {v2, v0}, Lcom/navdy/client/app/framework/models/Destination;->placesSearchResultToDestinationObject(Lcom/navdy/service/library/events/places/PlacesSearchResult;Lcom/navdy/client/app/framework/models/Destination;)V

    .line 361
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 364
    .end local v0    # "destination":Lcom/navdy/client/app/framework/models/Destination;
    .end local v2    # "placesSearchResult":Lcom/navdy/service/library/events/places/PlacesSearchResult;
    :cond_0
    return-object v1
.end method

.method private handleHudSearchResponse(Lcom/navdy/service/library/events/places/PlacesSearchResponse;)V
    .locals 2
    .param p1, "placesSearchResponse"    # Lcom/navdy/service/library/events/places/PlacesSearchResponse;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 304
    iget-object v0, p0, Lcom/navdy/client/app/framework/search/NavdySearch;->lastHudQuery:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/framework/search/NavdySearch;->lastHudQuery:Ljava/lang/String;

    iget-object v1, p1, Lcom/navdy/service/library/events/places/PlacesSearchResponse;->searchQuery:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 330
    :cond_0
    :goto_0
    return-void

    .line 308
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/client/app/framework/search/NavdySearch;->lastHudQuery:Ljava/lang/String;

    .line 310
    new-instance v0, Lcom/navdy/client/app/framework/search/NavdySearch$3;

    invoke-direct {v0, p0, p1}, Lcom/navdy/client/app/framework/search/NavdySearch$3;-><init>(Lcom/navdy/client/app/framework/search/NavdySearch;Lcom/navdy/service/library/events/places/PlacesSearchResponse;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    .line 329
    invoke-virtual {v0, v1}, Lcom/navdy/client/app/framework/search/NavdySearch$3;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method private runOfflineDisconnectSearch(Ljava/lang/String;)V
    .locals 2
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    .line 339
    new-instance v0, Lcom/navdy/client/app/framework/search/NavdySearch$4;

    invoke-direct {v0, p0, p1}, Lcom/navdy/client/app/framework/search/NavdySearch$4;-><init>(Lcom/navdy/client/app/framework/search/NavdySearch;Ljava/lang/String;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    .line 350
    invoke-virtual {v0, v1}, Lcom/navdy/client/app/framework/search/NavdySearch$4;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 351
    return-void
.end method

.method private runOnlineSearch(Ljava/lang/String;)V
    .locals 2
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    .line 260
    new-instance v0, Lcom/navdy/client/app/framework/search/NavdySearch$2;

    invoke-direct {v0, p0, p1}, Lcom/navdy/client/app/framework/search/NavdySearch$2;-><init>(Lcom/navdy/client/app/framework/search/NavdySearch;Ljava/lang/String;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    .line 293
    invoke-virtual {v0, v1}, Lcom/navdy/client/app/framework/search/NavdySearch$2;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 294
    return-void
.end method

.method public static runServiceSearch(Lcom/navdy/client/app/framework/search/GooglePlacesSearch;Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;)V
    .locals 2
    .param p0, "googlePlacesSearch"    # Lcom/navdy/client/app/framework/search/GooglePlacesSearch;
    .param p1, "serviceType"    # Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;

    .prologue
    .line 381
    sget-object v0, Lcom/navdy/client/app/framework/search/NavdySearch$5;->$SwitchMap$com$navdy$client$app$framework$search$GooglePlacesSearch$ServiceTypes:[I

    invoke-virtual {p1}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 387
    invoke-virtual {p0, p1}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->runServiceSearchWebApi(Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;)V

    .line 390
    :goto_0
    return-void

    .line 384
    :pswitch_0
    invoke-virtual {p1}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;->getHudServiceType()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->runTextSearchWebApi(Ljava/lang/String;)V

    goto :goto_0

    .line 381
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public onPlacesSearchResponse(Lcom/navdy/service/library/events/places/PlacesSearchResponse;)V
    .locals 3
    .param p1, "placesSearchResponse"    # Lcom/navdy/service/library/events/places/PlacesSearchResponse;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 70
    iget-object v0, p0, Lcom/navdy/client/app/framework/search/NavdySearch;->handler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 72
    if-nez p1, :cond_0

    .line 73
    iget-object v0, p0, Lcom/navdy/client/app/framework/search/NavdySearch;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Receiving search results from HUD that is null!"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 74
    invoke-direct {p0}, Lcom/navdy/client/app/framework/search/NavdySearch;->finishSearch()V

    .line 84
    :goto_0
    return-void

    .line 78
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/framework/search/NavdySearch;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Receiving search results from HUD for \""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/navdy/service/library/events/places/PlacesSearchResponse;->searchQuery:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/navdy/service/library/events/places/PlacesSearchResponse;->results:Ljava/util/List;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 82
    invoke-direct {p0, p1}, Lcom/navdy/client/app/framework/search/NavdySearch;->handleHudSearchResponse(Lcom/navdy/service/library/events/places/PlacesSearchResponse;)V

    .line 83
    invoke-static {}, Lcom/navdy/client/app/framework/util/BusProvider;->getInstance()Lcom/navdy/client/app/framework/util/BusProvider;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/navdy/client/app/framework/util/BusProvider;->unregister(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public runSearch(Ljava/lang/String;)V
    .locals 6
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    const/16 v5, 0x1e

    .line 100
    iget-object v2, p0, Lcom/navdy/client/app/framework/search/NavdySearch;->searchCallback:Lcom/navdy/client/app/framework/search/NavdySearch$SearchCallback;

    if-nez v2, :cond_0

    .line 101
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "NoSearchCallBackException"

    new-instance v4, Ljava/lang/Throwable;

    const-string v5, "No point running a search if there is no callback specified."

    invoke-direct {v4, v5}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2

    .line 105
    :cond_0
    iget-object v2, p0, Lcom/navdy/client/app/framework/search/NavdySearch;->searchResults:Lcom/navdy/client/app/framework/search/SearchResults;

    invoke-virtual {v2, p1}, Lcom/navdy/client/app/framework/search/SearchResults;->setQuery(Ljava/lang/String;)V

    .line 107
    iget-object v2, p0, Lcom/navdy/client/app/framework/search/NavdySearch;->searchCallback:Lcom/navdy/client/app/framework/search/NavdySearch$SearchCallback;

    invoke-interface {v2}, Lcom/navdy/client/app/framework/search/NavdySearch$SearchCallback;->onSearchStarted()V

    .line 109
    invoke-static {}, Lcom/navdy/client/app/framework/AppInstance;->getInstance()Lcom/navdy/client/app/framework/AppInstance;

    move-result-object v0

    .line 110
    .local v0, "appInstance":Lcom/navdy/client/app/framework/AppInstance;
    invoke-virtual {v0}, Lcom/navdy/client/app/framework/AppInstance;->canReachInternet()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 111
    invoke-direct {p0, p1}, Lcom/navdy/client/app/framework/search/NavdySearch;->runOnlineSearch(Ljava/lang/String;)V

    .line 142
    :goto_0
    return-void

    .line 112
    :cond_1
    invoke-virtual {v0}, Lcom/navdy/client/app/framework/AppInstance;->isDeviceConnected()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 113
    invoke-static {}, Lcom/navdy/client/app/framework/util/BusProvider;->getInstance()Lcom/navdy/client/app/framework/util/BusProvider;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcom/navdy/client/app/framework/util/BusProvider;->register(Ljava/lang/Object;)V

    .line 115
    invoke-direct {p0, p1}, Lcom/navdy/client/app/framework/search/NavdySearch;->cleanUpNearestQuery(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 116
    iput-object p1, p0, Lcom/navdy/client/app/framework/search/NavdySearch;->lastHudQuery:Ljava/lang/String;

    .line 117
    iget-object v2, p0, Lcom/navdy/client/app/framework/search/NavdySearch;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Requesting search from HUD for \""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " with a "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/navdy/client/app/framework/search/NavdySearch;->SEARCH_AREA_VALUE:Ljava/lang/Integer;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " search area"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " and "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " max results"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 120
    new-instance v2, Lcom/navdy/service/library/events/places/PlacesSearchRequest$Builder;

    invoke-direct {v2}, Lcom/navdy/service/library/events/places/PlacesSearchRequest$Builder;-><init>()V

    .line 121
    invoke-virtual {v2, p1}, Lcom/navdy/service/library/events/places/PlacesSearchRequest$Builder;->searchQuery(Ljava/lang/String;)Lcom/navdy/service/library/events/places/PlacesSearchRequest$Builder;

    move-result-object v2

    sget-object v3, Lcom/navdy/client/app/framework/search/NavdySearch;->SEARCH_AREA_VALUE:Ljava/lang/Integer;

    .line 122
    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/places/PlacesSearchRequest$Builder;->searchArea(Ljava/lang/Integer;)Lcom/navdy/service/library/events/places/PlacesSearchRequest$Builder;

    move-result-object v2

    .line 123
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/places/PlacesSearchRequest$Builder;->maxResults(Ljava/lang/Integer;)Lcom/navdy/service/library/events/places/PlacesSearchRequest$Builder;

    move-result-object v2

    .line 124
    invoke-virtual {v2}, Lcom/navdy/service/library/events/places/PlacesSearchRequest$Builder;->build()Lcom/navdy/service/library/events/places/PlacesSearchRequest;

    move-result-object v1

    .line 125
    .local v1, "searchRequest":Lcom/navdy/service/library/events/places/PlacesSearchRequest;
    invoke-static {v1}, Lcom/navdy/client/app/framework/DeviceConnection;->postEvent(Lcom/squareup/wire/Message;)Z

    .line 127
    iget-object v2, p0, Lcom/navdy/client/app/framework/search/NavdySearch;->handler:Landroid/os/Handler;

    new-instance v3, Lcom/navdy/client/app/framework/search/NavdySearch$1;

    invoke-direct {v3, p0}, Lcom/navdy/client/app/framework/search/NavdySearch$1;-><init>(Lcom/navdy/client/app/framework/search/NavdySearch;)V

    sget-wide v4, Lcom/navdy/client/app/framework/search/NavdySearch;->HUD_SEARCH_TIMEOUT:J

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 140
    .end local v1    # "searchRequest":Lcom/navdy/service/library/events/places/PlacesSearchRequest;
    :cond_2
    invoke-direct {p0, p1}, Lcom/navdy/client/app/framework/search/NavdySearch;->runOfflineDisconnectSearch(Ljava/lang/String;)V

    goto/16 :goto_0
.end method
