.class public Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler;
.super Ljava/lang/Object;
.source "SettingsServiceHandler.java"


# static fields
.field public static final sLogger:Lcom/navdy/service/library/log/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 37
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    invoke-static {}, Lcom/navdy/client/app/framework/util/BusProvider;->getInstance()Lcom/navdy/client/app/framework/util/BusProvider;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/navdy/client/app/framework/util/BusProvider;->register(Ljava/lang/Object;)V

    .line 42
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler;Lcom/squareup/wire/Message;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler;
    .param p1, "x1"    # Lcom/squareup/wire/Message;

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler;->sendRemoteMessage(Lcom/squareup/wire/Message;)V

    return-void
.end method

.method static synthetic access$100(Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler;Landroid/content/SharedPreferences$Editor;Ljava/lang/String;Ljava/lang/Boolean;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler;
    .param p1, "x1"    # Landroid/content/SharedPreferences$Editor;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Ljava/lang/Boolean;

    .prologue
    .line 36
    invoke-direct {p0, p1, p2, p3}, Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler;->putOnlyIfNotNull(Landroid/content/SharedPreferences$Editor;Ljava/lang/String;Ljava/lang/Boolean;)V

    return-void
.end method

.method private putOnlyIfNotNull(Landroid/content/SharedPreferences$Editor;Ljava/lang/String;Ljava/lang/Boolean;)V
    .locals 1
    .param p1, "editor"    # Landroid/content/SharedPreferences$Editor;
    .param p2, "key"    # Ljava/lang/String;
    .param p3, "value"    # Ljava/lang/Boolean;

    .prologue
    .line 226
    if-eqz p3, :cond_0

    .line 227
    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-interface {p1, p2, v0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 229
    :cond_0
    return-void
.end method

.method private sendRemoteMessage(Lcom/squareup/wire/Message;)V
    .locals 0
    .param p1, "message"    # Lcom/squareup/wire/Message;

    .prologue
    .line 269
    invoke-static {p1}, Lcom/navdy/client/app/framework/DeviceConnection;->postEvent(Lcom/squareup/wire/Message;)Z

    .line 270
    return-void
.end method


# virtual methods
.method public onCannedMessagesRequest(Lcom/navdy/service/library/events/glances/CannedMessagesRequest;)V
    .locals 3
    .param p1, "cannedMessagesRequest"    # Lcom/navdy/service/library/events/glances/CannedMessagesRequest;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 138
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler$5;

    invoke-direct {v1, p0, p1}, Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler$5;-><init>(Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler;Lcom/navdy/service/library/events/glances/CannedMessagesRequest;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 164
    return-void
.end method

.method public onConnectionStateChange(Lcom/navdy/service/library/events/connection/ConnectionStateChange;)V
    .locals 1
    .param p1, "connectionStateChange"    # Lcom/navdy/service/library/events/connection/ConnectionStateChange;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 256
    invoke-static {}, Lcom/navdy/client/app/framework/DeviceConnection;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 257
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->updateAllSettingsIfNecessary()V

    .line 259
    :cond_0
    return-void
.end method

.method public onDriverProfileRequest(Lcom/navdy/service/library/events/preferences/DriverProfilePreferencesRequest;)V
    .locals 3
    .param p1, "driverProfilePreferencesRequest"    # Lcom/navdy/service/library/events/preferences/DriverProfilePreferencesRequest;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 46
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler$1;

    invoke-direct {v1, p0, p1}, Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler$1;-><init>(Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler;Lcom/navdy/service/library/events/preferences/DriverProfilePreferencesRequest;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 65
    return-void
.end method

.method public onInputRequest(Lcom/navdy/service/library/events/preferences/InputPreferencesRequest;)V
    .locals 3
    .param p1, "inputPreferencesRequest"    # Lcom/navdy/service/library/events/preferences/InputPreferencesRequest;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 69
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler$2;

    invoke-direct {v1, p0, p1}, Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler$2;-><init>(Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler;Lcom/navdy/service/library/events/preferences/InputPreferencesRequest;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 88
    return-void
.end method

.method public onNavigationPreferencesUpdate(Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate;)V
    .locals 3
    .param p1, "navigationPreferencesUpdate"    # Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 168
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler$6;

    invoke-direct {v1, p0, p1}, Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler$6;-><init>(Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler;Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 223
    return-void
.end method

.method public onNavigationRequest(Lcom/navdy/service/library/events/preferences/NavigationPreferencesRequest;)V
    .locals 3
    .param p1, "navigationPreferencesRequest"    # Lcom/navdy/service/library/events/preferences/NavigationPreferencesRequest;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 115
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler$4;

    invoke-direct {v1, p0, p1}, Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler$4;-><init>(Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler;Lcom/navdy/service/library/events/preferences/NavigationPreferencesRequest;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 134
    return-void
.end method

.method public onNotificationRequest(Lcom/navdy/service/library/events/preferences/NotificationPreferencesRequest;)V
    .locals 3
    .param p1, "notificationPreferencesRequest"    # Lcom/navdy/service/library/events/preferences/NotificationPreferencesRequest;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 233
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler$7;

    invoke-direct {v1, p0, p1}, Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler$7;-><init>(Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler;Lcom/navdy/service/library/events/preferences/NotificationPreferencesRequest;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 252
    return-void
.end method

.method public onSpeakerRequest(Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferencesRequest;)V
    .locals 3
    .param p1, "displaySpeakerPreferencesRequest"    # Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferencesRequest;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 92
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler$3;

    invoke-direct {v1, p0, p1}, Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler$3;-><init>(Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler;Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferencesRequest;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 111
    return-void
.end method
