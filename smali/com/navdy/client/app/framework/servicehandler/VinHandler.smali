.class public Lcom/navdy/client/app/framework/servicehandler/VinHandler;
.super Ljava/lang/Object;
.source "VinHandler.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    invoke-static {}, Lcom/navdy/client/app/framework/util/BusProvider;->getInstance()Lcom/navdy/client/app/framework/util/BusProvider;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/navdy/client/app/framework/util/BusProvider;->register(Ljava/lang/Object;)V

    .line 17
    return-void
.end method


# virtual methods
.method public onObdStatusResponse(Lcom/navdy/service/library/events/obd/ObdStatusResponse;)V
    .locals 2
    .param p1, "odbStatusResponse"    # Lcom/navdy/service/library/events/obd/ObdStatusResponse;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 21
    const-string v0, "VIN"

    iget-object v1, p1, Lcom/navdy/service/library/events/obd/ObdStatusResponse;->vin:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/LocalyticsManager;->setProfileAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 22
    return-void
.end method
