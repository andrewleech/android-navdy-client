.class public Lcom/navdy/client/debug/SubmitTicketFragment;
.super Lcom/navdy/client/app/ui/base/BaseFragment;
.source "SubmitTicketFragment.java"


# static fields
.field public static final BUG:Ljava/lang/String; = "Bug"

.field public static final CONFIRMATION_DIALOG:I = 0x2

.field private static final CONNECTION_TIMEOUT_IN_MINUTES:I = 0x2

.field public static final DESCRIPTION:Ljava/lang/String; = "description"

.field public static final ERROR_DIALOG:I = 0x1

.field public static final FIELDS:Ljava/lang/String; = "fields"

.field public static final ISSUE_TYPE:Ljava/lang/String; = "issuetype"

.field private static final JIRA_ATTACHMENTS:Ljava/lang/String; = "/attachments/"

.field private static final JIRA_ISSUE_API_URL:Ljava/lang/String; = "https://navdyhud.atlassian.net/rest/api/2/issue/"

.field public static final KEY:Ljava/lang/String; = "key"

.field public static final NAME:Ljava/lang/String; = "name"

.field public static final PROGRESS_DIALOG:I = 0x3

.field public static final PROJECT:Ljava/lang/String; = "project"

.field private static final PROJECT_KEY:Ljava/lang/String; = "NAND"

.field public static final PULL_LOGS_TIMEOUT:J = 0x3a98L

.field public static final SUMMARY:Ljava/lang/String; = "summary"

.field private static final TEMP_FILE_TIMESTAMP_FORMAT:Ljava/lang/String; = "\'device_log\'_yyyy_dd_MM-hh_mm_ss_aa\'.txt\'"

.field private static final format:Ljava/text/SimpleDateFormat;


# instance fields
.field private ENCODED_CRED:Ljava/lang/String;

.field mDescription:Landroid/widget/EditText;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f1003ec
    .end annotation
.end field

.field private mDeviceLogFile:Ljava/lang/String;

.field private mDisplayLogFile:Ljava/lang/String;

.field mHttpManager:Lcom/navdy/service/library/network/http/IHttpManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private mJiraClient:Lcom/navdy/service/library/network/http/services/JiraClient;

.field mTitle:Landroid/widget/EditText;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f1003eb
    .end annotation
.end field

.field spinner:Landroid/widget/Spinner;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f1003ea
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 86
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "\'device_log\'_yyyy_dd_MM-hh_mm_ss_aa\'.txt\'"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/SubmitTicketFragment;->format:Ljava/text/SimpleDateFormat;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/navdy/client/app/ui/base/BaseFragment;-><init>()V

    .line 68
    const-string v0, ""

    iput-object v0, p0, Lcom/navdy/client/debug/SubmitTicketFragment;->ENCODED_CRED:Ljava/lang/String;

    .line 97
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/client/debug/SubmitTicketFragment;->mHttpManager:Lcom/navdy/service/library/network/http/IHttpManager;

    return-void
.end method

.method static synthetic access$000()Ljava/text/SimpleDateFormat;
    .locals 1

    .prologue
    .line 62
    sget-object v0, Lcom/navdy/client/debug/SubmitTicketFragment;->format:Ljava/text/SimpleDateFormat;

    return-object v0
.end method

.method static synthetic access$100(Lcom/navdy/client/debug/SubmitTicketFragment;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/debug/SubmitTicketFragment;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/navdy/client/debug/SubmitTicketFragment;->mDeviceLogFile:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$102(Lcom/navdy/client/debug/SubmitTicketFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/debug/SubmitTicketFragment;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 62
    iput-object p1, p0, Lcom/navdy/client/debug/SubmitTicketFragment;->mDeviceLogFile:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$200(Lcom/navdy/client/debug/SubmitTicketFragment;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/debug/SubmitTicketFragment;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/navdy/client/debug/SubmitTicketFragment;->mDisplayLogFile:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$202(Lcom/navdy/client/debug/SubmitTicketFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/debug/SubmitTicketFragment;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 62
    iput-object p1, p0, Lcom/navdy/client/debug/SubmitTicketFragment;->mDisplayLogFile:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$300(Lcom/navdy/client/debug/SubmitTicketFragment;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/debug/SubmitTicketFragment;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/navdy/client/debug/SubmitTicketFragment;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$400(Lcom/navdy/client/debug/SubmitTicketFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/debug/SubmitTicketFragment;

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/navdy/client/debug/SubmitTicketFragment;->bSubmitTicket()V

    return-void
.end method

.method static synthetic access$500(Lcom/navdy/client/debug/SubmitTicketFragment;)Lcom/navdy/service/library/network/http/services/JiraClient;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/debug/SubmitTicketFragment;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/navdy/client/debug/SubmitTicketFragment;->mJiraClient:Lcom/navdy/service/library/network/http/services/JiraClient;

    return-object v0
.end method

.method private bSubmitTicket()V
    .locals 13

    .prologue
    const/4 v2, 0x0

    .line 226
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v6

    .line 227
    .local v6, "appContext":Landroid/content/Context;
    if-nez v6, :cond_0

    .line 313
    :goto_0
    return-void

    .line 230
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/debug/SubmitTicketFragment;->mTitle:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 231
    .local v3, "summary":Ljava/lang/String;
    iget-object v0, p0, Lcom/navdy/client/debug/SubmitTicketFragment;->mDescription:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 232
    .local v4, "description":Ljava/lang/String;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 233
    .local v8, "descriptionBuilder":Ljava/lang/StringBuilder;
    const-string v0, "Device : "

    .line 234
    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    .line 235
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " : "

    .line 236
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    .line 237
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " (API "

    .line 238
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 239
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")\n\n"

    .line 240
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 241
    invoke-static {}, Lcom/navdy/client/ota/OTAUpdateService;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v11

    .line 242
    .local v11, "preferences":Landroid/content/SharedPreferences;
    const-string v0, "DEVICE_ID"

    invoke-interface {v11, v0, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 243
    .local v10, "lastConnectedDeviceId":Ljava/lang/String;
    if-eqz v10, :cond_1

    .line 244
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "SW_VERSION_NAME"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v11, v0, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 245
    .local v12, "swVersionString":Ljava/lang/String;
    const-string v0, "Display : "

    .line 246
    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 247
    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " , ID :"

    .line 248
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 249
    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n\n"

    .line 250
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 253
    .end local v12    # "swVersionString":Ljava/lang/String;
    :cond_1
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getCustomerPreferences()Landroid/content/SharedPreferences;

    move-result-object v7

    .line 254
    .local v7, "customerPrefs":Landroid/content/SharedPreferences;
    const-string v0, "fullName =    "

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "fname"

    const-string v2, ""

    invoke-interface {v7, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 255
    const-string v0, "email =       "

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "email"

    const-string v2, ""

    invoke-interface {v7, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 257
    const-string v0, "carYear =     "

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "vehicle-year"

    const-string v2, ""

    invoke-interface {v7, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 258
    const-string v0, "carMake =     "

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "vehicle-make"

    const-string v2, ""

    invoke-interface {v7, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 259
    const-string v0, "carModel =    "

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "vehicle-model"

    const-string v2, ""

    invoke-interface {v7, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 261
    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 263
    :try_start_0
    iget-object v0, p0, Lcom/navdy/client/debug/SubmitTicketFragment;->mJiraClient:Lcom/navdy/service/library/network/http/services/JiraClient;

    iget-object v1, p0, Lcom/navdy/client/debug/SubmitTicketFragment;->spinner:Landroid/widget/Spinner;

    invoke-virtual {v1}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Bug"

    new-instance v5, Lcom/navdy/client/debug/SubmitTicketFragment$3;

    invoke-direct {v5, p0, v6}, Lcom/navdy/client/debug/SubmitTicketFragment$3;-><init>(Lcom/navdy/client/debug/SubmitTicketFragment;Landroid/content/Context;)V

    invoke-virtual/range {v0 .. v5}, Lcom/navdy/service/library/network/http/services/JiraClient;->submitTicket(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 310
    :catch_0
    move-exception v9

    .line 311
    .local v9, "e":Ljava/io/IOException;
    const/4 v0, 0x1

    const v1, 0x7f080160

    invoke-virtual {p0, v1}, Lcom/navdy/client/debug/SubmitTicketFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f080161

    invoke-virtual {p0, v2}, Lcom/navdy/client/debug/SubmitTicketFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/navdy/client/debug/SubmitTicketFragment;->showSimpleDialog(ILjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method


# virtual methods
.method public createDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;
    .locals 8
    .param p1, "id"    # I
    .param p2, "arguments"    # Landroid/os/Bundle;

    .prologue
    .line 195
    invoke-virtual {p0}, Lcom/navdy/client/debug/SubmitTicketFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 196
    .local v0, "context":Landroid/content/Context;
    if-nez v0, :cond_0

    .line 197
    const/4 v3, 0x0

    .line 222
    :goto_0
    return-object v3

    .line 199
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 222
    invoke-super {p0, p1, p2}, Lcom/navdy/client/app/ui/base/BaseFragment;->createDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v3

    goto :goto_0

    .line 201
    :pswitch_0
    new-instance v3, Landroid/app/ProgressDialog;

    invoke-direct {v3, v0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 202
    .local v3, "progressDialog":Landroid/app/ProgressDialog;
    const-string v5, "title"

    invoke-virtual {p2, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 203
    .local v4, "title":Ljava/lang/String;
    if-eqz v4, :cond_1

    .line 204
    invoke-virtual {v3, v4}, Landroid/app/ProgressDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 205
    :cond_1
    const-string v5, "message"

    invoke-virtual {p2, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 206
    .local v2, "message":Ljava/lang/String;
    if-eqz v2, :cond_2

    .line 207
    invoke-virtual {v3, v2}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 209
    :cond_2
    const/4 v5, 0x0

    invoke-virtual {v3, v5}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    goto :goto_0

    .line 212
    .end local v2    # "message":Ljava/lang/String;
    .end local v3    # "progressDialog":Landroid/app/ProgressDialog;
    .end local v4    # "title":Ljava/lang/String;
    :pswitch_1
    invoke-super {p0, p1, p2}, Lcom/navdy/client/app/ui/base/BaseFragment;->createDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v1

    check-cast v1, Landroid/app/AlertDialog;

    .line 213
    .local v1, "dialog":Landroid/app/AlertDialog;
    const/4 v5, -0x1

    const-string v6, "Ok"

    new-instance v7, Lcom/navdy/client/debug/SubmitTicketFragment$2;

    invoke-direct {v7, p0}, Lcom/navdy/client/debug/SubmitTicketFragment$2;-><init>(Lcom/navdy/client/debug/SubmitTicketFragment;)V

    invoke-virtual {v1, v5, v6, v7}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    move-object v3, v1

    .line 220
    goto :goto_0

    .line 199
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const-wide/16 v4, 0x2

    .line 104
    invoke-super {p0, p1}, Lcom/navdy/client/app/ui/base/BaseFragment;->onCreate(Landroid/os/Bundle;)V

    .line 107
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f08055b

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/navdy/client/app/framework/util/CredentialsUtils;->getCredentials(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/client/debug/SubmitTicketFragment;->ENCODED_CRED:Ljava/lang/String;

    .line 108
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, p0}, Lcom/navdy/client/app/framework/Injector;->inject(Landroid/content/Context;Ljava/lang/Object;)V

    .line 110
    iget-object v1, p0, Lcom/navdy/client/debug/SubmitTicketFragment;->mHttpManager:Lcom/navdy/service/library/network/http/IHttpManager;

    invoke-interface {v1}, Lcom/navdy/service/library/network/http/IHttpManager;->getClientCopy()Lokhttp3/OkHttpClient$Builder;

    move-result-object v1

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, v4, v5, v2}, Lokhttp3/OkHttpClient$Builder;->readTimeout(JLjava/util/concurrent/TimeUnit;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v1

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, v4, v5, v2}, Lokhttp3/OkHttpClient$Builder;->connectTimeout(JLjava/util/concurrent/TimeUnit;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lokhttp3/OkHttpClient$Builder;->build()Lokhttp3/OkHttpClient;

    move-result-object v0

    .line 111
    .local v0, "httpClient":Lokhttp3/OkHttpClient;
    new-instance v1, Lcom/navdy/service/library/network/http/services/JiraClient;

    invoke-direct {v1, v0}, Lcom/navdy/service/library/network/http/services/JiraClient;-><init>(Lokhttp3/OkHttpClient;)V

    iput-object v1, p0, Lcom/navdy/client/debug/SubmitTicketFragment;->mJiraClient:Lcom/navdy/service/library/network/http/services/JiraClient;

    .line 112
    iget-object v1, p0, Lcom/navdy/client/debug/SubmitTicketFragment;->mJiraClient:Lcom/navdy/service/library/network/http/services/JiraClient;

    iget-object v2, p0, Lcom/navdy/client/debug/SubmitTicketFragment;->ENCODED_CRED:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/network/http/services/JiraClient;->setEncodedCredentials(Ljava/lang/String;)V

    .line 113
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 119
    invoke-virtual {p0}, Lcom/navdy/client/debug/SubmitTicketFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f0300fe

    invoke-virtual {v2, v3, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 120
    .local v1, "view":Landroid/view/View;
    invoke-static {p0, v1}, Lbutterknife/ButterKnife;->inject(Ljava/lang/Object;Landroid/view/View;)V

    .line 121
    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f090023

    const v4, 0x1090008

    invoke-static {v2, v3, v4}, Landroid/widget/ArrayAdapter;->createFromResource(Landroid/content/Context;II)Landroid/widget/ArrayAdapter;

    move-result-object v0

    .line 123
    .local v0, "adapter":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/CharSequence;>;"
    const v2, 0x1090009

    invoke-virtual {v0, v2}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 124
    iget-object v2, p0, Lcom/navdy/client/debug/SubmitTicketFragment;->spinner:Landroid/widget/Spinner;

    invoke-virtual {v2, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 125
    iget-object v2, p0, Lcom/navdy/client/debug/SubmitTicketFragment;->spinner:Landroid/widget/Spinner;

    invoke-virtual {v2, v5}, Landroid/widget/Spinner;->setSelection(I)V

    .line 126
    return-object v1
.end method

.method onSubmit(Landroid/view/View;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f1003ed
        }
    .end annotation

    .prologue
    const v2, 0x7f080160

    const/4 v3, 0x1

    .line 131
    iget-object v0, p0, Lcom/navdy/client/debug/SubmitTicketFragment;->mTitle:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 132
    invoke-virtual {p0, v2}, Lcom/navdy/client/debug/SubmitTicketFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0803a5

    invoke-virtual {p0, v1}, Lcom/navdy/client/debug/SubmitTicketFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v3, v0, v1}, Lcom/navdy/client/debug/SubmitTicketFragment;->showSimpleDialog(ILjava/lang/String;Ljava/lang/String;)V

    .line 191
    :goto_0
    return-void

    .line 135
    :cond_0
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/navdy/service/library/util/SystemUtils;->isConnectedToNetwork(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 136
    invoke-virtual {p0, v2}, Lcom/navdy/client/debug/SubmitTicketFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0802fe

    invoke-virtual {p0, v1}, Lcom/navdy/client/debug/SubmitTicketFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v3, v0, v1}, Lcom/navdy/client/debug/SubmitTicketFragment;->showSimpleDialog(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 139
    :cond_1
    const/4 v0, 0x3

    const v1, 0x7f080107

    invoke-virtual {p0, v1}, Lcom/navdy/client/debug/SubmitTicketFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f080108

    invoke-virtual {p0, v2}, Lcom/navdy/client/debug/SubmitTicketFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/navdy/client/debug/SubmitTicketFragment;->showSimpleDialog(ILjava/lang/String;Ljava/lang/String;)V

    .line 140
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/client/debug/SubmitTicketFragment$1;

    invoke-direct {v1, p0}, Lcom/navdy/client/debug/SubmitTicketFragment$1;-><init>(Lcom/navdy/client/debug/SubmitTicketFragment;)V

    invoke-virtual {v0, v1, v3}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    goto :goto_0
.end method
